%define name    kino
%define version 1.0.1
%define release 1
%define cvs 0
%define cvsdate 20040330

%define redhat		%(test ! -f /etc/redhat-release ; echo $?)
%define redhat		%(test ! -f /etc/fedora-release ; echo $?)
%define mandrake	%(test ! -f /etc/mandrake-release ; echo $?)

Summary: DV Video Editor
Name: %{name}
Version: %{version}
%if %{cvs}
Release: 0.%{cvsdate}
Source0: %{name}-%{cvsdate}.tar.bz2
%else
Release: %{release}
Source0: %{name}-%{version}.tar.gz
%endif
URL: http://kino.schirmacher.de
License: GPL
Group: Applications/Multimedia
BuildRoot: %{_tmppath}/%{name}-buildroot
Requires: libraw1394, libavc1394 >= 0.4.1, libdv >= 0.103
Requires: libxml2, mjpegtools
Requires: libsamplerate
Requires: libasound2
%if %{mandrake}
Requires: libglade2.0_0
%endif
%if %{redhat}
Requires: libglade2
%endif
Requires: libquicktime
Buildrequires: libraw1394-devel libavc1394-devel 
BuildRequires: libdv-devel >= 0.103
BuildRequires: libxml2-devel gtk+-devel
BuildRequires: libquicktime-devel
BuildRequires: libsndfile
BuildRequires: libsamplerate-devel
BuildRequires: libasound2-devel
%if %{mandrake}
BuildRequires: libglade2_0-devel
%endif
%if %{redhat}
BuildRequires: libglade2-devel
%endif
%if %{cvs}
BuildRequires: automake, autoconf, libtool
%endif

%description
The new generation of digital camcorders use the Digital Video (DV) data
format. Kino allows you to record, create, edit, and play movies recorded
with DV camcorders. Unlike other editors, this program uses many keyboard
commands for fast navigating and editing inside the movie.

%package devel
Group:	Development/C++
Summary: Header files for kino plugin development
Requires: %name = %version-%release

%description devel
This contains the C++ headers needed to build extensions for kino.

%prep
rm -rf $RPM_BUILD_ROOT

%if %{cvs}
%setup -q -n %{name}
%else
%setup -q
%endif

%build
%if %{cvs}
./autogen.sh
%endif
%configure \
       --with-quicktime \
       --with-hotplug-script-dir=%{buildroot}/%{_sysconfdir}/hotplug/usb \
       --with-hotplug-usermap-dir=%{buildroot}/%{_libdir}/hotplug/kino
make
%install
%makeinstall
%find_lang kino
%clean
rm -rf %buildroot

%files -f kino.lang
%defattr(-,root,root)
%doc AUTHORS BUGS ChangeLog NEWS README* TODO
%{_bindir}/*
%{_mandir}/man1/*
%dir %{_datadir}/kino/
%{_datadir}/kino/*
%{_datadir}/applications/*
%{_datadir}/pixmaps/kino.png
%{_sysconfdir}/hotplug/usb/kino-jogshuttle
%{_libdir}/hotplug/kino/usb.usermap

%files devel
%defattr(-,root,root)
%doc COPYING
%dir %{_includedir}/kino/
%{_includedir}/kino/*

%changelog
* Wed Aug 11 2004 David Hollis <dhollis@davehollis.com>
- Update to 0.7.3

* Sat Apr 10 2004 Dan Dennedy <dan@dennedy.org>
- added requires for libsamplerate

* Sat Mar 30 2004 David Hollis <dhollis@davehollis.com>
- Update to 0.7.1
- Added libdv 0.102 dependency
- Added support for translations and usb-hotplug support files

* Mon Dec 15 2003 David Hollis <dhollis@davehollis.com>
- Update to pre 0.7.0 CVS, adjust spec accordingly

* Thu Oct 24 2002 Bill Peck <bill@pecknet.com> 0.6.1-pn1
- Changed the versioning to the new standard. 
- Added desktop entry for Gnome (Does this cover KDE too?).

* Sun Oct 20 2002 Bill Peck <bill@pecknet.com> 0.6-1
- Final version of kino 0.6 is released.

* Thu Oct 10 2002 Bill Peck <bill@pecknet.com> 0.60-CVS-1pn
- Modified for RedHat

* Thu Aug 15 2002 G�z Waschk <waschk@linux-mandrake.com> 0.51-1mdk
- remove the build hacks
- drop patch
- 0.51
- libdv2

* Wed Jul 31 2002 G�z Waschk <waschk@linux-mandrake.com> 0.5-3mdk
- add missing files
- build fixes
- fix summary
- libdv-compat1
- fix buildrequirements
- quiet tar

* Mon Mar 18 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.5-2mdk
- rebuild

* Wed Feb 06 2002 Lenny Cartier <lenny@mandrakesoft.com> 0.5-1mdk
- 0.5
- icon

* Mon Oct 08 2001 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.46-2mdk
- fix {Build,}Requires
- use libdv1 too

* Tue Oct 02 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.46-1mdk
- 0.46

* Mon Aug 27 2001 Lenny Cartier <lenny@mandrakesoft.com> 0.45-1mdk
- 0.45
- recompiled against new libraw1394 and lilbavc1394

* Fri Dec 29 2000 Lenny Cartier <lenny@mandrakesoft.com> 0.3-1mdk 
- updated to 0.3

* Mon Dec 04 2000 Lenny Cartier <lenny@mandrakesoft.com> 0.21-1mdk
- new in contribs
- used srpm from rufus t firefly <rufus.t.firefly@linux-mandrake.com>
  - v0.21-1mdk (initial packaging)

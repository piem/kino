/*
*  oss.h
*
*  OSS code Copyright (C) Charles 'Buck' Krasic - January 2001 (from libdv)
*  OSS fragment handling addition by Tomoaki Hayasaka <hayasakas@postman.riken.go.jp>
*  ALSA code Copyright 2005 Troy Rollo
*  Integration code by Dan Dennedy
*
*  This program is free software; you can redistribute it and/or modify it
*  under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2, or (at your
*  option) any later version.
*
*  This program is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with GNU Make; see the file COPYING.  If not, write to
*  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA. 
*/

#ifndef KINO_OSS_H
#define KINO_OSS_H

#include <libdv/dv_types.h>
#include <inttypes.h>
#ifdef HAVE_ALSA
#include <alsa/asoundlib.h>
#endif

typedef struct {
  /* common */
  int16_t             *buffer;
  uint8_t             *arg_audio_file;
  char                *arg_audio_device;
  /* for OSS */
  int                 fd;
#ifdef HAVE_ALSA
  /* for ALSA */
  snd_pcm_t           *pcmh;
  snd_pcm_hw_params_t *hwparams;
  int                  writefail_noted;
  int                  channels;
#endif
} kino_sound_t;

#ifdef __cplusplus
extern "C"
{
#endif

	extern kino_sound_t *kino_sound_new( void );
	extern int kino_sound_init( dv_audio_t * audio, kino_sound_t * sound, char const *device );
	extern int kino_sound_play( dv_audio_t * audio, kino_sound_t * sound, int16_t **out );
	extern int kino_sound_player( kino_sound_t * sound, void * out, int size );
	extern void kino_sound_close( kino_sound_t * sound );

#ifdef __cplusplus
}
#endif

#endif

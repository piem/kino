/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include "displayer.h"
#include <gdk/gdkx.h>

AspectRatioCalculator::AspectRatioCalculator( int widgetWidth, int widgetHeight,
        int imageWidth, int imageHeight,
        int maxImageWidth, int maxImageHeight )
{
	double ratioWidth = ( double ) widgetWidth / ( double ) imageWidth;
	double ratioHeight = ( double ) widgetHeight / ( double ) imageHeight;
	double ratioConstant = ratioHeight < ratioWidth ?
	                       ratioHeight : ratioWidth;
	width = ( int ) ( imageWidth * ratioConstant + 0.5 );
	height = ( int ) ( imageHeight * ratioConstant + 0.5 );
	if ( width > maxImageWidth )
		width = maxImageWidth;
	if ( height > maxImageHeight )
		height = maxImageHeight;
	x = ( widgetWidth - width ) / 2;
	y = ( widgetHeight - height ) / 2;
}

AspectRatioCalculator::AspectRatioCalculator( int widgetWidth, int widgetHeight,
        int imageWidth, int imageHeight, bool isPAL, bool isWidescreen )
{

	static Preferences & prefs = Preferences::getInstance();

	if ( prefs.displayMode == 0 && imageWidth == 360 )
	{
		imageWidth *= 2;
		imageHeight *= 2;
	}

	// cerr << "Original: " << imageWidth << "x" << imageHeight << endl;

	if ( isWidescreen )
	{
		if ( isPAL )
		{
			// PAL: 720 x 576 at 16:9 aspect ratio: Use an image size of 1024 x 576.
			imageWidth = imageWidth * 1024 / 720;
			imageHeight = imageHeight * imageHeight / 576;
		}
		else
		{
			// NTSC: 720 x 480 at 16:9 aspect ratio: Use an image size of 854 x 480
			imageWidth = imageWidth * 854 / 720;
			imageHeight = imageHeight * imageHeight / 480;
		}
	}
	else
	{
		if ( isPAL )
		{
			// PAL: 720 x 576 at 4:3 aspect ratio: Use an image size of 768 x 576.
			imageWidth = imageWidth * 768 / 720;
			imageHeight = imageHeight * imageHeight / 576;
		}
		else
		{
			// NTSC: 720 x 480 at 4:3 aspect ratio: Use an image size of 720 x 540
			imageWidth = imageWidth * imageWidth / 720;
			imageHeight = imageHeight * 540 / 480;
		}
	}

	// cerr << "Changed : " << imageWidth << "x" << imageHeight << endl;

	double ratioWidth = ( double ) widgetWidth / ( double ) imageWidth;
	double ratioHeight = ( double ) widgetHeight / ( double ) imageHeight;

	if ( ratioHeight < ratioWidth )
	{
		width = ( int ) ( imageWidth * ratioHeight + 0.5 );
		height = ( int ) ( imageHeight * ratioHeight + 0.5 );
	}
	else
	{
		width = ( int ) ( imageWidth * ratioWidth + 0.5 );
		height = ( int ) ( imageHeight * ratioWidth + 0.5 );
	}

	// cerr << "Final   : " << width << "x" << height << endl;
	// cerr << "Ratio   : " << (double)width / (double)height << endl;

	x = ( widgetWidth - width ) / 2;
	y = ( widgetHeight - height ) / 2;
}

/** Indicates if an object can be used to render images on the running
	system.
*/

bool Displayer::usable()
{
	return false;
}

/** Indicates the format required by the abstract put method.
*/

DisplayerInput Displayer::format()
{
	return DISPLAY_NONE;
}

/** Expect width of input to put.
*/

int Displayer::preferredWidth()
{
	return img_width;
}

/** Expect width of input to put.
*/

int Displayer::preferredHeight()
{
	return img_height;
}

/** Put an image of a given width and height with the expected input
	format (as indicated by the format method).
 
	\param image	image of correct format and specified width/height
	\param width	width of image
	\param height	height of image
*/

void Displayer::put( void *image, int width, int height )
{
	if ( width == preferredWidth() && height == preferredHeight() )
	{
		put( image );
	}
	else
	{
		reformat( format(), format(), image, width, height );
		put( pixels );
	}
}

/** Put an image of a given width and height with the input format
	specified.
 
	\param format	format of image
	\param image	image of specified width/height
	\param width	width of image
	\param height	height of image
 
*/

void Displayer::put( DisplayerInput inFormat, void *image,
                     int width, int height )
{
	if ( format() == inFormat &&
	        width == preferredWidth() && height == preferredHeight() )
	{
		put( image );
	}
	else
	{
		reformat( inFormat, format(), image, width, height );
		put( pixels );
	}
}

void Displayer::Reformat424( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight )
{
	// Fixed value for rounding to the middle pixel (instead of defaulting to top left)
	int rounding = 1 << 15;
	// xfactor and yfactor represent the granularity of the output, relative to the input
	unsigned int xfactor = ( inWidth << 16 ) / outWidth;
	unsigned int yfactor = ( inHeight << 16 ) / outHeight;
	// multiplication of x and yfactors to the output height
	unsigned int ymax = yfactor * outHeight;
	unsigned int xmax = xfactor * outWidth;
	// y is used to point to the start of each line in the input
	unsigned int y = 0;
	// i is a temporary variable used in iterating through each line
	unsigned int i = 0;
	// o is the position in the output
	unsigned int o = 0;

	for ( unsigned int yft = 0; yft < ymax; yft += yfactor )
	{
		y = ( ( yft + rounding ) >> 16 ) * inWidth * 4;
		for ( unsigned int xft = 0; xft < xmax; xft += xfactor )
		{
			i = y + ( ( xft + rounding ) >> 16 ) * 4;
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ];
		}
	}
}

void Displayer::Reformat222( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight )
{
	// Fixed value for rounding to the middle pixel (instead of defaulting to top left)
	int rounding = 1 << 15;
	// xfactor and yfactor represent the granularity of the output, relative to the input
	unsigned int xfactor = ( inWidth << 16 ) / outWidth;
	unsigned int yfactor = ( inHeight << 16 ) / outHeight;
	// multiplication of x and yfactors to the output height
	unsigned int ymax = yfactor * outHeight;
	unsigned int xmax = xfactor * outWidth;
	// y is used to point to the start of each line in the input
	unsigned int y = 0;
	// i is a temporary variable used in iterating through each line
	unsigned int i = 0;
	// o is the position in the output
	unsigned int o = 0;

	for ( unsigned int yft = 0; yft < ymax; yft += yfactor )
	{
		y = ( ( yft + rounding ) >> 16 ) * inWidth * 2;
		for ( unsigned int xft = 0; xft < xmax; xft += xfactor )
		{
			i = y + ( ( xft + rounding ) >> 16 ) * 2;
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ];
		}
	}
}

void Displayer::Reformat323( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight )
{
	// Fixed value for rounding to the middle pixel (instead of defaulting to top left)
	int rounding = 1 << 15;
	// xfactor and yfactor represent the granularity of the output, relative to the input
	unsigned int xfactor = ( inWidth << 16 ) / outWidth;
	unsigned int yfactor = ( inHeight << 16 ) / outHeight;
	// multiplication of x and yfactors to the output height
	unsigned int ymax = yfactor * outHeight;
	unsigned int xmax = xfactor * outWidth;
	// y is used to point to the start of each line in the input
	unsigned int y = 0;
	// i is a temporary variable used in iterating through each line
	unsigned int i = 0;
	// o is the position in the output
	unsigned int o = 0;

	for ( unsigned int yft = 0; yft < ymax; yft += yfactor )
	{
		y = ( ( yft + rounding ) >> 16 ) * inWidth * 3;
		for ( unsigned int xft = 0; xft < xmax; xft += xfactor )
		{
			i = y + ( ( xft + rounding ) >> 16 ) * 3;
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ];
		}
	}
}

void Displayer::Reformat323R( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight )
{
	// Fixed value for rounding to the middle pixel (instead of defaulting to top left)
	int rounding = 1 << 15;
	// xfactor and yfactor represent the granularity of the output, relative to the input
	unsigned int xfactor = ( inWidth << 16 ) / outWidth;
	unsigned int yfactor = ( inHeight << 16 ) / outHeight;
	// multiplication of x and yfactors to the output height
	unsigned int ymax = yfactor * outHeight;
	unsigned int xmax = xfactor * outWidth;
	// y is used to point to the start of each line in the input
	unsigned int y = 0;
	// i is a temporary variable used in iterating through each line
	unsigned int i = 0;
	// o is the position in the output
	unsigned int o = 0;

	for ( unsigned int yft = 0; yft < ymax; yft += yfactor )
	{
		y = ( ( yft + rounding ) >> 16 ) * inWidth * 3;
		for ( unsigned int xft = 0; xft < xmax; xft += xfactor )
		{
			i = y + ( ( xft + rounding ) >> 16 ) * 3 + 2;
			pixels[ o ++ ] = image[ i ];
			pixels[ o ++ ] = image[ -- i ];
			pixels[ o ++ ] = image[ -- i ];
		}
	}
}

void Displayer::Reformat324( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight )
{
	// Fixed value for rounding to the middle pixel (instead of defaulting to top left)
	int rounding = 1 << 15;
	// xfactor and yfactor represent the granularity of the output, relative to the input
	unsigned int xfactor = ( inWidth << 16 ) / outWidth;
	unsigned int yfactor = ( inHeight << 16 ) / outHeight;
	// multiplication of x and yfactors to the output height
	unsigned int ymax = yfactor * outHeight;
	unsigned int xmax = xfactor * outWidth;
	// y is used to point to the start of each line in the input
	unsigned int y = 0;
	// i is a temporary variable used in iterating through each line
	unsigned int i = 0;
	// o is the position in the output
	unsigned int o = 0;

	for ( unsigned int yft = 0; yft < ymax; yft += yfactor )
	{
		y = ( ( yft + rounding ) >> 16 ) * inWidth * 3;
		for ( unsigned int xft = 0; xft < xmax; xft += xfactor )
		{
			i = y + ( ( xft + rounding ) >> 16 ) * 3;
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ++ ];
			pixels[ o ++ ] = image[ i ];
			pixels[ o ++ ] = 0;
		}
	}
}

/** Reformat.
 
  	\param inFormat
	\param outFromat
	\param image
	\param width
	\param height
*/

void Displayer::reformat( DisplayerInput inFormat, int outFormat, void *image,
                          int width, int height )
{
	//struct timeval tv;
	//gettimeofday(&tv, NULL);
	//long long starttime = tv.tv_sec * 1000000 + tv.tv_usec;

	unsigned char * img = ( unsigned char * ) image;
	if ( inFormat == DISPLAY_YUV && outFormat == DISPLAY_YUV )
		Reformat424( img, width, height, this->preferredWidth(), this->preferredHeight() );
	else if ( inFormat == DISPLAY_RGB16 && outFormat == DISPLAY_RGB16 )
		Reformat222( img, width, height, this->preferredWidth(), this->preferredHeight() );
	else if ( inFormat == DISPLAY_BGR && outFormat == DISPLAY_RGB )
		Reformat323( img, width, height, this->preferredWidth(), this->preferredHeight() );
	else if ( inFormat == DISPLAY_BGR && outFormat == DISPLAY_BGR0 )
		Reformat324( img, width, height, this->preferredWidth(), this->preferredHeight() );
	// Incorrect, but covers the most correctly [take care when introducing new formats]
	else if ( inFormat == outFormat )
		Reformat323( img, width, height, this->preferredWidth(), this->preferredHeight() );

	//gettimeofday(&tv, NULL);
	//long long endtime = tv.tv_sec * 1000000 + tv.tv_usec;
	//cerr << "Reformatting took " << endtime - starttime << endl;
}

/** X Displayer Constructor (retained for historical reasons - not used).
*/

XDisplayer::XDisplayer( GtkWidget *drawingarea ) : xImage( NULL )
{
	this->drawingarea = drawingarea;
	this->canuse = true;

	cerr << ">> Trying X Displayer" << endl;
#if 0

	memset( &shmInfo, 0, sizeof( XShmSegmentInfo ) );
	GdkWindowPrivate *priv = ( GdkWindowPrivate* ) drawingarea->window;
	window = priv->xwindow;
	display = priv->xdisplay;

	gc = XCreateGC( display, window, 0, &values );

	Visual *v = ( ( GdkVisualPrivate * ) gdk_visual_get_system() ) ->xvisual;
	depth = DefaultDepth( display, DefaultScreen( display ) );

	xImage = ( XImage * ) XShmCreateImage( display, v, depth, ZPixmap,
	                                       ( char * ) NULL, &shmInfo,
	                                       this->preferredWidth(),
	                                       this->preferredHeight() );

	shmInfo.shmid = shmget( IPC_PRIVATE,
	                        this->preferredWidth() * this->preferredHeight() * 4,
	                        IPC_CREAT | 0777 );

	shmInfo.shmaddr = ( char * ) shmat( shmInfo.shmid, 0, 0 );
	xImage->data = shmInfo.shmaddr;
	shmInfo.readOnly = 0;
	if ( !XShmAttach( display, &shmInfo ) )
	{
		this->canuse = false;
		fprintf( stderr, "Cannot attach shared memory\n" );
		xImage = NULL;
		return ;
	}
#endif
}

XDisplayer::~XDisplayer()
{
#if 0
	if ( shmInfo.shmaddr != NULL )
	{
		XShmDetach( display, &shmInfo );
		shmdt( shmInfo.shmaddr );
	}
	if ( xImage != NULL )
	{
		XDestroyImage( xImage );
	}
#endif
}

bool XDisplayer::usable()
{
	return canuse;
}

DisplayerInput XDisplayer::format()
{
	return DISPLAY_RGB;
}

void XDisplayer::put( void *data )
{
	unsigned char * image = ( unsigned char * ) data;
	memcpy( xImage->data, image,
	        this->preferredWidth() * this->preferredHeight() * 4 );
	XShmPutImage( display, window, gc, xImage, 0, 0, 0, 0,
	              this->preferredWidth(), this->preferredHeight(), false );
}

/** XVideo Constructor.
*/

XvDisplayer::XvDisplayer( GtkWidget *drawingarea, int width, int height, bool isPAL, bool isWidescreen ) :
		xvImage( NULL )
{

	cerr << ">> Trying XVideo at " << width << "x" << height << endl;

	this->drawingarea = drawingarea;
	img_width = width;
	img_height = height;
	this->isPAL = isPAL;
	this->isWidescreen = isWidescreen;

	shmInfo.shmaddr = NULL;
	gotPort = false;

	window = GDK_WINDOW_XID( drawingarea->window );
	display = GDK_WINDOW_XDISPLAY( drawingarea->window );

	unsigned int	count;
	XvAdaptorInfo	*adaptorInfo;

	if ( XvQueryAdaptors( display, window, &count, &adaptorInfo ) == Success )
	{

		cerr << ">>> XvQueryAdaptors count: " << count << endl;
		for ( unsigned int n = 0; gotPort == false && n < count; ++n )
		{
			// Diagnostics
			cerr << ">>> Xv: " << adaptorInfo[ n ].name
			<< ": ports " << adaptorInfo[ n ].base_id
			<< " - " << adaptorInfo[ n ].base_id +
			adaptorInfo[ n ].num_ports - 1
			<< endl;

			for ( port = adaptorInfo[ n ].base_id;
			        port < adaptorInfo[ n ].base_id + adaptorInfo[ n ].num_ports;
			        port ++ )
			{
				if ( XvGrabPort( display, port, CurrentTime ) == 0 )
				{

					int formats;
					XvImageFormatValues *list;

					list = XvListImageFormats( display, port, &formats );

					cerr << ">>> formats supported: " << formats << endl;

					for ( int i = 0; i < formats; i ++ )
					{
						fprintf( stderr, ">>>     0x%x (%c%c%c%c) %s\n",
						         list[ i ].id,
						         ( list[ i ].id ) & 0xff,
						         ( list[ i ].id >> 8 ) & 0xff,
						         ( list[ i ].id >> 16 ) & 0xff,
						         ( list[ i ].id >> 24 ) & 0xff,
						         ( list[ i ].format == XvPacked ) ? "packed" : "planar" );
						if ( list[ i ].id == 0x32595559 && !gotPort )
							gotPort = true;
					}

					if ( !gotPort )
					{
						XvUngrabPort( display, port, CurrentTime );
					}
					else
					{
						grabbedPort = port;
						break;
					}
				}
			}
		}

		if ( gotPort )
		{
			int num;
			unsigned int unum;
			XvEncodingInfo *enc;
			
			XvQueryEncodings( display, grabbedPort, &unum, &enc );
			for ( unsigned int index = 0; index < unum; index ++ )
			{
				fprintf( stderr, ">>> %d: %s, %ldx%ld rate = %d/%d\n", index, enc->name,
				        enc->width, enc->height, enc->rate.numerator,
				        enc->rate.denominator );
			}
			
			XvAttribute *xvattr = XvQueryPortAttributes( display, port, &num );
			for ( int k = 0; k < num; k++ )
			{
				if ( xvattr[k].flags & XvSettable ) 
				{
					if ( strcmp( xvattr[k].name, "XV_AUTOPAINT_COLORKEY") == 0 )
					{
						Atom val_atom = XInternAtom( display, xvattr[k].name, False );
						if ( XvSetPortAttribute( display, port, val_atom, 1 ) != Success )
							fprintf(stderr, "Couldn't set Xv attribute %s\n", xvattr[k].name);
					}
					else if (  strcmp( xvattr[k].name, "XV_COLORKEY") == 0 )
					{
						Atom val_atom = XInternAtom( display, xvattr[k].name, False );
						if ( XvSetPortAttribute( display, port, val_atom, 0x010102 ) != Success )
							fprintf(stderr, "Couldn't set Xv attribute %s\n", xvattr[k].name);
					}
				}
			}
		}

		if ( gotPort )
		{
			gc = XCreateGC( display, window, 0, &values );

			xvImage = ( XvImage * ) XvShmCreateImage( display, port, 0x32595559, 0, width, height, &shmInfo );

			shmInfo.shmid = shmget( IPC_PRIVATE, xvImage->data_size, IPC_CREAT | 0777 );
			if (shmInfo.shmid < 0) {
				perror("shmget");
				gotPort = false;
			} else {
				shmInfo.shmaddr = ( char * ) shmat( shmInfo.shmid, 0, 0 );
				xvImage->data = shmInfo.shmaddr;
				shmInfo.readOnly = 0;
				if ( !XShmAttach( gdk_display, &shmInfo ) )
				{
					gotPort = false;
				}
				XSync( display, false );
				shmctl( shmInfo.shmid, IPC_RMID, 0 );
#if 0
				xvImage = ( XvImage * ) XvCreateImage( display, port, 0x32595559, pix, width , height );
#endif
			}
		}
	}
	else
	{
		gotPort = false;
	}
}

XvDisplayer::~XvDisplayer()
{
	cerr << ">> Destroying XV Displayer" << endl;

	if ( gotPort )
	{
		XvUngrabPort( display, grabbedPort, CurrentTime );
	}

	if ( xvImage != NULL )
		XvStopVideo( display, port, window );

	if ( shmInfo.shmaddr != NULL )
	{
		XShmDetach( display, &shmInfo );
		shmctl( shmInfo.shmid, IPC_RMID, 0 );
		shmdt( shmInfo.shmaddr );
	}
	if ( xvImage != NULL )
		XFree( xvImage );
}

bool XvDisplayer::usable()
{
	return gotPort;
}

DisplayerInput XvDisplayer::format()
{
	return DISPLAY_YUV;
}

void XvDisplayer::put( void *image )
{
	AspectRatioCalculator calc( ( ( GtkWidget * ) drawingarea ) ->allocation.width,
	                            ( ( GtkWidget * ) drawingarea ) ->allocation.height,
	                            this->preferredWidth(), this->preferredHeight(),
	                            isPAL, isWidescreen );

	memcpy( xvImage->data, image, xvImage->data_size );

	XvShmPutImage( display, port, window, gc, xvImage,
	               0, 0, this->preferredWidth(), this->preferredHeight(),
	               calc.x, calc.y, calc.width, calc.height, false );
#if 0

	XvPutImage( display, port, window, gc, xvImage,
	            0, 0, this->preferredWidth(), this->preferredHeight(),
	            calc.x, calc.y, calc.width, calc.height );
#endif
}

GdkDisplayer::GdkDisplayer( GtkWidget *drawingarea, int width, int height, bool is_wide )
{
	this->drawingarea = drawingarea;
	img_width = width;
	img_height = height;
	m_is_wide = is_wide;
}

GdkDisplayer::~GdkDisplayer()
{}

bool GdkDisplayer::usable()
{
	return true;
}

DisplayerInput GdkDisplayer::format()
{
	return DISPLAY_RGB;
}

void GdkDisplayer::put( void *data )
{
	unsigned char * image = ( unsigned char * ) data;

	AspectRatioCalculator calc( ( ( GtkWidget * ) drawingarea ) ->allocation.width,
	                            ( ( GtkWidget * ) drawingarea ) ->allocation.height,
	                            this->preferredWidth(), this->preferredHeight(),
	                            img_height == 576, m_is_wide );

	GdkGC *gc = gdk_gc_new( drawingarea->window );
	GdkPixbuf *pix = gdk_pixbuf_new_from_data( image, GDK_COLORSPACE_RGB, FALSE, 8, preferredWidth(), preferredHeight(), preferredWidth() * 3, NULL, NULL );
	GdkPixbuf *im = gdk_pixbuf_scale_simple( pix, calc.width, calc.height, GDK_INTERP_NEAREST );
	gdk_draw_pixbuf( drawingarea->window, gc, im, 0, 0, 0, 0, -1, -1, GDK_RGB_DITHER_NORMAL, 0, 0 );
	g_object_unref( im );
	g_object_unref( pix );
	g_object_unref( gc );
}

Displayer *FindDisplayer::getDisplayer( GtkWidget *drawingarea, Frame &frame )
{
	Displayer * display = NULL;
	Preferences &prefs = Preferences::getInstance();
		
	switch ( prefs.displayMode )
	{
	case DISPLAY_XV:
		display = new XvDisplayer( drawingarea, frame.GetWidth(), frame.GetHeight(),
		                           frame.IsPAL(), frame.IsWide() );
		if ( !display->usable() )
		{
			delete display;
			display = NULL;
		}

	case DISPLAY_XX:
		if ( display == NULL )
		{
			display = new XvDisplayer( drawingarea, frame.GetWidth() / 2, frame.GetHeight() / 2,
			                           frame.IsPAL(), frame.IsWide() );
			if ( !display->usable() )
			{
				delete display;
				display = NULL;
			}
		}

	default:
		if ( display == NULL )
		{
			display = new GdkDisplayer( drawingarea, frame.GetWidth(), frame.GetHeight(), frame.IsWide( ) );
		}
	}

	return display;
}

Displayer *FindDisplayer::getDisplayer( GtkWidget *drawingarea, int width, int height )
{
	Displayer * display = NULL;
	Preferences &prefs = Preferences::getInstance();

	switch ( prefs.displayMode )
	{
	case DISPLAY_XV:
		display = new XvDisplayer( drawingarea, width, height, (height > 480), false );
		if ( !display->usable() )
		{
			delete display;
			display = NULL;
		}

	case DISPLAY_XX:
		if ( display == NULL )
		{
			display = new XvDisplayer( drawingarea, width / 2, height / 2, (height > 480), false );
			if ( !display->usable() )
			{
				delete display;
				display = NULL;
			}
		}

	default:
		if ( display == NULL )
		{
			display = new GdkDisplayer( drawingarea, width, height, false );
		}
	}

	return display;
}

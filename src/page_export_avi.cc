/*
* page_export_avi.cc Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "page_export_avi.h"
#include "preferences.h"
#include "filehandler.h"
#include "message.h"
#include "page_editor.h"

/** Constructor for page.

\param _exportPage the export page object to bind to
\param _common	common object to which this page belongs
*/

ExportAVI::ExportAVI( PageExport *_exportPage, KinoCommon *_common ) :
		Export( _exportPage, _common ), writer( NULL )
{
	cerr << "> Creating ExportAVI Page" << endl;

	/* Get pointers to our controls */
	fileEntry = GTK_ENTRY( lookup_widget( common->getWidget(), "entry_export_avi_file" ) );

	formatDV1Toggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "radiobutton_export_avi_type1" ) );
	formatDV2Toggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "radiobutton_export_avi_type2" ) );
	formatRawDVToggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "radiobutton_export_rawdv" ) );
	formatQtToggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "radiobutton_export_quicktime" ) );
#ifdef HAVE_LIBQUICKTIME
	gtk_widget_show( GTK_WIDGET( formatQtToggle ) );
	gtk_widget_set_sensitive( GTK_WIDGET( formatQtToggle ), 1 );
#endif

	autoSplitToggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "checkbutton_export_avi_autosplit" ) );
	timeStampToggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "checkbutton_export_avi_timestamp" ) );
	framesEntry
	= GTK_ENTRY( lookup_widget( exportPage->getWidget(),
	                            "entry_export_avi_frames" ) );
	metadataToggle
	= GTK_TOGGLE_BUTTON( lookup_widget( exportPage->getWidget(),
	                                    "checkbutton_export_avi_metadata" ) );
	sizeEntry
	= GTK_ENTRY( lookup_widget( exportPage->getWidget(),
	                            "entry_export_avi_size" ) );
	OpenDMLToggle
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "checkbutton_export_avi_opendml" ) );
}

/** Destructor for page.
 */

ExportAVI::~ExportAVI()
{
	cerr << "> Destroying ExportAVI Page" << endl;
}

/** Start of page.
 */

void ExportAVI::start()
{
	char s[ 20 ];
	cerr << ">> Starting ExportAVI" << endl;

#ifndef HAVE_LIBQUICKTIME
	gtk_widget_hide( GTK_WIDGET( formatQtToggle ) );
#endif
	switch ( Preferences::getInstance().fileFormat )
	{
	case AVI_DV1_FORMAT:
		gtk_toggle_button_set_active( formatDV1Toggle, TRUE );
		break;
	case AVI_DV2_FORMAT:
		gtk_toggle_button_set_active( formatDV2Toggle, TRUE );
		break;
	case QT_FORMAT:
		gtk_toggle_button_set_active( formatQtToggle, TRUE );
		break;
	default:
		gtk_toggle_button_set_active( formatRawDVToggle, TRUE );
		break;
	}
	gtk_toggle_button_set_active( autoSplitToggle,
	                              Preferences::getInstance().autoSplit );
	gtk_toggle_button_set_active( timeStampToggle,
	                              Preferences::getInstance().timeStamp );
	gtk_entry_set_text( fileEntry, Preferences::getInstance().file );
	sprintf( s, "%d", Preferences::getInstance().frames );
	gtk_entry_set_text( framesEntry, s );
	sprintf( s, "%d", Preferences::getInstance().maxFileSize );
	gtk_entry_set_text( sizeEntry, s );
	gtk_toggle_button_set_active( OpenDMLToggle, Preferences::getInstance().isOpenDML );
}

/** Leaving the page
 */

void ExportAVI::clean()
{
	cerr << ">> Leaving ExportAVI" << endl;
}


/** start exporting an AVI
 */
enum export_result
ExportAVI::doExport( PlayList * playlist, int begin, int end, int every,
                     bool preview )
{
	Frame& frame = *GetFramePool()->GetFrame();
	vector <int> scene = common->getPageEditor() ->GetScene();
	int sceneIndex = 0;
	enum export_result status = EXPORT_RESULT_SUCCESS;

	strcpy( Preferences::getInstance().file, gtk_entry_get_text( fileEntry ) );

	if ( !strcmp( Preferences::getInstance().file, "" ) )
	{
		modal_message( _( "You must enter a stem filename without an extension." ) );
		return EXPORT_RESULT_FAILURE;
	}

	bool updateMetadata = gtk_toggle_button_get_active( metadataToggle );

	if ( gtk_toggle_button_get_active( formatDV1Toggle ) )
		Preferences::getInstance().fileFormat = AVI_DV1_FORMAT;
	if ( gtk_toggle_button_get_active( formatDV2Toggle ) )
		Preferences::getInstance().fileFormat = AVI_DV2_FORMAT;
	if ( gtk_toggle_button_get_active( formatRawDVToggle ) )
		Preferences::getInstance().fileFormat = RAW_FORMAT;
	if ( gtk_toggle_button_get_active( formatQtToggle ) )
		Preferences::getInstance().fileFormat = QT_FORMAT;
	Preferences::getInstance().isOpenDML
	= gtk_toggle_button_get_active( OpenDMLToggle );
	Preferences::getInstance().autoSplit
	= gtk_toggle_button_get_active( autoSplitToggle );
	Preferences::getInstance().timeStamp
	= gtk_toggle_button_get_active( timeStampToggle );
	Preferences::getInstance().frames = atoi( gtk_entry_get_text( framesEntry ) );
	Preferences::getInstance().maxFileSize = atoi( gtk_entry_get_text( sizeEntry ) );
	exportPage->resetProgress();

	switch ( Preferences::getInstance().fileFormat )
	{
	case RAW_FORMAT:
		writer = new RawHandler();
		break;

#ifdef HAVE_LIBQUICKTIME
	case QT_FORMAT:
		writer = new QtHandler();
		break;
#endif

	case AVI_DV1_FORMAT:
	case AVI_DV2_FORMAT:
		AVIHandler *aviWriter = new AVIHandler( Preferences::getInstance().fileFormat );
		writer = aviWriter;
		aviWriter->SetOpenDML( Preferences::getInstance().isOpenDML ||
		                       Preferences::getInstance().maxFileSize == 0 ||
		                       Preferences::getInstance().maxFileSize > 1024 );
		break;
	}
	writer->SetMaxFrameCount( Preferences::getInstance().frames );
	writer->SetAutoSplit( false );
	writer->SetTimeStamp( Preferences::getInstance().timeStamp );
	// writer->SetEveryNthFrame( Preferences::getInstance().every );
	writer->SetEveryNthFrame( 1 );
	writer->SetMaxFileSize( ( off_t ) Preferences::getInstance().maxFileSize * ( off_t ) ( 1024 * 1024 ) );
	writer->SetBaseName( Preferences::getInstance().file );

	try
	{
		time_t datetime = time( NULL );
		int frameNum = 0;
		int i;
		bool resample = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(
		                    lookup_widget( common->getWidget(), "checkbutton_export_avi_resample" ) ) );
		int16_le_t *audio_buffers[ 4 ];
		AudioInfo info;
		AsyncAudioResample<int16_ne_t,int16_le_t>* resampler = 0;
		double adjustedRate = 0;

		for ( i = 0; i < 4; i++ )
			audio_buffers[ i ] = ( int16_le_t * ) calloc( 2 * DV_AUDIO_MAX_SAMPLES, sizeof( int16_t ) );

		/* Iterate over all frames */
		for ( i = begin; i <= end && exportPage->isExporting; i += every, frameNum++ )
		{
			innerLoopUpdate( i, begin, end, every );

			// Check for split
			playlist->GetFrame( i, frame );
			if ( Preferences::getInstance().autoSplit && writer->FileIsOpen()
			        && ( i > 0 ) && ( i == scene[ sceneIndex ] ) )
			{
				sceneIndex++;
				writer->Close();
				frameNum = 0;
				delete resampler;
				resampler = 0;
			}

			/* resample audio */
			if ( resample )
			{
				if ( ! resampler )
				{
					frame.GetAudioInfo( info );

					// Determine correct amount of audio for duration
					adjustedRate = calculateAdjustedRate( playlist, info.frequency, i,
						Preferences::getInstance().autoSplit ? ( scene[ sceneIndex ] - 1 ) : end, every );
					if ( ! adjustedRate )
					{
						status = EXPORT_RESULT_ABORT;
						break;
					}

					// Setup a resampler
					resampler = new AsyncAudioResample<int16_ne_t,int16_le_t>(
						AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, playlist, info.frequency, i,
						Preferences::getInstance().autoSplit ? ( scene[ sceneIndex ] - 1 ) : end, every );
					if ( ! resampler )
					{
						status = EXPORT_RESULT_FAILURE;
						break;
					}
					if ( resampler->IsError() )
					{
						status = EXPORT_RESULT_FAILURE;
						throw string( _("Resampler Error: ") ) + resampler->GetError();
					}
				}
				int requestedSamples = frame.CalculateNumberSamples( info.frequency, frameNum );
				info.samples = resampler->Process( adjustedRate, requestedSamples );
				int16_le_t *p = resampler->GetOutput();
				for ( int s = 0; s < info.samples; s++ )
					for ( int c = 0; c < info.channels; c++ )
						audio_buffers[ c ][ s ] = *p++;
				if ( info.samples )
				{
					frame.EncodeAudio( info, audio_buffers );
					frame.ExtractHeader();
				}
			}

			if ( updateMetadata )
			{
				frame.SetRecordingDate( &datetime, i );
				frame.SetTimeCode( frameNum );
			}
			if ( writer->WriteFrame( frame ) == false )
				throw _( "Failed to write to file. Is file name OK?" );
		}
		writer->Close();
		if ( !exportPage->isExporting )
			status = EXPORT_RESULT_ABORT;

		for ( i = 0; i < 4; i++ )
			free( audio_buffers[ i ] );
		delete resampler;
		resampler = 0;
	}
	catch ( string s )
	{
		cerr << "Could not save AVI file "
		<< Preferences::getInstance().file
		<< ", because an exception has occurred: " << endl;
		cerr << s << endl;
		status = EXPORT_RESULT_FAILURE;
	}
	catch ( const char * exc )
	{
		modal_message( ( char * ) exc );
		status = EXPORT_RESULT_FAILURE;
	}

	delete writer;
	writer = NULL;

	GetFramePool()->DoneWithFrame( &frame );
	
	return status;
}

extern "C"
{
	void
	on_button_export_avi_file_clicked    (GtkButton       *button,
                                            gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to Save As") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_export_avi_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_export_avi_file" ) ), filename );
	}
}

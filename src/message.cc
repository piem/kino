/*
* message.cc -- dialogs and prompts
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <string.h>
#include <stdarg.h>
#include "kino_common.h"
#include "message.h"

extern "C"
{
extern GtkWidget *main_window;

void modal_message ( const char * message, ... )
{
	gchar* msg;
	va_list args;

	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );

	modal_message_with_parent( common->getWidget(), msg, NULL );

	g_free( msg );
}

char *modal_prompt( const char * message, ... )
{
	gchar* msg;
	va_list args;
	char *rv;

	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );

	rv = modal_prompt_with_parent( common->getWidget(), msg, NULL );

	g_free( msg );
	return rv;
}

int modal_confirm( const char * affirm_label, const char *close_label, const char *message, ... )
{
	gchar* msg;
	va_list args;
	int rv = 0;

	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );

	rv = modal_confirm_with_parent( affirm_label, close_label, common->getWidget(), msg, NULL );

	g_free( msg );
	return rv;
}

int modal_confirm_files( const char * affirm_label, const char *close_label, const char* files, const char *message, ... )
{
	gchar* msg;
	va_list args;
	int rv = 0;

	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );

	rv = modal_confirm_files_with_parent( affirm_label, close_label, common->getWidget(), files, msg, NULL );

	g_free( msg );
	return rv;
}

void modal_message_with_parent( GtkWidget *parent, const char * message, ... )
{
	gchar* msg;
	va_list args;

	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );

	GtkWidget * dialog = gtk_message_dialog_new(
								GTK_WINDOW( main_window ),
								GTK_DIALOG_DESTROY_WITH_PARENT,
								GTK_MESSAGE_WARNING,
								GTK_BUTTONS_OK,
								msg );
	gtk_window_set_resizable( GTK_WINDOW( dialog ), FALSE );

	if(parent)
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(KinoCommon::getWidgetWindow(parent)));

	gtk_dialog_run( GTK_DIALOG( dialog ) );
	gtk_widget_destroy( dialog );

	g_free( msg );
}

char *modal_prompt_with_parent( GtkWidget *parent, const char * message, ... )
{
	GtkWidget *dialog, *hbox1, *hbox2, *label, *entry, *icon;
	char *response = NULL;
	gchar* msg;
	va_list args;

	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );

	/* Create the widgets */

	dialog = gtk_dialog_new_with_buttons( _("Question"),
										GTK_WINDOW( main_window ),
										GTK_DIALOG_DESTROY_WITH_PARENT,
										GTK_STOCK_CANCEL,
										GTK_RESPONSE_REJECT,
										GTK_STOCK_OK,
										GTK_RESPONSE_ACCEPT,
										NULL);
	gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_REJECT, -1);
	gtk_dialog_set_default_response( GTK_DIALOG( dialog ), GTK_RESPONSE_ACCEPT );
	gtk_dialog_set_has_separator( GTK_DIALOG( dialog ), FALSE );
	gtk_window_set_resizable( GTK_WINDOW( dialog ), FALSE );
	g_signal_connect( G_OBJECT( dialog ), "response",
							G_CALLBACK( gtk_widget_hide ),
							G_OBJECT( dialog ) );

	/* Add the label, and show everything we've added to the dialog. */
	hbox1 = gtk_hbox_new( FALSE, 12 );
	gtk_container_set_border_width( GTK_CONTAINER( hbox1 ), 6 );
	hbox2 = gtk_hbox_new( FALSE, 12 );
	gtk_container_set_border_width( GTK_CONTAINER( hbox2 ), 6 );
	icon = gtk_image_new_from_stock( GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG );
	label = gtk_label_new( msg );
	entry = gtk_entry_new();
	gtk_entry_set_activates_default( GTK_ENTRY( entry ), TRUE );
	gtk_container_add( GTK_CONTAINER( hbox1 ), icon );
	gtk_container_add( GTK_CONTAINER( hbox1 ), label );
	gtk_container_add( GTK_CONTAINER( hbox2 ), entry );
	gtk_container_add( GTK_CONTAINER( GTK_DIALOG( dialog )->vbox ), hbox1 );
	gtk_container_add( GTK_CONTAINER( GTK_DIALOG( dialog )->vbox ), hbox2 );

	if(parent)
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(KinoCommon::getWidgetWindow(parent)));

	gtk_widget_show_all( dialog );
	
	if ( gtk_dialog_run( GTK_DIALOG( dialog ) ) == GTK_RESPONSE_ACCEPT )
		response = strdup( gtk_entry_get_text( GTK_ENTRY( entry ) ) );
	gtk_widget_destroy( dialog );
	
	g_free( msg );
	return response;
}

int modal_confirm_with_parent( const char *affirm_label, const char *close_label, GtkWidget *parent, const char *message, ... )
{
	int result = GTK_RESPONSE_NONE;
	va_list args;
	GtkWidget* dialog;
	gchar* msg;
	
	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );
	
	dialog = gtk_message_dialog_new(
	                         GTK_WINDOW( main_window ),
	                         GTK_DIALOG_DESTROY_WITH_PARENT,
	                         GTK_MESSAGE_QUESTION,
	                         GTK_BUTTONS_NONE,
	                         msg );
	                         
	if ( close_label != NULL )
		gtk_dialog_add_button( GTK_DIALOG( dialog ), close_label, GTK_RESPONSE_CLOSE );
	gtk_dialog_add_button( GTK_DIALOG( dialog ), GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT );
	gtk_dialog_add_button( GTK_DIALOG( dialog ), affirm_label, GTK_RESPONSE_ACCEPT );
	if ( close_label != NULL )
	    gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_CLOSE, GTK_RESPONSE_REJECT, -1);
	else
	    gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_REJECT, -1);
	gtk_dialog_set_default_response( GTK_DIALOG( dialog ), GTK_RESPONSE_ACCEPT );
	gtk_window_set_title( GTK_WINDOW( dialog ), _("Question") );
	gtk_window_set_resizable( GTK_WINDOW( dialog ), FALSE );

	if(parent)
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(KinoCommon::getWidgetWindow(parent)));

	result = gtk_dialog_run( GTK_DIALOG( dialog ) );
	gtk_widget_destroy( dialog );
	
	g_free( msg );
	return result;
}

int modal_confirm_files_with_parent( const char *affirm_label, const char *close_label, GtkWidget *parent, const char* files, const char *message, ... )
{
	int result = GTK_RESPONSE_NONE;
	va_list args;
	GtkWidget* dialog, *hbox1, *hbox2, *label, *icon;
	gchar* msg;
	
	va_start( args, message );
	msg = g_strdup_vprintf( message, args );
	va_end( args );
	
	dialog = gtk_dialog_new_with_buttons( _("Question"),
	                         GTK_WINDOW( main_window ),
	                         GTK_DIALOG_DESTROY_WITH_PARENT,
	                         NULL );
	                         
	if ( close_label != NULL )
		gtk_dialog_add_button( GTK_DIALOG( dialog ), close_label, GTK_RESPONSE_CLOSE );
	gtk_dialog_add_button( GTK_DIALOG( dialog ), GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT );
	gtk_dialog_add_button( GTK_DIALOG( dialog ), affirm_label, GTK_RESPONSE_ACCEPT );
	if ( close_label != NULL )
	    gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_CLOSE, GTK_RESPONSE_REJECT, -1);
	else
	    gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_REJECT, -1);
	gtk_dialog_set_default_response( GTK_DIALOG( dialog ), GTK_RESPONSE_ACCEPT );
	gtk_dialog_set_has_separator( GTK_DIALOG( dialog ), FALSE );
	gtk_window_set_resizable( GTK_WINDOW( dialog ), FALSE );
	g_signal_connect( G_OBJECT( dialog ), "response",
							G_CALLBACK( gtk_widget_hide ),
							G_OBJECT( dialog ) );

	/* Add the label, and show everything we've added to the dialog. */
	hbox1 = gtk_hbox_new( FALSE, 12 );
	gtk_container_set_border_width( GTK_CONTAINER( hbox1 ), 6 );
	hbox2 = gtk_hbox_new( FALSE, 12 );
	gtk_container_set_border_width( GTK_CONTAINER( hbox2 ), 6 );
	icon = gtk_image_new_from_stock( GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG );
	label = gtk_label_new( msg );

	GtkWidget* sw = gtk_scrolled_window_new( NULL, NULL );
	gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( sw ), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );
	GtkWidget* tv = gtk_text_view_new();
	gtk_text_view_set_editable( GTK_TEXT_VIEW( tv ), FALSE );
	GtkTextBuffer* buffer = gtk_text_view_get_buffer( GTK_TEXT_VIEW( tv ) );
	gtk_text_buffer_set_text( buffer, files, -1 );

	gtk_container_add( GTK_CONTAINER( hbox1 ), icon );
	gtk_container_add( GTK_CONTAINER( hbox1 ), label );
	gtk_container_add( GTK_CONTAINER( sw ), tv );
	gtk_container_add( GTK_CONTAINER( hbox2 ), sw );
	gtk_container_add( GTK_CONTAINER( GTK_DIALOG( dialog )->vbox ), hbox1 );
	gtk_container_add( GTK_CONTAINER( GTK_DIALOG( dialog )->vbox ), hbox2 );

	if(parent)
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(KinoCommon::getWidgetWindow(parent)));

	gtk_widget_show_all( dialog );
	result = gtk_dialog_run( GTK_DIALOG( dialog ) );
	gtk_widget_destroy( dialog );
	
	g_free( msg );
	return result;
}

} // extern "C"

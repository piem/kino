/*
* page_trim.h Notebook Trim Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_TRIM_H
#define _PAGE_TRIM_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "framedisplayer.h"
#include "kino_common.h"
#include "page.h"
#include "preferences.h"
#include "gtkenhancedscale.h"
#include "filehandler.h"

class AVIFile;


typedef enum {
	PAGE_TRIM_MODE_UPDATE,
	PAGE_TRIM_MODE_INSERT,
	PAGE_TRIM_MODE_UNKNOWN = 99
} PageTrimMode;

typedef enum {
	TRIM_INSERT_MODE_BEFORE,
	TRIM_INSERT_MODE_AFTER
} TrimInsertMode;

/** This class controls the scene trimmer notebook page.
*/

class PageTrim : public Page
{
private:
	// Common object
	KinoCommon *common;

	// GUI Widgets
	FrameDisplayer *displayer;
	GtkDrawingArea *frameArea;
	GtkLabel *positionLabelCurrent;
	GtkLabel *positionLabelTotal;
	GtkWidget *trim;
	GtkButton *reset_in;
	GtkButton *reset_out;

	// State retention
	PlayList playlist;
	gint idleCommand;
	gboolean idleCommandActive;
	int start_orig;
	int update_start;
	int end_orig;
	int in_orig;
	int out_orig;
	int in;
	int out;
	int max; // the length of the clip in frames
	int pos;  // the current frame position relative to the clip
	int currentScene; // a frame number valid for the current scene in the (unaltered) playlist
	int lastPos;
	PageTrimMode mode;
	bool changed;

	// Playlist for holding the last copy/cut frames
	PlayList *g_copiedPlayList;

public:
	PageTrim( KinoCommon *common );
	virtual ~PageTrim();
	FrameDisplayer *getFrameDisplayer()
	{
		return this->displayer;
	}
	int getInPoint()
	{
		return this->in;
	}
	int getOutPoint()
	{
		return this->out;
	}
	PlayList &getPlayList()
	{
		return this->playlist;
	}
	int getTotalFrames()
	{
		return this->max + 1;
	}
	int getPosition()
	{
		return this->pos;
	}
	void setInPoint( int value )
	{
		this->in = value;
	}
	void setOutPoint( int value )
	{
		this->out = value;
	}
	void setPosition( int value )
	{
		this->pos = value;
	}

	// Overridden virtuals from Page
	gulong activate();
	void newFile();
	void start();
	void clean();
	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char *cmd );
	void selectScene( int );
	void videoStartOfMovie();
	void videoPreviousScene();
	void videoStartOfScene();
	void videoRewind();
	void videoBack(int step = -1);
	void videoPlay();
	void videoForward(int step = 1);
	void videoFastForward();
	void videoShuttle( int );
	void videoNextScene();
	void videoEndOfScene();
	void videoEndOfMovie();
	void videoPause();
	void videoStop();

	void startNavigator();
	void stopNavigator();

	void movedToFrame( int frame );

	void showFrame( int, gboolean );
	void showFrame( int, Frame& );

	void windowMoved();
	void showFrameInfo( int );
	void resetInPoint();
	void resetOutPoint();
	int getSceneIndex();
	bool loadFile( const string& );
	void insertScene( TrimInsertMode );
	void loadScene( int currentScene );
	void saveScene();
	void setMode( PageTrimMode );
	void timeFormatChanged();
	std::string getHelpPage()
	{
		return "trim";
	}
};

#endif

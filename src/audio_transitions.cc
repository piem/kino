/*
* audio_transitions.cc -- audio transitions
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include <glade/glade.h>
#include "audio_transitions.h"
#include "page_magick.h"

extern "C"
{
#include "support.h"
	extern GladeXML* magick_glade;
}

/** Audio transition switch - determines the point at which the b frames audio is used during
	an frame transition.
*/
static void
gtk_curve_reset_vector ( GtkCurve *curve )
{
	if ( curve->ctlpoint )
		g_free ( curve->ctlpoint );

	curve->num_ctlpoints = 2;
	curve->ctlpoint = ( gfloat( * ) [ 2 ] ) g_malloc ( 2 * sizeof ( curve->ctlpoint[ 0 ] ) );
	curve->ctlpoint[ 0 ][ 0 ] = curve->min_x;
	curve->ctlpoint[ 0 ][ 1 ] = curve->max_y;
	curve->ctlpoint[ 1 ][ 0 ] = curve->max_x;
	curve->ctlpoint[ 1 ][ 1 ] = curve->min_y;

}

class AudioNone : public GDKAudioTransition
{
public:
	char *GetDescription( ) const
	{
		return _( "No Change" );
	}

	void GetFrame( int16_t **aframe, int16_t **bframe, int frequency, int channels, int& samples, double position, double frame_delta )
	{}
};

#define MAX_VECTOR 1000

class AudioSwitch : public GDKAudioTransition
{
private:
	GtkWidget *window;
	gfloat vectorA[ MAX_VECTOR ];
	gfloat vectorB[ MAX_VECTOR ];


public:
	AudioSwitch( )
	{
		window = glade_xml_get_widget( magick_glade, "window_switch" );
	}

	virtual ~AudioSwitch( )
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Cross Fade" );
	}

	void GetFrame( int16_t **aframe, int16_t **bframe, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		GtkCurve * curveA = GTK_CURVE( lookup_widget( window, "curve1" ) );
		GtkCurve *curveB = GTK_CURVE( lookup_widget( window, "curve2" ) );
		gtk_curve_get_vector( curveA, MAX_VECTOR, vectorA );
		gtk_curve_get_vector( curveB, MAX_VECTOR, vectorB );
		
		int pos = ( int ) ( position * MAX_VECTOR );
		double factorA = vectorA[ pos ];
		double factorB = vectorB[ pos ];
		if ( factorA + factorB > 1.0 )
		{
			factorA /= factorA + factorB;
			factorB /= factorA + factorB;
		}
		for ( int s = 0; s < samples; s ++ )
			for ( int c = 0; c < channels; c ++ )
				aframe[ c ][ s ] = ( int16_t ) ( ( double ) aframe[ c ][ s ] * factorA + ( double ) bframe[ c ][ s ] * factorB );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
		GtkCurve *curveA = GTK_CURVE( lookup_widget( window, "curve1" ) );
		GtkCurve *curveB = GTK_CURVE( lookup_widget( window, "curve2" ) );
		gtk_curve_reset_vector( curveA );
		gtk_curve_set_curve_type( curveA, GTK_CURVE_TYPE_SPLINE );
		gtk_curve_set_curve_type( curveB, GTK_CURVE_TYPE_SPLINE );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};

/** Callback for selection change.
*/

static void
on_optionmenu_selected ( GtkMenuItem *menu_item, gpointer user_data )
{
	( ( GDKAudioTransitionRepository * ) user_data ) ->SelectionChange();
}

/** Constructor for the audio transition repository.
 
  	Registers an instance of each known transition for later GUI exposure via the Initialise method.
*/

GDKAudioTransitionRepository::GDKAudioTransitionRepository() : selected_transition( NULL ), menu( NULL ), container( NULL )
{
	// Register an instance of each object (adapting raw transitions to GDK transitions where necessary)
	Register( new AudioSwitch() );
	Register( new AudioNone() );
}

/** Destructor for the image repository - clears all the registered transitions
*/

GDKAudioTransitionRepository::~GDKAudioTransitionRepository()
{
	// Remove the transitions in the repository
	for ( unsigned int index = 0; index < transitions.size(); index ++ )
		delete transitions[ index ];
}

/** Register an audio transition.
*/

void GDKAudioTransitionRepository::Register( GDKAudioTransition *transition )
{
	std::cerr << ">>> Audio Transition: " << transition->GetDescription() << std::endl;
	transitions.push_back( transition );
}

/** Initialise the option menu with the current list of registered transitions.
*/

void GDKAudioTransitionRepository::Initialise( GtkOptionMenu *menu, GtkBin *container )
{
	// Store these for future reference
	this->menu = menu;
	this->container = container;

	// Add the transitions to the menu
	GtkMenu *menu_new = GTK_MENU( gtk_menu_new( ) );
	for ( unsigned int index = 0; index < transitions.size(); index ++ )
	{
		GtkWidget *item = gtk_menu_item_new_with_label( transitions[ index ] ->GetDescription( ) );
		gtk_widget_show( item );
		gtk_menu_append( menu_new, item );
		g_signal_connect( G_OBJECT( item ), "activate", G_CALLBACK( on_optionmenu_selected ), this );
	}
	gtk_menu_set_active( menu_new, 0 );
	gtk_option_menu_set_menu( menu, GTK_WIDGET( menu_new ) );

	// Register the selected items widgets
	SelectionChange();
}

/** Get the currently selected audio transition.
*/

GDKAudioTransition *GDKAudioTransitionRepository::Get( ) const
{
	GtkMenu * transitionMenu = GTK_MENU( gtk_option_menu_get_menu( menu ) );
	GtkWidget *active_item = gtk_menu_get_active( transitionMenu );
	return transitions[ g_list_index( GTK_MENU_SHELL( transitionMenu ) ->children, active_item ) ];
}

/** Handle attach/detach widgets on last selected/selected items.
*/

void GDKAudioTransitionRepository::SelectionChange( )
{
	bool isPreviewing = false;
	PageMagick* magick = 0;

	if ( common && common->getPageMagick( ) )
		magick = common->getPageMagick( );
	if ( magick && magick->IsPreviewing() )
	{
		isPreviewing = true;
		magick->StopPreview();
	}

	// Detach the selected transitions widgets
	if ( selected_transition != NULL )
		selected_transition->DetachWidgets( container );

	// Get the new selection
	selected_transition = Get();

	// Attach the new transitions widgets
	if ( selected_transition != NULL )
		selected_transition->AttachWidgets( container );

	if ( magick )
	{
		if ( isPreviewing )
			magick->StartPreview();
		else
			magick->PreviewFrame();
	}
}

/*
* image_create.cc -- RGB24 image create
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include <glade/glade.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "image_create.h"
#include "page_magick.h"
#include "message.h"
#include "kino_extra.h"

#define GAP_MAX (9999)

extern "C"
{
#include "support.h"
	extern GladeXML* magick_glade;
	extern void on_time_range_changed(GtkWidget *widget, gpointer user_data);
}

/*
 * Note that for speed of development I elected to make everything GUI here, but
 * this will limit its use a bit. For every creator, there should be a GUI component
 * which extends a non-GUI component. The intention is that we should be able
 * to propogate state via the playlist and some unspecified sml/smil aware class
 * (ummm... I think that's the way I want it anyway... :-/).
 */

/** Create a number of images from the specified colour.
*/

class ImageCreateColour : public GDKAudioImport
{
protected:
	GdkColor start;
	int frames;
	GtkWidget *picker;
	GtkWidget *frame_counter;

public:
	ImageCreateColour() : frame_counter( 0 )
	{
		start.red = start.green = start.blue = 0;
		frames = 25;
	}

	char *GetDescription( ) const
	{
		return _( "Fixed Colour" );
	}

	void CreateFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		if ( position == 0 )
		{
			uint8_t * p = pixels;
			while ( p < ( pixels + width * height * 3 ) )
			{
				*p ++ = start.red >> 8;
				*p ++ = start.green >> 8;
				*p ++ = start.blue >> 8;
			}
		}
	}

	int GetNumberOfFrames( )
	{
		return frames;
	}

	void AddFrameCollector( GtkWidget *table, int row )
	{
		GtkWidget * label = gtk_label_new( _( "Frames:" ) );
		gtk_widget_show( label );

		GtkObject *frame_counter_adj = gtk_adjustment_new( frames, 0, 999999, 1, 10, 0 );
		frame_counter = gtk_spin_button_new( GTK_ADJUSTMENT( frame_counter_adj ), 1, 0 );
		gtk_spin_button_set_value( GTK_SPIN_BUTTON( frame_counter ), frames );
		gtk_widget_show( frame_counter );
		gtk_widget_set_usize( frame_counter, 60, -2 );
		g_signal_connect( G_OBJECT( frame_counter ), "value-changed", G_CALLBACK( on_time_range_changed ), NULL );

		gtk_table_attach( GTK_TABLE( table ), label, 0, 1, row, row + 1, ( GtkAttachOptions ) ( GTK_FILL ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
		gtk_table_attach( GTK_TABLE( table ), frame_counter, 1, 2, row, row + 1, ( GtkAttachOptions ) ( 0 ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
	}

	void InterpretFrameCollector( )
	{
		if ( frame_counter )
			frames = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( frame_counter ) );
	}

	void AttachWidgets( GtkBin *bin )
	{
		GtkWidget * table = gtk_table_new ( 2, 2, FALSE );
		gtk_table_set_row_spacings( GTK_TABLE( table ), 6 );
		gtk_table_set_col_spacings( GTK_TABLE( table ), 6 );

		GtkWidget *label = gtk_label_new( _( "Colour:" ) );
		gtk_widget_show( label );

		picker = gtk_color_button_new();
		gtk_color_button_set_color( GTK_COLOR_BUTTON( picker ), &start );
		gtk_widget_show( picker );
		g_signal_connect( G_OBJECT( picker ), "color-set", G_CALLBACK( Repaint ), 0 );

		AddFrameCollector( table, 0 );

		gtk_table_attach( GTK_TABLE( table ), label, 0, 1, 1, 2, ( GtkAttachOptions ) ( 0 ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
		gtk_table_attach( GTK_TABLE( table ), picker, 1, 2, 1, 2, ( GtkAttachOptions ) ( 0 ), ( GtkAttachOptions ) ( 0 ), 0, 0 );

		gtk_widget_show( table );
		gtk_container_add( GTK_CONTAINER( bin ), table );
	}

	void DetachWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			InterpretFrameCollector( );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( picker ), &start );
			gtk_container_remove( GTK_CONTAINER( bin ), bin->child );
		}
	}

	void InterpretWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			InterpretFrameCollector( );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( picker ), &start );
		}
	}

	void CreateAudio( int16_t **buffer, short int *channels, int *frequency, int *samples )
	{}
}
;

/** Create noise.
*/

class ImageCreateNoise : public ImageCreateColour
{
public:
	char *GetDescription( ) const
	{
		return _( "Random noise" );
	}

	void CreateFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		uint8_t value;
		uint8_t *p = pixels;
		while ( p < ( pixels + width * height * 3 ) )
		{
			value = ( uint8_t ) ( 255.0 * rand() / ( RAND_MAX + 1.0 ) );
			*p ++ = value;
			*p ++ = value;
			*p ++ = value;
		}
	}

	void AttachWidgets( GtkBin *bin )
	{
		GtkWidget * table = gtk_table_new ( 2, 2, FALSE );
		gtk_table_set_row_spacings( GTK_TABLE( table ), 6 );
		gtk_table_set_col_spacings( GTK_TABLE( table ), 6 );
		AddFrameCollector( table, 0 );
		gtk_widget_show( table );
		gtk_container_add( GTK_CONTAINER( bin ), table );
	}

	void DetachWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			InterpretFrameCollector( );
			gtk_container_remove( GTK_CONTAINER( bin ), bin->child );
		}
	}

	void InterpretWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
			InterpretFrameCollector( );
	}
};

/** Create a number of images that move from one colour to the next.
*/

class ImageCreateColourRange : public ImageCreateColour
{
protected:
	GdkColor end;
	GtkWidget *start_picker;
	GtkWidget *end_picker;

public:
	ImageCreateColourRange()
	{
		end.red = end.green = end.blue = 0;
	}

	char *GetDescription( ) const
	{
		return _( "Colour Range" );
	}

	void CreateFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		uint8_t r = start.red + ( guint16( ( end.red - start.red ) * position ) >> 8 );
		uint8_t g = start.green + ( guint16( ( end.green - start.green ) * position ) >> 8 );
		uint8_t b = start.blue + ( guint16( ( end.blue - start.blue ) * position ) >> 8 );
		uint8_t *p = pixels;
		while ( p < ( pixels + width * height * 3 ) )
		{
			*p ++ = r;
			*p ++ = g;
			*p ++ = b;
		}
	}

	void AttachWidgets( GtkBin *bin )
	{
		GtkWidget * table = gtk_table_new( 2, 4, FALSE );
		gtk_table_set_row_spacings( GTK_TABLE( table ), 6 );
		gtk_table_set_col_spacings( GTK_TABLE( table ), 6 );

		GtkWidget *start_label = gtk_label_new( _( "From:" ) );
		gtk_widget_show( start_label );

		start_picker = gtk_color_button_new();
		gtk_color_button_set_color( GTK_COLOR_BUTTON( start_picker ), &start );
		gtk_widget_show( start_picker );
		g_signal_connect( G_OBJECT( start_picker ), "color-set", G_CALLBACK( Repaint ), 0 );

		GtkWidget *end_label = gtk_label_new( _( "To:" ) );
		gtk_widget_show( end_label );

		end_picker = gtk_color_button_new();
		gtk_color_button_set_color( GTK_COLOR_BUTTON( end_picker ), &end );
		gtk_widget_show( end_picker );
		g_signal_connect( G_OBJECT( end_picker ), "color-set", G_CALLBACK( Repaint ), 0 );

		AddFrameCollector( table, 0 );

		gtk_table_attach( GTK_TABLE( table ), start_label, 0, 1, 1, 2, ( GtkAttachOptions ) ( GTK_FILL ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
		gtk_table_attach( GTK_TABLE( table ), start_picker, 1, 2, 1, 2, ( GtkAttachOptions ) ( 0 ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
		gtk_table_attach( GTK_TABLE( table ), end_label, 2, 3, 1, 2, ( GtkAttachOptions ) ( GTK_FILL ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
		gtk_table_attach( GTK_TABLE( table ), end_picker, 3, 4, 1, 2, ( GtkAttachOptions ) ( 0 ), ( GtkAttachOptions ) ( 0 ), 0, 0 );

		gtk_widget_show( table );
		gtk_container_add( GTK_CONTAINER( bin ), table );
	}

	void DetachWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			InterpretFrameCollector( );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( start_picker ), &start );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( end_picker ), &end );
			gtk_container_remove( GTK_CONTAINER( bin ), bin->child );
		}
	}

	void InterpretWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			InterpretFrameCollector( );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( start_picker ), &start );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( end_picker ), &end );
		}
	}
};

/** Create a number of images that from gradiated colours.
*/

class ImageCreateGradiate : public GDKImageCreate
{
private:
	GtkWidget *window;
	GdkColor start_left, start_right, end_left, end_right, start, end;
	int type;
	int frames;

public:
	ImageCreateGradiate()
	{
		window = glade_xml_get_widget( magick_glade, "image_create_gradiate" );
		start_right.red = start_right.green = start_right.blue = 0;
		GtkWidget* widget = lookup_widget( window, "colorpicker_start_right" );
		gtk_color_button_set_color( GTK_COLOR_BUTTON( widget ), &start_right );
		g_signal_connect( G_OBJECT( widget ), "color-set", G_CALLBACK( Repaint ), 0 );

		end_right.red = end_right.green = end_right.blue = 0;
		widget = lookup_widget( window, "colorpicker_end_right" );
		gtk_color_button_set_color( GTK_COLOR_BUTTON( widget ), &end_right );
		g_signal_connect( G_OBJECT( widget ), "color-set", G_CALLBACK( Repaint ), 0 );

		widget = lookup_widget( window, "optionmenu_gradiate" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
		widget = lookup_widget( window, "colorpicker_start_left" );
		g_signal_connect( G_OBJECT( widget ), "color-set", G_CALLBACK( Repaint ), 0 );
		widget = lookup_widget( window, "colorpicker_end_left" );
		g_signal_connect( G_OBJECT( widget ), "color-set", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageCreateGradiate()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Gradient" );
	}

	void LeftMiddle( uint8_t *pixels, int width, int height, double position )
	{
		double rr = ( double ) ( end.red - start.red ) / ( double ) ( width ) * 2;
		double rg = ( double ) ( end.green - start.green ) / ( double ) ( width ) * 2;
		double rb = ( double ) ( end.blue - start.blue ) / ( double ) ( width ) * 2;
		uint8_t *p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			for ( int x = 0; x < width; x ++ )
			{
				if ( x < width / 2 )
				{
					*p ++ = guint16( start.red + rr * x ) >> 8;
					*p ++ = guint16( start.green + rg * x ) >> 8;
					*p ++ = guint16( start.blue + rb * x ) >> 8;
				}
				else
				{
					int o = width / 2 - ( x - width / 2 );
					*p ++ = guint16( start.red + rr * o ) >> 8;
					*p ++ = guint16( start.green + rg * o ) >> 8;
					*p ++ = guint16( start.blue + rb * o ) >> 8;
				}
			}
		}
	}

	void LeftRight( uint8_t *pixels, int width, int height, double position )
	{
		double rr = ( double ) ( end.red - start.red ) / ( double ) ( width );
		double rg = ( double ) ( end.green - start.green ) / ( double ) ( width );
		double rb = ( double ) ( end.blue - start.blue ) / ( double ) ( width );
		uint8_t *p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			for ( int x = 0; x < width; x ++ )
			{
				*p ++ = guint16( start.red + rr * x ) >> 8;
				*p ++ = guint16( start.green + rg * x ) >> 8;
				*p ++ = guint16( start.blue + rb * x ) >> 8;
			}
		}
	}

	void TopBottom( uint8_t *pixels, int width, int height, double position )
	{
		double rr = ( double ) ( end.red - start.red ) / ( double ) ( height );
		double rg = ( double ) ( end.green - start.green ) / ( double ) ( height );
		double rb = ( double ) ( end.blue - start.blue ) / ( double ) ( height );
		uint8_t *p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			for ( int x = 0; x < width; x ++ )
			{
				*p ++ = guint16( start.red + rr * y ) >> 8;
				*p ++ = guint16( start.green + rg * y ) >> 8;
				*p ++ = guint16( start.blue + rb * y ) >> 8;
			}
		}
	}

	void TopMiddle( uint8_t *pixels, int width, int height, double position )
	{
		double rr = ( double ) ( end.red - start.red ) / ( double ) ( height ) * 2;
		double rg = ( double ) ( end.green - start.green ) / ( double ) ( height ) * 2;
		double rb = ( double ) ( end.blue - start.blue ) / ( double ) ( height ) * 2;
		uint8_t *p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			for ( int x = 0; x < width; x ++ )
			{
				if ( y < height / 2 )
				{
					*p ++ = guint16( start.red + rr * y ) >> 8;
					*p ++ = guint16( start.green + rg * y ) >> 8;
					*p ++ = guint16( start.blue + rb * y ) >> 8;
				}
				else
				{
					int o = height / 2 - ( y - height / 2 );
					*p ++ = guint16( start.red + rr * o ) >> 8;
					*p ++ = guint16( start.green + rg * o ) >> 8;
					*p ++ = guint16( start.blue + rb * o ) >> 8;
				}
			}
		}
	}

	void CreateFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		start.red = start_left.red + guint16( ( end_left.red - start_left.red ) * position );
		start.green = start_left.green + guint16( ( end_left.green - start_left.green ) * position );
		start.blue = start_left.blue + guint16( ( end_left.blue - start_left.blue ) * position );

		end.red = start_right.red + guint16( ( end_right.red - start_right.red ) * position );
		end.green = start_right.green + guint16( ( end_right.green - start_right.green ) * position );
		end.blue = start_right.blue + guint16( ( end_right.blue - start_right.blue ) * position );

		if ( type == 0 )
			LeftMiddle( pixels, width, height, position );
		else if ( type == 1 )
			LeftRight( pixels, width, height, position );
		else if ( type == 2 )
			TopBottom( pixels, width, height, position );
		else if ( type == 3 )
			TopMiddle( pixels, width, height, position );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}

	void InterpretWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_gradiate" ) ) ) );
			GtkWidget *active_item = gtk_menu_get_active( menu );
			type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
	
			GtkEntry *entry = GTK_ENTRY( lookup_widget( window, "spinbutton_frames" ) );
			frames = atoi( gtk_entry_get_text( entry ) );
	
			gtk_color_button_get_color( GTK_COLOR_BUTTON( lookup_widget( window, "colorpicker_start_left" ) ),
									&start_left );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( lookup_widget( window, "colorpicker_start_right" ) ),
									&start_right );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( lookup_widget( window, "colorpicker_end_left" ) ),
									&end_left );
			gtk_color_button_get_color( GTK_COLOR_BUTTON( lookup_widget( window, "colorpicker_end_right" ) ),
									&end_right );
		}
	}

	int GetNumberOfFrames( )
	{
		return frames;
	}
};

/** Create a number of images that are generated from the input file.
*/

class ImageCreateFromFile : public ImageCreateColourRange
{
protected:
	static char file[ PATH_MAX + NAME_MAX ];
	GtkWidget *file_entry;
	int count;
	GdkPixbuf* still;
	bool hasFilenameChanged;
	int count_offset;

public:
	ImageCreateFromFile( )
		: file_entry( 0 )
		, still( 0 ) 
		, hasFilenameChanged( false )
		, count_offset( 0 )
	{ }

	char *GetDescription( ) const
	{
		return _( "From File" );
	}

	void CreateFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		if ( strcmp( file, "" ) == 0 )
			return;
		
		GError* gerror = NULL;
		SelectedFrames& fx = GetSelectedFramesForFX();

		// Determine if this is an image sequence
		if ( strchr( file, '%' ) != NULL )
		{
			char full[ PATH_MAX + NAME_MAX ];
			int gap = 0;
		
			if ( fx.IsRepainting() || fx.IsPreviewing() )
			{
				// compute the relative count
				count = int( position * GetNumberOfFrames() + 0.5 );
				
				// determine offset to first file
				if ( hasFilenameChanged )
				for ( count_offset = 0; count_offset < GAP_MAX; ++count_offset )
				{
					struct stat buf;
					snprintf( full, PATH_MAX + NAME_MAX, file, count + count_offset );
					if ( stat( full, &buf ) == 0 )
						break;
				}
				count += count_offset;
			}
			// Permit gaps in the file number sequence
			while ( gap < GAP_MAX )
			{
				struct stat buf;
				snprintf( full, PATH_MAX + NAME_MAX, file, count++ );
				if ( stat( full, &buf ) == 0 )
					break;
				else
					gap ++;
			}
			// If successfully obtained a new file
			if ( gap < GAP_MAX )
			{
				GdkPixbuf *image = gdk_pixbuf_new_from_file( full, &gerror );
				if ( still )
					g_object_unref( still );
				still = gdk_pixbuf_scale_simple( image, width, height, GDK_INTERP_HYPER );
				g_object_unref( image );
			}
		}
		// Otherwise, import single image just once
		else if ( hasFilenameChanged )
		{
			GdkPixbuf *image = gdk_pixbuf_new_from_file( file, &gerror );
			if ( still )
				g_object_unref( still );
			if ( image )
			{
				still = gdk_pixbuf_scale_simple( image, width, height, GDK_INTERP_HYPER );
				g_object_unref( image );
			}
		}
		// If gdk-pixbuf loader generated an error
		if ( gerror != NULL )
		{
			if ( hasFilenameChanged )
			{
				if ( fx.IsPreviewing() )
					gdk_threads_enter();
				modal_message( gerror->message );
				if ( fx.IsPreviewing() )
					gdk_threads_leave();
			}
			g_error_free( gerror );
			if ( still )
				g_object_unref( still );
			still = 0;
		}
		// Otherwise, if there is a pixbuf
		else if ( still )
		{
			if ( gdk_pixbuf_get_has_alpha( still ) ) {
			// This is a hack! The correct calls would be:
			// gdk_pixbuf_new/gdk_pixbuf_copy_area
				uint8_t *jp = gdk_pixbuf_get_pixels( still ), *jz = pixels;
				for ( int i = 0; i < width * height; i++ ) {
					memcpy( jz, jp, 3 );
					jp += 4; jz += 3;
				}
			}
			else
				memcpy( pixels, gdk_pixbuf_get_pixels( still ), width * height * 3 );
		}
		hasFilenameChanged = false;
	}

	static void
	on_button_file_clicked( GtkButton *button, gpointer user_data )
	{
		char *filename = common->getFileToOpen( _("Select an Image"), false );
		if ( filename && strcmp( filename, "" ) )
		{
			ImageCreateFromFile *me = static_cast< ImageCreateFromFile* >( user_data );
			gtk_entry_set_text( me->getEntry(), filename );
		}
	}
	
	void AttachWidgets( GtkBin *bin )
	{
		GtkWidget * table = gtk_table_new ( 2, 2, FALSE );
		gtk_table_set_row_spacings( GTK_TABLE( table ), 6 );
		gtk_table_set_col_spacings( GTK_TABLE( table ), 6 );

		GtkWidget *label = gtk_label_new( _( "File:" ) );
		gtk_widget_show( label );

		GtkWidget *hbox = gtk_hbox_new( FALSE, 6 );
		file_entry = gtk_entry_new();
		gtk_entry_set_text( GTK_ENTRY( file_entry ), file );
		g_signal_connect( G_OBJECT( file_entry ), "activate", G_CALLBACK( Repaint ), 0 );
		gtk_box_pack_start( GTK_BOX( hbox ), file_entry, TRUE, TRUE, 0 );
		GtkWidget *button = gtk_button_new();
		GtkWidget *image = gtk_image_new_from_stock( GTK_STOCK_OPEN, GTK_ICON_SIZE_BUTTON );
		gtk_container_add( GTK_CONTAINER( button ), image );
		gtk_button_set_relief( GTK_BUTTON( button ), GTK_RELIEF_NONE );
		g_signal_connect( G_OBJECT( button ), "clicked", G_CALLBACK( on_button_file_clicked ), this );

		gtk_box_pack_start( GTK_BOX( hbox ), button, FALSE, TRUE, 0 );
		gtk_widget_show( hbox );
		gtk_widget_show( file_entry );
		gtk_widget_show( button );
		gtk_widget_show( image );

		AddFrameCollector( table, 0 );
		gtk_table_attach( GTK_TABLE( table ), label, 0, 1, 1, 2, ( GtkAttachOptions ) ( 0 ), ( GtkAttachOptions ) ( 0 ), 0, 0 );
		gtk_table_attach( GTK_TABLE( table ), hbox, 1, 2, 1, 2, ( GtkAttachOptions ) ( GTK_EXPAND | GTK_FILL | GTK_SHRINK ), ( GtkAttachOptions ) ( 0 ), 0, 0 );

		GtkWidget *tip_label = gtk_label_new( _( "TIP: use a format specifier in the file name\nto import from an image sequence.\nFor example: gap-%06d.png." ) );
		gtk_label_set_line_wrap( GTK_LABEL( tip_label ), FALSE );
		gtk_widget_show( tip_label );
		gtk_table_attach( GTK_TABLE( table ), tip_label, 0, 2, 2, 3, ( GtkAttachOptions ) ( GTK_EXPAND | GTK_FILL | GTK_SHRINK ), ( GtkAttachOptions ) ( 0 ), 0, 0 );

		gtk_widget_show( table );
		gtk_container_add( GTK_CONTAINER( bin ), table );
	}

	void DetachWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			// Make sure we save the current settings (this should be a method, rather than a duplicate
			// of InterpretWidgets ... since Interpret may throw exceptions, it can't be called directly
			// here...)
			InterpretFrameCollector( );
			strcpy( file, gtk_entry_get_text( GTK_ENTRY( file_entry ) ) );
			gtk_container_remove( GTK_CONTAINER( bin ), bin->child );
		}
	}

	void InterpretWidgets( GtkBin *bin )
	{
		if ( bin->child != NULL )
		{
			InterpretFrameCollector( );
			if ( file_entry )
			{
				int n = PATH_MAX + NAME_MAX - 1;
				char newFileName[ n + 1 ];
				newFileName[ n ] = '\0';
				strncpy( newFileName, gtk_entry_get_text( GTK_ENTRY( file_entry ) ), n );
				SelectedFrames& fx = GetSelectedFramesForFX();
				if ( strcmp( newFileName, file ) || !( fx.IsRepainting() || fx.IsPreviewing() ) )
					hasFilenameChanged = true;
				strcpy( file, newFileName );
				count = 0;
			}
		}
	}

	GtkEntry *getEntry() const
	{
		if ( file_entry )
			return GTK_ENTRY( file_entry );
		else
			return NULL;
	}
};

char ImageCreateFromFile::file[ PATH_MAX + NAME_MAX ] = "";

/** Callback for selection change.
*/

static void
on_optionmenu_selected ( GtkMenuItem *menu_item, gpointer user_data )
{
	( ( GDKImageCreateRepository * ) user_data ) ->SelectionChange();
}

/** Constructor for the image creator repository.
 
  	Registers an instance of each known creator for later GUI exposure via the Initialise method.
*/

GDKImageCreateRepository::GDKImageCreateRepository() : selected_creator( NULL ), menu( NULL ), container( NULL )
{
	// Register an instance of each object
	Register( new ImageCreateColourRange() );
	Register( new ImageCreateColour() );
	Register( new ImageCreateFromFile() );
	Register( new ImageCreateGradiate() );
	Register( new ImageCreateNoise() );
}

/** Destructor for the image repository - clears all the registered creators
*/

GDKImageCreateRepository::~GDKImageCreateRepository()
{
	// Remove the creators in the repository
	for ( unsigned int index = 0; index < creators.size(); index ++ )
		delete creators[ index ];
}

/** Register an image creator creator.
*/

void GDKImageCreateRepository::Register( GDKImageCreate *creator )
{
	std::cerr <<  ">>> Image Create: " << creator->GetDescription( ) << std::endl;
	creators.push_back( creator );
}

/** Initialise the option menu with the current list of registered creators.
*/

void GDKImageCreateRepository::Initialise( GtkOptionMenu *menu, GtkBin *container )
{
	// Store these for future reference
	this->menu = menu;
	this->container = container;

	// Add the creators to the menu
	GtkMenu *menu_new = GTK_MENU( gtk_menu_new( ) );
	for ( unsigned int index = 0; index < creators.size(); index ++ )
	{
		GtkWidget *item = gtk_menu_item_new_with_label( creators[ index ] ->GetDescription( ) );
		gtk_widget_show( item );
		gtk_menu_append( menu_new, item );
		g_signal_connect( G_OBJECT( item ), "activate", G_CALLBACK( on_optionmenu_selected ), this );
	}
	gtk_menu_set_active( menu_new, 0 );
	gtk_option_menu_set_menu( menu, GTK_WIDGET( menu_new ) );

	// Register the selected items widgets
	SelectionChange();
}

/** Get the currently selected image creator creator.
*/

GDKImageCreate *GDKImageCreateRepository::Get( ) const
{
	GtkMenu * creatorMenu = GTK_MENU( gtk_option_menu_get_menu( menu ) );
	GtkWidget *active_item = gtk_menu_get_active( creatorMenu );
	return creators[ g_list_index( GTK_MENU_SHELL( creatorMenu ) ->children, active_item ) ];
}

/** Handle attach/detach widgets on last selected/selected items.
*/

void GDKImageCreateRepository::SelectionChange( )
{
	// Detach the selected creators widgets
	if ( selected_creator != NULL )
		selected_creator->DetachWidgets( container );

	// Get the new selection
	selected_creator = Get();

	// Attach the new creators widgets
	if ( selected_creator != NULL )
		selected_creator->AttachWidgets( container );

	// Inform the main page of the change
	if ( common != NULL && common->getPageMagick( ) != NULL )
		common->getPageMagick( ) ->RefreshStatus( );
}

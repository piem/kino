/*
* page_magick.h -- FX Page
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_MAGICK_H
#define _PAGE_MAGICK_H

#include "image_create.h"
#include "image_filters.h"
#include "image_transitions.h"
#include "audio_filters.h"
#include "audio_transitions.h"
#include "kino_common.h"
#include "page.h"
#include "oss.h"
#include "kino_extra.h"

/** Simple class wrapping for the shared object functions.
*/

class Plugin
{
private:
	void *ptr;

public:
	bool Open( char *file );
	void Close( );
	void *Find( const char *symbol );
	const char *GetError( );
};

/** Holds the complete collection of shared objects.
*/

class PluginCollection
{
private:
	vector < Plugin * > collection;

public:
	PluginCollection( );
	~PluginCollection( );
	void Initialise( const char *directory );
	void RegisterPlugin( char *filename );
	unsigned int Count();
	Plugin *Get( unsigned int index );
};

/** Image Create Repository with load from plugin functionality.
*/

class PluginImageCreateRepository : public GDKImageCreateRepository
{
public:
	void InstallPlugins( Plugin * );
};

/** Image Filter Repository with load from plugin functionality.
*/

class PluginImageFilterRepository : public GDKImageFilterRepository
{
public:
	void InstallPlugins( Plugin * );
};

/** Image Transition Repository with load from plugin functionality.
*/

class PluginImageTransitionRepository : public GDKImageTransitionRepository
{
public:
	void InstallPlugins( Plugin * );
};

/** Audio Filter Repository with load from plugin functionality.
*/

class PluginAudioFilterRepository : public GDKAudioFilterRepository
{
public:
	void InstallPlugins( Plugin * );
};

/** Audio Transition Repository with load from plugin functionality.
*/

class PluginAudioTransitionRepository : public GDKAudioTransitionRepository
{
public:
	void InstallPlugins( Plugin * );
};

/** Magick page class
*/

class PageMagick : public Page, public KeyFrameController
{
public:
	PageMagick( KinoCommon *common );
	virtual ~PageMagick();

	// Page interface implementation
	void newFile();
	void start();
	gulong activate();
	void clean();
	void selectScene( int );
	void videoStartOfMovie();
	void videoBack(int step = -1);
	void videoPlay();
	void videoForward(int step = 1);
	void videoEndOfMovie();
	void videoPause();
	void videoStop();
	void videoPreviousScene();
	void videoNextScene();
	void videoStartOfScene()
		{ videoPreviousScene(); }
	void videoEndOfScene()
		{ videoNextScene(); }
	void showFrameInfo( int frame_number );
	void showFrameInfo( int frame_number, int duration );
	void movedToFrame( int frame_number );
	void windowMoved();
	void timeFormatChanged();
	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char *cmd );
	std::string getHelpPage()
	{
		return "FX";
	}

	// PageMagick methods
	void PreviewFrame();
	void StartPreview();
	void AudioThread();
	void VideoThread();
	void StartRender();
	void Stop();
	void StopPreview();
	void UpdateProgress( gfloat );
	void UpdateStatus( int currentFrame, int begin, int end, int every );
	void RefreshStatus( bool with_fx_notify = false );

	// PageMagick accessors
	GDKImageCreate *GetImageCreate() const;
	GDKImageFilter *GetImageFilter() const;
	GDKImageTransition *GetImageTransition() const;
	GDKAudioFilter *GetAudioFilter() const;
	GDKAudioTransition *GetAudioTransition() const;
	GtkWidget* GetWindow() const
	{
		return window;
	}
	bool IsRepainting() const
	{
		return repainting;
	}
	bool IsPreviewing() const
	{
		return previewing;
	}
	void SetKeyFrameControllerClient( KeyFrameControllerClient* client )
	{
		keyFrameControllerClient = client;
	}

	// KeyFrameController methods
	void ShowCurrentStatus( double position, frame_type type, bool hasPrev, bool hasNext );
	double GetCurrentPosition( );

	// KeyFrameController callbacks
	void OnKeyFrameControllerKeyChanged( GtkToggleButton* togglebutton );

	// Used by signal handlers
	void OnTimeRangeChanged();
	GtkWidget *window;

private:
	// Imported
	KinoCommon *common;

	int last_page;
	bool rendering;
	bool paused;
	bool previewing;
	bool repainting;
	GtkProgressBar *progressBar;
	char status[ 10240 ];
	int previewPosition;
	GtkWidget *scrubBar;
	GtkAdjustment *scrubAdjustment;

	// Filters and Transitions
	PluginImageCreateRepository image_creators;
	PluginImageFilterRepository image_filters;
	PluginImageTransitionRepository image_transitions;
	PluginAudioFilterRepository audio_filters;
	PluginAudioTransitionRepository audio_transitions;

	// audio structures
	kino_sound_t *audio_device;
	dv_audio_t dv_audio;
	bool audio_device_avail;
	gint audio_sampling_rate;
	void PlayAudio( int16_t *buffers[], int, int, int, bool );

	// Video structures
	void ShowImage( GtkWidget *area, uint8_t *pixels, int, int, bool );

	PluginCollection plugins;
	string last_fx_file;
	double startTime;
	double pauseTime;
	double nextUpdateTime;
	KeyFrameControllerClient* keyFrameControllerClient;
	bool isGuiLocked;
	bool isPreviousScene;
	bool isNextScene;
	double newPosition;
};

#endif

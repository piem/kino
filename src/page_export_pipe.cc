/*
* page_export_pipe.cc DV Pipe exports
* Copyright (C) 2003 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
using std::cerr;
using std::endl;

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "page_export_pipe.h"
#include "preferences.h"
#include "message.h"
#include "frame.h"
#include "rwpipe.h"
#include "stringutils.h"

#include <sys/types.h>
#include <dirent.h>
#include <sys/wait.h>

class DVPipeTool : protected RWPipe
{
public:
	string m_command;
	string m_description;
	bool m_active;
	string m_flags;
	vector < string > m_profiles;

	DVPipeTool( string command ) :
			m_command( command ),
			m_description( "" ),
			m_active( false )
	{
		load( command );
	}

	void load( string command )
	{
		if ( run( command ) )
		{
			char temp[ 10240 ];
			while ( readLine( temp, 10240 ) > 0 )
			{
				char * ptr = strchr( temp, ':' );
				if ( ptr != NULL )
				{
					*ptr = '\0';
					ptr += 2;

					if ( !strcmp( temp, "Title" ) )
						m_description = ptr;
					else if ( !strcmp( temp, "Status" ) )
						m_active = !strcmp( ptr, "Active" );
					else if ( !strcmp( temp, "Flags" ) )
						m_flags = ptr;
					else if ( !strcmp( temp, "Profile" ) )
						m_profiles.push_back( ptr );
					else
						fprintf( stderr, "Unrecognised %s: %s\n", temp, ptr );
				}
			}
			stop( );
		}
	}

	bool execute( const string& normalisation, double length, int profile, 
		const string& file, const string& docfile, int pass, const string& aspect )
	{
		gchar *command;
		bool result;

		// Construct the command
		command = g_strdup_printf( "\"%s\" %s %f %d \"%s\" \"%s\" %d \"%s\"", m_command.c_str( ),
		         normalisation.c_str( ),
		         length,
		         profile,
		         StringUtils::replaceAll( file, "\"", "\\\"" ).c_str( ),
		         docfile.c_str(),
		         pass,
		         aspect.c_str() );

		result = run( command );
		g_free( command );
		return result;
	}

	bool output( Frame &frame )
	{
		int size = frame.IsPAL( ) ? 144000 : 120000;
		return writeData( frame.data, size ) == size ;
	}

	void close( )
	{
		stop( );
	}

	static bool compare( const DVPipeTool* x, const DVPipeTool* y )
	{
		return ( x->m_description < y->m_description );
	}
};

static void on_tool_change( GtkOptionMenu *optionmenu, gpointer user_data )
{
	int index = gtk_option_menu_get_history( optionmenu );
	( ( ExportPipe * ) user_data ) ->selectTool( index );
}

/** Constructor for page.

	\param exportPage the export page object to bind to
	\param common	common object to which this page belongs
*/
ExportPipe::ExportPipe( PageExport *_exportPage, KinoCommon *_common ) :
		Export( _exportPage, _common )
{
	cerr << "> Creating ExportPipe Page" << endl;

	loadTools( DATADIR "/kino/scripts/exports" );
	string alternate_dir = string( getenv( "HOME" ) ) + string( "/kino/exports" );
	loadTools( alternate_dir );

	char *kinoHome=getenv("KINO_HOME");

	if(kinoHome)
	{
		string alternate_dir = string( kinoHome ) + string( "/exports" );
		loadTools( alternate_dir );
	}

	GtkWidget *window = common->getWidget();
	GtkOptionMenu *option_menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_pipe_tools" ) );
	g_signal_connect( G_OBJECT( option_menu ), "changed", G_CALLBACK( on_tool_change ), this );

	activateTool( 0 );
}

/** Destructor for page.
 */

ExportPipe::~ExportPipe()
{
	cerr << "> Destroying ExportPipe Page" << endl;
}

/** Load all scripts in a given directory.
*/

void ExportPipe::loadTools( string directory )
{
	char * filename;
	struct dirent *entry;

	DIR *dir = opendir( directory.c_str( ) );

	if ( dir )
	{
		while ( ( entry = readdir( dir ) ) != NULL )
		{
			filename = g_strdup_printf( "%s/%s", directory.c_str( ), entry->d_name );
			if ( entry->d_name[ 0 ] != '.' )
			{
				fprintf( stderr, "%s\n", filename );
				DVPipeTool *tool = new DVPipeTool( filename );
				if ( tool->m_active )
					m_tools.push_back( tool );
				else
					delete tool;
			}
			g_free( filename );
		}
		closedir( dir );
	}
	std::sort( m_tools.begin(), m_tools.end(), DVPipeTool::compare );
}

void ExportPipe::activateTool( int index )
{
	// Get the main widget
	GtkWidget * window = common->getWidget();

	// Start with the option menu
	GtkOptionMenu *option_menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_pipe_tools" ) );
	GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( option_menu ) );

	// Create a menu if we don't have one
	if ( menu == NULL )
	{
		menu = GTK_MENU( gtk_menu_new( ) );
		for ( unsigned int i = 0; i < m_tools.size( ); i ++ )
		{
			GtkWidget *item = gtk_menu_item_new_with_label( m_tools[ i ] ->m_description.c_str( ) );
			gtk_widget_show( item );
			gtk_menu_append( menu, item );
		}

		gtk_option_menu_set_menu( option_menu, GTK_WIDGET( menu ) );
	}

	// Set the active item
	gtk_option_menu_set_history( option_menu, index );
}

void ExportPipe::selectTool( int index )
{
	// Get the main widget
	GtkWidget * window = common->getWidget();

	// Populate the profiles
	GtkOptionMenu *option_menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_pipe_profile" ) );
	GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( option_menu ) );

	// Remove the old menu if it exists
	if ( menu == NULL )
		gtk_option_menu_remove_menu( option_menu );

	// Create a new one
	menu = GTK_MENU( gtk_menu_new( ) );

	// Populate it...
	if ( m_tools[ index ] ->m_profiles.size( ) )
	{
		for ( unsigned int i = 0; i < m_tools[ index ] ->m_profiles.size( ); i ++ )
		{
			GtkWidget *item = gtk_menu_item_new_with_label( m_tools[ index ] ->m_profiles[ i ].c_str( ) );
			gtk_widget_show( item );
			gtk_menu_append( menu, item );
		}
	}
	else
	{
		GtkWidget *item = gtk_menu_item_new_with_label( "[No Profile Available]" );
		gtk_widget_show( item );
		gtk_menu_append( menu, item );
	}

	// Set it
	gtk_option_menu_set_menu( option_menu, GTK_WIDGET( menu ) );
}

/** Start of page.
 */

void ExportPipe::start()
{
	cerr << ">> Entering ExportPipe" << endl;
	playlistTempName[ 0 ] = '\0';
}

/** Define active widgets.
    This page does not support pausing, and if the system is
    not loaded, it does not support anything at all.
 */

gulong ExportPipe::onActivate()
{
	return EXPORT_EXPORT | EXPORT_SCENE_LIST | EXPORT_PAUSE;
}

/** Leaving the page
 */

void ExportPipe::clean()
{
	cerr << ">> Leaving ExportPipe" << endl;
	if ( strcmp( playlistTempName, "" ) )
		unlink( playlistTempName );
}

/** The actual export function */

enum export_result ExportPipe::doExport( PlayList *playlist, int begin, int end, int every, bool preview )
{
	enum export_result status = EXPORT_RESULT_SUCCESS;
	Frame& frame = *GetFramePool()->GetFrame();
	int i = -1, j;

	// Get the main widget
	GtkWidget *window = common->getWidget();

	// Obtain the selected tool
	GtkOptionMenu *option_menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_pipe_tools" ) );
	DVPipeTool *tool = m_tools[ gtk_option_menu_get_history( option_menu ) ];

	if ( tool != NULL )
	{
		// Determine the number of passes to do
		int passes = 1;
		if ( tool->m_flags.find("double-pass") != string::npos )
			passes = 2;

		// Get the first frame to determine normalisation
		playlist->GetFrame( begin, frame );

		// Set up resampling
		int16_le_t *audio_buffers[ 4 ];
		for ( int c = 0; c < 4; c++ )
			audio_buffers[ c ] = new int16_le_t[ 2 * DV_AUDIO_MAX_SAMPLES ];

		// Get audio info
		AudioInfo info;
		frame.GetAudioInfo( info );

		// Resample to 48000 if invalid rate detected
		if ( info.frequency == 0 )
			info.frequency = 48000;

		// Evaluate argument for tool
		string normalisation = frame.IsPAL( ) ? "pal" : "ntsc";
		string aspect = frame.IsWide() ? "16:9" : "4:3";
		double length = ( double ) ( end - begin ) / ( frame.IsPAL( ) ? 25.0 : 29.97 );

		// Get the profile
		GtkOptionMenu *option_menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_pipe_profile" ) );
		int profile = gtk_option_menu_get_history( option_menu );

		// Get the file name
		string filename = ( char* ) gtk_entry_get_text( GTK_ENTRY( lookup_widget( window, "entry_export_pipe_file" ) ) );
		string baseFileName = playlist->GetBaseFileOfFrame( begin );
		if ( filename.length() < 1 && baseFileName.length() > 0 )
			filename = baseFileName;
	
		// first call to innerLoopUpdate initializes progress tracker, which
		// we want to do because calculateAdjustedRate generates paused time.
		innerLoopUpdate( begin, begin, end + (end - begin + 1) * (passes - 1), every );

		// Determine correct amount of audio for duration
		double adjustedRate = calculateAdjustedRate( playlist, info.frequency, begin, end, every );
		if ( !adjustedRate )
			status = EXPORT_RESULT_ABORT;

		for (int pass = 1; pass <= passes && exportPage->isExporting; pass++ )
		{
			// Execute the tool
			bool success = false;
			if ( common->getPlayList()->GetDocName() == "" )
			{
				if ( strcmp( playlistTempName, "" ) == 0 )
				{
					PlayList* copy = new PlayList( *( common->getPlayList() ) );
					strcpy( playlistTempName, "/tmp/kino.XXXXXX" );
					mkstemp( playlistTempName );
					copy->SavePlayList( playlistTempName );
					delete copy;
				}
				success = tool->execute( normalisation, length, profile, filename, playlistTempName, pass, aspect );
			}
			else
			{
				success = tool->execute( normalisation, length, profile, filename, common->getPlayList()->GetDocName(), pass, aspect );
			}
	
			// Create the resampler
			AsyncAudioResample<int16_ne_t,int16_le_t>* resampler = new AsyncAudioResample<int16_ne_t,int16_le_t>(
				AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, playlist, info.frequency, begin, end, every );
			if ( resampler->IsError() )
			{
				std::cerr << ">>> Resampler error: " << resampler->GetError() << std::endl;
				exportPage->isExporting = false;
				status = EXPORT_RESULT_FAILURE;
			}
	
			// Iterate over all frames in selection
			for ( i = begin, j = 0; success && i <= end && exportPage->isExporting; i += every, j++ )
			{
				// Call innerLoopUpdate
				if ( passes > 1 )
					innerLoopUpdate( i + (end - begin + 1) / every * (pass - 1),
						begin, end + (end - begin + 1), every, i,
						pass == 1 ? _("Exporting (pass 1)") : _("Exporting (pass 2)") );
				else
					innerLoopUpdate( i, begin, end, every );
	
				// Resample
				int requestedSamples = frame.CalculateNumberSamples( info.frequency, j );
				info.samples = resampler->Process( adjustedRate, requestedSamples );
				int16_le_t *p = resampler->GetOutput();
				for ( int s = 0; s < info.samples; s++ )
					for ( int c = 0; c < info.channels; c++ )
						audio_buffers[ c ][ s ] = *p++;

				// Get the frame
				playlist->GetFrame( i, frame );
	
				if ( info.samples )
					frame.EncodeAudio( info, audio_buffers );
	
				// Write the frame
				success = tool->output( frame );
			}
			tool->close( );
			delete resampler;
		}
		for ( int c = 0; c < 4; c++ )
			delete[] audio_buffers[ c ];
	}

	GetFramePool()->DoneWithFrame( &frame );

	// Return result
	if ( status == EXPORT_RESULT_SUCCESS && !exportPage->isExporting )
		return EXPORT_RESULT_ABORT;

	return status;
}

extern "C"
{
	void
	on_button_export_pipe_file_clicked     (GtkButton       *button,
                                            gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to supply to Tool") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_export_pipe_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_export_pipe_file" ) ), filename );
	}
}

/*
* page_export_audio.h Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EXPORT_AUDIO_H
#define _PAGE_EXPORT_AUDIO_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "export.h"
#include "page_export.h"

/** This class represents the Export/Still Frames notebook page.
 */

class ExportAudio : public Export
{
private:
	/* Pointers to the controls on this page */
	GtkEntry *fileEntry;
	GtkMenu	*sampleRate;
	GtkMenu	*audioFormat;
	GtkToggleButton	*splitCheck;

	enum export_result
	doExport( PlayList * playlist, int begin, int end, int every,
	          bool preview );
	void start( );

public:
	ExportAudio( PageExport*, KinoCommon* );
	virtual ~ExportAudio();
};

#endif


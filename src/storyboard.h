/*
* storyboard.h Storyboard view object
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef STORYBOARD_H
#define STORYBOARD_H

#include <gtk/gtk.h>
#include "kino_common.h"
#include "smiltime.h"

class Storyboard
{

public:
	Storyboard( KinoCommon *common );
	~Storyboard();
	void redraw();
	void select( int scene );
	void setSensitive( bool sensitive );
	void moveScene( unsigned int destScene );

	GtkTreeView *getView()
	{
		return this->view;
	}
	GtkTreeStore *getModel()
	{
		return this->model;
	}
	bool getSkipSelect()
	{
		return this->skipSelect;
	}
	void reset()
	{
		this->selection = -1;
	}
	bool getSkip()
	{
		return this->skip;
	}
	void setSkip()
	{
		this->skip = true;
	}
	void clearSkip()
	{
		this->skip = false;
	}
	SMIL::MediaClippingTime& getTime()
	{
		return m_time;
	}

private:
	GtkTreeView* view;
	GtkTreeStore* model;
	KinoCommon* common;
	int selection;
	bool skipSelect;
	bool skip;
	SMIL::MediaClippingTime m_time;
};

extern Storyboard *GetStoryboard( );

#endif

/*
* page_export.cc Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include <gtk/gtk.h>
#include <sys/time.h>

#include "page_export.h"
#include "page_export_1394.h"
#include "page_export_avi.h"
#include "page_export_stills.h"
#include "page_export_audio.h"
#include "page_export_mjpeg.h"
#include "page_export_pipe.h"
#include "page_editor.h"
#include "filehandler.h"

extern "C"
{
#include "support.h"
	#include "callbacks.h"
	#include "commands.h"

	extern struct navigate_control g_nav_ctl;
	extern char cmd[];
	static char lastcmd[ 16 ];
}

/** Constructor for page.

  	\param common	common object to which this page belongs
*/

PageExport::PageExport( KinoCommon *common )
		: currentMode( -1 ), isExporting( false ), isPausing( false ), exportMutex( false )
{
	cerr << "> Creating Export Page" << endl;
	this->common = common;
	this->notebook = GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_export" ) );
	this->progressBar = GTK_PROGRESS_BAR( lookup_widget( common->getWidget(), "progressbar" ) );
	gtk_progress_bar_set_pulse_step( this->progressBar, 0.05 );

	// Create PageExport objects
	this->export1394 = new Export1394( this, common );
	this->exportAVI = new ExportAVI( this, common );
	this->exportStills = new ExportStills( this, common );
	this->exportAudio = new ExportAudio( this, common );
	this->exportMJPEG = new ExportMJPEG( this, common );
	this->exportPipe = new ExportPipe( this, common );

	// Set to the first page
	this->currentMode = EXPORT_MODE_1394;

}

/** Destructor for page.
*/

PageExport::~PageExport()
{
	cerr << "> Destroying Export Page" << endl;
}

/** Start of page.
*/

void PageExport::start()
{
	cerr << ">> Starting Export" << endl;
	gtk_notebook_set_page( GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_keyhelp" ) ), 3 );
	this->getCurrentPage() ->start();
	if ( !isExporting )
	{
		resetProgress();
	}

	// Because the file tracker tracks all created files, make sure we ignore exports
	FileTracker::GetInstance().SetMode( CAPTURE_IGNORE );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_export" ) ), TRUE );
}

/** Define active widgets.
*/

gulong PageExport::activate()
{
	return this->getCurrentPage() ->activate();
}

/** Leaving the page
*/

void PageExport::clean()
{
	cerr << ">> Leaving Export" << endl;
	this->getCurrentPage() ->clean();
	// Just in case the user goes straight to capture
	FileTracker::GetInstance().SetMode( CAPTURE_MOVIE_APPEND );
}


/** accessor method to the common widget

	For use by the export objects.
*/
GtkWidget *PageExport::getWidget()
{
	return common->getWidget();
}


/**	Sends change export mode request to the gui.

	\param mode export mode to change to
*/

void PageExport::changeModeRequest( int mode )
{
	gtk_notebook_set_page( notebook, mode );
}

/**	Carries out a change mode - this should be triggered by changeModeRequest
	to ensure that the GUI stays in sync with the common structure.

	\param mode export mode to change to
*/
void PageExport::setCurrentMode( int mode )
{
	if ( currentMode != mode )
	{
		if ( currentMode != -1 )
			this->clean();
		currentMode = mode;
		this->start();
		
		// Enable or disable the widgets - it calls our activate() and
		// deactivate() and can control the storyboard whereas 
		// Export::activate() can not!
		common->activateWidgets();
	}
}


/** Returns the Page object associated to the specified notebook page.

	\param mode export mode of PageExport object to return
	\return the PageExport object associated to the given notebook page or NULL if invalid.
*/

Export *PageExport::getPage( int mode )
{
	Export * ret = NULL;

	switch ( mode )
	{
	case EXPORT_MODE_1394:
		ret = export1394;
		break;
	case EXPORT_MODE_AVI:
		ret = exportAVI;
		break;
	case EXPORT_MODE_STILLS:
		ret = exportStills;
		break;
	case EXPORT_MODE_AUDIO:
		ret = exportAudio;
		break;
	case EXPORT_MODE_MJPEG:
		ret = exportMJPEG;
		break;
	case EXPORT_MODE_PIPE:
		ret = exportPipe;
		break;
	}

	return ret;
}

/** Returns the Page object that corresponds to the current export mode.

	\return the current Page object (null if current page is invalid).
*/

Export *PageExport::getCurrentPage( )
{
	Export * ret = getPage( this->currentMode );
	return ret;
}

/** Preview export generically
 */
void PageExport::previewExport()
{
	if ( !exportMutex && !isExporting )
	{
		exportMutex = true;
		/* Preview is also exporting - in the lock sense */
		isExporting = true;
		/* Call with true to indicate preview */
		getCurrentPage() ->startExport( true );
		isExporting = false;
		exportMutex = false;
	}
}

/** Start export generically
*/
void PageExport::startExport()
{
	if ( !exportMutex && !isExporting )
	{
		exportMutex = true;
		isExporting = true;

		getCurrentPage() ->startExport();

		exportMutex = false;
	}
}


/** Stop the export generically
*/
void PageExport::stopExport()
{
	if ( !exportMutex && isExporting )
	{
		exportMutex = true;

		getCurrentPage() ->stopExport();

		isExporting = false;
		/* The buttons are fixed from Export */
		isPausing = false;
		exportMutex = false;
	}
}

/* Pause the export generically
 */
void PageExport::pauseExport()
{
	cerr << ">>> PageExport::pauseExport()" << endl;
	if ( !exportMutex && isExporting )
	{
		exportMutex = true;
		isPausing = !isPausing;
		getCurrentPage() ->pauseExport();
		exportMutex = false;
	}
}

/** Set the export range to a particular scene.
 
    \param scene the numerical index referring to a scene in the playlist
     
*/
void PageExport::selectScene( int scene )
{
	getCurrentPage() ->selectScene( scene );
}


/** Process a keyboard event.
 
  	\param event	keyboard event
*/

gboolean PageExport::processKeyboard( GdkEventKey *event )
{
	gboolean ret = FALSE;

#if 0
	printf( "send_event: %2.2x\n", event->send_event );
	printf( "time  : %8.8x\n", event->time );
	printf( "state : %8.8x\n", event->state );
	printf( "keyval: %8.8x\n", event->keyval );
	printf( "length: %8.8x\n", event->length );
	printf( "group : %8.8x\n", event->group );
	printf( "string: %s\n", event->string );
	printf( "(hex) : %2.2x\n", event->string[ 0 ] );
	printf( "cmd   : %s\n", cmd );
	printf( "(hex) : %8.8x\n", cmd[ 0 ] );
#endif

	// Only process while not escape mode
	if ( g_nav_ctl.escaped == FALSE )
	{
		if ( strcmp( lastcmd, "alt" ) == 0 )
		{
			strcpy( lastcmd, "" );
			return ret;
		}

		switch ( event->keyval )
		{
			// Translate special keys to equivalent command
		case GDK_Return:
			common->keyboardFeedback( cmd, _( "Start Export" ) );
			startExport();
			return TRUE;
		case GDK_Escape:
			if ( this->isExporting )
			{
				common->keyboardFeedback( cmd, _( "Stop Export" ) );
				stopExport( );
			}
			else
			{
				common->changePageRequest( PAGE_EDITOR );
			}
			cmd[ 0 ] = 0;
			return TRUE;
		case GDK_Alt_L:
		case GDK_Alt_R:
			strcpy( lastcmd, "alt" );
			return FALSE;
		default:
			strcat( cmd, event->string );
			break;
		}

		if ( !strcmp( cmd, "." ) )
			strcpy( cmd, lastcmd );

		ret = processCommand( cmd );

	}
	else if ( event->keyval == GDK_Return || event->keyval == GDK_Escape )
	{
		g_nav_ctl.escaped = FALSE;
		gtk_widget_grab_focus( GTK_WIDGET( notebook ) );
	}

	return ret;
}


/** Internal method for handling a complete keyboard scene.
 
  	\param cmd		command to be processed;
*/

gboolean PageExport::processCommand( const char *command )
{
	int count = 1;
	char real[ 256 ] = "";

	strcpy( cmd, command );

	switch ( sscanf( cmd, "%d%s", &count, real ) )
	{
	case 1:
		// Numeric value only - return immediately if the cmd is not "0"
		if ( strcmp( cmd, "0" ) )
		{
			common->keyboardFeedback( cmd, "" );
			return FALSE;
		}
		break;
	case 0:
		sscanf( cmd, "%s", real );
		count = 1;
		break;
	}

	strcpy( lastcmd, cmd );

	/* write PlayList */

	if ( strcmp( cmd, ":w" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Write playlist" ) );
		common->savePlayList( );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "Enter" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Start Export" ) );
		GdkEvent ev;
		ev.key.type = GDK_KEY_PRESS;
		ev.key.window = common->getWidget()->window;
		ev.key.send_event = TRUE;
		ev.key.state = 0;
		ev.key.length = 0;
		ev.key.string = (gchar*) "";
		ev.key.keyval = GDK_Return;
		ev.key.group = 0;
		gdk_event_put( &ev );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "Esc" ) == 0 && isExporting )
	{
		common->keyboardFeedback( cmd, _( "Stop Export" ) );
		stopExport();
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "F2" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Edit" ) );
		common->changePageRequest( PAGE_EDITOR );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "A" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, append to movie" ) );
		common->changePageRequest( PAGE_EDITOR );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "v" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Timeline" ) );
		common->changePageRequest( PAGE_TIMELINE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "t" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Trim" ) );
		common->changePageRequest( PAGE_TRIM );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "C" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "FX" ) );
		common->changePageRequest( PAGE_TIMELINE );
		cmd[ 0 ] = 0;
	}

	/* quit */

	else if ( strcmp( cmd, ":q" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "quit" ) );
		kinoDeactivate();
		cmd[ 0 ] = 0;
	}

	else
	{
		// Check for invalid commands
		if ( strlen( real ) > 3 )
			cmd[ 0 ] = 0;
		else if ( strchr( "dgy ", real[ strlen( real ) - 1 ] ) == NULL )
			cmd[ 0 ] = 0;

		common->keyboardFeedback( cmd, "" );
	}

	return FALSE;
}

/** update the progress bar for the export process
*/

void PageExport::updateProgress( gfloat val )
{
	struct timeval tv;
	long long now;
	static long long lastUpdateTime = 0;

	gettimeofday( &tv, 0 );
	now = 1000000 * ( long long ) tv.tv_sec + ( long long ) tv.tv_usec;

	/* update every 0.3 second */

	if ( val > 1.0 )
		val = 1.0;

	if ( now > lastUpdateTime + 300000 || val == 1.0 )
	{
		if ( val < 0.0 )
			gtk_progress_bar_pulse( progressBar );
		else
			gtk_progress_bar_set_fraction( progressBar, val );
		lastUpdateTime = now;
	}

	while ( gtk_events_pending() )
	{
		gtk_main_iteration();
	}
}

void PageExport::resetProgress()
{
	gtk_progress_bar_update( progressBar, 0.0 );
}

void PageExport::timeFormatChanged()
{
	on_spinbutton_export_range_start_value_changed( GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "spinbutton_export_range_start" ) ), NULL );
	on_spinbutton_export_range_end_value_changed( GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "spinbutton_export_range_end" ) ), NULL );
}


extern "C"
{

	gchar avi_entry_text[ 1024 ];
	gchar stills_entry_text[ 1024 ];
	gchar audio_entry_text[ 1024 ];
	gchar mjpeg_entry_text[ 1024 ];

	void
	on_notebook_export_switch_page ( GtkNotebook * notebook,
	                                 GtkNotebookPage * page,
	                                 gint page_num,
	                                 gpointer user_data )
	{
		setExportMode( page_num );

	}

	/* The common export control buttons (start, stop, preview) */
	void
	on_togglebutton_export_preview_toggled ( GtkToggleButton * togglebutton,
	        gpointer user_data )
	{
		previewExport();
	}


	void
	on_togglebutton_export_record_toggled ( GtkToggleButton * togglebutton,
	                                        gpointer user_data )
	{
		startExport();
	}


	void
	on_togglebutton_export_stop_toggled ( GtkToggleButton * togglebutton,
	                                      gpointer user_data )
	{
		stopExport();
	}


	void
	on_togglebutton_export_pause_toggled ( GtkToggleButton * togglebutton,
	                                       gpointer user_data )
	{
		pauseExport();
	}


	/* Focus on the from-to radiobutton */
	void
	on_spinbutton_export_range_start_end_changed
	( GtkEditable * editable,
	  gpointer user_data )
	{
		common->getPageExport() ->getCurrentPage() ->onRangeChange( GTK_SPIN_BUTTON( editable ) );
	}
	
	void
	on_spinbutton_export_range_start_value_changed
											(GtkSpinButton   *spinbutton,
											gpointer         user_data)
	{
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_export_start" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
	}
	
	
	void
	on_spinbutton_export_range_end_value_changed
											(GtkSpinButton   *spinbutton,
											gpointer         user_data)
	{
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_export_end" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
	}
	
	void
	on_entry_export_start_activate         (GtkEntry        *entry,
											gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_export_range_start" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_export_range_start_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_export_start_focus_out_event  (GtkWidget       *widget,
											GdkEventFocus   *event,
											gpointer         user_data)
	{
		on_entry_export_start_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
	
	void
	on_entry_export_end_activate           (GtkEntry        *entry,
											gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_export_range_end" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_export_range_end_value_changed( spinbutton, NULL );
	}

	gboolean
	on_entry_export_end_focus_out_event    (GtkWidget       *widget,
											GdkEventFocus   *event,
											gpointer         user_data)
	{
		on_entry_export_end_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
}

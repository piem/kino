/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
 * Initial main.c file generated by Glade. Edit as required.
 * Glade will not overwrite this file.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_EXECINFO_H
#include <execinfo.h>
#endif

#include <gtk/gtk.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef ENABLE_NLS
#include <libintl.h>
#endif
#include <glade/glade.h>
#include <string.h>

#include "commands.h"

GtkWidget *main_window;
GladeXML *magick_glade;
GladeXML *kino_glade;
char* g_help_language;

static int sigpipe = 0;
void sigpipe_clear( void );
int sigpipe_get( void );

void sigpipe_clear( void )
{
	sigpipe = 0;
}

int sigpipe_get( void )
{
	return sigpipe;
}

static void sigpipe_handler( int value )
{
	fprintf( stderr, "SIGPIPE Received (%d) - setting indicator\n", value );
	sigpipe ++;
}

static void sigusr2_handler( int value )
{
	fprintf( stderr, "SIGUSR2 Received (%d) - scanning for jog/shuttle controllers\n", value );
	startJogShuttle();
}

/**
 * Segmentation fault handler.
 *
 * <p>This handler unwinds the stack and prints the stackframes.</p>
 *
 */
#ifdef HAVE_EXECINFO_H
static void sigsegv_handler()
{
	void *array[ 10 ];
	size_t size;
	char **strings;
	size_t i;

	fprintf( stderr, "\nKino experienced a segmentation fault.\n"
		"Dumping stack from the offending thread\n\n" );
	size = backtrace( array, 10 );
	strings = backtrace_symbols( array, size );

	fprintf( stderr, "Obtained %zd stack frames.\n", size );

	for ( i = 0; i < size; i++ )
		fprintf( stderr, "%s\n", strings[ i ] );

	free( strings );

	fprintf( stderr, "\nDone dumping - exiting.\n" );
	exit( 1 );
}
#endif



int
main ( int argc, char *argv[] )
{
	if ( strstr( argv[0], "kino2raw" ) )
	{
		if ( argc > 1 )
			exit( kino2raw( argv[1], argc > 2 ? argv[2] : NULL ) );
		else
			exit( 1 );
	}
#ifdef ENABLE_NLS
	bindtextdomain ( PACKAGE, DATADIR "/locale" );
	bind_textdomain_codeset( PACKAGE, "UTF-8" );
	textdomain ( PACKAGE );
#endif

	signal( SIGPIPE, sigpipe_handler );
	signal( SIGUSR2, sigusr2_handler );
#ifdef HAVE_EXECINFO_H
	signal( SIGSEGV,  sigsegv_handler );
#endif

	g_help_language = calloc( 1, 3 );
	g_thread_init( NULL );
#ifdef HELP_LINGUAS
	gchar** translations = g_strsplit( HELP_LINGUAS, " ", 0 );
	const gchar* const * locales = g_get_language_names();
	int i, j;
	for ( i = 0; translations[i] && !g_help_language[0]; ++i )
	{
		for ( j = 0; locales[j]; ++j )
		{
			gchar** parts = g_strsplit_set( locales[j], "._", 0 );
			if ( parts[0] && strncmp( parts[0], translations[i], 2 ) == 0 )
			{
				strncpy( g_help_language, parts[0], 2 );
				g_strfreev( parts );
				break;
			}
			g_strfreev( parts );
		}
	}
	g_strfreev( translations );
#endif
	if ( !g_help_language[0] )
		strncpy( g_help_language, "en", 2 );
	fprintf( stderr, "> help language code %s\n", g_help_language);

	gdk_threads_init();
	gdk_rgb_init();
	gtk_init( &argc, &argv );
	glade_init();

	magick_glade = glade_xml_new( DATADIR "/kino/magick.glade", NULL, NULL );
	kino_glade = glade_xml_new( DATADIR "/kino/kino.glade", NULL, NULL );
	glade_xml_signal_autoconnect( magick_glade );
	glade_xml_signal_autoconnect( kino_glade );
	main_window = glade_xml_get_widget( kino_glade, "main_window" );
	gtk_widget_set_events( main_window, GDK_KEY_RELEASE_MASK );
	gtk_window_set_default_icon_from_file( DATADIR "/kino/kino.png", NULL );

	kinoInitialise( main_window );
	gtk_widget_show_all( main_window );
	kinoPostInit();

	gdk_threads_enter();
	bulkLoad( argc, argv );
	gtk_main();
	gdk_threads_leave();
	free( g_help_language );

	return 0;
}

/*
* page_edit.h Notebook Editor Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EDITOR_H
#define _PAGE_EDITOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "framedisplayer.h"
#include "kino_common.h"
#include "page.h"
#include "preferences.h"

/** This class controls the editor notebook page.
*/

class PageEditor : public Page
{
private:
	// Common object
	KinoCommon *common;

	// GUI Widgets
	FrameDisplayer *displayer;
	GtkDrawingArea *frameArea;
	GtkLabel *positionLabelCurrent;
	GtkLabel *positionLabelTotal;
	GtkWidget *scrubBar;
	GtkAdjustment *scrubAdjustment;

	// State retention
	int lastFrameShown;
	gint idleCommand;
	gboolean idleCommandActive;
	GArray *sceneIndex;
	vector <int> sceneStartList;

	// Playlist for holding the last copy/cut frames
	PlayList *g_copiedPlayList;

public:
	PageEditor( KinoCommon *common );
	virtual ~PageEditor();
	FrameDisplayer *getFrameDisplayer()
	{
		return this->displayer;
	}
	// Overridden virtuals from Page
	gulong activate();
	void newFile();
	void start();
	void clean();
	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char * );
	void selectScene( int );
	void videoStartOfMovie();
	void videoPreviousScene();
	void videoStartOfScene();
	void videoRewind();
	void videoBack(int step = -1);
	void videoPlay();
	void videoForward(int step = 1);
	void videoFastForward();
	void videoShuttle( int );
	void videoNextScene();
	void videoEndOfScene();
	void videoEndOfMovie();
	void videoPause();
	void videoStop();

	void startNavigator();
	void stopNavigator();

	void movedToFrame( int frame );

	void showFrame( int, gboolean );
	void showFrame( int, Frame& );

	void windowMoved();
	void showFrameInfo( int );
	void showFrameInfo( int, Frame & );

	void DrawBar( int currentFrame );
	void ResetBar();
	vector <int> GetScene();
	void snapshot();
	void CopyFrames( int, int );
	void PasteFrames( int );
	void DeleteFrames( int, int );
	std::string getHelpPage()
	{
		return "edit";
	}
};

#endif

/*
* page_export_stills.cc Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vector>
#include <iostream>
using std::cerr;
using std::endl;

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "page_export_stills.h"
#include "preferences.h"
#include "kino_common.h"
#include "frame.h"
#include "page_editor.h"
#include "message.h"

/** Constructor for page.

\param _exportPage the PageExport object to which this page belongs
\param common	KinoCommon object
*/

ExportStills::ExportStills( PageExport *_exportPage, KinoCommon *_common ) :
		Export( _exportPage, _common )
{
	cerr << "> Creating ExportStills Page" << endl;

	/* Get a pointer to the controls of this page */
	qualityScale
	= GTK_RANGE( lookup_widget( common->getWidget(), "hscale_export_stills" ) );

	fileEntry
	= GTK_ENTRY( lookup_widget( common->getWidget(), "entry_export_stills_file" ) );
}

/** Destructor for page.
 */

ExportStills::~ExportStills()
{
	cerr << "> Destroying ExportStills Page" << endl;
}

/** start exporting still frames
 */
enum export_result
ExportStills::doExport( PlayList * playlist, int begin, int end, int every,
                        bool preview )
{
	static unsigned char pixels[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
	GdkPixbuf *image = NULL;
	gchar *file = NULL;
	gchar *filename = NULL;
	gchar *extension = NULL;
	int i = -1;
	GError *gerror = NULL;
	char quality[ 8 ] = "75";
	int extractOption;
	bool isResample;

	file = g_strdup( gtk_entry_get_text( fileEntry ) );

	/* Check for a valid filename */
	if ( !strcmp( file, "" ) )
	{
		modal_message( _( "You must enter a filename." ) );
		g_free( file );
		return EXPORT_RESULT_FAILURE;
	}

	/* Take care of the quality parameter */
	GtkAdjustment * adjust = gtk_range_get_adjustment( qualityScale );
	if ( adjust )
		snprintf( quality, 8, "%d", ( int ) adjust->value );

	/* make sure a file extension is supplied */
	char *tmp = strrchr( file, '.' );
	if ( tmp == NULL )
	{
		modal_message( _( "You must enter a filename with an extension" ) );
		g_free( file );
		return EXPORT_RESULT_FAILURE;
	}
	else
	{
		extension = g_strdup( tmp );
		tmp[0] = '\0';
	}

	/* get the frame extraction method */
	GtkWidget *widget = gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( common->getWidget(),
		"optionmenu_export_stills_extract" ) ) );
	GtkWidget *active_item = gtk_menu_get_active( GTK_MENU( widget ) );
	extractOption = g_list_index( GTK_MENU_SHELL( widget )->children, active_item );
		
	widget = lookup_widget( common->getWidget(), "checkbutton_export_stills_resample" );
	isResample = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		
	/* Iterate over all frames in selection */
	Frame &frame = *GetFramePool( ) ->GetFrame( );
	frame.decoder->quality = DV_QUALITY_BEST;

	for ( i = begin; i <= end && exportPage->isExporting; i += every )
	{

		/* Call innerLoopUpdate */
		innerLoopUpdate( i, begin, end, every );

		// Extract pixel data
		playlist->GetFrame( i, frame );
		frame.ExtractRGB( pixels );
		switch ( extractOption )
		{
			case 1:
				frame.Deinterlace( ( uint8_t* ) pixels, ( uint8_t* ) pixels, frame.GetWidth() * 3, frame.GetHeight() );
				break;
			case 2:
				frame.GetLowerField( pixels, 3 );
				break;
			case 3:
				frame.GetUpperField( pixels, 3 );
				break;
			default:
				break;
		}

		// Get gdk-pixbuf
		image = gdk_pixbuf_new_from_data( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
		                                  frame.GetWidth(), frame.GetHeight(), frame.GetWidth() * 3, NULL, NULL );

		// Adjust pixel aspect
		if ( isResample )
		{
			int width = frame.GetWidth();
			if ( frame.IsWide() )
				width = frame.IsPAL() ? 1024 : 854;
			else
				width = frame.IsPAL() ? 768 : 640;
			AspectRatioCalculator calc( width, frame.GetHeight(),
		                            frame.GetWidth(), frame.GetHeight(),
		                            frame.IsPAL(), frame.IsWide() );
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( image, calc.width, calc.height, GDK_INTERP_HYPER );
			g_object_unref( image );
			image = scaled;
		}
		
		// save to image
		filename = g_strdup_printf( "%s_%06i%s", file, i + 1, extension );
		if ( strncasecmp( extension + 1, "png", 8 ) == 0 )
			gdk_pixbuf_save( image, filename, "png", &gerror, NULL );
		else
			gdk_pixbuf_save( image, filename, "jpeg", &gerror, "quality", quality, NULL );

		if ( gerror != NULL )
		{
			modal_message( gerror->message );
			g_error_free( gerror );
		}
		g_object_unref( image );
		g_free( filename );
	}

	GetFramePool( ) ->DoneWithFrame( &frame );
	
	g_free( file );
	g_free( extension );

	if ( !exportPage->isExporting )
		return EXPORT_RESULT_ABORT;
	else if ( i > end )
		return EXPORT_RESULT_SUCCESS;
	else
		return EXPORT_RESULT_FAILURE;
}

extern "C"
{
	void
	on_button_export_stills_file_clicked    (GtkButton       *button,
                                            gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to Save As") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_export_stills_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_export_stills_file" ) ), filename );
	}
}

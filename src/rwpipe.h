/*
* rwpipe.h - bidirectional pipe
* Copyright (C) 2003 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _RWPIPE_H
#define _RWPIPE_H

#include <string>
using std::string;
#include <vector>
using std::vector;

#include <glib.h>

class RWPipe
{
private:
	int m_pid;

protected:
	int m_reader;
	int m_writer;
	GError *m_error;

public:
	RWPipe( );
	~RWPipe( );
	bool run( string command );
	bool isRunning( );
	int readData( void *data, int size );
	int readLine( char *text, int max );
	int writeData( void *data, int size );
	void stop( );

};

#endif

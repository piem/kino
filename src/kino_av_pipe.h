/*
* kino_av_pipe.h -- AV Export Pipe Utilities
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _KINO_AV_PIPE_H
#define _KINO_AV_PIPE_H 1

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>

class KinoAudioInput
{
public:
	virtual ~KinoAudioInput() {}
	virtual bool Open( char *input ) = 0;
	virtual int GetChannels( ) = 0;
	virtual int GetFrequency( ) = 0;
	virtual int GetBytesPerSample( ) = 0;
	virtual long GetNumberOfSamples( ) = 0;
	virtual bool Seek( off_t offset ) = 0;
	virtual bool Get( int16_t *data, int numberofsamples ) = 0;
	virtual bool Close( ) = 0;
};

class KinoAudioInputFactory
{
public:
	static KinoAudioInput *CreateAudioInput( char *input );
};

class KinoAudioPipe
{
public:
	virtual ~KinoAudioPipe() {}
	virtual bool OpenAudio( char *output, int channels, int frequency, int bytespersample ) = 0;
	virtual bool OutputAudioFrame( int16_t *audio, int size ) = 0;
	virtual bool CloseAudio( ) = 0;
};

typedef enum {
    PIPE_AUDIO_WAV,
    PIPE_AUDIO_PCM
} kino_audio_pipe;

class KinoAudioFactory
{
public:
	static KinoAudioPipe *CreateAudioPipe( kino_audio_pipe );
};

class KinoVideoPipe
{
public:
	virtual ~KinoVideoPipe() {}
	virtual bool OpenVideoPipe( char *pipe, int width, int height, bool interlaced = true ) = 0;
	virtual bool OutputVideoFrame( uint8_t *frame, int size ) = 0;
	virtual bool CloseVideo( ) = 0;
};

typedef enum {
    PIPE_VIDEO_MJPEG,
    PIPE_VIDEO_DEINTERLACED_MJPEG,
    PIPE_VIDEO_DV_YUV,
    PIPE_VIDEO_DV_PGM
} kino_video_pipe;

class KinoVideoFactory
{
public:
	static KinoVideoPipe *CreateVideoPipe( kino_video_pipe );
};

class KinoAVPipe : public KinoAudioPipe, KinoVideoPipe
{
private:
	KinoAudioPipe *audio;
	KinoVideoPipe *video;

public:
	KinoAVPipe( KinoAudioPipe *audio, KinoVideoPipe *video )
	{
		this->audio = audio;
		this->video = video;
	}

	bool OpenAudio( char *output, int channels, int frequency, int bytespersample )
	{
		return audio->OpenAudio( output, channels, frequency, bytespersample );
	}

	bool OpenVideoPipe( char *pipe, int width, int height, bool interlaced = true )
	{
		return video->OpenVideoPipe( pipe, width, height, interlaced );
	}

	bool OutputVideoFrame( uint8_t *frame, int size )
	{
		return video->OutputVideoFrame( frame, size );
	}

	bool OutputAudioFrame( int16_t *frame, int size )
	{
		return audio->OutputAudioFrame( frame, size );
	}

	bool CloseAudio( )
	{
		return audio->CloseAudio( );
	}

	bool CloseVideo( )
	{
		return video->CloseVideo( );
	}

	int ExecuteCommand( char *command )
	{
		return system( command );
	}
};

#endif


/*
 * PixbufUtils.h -- Frame/Kino oriented pixbuf wrapper for various image manipuations
 * Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _KINO_PIXBUF_
#define _KINO_PIXBUF_

#include <string>
using std::string;

#include "DVScalingParams.h"
#include <gdk-pixbuf/gdk-pixbuf.h>

class PixbufUtils : virtual public DVScalingParams
{
	public:
		bool ReadImageFile( string, uint8_t *, int, int );
		bool ZoomAndScaleRGB( uint8_t *, int, int, int, int, int, int );
		bool ScalePixbuf( GdkPixbuf *, uint8_t *, int, int );
		GdkPixbuf *ConvertRGBToPixbuf( uint8_t *, int, int );
		void FillWithBackgroundColour( uint8_t *, int, int, DV_RGB & );
	protected:
		bool Composite( uint8_t *, int, int, GdkPixbuf * );
		bool ReadScaledFrame( uint8_t *, int, int, GdkPixbuf * );
		bool ReadAspectFrame( uint8_t *, int, int, GdkPixbuf * );
		bool ReadCroppedFrame( uint8_t *, int, int, GdkPixbuf * );
};

#endif

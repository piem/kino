/*
 * book.cc -- KinoPlus plugins for Kino
 * Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
 * Copyright (C) 2006-2007 Dan Dennedy <dan@dennedy.org>
 * Copyright (C) 2006 Ruslan Popov <radz@yandex.ru>: brightness, contrast,
 *   and HSV UI code
 * Copyright (C) 2005-2006 Gilles Caulier <caulier dot gilles at kdemail dot net>:
 *   digikam-based white balance code (Levels::onColorPicked and LevelsEntry::setRGBmult)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <map>
using std::map;

#include "../image_transitions.h"
#include "../image_filters.h"
#include "../kino_extra.h"
#include "../kino_plugin_types.h"
#include "../time_map.h"

#include "affine.h"
#include "PixbufUtils.h"
#include "blackbody.h"
using DigikamWhiteBalanceImagesPlugin::bbWB;

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>
#include <stdint.h>

#include <algorithm>
#include <cmath>
#include <functional>
#include <string>
using std::string;

extern "C" {
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

extern void sigpipe_clear( );
extern int sigpipe_get( );

}

GladeXML* kinoplus_glade = glade_xml_new( DATADIR "/" PACKAGE "/kinoplus.glade", NULL, NULL );

class Pixelate
	: public GDKImageFilter
{
	private:
		GtkWidget *window;
		int sw, sh;
		int ew, eh;

	public:
		Pixelate( )
			: sw( 16 )
			, sh( 16 )
			, ew( 16 )
			, eh( 16 )
		{
			window = glade_xml_get_widget( kinoplus_glade, "window_pixelate" );
		}

		virtual ~Pixelate( ) 
		{
			gtk_widget_destroy( window );
		}

		char *GetDescription( ) const 
		{ 
			return _("Pixelate");
		}

		void Average( uint8_t *start, int width, int height, int offset )
		{
			double r = (double)*( start );
			double g = (double)*( start + 1 );
			double b = (double)*( start + 2 );

			for ( int y = 0; y < height; y ++ )
			{
				uint8_t *p = start + y * offset;
				for ( int x = 0; x < width; x ++ )
				{
					r = ( r + (double)*( p ++ ) ) / 2;
					g = ( g + (double)*( p ++ ) ) / 2;
					b = ( b + (double)*( p ++ ) ) / 2;
				}
			}

			for ( int y = 0; y < height; y ++ )
			{
				uint8_t *p = start + y * offset;
				for ( int x = 0; x < width; x ++ )
				{
					*p ++ = (uint8_t)r;
					*p ++ = (uint8_t)g;
					*p ++ = (uint8_t)b;
				}
			}
		}

		void FilterFrame( uint8_t *io, int width, int height, double position, double frame_delta ) 
		{
			double ratio = double( width ) / 720.0;
			GtkEntry *spin = GTK_ENTRY( glade_xml_get_widget( kinoplus_glade, "spinbutton_start_width" ) );
			sw = int( atof( gtk_entry_get_text( spin ) ) * ratio + 0.5 );
			spin = GTK_ENTRY( glade_xml_get_widget( kinoplus_glade, "spinbutton_start_height" ) );
			sh = int( atof( gtk_entry_get_text( spin ) ) * ratio + 0.5 );
			spin = GTK_ENTRY( glade_xml_get_widget( kinoplus_glade, "spinbutton_end_width" ) );
			ew = int( atof( gtk_entry_get_text( spin ) ) * ratio + 0.5 );
			spin = GTK_ENTRY( glade_xml_get_widget( kinoplus_glade, "spinbutton_end_height" ) );
			eh = int( atof( gtk_entry_get_text( spin ) ) * ratio + 0.5 );
			
			if ( sw && sh )
			{
				int rw = (int)( sw + ( ew - sw ) * position );
				int rh = (int)( sh + ( eh - sh ) * position );
				for ( int w = 0; w < width; w += rw )
				{
					for ( int h = 0; h < height; h += rh )
					{
						int aw = w + rw > width ? rw - ( w + rw - width ) : rw;
						int ah = h + rh > height ? rh - ( h + rh - height ) : rh;
						Average( io + h * width * 3 + w * 3, aw, ah, width * 3 );
					}
				}
			}
		}
		
		void AttachWidgets( GtkBin *bin ) 
		{
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}

		void DetachWidgets( GtkBin *bin ) 
		{ 
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}

		void InterpretWidgets( GtkBin *bin ) 
		{
		}
};

class Jerker
	: public GDKImageFilter
{
	private:
		uint8_t tempframe[ 720 * 576 * 3 ];
		GtkWidget *window;
		int show_every;
		int current;

	public:
		Jerker( )
		{
			window = glade_xml_get_widget( kinoplus_glade, "window_slow_mo" );
		}

		virtual ~Jerker( ) 
		{
			gtk_widget_destroy( window );
		}

		char *GetDescription( ) const 
		{ 
			return _("Jerky");
		}

		void FilterFrame( uint8_t *io, int width, int height, double position, double frame_delta ) 
		{
			show_every = (int) gtk_range_get_value( GTK_RANGE( glade_xml_get_widget( kinoplus_glade, "hscale_slow_mo" ) ) );

			if ( current ++ % show_every == 0 )
				memcpy( tempframe, io, width * height * 3 );
			else
				memcpy( io, tempframe, width * height * 3 );
		}
		
		void AttachWidgets( GtkBin *bin ) 
		{
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}

		void DetachWidgets( GtkBin *bin ) 
		{ 
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}

		void InterpretWidgets( GtkBin *bin ) 
		{
			current = 0;
		}
};

/** Chroma key on blue....
    It is assumed that the blue stuff is in mesh, io contains the background,
    mesh should contain the foreground
*/

class ImageTransitionChromaKeyBlue
	: public ImageTransition
{
	public:
		char *GetDescription( ) const
  		{
    		return _("Blue Chroma Key");
  		}

  		void GetFrame( uint8_t *io, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
  		{
    		uint8_t *k = mesh;
    		uint8_t *p = io;
    		uint8_t kr, kg, kb;
			int max = width * height * 3;
    		while ( p < ( io + max ) ) 
      		{
        		kr = *p;
        		kg = *( p + 1 );
        		kb = *( p + 2 );
        		if (kb >= 240 && kr <= 5 && kg <= 5) 
				{
          			*p ++ = *k ++; 
          			*p ++ = *k ++; 
          			*p ++ = *k ++; 
        		} 
				else 
				{
          			p += 3;
          			k += 3;
        		}
      		}
  		}
};

class ImageTransitionChromaKeyGreen
	: public ImageTransition
{
	public:
		char *GetDescription( ) const
  		{
    		return _("Green Chroma Key");
  		}

  		void GetFrame( uint8_t *io, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
  		{
    		uint8_t *k = mesh;
    		uint8_t *p = io;
    		uint8_t kr, kg, kb;
			int max = width * height * 3;
    		while ( p < ( io + max ) ) 
      		{
        		kr = *p;
        		kg = *( p + 1 );
        		kb = *( p + 2 );
        		if (kg >= 240 && kr <= 5 && kb <= 5) 
				{
          			*p ++ = *k ++; 
          			*p ++ = *k ++; 
          			*p ++ = *k ++; 
        		} 
				else 
				{
          			p += 3;
          			k += 3;
        		}
      		}
  		}
};

class LineDraw
	: public GDKImageFilter
{
	private:
		GtkWidget *window;
		uint8_t *temp;
		double scale;
		bool interlace_on;
		bool interlace_first_field;
		int y_scatter;
		int x_scatter;
		double mix;

	public:
		LineDraw( )
			: scale( 2.0 )
			, interlace_on( false )
			, interlace_first_field( false )
			, y_scatter( 2 )
			, x_scatter( 2 )
			, mix( 0 )
		{
			window = glade_xml_get_widget( kinoplus_glade, "window_line_draw" );
			GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "hscale_line_draw" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), NULL );
			widget = glade_xml_get_widget( kinoplus_glade, "hscale_x_scatter" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), NULL );
			widget = glade_xml_get_widget( kinoplus_glade, "hscale_y_scatter" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), NULL );
			widget = glade_xml_get_widget( kinoplus_glade, "hscale_mix" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), NULL );
		}

		virtual ~LineDraw( ) 
		{
			gtk_widget_destroy( window );
		}

		char *GetDescription( ) const 
		{
			return _("Charcoal");
		}

		GtkWidget *GetGtkWidget()
		{
			return window;
		}

		int GetPix( uint8_t *pixels, int width, int height, int x, int y )
		{
			if ( x < 0 || x >= width || y < 0 || y >= height )
			{
				return 0;
			}
			else
			{
				uint8_t *pixel = pixels + y * width * 3 + x * 3;
				//kino::basic_rgb<uint8_t>* pixel = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels + y * width * 3 + x * 3 );
				//return pixel->red;
				return *pixel;
			}
		}

		void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta ) 
		{
			uint8_t *copy = NULL;
			if ( mix != 0 )
			{
				copy = new uint8_t[ width * height * 3 ];
				memcpy( copy, pixels, width * height * 3 );
			}

			scale = gtk_range_get_value( GTK_RANGE( glade_xml_get_widget( kinoplus_glade, "hscale_line_draw" ) ) ) / 10;
			x_scatter = (int) gtk_range_get_value( GTK_RANGE( glade_xml_get_widget( kinoplus_glade, "hscale_x_scatter" ) ) );
			y_scatter = (int) gtk_range_get_value( GTK_RANGE( glade_xml_get_widget( kinoplus_glade, "hscale_y_scatter" ) ) );
			mix = 1.0 - gtk_range_get_value( GTK_RANGE( glade_xml_get_widget( kinoplus_glade, "hscale_mix" ) ) ) / 100;
			GtkWidget* widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_linedraw_interlace" );
			interlace_on = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
			
			for ( int y = ( interlace_on && interlace_first_field ) ? 0 : 1; y < height; y += ( interlace_on ? 2 : 1 ) ) 
			{
				uint8_t *p = pixels + y * width * 3;

				for ( int x = 0; x < width; x ++ )
				{
					uint8_t r = *( p );
					uint8_t g = *( p + 1 );
					uint8_t b = *( p + 2 );
					r = (uint8_t)( 0.299 * r + 0.587 * g + 0.114 * b );
					*p ++ = r;
					*p ++ = r;
					*p ++ = r;
				}

				if ( interlace_on && interlace_first_field )
					memcpy( p, p - width * 3, width * 3 );
				else if ( interlace_on && !interlace_first_field )
					memcpy( p - width * 6, p - width * 3, width * 3 );
			}

			temp = new uint8_t[ width * height * 3 ];
			uint8_t *dest = temp;
			uint8_t *hack = copy;

			for ( int y = 0; y < height; y ++ )
			{
				for ( int x = 0; x < width; x ++ )
				{
					int pix[ 3 ][ 3 ];

					pix[ 0 ][ 0 ] = GetPix( pixels, width, height, x - x_scatter, y - y_scatter );
					pix[ 0 ][ 1 ] = GetPix( pixels, width, height, x            , y - y_scatter );
					pix[ 0 ][ 2 ] = GetPix( pixels, width, height, x + x_scatter, y - y_scatter );
					pix[ 1 ][ 0 ] = GetPix( pixels, width, height, x - x_scatter, y             );
					pix[ 1 ][ 2 ] = GetPix( pixels, width, height, x + x_scatter, y             );
					pix[ 2 ][ 0 ] = GetPix( pixels, width, height, x - x_scatter, y + y_scatter );
					pix[ 2 ][ 1 ] = GetPix( pixels, width, height, x            , y + y_scatter );
					pix[ 2 ][ 2 ] = GetPix( pixels, width, height, x + x_scatter, y + y_scatter );

		      		double sum1 = (pix[2][0] - pix[0][0]) + 2*(pix[2][1] - pix[0][1]) + (pix[2][2] - pix[2][0]);
		      		double sum2 = (pix[0][2] - pix[0][0]) + 2*(pix[1][2] - pix[1][0]) + (pix[2][2] - pix[2][0]);
		      		double sum = sqrt( (long) sum1 * sum1 + (long) sum2 * sum2 );

					sum = (int)( sum * scale );
					if ( sum > 255 )
						sum = 255;

					if ( copy == NULL )
					{
						*dest ++ = (uint8_t)( 255 - sum );
						*dest ++ = (uint8_t)( 255 - sum );
						*dest ++ = (uint8_t)( 255 - sum );
					}
					else
					{
						*dest ++ = (uint8_t)( ( 255 - sum ) * ( 1 - mix ) + ( *hack ++ ) * mix );
						*dest ++ = (uint8_t)( ( 255 - sum ) * ( 1 - mix ) + ( *hack ++ ) * mix );
						*dest ++ = (uint8_t)( ( 255 - sum ) * ( 1 - mix ) + ( *hack ++ ) * mix );
					}
				}
			}

			memcpy( pixels, temp, width * height * 3 );
			delete [] temp;
			delete [] copy;
		}
		
		void AttachWidgets( GtkBin *bin ) 
		{
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}

		void DetachWidgets( GtkBin *bin ) 
		{
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}

		void InterpretWidgets( GtkBin *bin ) 
		{
		}
};

class TweenieEntry
	: public TransitionTimeEntry < TweenieEntry >
	, protected PixbufUtils
{
	public:
		double x;
		double y;
		double width;
		double height;
		double angle;
		double fade;
		double shear;

		bool scaled;
		bool interlace_on;
		bool interlace_first_field;
		uint8_t *luma;
		int luma_width;
		int luma_height;
		double luma_softness;
		double frame_delta;
		double merge;

		TweenieEntry()
			: x(50)
			, y(50)
			, width(50)
			, height(50)
			, angle(0)
			, fade(0)
			, shear(0)
			, luma(NULL)
		{
			SetEditable( false );
		}

		TweenieEntry( double position ) : x(50), y(50), width(50), height(50), angle(0), fade(0), shear(0), luma(NULL)
		{
			SetEditable( false );
			SetPosition( position );
		}

		TweenieEntry( double position, TweenieEntry entry )
		{
			SetPosition( position );
			SetEditable( false );
			x = entry.x;
			y = entry.y;
			width = entry.width;
			height = entry.height;
			angle = entry.angle;
			fade = entry.fade;
			shear = entry.shear;
			merge = entry.merge;
		}

		TweenieEntry *Get( double position, TweenieEntry *ante )
		{
			TweenieEntry *current = new TweenieEntry( );
			double r = ( position - GetPosition( ) ) / ( ante->GetPosition() - GetPosition( ) );
			current->SetEditable( false );
			current->SetPosition( position );
			current->x = x + ( ante->x - x ) * r;
			current->y = y + ( ante->y - y ) * r;
			current->width = width + ( ( ante->width - width ) * r );
			current->height = height + ( ( ante->height - height ) * r );
			current->angle = angle + ( ( ante->angle - angle ) * r );
			current->fade = fade + ( ( ante->fade - fade ) * r );
			current->shear = shear + ( ( ante->shear - shear ) * r );
// 			current->merge = merge + ( ( ante->merge - merge ) * r );
			return current;
		}

		void Composite( uint8_t *dest, int width, int height, uint8_t *src, double x,
			double y, int src_width, int src_height, double angle, bool scaled, double merge, double fade )
		{
			// Set up the affine transform (rescaling could be done here, but quality sucks)
			AffineTransform affine;
			affine.Shear( shear / 100 );
			affine.Rotate( angle );

			int origin_x = (int)( width * x / 100 );
			int origin_y = (int)( height * y / 100 );

			// Rescale the luma if it exists
			uint8_t *scaled_luma = NULL;

			if ( luma != NULL )
			{
				SetScale( SCALE_FULL );
				GdkPixbuf *input = gdk_pixbuf_new_from_data( luma, GDK_COLORSPACE_RGB, FALSE, 8, luma_width, luma_height, ( luma_width * 3 ), NULL, NULL );
				scaled_luma = new uint8_t[ src_width * src_height * 3 ];
				ScalePixbuf( input, scaled_luma, src_width, src_height );
				gdk_pixbuf_unref( input );
			}
			else
			{
				scaled_luma = new uint8_t[ src_width * src_height * 3 ];
				memset( scaled_luma, 0, src_width * src_height * 3 );
			}

			// could probably calculate these a bit more accurately...
			int max_height = (int)sqrt( 2 * ( src_height > src_width ? src_height * src_height : src_width * src_width ) ) / 2;
			int max_width = max_height;

			//if ( shear != 0 )
			{
				max_height = height / 2;
				max_width = width / 2;
			}

			for( int field = 0; field < (interlace_on ? 2 : 1); ++field )
			{
				// Offset the position based on which field we're looking at ...
				const double field_position = merge + ((interlace_first_field ? 1 - field : field) * frame_delta * 0.5);
	
				// Scale the position so that the entire wipe will be complete (including the "soft" edge) by the time we finish ...
				const double adjusted_position = kino::lerp(0.0, 1.0 + luma_softness, field_position);
	
				for ( int iy = 0 - max_height + field; iy < max_height; iy += (interlace_on ? 2 : 1) )
				{
					int real_y = iy + origin_y;
	
					if ( real_y >= 0 && real_y < height )
					{
						uint8_t *dest_row = dest + real_y * width * 3 + origin_x * 3 - max_width * 3;
	
						for ( int ix = 0 - max_width; ix < max_width; ix ++ )
						{
							int map_x = (int)( src_width / 2 + affine.MapX( ix, iy ) );
							int map_y = (int)( src_height / 2 + affine.MapY( ix, iy ) );
	
							int real_x = origin_x + ix;
	
							if ( real_x >= 0 && real_x < width && map_x >= 0 && map_y >= 0 && map_x < src_width && map_y < src_height )
							{
								uint8_t *pixel;
	
								if ( scaled )
									pixel = src + map_y * src_width * 3 + map_x * 3;
								else
									pixel = src + real_y * width * 3 + real_x * 3;
	
								uint8_t *luma_pixel = scaled_luma + map_y * src_width * 3 + map_x * 3;
								double sample = (double)*luma_pixel / 255;
								const double mix = ( luma ? kino::smoothstep( sample, sample + luma_softness, adjusted_position ) : 1.0 ) * ( 1.0 - fade );
	
								for ( int j = 0; j < 3; ++j )
								{
									*dest_row = kino::lerp( *dest_row, *pixel++, mix );
									++dest_row;
								}
							}
							else
							{
								dest_row += 3;
							}
						}
					}
				}
			}
			delete scaled_luma;
		}

		void RenderFinal( uint8_t *background, uint8_t *foreground, int width, int height )
		{
			GdkPixbuf *i1 = gdk_pixbuf_new_from_data( foreground, GDK_COLORSPACE_RGB, FALSE, 8, width, height, ( 3 * width ), NULL, NULL );
			int new_width = (int)( width * this->width / 100 );
			int new_height = (int)( height * this->height / 100 );

			if ( new_width > 1 && new_height > 1 )
			{
				SetScale( SCALE_NONE );
				if ( scaled )
				{
					GdkPixbuf *im = gdk_pixbuf_scale_simple( i1, new_width, new_height, GDK_INTERP_HYPER );
					uint8_t *temp_image = new uint8_t[ new_width * new_height * 3 ];
					ScalePixbuf( im, temp_image, new_width, new_height );
					Composite( background, width, height, temp_image, x, y, new_width, new_height, angle, true, merge, fade / 100 );
					delete temp_image;
					gdk_pixbuf_unref( im );
				}
				else
				{
					uint8_t *temp_image = new uint8_t[ new_width * new_height * 3 ];
					ScalePixbuf( i1, temp_image, new_width, new_height );
					Composite( background, width, height, temp_image, x, y, new_width, new_height, angle, true, merge, fade / 100 );
					delete temp_image;
				}
			}

			gdk_pixbuf_unref( i1 );
		}
};

class Tweenies
	: public GDKImageTransition
	, private KeyFrameControllerClient
	, protected PixbufUtils
{
	private:
		GtkWidget *window;
		KeyFrameController *controller;
		bool gui_active;
		string luma_path;
		string luma;
		uint8_t *luma_image;
		double luma_softness;
		int last_predefine;
		bool scaled;
		bool reversed;
		int luma_width;
		int luma_height;
		bool interlace_on;
		bool interlace_first_field;
		TimeMap < TweenieEntry > time_map;

	public:
		Tweenies( )
			: gui_active( true )
			, luma_path(std::string( DATADIR "/" PACKAGE "/lumas" ))
			, luma( "" )
			, luma_image( 0 )
			, luma_softness( 0.20 )
			, last_predefine( 0 )
			, scaled( true )
			, reversed( false )
			, interlace_on( true )
			, interlace_first_field( true )
		{
			// Define the widget
			window = glade_xml_get_widget( kinoplus_glade, "window_tweenies" );
			GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_x" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_y" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_w" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_h" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_angle" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_fade" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_shear" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( TweeniesRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_rescale" );
			g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), NULL );
			widget = glade_xml_get_widget( kinoplus_glade, "combobox_predefines" );
			gtk_combo_box_set_active( GTK_COMBO_BOX( widget ), 0 );
			g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), NULL );
			widget = glade_xml_get_widget( kinoplus_glade, "filechooserbutton" );
			gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( widget ), luma_path.c_str() );
			g_signal_connect( G_OBJECT( widget ), "file-activated", G_CALLBACK( Repaint ), 0 );

			// Define the default animation
			TweenieEntry *entry = time_map.SetEditable( 0, true );
			entry->x = 50;
			entry->y = 50;
			entry->width = 1;
			entry->height = 1;
			entry->fade = 0;
			time_map.FinishedWith( entry );

			entry = time_map.SetEditable( 0.999999, true );
			entry->x = 50;
			entry->y = 50;
			entry->width = 100;
			entry->height = 100;
			entry->fade = 0;
			time_map.FinishedWith( entry );
		}

		virtual ~Tweenies( ) 
		{
			delete [] luma_image;
			gtk_widget_destroy( window );
		}

		char *GetDescription( ) const 
		{
			return _("Composite");
		}

		void Refresh(void)
		{
			if ( gui_active )
			{
				gui_active = false;
				GetSelectedFramesForFX().Repaint();
				gui_active = true;
			}
		}

		static void TweeniesRepaint( GtkWidget* widget, gpointer user_data )
		{
			Tweenies* object = static_cast< Tweenies* >( user_data );
			object->Refresh();
		}

		void GetFrame( uint8_t *io, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
		{
			uint8_t *in = mesh;
			uint8_t *out = io;

			GtkWidget* widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_rescale" );
			scaled = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
			widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_tweenies_interlace" );
			interlace_on = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_softness" );
			luma_softness = gtk_spin_button_get_value_as_float( GTK_SPIN_BUTTON( widget ) ) / 100;
			
			// Reverse for fly-out
			if ( reverse != reversed )
			{
				reversed = reverse;
				time_map.Invert();
			}
			if ( reverse )
			{
				in = io;
				out = mesh;
			}

			TweenieEntry *entry = time_map.Get( position );
			ChangeController( entry );
			if ( entry->IsEditable() )
			{
				GtkWidget* widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_x" );
				entry->x = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_y" );
				entry->y = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_w" );
				entry->width = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_h" );
				entry->height = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_angle" );
				entry->angle = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_fade" );
				entry->fade = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_shear" );
				entry->shear = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
			}
			entry->merge = reverse ? 1.0 - position : position;
			entry->luma = luma_image;
			entry->luma_width = luma_width;
			entry->luma_height = luma_height;
			entry->luma_softness = luma_softness;
			entry->frame_delta = frame_delta;
			entry->scaled = scaled;
			entry->interlace_on = interlace_on;
			entry->interlace_first_field = interlace_first_field;
			entry->RenderFinal( out, in, width, height );
			time_map.FinishedWith( entry );

			// Correct for fly out
			if ( reverse )
				memcpy( io, mesh, width * height * 3 );
		}
		
		void AttachWidgets( GtkBin *bin ) 
		{
			controller = GetKeyFrameController( this );
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}

		void DetachWidgets( GtkBin *bin ) 
		{
			controller = 0;
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}

		void InterpretWidgets( GtkBin *bin )
		{
			GtkWidget* widget = glade_xml_get_widget( kinoplus_glade, "filechooserbutton" );
			gchar* filename = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER( widget ) );
			string filenameString = ( filename == NULL ) ? "" : filename;
			if ( luma != filenameString )
			{
				luma = filenameString;
				delete luma_image;
				luma_image = 0;
				if ( filename )
				{
					GError *err = NULL;
					GdkPixbuf *pix = gdk_pixbuf_new_from_file( filename, &err );
					if ( pix )
					{
						luma_path = filenameString;
						luma_width = gdk_pixbuf_get_width( pix );
						luma_height = gdk_pixbuf_get_height( pix );
						luma_image = new uint8_t[ luma_width * luma_height * 3 ];
						ScalePixbuf( pix, luma_image, luma_width, luma_height );
						gdk_pixbuf_unref( pix );
					}
				}
			}
			OnPredefineChange();
		}

		void ChangeController( TweenieEntry *entry )
		{
			if ( gui_active )
			{
				frame_type type = FRAME;
				if ( entry->GetPosition() == 0 )
					type = LOCKED_KEY;
				else if ( entry->IsEditable() )
					type = KEY;
	
				gui_active = false;
				bool isPreviewing = GetSelectedFramesForFX().IsPreviewing();
				if ( isPreviewing )
					gdk_threads_enter();
				controller->ShowCurrentStatus( entry->GetPosition(), type, entry->GetPosition() > time_map.GetFirst(), entry->GetPosition() < time_map.GetLast() );
				GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_x" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->x );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_y" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->y );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_w" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->width );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_tweenies_h" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->height );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_angle" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->angle );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_fade" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->fade );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_shear" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->shear );
				widget = glade_xml_get_widget( kinoplus_glade, "frame_tweenies_key_input" );
				gtk_widget_set_sensitive( widget, entry->IsEditable() );
				if ( isPreviewing )
					gdk_threads_leave();
				gui_active = true;
			}
		}

		void OnControllerKeyChanged( double position, bool value )
		{
			TweenieEntry *entry;
			if ( position > 0 )
				entry = time_map.SetEditable( position, value );
			else
				entry = time_map.Get( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnControllerPrevKey( double position )
		{
			TweenieEntry *entry = time_map.GotoPreviousKey( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnControllerNextKey( double position )
		{
			TweenieEntry *entry = time_map.GotoNextKey( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnPredefineChange( )
		{
			int direction = gtk_combo_box_get_active( GTK_COMBO_BOX( glade_xml_get_widget( kinoplus_glade, "combobox_predefines" ) ) );

			if ( direction == last_predefine )
				return;
			last_predefine = direction;

			// Wipe any previous definition
			time_map.Clear( );

			// Obtain first and last entries as keys
			TweenieEntry *first_entry = time_map.SetEditable( 0, true );
			TweenieEntry *last_entry = time_map.SetEditable( 0.999999, true );

			// Define the default last entry
			last_entry->x = 50;
			last_entry->y = 50;
			last_entry->width = 100;
			last_entry->height = 100;
			last_entry->fade = 0;

			// Define the first according to the selected direction
			if ( direction == 0 )
			{
				first_entry->x = 50;
				first_entry->y = 50;
				first_entry->width = 1;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 1 )
			{
				first_entry->x = 0;
				first_entry->y = 50;
				first_entry->width = 1;
				first_entry->height = 100;
				first_entry->fade = 0;
			}
			else if ( direction == 2 )
			{
				first_entry->x = 100;
				first_entry->y = 50;
				first_entry->width = 1;
				first_entry->height = 100;
				first_entry->fade = 0;
			}
			else if ( direction == 3 )
			{
				first_entry->x = 50;
				first_entry->y = 50;
				first_entry->width = 1;
				first_entry->height = 100;
				first_entry->fade = 0;
			}
			else if ( direction == 4 )
			{
				first_entry->x = 50;
				first_entry->y = 0;
				first_entry->width = 100;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 5 )
			{
				first_entry->x = 50;
				first_entry->y = 100;
				first_entry->width = 100;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 6 )
			{
				first_entry->x = 50;
				first_entry->y = 50;
				first_entry->width = 100;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 7 )
			{
				first_entry->x = 0;
				first_entry->y = 0;
				first_entry->width = 1;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 8 )
			{
				first_entry->x = 100;
				first_entry->y = 0;
				first_entry->width = 1;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 9 )
			{
				first_entry->x = 0;
				first_entry->y = 100;
				first_entry->width = 1;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			else if ( direction == 10 )
			{
				first_entry->x = 100;
				first_entry->y = 100;
				first_entry->width = 1;
				first_entry->height = 1;
				first_entry->fade = 0;
			}
			time_map.FinishedWith( first_entry );
			time_map.FinishedWith( last_entry );
			reversed = false;
			Repaint();
		}
};

class PanZoomEntry
	: public FilterTimeEntry < PanZoomEntry >
	, public PixbufUtils
{
	public:
		double x;
		double y;
		double width;
		double height;
		bool interlace_on;
		bool interlace_first_field;

		PanZoomEntry() : x(50), y(50), width(50), height(50)
		{
			SetEditable( false );
		}

		PanZoomEntry( double position ) : x(50), y(50), width(50), height(50)
		{
			SetEditable( false );
			SetPosition( position );
		}

		PanZoomEntry( double position, PanZoomEntry &entry )
		{
			SetPosition( position );
			SetEditable( false );
			x = entry.x;
			y = entry.y;
			width = entry.width;
			height = entry.height;
		}

		PanZoomEntry *Get( double position, PanZoomEntry *ante )
		{
			PanZoomEntry *current = new PanZoomEntry( );
			double r = ( position - GetPosition( ) ) / ( ante->GetPosition() - GetPosition( ) );
			current->SetEditable( false );
			current->SetPosition( position );
			current->x = x + ( ante->x - x ) * r;
			current->y = y + ( ante->y - y ) * r;
			current->width = width + ( ( ante->width - width ) * r );
			current->height = height + ( ( ante->height - height ) * r );
			return current;
		}

		void RenderFinal( uint8_t *image, int width, int height )
		{
			int x = (int)( ( this->x * width ) / 100 );
			int y = (int)( ( this->y * height ) / 100 );
			int cut_width = (int)( ( this->width * width ) / 100 );
			int cut_height = (int)( ( this->height * height ) / 100 );
			int lower_x = x - cut_width / 2;
			int lower_y = y - cut_height / 2;
			int upper_x = x + cut_width / 2;
			int upper_y = y + cut_height / 2;

			if ( lower_x < 0 )
			{
				cut_width = cut_width - ( 0 - lower_x );
				lower_x = 0;
			}
			if ( lower_y < 0 )
			{
				cut_height = cut_height - ( 0 - lower_y );
				lower_y = 0;
			}
			if ( upper_x > width )
			{
				cut_width = cut_width - ( upper_x - width );
				upper_x = width;
			}
			if ( upper_y > height )
			{
				cut_height = cut_height - ( upper_y - height );
				upper_y = height;
			}

			if ( interlace_on )
			{
				for ( int row = interlace_first_field ? 0 : 1; row < height; row += 2 )
				{
					if ( interlace_first_field )
						memcpy( image + ( row + 1 ) * width * 3, image + row * width * 3, width * 3 );
					else
						memcpy( image + ( row - 1 ) * width * 3, image + row * width * 3, width * 3 );
				}
			}

			SetScale( SCALE_FULL );
			ZoomAndScaleRGB( image, width, height, upper_x, upper_y, lower_x, lower_y );
		}
};

class PanZoom
	: public GDKImageFilter
	, private KeyFrameControllerClient
{
	private:
		GtkWidget *window;
		KeyFrameController *controller;
		bool gui_active;
		bool reverse;
		bool interlace_on;
		bool interlace_first_field;
		TimeMap < PanZoomEntry > time_map;

	public:
		PanZoom( )
			: gui_active( true )
			, reverse( false )
			, interlace_on( false )
			, interlace_first_field( false )
		{
			// Create the window
			window = glade_xml_get_widget( kinoplus_glade, "window_pan_zoom" );
			GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_panzoom_reverse" );
			g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), NULL );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_x" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( PanZoomRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_y" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( PanZoomRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_w" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( PanZoomRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_h" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( PanZoomRepaint ), this );
			widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_panzoom_interlace" );
			g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), NULL );

			// Define the default animation
			PanZoomEntry *entry = time_map.SetEditable( 0, true );
			entry->x = 50;
			entry->y = 50;
			entry->width = 50;
			entry->height = 50;
			time_map.FinishedWith( entry );

			entry = time_map.SetEditable( 0.999999, true );
			entry->x = 50;
			entry->y = 50;
			entry->width = 100;
			entry->height = 100;
			time_map.FinishedWith( entry );
		}

		virtual ~PanZoom( ) 
		{
			gtk_widget_destroy( window );
		}

		char *GetDescription( ) const 
		{
			return _("Pan and Zoom");
		}

		void Refresh(void)
		{
			if ( gui_active )
			{
				gui_active = false;
				GetSelectedFramesForFX().Repaint();
				gui_active = true;
			}
		}

		static void PanZoomRepaint( GtkWidget* widget, gpointer user_data )
		{
			PanZoom* object = static_cast< PanZoom* >( user_data );
			object->Refresh();
		}

		void FilterFrame( uint8_t *io, int width, int height, double position, double frame_delta ) 
		{
			GtkWidget* widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_panzoom_interlace" );
			interlace_on = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
			widget = glade_xml_get_widget( kinoplus_glade, "checkbutton_panzoom_reverse" );
			if ( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) ) != reverse )
			{
				reverse = !reverse;
				time_map.Invert();
			}
			
			PanZoomEntry *entry = time_map.Get( position );
			ChangeController( entry );
			if ( entry->IsEditable() )
			{
				GtkWidget* widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_x" );
				entry->x = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_y" );
				entry->y = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_w" );
				entry->width = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_h" );
				entry->height = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );
			}
			entry->interlace_on = interlace_on;
			entry->interlace_first_field = interlace_first_field;
			entry->RenderFinal( io, width, height );
			time_map.FinishedWith( entry );
		}
		
		void AttachWidgets( GtkBin *bin ) 
		{
			// Put the key frame controller widget in place
			controller = GetKeyFrameController( this );
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}

		void DetachWidgets( GtkBin *bin ) 
		{
			controller = 0;
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}

		void InterpretWidgets( GtkBin *bin )
		{
		}

		void ChangeController( PanZoomEntry *entry )
		{
			if ( gui_active )
			{
				frame_type type = FRAME;
				if ( entry->GetPosition() == 0 )
					type = LOCKED_KEY;
				else if ( entry->IsEditable() )
					type = KEY;
	
				gui_active = false;
				bool isPreviewing = GetSelectedFramesForFX().IsPreviewing();
				if ( isPreviewing )
					gdk_threads_enter();
				controller->ShowCurrentStatus( entry->GetPosition(), type, entry->GetPosition() > time_map.GetFirst(), entry->GetPosition() < time_map.GetLast() );
				GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "frame_panzoom_key_input" );
				gtk_widget_set_sensitive( widget, entry->IsEditable() );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_x" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->x );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_y" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->y );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_w" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->width );
				widget = glade_xml_get_widget( kinoplus_glade, "spinbutton_panzoom_h" );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), entry->height );
				if ( isPreviewing )
					gdk_threads_leave();
				gui_active = true;
			}
		}

		void OnControllerKeyChanged( double position, bool value )
		{
			PanZoomEntry *entry;
			if ( position > 0 )
				entry = time_map.SetEditable( position, value );
			else
				entry = time_map.Get( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnControllerPrevKey( double position )
		{
			PanZoomEntry *entry = time_map.GotoPreviousKey( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnControllerNextKey( double position )
		{
			PanZoomEntry *entry = time_map.GotoNextKey( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}
};


class ColourAverage
	: public GDKImageFilter
{
	private:
		GtkWidget *window;
		uint8_t *temp;
		int scale;

	public:
		ColourAverage( )
			: scale( 2 )
		{
			window = glade_xml_get_widget( kinoplus_glade, "window_colour_average" );
			GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "scale_colour_average" );
			g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), NULL );
		}

		virtual ~ColourAverage( ) 
		{
			gtk_widget_destroy( window );
		}

		char *GetDescription( ) const 
		{
			return _("Colour Average");
		}

		void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta ) 
		{
			scale = int( gtk_range_get_value( GTK_RANGE( glade_xml_get_widget( kinoplus_glade, "scale_colour_average" ) ) ) / 100 * 255 + 0.5 );
// 			if ( !scale ) return;
			for ( int h = 0; h < height; h += 1 )
			{
				for ( int w = 0; w < width; w += 1 )
				{
					uint8_t *p = pixels + h * width * 3 + w * 3;
					*p = ( *p / scale ) * scale + scale / 2;
					++p;
					*p = ( *p / scale ) * scale + scale / 2;
					++p;
					*p = ( *p / scale ) * scale + scale / 2;
					++p;
				}
			}
		}
		
		void AttachWidgets( GtkBin *bin ) 
		{
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}

		void DetachWidgets( GtkBin *bin ) 
		{
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}

		void InterpretWidgets( GtkBin *bin ) 
		{
		}
};


class LevelsEntry
	: public FilterTimeEntry < LevelsEntry >
{
	public:
		double contrast;
		double brightness;
		double gamma;
		double hue;
		double saturation;
		double value;
		double temperature;
		double green;
		uint8_t transform[256];

		LevelsEntry()
			: contrast( 0 )
			, brightness( 0 )
			, gamma( 1.0 )
			, hue( 0 )
			, saturation( 0 )
			, value( 0 )
			, temperature( 4750 )
			, green( 1.2 )
		{
			SetEditable( false );
		}

		LevelsEntry( double position )
		{
			LevelsEntry();
			SetEditable( false );
			SetPosition( position );
		}

		LevelsEntry( double position, LevelsEntry &entry )
		{
			SetPosition( position );
			SetEditable( false );
			contrast = entry.contrast;
			brightness = entry.brightness;
			gamma = entry.gamma;
			hue = entry.hue;
			saturation = entry.saturation;
			value = entry.value;
			temperature = entry.temperature;
			green = entry.green;
		}

		LevelsEntry *Get( double position, LevelsEntry *ante )
		{
			LevelsEntry *current = new LevelsEntry( );
			current->SetEditable( false );
			current->SetPosition( position );
			current->brightness = brightness;
			current->contrast = contrast;
			current->gamma = gamma;
			current->hue = hue;
			current->saturation = saturation;
			current->value = value;
			current->temperature = temperature;
			current->green = green;
			return current;
		}
		
#define MIDDLE (159)

		void buildTransform()
		{
			// Brightness & gamma
			double exp = 1.0 / gamma;
			for ( int i = 0; i < 256; ++i )
			{
				double value = CLAMP( brightness + i, 0, 255 );
				transform[i] = uint8_t( pow( value / 255, exp ) * 255 );
			}

			// Contrast
			int offset, limit_b, limit_t, idx_b, idx_t;
			double col, step;
			for ( int i=255; i>=0; --i )
			{
				if ( 0 > contrast )
				{
					if ( MIDDLE > transform[i] )
					{
						offset = ( MIDDLE - transform[i] ) * int( contrast ) / 128;
						if ( MIDDLE < transform[i] - offset )
							transform[i] = MIDDLE;
						else
							transform[i] -= offset;
					}
					else
					{
						offset = ( transform[i] - MIDDLE ) * int( contrast ) / 128;
						if ( MIDDLE > transform[i] + offset )
							transform[i] = MIDDLE;
						else
							transform[i] += offset;
					}
				}
				else
				{
					limit_b = int( contrast ) * MIDDLE / 128;
					for ( idx_b=0; idx_b<256; ++idx_b )
					{
						if ( limit_b > transform[idx_b] )
							transform[idx_b] = 0;
						else
							break;
					}
					limit_t = int( contrast ) * 128 / MIDDLE;
					for ( idx_t=0; idx_t<256; ++idx_t )
					{
						if ( 255 < limit_t + transform[idx_t] )
							transform[idx_t] = 255;
						else
							break;
					}
					step = 256.0 / ( ( double ) ( 256 - limit_b + limit_t ) );
					col = 0.0;
					for ( int i=idx_b; i<idx_t; ++i )
					{
						if ( transform[i] >= limit_b || transform[i] < 256 - limit_t )
						{
							col = ( double ) ( transform[i] - limit_b ) * step + 0.5;
							transform[i] = ( col > 255.0 ) ? 255 : ( uint8_t ) col;
						}
					}
				}
			}
		}

		void setRGBmult( float& mr, float& mg, float& mb )
		{
			int   t;
			float mi;
		
			if ( temperature/1000.0 > 7.0 )
				temperature = 7000.0;
			
			t = int( ( temperature / 1000.0 ) * 100.0 - 200.0 );
			mr  = 1.0 / bbWB[t][0];
			mg  = 1.0 / bbWB[t][1];
			mb  = 1.0 / bbWB[t][2];
			mg *= green;
			
			// Normalize to at least 1.0, so we are not dimming colors only bumping.
			mi = MIN( mr, mg );
			mi = MIN( mi, mb );
			mr /= mi;
			mg /= mi;
			mb /= mi;
		}

		static inline double clamp( double x, double low, double high )
		{
			double ret;
			(x > high) ? ret = high : ((x < low) ? ret = low : ret = x);
			return ret;
		}

		static inline uint8_t clamp( float pixel )
		{
			return uint8_t( clamp( pixel, 0, 255 ) );
		}

		void RenderFinal( uint8_t *pixels, int width, int height )
		{
			// Prepare brightness, contrast, gamma
			buildTransform();

			// Prepare white balance
			float mr, mg, mb;
			setRGBmult( mr, mg, mb );

			// Prepare HSV
			double h = hue / 100;
			double s = saturation / 100;
			double v = value / 100;

			// For each pixel
			const kino::basic_rgb<uint8_t>* const end = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels) + width * height;
			for(kino::basic_rgb<uint8_t>* pixel = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels); pixel != end; ++pixel)
			{
				// Brightness, Contrast, Gamma, While balance
				pixel->red = transform[ clamp( mr * pixel->red ) ];
				pixel->green = transform[ clamp( mg * pixel->green ) ];
				pixel->blue = transform[ clamp( mb * pixel->blue ) ];

				// HSV
				kino::basic_hsv hsv_pixel(*pixel);
				hsv_pixel.hue += h * 360;
				while ( hsv_pixel.hue < 0 )
					hsv_pixel.hue += 360;
				while ( hsv_pixel.hue >= 360 )
					hsv_pixel.hue -= 360;
				hsv_pixel.saturation = clamp( hsv_pixel.saturation + s, kino::basic_hsv::sample_traits::minimum(), kino::basic_hsv::sample_traits::maximum() );
				hsv_pixel.value = clamp( hsv_pixel.value + v, kino::basic_hsv::sample_traits::minimum(), kino::basic_hsv::sample_traits::maximum() );

				*pixel = hsv_pixel;
			}
		}
};

class Levels
	: public GDKImageFilter
	, private KeyFrameControllerClient
{
	private:
		KeyFrameController* controller;
		TimeMap< LevelsEntry > time_map;
		bool gui_active;
		GtkWidget* window;
		GtkWidget* brightnessScale;
		GtkWidget* brightnessSpinner;
		GtkWidget* contrastScale;
		GtkWidget* contrastSpinner;
		GtkWidget* gammaScale;
		GtkWidget* gammaSpinner;
		GtkWidget* hueScale;
		GtkWidget* hueSpinner;
		GtkWidget* saturationScale;
		GtkWidget* saturationSpinner;
		GtkWidget* valueScale;
		GtkWidget* valueSpinner;
		GtkWidget* temperatureSpinner;
		GtkWidget* greenScale;
		GtkWidget* greenSpinner;
		GtkWidget* colourPicker;

	public:
		Levels()
			: gui_active( true )
		{
			window = glade_xml_get_widget( kinoplus_glade, "window_levels" );
			g_signal_connect( G_OBJECT( glade_xml_get_widget( kinoplus_glade, "button_levels_reset" ) ), "clicked", G_CALLBACK( onResetClickedProxy ), this );
			brightnessScale = glade_xml_get_widget( kinoplus_glade, "hscale_brightness" );
			contrastScale = glade_xml_get_widget( kinoplus_glade, "hscale_contrast" );
			gammaScale = glade_xml_get_widget( kinoplus_glade, "hscale_gamma" );
			hueScale = glade_xml_get_widget( kinoplus_glade, "hscale_hue" );
			saturationScale = glade_xml_get_widget( kinoplus_glade, "hscale_saturation" );
			valueScale = glade_xml_get_widget( kinoplus_glade, "hscale_value" );
			greenScale = glade_xml_get_widget( kinoplus_glade, "hscale_green" );
			brightnessSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_brightness" );
			contrastSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_contrast" );
			gammaSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_gamma" );
			hueSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_hue" );
			saturationSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_saturation" );
			valueSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_value" );
			temperatureSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_temperature" );
			greenSpinner = glade_xml_get_widget( kinoplus_glade, "spinbutton_green" );
			g_signal_connect( G_OBJECT( brightnessSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( contrastSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( gammaSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( hueSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( saturationSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( valueSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( temperatureSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( greenSpinner ), "value-changed", G_CALLBACK( onSpinnerUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( brightnessScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( contrastScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( gammaScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( hueScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( saturationScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( valueScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			g_signal_connect( G_OBJECT( greenScale ), "value-changed", G_CALLBACK( onScaleUpdatedProxy ), this );
			colourPicker = glade_xml_get_widget( kinoplus_glade, "colorbutton_levels" );
			g_signal_connect( G_OBJECT( colourPicker ), "color-set", G_CALLBACK( onColorPickedProxy ), this );
			g_signal_connect( G_OBJECT( colourPicker ), "clicked", G_CALLBACK( onColorClickedProxy ), this );

			GdkColor c;
			c.red = c.green = c.blue = 0xffff;
			gtk_color_button_set_color( GTK_COLOR_BUTTON( colourPicker ), &c );
	
			// Define the default animation
			LevelsEntry *entry = time_map.SetEditable( 0, true );
			entry->brightness = 0;
			entry->contrast = 0;
			entry->gamma = 1.0;
			entry->hue = 0;
			entry->saturation = 0;
			entry->value = 0;
			entry->temperature = 4750;
			entry->green = 1.2;
			time_map.FinishedWith( entry );
		}

		virtual ~Levels()
		{
			gtk_widget_destroy(window);
		}

		char *GetDescription( ) const
		{
			return _("Levels");
		}

		void onResetClicked()
		{
			if ( gui_active )
			{
				gui_active = false;
				gtk_range_set_value( GTK_RANGE( brightnessScale ), 0 );
				gtk_range_set_value( GTK_RANGE( contrastScale ), 0 );
				gtk_range_set_value( GTK_RANGE( gammaScale ), 1.0 );
				gtk_range_set_value( GTK_RANGE( hueScale ), 0 );
				gtk_range_set_value( GTK_RANGE( saturationScale ), 0 );
				gtk_range_set_value( GTK_RANGE( valueScale ), 0 );
				gtk_range_set_value( GTK_RANGE( greenScale ), 1.2 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( brightnessSpinner ), 0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( contrastSpinner ), 0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( gammaSpinner ), 1.0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( hueSpinner ), 0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( saturationSpinner ), 0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( valueSpinner ), 0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( temperatureSpinner ), 4750.0 );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( greenSpinner ), 1.2 );
				GdkColor c;
				c.red = c.green = c.blue = 0xffff;
				gtk_color_button_set_color( GTK_COLOR_BUTTON( colourPicker ), &c );
				Repaint();
				gui_active = true;
			}
		}

		static void onResetClickedProxy( GtkWidget* widget, gpointer user_data )
		{
			Levels* object = static_cast< Levels* >( user_data );
			object->onResetClicked();
		}
		
		void onSpinnerUpdated()
		{
			if ( gui_active )
			{
				gui_active = false;
				gtk_range_set_value( GTK_RANGE( brightnessScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( brightnessSpinner ) ) );
				gtk_range_set_value( GTK_RANGE( contrastScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( contrastSpinner ) ) );
				gtk_range_set_value( GTK_RANGE( gammaScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( gammaSpinner ) ) );
				gtk_range_set_value( GTK_RANGE( hueScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( hueSpinner ) ) );
				gtk_range_set_value( GTK_RANGE( saturationScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( saturationSpinner ) ) );
				gtk_range_set_value( GTK_RANGE( valueScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( valueSpinner ) ) );
				gtk_range_set_value( GTK_RANGE( greenScale ), gtk_spin_button_get_value( GTK_SPIN_BUTTON( greenSpinner ) ) );
				Repaint();
				gui_active = true;
			}
		}

		static void onSpinnerUpdatedProxy( GtkWidget* widget, gpointer user_data )
		{
			Levels* object = static_cast< Levels* >( user_data );
			object->onSpinnerUpdated();
		}
		
		void onColorPicked()
		{
			if ( gui_active )
			{
				gui_active = false;
				GdkColor tc;
				gtk_color_button_get_color( GTK_COLOR_BUTTON( colourPicker ), &tc );

				// Calculate Temperature and Green component from color picked.
				register int l, r, m;
				double sR, sG, sB, mRB, t;
				
				t   = MAX( MAX( tc.red, tc.green ), tc.blue );
				if ( t > 0 )
				{
					sR  = tc.red   / t;
					sG  = tc.green / t;
					sB  = tc.blue  / t;
					mRB = sR / sB;
					
					l = 0;
					r = sizeof( bbWB )/( sizeof( float ) * 3 );
					m = ( r + l ) / 2;
					
					for ( l = 0, r = sizeof( bbWB )/( sizeof( float ) * 3 ), m = ( l + r ) / 2 ; r - l > 1 ; m = ( l + r ) / 2 )
					{
						if ( bbWB[m][0] / bbWB[m][2] > mRB )
							l = m;
						else
							r = m;
					}
					t = m * 10.0 + 2000.0;
					double green = ( bbWB[m][1] / bbWB[m][0] ) / ( sG / sR );
// 					std::cerr << "whitebalance m " << m << " temp " << t << " green " << green << std::endl;
	
					gtk_spin_button_set_value( GTK_SPIN_BUTTON( temperatureSpinner ), t );
					gtk_spin_button_set_value( GTK_SPIN_BUTTON( greenSpinner ), green );
					gtk_range_set_value( GTK_RANGE( greenScale ), green );
					Repaint();
				}
				gui_active = true;
			}
		}

		static void onColorPickedProxy( GtkWidget* widget, gpointer user_data )
		{
			Levels* object = static_cast< Levels* >( user_data );
			object->onColorPicked();
		}
		
		void onColorClicked()
		{
			GdkColor c;
			c.red = c.green = c.blue = 0xffff;
			gtk_color_button_set_color( GTK_COLOR_BUTTON( colourPicker ), &c );
			onColorPicked();
		}

		static void onColorClickedProxy( GtkWidget* widget, gpointer user_data )
		{
			Levels* object = static_cast< Levels* >( user_data );
			object->onColorClicked();
		}
		
		void onScaleUpdated()
		{
			if ( gui_active )
			{
				gui_active = false;
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( brightnessSpinner ), gtk_range_get_value( GTK_RANGE( brightnessScale ) ) );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( contrastSpinner ), gtk_range_get_value( GTK_RANGE( contrastScale ) ) );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( gammaSpinner ), gtk_range_get_value( GTK_RANGE( gammaScale ) ) );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( hueSpinner ), gtk_range_get_value( GTK_RANGE( hueScale ) ) );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( saturationSpinner ), gtk_range_get_value( GTK_RANGE( saturationScale ) ) );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( valueSpinner ), gtk_range_get_value( GTK_RANGE( valueScale ) ) );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( greenSpinner ), gtk_range_get_value( GTK_RANGE( greenScale ) ) );
				Repaint();
				gui_active = true;
			}
		}

		static void onScaleUpdatedProxy( GtkWidget* widget, gpointer user_data )
		{
			Levels* object = static_cast< Levels* >( user_data );
			object->onScaleUpdated();
		}
		
		void FilterFrame(uint8_t *io, int width, int height, double position, double frame_delta)
		{
			LevelsEntry *entry = time_map.Get( position );
			ChangeController( entry );
			if ( entry->IsEditable() )
			{
				entry->brightness = gtk_spin_button_get_value( GTK_SPIN_BUTTON( brightnessSpinner ) );
				entry->contrast = gtk_spin_button_get_value( GTK_SPIN_BUTTON( contrastSpinner ) );
				entry->gamma = gtk_spin_button_get_value( GTK_SPIN_BUTTON( gammaSpinner ) );
				entry->hue = gtk_spin_button_get_value( GTK_SPIN_BUTTON( hueSpinner ) );
				entry->saturation = gtk_spin_button_get_value( GTK_SPIN_BUTTON( saturationSpinner ) );
				entry->value = gtk_spin_button_get_value( GTK_SPIN_BUTTON( valueSpinner ) );
				entry->temperature = gtk_spin_button_get_value( GTK_SPIN_BUTTON( temperatureSpinner ) );
				entry->green = gtk_spin_button_get_value( GTK_SPIN_BUTTON( greenSpinner ) );
			}
			entry->RenderFinal( io, width, height );
			time_map.FinishedWith( entry );
		}
		
		void AttachWidgets(GtkBin *bin)
		{
			controller = GetKeyFrameController( this );
			gtk_widget_reparent( ( GTK_BIN( window ) )->child, GTK_WIDGET( bin ) );
		}
		
		void DetachWidgets( GtkBin *bin ) 
		{
			controller = 0;
			gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( window ) );
		}
		
		void ChangeController( LevelsEntry *entry )
		{
			if ( gui_active )
			{
				frame_type type = FRAME;
				if ( entry->GetPosition() == 0 )
					type = LOCKED_KEY;
				else if ( entry->IsEditable() )
					type = KEY;
				bool isPreviewing = GetSelectedFramesForFX().IsPreviewing();
				if ( isPreviewing )
					gdk_threads_enter();
				gui_active = false;
				controller->ShowCurrentStatus( entry->GetPosition(), type, entry->GetPosition() > time_map.GetFirst(), entry->GetPosition() < time_map.GetLast() );
				GtkWidget *widget = glade_xml_get_widget( kinoplus_glade, "table_levels" );
				gtk_widget_set_sensitive( widget, entry->IsEditable() );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( brightnessSpinner ), entry->brightness );
				gtk_range_set_value( GTK_RANGE( brightnessScale ), entry->brightness );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( contrastSpinner ), entry->contrast );
				gtk_range_set_value( GTK_RANGE( contrastScale ), entry->contrast );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( gammaSpinner ), entry->gamma );
				gtk_range_set_value( GTK_RANGE( gammaScale ), entry->gamma );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( hueSpinner ), entry->hue );
				gtk_range_set_value( GTK_RANGE( hueScale ), entry->hue );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( saturationSpinner ), entry->saturation );
				gtk_range_set_value( GTK_RANGE( saturationScale ), entry->saturation );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( valueSpinner ), entry->value );
				gtk_range_set_value( GTK_RANGE( valueScale ), entry->value );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( temperatureSpinner ), entry->temperature );
				gtk_spin_button_set_value( GTK_SPIN_BUTTON( greenSpinner ), entry->green );
				gtk_range_set_value( GTK_RANGE( greenScale ), entry->green );
				if ( isPreviewing )
					gdk_threads_leave();
				gui_active = true;
			}
		}

		void OnControllerKeyChanged( double position, bool value )
		{
			LevelsEntry *entry;
			if ( position > 0 )
				entry = time_map.SetEditable( position, value );
			else
				entry = time_map.Get( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnControllerPrevKey( double position )
		{
			LevelsEntry *entry = time_map.GotoPreviousKey( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}

		void OnControllerNextKey( double position )
		{
			LevelsEntry *entry = time_map.GotoNextKey( position );
			ChangeController( entry );
			time_map.FinishedWith( entry );
		}
};


extern "C" {

GDKImageFilter *GetImageFilter( int index )
{
	switch( index )
	{
		case 0:
			return new ColourAverage();
		case 1:
			return new LineDraw();
		case 2:
			return new Jerker();
		case 3:
			return new Levels();
		case 4:
			return new PanZoom();
		case 5:
			return new Pixelate();
	}
	return NULL;
}

GDKImageTransition *GetImageTransition( int index )
{
	switch( index )
	{
		case 0:
			return new Tweenies();
		case 1:
			return new GDKImageTransitionAdapter( new ImageTransitionChromaKeyBlue() );
		case 2:
			return new GDKImageTransitionAdapter( new ImageTransitionChromaKeyGreen() );
	}
	return NULL;
}

}

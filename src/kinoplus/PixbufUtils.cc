/*
 * PixbufUtils.cc -- Frame/Kino oriented pixbuf wrapper for various image manipuations
 * Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <string>
using std::string;

#include <string.h>

#include "PixbufUtils.h"

bool PixbufUtils::ReadImageFile( string name, uint8_t *image, int width, int height )
{
	bool obtained = false;
	GError *err = NULL;
	GdkPixbuf *pix = gdk_pixbuf_new_from_file( name.c_str( ), &err );
	if ( pix != NULL )
	{
		obtained = ScalePixbuf( pix, image, width, height );
		gdk_pixbuf_unref( pix );
	}
	return obtained;
}

bool PixbufUtils::ZoomAndScaleRGB( uint8_t *image, int width, int height, int upper_x, int upper_y, int lower_x, int lower_y )
{
	GdkPixbuf *input = gdk_pixbuf_new_from_data( image, GDK_COLORSPACE_RGB, FALSE, 8, width, height, ( width * 3 ), NULL, NULL );
	GdkPixbuf *temp = gdk_pixbuf_new( GDK_COLORSPACE_RGB, FALSE, 8, upper_x - lower_x, upper_y - lower_y );
	gdk_pixbuf_copy_area( input, lower_x, lower_y, upper_x - lower_x, upper_y - lower_y, temp, 0, 0 );
	ScalePixbuf( temp, image, width, height );
	gdk_pixbuf_unref( temp );
	gdk_pixbuf_unref( input );
	return true;
}

bool PixbufUtils::ScalePixbuf( GdkPixbuf *pix, uint8_t *image, int width, int height )
{
	bool obtained = false;
	switch( GetScale( ) )
	{
		case SCALE_ASPECT_RATIO:
			obtained = ReadAspectFrame( image, width, height, pix );
			break;
		case SCALE_FULL:
			obtained = ReadScaledFrame( image, width, height, pix );
			break;
		case SCALE_NONE:
		default:
			obtained = ReadCroppedFrame( image, width, height, pix );
			break;
	}
	return obtained;
}

bool PixbufUtils::Composite( uint8_t *image, int width, int height, GdkPixbuf *pix )
{
	register uint8_t *p_src, *p_dest;

	int src_width = gdk_pixbuf_get_width( pix );
	int src_height = gdk_pixbuf_get_height( pix );
	int stride = gdk_pixbuf_get_rowstride( pix );

	int off_x = ( width - src_width ) / 2;
	int off_y = ( height - src_height ) / 2;

	p_dest = image + (((off_y * width) + off_x) * 3);
	p_src = gdk_pixbuf_get_pixels( pix );

	if ( !gdk_pixbuf_get_has_alpha( pix ) )
	{
		for ( int i = 0; i < src_height; i++ ) 
		{
			memcpy( p_dest, p_src, src_width * 3 );
			p_src += stride;
			p_dest += width * 3;
		}
	}
	else
	{
   		for ( int i = 0; i < src_height; i++ )
   		{
			uint8_t *p = p_src;
			uint8_t *q = p_dest;

			for ( int j = 0; j < src_width; j ++ )
			{
				uint8_t r = *p ++;
				uint8_t g = *p ++;
				uint8_t b = *p ++;
				uint8_t a = *p ++;
				double value = (double)a / 255.0;
				*q ++ = (uint8_t)( r * value );
				*q ++ = (uint8_t)( g * value );
				*q ++ = (uint8_t)( b * value );
			}

       		p_src += stride;
			p_dest += width * 3;
   		}
	}

	return true;
}

GdkPixbuf *PixbufUtils::ConvertRGBToPixbuf( uint8_t *image, int width, int height )
{
	return gdk_pixbuf_new_from_data( image, GDK_COLORSPACE_RGB, FALSE, 8, width, height, ( width * 3 ), NULL, NULL );
}

bool PixbufUtils::ReadScaledFrame( uint8_t *image, int width, int height, GdkPixbuf *pix )
{
	GdkPixbuf *im = gdk_pixbuf_scale_simple( pix, width, height, GDK_INTERP_HYPER );
	Composite( image, width, height, im );
	gdk_pixbuf_unref( im );
	return true;
}

void PixbufUtils::FillWithBackgroundColour( uint8_t *image, int width, int height, DV_RGB &rgb )
{
	DV_RGB *rgbs = ( DV_RGB * )image;
	for ( int i = 0; i < width * height; i ++ )
		rgbs[ i ] = rgb;
}

bool PixbufUtils::ReadAspectFrame( uint8_t *image, int width, int height, GdkPixbuf *pix )
{
	DV_RGB rgb;
	GetBackgroundColour( rgb );
	FillWithBackgroundColour( image, width, height, rgb );

	double ratioWidth = (double)width / (double)gdk_pixbuf_get_width( pix );
    double ratioHeight = (double)height / (double)gdk_pixbuf_get_height( pix );
	int imageWidth, imageHeight;

    if (ratioHeight < ratioWidth) 
	{
    	imageWidth = (int)( gdk_pixbuf_get_width( pix ) * ratioHeight );
       	imageHeight = (int)( gdk_pixbuf_get_height( pix ) * ratioHeight );
	} 
	else 
	{
   		imageWidth = (int)( gdk_pixbuf_get_width( pix ) * ratioWidth );
   		imageHeight = (int)( gdk_pixbuf_get_height( pix ) * ratioWidth );
   	}
	
	GdkPixbuf *im= gdk_pixbuf_scale_simple( pix, imageWidth, imageHeight, GDK_INTERP_HYPER );
	Composite( image, width, height, im );
	gdk_pixbuf_unref( im );

	return true;
}

bool PixbufUtils::ReadCroppedFrame( uint8_t *image, int width, int height, GdkPixbuf *pix )
{
	DV_RGB rgb;
	GetBackgroundColour( rgb );
	FillWithBackgroundColour( image, width, height, rgb );
	if ( gdk_pixbuf_get_width( pix ) > width || gdk_pixbuf_get_height( pix ) > height )
	{
		int real_width = gdk_pixbuf_get_width( pix );
		int real_height = gdk_pixbuf_get_height( pix );
		int temp_width = width < real_width ? width : real_width;
		int temp_height = height < real_height ? height : real_height;
		GdkPixbuf *temp = gdk_pixbuf_new( GDK_COLORSPACE_RGB, FALSE, 8, temp_width, temp_height );
		gdk_pixbuf_copy_area ( pix, 
							   ( real_width - temp_width ) / 2, 
							   ( real_height - temp_height ) / 2,
			   			       temp_width, 
							   temp_height, 
							   temp,  0, 0 );

		Composite( image, width, height, temp );

		gdk_pixbuf_unref( temp );
	}
	else
	{
		Composite( image, width, height, pix );
	}
	return true;
}



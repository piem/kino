/*
 * DVScalingParams.h -- DV Encoder Classes
 * Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _KINO_DV_SCALING_PARAMS_
#define _KINO_DV_SCALING_PARAMS_

#include <string>
using std::string;

#include <stdio.h>
#ifdef __linux__
#include <stdint.h>
#else
#include <inttypes.h>
#endif

/** Helper class for scaling functionality.
*/

typedef enum
{
	SCALE_NONE,
	SCALE_ASPECT_RATIO,
	SCALE_FULL
}
DV_SCALING;

class DV_RGB
{
	public:
		uint8_t r;
		uint8_t g;
		uint8_t b;
		DV_RGB( ) : r( 0 ), g( 0 ), b( 0 )
		{
		}

		DV_RGB( string value )
		{
			// HACK
			short unsigned int rr, rg, rb;
			sscanf( value.c_str( ), "%2hx%2hx%2hx", &rr, &rg, &rb );
			r = rr;
			g = rg;
			b = rb;
		}
};

class DVScalingParams
{
	private:
		DV_SCALING scale;
		DV_RGB colour;
	public:
		DVScalingParams( ) : 
			scale( SCALE_NONE )
		{
			colour.r = 0;
			colour.g = 0;
			colour.b = 0;
		}
		void SetScale( DV_SCALING _scale ) { scale = _scale; }
		DV_SCALING GetScale( ) { return scale; }
		void SetBackgroundColour( string value ) { DV_RGB rgb( value ); colour = rgb; }
		void SetBackgroundColour( uint8_t r, uint8_t g, uint8_t b ) { colour.r = r; colour.g = g; colour.b = b; }
		void SetBackgroundColour( DV_RGB &rgb ) { colour = rgb; }
		void GetBackgroundColour( DV_RGB &rgb ) { rgb = colour; }
};

#endif

/*
* page_trim.cc Notebook Trim Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include <sys/time.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>

#include "page_trim.h"
#include "avi.h"
#include "error.h"
#include "message.h"
#include "filehandler.h"
#include "frame.h"
#include "commands.h"
#include "page_editor.h"
#include "ieee1394io.h"

#undef PLAY_WITH_STATS

#define TRIM_ADJ_POS 0
#define TRIM_ADJ_IN 1
#define TRIM_ADJ_OUT 2

extern "C"
{
#include "support.h"

	extern KinoCommon *common;
	extern struct navigate_control g_nav_ctl;
	extern char cmd[];
	static char lastcmd[ 256 ] = { 0 };
	static int _getOneSecond( void );

	static void resetThreads( );
	static void *readThread( void * info );
	static void *videoThread( void * info );
	static void *audioThread( void * info );
	static pthread_t readthreadTrim;
	static pthread_t audiothreadTrim;
	static pthread_t videothreadTrim;
	static pthread_mutex_t threadlock = PTHREAD_MUTEX_INITIALIZER;
	static pthread_mutex_t readlock = PTHREAD_MUTEX_INITIALIZER;
	static GtkAdjustment *trim_adj[ 3 ];
	static gboolean skipPosUpdate = FALSE;
	static GtkSpinButton *spin_in;
	static GtkSpinButton *spin_out;
	static GtkToggleButton *link_toggle;
	static GtkToggleButton *loop_toggle;
	static gboolean doScrub = false;
	static int newFrame = 0;
	static int lastFrame = -1;

#define PLAYBACK_FRAMES 50

	static Frame *frameContent[ PLAYBACK_FRAMES ];
	extern IEEE1394Writer *writer1394;

	/* signal callbacks */
	gboolean
	on_trim_value_changed_event ( GtkWidget * widget,
	                              GdkEventButton * event,
	                              gpointer user_data )
	{
		int value = ( int ) GTK_ADJUSTMENT( widget ) ->value;
		if ( GTK_ADJUSTMENT( widget ) == trim_adj[ TRIM_ADJ_IN ] )
		{
			int diff = common->getPageTrim() ->getOutPoint() - common->getPageTrim() ->getInPoint() + value;

			if ( gtk_toggle_button_get_active( link_toggle ) == TRUE && skipPosUpdate == FALSE )
			{
				if ( diff <= common->getPageTrim() ->getTotalFrames() )
				{
					if ( value >= common->getPageTrim() ->getOutPoint() )
						gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], value );
					else
						common->getPageTrim() ->setInPoint( value );

					skipPosUpdate = TRUE;
					gtk_spin_button_set_value( spin_out, ( gfloat ) diff );
					skipPosUpdate = TRUE;
					gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], diff );
				}
				else
				{
					skipPosUpdate = TRUE;
					gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], common->getPageTrim() ->getInPoint() );
				}
			}
			else
			{
				if ( value > common->getPageTrim() ->getOutPoint() )
					gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], value );
				common->getPageTrim() ->setInPoint( value );
			}
			skipPosUpdate = TRUE;
			common->getPageTrim() ->movedToFrame( value );
			gtk_spin_button_set_value( spin_in, ( gfloat ) value );

		}
		else if ( GTK_ADJUSTMENT( widget ) == trim_adj[ TRIM_ADJ_OUT ] )
		{
			int diff = common->getPageTrim() ->getInPoint() - common->getPageTrim() ->getOutPoint() + value;

			if ( gtk_toggle_button_get_active( link_toggle ) == TRUE && skipPosUpdate == FALSE )
			{
				if ( diff >= 0 )
				{
					if ( value <= common->getPageTrim() ->getInPoint() )
						gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], value - 1 );
					common->getPageTrim() ->setOutPoint( value );

					skipPosUpdate = TRUE;
					gtk_spin_button_set_value( spin_in, ( gfloat ) diff );
					skipPosUpdate = TRUE;
					gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], diff );
				}
				else
				{
					skipPosUpdate = TRUE;
					gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], common->getPageTrim() ->getOutPoint() );
				}
			}
			else
			{
				if ( value < common->getPageTrim() ->getInPoint() )
					gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], value );
				common->getPageTrim() ->setOutPoint( value );
			}
			skipPosUpdate = TRUE;
			common->getPageTrim() ->movedToFrame( value );
			gtk_spin_button_set_value( spin_out, ( gfloat ) value );

		}
		else if ( skipPosUpdate == FALSE )
		{
			skipPosUpdate = TRUE;
			common->getPageTrim() ->movedToFrame( value );
		}
		skipPosUpdate = FALSE;
		return FALSE;
	}

	void
	on_spinbutton_trim_in_value_changed ( GtkSpinButton * spinbutton,
	                                      gpointer user_data )
	{
		if ( strcmp( gtk_entry_get_text( GTK_ENTRY( spin_in ) ), "" ) )
		{
			int value = atoi( gtk_entry_get_text( GTK_ENTRY( spin_in ) ) );
			gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], value );
		}
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_trim_in" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
	}

	void
	on_spinbutton_trim_out_value_changed ( GtkSpinButton * spinbutton,
	                                       gpointer user_data )
	{
		if ( strcmp( gtk_entry_get_text( GTK_ENTRY( spin_out ) ), "" ) )
		{
			int value = atoi( gtk_entry_get_text( GTK_ENTRY( spin_out ) ) );
			gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], value );
		}
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_trim_out" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
	}

	void
	on_button_trim_in_reset_clicked ( GtkButton * button,
	                                  gpointer user_data )
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		common->getPageTrim() ->resetInPoint();
	}

	void
	on_button_trim_out_reset_clicked ( GtkButton * button,
	                                   gpointer user_data )
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		common->getPageTrim() ->resetOutPoint();
	}

	void
	on_button_trim_in_set_clicked ( GtkButton * button,
	                                gpointer user_data )
	{
		int value = common->getPageTrim() ->getPosition();
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], value );
	}

	void
	on_button_trim_out_set_clicked ( GtkButton * button,
	                                 gpointer user_data )
	{
		int value = common->getPageTrim() ->getPosition();
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], value );
	}

	void
	on_togglebutton_trim_link_toggled ( GtkToggleButton * togglebutton,
	                                    gpointer user_data )
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( togglebutton ), "eventbox_trim" ) );
		GtkImage *image = GTK_IMAGE( lookup_widget( GTK_WIDGET( togglebutton ), "pixmap_trim_link" ) );
		if ( gtk_toggle_button_get_active( togglebutton ) )
			gtk_image_set_from_file( image, DATADIR "/kino/link.xpm" );
		else
			gtk_image_set_from_file( image, DATADIR "/kino/link_off.xpm" );
	}

	gboolean
	on_spinbutton_trim_in_focus_in_event ( GtkWidget * widget,
	                                       GdkEventFocus * event,
	                                       gpointer user_data )
	{
		g_nav_ctl.escaped = TRUE;
		return FALSE;
	}

	gboolean
	on_spinbutton_trim_in_focus_out_event ( GtkWidget * widget,
	                                        GdkEventFocus * event,
	                                        gpointer user_data )
	{
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}

	gboolean
	on_spinbutton_trim_out_focus_in_event ( GtkWidget * widget,
	                                        GdkEventFocus * event,
	                                        gpointer user_data )
	{
		g_nav_ctl.escaped = TRUE;
		return FALSE;
	}


	gboolean
	on_spinbutton_trim_out_focus_out_event ( GtkWidget * widget,
	        GdkEventFocus * event,
	        gpointer user_data )
	{
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}

	gboolean
	on_trim_button_press_event ( GtkWidget * widget,
	                             GdkEventButton * event,
	                             gpointer user_data )
	{
		doScrub = TRUE;
		videoPause();
		return FALSE;
	}


	gboolean
	on_trim_button_release_event ( GtkWidget * widget,
	                               GdkEventButton * event,
	                               gpointer user_data )
	{
		doScrub = FALSE;
		return FALSE;
	}

	/* insert mode callbacks */

	gboolean
	on_combo_trim_clip_entry_focus_in_event( GtkWidget       *widget,
	                                         GdkEventFocus   *event,
	                                         gpointer         user_data)
	{
		g_nav_ctl.escaped = TRUE;
		return FALSE;
	}

	gboolean
	on_combo_trim_clip_entry_focus_out_event( GtkWidget       *widget,
	                                          GdkEventFocus   *event,
	                                          gpointer         user_data)
	{
		g_nav_ctl.escaped = FALSE;
		const gchar* filename = gtk_entry_get_text( GTK_ENTRY( widget ) );
		if ( strlen( filename ) )
		{
			switch ( common->checkFile( ( char* ) filename ) )
			{
				case AVI:
				case RAW_DV:
				case QT:
					if ( common->getPageTrim()->loadFile( filename ) )
						common->getPageTrim()->movedToFrame( common->getPageTrim()->getInPoint() );
					break;
				default:
					// XXX: the following is making gtk abort
					//modal_message( _( "Invalid file specified." ) );
					break;
			}
		}
		return FALSE;
	}

	void
	on_combo_trim_clip_entry_changed( GtkEditable     *editable,
	                                  gpointer         user_data)
	{
		if ( g_nav_ctl.escaped == FALSE )
		{
			const gchar* filename = gtk_entry_get_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( editable ), "entry_trim_clip" ) ) );
			if ( strlen( filename ) )
			{
				switch ( common->checkFile( ( char* ) filename ) )
				{
					case AVI:
					case RAW_DV:
					case QT:
						if ( common->getPageTrim()->loadFile( filename ) )
						{
							common->getPageTrim()->movedToFrame( common->getPageTrim()->getInPoint() );
							break;
						}
					default:
					{
						const std::string& importedFile = common->importFile( filename );
						if ( common->getPageTrim()->loadFile( const_cast<char*>( importedFile.c_str() ) ) )
						{
							gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( editable ), "entry_trim_clip" ) ), const_cast<char*>( importedFile.c_str() ) );
							common->getPageTrim()->movedToFrame( common->getPageTrim()->getInPoint() );
						}
						else
						{
							gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( editable ), "entry_trim_clip" ) ), "" );
						}
						break;
					}
						// XXX: the following is making gtk abort
						//modal_message( _( "Invalid file specified." ) );
// 						break;
				}
			}
		}
	}

	void
	on_button_trim_open_clicked( GtkButton       *button,
	                             gpointer         user_data)
	{
		char *filename = common->getFileToOpen( _("Choose a DV file") );
		if ( filename && strcmp( filename, "" ) )
		{
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_trim_clip" ) ),
				filename );
		}
	}

	void
	on_button_trim_insert_before_clicked( GtkButton       *button,
	                                      gpointer         user_data)
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		common->getPageTrim()->insertScene( TRIM_INSERT_MODE_BEFORE );
	}

	void
	on_button_trim_insert_after_clicked( GtkButton       *button,
	                                     gpointer         user_data)
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		common->getPageTrim()->insertScene( TRIM_INSERT_MODE_AFTER );
	}

	void
	on_menuitem_trim_update_activate( GtkMenuItem     *menuitem,
	                                  gpointer         user_data)
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( menuitem ), "eventbox_trim" ) );
		common->getPageTrim()->setMode( PAGE_TRIM_MODE_UPDATE );
	}

	void
	on_menuitem_trim_insert_activate( GtkMenuItem     *menuitem,
                                      gpointer         user_data)
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( menuitem ), "eventbox_trim" ) );
		common->getPageTrim()->setMode( PAGE_TRIM_MODE_INSERT );
	}

	void
	on_button_trim_apply_clicked( GtkButton       *button,
	                              gpointer         user_data)
	{
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_trim" ) );
		processCommand( "Enter" );
	}
	
	void
	on_entry_trim_in_activate              (GtkEntry        *entry,
											gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_trim_in" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_trim_in_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_trim_in_focus_out_event       (GtkWidget       *widget,
											GdkEventFocus   *event,
											gpointer         user_data)
	{
		on_entry_trim_in_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
	
	void
	on_entry_trim_out_activate             (GtkEntry        *entry,
											gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_trim_out" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_trim_out_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_trim_out_focus_out_event      (GtkWidget       *widget,
											GdkEventFocus   *event,
											gpointer         user_data)
	{
		on_entry_trim_out_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}

}

/** Constructor for the trimmer page object.

  	\param common	KinoCommon object to which this page belongs
*/
GArray *scenes = NULL;
PageTrim::PageTrim( KinoCommon *common ) :
		start_orig( 0 ), update_start(0), end_orig( 0 ), in_orig( 0 ), out_orig( 0 ), in( 0 ), out( 0 ),
		max( 0 ), pos( -1 ), currentScene( 0 ), lastPos( -1 ), mode( PAGE_TRIM_MODE_UNKNOWN ),
		changed( false )
{
	cerr << "> Creating page trim" << endl;
	this->common = common;
	this->frameArea = GTK_DRAWING_AREA( lookup_widget( common->getWidget(), "drawingarea_trim" ) );
	gtk_widget_set_double_buffered( GTK_WIDGET( frameArea ), FALSE );
	this->positionLabelCurrent = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_current" ) );
	this->positionLabelTotal = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_total" ) );
	spin_in = GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "spinbutton_trim_in" ) );
	spin_out = GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "spinbutton_trim_out" ) );
	link_toggle = GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(), "togglebutton_trim_link" ) );
	loop_toggle = GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(), "togglebutton_trim_loop" ) );

	this->g_copiedPlayList = new PlayList();

	// Attach the custom GTK+ Trim control
	trim_adj[ TRIM_ADJ_POS ] = GTK_ADJUSTMENT(
	                               gtk_adjustment_new( 0, 0, 0, 1, 10, 0 ) );
	trim_adj[ TRIM_ADJ_IN ] = GTK_ADJUSTMENT(
	                              gtk_adjustment_new( 0, 0, 0, 1, 10, 0 ) );
	trim_adj[ TRIM_ADJ_OUT ] = GTK_ADJUSTMENT(
	                               gtk_adjustment_new( 0, 0, 0, 1, 10, 0 ) );
	for ( int i = 0; i < 3; i++ )
		g_signal_connect ( G_OBJECT ( trim_adj[ i ] ), "value_changed",
		                   G_CALLBACK( on_trim_value_changed_event ), ( gpointer ) i );
	trim = gtk_enhanced_scale_new( ( GtkObject** ) trim_adj, 3 );
	gtk_widget_set_name ( trim, "trim" );
	gtk_widget_ref( trim );
	gtk_object_set_data_full ( GTK_OBJECT( common->getWidget() ), "trim", trim,
	                           ( GtkDestroyNotify ) gtk_widget_unref );
	GtkWidget *vbox_trim = lookup_widget( common->getWidget(), "vbox_trim_custom" );
	gtk_widget_show( trim );
	gtk_box_pack_start( GTK_BOX ( vbox_trim ), trim, FALSE, TRUE, 0 );
	g_signal_connect( G_OBJECT( trim ), "button_press_event",
	                  G_CALLBACK( on_trim_button_press_event ), NULL );
	g_signal_connect( G_OBJECT( trim ), "button_release_event",
	                  G_CALLBACK( on_trim_button_release_event ), NULL );
}

/** Destructor for the trimmer page object.
*/

PageTrim::~PageTrim( )
{
	cerr << "> Destroying page trim" << endl;
	delete this->g_copiedPlayList;
}

/** New File action.
*/

void PageTrim::newFile()
{
	this->stopNavigator();
}

/** Start action. Called when the page becomes current.
*/

void PageTrim::start()
{
	cerr << ">> Starting Trimmer" << endl;

	start_orig = update_start = end_orig = in = in_orig = out = out_orig = max = 0;

	for ( int i = 0; i < PLAYBACK_FRAMES; i ++ )
		frameContent[ i ] = GetFramePool( ) ->GetFrame( );

	this->displayer = new FrameDisplayer();
	if ( Preferences::getInstance().dv1394Preview )
	{
#ifdef HAVE_IEC61883
		AVC avc;
		Preferences::getInstance( ).phyID = avc.getNodeId( Preferences::getInstance( ).avcGUID );
		Preferences::getInstance( ).phyID = avc.isPhyIDValid( Preferences::getInstance( ).phyID );
		if ( !writer1394 )
			writer1394 = new iec61883Writer(
			             ( avc.getPort() < 0 ) ? 0 : avc.getPort(),
			             Preferences::getInstance().channel,
			             2 );
#endif
#ifdef HAVE_DV1394
		if ( Preferences::getInstance().dvExportDevice > 0 && !writer1394 )
			writer1394 = new dv1394Writer(
			             Preferences::getInstance().dvExportDevice,
			             Preferences::getInstance().channel,
			             2,
			             Preferences::getInstance().cip_n,
			             Preferences::getInstance().cip_d,
			             Preferences::getInstance().syt_offset );
#endif
	}

	if ( ! Preferences::getInstance().trimModeInsert && common->g_currentFrame > -1 )
		setMode( PAGE_TRIM_MODE_UPDATE );
	else
		setMode( PAGE_TRIM_MODE_INSERT );

	if ( common->getPlayList() ->GetNumFrames() > 0 )
	{
		// Load the current scene
		loadScene( common->g_currentFrame );
		lastPos = pos = in + common->g_currentFrame - start_orig;
		movedToFrame( pos );

		// Load the clip combo
		GList *items = NULL;
		map<string, FileHandler*>::iterator n;
		for ( n = GetFileMap()->GetMap().begin(); n != GetFileMap()->GetMap().end(); ++n )
			items = g_list_append( items, ( void* ) n->first.c_str() );
		g_nav_ctl.escaped = TRUE;
		gtk_combo_set_popdown_strings( GTK_COMBO( lookup_widget( common->getWidget(), "combo_trim_clip" ) ), items);
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( common->getWidget(), "entry_trim_clip" ) ), "" );
		g_nav_ctl.escaped = FALSE;
	}

	gtk_widget_grab_focus( GTK_WIDGET( frameArea ) );
	common->packIt( "packer_trim", "packer_trim_outer" );

	gtk_notebook_set_page( GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_keyhelp" ) ), 2 );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_trim" ) ), TRUE );
}

/** Activate the returned widgets.
*/

gulong PageTrim::activate()
{
	return EDIT_MENU |
	       SCENE_LIST |
	       VIDEO_START_OF_MOVIE |
	       VIDEO_START_OF_SCENE |
	       VIDEO_REWIND |
	       VIDEO_BACK |
	       VIDEO_PLAY |
	       VIDEO_PAUSE |
	       VIDEO_STOP |
	       VIDEO_FORWARD |
	       VIDEO_FAST_FORWARD |
	       VIDEO_NEXT_SCENE |
	       VIDEO_END_OF_MOVIE |
	       VIDEO_SHUTTLE |
	       INFO_FRAME;
}

/** Clean action. Called when another page becomes current.
*/

void PageTrim::clean()
{
	cerr << ">> Leaving Trimmer" << endl;
	stopNavigator();
	delete displayer;
	displayer = 0;
	delete writer1394;
	writer1394 = NULL;
	saveScene();
	for ( int i = 0; i < PLAYBACK_FRAMES; i ++ )
		GetFramePool( ) ->DoneWithFrame( frameContent[ i ] );

	cerr << ">> Left Trimmer" << endl;
}


/** Load a scene from the movie into the trimmer to edit it.

    \param newScene The index of the scene to load.
*/
void PageTrim::loadScene( int newScene )
{
	char value[ 20 ];
	PlayList newPlayList;
	playlist = newPlayList;
	PlayList *pl = common->getPlayList();
	FileHandler *media;

	currentScene = newScene;

	// first, save begin and end time markers to region of the
	// the whole movie that this scene represents
	update_start = start_orig = pl->FindStartOfScene( currentScene );
	end_orig = pl->FindEndOfScene( currentScene );

	// get a local playlist representing just the scene
	// based on time markers in the movie
	pl->GetPlayList( start_orig, end_orig, playlist );

	// set the in point and current position to the in point
	// of the first clip in the scene
	in = in_orig = lastPos = pos = pl->GetClipBegin( currentScene );

	// unwind the clip at the beginning of the scene
	playlist.SetClipBegin( 0, "0" );

	// get the clip at the end of the scene
	playlist.GetMediaObject( playlist.FindEndOfScene( 0 ), &media );

	// unwind the clip at the end of the scene
	// XXX: what about frames deleted from the beginning or middle
	// of a multi-clip scene?
	int prevClipEnd = playlist.GetClipEnd( playlist.GetNumFrames() - 1 );
	int newClipEnd = media->GetTotalFrames() - 1;
	snprintf( value, 19, "%i", newClipEnd );
	playlist.SetClipEnd( playlist.GetNumFrames() - 1, value );

	// get the total number of frames now in the local playlist
	max = playlist.GetNumFrames() - 1;

	// set the out point to the difference between out point before
	// and after unwinding
	out = out_orig = max - ( newClipEnd - prevClipEnd );

	// inform controller of our scene change
	common->setCurrentScene( start_orig );

	// update the trim UI
	trim_adj[ TRIM_ADJ_IN ] ->upper = max;
	trim_adj[ TRIM_ADJ_IN ] ->value = in;
	g_signal_emit_by_name( trim_adj[ TRIM_ADJ_IN ], "changed" );
	trim_adj[ TRIM_ADJ_OUT ] ->upper = max;
	trim_adj[ TRIM_ADJ_OUT ] ->value = out;
	g_signal_emit_by_name( trim_adj[ TRIM_ADJ_OUT ], "changed" );
	trim_adj[ TRIM_ADJ_POS ] ->upper = max;
	g_signal_emit_by_name( trim_adj[ TRIM_ADJ_POS ], "changed" );

	// Recalibrate the spinners
	GtkAdjustment *adjust = gtk_spin_button_get_adjustment( spin_in );
	adjust->lower = 0;
	adjust->upper = max;
	adjust->value = in;
	g_signal_emit_by_name( adjust, "changed" );
	gtk_spin_button_set_value( spin_in, ( gfloat ) in );

	adjust = gtk_spin_button_get_adjustment( spin_out );
	adjust->lower = 0;
	adjust->upper = max;
	adjust->value = out;
	g_signal_emit_by_name( adjust, "changed" );
	gtk_spin_button_set_value( spin_out, ( gfloat ) out );

	g_nav_ctl.escaped = TRUE;
	gtk_entry_set_text( GTK_ENTRY( lookup_widget( common->getWidget(), "entry_trim_clip" ) ), "" );
	g_nav_ctl.escaped = FALSE;

	timeFormatChanged();
}


/** Saves the scene currently being edited back to the movie.
*/
void PageTrim::saveScene()
{
	if ( playlist.GetNumFrames() && ( in_orig != in || out_orig != out || changed ) )
	{
		// commit out point to local playlist
		playlist.Delete( out + 1, max );
		// commmit in point to local playlist
		playlist.Delete( 0, in - 1 );
		// delete the original scene from the movie
		common->getPlayList() ->Delete( start_orig, end_orig );
		// insert the revised scene (local playlist) into the movie
		// at the saved begin time marker
		common->getPlayList() ->InsertPlayList( playlist, start_orig );

		changed = false;

		// put into undo history
		GetEditorBackup() ->Store( common->getPlayList() );

		// update commons and other views (title bar and storyboard)
		common->hasListChanged = TRUE;
		common->setWindowTitle( );
		common->getPageEditor()->DrawBar( common->g_currentFrame );

		// Refresh the combo menu
		GList *items = NULL;
		map<string, FileHandler*>::iterator n;
		for ( n = GetFileMap()->GetMap().begin(); n != GetFileMap()->GetMap().end(); ++n )
			items = g_list_append( items, ( void* ) n->first.c_str() );
		g_nav_ctl.escaped = TRUE;
		gtk_combo_set_popdown_strings( GTK_COMBO( lookup_widget( common->getWidget(), "combo_trim_clip" ) ), items);
		g_nav_ctl.escaped = FALSE;
	}
}


/** Loads a clip into the trimmer.

    \param filename The name of the clip to load.
    \return true if the clip loaded successfully.
*/
bool PageTrim::loadFile( const string& filename )
{
	PlayList newList;
	bool result = true;

	try
	{
		if ( newList.LoadMediaObject( const_cast< char* >( filename.c_str() ) ) )
		{
			playlist = newList;
			start_orig = common->getPlayList()->FindStartOfScene( currentScene );
			end_orig = common->getPlayList()->FindEndOfScene( currentScene );
			in = in_orig = lastPos = pos = 0;
			out = out_orig = max = playlist.GetNumFrames() - 1;
			changed = true;

			trim_adj[ TRIM_ADJ_IN ] ->upper = max;
			trim_adj[ TRIM_ADJ_IN ] ->value = in;
			g_signal_emit_by_name( trim_adj[ TRIM_ADJ_IN ], "changed" );
			trim_adj[ TRIM_ADJ_OUT ] ->upper = max;
			trim_adj[ TRIM_ADJ_OUT ] ->value = out;
			g_signal_emit_by_name( trim_adj[ TRIM_ADJ_OUT ], "changed" );
			trim_adj[ TRIM_ADJ_POS ] ->upper = max;
			g_signal_emit_by_name( trim_adj[ TRIM_ADJ_POS ], "changed" );

			GtkAdjustment *adjust = gtk_spin_button_get_adjustment( spin_in );
			adjust->lower = 0;
			adjust->upper = max;
			adjust->value = in;
			g_signal_emit_by_name( adjust, "changed" );
			gtk_spin_button_set_value( spin_in, ( gfloat ) in );

			adjust = gtk_spin_button_get_adjustment( spin_out );
			adjust->lower = 0;
			adjust->upper = max;
			adjust->value = out;
			g_signal_emit_by_name( adjust, "changed" );
			gtk_spin_button_set_value( spin_out, ( gfloat ) out );
			
			timeFormatChanged();
		}
		else
		{
			// XXX: the following is making gtk abort
			// modal_message( _( "Failed to load media file." ) );
			result = false;
		}
	}
	catch ( string s )
	{
		cerr << "Could not load file " << filename << ", because an exception has occurred: " << endl;
		cerr << s << endl;
		result = false;
	}

	return result;
}


/** Inserts the clip currently in the trimmer into the movie.

    \param insertMode Indicates whether to insert before or after
    the current position.
*/
void PageTrim::insertScene( TrimInsertMode insertMode )
{
	if ( playlist.GetNumFrames() > 0 )
	{
		int i = getSceneIndex();

		if ( insertMode == TRIM_INSERT_MODE_BEFORE )
		{
			PlayList tmp;
			tmp = playlist;
			tmp.Delete( out + 1, max );
			tmp.Delete( 0, in - 1 );
			common->getPlayList()->InsertPlayList( tmp, start_orig );
		}
		else if ( insertMode == TRIM_INSERT_MODE_AFTER )
		{
			PlayList tmp;
			tmp = playlist;
			tmp.Delete( out + 1, max );
			tmp.Delete( 0, in - 1 );
			common->getPlayList()->InsertPlayList( tmp, end_orig + 1 );
			i++;
		}
		GetEditorBackup()->Store( common->getPlayList() );
		common->hasListChanged = TRUE;
		common->setWindowTitle( );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		selectScene( i );
		common->setCurrentScene( start_orig );

		// Refresh the combo menu
		GList *items = NULL;
		const char *filename = gtk_entry_get_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( common->getWidget() ), "entry_trim_clip" ) ) );
		map<string, FileHandler*>::iterator n;
		items = g_list_append( items, ( void* ) filename );
		for ( n = GetFileMap()->GetMap().begin(); n != GetFileMap()->GetMap().end(); ++n )
			if ( strcmp( n->first.c_str(), filename ) )
				items = g_list_append( items, ( void* ) n->first.c_str() );
		g_nav_ctl.escaped = TRUE;
		gtk_combo_set_popdown_strings( GTK_COMBO( lookup_widget( common->getWidget(), "combo_trim_clip" ) ), items);
		g_nav_ctl.escaped = FALSE;
	}
}


void PageTrim::setMode( PageTrimMode newMode )
{
	if ( newMode != mode )
	{
		if ( newMode == PAGE_TRIM_MODE_UPDATE && common->getPlayList()->GetNumFrames() > 0 )
		{
			mode = newMode;
			gtk_widget_show( lookup_widget( common->getWidget(), "hbox_trim_update" ) );
			gtk_widget_hide( lookup_widget( common->getWidget(), "hbox_trim_insert" ) );

			// Load the currently selected scene
			gfloat saved_in = in;
			gfloat saved_out = out;
			loadScene( update_start );
			gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], saved_in );
			gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], saved_out );
		}
		else if ( newMode == PAGE_TRIM_MODE_INSERT )
		{
			mode = newMode;
			gtk_widget_hide( lookup_widget( common->getWidget(), "hbox_trim_update" ) );
			gtk_widget_show( lookup_widget( common->getWidget(), "hbox_trim_insert" ) );
		}

		// initialize insert mode controls for edit mode
		gtk_option_menu_set_history( GTK_OPTION_MENU( lookup_widget( common->getWidget(), "optionmenu_trim" ) ), static_cast< int >( mode ) );
	}
}

/** Starts the navigator thread. See comments on g_nav_ctl.
*/

void PageTrim::startNavigator()
{
	stopNavigator( );
	g_nav_ctl.active = TRUE;
	resetThreads( );
	pthread_create( &readthreadTrim, NULL, readThread, &g_nav_ctl );
	pthread_create( &audiothreadTrim, NULL, audioThread, &g_nav_ctl );
	pthread_create( &videothreadTrim, NULL, videoThread, ( gpointer ) & g_nav_ctl );
}

/** Stops the navigator thread. See comments on g_nav_ctl.
*/

void PageTrim::stopNavigator()
{
	if ( g_nav_ctl.active )
	{
		g_nav_ctl.active = FALSE;
		gdk_threads_leave();
		pthread_join( readthreadTrim, NULL );
		pthread_join( audiothreadTrim, NULL );
		pthread_join( videothreadTrim, NULL );
		gdk_threads_enter();
		getFrameDisplayer()->CloseSound();
	}
}

/** Called when the current frame has changed through the common moveToFrame
	method and this page is the current page.

	\param	frame	frame moved to
*/

void PageTrim::movedToFrame( int frame )
{
	if ( common->hasListChanged == TRUE )
	{
		common->getPageEditor()->ResetBar();
		common->hasListChanged = FALSE;

		if ( mode == PAGE_TRIM_MODE_UPDATE &&
			common->getPlayList() ->GetNumFrames() > 0 )
		{
			loadScene( frame );
			frame = in;
		}
		else if ( mode == PAGE_TRIM_MODE_INSERT )
		{
			const char *file = gtk_entry_get_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( common->getWidget() ), "entry_trim_clip" ) ) );
			if ( file && strcmp( file, "" ) )
			{
				loadFile( file );
				frame = pos;
			}
		}
	}
	if ( max > 0 )
	{
		if ( frame > out && gtk_toggle_button_get_active( loop_toggle ) == TRUE )
			pos = in;
		else if ( frame < in && gtk_toggle_button_get_active( loop_toggle ) == TRUE )
			pos = out;
		else if ( frame >= 0 && frame <= max )
			pos = frame;
		else if ( frame >= max )
			pos = max;
		else if ( frame < 0 && max > 0 )
			pos = 0;
		else
			pos = -1;

		if ( g_nav_ctl.active == FALSE )
		{
			skipPosUpdate = FALSE;
			showFrame( pos, ( pos == lastPos ) || ( Preferences::getInstance().audioScrub == FALSE ) );
		}
		else
		{
			lastFrame = newFrame = pos;
			startNavigator();
		}
	}
}

/** Show the frame requested

  	\param i		frame to be shown
	\param no_audio	indicate if audio is required or not
*/

void PageTrim::showFrame( int i, gboolean no_audio )
{
	if ( max == 0 )
	{
		common->loadSplash( frameArea );
		common->showFrameInfo( -1 );
		return ;
	}
	else if ( i > -1 )
	{
		Frame & frame = *( GetFramePool( ) ->GetFrame( ) );
		if ( i > max )
			i = max;
		playlist.GetFrame( i, frame );
		common->showFrameInfo( i );
		if ( writer1394 != NULL )
			writer1394->SendFrame( frame, false );
		if ( GTK_WIDGET_REALIZED( frameArea ) && getFrameDisplayer() )
			getFrameDisplayer() ->Put( frame, GTK_WIDGET( frameArea ), no_audio );
		lastPos = i;
		gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_POS ], ( gfloat ) i );
		GetFramePool( ) ->DoneWithFrame( &frame );
		common->g_currentFrame = start_orig + (pos - in_orig);
	}
}


void PageTrim::showFrame( int position, Frame& frame )
{
	if ( max == 0 )
	{
		common->loadSplash( frameArea );
	}
	else
	{
		getFrameDisplayer() ->Put( frame, GTK_WIDGET( frameArea ), TRUE );
		common->showFrameInfo( position );
		skipPosUpdate = TRUE;
		gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_POS ], ( gfloat ) position );
	}
}


/** Move to the start of the scene.
*/

void PageTrim::videoStartOfMovie()
{
	movedToFrame( 0 );
	common->toggleComponents( VIDEO_START_OF_MOVIE, false );
}

/** Move to the in point of the scene.
*/

void PageTrim::videoPreviousScene()
{
	movedToFrame( in );
	common->toggleComponents( VIDEO_START_OF_SCENE, false );
}

/** Move to the in point of the scene.
*/

void PageTrim::videoStartOfScene()
{
	movedToFrame( in );
	common->toggleComponents( VIDEO_START_OF_SCENE, false );
}

/** Rewind.
*/

void PageTrim::videoRewind()
{
	common->toggleComponents( common->getComponentState(), false );

	// Toggle Rewind state
	if ( g_nav_ctl.step != -10 )
	{
		common->toggleComponents( VIDEO_REWIND, true );
		g_nav_ctl.step = -10;
		g_nav_ctl.rate = 0;
		startNavigator();
	}
	else
	{
		stopNavigator();
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		common->toggleComponents( VIDEO_REWIND, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Move one frame back. If the navigator is active, then this action toggles
	between reverse and stop.
*/

void PageTrim::videoBack(int step)
{
	common->toggleComponents( common->getComponentState(), false );
	if ( g_nav_ctl.active )
	{
		if ( g_nav_ctl.step != -1 )
		{
			g_nav_ctl.step = -1;
			g_nav_ctl.rate = 1;
			common->toggleComponents( VIDEO_BACK, true );
		}
		else
		{
			stopNavigator();
			g_nav_ctl.step = 1;
			g_nav_ctl.rate = 0;
			common->toggleComponents( VIDEO_BACK, false );
			common->toggleComponents( VIDEO_STOP, true );
		}
	}
	else
	{
		common->moveByFrames( step );
		common->toggleComponents( VIDEO_BACK, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Play.
*/

void PageTrim::videoPlay()
{
	common->toggleComponents( common->getComponentState(), false );
	if ( g_nav_ctl.active == FALSE || g_nav_ctl.step != 1 )
	{
		common->toggleComponents( VIDEO_PLAY, true );
		g_nav_ctl.step = 1;
		startNavigator();
	}
	else
	{
		stopNavigator();
		common->toggleComponents( VIDEO_PLAY, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Pause.
*/

void PageTrim::videoPause()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_STOP, true );
	stopNavigator();
}

/** Stop.
*/

void PageTrim::videoStop()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_STOP, true );
	stopNavigator();
}

/** Move one frame forward. If the navigator is active, then this action puts
	the video into normal play.
*/

void PageTrim::videoForward(int step)
{
	common->toggleComponents( common->getComponentState(), false );
	if ( g_nav_ctl.active && g_nav_ctl.step != 1 )
	{
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		common->toggleComponents( VIDEO_FORWARD, true );
		startNavigator();
	}
	else
	{
		stopNavigator();
		common->moveByFrames( step );
		common->toggleComponents( VIDEO_FORWARD, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Fast forward.
*/

void PageTrim::videoFastForward()
{
	common->toggleComponents( common->getComponentState(), false );

	if ( g_nav_ctl.step != 10 )
	{
		common->toggleComponents( VIDEO_FAST_FORWARD, true );
		g_nav_ctl.step = 10;
		g_nav_ctl.rate = 0;
		startNavigator();
	}
	else
	{
		stopNavigator();
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		common->toggleComponents( VIDEO_FAST_FORWARD, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Shuttle

	Bi-directionaly variable-speed playback.

	\param angle A number from -15 (fastest reverse) to 15 (fastest forward), 0=stop
*/

void PageTrim::videoShuttle( int angle )
{
	int frames_sec = _getOneSecond();
	int speedTable[] = {
			0,
			8,  10, 15, 20, 33, 50, 75,
			100,
			200, 300, 400, 500, 800, 1200,
			( frames_sec * 100 ) };
	char s[ 64 ];

	if ( angle < -15 )
		angle = -15;
	if ( angle > 15 )
		angle = 15;

	int speed = speedTable[ ( angle < 0 ) ? -angle : angle ] * ( ( angle < 0 ) ? -1 : 1 );
	if ( speed == 0 || g_nav_ctl.step != speed / 100 || g_nav_ctl.rate != 100 / speed )
	{
		stopNavigator( );
		if ( speed == 0 )
		{
			common->keyboardFeedback( "", "" );
			common->toggleComponents( VIDEO_STOP, true );
		}
		else
		{
			g_nav_ctl.step = speed / 100;
			g_nav_ctl.rate = 100 / speed;
			g_nav_ctl.subframe = 0;
			startNavigator();
			snprintf( s, 63, _( "Shuttle %+.1f fps" ), ( float ) speed / 100.0 * frames_sec );
			common->keyboardFeedback( "", s );
		}
	}
}

/** Move to the end of the current scene.
*/

void PageTrim::videoNextScene()
{
	movedToFrame( out );
	common->toggleComponents( VIDEO_NEXT_SCENE, false );
}

/** Move to the end of the current scene.
*/

void PageTrim::videoEndOfScene()
{
	movedToFrame( out );
	common->toggleComponents( VIDEO_NEXT_SCENE, false );
}

/** Move to the end of the play list.
*/

void PageTrim::videoEndOfMovie()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_END_OF_MOVIE, true );
	stopNavigator();
	movedToFrame( max );
	common->toggleComponents( VIDEO_END_OF_MOVIE, false );
	common->toggleComponents( VIDEO_STOP, true );
}

/** Load the selected scene.

	\param i	scene to move to
*/

void PageTrim::selectScene( int i )
{
	bool restartNavigator = ( g_nav_ctl.active == TRUE );
	
	stopNavigator();
	
	if ( mode == PAGE_TRIM_MODE_UPDATE )
		// commit the changes and load a new scene
		saveScene();

	// figure out what scene I am in
	vector <int> scene = common->getPageEditor() ->GetScene();
	if ( i >= ( int ) scene.size() )
		i = scene.size() - 1;
	currentScene = ( i <= 0 ) ? 0 : scene[ i - 1 ];
	common->g_currentFrame = currentScene;

	if ( mode == PAGE_TRIM_MODE_UPDATE )
	{
		loadScene( currentScene );
		movedToFrame( in );
	}
	else if ( mode == PAGE_TRIM_MODE_INSERT )
	{
		// set the insertion points
		start_orig = common->getPlayList()->FindStartOfScene( currentScene );
		end_orig = common->getPlayList()->FindEndOfScene( currentScene );

		common->setCurrentScene( start_orig );
	}
	
	if ( restartNavigator )
		startNavigator();
}

int PageTrim::getSceneIndex()
{
	int i = 0;
	if ( common->getPlayList()->GetNumFrames() > 0 )
	{
		vector <int> scene = common->getPageEditor() ->GetScene();
		for ( i = 0; i < (int) scene.size() - 1; i++ )
			if ( currentScene < scene[ i ] )
				break;
	}
	else
	{
		i = -1;
	}
	return i;
}

void PageTrim::resetInPoint()
{
	in = in_orig;
	gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], ( gfloat ) in );
	gtk_spin_button_set_value( spin_in, ( gfloat ) in );
}


void PageTrim::resetOutPoint()
{
	out = out_orig;
	gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], ( gfloat ) out );
	gtk_spin_button_set_value( spin_out, ( gfloat ) out );
}

/** Process a keyboard event.

  	\param event	keyboard event
*/

gboolean PageTrim::processKeyboard( GdkEventKey *event )
{
	gboolean ret = FALSE;

	// Only process while not escape mode
	if ( g_nav_ctl.escaped == FALSE )
	{
		if ( strcmp( lastcmd, "alt" ) == 0 )
		{
			strcpy( lastcmd, "" );
			return ret;
		}

		// Translate special keys to equivalent command
		switch ( event->keyval )
		{
		case GDK_Home:
			strcat( cmd, "gg");
			ret = TRUE;
			break;
		case GDK_End:
			strcat( cmd, "G");
			ret = TRUE;
			break;
		case GDK_BackSpace:
		case GDK_Left:
			strcat( cmd, "h" );
			ret = TRUE;
			break;
		case GDK_Up:
			strcat( cmd, "k" );
			ret = TRUE;
			break;
		case GDK_Right:
			strcat( cmd, "l" );
			ret = TRUE;
			break;
		case GDK_Return:
			if ( cmd[ 0 ] != 0 )
			{
				// Last command is now
				strcpy( lastcmd, cmd );

				// end the command entry
				cmd[ 0 ] = 0;
				common->setStatusBar( "" );
				break;
			}
			else
			{
				strcat( cmd, "Enter" );
				ret = TRUE;
				break;
			}
		case GDK_Down:
			strcat( cmd, "j" );
			ret = TRUE;
			break;
		case GDK_Delete:
			strcat( cmd, "x" );
			ret = TRUE;
			break;
		case GDK_Escape:
			if ( common->getComponentState() & VIDEO_STOP )
			{
				common->changePageRequest( PAGE_EDITOR );
				return ret;
			}
			else
			{
				strcat( cmd, "Esc" );
				break;
			}
		case GDK_Alt_L:
		case GDK_Alt_R:
			strcpy( lastcmd, "alt" );
			return ret;
		case GDK_Insert:
		case GDK_KP_Insert:
			strcat( cmd, "Ins" );
			ret = TRUE;
			break;
		default:
			if ( strcmp( event->string, "." ) )
				strcat( cmd, event->string );
			break;
		}

		if ( !strcmp( event->string, "." ) )
			strcpy( cmd, lastcmd );
		else if ( cmd[ 0 ] == 0x12 )
			strcpy( cmd, "Ctrl+R" );

#if 0
		printf( "send_event: %2.2x\n", event->send_event );
		printf( "time  : %8.8x\n", event->time );
		printf( "state : %8.8x\n", event->state );
		printf( "keyval: %8.8x\n", event->keyval );
		printf( "length: %8.8x\n", event->length );
		printf( "string: %s\n", event->string );
		printf( "(hex) : %2.2x\n", event->string[ 0 ] );
		printf( "cmd   : %s\n", cmd );
		printf( "(hex) : %8.8x\n", cmd[ 0 ] );
		fflush( stdout );
#endif

		processCommand( cmd );
	}
	return ret;
}

/** Internal method for handling a complete keyboard scene.

  	\param cmd		command to be processed;
*/

gboolean PageTrim::processCommand( const char *command )
{
	int end;
	int count = 1;
	char real[ 256 ] = "";

	strcpy( cmd, command );

	switch ( sscanf( cmd, "%d%s", &count, real ) )
	{
	case 1:
		// Numeric value only - return immediately if the cmd is not "0"
		if ( strcmp( cmd, "0" ) )
		{
			common->keyboardFeedback( cmd, "" );
			return FALSE;
		}
		break;
	case 0:
		sscanf( cmd, "%s", real );
		count = 1;
		break;
	}

	if ( strcmp( cmd, "." ) )
		strcpy( lastcmd, cmd );

	/* Navigation */

	/* play, pause */

	if ( strcmp( cmd, " " ) == 0 )
	{
		if ( g_nav_ctl.active == TRUE )
		{
			common->keyboardFeedback( cmd, _( "Pause" ) );
			common->videoPause( );
		}
		else
		{
			common->keyboardFeedback( cmd, _( "Play" ) );
			common->videoPlay( );
		}
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "Esc" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Stop" ) );
		common->videoStop( );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "Enter" ) == 0 )
	{
		if ( mode == PAGE_TRIM_MODE_UPDATE )
		{
			int pos = common->getPageTrim()->getPosition();
			common->keyboardFeedback( cmd, _( "Overwrite" ) );
			saveScene();
			loadScene( currentScene );
			movedToFrame( pos );
		}
		else if ( mode == PAGE_TRIM_MODE_INSERT )
		{
			common->keyboardFeedback( cmd, _( "Insert After" ) );
			insertScene( TRIM_INSERT_MODE_AFTER );
			movedToFrame( in );
		}
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, ":r" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Insert Before" ) );
		insertScene( TRIM_INSERT_MODE_BEFORE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, ":a" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Insert After" ) );
		insertScene( TRIM_INSERT_MODE_AFTER );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "Ins" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Toggle Insert/Overwrite" ) );
		setMode( mode == PAGE_TRIM_MODE_UPDATE ? PAGE_TRIM_MODE_INSERT : PAGE_TRIM_MODE_UPDATE );
		cmd[ 0 ] = 0;
	}

	/* advance one frame */

	else if ( strcmp( real, "l" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward" ) );
		movedToFrame( pos + count );
		cmd[ 0 ] = 0;
	}

	/* backspace one frame */

	else if ( strcmp( real, "h" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move backward" ) );
		movedToFrame( pos - count );
		cmd[ 0 ] = 0;
	}

	/* advance one second */

	else if ( strcmp( real, "w" ) == 0 || strcmp( real, "W" ) == 0 ||
	          strcmp( real, "e" ) == 0 || strcmp( real, "E" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward second" ) );
		movedToFrame( pos + count * _getOneSecond() );
		cmd[ 0 ] = 0;
	}

	/* backspace one second */

	else if ( ( strcmp( real, "b" ) == 0 ) || ( strcmp( real, "B" ) == 0 ) )
	{
		common->keyboardFeedback( cmd, _( "Move backwards one second" ) );
		moveToFrame( pos - count * _getOneSecond() );
		cmd[ 0 ] = 0;
	}

	/* start of scene */

	else if ( ( strcmp( cmd, "0" ) == 0 ) || ( strcmp( real, "^" ) == 0 ) )
	{
		common->videoStartOfScene( );
		common->keyboardFeedback( cmd, _( "Move to start of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* end of scene */

	else if ( strcmp( real, "$" ) == 0 )
	{
		common->videoEndOfScene( );
		common->keyboardFeedback( cmd, _( "Move to end of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* start of next scene */

	else if ( ( strcmp( real, "j" ) == 0 ) || strcmp( real, "+" ) == 0 )
	{
		selectScene( getSceneIndex() + 1 );
		common->keyboardFeedback( cmd, _( "Move to start of next scene" ) );
		cmd[ 0 ] = 0;
	}

	/* start of previous scene */

	else if ( ( strcmp( real, "k" ) == 0 ) || ( strcmp( real, "-" ) == 0 ) )
	{
		selectScene( getSceneIndex() - 1 );
		common->keyboardFeedback( cmd, _( "Move to start of previous scene" ) );
		cmd[ 0 ] = 0;
	}

	/* first frame */

	else if ( strcmp( cmd, "gg" ) == 0 )
	{
		common->videoStartOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to first frame" ) );
		cmd[ 0 ] = 0;
	}

	/* last frame */

	else if ( strcmp( cmd, "G" ) == 0 )
	{
		common->videoEndOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to last frame" ) );
		cmd[ 0 ] = 0;
	}

	/* delete current scene */

	else if ( strcmp( real, "dd" ) == 0 )
	{
		end_orig = start_orig;
		for ( ; count >= 1 && end_orig <= common->getPlayList() ->GetNumFrames() - 1; count -- )
		{
			end_orig = common->getPlayList() ->FindEndOfScene( end_orig );
			end_orig ++;
		}
		common->getPageEditor()->CopyFrames( start_orig, end_orig - 1 );
		common->getPageEditor()->DeleteFrames( start_orig, end_orig - 1 );
		if ( start_orig > common->getPlayList() ->GetNumFrames() - 1 )
			start_orig = common->getPlayList() ->GetNumFrames() - 1;
		common->moveToFrame( start_orig );
		common->keyboardFeedback( cmd, _( "Cut current scene" ) );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete from current frame up to end of scene */

	else if ( ( strcmp( cmd, "o" ) == 0 ) || ( strcmp( cmd, "d$" ) == 0 ) )
	{
		out = pos;
		gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_OUT ], ( gfloat ) out );
		common->keyboardFeedback( cmd, _( "Cut to end of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* delete from start of scene just before current frame */

	else if ( ( strcmp( cmd, "i" ) == 0 ) || ( strcmp( cmd, "d0" ) == 0 ) || strcmp( cmd, "d^" ) == 0 )
	{
		in = pos;
		gtk_adjustment_set_value( trim_adj[ TRIM_ADJ_IN ], ( gfloat ) in );
		common->keyboardFeedback( cmd, _( "Cut from start of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* copy current scene */

	else if ( ( strcmp( real, "yy" ) == 0 ) || ( strcmp( real, "Y" ) == 0 ) )
	{
		end_orig = start_orig;
		for ( ; count >= 1; count -- )
		{
			end_orig = common->getPlayList() ->FindEndOfScene( end_orig );
			end_orig ++;
		}
		common->getPageEditor()->CopyFrames( start_orig, end_orig - 1 );
		common->keyboardFeedback( cmd, _( "Copy current scene" ) );
		cmd[ 0 ] = 0;
	}

	/* paste after current frame */

	else if ( strcmp( real, "p" ) == 0 )
	{
		for ( ; count >= 1; count -- )
			common->getPageEditor()->PasteFrames( common->g_currentFrame + 1 );
		movedToFrame( pos );
		common->keyboardFeedback( cmd, _( "Paste after current frame" ) );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* paste before current frame */

	else if ( strcmp( real, "P" ) == 0 )
	{
		for ( ; count >= 1; count -- )
			common->getPageEditor()->PasteFrames( common->g_currentFrame );
		end_orig = common->getPlayList() ->FindEndOfScene( common->g_currentFrame + 1 );
		movedToFrame( pos );
		common->keyboardFeedback( cmd, _( "Paste before current frame" ) );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* Switch to capture mode */

	else if ( strcmp( cmd, "a" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, insert after frame" ) );
		end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
		common->moveToFrame( end );
		FileTracker::GetInstance().SetMode( CAPTURE_FRAME_APPEND );
		common->changePageRequest( PAGE_CAPTURE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "A" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, append to movie" ) );
		end = common->getPlayList() ->GetNumFrames();
		common->moveToFrame( end );
		FileTracker::GetInstance().SetMode( CAPTURE_MOVIE_APPEND );
		common->changePageRequest( PAGE_CAPTURE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "v" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Timeline" ) );
		common->changePageRequest( PAGE_TIMELINE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "C" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "FX" ) );
		common->changePageRequest( PAGE_MAGICK );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "u" ) == 0 )
	{
		GetEditorBackup() ->Undo( common->getPlayList() );
		common->keyboardFeedback( cmd, _( "Undo" ) );
		common->hasListChanged = TRUE;
		movedToFrame( 0 );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "Ctrl+R" ) == 0 )
	{
		GetEditorBackup() ->Redo( common->getPlayList() );
		common->keyboardFeedback( cmd, _( "Redo" ) );
		common->hasListChanged = TRUE;
		movedToFrame( 0 );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	// the following are specific to Trim
	else if ( strcmp( cmd, "U" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Undo" ) );
		resetInPoint();
		resetOutPoint();
		movedToFrame( pos );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "\\" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Toggle Looping" ) );
		gtk_toggle_button_set_active( loop_toggle, ! gtk_toggle_button_get_active( loop_toggle ) );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "=" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Toggle Link" ) );
		gtk_toggle_button_set_active( link_toggle, ! gtk_toggle_button_get_active( link_toggle ) );
		cmd[ 0 ] = 0;
	}

	/* switch to export mode */

	else if ( strcmp( cmd, ":W" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Export" ) );
		common->changePageRequest( PAGE_EXPORT );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "F2" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Edit" ) );
		common->changePageRequest( PAGE_EDITOR );
		cmd[ 0 ] = 0;
	}

	/* write PlayList */

	else if ( strcmp( cmd, ":w" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Write playlist" ) );
		common->savePlayList( );
		cmd[ 0 ] = 0;
	}

	/* quit */

	else if ( strcmp( cmd, ":q" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "quit" ) );
		kinoDeactivate();
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "J" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Join scenes" ) );
		for ( ; count >= 1 && common->getPlayList() ->JoinScenesAt( common->g_currentFrame ); count -- )
			common->hasListChanged = TRUE;
		if ( common->hasListChanged == TRUE )
		{
			GetEditorBackup() ->Store( common->getPlayList() );
			movedToFrame( pos );
			g_signal_emit_by_name( trim_adj[ TRIM_ADJ_POS ], "value-changed" );
		}
		cmd[ 0 ] = 0;
	}

	/* split scene */

	else if ( strcmp( cmd, "Ctrl+J" ) == 0 && currentScene != -1 )
	{
		int newScene = getSceneIndex() + 1;
		common->keyboardFeedback( cmd, _( "Split scene before frame" ) );
		if ( pos >= in && pos <= out &&
			common->getPlayList() ->SplitSceneBefore( common->g_currentFrame ) )
		{
			GetEditorBackup() ->Store( common->getPlayList() );
			common->hasListChanged = TRUE;
			g_signal_emit_by_name( trim_adj[ TRIM_ADJ_POS ], "value-changed" );
			selectScene( newScene );
			common->videoStartOfScene( );
		}
		cmd[ 0 ] = 0;
	}

	/* goto a frame */
	else if ( strncmp( cmd, ":", 1 ) == 0 )
	{
		int val = 0;
		char t[ 132 ] = "";
		if ( sscanf( cmd + 1, "%d", &val ) == 1 )
		{
			common->moveToFrame( val );
			sprintf( t, "Move to frame %d", val );
			common->keyboardFeedback( cmd, t );
		}
		else
			common->setStatusBar( cmd );
	}

	else
	{
		// Check for invalid commands
		if ( strlen( real ) > 5 )
			cmd[ 0 ] = 0;
		else if ( strchr( "dgy ", real[ strlen( real ) - 1 ] ) == NULL )
			cmd[ 0 ] = 0;

		common->keyboardFeedback( cmd, "" );

	}

	return FALSE;
}


void PageTrim::windowMoved()
{
	if ( g_nav_ctl.active == FALSE )
	{
		if ( max == 0 )
			common->clearPreview( frameArea );
		else
			showFrame( pos, TRUE );
	}
}


void PageTrim::showFrameInfo( int i )
{
	Frame & frame = *( GetFramePool( ) ->GetFrame( ) );
	FileHandler *media;

	playlist.GetFrame( i, frame );
	playlist.GetMediaObject( i, &media );
	common->showFrameMoreInfo( frame, media );
	GetFramePool( ) ->DoneWithFrame( &frame );

	if ( i < 0 )
	{
		gtk_label_set_text( positionLabelCurrent, "" );
		gtk_label_set_text( positionLabelTotal, "" );
	}
	else
	{
		common->getTime().setFramerate( frame.GetFrameRate() );
		string tc = "<span size=\"x-large\">" + common->getTime().parseFramesToString( i, common->getTimeFormat() ) + "</span>";
		gtk_label_set_markup( positionLabelCurrent, tc.c_str() );
		gtk_widget_set_redraw_on_allocate( GTK_WIDGET( positionLabelCurrent ), FALSE );
		tc = _("Duration: ") + common->getTime().parseFramesToString( out - in + 1, common->getTimeFormat() );
		gtk_label_set_markup( positionLabelTotal, tc.c_str() );
	}
}

void PageTrim::timeFormatChanged()
{
	on_spinbutton_trim_in_value_changed( GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "spinbutton_trim_in" ) ), NULL );
	on_spinbutton_trim_out_value_changed( GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "spinbutton_trim_out" ) ), NULL );
}


extern "C"
{

	// Share the frames extracted from the audio and video thread - this
	// provides a smoother playback and is less intensive on the CPU. The
	// code assumes the video will never fall more than PLAYBACK_FRAMES behind
	// the audio.
	//
	// 'position' stores last obtained index in the 'frameContent' array and the
	// corresponding frame number is stored in the corresponding entry in the
	// frameNumber array.
	//
	// The audio thread is responsible for write access to these.

	static int frameNumber[ PLAYBACK_FRAMES ];
	static int pending = 0;
	static int head = -1;
	static int tail = -1;
	static bool showing = false;
	static bool playing = false;

	static void resetThreads( )
	{
		showing = false;
		playing = false;
		pending = 0;
		head = 0;
		tail = 0;
	}

	/** This function carries out the read ahead and is responsible for obtaining
		the each frame. audioThread monitors the queue (by checking mainly on pending)
		and plays the audio of each frame. videoThread picks up the last tail and
		displays it.

		Note that only half of the queue is filled here - this provides the video thread
		some time to safely use a frame without worrying about it being overwritten.
	*/

	static void *readThread( void * info )
	{
		struct navigate_control * ctl = ( struct navigate_control * ) info;
		newFrame = 0;
		lastFrame = common->getPageTrim() ->getPosition() - 1;
		gint totalFrames = common->getPageTrim() ->getTotalFrames();
		gint countFrames = 0;
		static Preferences &prefs = Preferences::getInstance();
		int time_per_frame = 1000000 / _getOneSecond( );

		// cerr << ">>> Starting read thread " << endl;

		// Get start of time
		struct timeval start;
		struct timeval end;
		gettimeofday( &start, NULL );

		// Loop while active
		while ( ctl->active )
		{

			// Calculate time for next frame
			start.tv_usec += time_per_frame;
			if ( start.tv_usec >= 1000000 )
			{
				start.tv_usec -= 1000000;
				start.tv_sec ++;
			}

			pthread_mutex_lock( &readlock );
			// Determine the frame to render
			newFrame = lastFrame + ctl->step;

			// determine new frame based upon jogshuttle rate
			if ( ctl->step == 0 )
			{
				ctl->subframe++;
				if ( ctl->rate < 0 )
				{
					if ( ctl->subframe >= -ctl->rate )
					{
						newFrame --;
						ctl->subframe = 0;
					}
				}
				else
				{
					if ( ctl->subframe >= ctl->rate )
					{
						newFrame ++;
						ctl->subframe = 0;
					}
				}
			}
			pthread_mutex_unlock( &readlock );

			// Check the bounds and adjust as necessary
			if ( newFrame < 0 )
				newFrame = totalFrames - 1;
			else if ( newFrame > common->getPageTrim() ->getOutPoint() &&
			          gtk_toggle_button_get_active( loop_toggle ) == TRUE )
				newFrame = common->getPageTrim() ->getInPoint();
			else if ( newFrame < common->getPageTrim() ->getInPoint() &&
			          gtk_toggle_button_get_active( loop_toggle ) == TRUE )
				newFrame = common->getPageTrim() ->getOutPoint();
			else if ( newFrame >= totalFrames )
				newFrame = 0;

			// Determine which locations in frameNumber and frameContent we need to use
			if ( pending >= PLAYBACK_FRAMES / 2 )
			{
				// Queue is full.. do nothing
				playing = true;
				struct timespec t = { 0, 1000000 }; // 1ms
				nanosleep( &t, NULL );
			}
			else if ( pending < PLAYBACK_FRAMES )
			{
				pthread_mutex_lock( &threadlock );
				int new_head = ( head + 1 ) % PLAYBACK_FRAMES;
				//cerr << "captured pending = " << pending << " new_head = " << new_head << " tail = " << tail << endl;
				pthread_mutex_unlock( &threadlock );

				frameNumber[ new_head ] = newFrame;
				common->getPageTrim() ->getPlayList().GetFrame( frameNumber[ new_head ], *frameContent[ new_head ] );

				pthread_mutex_lock( &threadlock );
				pending ++;
				head = new_head;
				pthread_mutex_unlock( &threadlock );

				if ( writer1394 != NULL )
					writer1394->SendFrame( *frameContent[ new_head ], false );

				// Inform the audio thread that it's got something to work with
				playing = true;

				// Render all frames if requested
				if ( ( ! prefs.dropFrame || ! prefs.enableAudio ) && ctl->active )
				{

					// Change the queue position
					tail = ( tail + 1 ) % PLAYBACK_FRAMES;
					pending --;

					// Set the current position
					common->getPageTrim() ->setPosition( frameNumber[ tail ] );

					// If audio is enabled and some other conditions
					if ( prefs.enableAudio && ( frameNumber[ tail ] != lastFrame || ctl->rate > 1 || ctl->rate < -1 ) )
						common->getPageTrim() ->getFrameDisplayer() ->PutSound( *frameContent[ tail ] );

					// Show the corresponding image
					gdk_threads_enter();
					common->getPageTrim() ->showFrame( frameNumber[ tail ], *frameContent[ tail ] );
					gdk_flush();
					gdk_threads_leave();

					// Determine how far from the next frame we really are
					gettimeofday( &end, NULL );
					int difference = ( ( start.tv_sec * 1000000 + start.tv_usec ) - ( end.tv_sec * 1000000 + end.tv_usec ) );

					if ( difference > 2000 && difference < time_per_frame )
					{
						// Sleep for half the remaining time when audio is disabled
						if ( ! prefs.enableAudio )
						{
							struct timespec t =
							    {
								    0, difference * 500
							    };
							nanosleep( &t, NULL );
						}
					}
					else if ( difference < 0 || difference >= time_per_frame )
					{
						gettimeofday( &start, NULL );
					}
				}

				// update the lastFrame and lastPos variables
				lastFrame = newFrame;

				// Incrment the frame count
				countFrames ++;
			}
		}

		// cerr << ">>> Ending reading thread" << endl;

		return NULL;
	}

	static void *audioThread( void * info )
	{

		static Preferences & prefs = Preferences::getInstance();
		struct navigate_control *ctl = ( struct navigate_control * ) info;
		gint lastFrame = common->getPageTrim() ->getPosition() - 1;
		int time_per_frame = 1000000 / _getOneSecond( );

		// cerr << ">>> Starting audio thread" << endl;

		while ( ctl->active )
		{

			// Only do this when drop frames is active (otherwise the audio thread takes care of it)
			if ( prefs.dropFrame && prefs.enableAudio )
			{

				// Obtain the position of the last released frame
				pthread_mutex_lock( &threadlock );
				int new_tail = ( tail + 1 ) % PLAYBACK_FRAMES;
				//cerr << "playing  pending = " << pending << " head = " << head << " new_tail = " << new_tail << endl;
				pthread_mutex_unlock( &threadlock );

				// As long as its different to the previous one, then display it
				if ( pending > 0 && playing )
				{

					// Move the main frame position to our newly derived location
					common->getPageTrim() ->setPosition( frameNumber[ tail ] );

					// Play the audio
					if ( prefs.enableAudio && ( frameNumber[ tail ] != lastFrame || ctl->rate > 1 || ctl->rate < -1 ) )
					{
						common->getPageTrim() ->getFrameDisplayer() ->PutSound( *frameContent[ tail ] );
						if ( pending < 10 )
						{
							// Give the reader more time
							struct timespec t = { 0, 1000000 }; // 1ms
							nanosleep( &t, NULL );
						}
					}
					else if ( ctl->step == 0 && !prefs.audioScrub )
					{
						struct timespec t =
						    {
							    0, time_per_frame * 1000
						    };
						nanosleep( &t, NULL );
					}

					// Last encountered audio frame
					lastFrame = frameNumber[ tail ];

					// Move the tail
					pthread_mutex_lock( &threadlock );
					pending --;
					tail = new_tail;
					pthread_mutex_unlock( &threadlock );

					// Start showing frames from this point onwards
					showing = true;

				}
				else
				{
					struct timespec t = { 0, 1000000 }; // 1ms
					nanosleep( &t, NULL );
				}
			}
			else
			{
				// It is safe to cancel the audio thread here since a change to prefs necessitates a restart.
				// cerr << ">>> Audio Thread not needed" << endl;
				return NULL;
			}
		}

		// cerr << ">>> Ending video thread" << endl;

		return NULL;
	}

	static void *videoThread( void * info )
	{

		static Preferences & prefs = Preferences::getInstance();
		struct navigate_control *ctl = ( struct navigate_control * ) info;
		int lastFrame = common->getPageTrim() ->getPosition() - 1;

#ifdef PLAY_WITH_STATS
		// Statistical analysis variables
		int dropped = 0;
		int count = 0;
#endif

		// cerr << ">>> Starting video thread" << endl;

		while ( ctl->active )
		{

			// Only do this when drop frames is active (otherwise the audio thread takes care of it)
			if ( prefs.dropFrame && prefs.enableAudio )
			{

				if ( showing && pending > 0 )
				{

					// Store the current tail
					int my_tail = tail;

					// As long as its different to the previous one, then display it
					if ( frameNumber[ my_tail ] != lastFrame || ctl->rate > 1 )
					{
#ifdef PLAY_WITH_STATS
						// Statistical analysis of frame playback (useful for limited kinds of tests)
						if ( ctl->step == 1 )
						{
							int skipped = frameNumber[ my_tail ] - lastFrame - 1;
							count += skipped + 1;
							if ( skipped != 0 && lastFrame != 0 && count != 0 )
							{
								dropped += skipped;
								cerr << ">>>> Dropped " << dropped << " in " << count << " frames - " << ( double ) ( ( ( double ) dropped / ( double ) count ) * 100.0 ) << "%" << endl;
							}
							lastFrame = frameNumber[ my_tail ];
						}
#endif

						// Show this frame
						if ( ctl->active )
						{
							gdk_threads_enter();
							common->getPageTrim() ->showFrame( frameNumber[ my_tail ], *frameContent[ my_tail ] );
							gdk_flush();
							gdk_threads_leave();
						}
					}
				}
				// Release thread
				struct timespec t = { 0, 1000000 }; // 1ms
				nanosleep( &t, NULL );
			}
			else
			{
				// It is safe to cancel the video thread here since a change to prefs necessitates a restart.
				// cerr << ">>> Video Thread not needed" << endl;
				return NULL;
			}
		}

#ifdef PLAY_WITH_STATS
		cerr << ">>>> Video stopped: Dropped " << dropped << " in " << count << " frames - " << ( double ) ( ( ( double ) dropped / ( double ) count ) * 100.0 ) << "%" << endl;
#endif

		// cerr << ">>> Ending video thread" << endl;

		return NULL;
	}

	static int _getOneSecond( void )
	{
		Frame & frame = *( GetFramePool() ->GetFrame( ) );
		common->getPlayList() ->GetFrame( common->g_currentFrame, frame );
		int value = ( frame.IsPAL() ? 25 : 30 );
		GetFramePool( ) ->DoneWithFrame( &frame );
		return value;
	}

}

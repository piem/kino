/*
* page_magick.cc -- FX Page definition
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <vector>
using std::cerr;
using std::endl;
using std::vector;

#include <string>
#include <iostream>
using std::cerr;
using std::endl;

#include <glade/glade.h>

#include "kino_extra.h"
#include "page_magick.h"
#include "kino_common.h"
#include "page.h"
#include "storyboard.h"
#include "page_editor.h"
#include "displayer.h"
#include "message.h"
#include "error.h"
#include "commands.h"
#include "gtkenhancedscale.h"
#include "export.h"

/** Provides plug-ins with current playlist.
*/

PlayList &GetCurrentPlayList( )
{
	return * common->getPlayList( );
}

extern "C"
{
#include "support.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dlfcn.h>
#include <dirent.h>

	extern GladeXML* magick_glade;
	extern struct navigate_control g_nav_ctl;

	static bool buttonMutex = false;

	void
	on_togglebutton_magick_start_toggled ( GtkToggleButton * togglebutton,
	                                       gpointer user_data )
	{
		if ( !buttonMutex )
		{
			buttonMutex = true;
			common->getPageMagick() ->StartRender();
			buttonMutex = false;
		}
	}

	void
	on_notebook_magick_switch_video_page ( GtkNotebook * notebook,
	                                 GtkNotebookPage * page,
	                                 gint page_num,
	                                 gpointer user_data )
	{
		PageMagick* magick = common->getPageMagick();
		magick->SetKeyFrameControllerClient( 0 );
		magick->ShowCurrentStatus( magick->GetCurrentPosition(), LOCKED_KEY, false, false );
		videoStop();
		magick->OnTimeRangeChanged();
	}

	void
	on_notebook_magick_switch_page ( GtkNotebook * notebook,
	                                 GtkNotebookPage * page,
	                                 gint page_num,
	                                 gpointer user_data )
	{
		PageMagick* magick = common->getPageMagick();
		videoStop();
		magick->OnTimeRangeChanged();
	}

	gboolean
	on_hscale_transition_start_button_press_event
	( GtkWidget * widget,
	  GdkEventButton * event,
	  gpointer user_data )
	{
		GtkWidget * lower = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_start" );
		GtkWidget *upper = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_end" );
		GtkAdjustment *adjust_lower = GTK_RANGE( lower ) ->adjustment;
		GtkAdjustment *adjust_upper = GTK_RANGE( upper ) ->adjustment;
		adjust_upper->lower = adjust_lower->value;
		g_signal_emit_by_name( adjust_upper, "changed" );
		return FALSE;
	}


	gboolean
	on_hscale_transition_start_button_release_event
	( GtkWidget * widget,
	  GdkEventButton * event,
	  gpointer user_data )
	{
		GtkWidget * lower = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_start" );
		GtkWidget *upper = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_end" );
		GtkAdjustment *adjust_lower = GTK_RANGE( lower ) ->adjustment;
		GtkAdjustment *adjust_upper = GTK_RANGE( upper ) ->adjustment;
		adjust_upper->lower = adjust_lower->value;
		g_signal_emit_by_name( adjust_upper, "changed" );
		common->getPageMagick() ->RefreshStatus( true );
		return FALSE;
	}


	gboolean
	on_hscale_transition_end_button_press_event
	( GtkWidget * widget,
	  GdkEventButton * event,
	  gpointer user_data )
	{
		GtkWidget * lower = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_start" );
		GtkWidget *upper = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_end" );
		GtkAdjustment *adjust_lower = GTK_RANGE( lower ) ->adjustment;
		GtkAdjustment *adjust_upper = GTK_RANGE( upper ) ->adjustment;
		adjust_lower->upper = adjust_upper->value;
		g_signal_emit_by_name( adjust_lower, "changed" );
		return FALSE;
	}


	gboolean
	on_hscale_transition_end_button_release_event
	( GtkWidget * widget,
	  GdkEventButton * event,
	  gpointer user_data )
	{

		GtkWidget * lower = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_start" );
		GtkWidget *upper = lookup_widget ( common->getPageMagick() ->window, "hscale_transition_end" );
		GtkAdjustment *adjust_lower = GTK_RANGE( lower ) ->adjustment;
		GtkAdjustment *adjust_upper = GTK_RANGE( upper ) ->adjustment;
		adjust_lower->upper = adjust_upper->value;
		g_signal_emit_by_name( adjust_lower, "changed" );
		common->getPageMagick() ->RefreshStatus( true );
		return FALSE;
	}

	void
	on_button_magick_file_clicked    (GtkButton       *button,
                                            gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to Save As") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_magick_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_magick_file" ) ), filename );
	}

	void
	on_spinbutton_magick_start_value_changed(GtkSpinButton   *spinbutton,
	                                         gpointer         user_data)
	{
		GtkSpinButton *endSpin = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( spinbutton ), "spinbutton_magick_end" ) );
		gtk_spin_button_set_range( endSpin, gtk_spin_button_get_value( spinbutton ),
			common->getPlayList() ->GetNumFrames() - 1 );
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_magick_start" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
		common->getPageMagick()->OnTimeRangeChanged();
	}
	
	void
	on_entry_magick_start_activate         (GtkEntry        *entry,
	                                        gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_magick_start" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_magick_start_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_magick_start_focus_out_event  (GtkWidget       *widget,
	                                        GdkEventFocus   *event,
	                                        gpointer         user_data)
	{
		on_entry_magick_start_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
	
	void
	on_spinbutton_magick_end_value_changed (GtkSpinButton   *spinbutton,
	                                        gpointer         user_data)
	{
		GtkSpinButton *startSpin = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( spinbutton ), "spinbutton_magick_start" ) );
		gtk_spin_button_set_range( startSpin, 0, gtk_spin_button_get_value( spinbutton ) );
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_magick_end" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
		common->getPageMagick()->OnTimeRangeChanged();
	}

	void
	on_entry_magick_end_activate           (GtkEntry        *entry,
	                                        gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_magick_end" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_magick_end_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_magick_end_focus_out_event    (GtkWidget       *widget,
	                                        GdkEventFocus   *event,
	                                        gpointer         user_data)
	{
		on_entry_magick_end_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
	
	void
	on_spinbutton_magick_limit_value_changed (GtkSpinButton   *spinbutton,
	                                        gpointer         user_data)
	{
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_magick_limit" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
		common->getPageMagick()->OnTimeRangeChanged();
	}

	void
	on_entry_magick_limit_activate           (GtkEntry        *entry,
	                                        gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "spinbutton_magick_limit" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_spinbutton_magick_limit_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_magick_limit_focus_out_event    (GtkWidget       *widget,
	                                        GdkEventFocus   *event,
	                                        gpointer         user_data)
	{
		on_entry_magick_limit_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
	
	static gboolean doScrub = FALSE;
	gboolean
	on_scrubbar_magick_button_press_event ( GtkWidget * widget,
	                                  GdkEventButton * event,
	                                  gpointer user_data )
	{
		doScrub = TRUE;
		videoStop();
		return FALSE;
	}


	gboolean
	on_scrubbar_magick_button_release_event ( GtkWidget * widget,
	                                    GdkEventButton * event,
	                                    gpointer user_data )
	{
		doScrub = FALSE;
		return FALSE;
	}

	gboolean
	on_scrubbar_magick_value_changed_event ( GtkWidget * widget,
	                                   GdkEventButton * event,
	                                   gpointer user_data )
	{
		if ( doScrub )
		{
			videoStop();
			moveToFrame( int( gtk_adjustment_get_value( GTK_ADJUSTMENT( widget ) ) ) );
		}
		return FALSE;
	}

	void
	on_time_range_changed(GtkWidget   *widget,
	                      gpointer       user_data)
	{
		common->getPageMagick()->OnTimeRangeChanged();
	}

	void
	on_togglebutton_key_frame_toggled ( GtkToggleButton * togglebutton,
	                                    gpointer user_data )
	{
		if ( !buttonMutex )
		{
			buttonMutex = true;
			PageMagick* magick = static_cast< PageMagick* >( user_data );
			magick->OnKeyFrameControllerKeyChanged( togglebutton );
			buttonMutex = false;
		}
	}

	static int _getOneSecond( void )
	{
		Frame & frame = *( GetFramePool() ->GetFrame( ) );
		common->getPlayList() ->GetFrame( common->g_currentFrame, frame );
		int value = ( frame.IsPAL() ? 25 : 30 );
		GetFramePool( ) ->DoneWithFrame( &frame );
		return value;
	}
}

/** Plugin - wrapper for the dlopen/dlsym functions.
*/

bool Plugin::Open( char *file )
{
	ptr = dlopen( file, RTLD_NOW );
	return ptr != NULL;
}

void Plugin::Close( )
{
	if ( ptr != NULL )
		dlclose( ptr );
}

void *Plugin::Find( const char *symbol )
{
	return dlsym( ptr, symbol );
}

const char *Plugin::GetError( )
{
	return dlerror();
}

/** Plugin Collection - loads all shared objects in the specified directory.
*/

PluginCollection::PluginCollection( )
{}

PluginCollection::~PluginCollection( )
{
	for ( unsigned int index = 0; index < collection.size(); index ++ )
	{
		collection[ index ] ->Close();
		delete collection[ index ];
	}
}

void PluginCollection::Initialise( const char *directory )
{
	char * filename;
	char *extension;
	DIR *dir;
	struct dirent *entry;
	struct stat statbuf;

	dir = opendir( directory );

	if ( dir )
	{
		while ( ( entry = readdir( dir ) ) != NULL )
		{
			filename = g_strdup_printf( "%s/%s", directory, entry->d_name );
			extension = strrchr( entry->d_name, '.' );
			if ( extension != NULL && !stat( filename, &statbuf ) && S_ISREG( statbuf.st_mode ) )
			{
				if ( !strcmp( extension, ".so" ) )
				{
					RegisterPlugin( filename );
				}
			}
			g_free( filename );
		}
		closedir( dir );
	}
}

void PluginCollection::RegisterPlugin( char *filename )
{
	Plugin * plugin = new Plugin;
	if ( plugin->Open( filename ) )
	{
		cerr << ">>> Registering plugin " << filename << endl;
		collection.push_back( plugin );
	}
	else
	{
		cerr << ">>> Rejecting plugin " << filename << " : " << plugin->GetError() << endl;
		delete plugin;
	}
}

unsigned int PluginCollection::Count()
{
	return collection.size();
}

Plugin *PluginCollection::Get( unsigned int index )
{
	return collection[ index ];
}

void PluginImageCreateRepository::InstallPlugins( Plugin *plugin )
{
	GDKImageCreate * ( *func ) ( int ) = ( GDKImageCreate * ( * ) ( int ) ) plugin->Find( "GetImageCreate" );
	if ( func != NULL )
	{
		int index = 0;
		GDKImageCreate *entry = func( index ++ );
		while ( entry != NULL )
		{
			if ( entry->IsUsable( ) )
				Register( entry );
			entry = func( index ++ );
		}
	}
}

void PluginImageFilterRepository::InstallPlugins( Plugin *plugin )
{
	GDKImageFilter * ( *func ) ( int ) = ( GDKImageFilter * ( * ) ( int ) ) plugin->Find( "GetImageFilter" );
	if ( func != NULL )
	{
		int index = 0;
		GDKImageFilter *entry = func( index ++ );
		while ( entry != NULL )
		{
			if ( entry->IsUsable( ) )
				Register( entry );
			entry = func( index ++ );
		}
	}
}

void PluginImageTransitionRepository::InstallPlugins( Plugin *plugin )
{
	GDKImageTransition * ( *func ) ( int ) = ( GDKImageTransition * ( * ) ( int ) ) plugin->Find( "GetImageTransition" );
	if ( func != NULL )
	{
		int index = 0;
		GDKImageTransition *entry = func( index ++ );
		while ( entry != NULL )
		{
			if ( entry->IsUsable( ) )
				Register( entry );
			entry = func( index ++ );
		}
	}
}

void PluginAudioFilterRepository::InstallPlugins( Plugin *plugin )
{
	GDKAudioFilter * ( *func ) ( int ) = ( GDKAudioFilter * ( * ) ( int ) ) plugin->Find( "GetAudioFilter" );
	if ( func != NULL )
	{
		int index = 0;
		GDKAudioFilter *entry = func( index ++ );
		while ( entry != NULL )
		{
			if ( entry->IsUsable( ) )
				Register( entry );
			entry = func( index ++ );
		}
	}
}

void PluginAudioTransitionRepository::InstallPlugins( Plugin *plugin )
{
	GDKAudioTransition * ( *func ) ( int ) = ( GDKAudioTransition * ( * ) ( int ) ) plugin->Find( "GetAudioTransition" );
	if ( func != NULL )
	{
		int index = 0;
		GDKAudioTransition *entry = func( index ++ );
		while ( entry != NULL )
		{
			if ( entry->IsUsable( ) )
				Register( entry );
			entry = func( index ++ );
		}
	}
}

/** Common info and factory for all PageMagick processors.
*/

class PageMagickFrames;
class PageMagickImage;
class PageMagickAudio;

class PageMagickInfo
{
private:
	PageMagickFrames *frameSource;
	PageMagickImage *imageManipulator;
	PageMagickAudio *audioManipulator;
public:
	KinoCommon *common;
	int begin;
	int end;
	double increment;
	bool reverse;
	int anteFrame;
	int postFrame;
	int width;
	int height;
	int isPAL;
	int isWide;
	int frequency;
	short channels;
	int samples_this_frame;
	bool preview;
	PageMagickInfo( KinoCommon *common );
	~PageMagickInfo();
	void SetLowQuality( bool quality );
	KinoCommon *GetCommon( )
	{
		return this->common;
	}
	void Initialise();
	PageMagickFrames *GetFrameSource();
	PageMagickImage *GetImageManipulator();
	PageMagickAudio *GetAudioManipulator();
	void SetBegin( int begin )
	{
		this->begin = begin > 0 ? begin : 0;
	}
	void SetEnd( int end )
	{
		this->end = end > 0 ? end : 0;
	}
	void SetPostFrame( int postFrame )
	{
		this->postFrame = postFrame;
	}
	void SetAnteFrame( int anteFrame )
	{
		this->anteFrame = anteFrame;
	}
	void GetAnteFrame( uint8_t *pixels );
	void GetPostFrame( uint8_t *pixels );
	int GetPostFrame( )
	{
		return this->postFrame;
	}
	int GetAnteFrame( )
	{
		return this->anteFrame;
	}
};

namespace
{

/// Helper class used to calculate position and frame_delta for plugins
class time_info
{
public:
	time_info( const PageMagickInfo& Info, const int FrameIndex, const double StartPosition = 0.0, const double EndPosition = 1.0 ) :
			info( Info ),
			frame_index( FrameIndex ),
			start_position( StartPosition ),
			end_position( EndPosition )
	{
		// Sanity checks ...
		if ( start_position > end_position )
			throw _( "Invalid position range" );
	}

	/// Returns the duration of the effect in frames
	unsigned int frame_count() const
	{
		return info.end - info.begin + 1;
	}

	/// Returns the duration of the effect as a percentage
	double duration() const
	{
		return end_position - start_position;
	}

	/// Returns the effect "position" (or percent complete)
	double position() const
	{
		double position = static_cast<double>( frame_index - info.begin ) / static_cast<double>( frame_count() ) * duration() + start_position;
		if ( info.reverse )
			position = 1 - position;
		return position;
	}

	/// Returns the duration of a frame as a percent
	double frame_delta() const
	{
		return duration() / static_cast<double>( frame_count() );
	}

	/// Serialization
	friend std::ostream& operator<<( std::ostream& Stream, const time_info& RHS )
	{
		Stream << RHS.frame_count() << " " << RHS.duration() << " " << RHS.frame_index << " " << RHS.position() << " " << RHS.frame_delta();
		return Stream;
	}

private:
	const PageMagickInfo& info;
	const int frame_index;
	const double start_position;
	const double end_position;
};

} // namespace

/** PageMagick frame source - this interface is used to source the frames.
*/

class PageMagickFrames
{
public:
	virtual ~PageMagickFrames() {}
	virtual void Initialise( PageMagickInfo * )
	{ }
	virtual void GetFrame( uint8_t *pixels, int16_t **audio, int i )
	{ }
	virtual void GetFrame( uint8_t *pixels, int width, int height, int16_t **audio, int i )
	{ }
	virtual void Close()
	{ }
	virtual bool IsSynth( )
	{
		return false;
	}
};

/** PageMagic Image manipulator - this interface is used to manipulate the images.
*/

class PageMagickImage
{
public:
	virtual ~PageMagickImage() {}
	virtual void Initialise( PageMagickInfo *info )
	{ }
	;
	virtual void PreGetFrame( int keyPosition = -1 ) // -1 = automatic
	{ }
	virtual void PreGetFrameAudio( int keyPosition = -1 ) // -1 = automatic
	{ }
	;
	virtual void GetFrame( uint8_t *pixels, int i )
	{ }
	;
	virtual void Close()
	{ }
	;
	virtual bool ChangesImage( )
	{
		return true;
	}
};

/** PageMagic Audio manipulator - this interface is used to manipulate the audio.
*/

class PageMagickAudio
{
public:
	virtual ~PageMagickAudio() {}
	virtual void Initialise( PageMagickInfo * )
	{ }
	;
	virtual void GetFrame( int16_t **audio, int i, int& samples, int locked_samples )
	{ }
	;
	virtual void Close()
	{ }
	;
};

/** Implementation of the Overwrite frame source.
*/

class PageMagickOverwrite : public PageMagickFrames
{
private:
	PageMagickInfo *info;
public:
	void Initialise( PageMagickInfo * );
	void GetFrame( uint8_t *pixels, int16_t **audio, int i );
	void GetFrame( uint8_t *pixels, int width, int height, int16_t **audio, int i );
	void Close();
};

void PageMagickOverwrite::Initialise( PageMagickInfo *info )
{
	this->info = info;
	// Obtain begining and ending of sequence
	info->SetBegin( 0 );
	info->SetEnd( info->GetCommon() ->getPlayList() ->GetNumFrames() - 1 );
	info->increment = 1;
	info->reverse = false;

	GtkEntry *startSpin = GTK_ENTRY( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "spinbutton_magick_start" ) );
	GtkEntry *endSpin = GTK_ENTRY( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "spinbutton_magick_end" ) );

	if ( info->GetCommon() ->getPlayList() ->GetNumFrames() != 0 )
	{
		info->SetBegin( atoi( gtk_entry_get_text( startSpin ) ) );
		info->SetEnd( atoi( gtk_entry_get_text( endSpin ) ) );
		GtkToggleButton *limit = GTK_TOGGLE_BUTTON( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "checkbutton_magick_frame_limit" ) );
		if ( gtk_toggle_button_get_active( limit ) )
		{
			GtkEntry * spin = GTK_ENTRY( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "spinbutton_magick_limit" ) );
			GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "optionmenu_magick_frame_offset" ) ) ) );
			GtkWidget *active_item = gtk_menu_get_active( menu );
			if ( g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item ) == 1 )
			{
				int end = info->begin + atoi( gtk_entry_get_text( spin ) ) - 1;
				if ( end < info->end )
					info->SetEnd( end );
			}
			else
			{
				info->SetBegin( info->end - atoi( gtk_entry_get_text( spin ) ) + 1 );
			}
		}
		info->SetAnteFrame( info->begin - 1 );
		info->SetPostFrame( info->end + 1 );
	}
	else
	{
 		throw _( "No frames available for rewriting." );
	}

	// Determine speed
	GtkToggleButton *speed = GTK_TOGGLE_BUTTON( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "checkbutton_speed" ) );
	if ( gtk_toggle_button_get_active( speed ) )
	{
		GtkRange * range = GTK_RANGE( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "hscale_speed" ) );
		info->increment = range->adjustment->value;
	}

	// Determine direction
	GtkToggleButton *reverse = GTK_TOGGLE_BUTTON( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "checkbutton_reverse" ) );
	info->reverse = gtk_toggle_button_get_active( reverse );
}

void PageMagickOverwrite::GetFrame( uint8_t *pixels, int16_t **audio, int i )
{
	Frame* infoFrame = GetFramePool()->GetFrame();
	common->getPlayList() ->GetFrame( i, *( infoFrame ) );

	if ( pixels )
	{
		infoFrame->decoder->quality = DV_QUALITY_BEST;
		infoFrame->ExtractRGB( pixels );
	}

	if ( audio )
	{
		AudioInfo ainfo;
		infoFrame->GetAudioInfo( ainfo );
		if ( ainfo.channels )
		{
			AudioResample<int16_ne_t,int16_ne_t> * resampler = AudioResampleFactory<int16_ne_t,int16_ne_t>::createAudioResample(
											AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, info->frequency, false );
			resampler->Resample( *infoFrame );
			info->samples_this_frame = resampler->size / ( 2 * ainfo.channels );
			int16_t *p = resampler->output;
			for ( int s = 0; s < info->samples_this_frame; s++ )
				for ( int c = 0; c < ainfo.channels; c++ )
					audio[ c ][ s ] = *p++;
			delete resampler;
		}
	}
	GetFramePool()->DoneWithFrame( infoFrame );
}

void PageMagickOverwrite::GetFrame( uint8_t *pixels, int width, int height, int16_t **audio, int i )
{
	Frame* infoFrame = GetFramePool()->GetFrame();
	common->getPlayList() ->GetFrame( i, *( infoFrame ) );

	if ( audio )
	{
		AudioInfo ainfo;
		infoFrame->GetAudioInfo( ainfo );
		if ( ainfo.channels )
		{
			AudioResample<int16_ne_t,int16_ne_t> *resampler = AudioResampleFactory<int16_ne_t,int16_ne_t>::createAudioResample(
									AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, info->frequency, false );
			resampler->Resample( *infoFrame );
			info->samples_this_frame = resampler->size / ( 2 * ainfo.channels );
			int16_t *p = resampler->output;
			for ( int s = 0; s < info->samples_this_frame; s++ )
				for ( int c = 0; c < ainfo.channels; c++ )
					audio[ c ][ s ] = *p++;
			delete resampler;
		}
	}
	if ( pixels )
	{
		infoFrame->decoder->quality = DV_QUALITY_BEST;
		infoFrame->ExtractRGB( pixels );
		if ( ( infoFrame->GetWidth() != width || infoFrame->GetHeight() != height ) )
		{
			GdkPixbuf * i1 = gdk_pixbuf_new_from_data( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
							infoFrame->GetWidth(), infoFrame->GetHeight(), infoFrame->GetWidth() * 3, NULL, NULL );
			GdkPixbuf *i2 = gdk_pixbuf_scale_simple( i1, width, height, GDK_INTERP_HYPER );
			memcpy( pixels, gdk_pixbuf_get_pixels( i2 ), width * height * 3 );
			g_object_unref( i2 );
			g_object_unref( i1 );
		}
	}
	GetFramePool()->DoneWithFrame( infoFrame );
}

void PageMagickOverwrite::Close( )
{
	cerr << ">>> Deleting " << info->begin << " << " << info->end << endl;
	info->GetCommon() ->getPlayList() ->Delete( info->begin, info->end );
}

/** Implementation of the Create frame source.
*/

class PageMagickCreate : public PageMagickFrames
{
private:
	PageMagickInfo *info;
	uint8_t *image;
	GDKImageCreate *creator;

public:
	PageMagickCreate();
	virtual ~PageMagickCreate();
	void Initialise( PageMagickInfo * );
	void GetFrame( uint8_t *pixels, int16_t **audio, int i );
	void GetFrame( uint8_t *pixels, int width, int height, int16_t **audio, int i );
	virtual bool IsSynth( )
	{
		return true;
	}
};

PageMagickCreate::PageMagickCreate() : image( NULL )
{
	image = new uint8_t[ 720 * 576 * 3 ];
}

PageMagickCreate::~PageMagickCreate()
{
	delete[] image;
}

void PageMagickCreate::Initialise( PageMagickInfo *info )
{
	this->info = info;
	// Get the start of the scene
	info->SetBegin( common->getPlayList()->FindStartOfScene( common->g_currentFrame ) );
	info->increment = 1;
	info->reverse = false;
	info->SetAnteFrame( info->begin - 1 );
	info->SetPostFrame( info->begin );

/* Commented to simplify UI to always create before current scene
	GtkToggleButton *frameToggle = GTK_TOGGLE_BUTTON( lookup_widget( common->getPageMagick() ->window, "radiobutton_magick_create_before" ) );
	if ( gtk_toggle_button_get_active( frameToggle ) )
	{
		GtkEntry * spin = GTK_ENTRY( lookup_widget( common->getPageMagick() ->window, "spinbutton_magick_create_before" ) );
		info->begin = atoi( gtk_entry_get_text( spin ) );
	}
*/

	creator = info->GetCommon() ->getPageMagick() ->GetImageCreate();
	if ( creator != NULL )
	{
		creator->CreatePAL( info->isPAL );
		creator->InterpretWidgets( GTK_BIN( lookup_widget( common->getPageMagick() ->window, "frame_magick_frames_create" ) ) );
	}
	else
		throw _( "Invalid image creator selected" );

	// Indicate proper duration
	info->SetEnd( info->begin + creator->GetNumberOfFrames() - 1 );
}

void PageMagickCreate::GetFrame( uint8_t *pixels, int16_t **audio, int i )
{
	// Sanity checks ...
	if ( !creator )
		throw _( "Invalid image creator selected" );

	if ( pixels )
	{
		creator->CreateFrame( image, info->width, info->height, time_info( *info, i ).position(), time_info( *info, i ).frame_delta() );
		memcpy( pixels, image, info->width * info->height * 3 );
	}

	if ( audio )
	{
		for ( int i = 0; i < 4; i ++ )
			memset( audio[ i ], 0, 2 * DV_AUDIO_MAX_SAMPLES * sizeof( int16_t ) );
		GDKAudioImport *import = dynamic_cast <GDKAudioImport *>( creator );
		if ( import != NULL )
			import->CreateAudio( audio, &info->channels, &info->frequency, &info->samples_this_frame );
	}
}

void PageMagickCreate::GetFrame( uint8_t *pixels, int width, int height, int16_t **audio, int i )
{
	// Sanity checks ...
	if ( !creator )
		throw _( "Invalid image creator selected" );

	if ( pixels )
	{
		creator->CreateFrame( image, width, height, time_info( *info, i ).position(), time_info( *info, i ).frame_delta() );
		memcpy( pixels, image, width * height * 3 );
	}

	if ( audio )
	{
		for ( int i = 0; i < 4; i ++ )
			memset( audio[ i ], 0, 2 * DV_AUDIO_MAX_SAMPLES * sizeof( int16_t ) );
		GDKAudioImport *import = dynamic_cast <GDKAudioImport *>( creator );
		if ( import != NULL )
			import->CreateAudio( audio, &info->channels, &info->frequency, &info->samples_this_frame );
	}
}

/** Filter implementation
*/

class PageMagickTransition : public PageMagickImage
{
private:
	PageMagickInfo *info;
	GDKImageTransition *transition;
	uint8_t keyFrame[ 720 * 576 * 3 ];
	bool animateKey;
	int direction;
	double start_position;
	double end_position;
public:
	void Initialise( PageMagickInfo *info );
	void PreGetFrame( int keyPosition );
	void PreGetFrameAudio( int keyPosition );
	void GetFrame( uint8_t *pixels, int i );
	void Close();
};

void PageMagickTransition::Initialise( PageMagickInfo *info )
{
	this->info = info;
	this->animateKey = false;

	transition = info->GetCommon() ->getPageMagick() ->GetImageTransition();
	if ( transition != NULL )
		transition->InterpretWidgets( GTK_BIN( lookup_widget( common->getPageMagick() ->window, "frame_magick_image_transition" ) ) );
	else
		throw _( "Invalid image transition selected" );

	GtkToggleButton *toggle = GTK_TOGGLE_BUTTON( lookup_widget( common->getPageMagick() ->window, "radiobutton_magick_transition_colour" ) );

	if ( gtk_toggle_button_get_active( toggle ) )
	{
		GdkColor color;
		GtkColorButton * colorButton = GTK_COLOR_BUTTON( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "colorpicker_magick_transition" ) );
		gtk_color_button_get_color( colorButton, &color );
		
		for ( uint8_t * p = keyFrame; p < ( keyFrame + info->width * info->height * 3 ); )
		{
			*p ++ = color.red >> 8;
			*p ++ = color.green >> 8;
			*p ++ = color.blue >> 8;
		}
	}
	else
	{
		GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "optionmenu_magick_transition_frame" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		switch ( g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item ) )
		{
		case 1:
			info->GetPostFrame( keyFrame );
			break;
		case 2:
			info->GetAnteFrame( keyFrame );
			break;
		case 0:
			animateKey = true;
			break;
		}
	}

	GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "optionmenu_direction" ) ) ) );
	GtkWidget *active_item = gtk_menu_get_active( menu );
	direction = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );

	GtkRange *range = GTK_RANGE( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "hscale_transition_start" ) );
	start_position = range->adjustment->value;
	range = GTK_RANGE( lookup_widget( info->GetCommon() ->getPageMagick() ->window, "hscale_transition_end" ) );
	end_position = range->adjustment->value;
}

void PageMagickTransition::PreGetFrame( int keyPosition )
{
	if ( animateKey )
	{
		if ( keyPosition > -1 )
			info->postFrame = info->end + keyPosition;
		info->GetPostFrame( keyFrame );
	}
	info->postFrame ++;
}

void PageMagickTransition::PreGetFrameAudio( int keyPosition )
{
	if ( animateKey )
	{
		if ( keyPosition > -1 )
			info->postFrame = info->end + keyPosition;
	}
	info->postFrame ++;
}

void PageMagickTransition::GetFrame( uint8_t *pixels, int i )
{
	// Sanity checks ...
	if ( !transition )
		throw _( "Invalid image transition selected" );

	transition->GetFrame( pixels, keyFrame, info->width, info->height, time_info( *info, i, start_position, end_position ).position(), time_info( *info, i, start_position, end_position ).frame_delta(), direction == 1 );
}

void PageMagickTransition::Close( )
{
	if ( this->animateKey )
	{
		cerr << ">>> Deleting " << info->begin << " << " << info->begin + ( info->end - info->begin ) / info->increment << endl;
		info->GetCommon() ->getPlayList() ->Delete( info->begin, ( int ) ( info->begin + ( info->end - info->begin ) / info->increment ) );
	}
}

/** Filter implementation
*/

class PageMagickFilter : public PageMagickImage
{
private:
	PageMagickInfo *info;
	GDKImageFilter *filter;
public:
	void Initialise( PageMagickInfo *info );
	void GetFrame( uint8_t *pixels, int i );
	bool ChangesImage( );
	void PreGetFrame( int keyPosition )
	{
		if ( keyPosition > -1 )
			info->postFrame = info->end + keyPosition;
		else
			info->postFrame ++;
	}
	void PreGetFrameAudio( int keyPosition )
	{
		PreGetFrame( keyPosition );
	}
};

void PageMagickFilter::Initialise( PageMagickInfo *info )
{
	this->info = info;
	filter = info->GetCommon() ->getPageMagick() ->GetImageFilter();
	if ( filter != NULL )
		filter->InterpretWidgets( GTK_BIN( lookup_widget( common->getPageMagick() ->window, "frame_magick_image_filter" ) ) );
	else
		throw _( "Invalid image filter selected" );
}

void PageMagickFilter::GetFrame( uint8_t *pixels, int i )
{
	// Sanity checks ...
	if ( !filter )
		throw _( "Invalid image filter selected" );

	filter->FilterFrame( pixels, info->width, info->height, time_info( *info, i ).position(), time_info( *info, i ).frame_delta() );
}

bool PageMagickFilter::ChangesImage( )
{
	NullImageFilter * no_image_encode = dynamic_cast <NullImageFilter *>( filter );
	return no_image_encode == NULL;
}

/** PageMagic Audio Filter implementation.
*/

class PageMagickAudioFilter : public PageMagickAudio
{
private:
	PageMagickInfo *info;
	GDKAudioFilter *filter;
public:
	void Initialise( PageMagickInfo *info );
	void GetFrame( int16_t **audio, int i, int& samples, int locked_samples );
};

void PageMagickAudioFilter::Initialise( PageMagickInfo *info )
{
	this->info = info;
	filter = info->GetCommon() ->getPageMagick() ->GetAudioFilter();
	if ( filter != NULL )
		filter->InterpretWidgets( GTK_BIN( lookup_widget( common->getPageMagick() ->window, "frame_magick_audio_filter" ) ) );
	else
		throw _( "Invalid audio filter selected" );
}

void PageMagickAudioFilter::GetFrame( int16_t **audio, int i, int& samples, int locked_samples )
{
	// Sanity checks ...
	if ( !filter )
		throw _( "Invalid audio filter selected" );
	if ( !filter->IsAFrameConsumer() )
		samples = locked_samples;
	filter->GetFrame( audio, info->frequency, info->channels, samples, time_info( *info, i ).position(), time_info( *info, i ).frame_delta() );
}

/** PageMagick Audio Transition implementation.
*/

class PageMagickAudioTransition : public PageMagickAudio
{
private:
	PageMagickInfo *info;
	int16_t *audio_buffers[ 4 ];
	GDKAudioTransition *transition;
AudioResample<int16_ne_t,int16_ne_t> * resampler;
public:
	PageMagickAudioTransition();
	virtual ~PageMagickAudioTransition();
	void Initialise( PageMagickInfo *info );
	void GetFrame( int16_t **audio, int i, int& samples, int locked_samples );
};

PageMagickAudioTransition::PageMagickAudioTransition() : resampler( 0 )
{
	for ( int index = 0; index < 4; index ++ )
		audio_buffers[ index ] = new int16_t[ 2 * DV_AUDIO_MAX_SAMPLES ];
}

PageMagickAudioTransition::~PageMagickAudioTransition()
{
	for ( int index = 0; index < 4; index ++ )
		delete[] audio_buffers[ index ];
	delete resampler;
}

void PageMagickAudioTransition::Initialise( PageMagickInfo *info )
{
	this->info = info;

	transition = info->GetCommon() ->getPageMagick() ->GetAudioTransition();
	if ( transition != NULL )
		transition->InterpretWidgets( GTK_BIN( lookup_widget( common->getPageMagick() ->window, "frame_magick_audio_transition" ) ) );
	else
		throw _( "Invalid audio transition selected" );
}

void PageMagickAudioTransition::GetFrame( int16_t **audio, int i, int& samples, int locked_samples )
{
	// Sanity checks ...
	if ( !transition )
		throw _( "Invalid audio filter selected" );

	if ( transition->IsBFrameConsumer() )
	{
		// Pick up the b-frame audio (ugly.. moved by image transition when appropriate)
		// should also be resampled to the a-frames frequency - this is going to go haywire in
		// mixed sample rate projects :-/
		// In truth, even the aframes should be resampled to original aframes frequency... (fun, fun, fun)
		Frame* infoFrame = GetFramePool()->GetFrame();
		AudioInfo ainfo;
		common->getPlayList()->GetFrame( info->GetPostFrame() - 1, *( infoFrame ) );
		infoFrame->GetAudioInfo( ainfo );
		if ( ainfo.channels )
		{
			if ( !resampler )
				resampler = AudioResampleFactory<int16_ne_t,int16_ne_t>::createAudioResample(
					AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, info->frequency, true );
			resampler->Resample( *infoFrame );
			int16_t *p = resampler->output;
			samples = resampler->size / info->channels / 2;
			for ( int s = 0; s < samples; s++ )
				for ( int c = 0; c < info->channels; c++ )
					audio_buffers[ c ][ s ] = *p++;
		}
		GetFramePool()->DoneWithFrame( infoFrame );
	}
	else
	{
		samples = locked_samples;
	}
	transition->GetFrame( audio, audio_buffers, info->frequency, info->channels, 
		samples, time_info( *info, i ).position(), time_info( *info, i ).frame_delta() );
}

/** Info implementation.
*/

PageMagickInfo::PageMagickInfo( KinoCommon *common ) : preview( false )
{
	this->common = common;
	this->frameSource = NULL;
	this->imageManipulator = NULL;
	this->audioManipulator = NULL;
}

PageMagickInfo::~PageMagickInfo()
{
	delete frameSource;
	delete imageManipulator;
	delete audioManipulator;
}

void PageMagickInfo::Initialise()
{
	Frame* infoFrame = GetFramePool()->GetFrame();
	
	// Get a sample frame to obtain recording info
	if ( common->getPlayList() ->GetFrame( 0, *infoFrame ) )
	{
		// Get all video and audio info required
		width = infoFrame->GetWidth();
		height = infoFrame->GetHeight();
		isPAL = infoFrame->IsPAL();
		isWide = infoFrame->IsWide();
		AudioInfo info;
		infoFrame->GetAudioInfo( info );
		if ( info.channels && info.frequency )
		{
			channels = info.channels;
			frequency = info.frequency;
			samples_this_frame = frequency / ( isPAL ? 25 : 30 );
		}
		else
		{
			channels = ( short ) 2;
			switch ( Preferences::getInstance().defaultAudio )
			{
			case AUDIO_32KHZ:
				frequency = 32000;
				break;
			case AUDIO_44KHZ:
				frequency = 44100;
				break;
			case AUDIO_48KHZ:
				frequency = 48000;
				break;
			}
			samples_this_frame = frequency / ( isPAL ? 25 : 30 );
		}
	}
	else
	{
		if ( Preferences::getInstance().defaultNormalisation != NORM_UNSPECIFIED )
		{
			isPAL = Preferences::getInstance().defaultNormalisation == NORM_PAL;
		}
		else
		{
			switch ( modal_confirm( "PAL", "NTSC", _("Please choose a video standard") ) )
			{
			case GTK_RESPONSE_ACCEPT:
				isPAL = true;
				Preferences::getInstance().defaultNormalisation = NORM_PAL;
				break;
			case GTK_RESPONSE_CLOSE:
				isPAL = false;
				Preferences::getInstance().defaultNormalisation = NORM_NTSC;
				break;
			default:
				// Do nothing - action cancelled
				GetFramePool()->DoneWithFrame( infoFrame );
				throw _("Aborted");
			}
		}
		width = 720;
		height = isPAL ? 576 : 480;
		isWide = Preferences::getInstance().defaultAspect == ASPECT_169;
		channels = ( short ) 2;
		switch ( Preferences::getInstance().defaultAudio )
		{
		case AUDIO_32KHZ:
			frequency = 32000;
			break;
		case AUDIO_44KHZ:
			frequency = 44100;
			break;
		case AUDIO_48KHZ:
			frequency = 48000;
			break;
		}
		samples_this_frame = frequency / ( isPAL ? 25 : 30 );
	}
	GetFramePool()->DoneWithFrame( infoFrame );

	preview = false;
}

void PageMagickInfo::SetLowQuality( bool low_quality )
{
	if ( preview != low_quality )
	{
		if ( low_quality )
		{
			width /= 4;
			height /= 4;
		}
		else
		{
			width *= 4;
			height *= 4;
		}

		preview = low_quality;
	}
}

PageMagickFrames *PageMagickInfo::GetFrameSource()
{
	if ( frameSource == NULL )
	{
		GtkNotebook * notebook = GTK_NOTEBOOK( lookup_widget( this->common->getPageMagick() ->window, "notebook_magick_frames" ) );
		int page = gtk_notebook_get_current_page( notebook );

		switch ( page )
		{
		case 0:
			frameSource = new PageMagickOverwrite();
			break;
		case 1:
			frameSource = new PageMagickCreate();
			break;
		}
	}

	if ( frameSource != NULL )
		frameSource->Initialise( this );
	else
		throw _( "The requested frame source has not been implemented yet..." );

	return frameSource;
}

PageMagickImage *PageMagickInfo::GetImageManipulator()
{
	if ( imageManipulator == NULL )
	{
		GtkNotebook * notebook = GTK_NOTEBOOK( lookup_widget( this->common->getPageMagick() ->window, "notebook_magick_video" ) );
		int page = gtk_notebook_get_current_page( notebook );

		switch ( page )
		{
		case 0:
			imageManipulator = new PageMagickFilter();
			break;
		case 1:
			imageManipulator = new PageMagickTransition();
			break;
		}
	}

	if ( imageManipulator != NULL )
		imageManipulator->Initialise( this );
	else
		throw _( "The requested image manipulator has not been implemented yet..." );

	return imageManipulator;
}

PageMagickAudio *PageMagickInfo::GetAudioManipulator()
{
	if ( audioManipulator == NULL )
	{
		GtkNotebook * notebook = GTK_NOTEBOOK( lookup_widget( this->common->getPageMagick() ->window, "notebook_magick_audio" ) );
		int page = gtk_notebook_get_current_page( notebook );

		switch ( page )
		{
		case 0:
			audioManipulator = new PageMagickAudioFilter();
			break;
		case 1:
			audioManipulator = new PageMagickAudioTransition();
			break;
		}
	}

	if ( audioManipulator != NULL )
		audioManipulator->Initialise( this );
	else
		throw _( "The requested audio manipulator has not been implemented yet..." );

	return audioManipulator;
}

void PageMagickInfo::GetAnteFrame( uint8_t *pixels )
{
	if ( this->anteFrame >= 0 )
	{
		Frame* infoFrame = GetFramePool()->GetFrame();
		common->getPlayList() ->GetFrame( this->anteFrame, *infoFrame );
		infoFrame->decoder->quality = DV_QUALITY_BEST;
		infoFrame->ExtractRGB( pixels );
		if ( preview )
		{
			GdkPixbuf * i1 = gdk_pixbuf_new_from_data( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
			                 width * 4, height * 4, width * 4 * 3, NULL, NULL );
			GdkPixbuf *i2 = gdk_pixbuf_scale_simple( i1, width, height, GDK_INTERP_NEAREST );
			memcpy( pixels, gdk_pixbuf_get_pixels( i2 ), width * height * 3 );
			g_object_unref( i2 );
			g_object_unref( i1 );
		}
		GetFramePool()->DoneWithFrame( infoFrame );
	}
	else
	{
		memset( pixels, 0, 720 * 576 * 3 );
	}
}

void PageMagickInfo::GetPostFrame( uint8_t *pixels )
{
	if ( this->postFrame < common->getPlayList() ->GetNumFrames() )
	{
		Frame* infoFrame = GetFramePool()->GetFrame();
		common->getPlayList() ->GetFrame( this->postFrame, *infoFrame );
		infoFrame->decoder->quality = DV_QUALITY_BEST;
		infoFrame->ExtractRGB( pixels );
		if ( preview )
		{
			GdkPixbuf * i1 = gdk_pixbuf_new_from_data( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
			                 width * 4, height * 4, width * 4 * 3, NULL, NULL );
			GdkPixbuf *i2 = gdk_pixbuf_scale_simple( i1, width, height, GDK_INTERP_NEAREST );
			memcpy( pixels, gdk_pixbuf_get_pixels( i2 ), width * height * 3 );
			g_object_unref( i2 );
			g_object_unref( i1 );
		}
		GetFramePool()->DoneWithFrame( infoFrame );
	}
	else
	{
		memset( pixels, 0, 720 * 576 * 3 );
	}
}

static void on_drawingarea_refresh_required( GtkWidget *some_widget, GdkEventConfigure *event, gpointer user_data );

/** Constructor for Page Magick.
*/

PageMagick::PageMagick( KinoCommon *common ) : 
	last_page( 0 ),
	rendering( false ),
	previewing( false ),
	repainting( false ),
	previewPosition( 0 ),
	keyFrameControllerClient( 0 ),
	isGuiLocked( false ),
	isPreviousScene( false ),
	isNextScene( false )
{
	cerr << "> Creating Magick Page" << endl;

	window = glade_xml_get_widget( magick_glade, "window_magick" );
	GtkWidget *bin = lookup_widget( common->getWidget(), "frame_magick" );
	gtk_widget_reparent( gtk_bin_get_child( GTK_BIN( window ) ), bin );

	this->common = common;

	progressBar = GTK_PROGRESS_BAR( lookup_widget( common->getWidget(), "progressbar" ) );
	cerr << ">> Searching " << KINO_PLUGINDIR << " for plugins" << endl;
	plugins.Initialise( KINO_PLUGINDIR );

	for ( unsigned int index = 0; index < plugins.Count(); index ++ )
	{
		image_creators.InstallPlugins( plugins.Get( index ) );
		image_filters.InstallPlugins( plugins.Get( index ) );
		image_transitions.InstallPlugins( plugins.Get( index ) );
		audio_filters.InstallPlugins( plugins.Get( index ) );
		audio_transitions.InstallPlugins( plugins.Get( index ) );
	}

	GtkOptionMenu *menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_magick_filter" ) );
	GtkBin *container = GTK_BIN( lookup_widget( window, "frame_magick_image_filter" ) );
	image_filters.Initialise( menu, container );
	
	menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_magick_frames_create" ) );
	container = GTK_BIN( lookup_widget( window, "frame_magick_frames_create" ) );
	image_creators.Initialise( menu, container );

	menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_magick_transition" ) );
	container = GTK_BIN( lookup_widget( window, "frame_magick_image_transition" ) );
	image_transitions.Initialise( menu, container );

	menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_magick_audio_filter" ) );
	container = GTK_BIN( lookup_widget( window, "frame_magick_audio_filter" ) );
	audio_filters.Initialise( menu, container );

	menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_magick_audio_transition" ) );
	container = GTK_BIN( lookup_widget( window, "frame_magick_audio_transition" ) );
	audio_transitions.Initialise( menu, container );

	GtkWidget *drawing_area = lookup_widget( window, "drawingarea_magick_preview" );
	g_signal_connect( G_OBJECT( drawing_area ), "expose_event", G_CALLBACK( on_drawingarea_refresh_required ), this );
	gtk_widget_set_double_buffered( drawing_area, FALSE );

	GtkWidget *widget = lookup_widget( window, "notebook_magick_frames" );
	g_signal_connect_after( G_OBJECT( widget ), "switch_page", G_CALLBACK( on_notebook_magick_switch_page ), this );
	widget = lookup_widget( window, "notebook_magick_video" );
	g_signal_connect_after( G_OBJECT( widget ), "switch_page", G_CALLBACK( on_notebook_magick_switch_video_page ), NULL );
	widget = lookup_widget( window, "notebook_magick_audio" );
	g_signal_connect_after( G_OBJECT( widget ), "switch_page", G_CALLBACK( on_notebook_magick_switch_page ), NULL );

	widget = lookup_widget( window, "checkbutton_magick_frame_limit" );
	g_signal_connect( G_OBJECT( widget ), "clicked", G_CALLBACK( on_time_range_changed ), NULL );
	widget = lookup_widget( window, "spinbutton_magick_limit" );
	gtk_spin_button_set_value( GTK_SPIN_BUTTON( widget ), Preferences::getInstance().defaultNormalisation == NORM_NTSC ? 30 : 25 );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( on_spinbutton_magick_limit_value_changed ), NULL );
	widget = lookup_widget( window, "optionmenu_magick_frame_offset" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( on_time_range_changed ), NULL );

	// Attach the scrub bar
	scrubAdjustment = GTK_ADJUSTMENT( gtk_adjustment_new( 0, 0, 0, 1, 10, 0 ) );
	scrubBar = gtk_enhanced_scale_new( ( GtkObject** ) & scrubAdjustment, 1 );
	gtk_widget_set_name( scrubBar, "scrubmar_magick" );
	gtk_widget_ref( scrubBar );
	gtk_object_set_data_full( GTK_OBJECT( window ), "scrubbar_magick", scrubBar,
	                          ( GtkDestroyNotify ) gtk_widget_unref );
	GtkWidget *vbox_scrub = lookup_widget( window, "vbox_scrub" );
	gtk_widget_show( scrubBar );
	gtk_box_pack_start( GTK_BOX( vbox_scrub ), scrubBar, FALSE, TRUE, 0 );
	g_signal_connect( G_OBJECT( scrubAdjustment ), "value_changed",
	                  G_CALLBACK( on_scrubbar_magick_value_changed_event ), NULL );
	g_signal_connect( G_OBJECT( scrubBar ), "button_press_event",
	                  G_CALLBACK( on_scrubbar_magick_button_press_event ), NULL );
	g_signal_connect( G_OBJECT( scrubBar ), "button_release_event",
	                  G_CALLBACK( on_scrubbar_magick_button_release_event ), NULL );
	widget = lookup_widget( window, "togglebutton_key_frame" );
	g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( on_togglebutton_key_frame_toggled ), this );
	widget = lookup_widget( window, "optionmenu_direction" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), this );
	widget = lookup_widget( window, "radiobutton_magick_transition_frame" );
	g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), this );
	widget = lookup_widget( window, "optionmenu_magick_transition_frame" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), this );
	widget = lookup_widget( window, "radiobutton_magick_transition_colour" );
	g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), this );
	widget = lookup_widget( window, "colorpicker_magick_transition" );
	g_signal_connect( G_OBJECT( widget ), "color-set", G_CALLBACK( Repaint ), this );
	widget = lookup_widget( window, "hscale_transition_start" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), this );
	widget = lookup_widget( window, "hscale_transition_end" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), this );
}

/** Destructor for Page Magick.
*/

PageMagick::~PageMagick()
{
	GtkBin * bin = GTK_BIN( lookup_widget( common->getWidget(), "frame_magick" ) );
	gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	gtk_widget_destroy( window );
}

void PageMagick::newFile( )
{
	last_fx_file = "";
}

/** Start when entering page.
*/

void PageMagick::start()
{
	cerr << ">>> Starting magick" << endl;
	strcpy( status, "" );

	if ( common->getPlayList() ->GetNumFrames() > 0 )
	{
		int begin = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
		int end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );

		GtkSpinButton *startSpin = GTK_SPIN_BUTTON( lookup_widget( window, "spinbutton_magick_start" ) );
		GtkSpinButton *endSpin = GTK_SPIN_BUTTON( lookup_widget( window, "spinbutton_magick_end" ) );
		gtk_spin_button_set_range( startSpin, 0, end );
		gtk_spin_button_set_value( startSpin, begin );
		gtk_spin_button_set_range( endSpin, begin, common->getPlayList() ->GetNumFrames() - 1 );
		gtk_spin_button_set_value( endSpin, end );

		previewPosition = common->g_currentFrame - common->getPlayList()->FindStartOfScene( common->g_currentFrame );
	}
	else
	{
		GtkNotebook * notebook = GTK_NOTEBOOK( lookup_widget( this->window, "notebook_magick_frames" ) );
		gtk_notebook_set_current_page( notebook, 1 );
	}

	// Default the file name
	GtkEntry *fileEntry = GTK_ENTRY( lookup_widget( window, "entry_magick_file" ) );
	string fx_file_name = common->getPlayList( ) ->GetProjectDirectory( ) + "/";

	if ( fx_file_name != last_fx_file )
	{
		last_fx_file = fx_file_name;
		gtk_entry_set_text( fileEntry, fx_file_name.c_str() );
	}

	audio_transitions.SelectionChange();
	gtk_notebook_set_page( GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_keyhelp" ) ), 3 );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_fx" ) ), TRUE );
	
	timeFormatChanged();
	RefreshStatus();
}

/** Activate scene list selection (for Frame sources).
*/

gulong PageMagick::activate()
{
	if ( rendering )
		return VIDEO_PLAY | VIDEO_STOP;
	else
		return SCENE_LIST |
			VIDEO_START_OF_MOVIE |
			VIDEO_BACK |
			VIDEO_PLAY |
			VIDEO_PAUSE |
			VIDEO_STOP |
			VIDEO_FORWARD |
			VIDEO_END_OF_MOVIE |
			( isPreviousScene ? VIDEO_START_OF_SCENE : 0 ) |
			( isNextScene ? VIDEO_NEXT_SCENE : 0 );
}

/** Clean up when leaving page.
*/

void PageMagick::clean()
{
	StopPreview();
	strcpy( status, "" );
}

/** Play an audio frame.
*/

void PageMagick::PlayAudio( int16_t *buffers[], int samples, int frequency, int channels, bool isPAL )
{
	dv_audio.frequency = frequency;
	dv_audio.samples_this_frame = samples;
	dv_audio.num_channels = channels;

	if ( !audio_device_avail && ( audio_sampling_rate = kino_sound_init( 
			&dv_audio, audio_device, Preferences::getInstance().audioDevice ) ) != 0 )
		audio_device_avail = true;
	if ( audio_device_avail )
		kino_sound_play( &dv_audio, audio_device, buffers );
}

/** Show an image in the preview drawing area.
*/

void PageMagick::ShowImage( GtkWidget *area, uint8_t *image, int width, int height, bool isWide )
{
	GdkPixbuf * pix = gdk_pixbuf_new_from_data( image, GDK_COLORSPACE_RGB, FALSE, 8, width, height, width * 3, NULL, NULL );
	GdkPixbuf *im = gdk_pixbuf_scale_simple( pix, area->allocation.width, area->allocation.height, GDK_INTERP_NEAREST );
	GdkGC *gc = gdk_gc_new( area->window );
	GtkWidget* aspectFrame = lookup_widget( area, "aspectframe1" );
	gtk_aspect_frame_set( GTK_ASPECT_FRAME( aspectFrame ), 0.5, 0, isWide ? (16.0/9.0) : (4.0/3.0), FALSE );
	gdk_draw_pixbuf( area->window, gc, im, 0, 0, area->allocation.x, area->allocation.y, -1, -1, GDK_RGB_DITHER_NORMAL, 0, 0 );
	g_object_unref( im );
	g_object_unref( pix );
	g_object_unref( gc );
}

/** Update UI with metadata
*/

void PageMagick::showFrameInfo( int frame_number )
{
	PageMagickInfo *info = new PageMagickInfo( common );
	info->GetFrameSource();
	int duration = info->end - info->begin + 1;
	delete info;
	showFrameInfo( frame_number, duration );
}

void PageMagick::showFrameInfo( int frame_number, int duration )
{
	GtkLabel* positionLabelCurrent = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_current" ) );
	GtkLabel* positionLabelTotal = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_total" ) );

	gtk_adjustment_set_value( scrubAdjustment, ( gfloat ) previewPosition );
	if ( common->getPlayList()->GetNumFrames() > 0 && duration > 0 )
	{
		Frame &frame = *( GetFramePool( ) ->GetFrame( ) );
		FileHandler *media;

		common->g_currentFrame = common->getPlayList()->FindStartOfScene( common->g_currentFrame ) + previewPosition;
		if ( common->getPlayList()->GetMediaObject( frame_number, &media ) &&
			common->getPlayList()->GetFrame( frame_number, frame ) )
		{
			common->getTime().setFramerate( frame.GetFrameRate() );
			string tc = "<span size=\"x-large\">" + common->getTime().parseFramesToString( frame_number, common->getTimeFormat() ) + "</span>";
			gtk_label_set_markup( positionLabelCurrent, tc.c_str() );
			gtk_widget_set_redraw_on_allocate( GTK_WIDGET( positionLabelCurrent ), FALSE );
			tc = _("Duration: ") + common->getTime().parseFramesToString( duration, common->getTimeFormat() );
			gtk_label_set_markup( positionLabelTotal, tc.c_str() );
			common->showFrameMoreInfo( frame, media );
		}
		GetFramePool( ) ->DoneWithFrame( &frame );
	}
	else
	{
		gtk_label_set_text( positionLabelCurrent, "" );
		gtk_label_set_text( positionLabelTotal, "" );
	}
}

void PageMagick::movedToFrame( int frame_number )
{
	if ( common->hasListChanged == TRUE )
	{
		common->getPageEditor()->ResetBar();
		common->hasListChanged = FALSE;
		if ( common->getPlayList() ->GetNumFrames() > 0 )
		{
			std::vector<int> scenes = common->getPageEditor()->GetScene();
			int i = 0;
			for ( i = 0; i < int( scenes.size() ) && frame_number >= scenes[ i ]; i++ );
			selectScene( i );
		}
		GtkSpinButton *startSpin = GTK_SPIN_BUTTON( lookup_widget( window, "spinbutton_magick_start" ) );
		GtkSpinButton *endSpin = GTK_SPIN_BUTTON( lookup_widget( window, "spinbutton_magick_end" ) );
		gtk_spin_button_set_range( startSpin, 0, gtk_spin_button_get_value( endSpin ) );
		gtk_spin_button_set_range( endSpin, gtk_spin_button_get_value( startSpin ),
			common->getPlayList() ->GetNumFrames() - 1 );
	}
	else
	{
		try {
			StopPreview();
			previewPosition = frame_number;
			PreviewFrame();
		} catch ( const char * exc )
		{ }
	}
}

void PageMagick::windowMoved()
{
	if ( ! previewing )
		movedToFrame( previewPosition );
}

void PageMagick::PreviewFrame()
{
	// Make sure we're not already previewing.
	// Do not interrupt rendering
	if ( rendering || previewing || repainting )
		return;
	repainting = true;

	PageMagickInfo *info = new PageMagickInfo( common );
	info->Initialise();
	PageMagickFrames *frames = info->GetFrameSource();
	GtkWidget *area = glade_xml_get_widget( magick_glade, "drawingarea_magick_preview" );
	if ( previewPosition > info->end - info->begin )
		previewPosition = info->end - info->begin;

	try
	{
		if ( GDK_IS_DRAWABLE(area->window) && ( info->begin <= info->end ) )
		{
			GtkWidget *qualityButton = glade_xml_get_widget( magick_glade, "checkbutton_low_quality" );
			info->SetLowQuality( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( qualityButton ) ) );
			PageMagickImage *image = info->GetImageManipulator();
			double i = double( info->begin ) + previewPosition * info->increment;
			int frame_number = int( i + 0.5 );
			uint8_t *pixels = new uint8_t[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];

			showFrameInfo( frame_number, info->end - info->begin + 1 );

			// Apply the filter and show image
			if ( info->reverse )
				frame_number = int( double( info->end ) - ( i - info->begin ) + 0.5 );
			if ( frame_number > info->end )
				frame_number = info->end;
			frames->GetFrame( pixels, info->width, info->height, NULL, frame_number );
			image->PreGetFrame( frame_number - info->begin );
			image->GetFrame( pixels, frame_number );
			ShowImage( area, pixels, info->width, info->height, info->isWide );
			delete[] pixels;
		}
		else
		{
			showFrameInfo( 0, 0 );
		}
	}
	catch ( const char * exc )
	{
		modal_message( ( char * ) exc );
	}

	repainting = false;
	delete info;
}

/** Provide a preview of the effect.
*/
static pthread_t audioThread;
static pthread_t videoThread;
static pthread_mutex_t init_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;
static int frameNumber;

static int WaitForAction( int lastPosition )
{
	if ( frameNumber == lastPosition )
	{
		pthread_mutex_lock( &condition_mutex );
		pthread_cond_wait( &condition, &condition_mutex );
		pthread_mutex_unlock( &condition_mutex );
	}
	return frameNumber;
}

static void TriggerAction( )
{
	pthread_mutex_lock( &condition_mutex );
	pthread_cond_signal( &condition );
	pthread_mutex_unlock( &condition_mutex );
}


static void* audioThreadProxy( void* data )
{
	PageMagick* o = static_cast< PageMagick* >( data );
	o->AudioThread();
	return NULL;
}

static void* videoThreadProxy( void* data )
{
	PageMagick* o = static_cast< PageMagick* >( data );
	o->VideoThread();
	return NULL;
}

void PageMagick::StartPreview()
{
	previewing = true;
	pthread_create( &audioThread, NULL, audioThreadProxy, this );
	pthread_create( &videoThread, NULL, videoThreadProxy, this );
}

void PageMagick::AudioThread()
{
	// Avoid reentrancy, interrupt rendering, and empty project
	if ( rendering )
		return;

	GtkWidget *area = GTK_WIDGET( lookup_widget( window, "drawingarea_magick_preview" ) );

	// Create the temporary space
	uint8_t *pixels = new uint8_t[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
	int16_t *audio_buffers[ 4 ];
	for ( int n = 0; n < 4; n++ )
		audio_buffers[ n ] = new int16_t [ 2 * DV_AUDIO_MAX_SAMPLES ];

	// Generate the info
	PageMagickInfo *info = new PageMagickInfo( common );

	audio_device = kino_sound_new();
	audio_device_avail = false;
	audio_sampling_rate = 0;
	int audio_number = 0;
	Frame* infoFrame = GetFramePool()->GetFrame();

	GtkToggleButton *audioButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_magick_preview_audio" ) );
	GtkToggleButton *everyButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_magick_preview_every" ) );
	GtkToggleButton *loopButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_magick_preview_loop" ) );
	GtkToggleButton *contextButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_magick_preview_context" ) );
	GtkToggleButton *qualityButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_low_quality" ) );
	GtkEntry *contextSpin = GTK_ENTRY( lookup_widget( window, "spinbutton_magick_preview_context" ) );

	try
	{
		pthread_mutex_lock( &init_mutex );
		info->Initialise();
		info->SetLowQuality( gtk_toggle_button_get_active( qualityButton ) );
		do
		{
			PageMagickFrames *frames = info->GetFrameSource();
			PageMagickImage *image = info->GetImageManipulator();
			PageMagickAudio *sound = info->GetAudioManipulator();
			pthread_mutex_unlock( &init_mutex );

			if ( info->begin > info->end )
				throw _( "Invalid frame range specified." );

			if ( previewPosition <= 1 )
			for ( frameNumber = info->begin - atoi( gtk_entry_get_text( contextSpin ) );
				frameNumber < info->begin &&
				gtk_toggle_button_get_active( contextButton ) &&
				previewing;
				frameNumber++ )
			{
				if ( frameNumber >= 0 )
				{
					TriggerAction();
					common->getPlayList()->GetFrame( frameNumber, *( infoFrame ) );
					if ( !gtk_toggle_button_get_active( audioButton ) &&
							Preferences::getInstance().enableAudio )
					{
						if ( infoFrame->ExtractAudio( audio_buffers ) )
							PlayAudio( audio_buffers, info->samples_this_frame, info->frequency, info->channels, info->isPAL );
					}
					if ( !gtk_toggle_button_get_active( everyButton ) || gtk_toggle_button_get_active( audioButton ) )
					{
						infoFrame->ExtractPreviewRGB( pixels );
						gdk_threads_enter();
						ShowImage( area, pixels, infoFrame->GetWidth(), infoFrame->GetHeight(), infoFrame->IsWide() );
						gdk_flush();
						gdk_threads_leave();
					}
				}
			}

			int frame_number = info->begin + previewPosition;
			for ( double i = ( double ) info->begin + previewPosition;
				frame_number <= info->end && previewing;
				frame_number = ( int )( ( i += info->increment ) + 0.5 ) )
			{
				previewPosition = frame_number - info->begin;
				if ( info->reverse )
					frame_number = info->end - ( ( int ) i - info->begin );

				frameNumber = frame_number;
				TriggerAction();
				if ( !gtk_toggle_button_get_active( audioButton ) &&
					Preferences::getInstance().enableAudio )
				{
					int samples = info->samples_this_frame;
					int locked_samples = infoFrame->CalculateNumberSamples( info->frequency, audio_number ++ );
					if ( samples == 0 )
						samples = locked_samples;
					frames->GetFrame( NULL, 0, 0, audio_buffers, frame_number );
					if ( gtk_toggle_button_get_active( everyButton ) )
						image->PreGetFrameAudio( );
					sound->GetFrame( audio_buffers, frame_number, samples, locked_samples );
					PlayAudio( audio_buffers, samples, info->frequency, info->channels, info->isPAL );
				}
				if ( !gtk_toggle_button_get_active( everyButton ) || gtk_toggle_button_get_active( audioButton ) )
				{
					frames->GetFrame( pixels, info->width, info->height, NULL, frame_number );
					image->PreGetFrame( );
					image->GetFrame( pixels, frame_number );
					gdk_threads_enter();
					ShowImage( area, pixels, info->width, info->height, info->isWide );
					showFrameInfo( ( i += info->increment ) + 0.5, info->end - info->begin + 1 );
					gdk_flush();
					gdk_threads_leave();
				}
			}

			for ( frameNumber = info->postFrame;
				frameNumber < info->postFrame + atoi( gtk_entry_get_text( contextSpin ) ) &&
				gtk_toggle_button_get_active( contextButton ) &&
				previewing;
				frameNumber++ )
			{
				if ( frameNumber < common->getPlayList()->GetNumFrames() )
				{
					TriggerAction();
					common->getPlayList()->GetFrame( frameNumber, *( infoFrame ) );
					if ( !gtk_toggle_button_get_active( audioButton ) &&
							Preferences::getInstance().enableAudio )
					{
						if ( infoFrame->ExtractAudio( audio_buffers ) )
							PlayAudio( audio_buffers, info->samples_this_frame, info->frequency, info->channels, info->isPAL );
					}
					if ( !gtk_toggle_button_get_active( everyButton ) || gtk_toggle_button_get_active( audioButton ) )
					{
						infoFrame->ExtractPreviewRGB( pixels );
						gdk_threads_enter();
						ShowImage( area, pixels, infoFrame->GetWidth(), infoFrame->GetHeight(), infoFrame->IsWide() );
						gdk_flush();
						gdk_threads_leave();
					}
				}
			}
			if ( previewing )
				previewPosition = 0;
			pthread_mutex_lock( &init_mutex );
		}
		while ( previewing && gtk_toggle_button_get_active( loopButton ) );
	}
	catch ( const char * exc )
	{
		modal_message( ( char * ) exc );
	}
	pthread_mutex_unlock( &init_mutex );

	kino_sound_close( audio_device );
	previewing = false;
	delete info;
	delete[] pixels;
	for ( int n = 0; n < 4; n++ )
		delete[] audio_buffers[ n ];
    GetFramePool()->DoneWithFrame( infoFrame );
	TriggerAction();
}

void PageMagick::VideoThread()
{
	GtkWidget *area = GTK_WIDGET( lookup_widget( window, "drawingarea_magick_preview" ) );
	uint8_t *pixels = new uint8_t[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
	PageMagickInfo *info = new PageMagickInfo( common );
	Frame* infoFrame = GetFramePool()->GetFrame();
	GtkToggleButton *audioButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_magick_preview_audio" ) );
	GtkToggleButton *everyButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_magick_preview_every" ) );
	int lastFrame = frameNumber;

	pthread_mutex_lock( &init_mutex );
	info->Initialise();
	GtkToggleButton *qualityButton = GTK_TOGGLE_BUTTON( lookup_widget( window, "checkbutton_low_quality" ) );
	info->SetLowQuality( gtk_toggle_button_get_active( qualityButton ) );
	PageMagickFrames *frames = info->GetFrameSource();
	PageMagickImage *image = info->GetImageManipulator();
	pthread_mutex_unlock( &init_mutex );

	if ( Preferences::getInstance().enableAudio )
	{
		while ( previewing )
		{
			int frame_number = WaitForAction( lastFrame );
			if ( gtk_toggle_button_get_active( everyButton ) && !gtk_toggle_button_get_active( audioButton ) )
			{
				if ( frame_number >= info->begin && frame_number <= info->end )
				{
					// Render the effect
					frames->GetFrame( pixels, info->width, info->height, NULL, frame_number );
					image->PreGetFrame( previewPosition );
					image->GetFrame( pixels, frame_number );
					gdk_threads_enter();
					ShowImage( area, pixels, info->width, info->height, info->isWide );
					showFrameInfo( frame_number, info->end - info->begin + 1 );
					gdk_flush();
					gdk_threads_leave();
				}
				else
				{
					// Render video for the fore- and aft-context
					common->getPlayList()->GetFrame( frame_number, *( infoFrame ) );
					infoFrame->ExtractPreviewRGB( pixels );
					gdk_threads_enter();
					ShowImage( area, pixels, infoFrame->GetWidth(), infoFrame->GetHeight(), infoFrame->IsWide() );
					showFrameInfo( frame_number, info->end - info->begin + 1 );
					gdk_flush();
					gdk_threads_leave();
				}
			}
			lastFrame = frame_number;
		}
	}
	delete info;
	delete[] pixels;
    GetFramePool()->DoneWithFrame( infoFrame );
}

/** Render the effect.
*/

void PageMagick::StartRender()
{
	// Allow immediate render from preview
	if ( previewing )
		Stop();

	// Make sure we're not already rendering
	if ( rendering )
		return ;

	// Set the page condition
	rendering = true;
	paused = false;

	// Show the current button status
	buttonMutex = true;
	gtk_widget_set_sensitive( lookup_widget( window, "hpaned_magick" ), false );
	gtk_widget_set_sensitive( lookup_widget( window, "togglebutton_magick_start" ), false );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( lookup_widget( window, "togglebutton_magick_start" ) ), false );
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_STOP, false );
	common->toggleComponents( VIDEO_PLAY, true );
	common->commitComponentState();
	common->activateWidgets();
	gtk_widget_set_sensitive( scrubBar, FALSE );
	buttonMutex = false;

	// Create the temporary space
	uint8_t *pixels = new unsigned char[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
	int16_t *audio_buffers[ 4 ];
	for ( int n = 0; n < 4; n++ )
		audio_buffers[ n ] = new int16_t [ 2 * DV_AUDIO_MAX_SAMPLES ];
	unsigned char *dv_pixels[ 3 ];
	dv_pixels[ 0 ] = pixels;

	// Build the info
	PageMagickInfo *info = new PageMagickInfo( common );

	// Output file
	FILE *output = NULL;

	Frame* infoFrame = GetFramePool()->GetFrame();
	
	// All exceptions thrown should be safely picked up in the try/catch
	try
	{
		// Get the current date and time to set in dv
		time_t datetime = time( NULL );
		int frameNum = 0;

		// Initialise the info
		info->Initialise();

		int audio_number = 0;

		// Obtain the file name
		GtkEntry *fileEntry = GTK_ENTRY( lookup_widget( window, "entry_magick_file" ) );
		const char *filename = gtk_entry_get_text( fileEntry );
		int counter = 0;
		string thisfile;
		struct stat stats;

		if ( !strcmp( filename, "" ) )
			throw _( "No file name specified" );

		string directory = "";

		// Relative/absolute file checks
		if ( filename[ 0 ] != '/' )
			directory = common->getPlayList() ->GetProjectDirectory( ) + "/";

		// Generate the full file name
		do
		{
			char name[ 132 ];
			sprintf( name, "%s%03d.kinofx.dv", filename, ++ counter );
			thisfile = name;
			cerr << ">>> Trying " << thisfile << endl;
		}
		while ( stat( thisfile.c_str(), &stats ) == 0 );

		infoFrame->CreateEncoder( ( info->height == 576 ), info->isWide );

		// Get the source and manipulators
		PageMagickFrames *frames = info->GetFrameSource();
		PageMagickImage *image = info->GetImageManipulator();
		PageMagickAudio *sound = info->GetAudioManipulator();

		// Check that we're rendering a valid set of frames
		if ( info->begin > info->end )
			throw _( "Invalid frame range specified." );

		// Open the file
		output = fopen( thisfile.c_str(), "w" );
		if ( output == NULL )
			throw _( "Unable to open output file" );

		bool change_image = image->ChangesImage( ) || frames->IsSynth( );

		// Initialise status and progress
		startTime = pauseTime - 0.0;

		// For each frame
		int frame_number = info->begin;
		for ( double i = ( double ) info->begin; 
			frame_number <= info->end && rendering; 
			frame_number = ( int )( ( i += info->increment ) + 0.5 ) )
		{
			if ( info->reverse )
				frame_number = info->end - ( ( int ) i - info->begin );

			frames->GetFrame( pixels, audio_buffers, frame_number );
			image->PreGetFrame( );

			int samples = info->samples_this_frame; // set in frames->GetFrame() above
			int locked_samples = infoFrame->CalculateNumberSamples( info->frequency, audio_number ++ );
			if ( samples == 0 )
				samples = locked_samples;

			sound->GetFrame( audio_buffers, frame_number, samples, locked_samples );

			if ( ! change_image )
			{
				common->getPlayList()->GetFrame( frame_number, *( infoFrame ) );
			}
			else
			{
				image->GetFrame( pixels, frame_number );
				infoFrame->EncodeRGB( pixels );
			}
			AudioInfo audioInfo;
			audioInfo.channels = info->channels;
			audioInfo.frequency = info->frequency;
			audioInfo.samples = samples;
			infoFrame->EncodeAudio( audioInfo, audio_buffers );
			infoFrame->SetRecordingDate( &datetime, frameNum );
			infoFrame->SetTimeCode( frameNum++ );

			if ( fwrite( infoFrame->data, ( info->isPAL ? 144000 : 120000 ), 1, output ) != 1 )
				throw _( "Unable to write video - disk full?" );

			if ( info->end != info->begin )
				UpdateStatus( int(i), info->begin, info->end, int( info->increment + 0.5) );
		}
		struct timeval tv;
		if ( 0 == gettimeofday( &tv, NULL ) )
		{
			double now = tv.tv_sec + tv.tv_usec / 1000000.0;
			char buf[ 17 ];
			string message;

			if ( rendering )
				message = _( "Rendering finished - time: " );
			else
				message = _( "Rendering stopped - time: " );
			message += Export::formatSecs( buf, 16, now - startTime );
			common->setStatusBar( message.c_str() );
		}
		UpdateProgress( ( gfloat ) 1 );

		frames->Close();
		image->Close();
		sound->Close();

		fclose( output );
		output = NULL;

		PlayList temp;
		temp.LoadMediaObject( ( char * ) thisfile.c_str() );
		bool isPlayListEmpty = ( common->getPlayList()->GetNumFrames() == 0 );
		common->getPlayList() ->InsertPlayList( temp, info->begin );
		common->getPageEditor() ->snapshot();
		common->getPageEditor() ->ResetBar();
		if ( isPlayListEmpty )
			common->setCurrentScene( 0 );
	}
	catch ( const char * exc )
	{
		common->setStatusBar( _( "Rendering failed" ) );
		modal_message( ( char * ) exc );
	}

	rendering = false;

	buttonMutex = true;
	gtk_widget_set_sensitive( lookup_widget( window, "hpaned_magick" ), true );
	gtk_widget_set_sensitive( lookup_widget( window, "togglebutton_magick_start" ), true );
	common->toggleComponents( VIDEO_STOP, true );
	common->commitComponentState();
	common->activateWidgets();
	gtk_widget_set_sensitive( scrubBar, TRUE );
	buttonMutex = false;

	if ( output )
		fclose( output );

	delete info;
	delete[] pixels;
	for ( int n = 0; n < 4; n++ )
		delete[] audio_buffers[ n ];
	GetFramePool()->DoneWithFrame( infoFrame );
}

/** Stop whatever is running (either preview or rendering).
*/

void PageMagick::Stop()
{
	StopPreview();
	rendering = false;
}

/** Stop the preview.
*/

void PageMagick::StopPreview()
{
	if ( previewing )
	{
		previewing = false;
		gdk_threads_leave();
		pthread_join( audioThread, NULL );
		TriggerAction();
		pthread_join( videoThread, NULL );
		gdk_threads_enter();
	}
}

/** Update the progress bar for the export process
*/


void PageMagick::UpdateProgress( gfloat val )
{
	struct timeval tv;

	long long now;
	static long long lastUpdateTime = 0;

	gettimeofday( &tv, 0 );
	now = 1000000 * ( long long ) tv.tv_sec + ( long long ) tv.tv_usec;

	/* update every 0.3 second */

	if ( val < 0.0 )
		val = 0;
	if ( val > 1.0 )
		val = 1.0;

	if ( now > lastUpdateTime + 300000 || val == 1.0 )
	{
		gtk_progress_bar_update( progressBar, val );
		lastUpdateTime = now;
	}

	while ( gtk_events_pending() )
		gtk_main_iteration();
}


void PageMagick::UpdateStatus( int currentFrame, int begin, int end, int every )
{
	char buf[ 512 ];
	struct timeval tv;
	if ( 0 != gettimeofday( &tv, NULL ) )
	{
		cerr << ">>> Error calling gettimeofday?" << endl;
	}
	double now = tv.tv_sec + tv.tv_usec / 1000000.0;
	gfloat ratio = ( gfloat ) ( currentFrame - begin ) / ( gfloat ) ( end + 1 - begin ) ;
	/* Figure out how much time we have spend */
	if ( 0 == startTime )
	{
		/* First time */
		startTime = now;
		nextUpdateTime = now - 1;
		snprintf( buf, 512, _( "Rendering frame %i." ), currentFrame );
	}
	else
	{
		/* Not first time
		   We do not care to use difftime on this... */
		double time_so_far = now - startTime - pauseTime;
		double total_est = time_so_far / ratio;
		/* Write the time values into buffers */
		char buf1[ 16 ];
		char buf2[ 16 ];
		char buf3[ 16 ];
		snprintf( buf, 512, _( "Rendering frame %i. Time  used: %s,  estimated: %s,  left: %s" ),
		          currentFrame + 1,
		          Export::formatSecs( buf1, 16, time_so_far ),
		          Export::formatSecs( buf2, 16, total_est ),
		          Export::formatSecs( buf3, 16, total_est - time_so_far ) );
	}

	/* Update status message
	   TODO: Do filesystem checks first....
	*/
	if ( now > nextUpdateTime )
	{
		common->setStatusBar( buf );
		nextUpdateTime = now + 0.25;
	}

	/* Update progressbar - assuming currentFrame have yet to be exported
	   Updating the progressbar will handle any pending events. */
	UpdateProgress( ratio );

	/* Check pause - make sure we ignore time paused in export. */
	if ( paused )
	{
		struct timespec ts;
		ts.tv_sec = 0;
		ts.tv_nsec = 1000 * 1000 * 20; /* 20 ms + sched overhead */
		while ( paused )
		{
			while ( gtk_events_pending() )
				gtk_main_iteration();
			/* Lets try not to hog the CPU */
			nanosleep( &ts, NULL );
		}
		if ( 0 != gettimeofday( &tv, NULL ) )
			cerr << ">>> Error calling gettimeofday?" << endl;
		double foo = tv.tv_sec + tv.tv_usec / 1000000.0;
		pauseTime += ( foo - now );
	}
}

/** put the scene begin and end frame numbers into spinners

    \param i the numerical index position of the scene in the playlist1
*/
void PageMagick::selectScene( int i )
{
	int begin = 0;
	int end = 0;
	vector <int> scene = common->getPageEditor() ->GetScene();

	begin = i == 0 ? 0 : scene[ i - 1 ];
	end = scene[ i ] - 1;
	common->g_currentFrame = begin;

	if ( GTK_WIDGET_SENSITIVE( lookup_widget( window, "hpaned_magick" ) ) )
	{
		GtkSpinButton *startSpin = GTK_SPIN_BUTTON( lookup_widget( window, "spinbutton_magick_start" ) );
		GtkSpinButton *endSpin = GTK_SPIN_BUTTON( lookup_widget( window, "spinbutton_magick_end" ) );
		gtk_spin_button_set_range( startSpin, 0, end );
		gtk_spin_button_set_value( startSpin, begin );
		gtk_spin_button_set_range( endSpin, begin, common->getPlayList() ->GetNumFrames() - 1 );
		gtk_spin_button_set_value( endSpin, end );

		if ( previewing )
		{
			StopPreview();
			StartPreview();
		}
		else
		{
			movedToFrame( 0 );
		}
	}

	Frame &frame = *( GetFramePool( ) ->GetFrame( ) );
	FileHandler *media;
	common->getPlayList() ->GetMediaObject( begin, &media );
	common->getPlayList() ->GetFrame( begin, frame );
	common->showFrameMoreInfo( frame, media );
	GetFramePool( ) ->DoneWithFrame( &frame );

	common->setCurrentScene( begin );
	RefreshStatus( true );
}

void PageMagick::videoStartOfMovie()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_START_OF_MOVIE, false );
	common->toggleComponents( VIDEO_STOP, true );
	try
	{
		StopPreview();
		previewPosition = 0;
		PreviewFrame();
	}
	catch ( const char * exc )
	{
	}
}

void PageMagick::videoBack( int step )
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_BACK, false );
	common->toggleComponents( VIDEO_STOP, true );
	try
	{
		PageMagickInfo *info = new PageMagickInfo( common );
		info->GetFrameSource();
		StopPreview();
		previewPosition += step;
		if ( previewPosition < 0 )
			previewPosition = 0;
		delete info;
		PreviewFrame();
	}
	catch ( const char * exc )
	{
	}
}


void PageMagick::videoPlay()
{
	if ( previewing )
	{
		videoStop();
	}
	else if ( rendering )
	{
		common->toggleComponents( common->getComponentState(), false );
		common->toggleComponents( VIDEO_PLAY, paused );
		common->commitComponentState();
		paused = !paused;
	}
	else
	{
		// Start playing
		common->toggleComponents( common->getComponentState(), false );
		common->toggleComponents( VIDEO_PLAY, true );
		common->commitComponentState();
		StartPreview();
	}
}

void PageMagick::videoForward( int step )
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_FORWARD, false );
	common->toggleComponents( VIDEO_STOP, true );
	try
	{
		PageMagickInfo *info = new PageMagickInfo( common );
		info->GetFrameSource();
		StopPreview();
		previewPosition += step;
		if ( previewPosition > info->end - info->begin )
			previewPosition = info->end - info->begin;
		delete info;
		PreviewFrame();
	}
	catch ( const char * exc )
	{
	}
}

void PageMagick::videoEndOfMovie()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_END_OF_MOVIE, false );
	common->toggleComponents( VIDEO_STOP, true );
	try
	{
		PageMagickInfo *info = new PageMagickInfo( common );
		info->GetFrameSource();
		StopPreview();
		previewPosition = info->end - info->begin;
		delete info;
		PreviewFrame();
	}
	catch ( const char * exc )
	{
	}
}

void PageMagick::videoPause()
{
	videoStop();
}

void PageMagick::videoStop()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_STOP, true );
	Stop();
}

/** Refresh the status label to show the current selections.
*/

void PageMagick::RefreshStatus( bool with_fx_notify )
{
	GtkNotebook *notebookFrame = GTK_NOTEBOOK( lookup_widget( this->window, "notebook_magick_frames" ) );
	GtkNotebook *notebookImage = GTK_NOTEBOOK( lookup_widget( this->window, "notebook_magick_video" ) );
	GtkNotebook *notebookAudio = GTK_NOTEBOOK( lookup_widget( this->window, "notebook_magick_audio" ) );

	int page = gtk_notebook_get_current_page( notebookFrame );
	if ( page == 1 )
	{
		SelectionNotification *notify = dynamic_cast <SelectionNotification *>( GetImageCreate( ) );
		if ( with_fx_notify && notify != NULL )
			notify->OnSelectionChange( );
	}

	page = gtk_notebook_get_current_page( notebookImage );

	if ( page == 0 && GetImageFilter() != NULL )
	{
		SelectionNotification *notify = dynamic_cast <SelectionNotification *>( GetImageFilter( ) );
		if ( with_fx_notify && notify != NULL )
			notify->OnSelectionChange( );
	}
	else if ( page == 1 && GetImageTransition() != NULL )
	{
		SelectionNotification *notify = dynamic_cast <SelectionNotification *>( GetImageTransition( ) );
		if ( with_fx_notify && notify != NULL )
			notify->OnSelectionChange( );
	}

	page = gtk_notebook_get_current_page( notebookAudio );
	if ( page == 0 )
	{
		SelectionNotification *notify = dynamic_cast <SelectionNotification *>( GetAudioFilter( ) );
		if ( with_fx_notify && notify != NULL )
			notify->OnSelectionChange( );
	}
	else if ( page == 1 )
	{
		SelectionNotification *notify = dynamic_cast <SelectionNotification *>( GetAudioTransition( ) );
		if ( with_fx_notify && notify != NULL )
			notify->OnSelectionChange( );
	}

	ShowCurrentStatus( GetCurrentPosition(), LOCKED_KEY, false, false );

	// This is a special case to cause preview to restart when fx options change
	if ( !with_fx_notify )
	{
		if ( previewing )
		{
			StopPreview();
			StartPreview();
		}
		else
		{
			PreviewFrame();
		}
	}
}

void PageMagick::timeFormatChanged()
{
	on_spinbutton_magick_start_value_changed( GTK_SPIN_BUTTON( lookup_widget( this->window, "spinbutton_magick_start" ) ), NULL );
	on_spinbutton_magick_end_value_changed( GTK_SPIN_BUTTON( lookup_widget( this->window, "spinbutton_magick_end" ) ), NULL );
	on_spinbutton_magick_limit_value_changed( GTK_SPIN_BUTTON( lookup_widget( this->window, "spinbutton_magick_limit" ) ), NULL );
}


/** Return the currently selected image filter.
*/

GDKImageFilter *PageMagick::GetImageFilter( ) const
{
	return image_filters.Get( );
}

/** Return the currently selected image transition.
*/

GDKImageTransition *PageMagick::GetImageTransition( ) const
{
	return image_transitions.Get( );
}

/** Return the currently selected audio filter.
*/

GDKAudioFilter *PageMagick::GetAudioFilter( ) const
{
	return audio_filters.Get( );
}

/** Return the currently selected audio transition.
*/

GDKAudioTransition *PageMagick::GetAudioTransition( ) const
{
	return audio_transitions.Get( );
}

/** Return the currently selected image creator.
*/

GDKImageCreate *PageMagick::GetImageCreate( ) const
{
	return image_creators.Get( );
}

gboolean PageMagick::processKeyboard( GdkEventKey *event )
{
	gboolean ret = FALSE;

	// Translate special keys to equivalent command
	switch ( event->keyval )
	{
	case GDK_Escape:
		if ( rendering || previewing )
		{
			common->keyboardFeedback( "", _( "Stop" ) );
			common->videoStop();
		}
		else
		{
			common->changePageRequest( PAGE_EDITOR );
		}
	default:
		break;
	}

	return ret;
}

gboolean PageMagick::processCommand( const char *cmd )
{
	/* play, pause */

	if ( strcmp( cmd, " " ) == 0 )
	{
		if ( !previewing )
		{
			common->keyboardFeedback( cmd, _( "Play" ) );
			common->videoPlay( );
		}
		else
		{
			common->keyboardFeedback( cmd, _( "Pause" ) );
			common->videoPause( );
		}
	}

	/* advance one frame */

	else if ( strcmp( cmd, "l" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward" ) );
		common->moveByFrames( 1 );
	}

	/* backspace one frame */

	else if ( strcmp( cmd, "h" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move backward" ) );
		common->moveByFrames( -1 );
	}

	/* advance one second */

	else if ( strcmp( cmd, "w" ) == 0 || strcmp( cmd, "W" ) == 0 ||
	          strcmp( cmd, "e" ) == 0 || strcmp( cmd, "E" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward second" ) );
		common->moveByFrames( _getOneSecond() );
	}

	/* backspace one second */

	else if ( ( strcmp( cmd, "b" ) == 0 ) || ( strcmp( cmd, "B" ) == 0 ) )
	{
		common->keyboardFeedback( cmd, _( "Move backwards one second" ) );
		common->moveByFrames( -1 * _getOneSecond() );
	}

	/* start of scene */

	else if ( ( strcmp( cmd, "0" ) == 0 ) || ( strcmp( cmd, "^" ) == 0 ) )
	{
		common->videoStartOfScene( );
		common->keyboardFeedback( cmd, _( "Move to start of scene" ) );
	}

	/* end of scene */

	else if ( strcmp( cmd, "$" ) == 0 )
	{
		common->videoEndOfScene( );
		common->keyboardFeedback( cmd, _( "Move to end of scene" ) );
	}

	/* start of next scene */

	else if ( ( strcmp( cmd, "j" ) == 0 ) || strcmp( cmd, "+" ) == 0 )
	{
		common->videoNextScene( );
		common->keyboardFeedback( cmd, _( "Move to start of next scene" ) );
	}

	/* start of previous scene */

	else if ( ( strcmp( cmd, "k" ) == 0 ) || ( strcmp( cmd, "-" ) == 0 ) )
	{
		common->videoPreviousScene( );
		common->keyboardFeedback( cmd, _( "Move to start of previous scene" ) );
	}

	/* first frame */

	else if ( strcmp( cmd, "gg" ) == 0 )
	{
		common->videoStartOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to first frame" ) );
	}

	/* last frame */

	else if ( strcmp( cmd, "G" ) == 0 )
	{
		common->videoEndOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to last frame" ) );
	}

	/* write PlayList */

	else if ( strcmp( cmd, ":w" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Write playlist" ) );
		common->savePlayList( );
	}

	else if ( strcmp( cmd, "Enter" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Start Render" ) );
		GdkEvent ev;
		ev.key.type = GDK_KEY_PRESS;
		ev.key.window = common->getWidget()->window;
		ev.key.send_event = TRUE;
		ev.key.state = 0;
		ev.key.length = 0;
		ev.key.string = (gchar*) "";
		ev.key.keyval = GDK_Return;
		ev.key.group = 0;
		gdk_event_put( &ev );
	}

	else if ( strcmp( cmd, "Esc" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Stop" ) );
		common->videoStop();
	}

	else if ( strcmp( cmd, "F2" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Edit" ) );
		common->changePageRequest( PAGE_EDITOR );
	}

	else if ( strcmp( cmd, "A" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, append to movie" ) );
		common->changePageRequest( PAGE_EDITOR );
	}

	else if ( strcmp( cmd, "v" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Timeline" ) );
		common->changePageRequest( PAGE_TIMELINE );
	}

	else if ( strcmp( cmd, "t" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Trim" ) );
		common->changePageRequest( PAGE_TRIM );
	}

	else if ( strcmp( cmd, ":W" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Export" ) );
		common->changePageRequest( PAGE_TIMELINE );
	}

	/* quit */

	else if ( strcmp( cmd, ":q" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "quit" ) );
		kinoDeactivate();
	}

	return FALSE;
}

void PageMagick::OnTimeRangeChanged()
{
	try
	{
		PageMagickInfo *info = new PageMagickInfo( common );

		info->GetFrameSource();
		scrubAdjustment->upper = info->end - info->begin + 1;
		if ( gtk_adjustment_get_value( scrubAdjustment ) >= scrubAdjustment->upper )
			previewPosition = info->end - info->begin;
		delete info;
		PreviewFrame();
		g_signal_emit_by_name( scrubAdjustment, "changed" );
	}
	catch ( const char * exc )
	{
	}
}

static void on_drawingarea_refresh_required( GtkWidget *some_widget, GdkEventConfigure *event, gpointer user_data )
{
	try
	{
		( ( PageMagick * ) user_data )->PreviewFrame();
	}
	catch ( const char * exc )
	{
	}
}


class FXSelectedFrames : public SelectedFrames
{
protected:
	bool seekable;
	int start;
	int end;
	double increment;
	bool frames_reverse;
	bool effect_reverse;
	double real_start;
	double real_end;
	int b_frame_type;
	GdkColor color;
	PageMagick* magick;

public:
	void Initialise( PageMagick *page )
	{
		GtkNotebook * notebook = GTK_NOTEBOOK( lookup_widget( page->window, "notebook_magick_frames" ) );
		int source = gtk_notebook_get_current_page( notebook );
		magick = page;

		if ( source == 0 )
		{
			// Obtain begining and ending of sequence
			start = 0;
			end = GetCurrentPlayList().GetNumFrames();
			increment = 1;
			frames_reverse = false;
			effect_reverse = false;
			real_start = 0;
			real_end = 1;

			GtkEntry *startSpin = GTK_ENTRY( lookup_widget( page->window, "spinbutton_magick_start" ) );
			GtkEntry *endSpin = GTK_ENTRY( lookup_widget( page->window, "spinbutton_magick_end" ) );

			if ( GetCurrentPlayList().GetNumFrames() != 0 )
			{
				seekable = true;
				start = atoi( gtk_entry_get_text( startSpin ) );
				end = atoi( gtk_entry_get_text( endSpin ) );
				GtkToggleButton *limit = GTK_TOGGLE_BUTTON( lookup_widget( page->window, "checkbutton_magick_frame_limit" ) );
				if ( gtk_toggle_button_get_active( limit ) )
				{
					GtkEntry * spin = GTK_ENTRY( lookup_widget( page->window, "spinbutton_magick_limit" ) );
					GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( page->window, "optionmenu_magick_frame_offset" ) ) ) );
					GtkWidget *active_item = gtk_menu_get_active( menu );
					if ( g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item ) == 1 )
					{
						int requested_end = start + atoi( gtk_entry_get_text( spin ) ) - 1;
						if ( requested_end < end )
							end = requested_end;
					}
					else
					{
						start = end - atoi( gtk_entry_get_text( spin ) ) + 1;
					}
				}
			}
			else
			{
				seekable = false;
			}

			// Determine speed
			GtkToggleButton *speed = GTK_TOGGLE_BUTTON( lookup_widget( page->window, "checkbutton_speed" ) );
			if ( gtk_toggle_button_get_active( speed ) )
			{
				GtkRange * range = GTK_RANGE( lookup_widget( page->window, "hscale_speed" ) );
				increment = range->adjustment->value;
			}

			// Determine direction
			GtkToggleButton *reverse_button = GTK_TOGGLE_BUTTON( lookup_widget( page->window, "checkbutton_reverse" ) );
			frames_reverse = gtk_toggle_button_get_active( reverse_button );
		}
		else
		{
			seekable = false;
		}

		// Check if we're reversed (currently only image transitions have a global effect reverse)
		GtkNotebook *notebookImage = GTK_NOTEBOOK( lookup_widget( page->window, "notebook_magick_video" ) );

		int current = gtk_notebook_get_current_page( notebookImage );

		if ( current == 1 )
		{
			GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( page->window, "optionmenu_direction" ) ) ) );
			GtkWidget *active_item = gtk_menu_get_active( menu );
			effect_reverse = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item ) == 1;

			GtkRange *range = GTK_RANGE( lookup_widget( page->window, "hscale_transition_start" ) );
			real_start = range->adjustment->value;
			range = GTK_RANGE( lookup_widget( page->window, "hscale_transition_end" ) );
			real_end = range->adjustment->value;

			GtkColorButton *colorButton = GTK_COLOR_BUTTON( lookup_widget( page->window, "colorpicker_magick_transition" ) );
			gtk_color_button_get_color( colorButton, &color );

			GtkToggleButton *toggle = GTK_TOGGLE_BUTTON( lookup_widget( page->window, "radiobutton_magick_transition_colour" ) );

			if ( !gtk_toggle_button_get_active( toggle ) )
			{
				GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( page->window, "optionmenu_magick_transition_frame" ) ) ) );
				GtkWidget *active_item = gtk_menu_get_active( menu );
				b_frame_type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
			}
			else
			{
				b_frame_type = 3;
			}
		}
		else
		{
			b_frame_type = -1;
			effect_reverse = false;
			real_start = 0;
			real_end = 1;
		}
	}

	int GetNumInputFrames( )
	{
		if ( seekable )
			return end - start + 1;
		else
			return 0;
	}

	bool IsEffectReversed( )
	{
		return effect_reverse;
	}

	double GetRealStart( )
	{
		return real_start;
	}

	double GetRealEnd( )
	{
		return real_end;
	}

	int GetNumOutputFrames( )
	{
		return ( int ) ( GetNumInputFrames( ) / increment );
	}

	double GetPosition( int index )
	{
		return index / GetNumOutputFrames( );
	}

	double GetFrameDelta( )
	{
		return 1 / GetNumOutputFrames( );
	}

	int GetIndex( double position )
	{
		if ( frames_reverse )
			position = 1 - position;
		return ( int ) ( GetNumOutputFrames( ) * position + 0.5 );
	}

	void GetScaledImage( int index, uint8_t *image, int width = 0, int height = 0 )
	{
		// Get the current playlist
		PlayList & list = GetCurrentPlayList();

		if ( index >= 0 && index < list.GetNumFrames( ) )
		{
			// Get a frame to work with
			Frame & frame = *( GetFramePool() ->GetFrame() );

			frame.decoder->quality = DV_QUALITY_BEST;

			// Obtain the frame as specified by the index
			list.GetFrame( index, frame );

			// If the width and height is specified
			if ( width != 0 && height != 0 )
			{
				// Create a temporary space to hold the full RGB image
				uint8_t * temp = new uint8_t[ frame.GetWidth( ) * frame.GetHeight( ) * 3 ];

				// Extract it
				frame.ExtractRGB( temp );

				// Convert to a GDK Image
				GdkPixbuf *im1 = gdk_pixbuf_new_from_data( temp, GDK_COLORSPACE_RGB, FALSE, 8,
				                 frame.GetWidth( ), frame.GetHeight( ), frame.GetWidth( ) * 3, NULL, NULL );

				// Clone and scale
				GdkPixbuf *im2 = gdk_pixbuf_scale_simple( im1, width, height, GDK_INTERP_HYPER );

				// Copy it into the image
				memcpy( image, gdk_pixbuf_get_pixels( im2 ), width * height * 3 );

				// Destroy the GDK images
				g_object_unref( im2 );
				g_object_unref( im1 );

				// Delete the temporary space
				delete[] temp;
			}
			else
			{
				// Extract it
				frame.ExtractRGB( image );
			}

			GetFramePool() ->DoneWithFrame( &frame );
		}
		else
		{
			uint8_t *end = image + width * height * 3;
			for ( uint8_t * p = image; p < end; )
			{
				*p ++ = color.red >> 8;
				*p ++ = color.green >> 8;
				*p ++ = color.blue >> 8;
			}
		}
	}

	void GetImageA( double position, uint8_t *image, int width = 0, int height = 0 )
	{
		GetScaledImage( start + GetIndex( position ), image, width, height );
	}

	void GetImageB( double position, uint8_t *image, int width = 0, int height = 0 )
	{
		switch ( b_frame_type )
		{
		case 1:
			GetScaledImage( ( int ) ( end + 1 ), image, width, height );
			break;
		case 2:
			GetScaledImage( ( int ) ( start - 1 ), image, width, height );
			break;
		case 0:
			GetScaledImage( ( int ) ( end + 1 + position * GetNumInputFrames( ) ), image, width, height );
			break;
		case - 1:
		case 3:
			{
				uint8_t *end = image + width * height * 3;
				for ( uint8_t * p = image; p < end; )
				{
					*p ++ = color.red >> 8;
					*p ++ = color.green >> 8;
					*p ++ = color.blue >> 8;
				}
			}
			break;
		}
	}

	void GetAudio( int index, int16_t **audio, short int &channels, int &frequency, int &samples )
	{
		// Get the current playlist
		PlayList & list = GetCurrentPlayList();

		if ( index >= 0 && index < list.GetNumFrames( ) )
		{
			// Get a frame to work with
			Frame & frame = *( GetFramePool() ->GetFrame() );

			// Obtain the frame as specified by the index
			list.GetFrame( index, frame );

			// Extract the audio
			if ( frame.ExtractAudio( audio ) )
			{
				// Update the info
				AudioInfo info;
				frame.GetAudioInfo( info );
				channels = info.channels;
				frequency = info.frequency;
				samples = info.samples;
			}
			else
			{
				channels = 2;
				// XXX: frequency not known! But are any plugins really using this?
				frequency = 48000;
				samples = frequency / int( frame.GetFrameRate() );
				for ( int i = 0; i < channels; i++ )
					memset( audio[i], 0, samples * sizeof(int16_t) );
			}
			// Return the frame to the pool
			GetFramePool() ->DoneWithFrame( &frame );
		}
		else if ( list.GetNumFrames( ) )
		{
			// Get a frame to work with
			Frame & frame = *( GetFramePool() ->GetFrame() );

			// Obtain the frame as specified by the index
			list.GetFrame( index, frame );

			// Update the info
			AudioInfo info;
			frame.GetAudioInfo( info );
			channels = info.channels;
			frequency = info.frequency;
			samples = info.samples;

			// Wipe the audio
			for ( int i = 0; i < channels; i ++ )
				memset( audio[ i ], 0, samples * sizeof( int16_t ) );

			// Return the frame to the pool
			GetFramePool() ->DoneWithFrame( &frame );
		}
	}

	void GetAudioA( double position, int16_t **audio, short int &channels, int &frequency, int &samples )
	{
		GetAudio( start + GetIndex( position ), audio, channels, frequency, samples );
	}

	void GetAudioB( double position, int16_t **audio, short int &channels, int &frequency, int &samples )
	{
		switch ( b_frame_type )
		{
		case 0:
			GetAudio( ( int ) ( end + 1 + position * GetNumInputFrames( ) ), audio, channels, frequency, samples );
			break;
		case - 1:
		case 1:
		case 2:
		case 3:
			GetAudio( -1, audio, channels, frequency, samples );
			break;
		}
	}

	void Repaint( )
	{
		while ( gtk_events_pending() )
			gtk_main_iteration();
		if ( magick )
			magick->PreviewFrame();
	}

	bool IsRepainting( )
	{
		return magick->IsRepainting();
	}

	bool IsPreviewing( )
	{
		return magick->IsPreviewing();
	}

	int GetFrameNumber( double position )
	{
		return start + GetIndex( position );
	}
};

// Get the selected frames for FX
SelectedFrames &GetSelectedFramesForFX( )
{
	static FXSelectedFrames selected;
	selected.Initialise( common->getPageMagick( ) );
	return selected;
}

GtkWindow* GetKinoWidgetWindow( )
{
	return KinoCommon::getWidgetWindow( common->getWidget() );
}

void Repaint( )
{
	GetSelectedFramesForFX().Repaint();
}


void PageMagick::ShowCurrentStatus( double position, frame_type type, bool hasPrev, bool hasNext )
{
	newPosition = position;
	isGuiLocked = true;
	// Determine Key button status
	GtkWidget *widget = lookup_widget( window, "togglebutton_key_frame" );
	if ( rendering )
	{
		gtk_widget_set_sensitive( widget, false );
	}
	else
	{
		gtk_widget_set_sensitive( widget, keyFrameControllerClient != 0 );
		if ( type & LOCKED_KEY )	 // Locked Key
		{
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), true );
			GtkImage* img = GTK_IMAGE( lookup_widget( window, "image_key_frame" ) );
			gtk_image_set_from_stock( img, GTK_STOCK_REMOVE, GTK_ICON_SIZE_BUTTON );
		}
		else if ( type & KEY ) 		// Normal Key
		{
			
			GtkImage* img = GTK_IMAGE( lookup_widget( window, "image_key_frame" ) );
			gtk_image_set_from_stock( img, GTK_STOCK_REMOVE, GTK_ICON_SIZE_BUTTON );
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), true );
		}
		else 						// Not Key
		{
			GtkImage* img = GTK_IMAGE( lookup_widget( window, "image_key_frame" ) );
			gtk_image_set_from_stock( img, GTK_STOCK_ADD, GTK_ICON_SIZE_BUTTON );
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), false );
		}
		isPreviousScene = hasPrev;
		isNextScene = hasNext;
		common->activateWidgets();
	}
	isGuiLocked = false;
}

double PageMagick::GetCurrentPosition( )
{
	try
	{
		PageMagickInfo info( common );
		info.GetFrameSource();
		return time_info( info, info.begin + previewPosition ).position();
	}
	catch( const char * exc )
	{
		return 0;
	}
}

void PageMagick::videoPreviousScene()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_START_OF_SCENE, false );
	common->toggleComponents( VIDEO_STOP, true );
	StopPreview();
	if ( !isGuiLocked && keyFrameControllerClient )
	{
		double position = GetCurrentPosition( );
		keyFrameControllerClient->OnControllerPrevKey( position );
		previewPosition = GetSelectedFramesForFX().GetIndex( newPosition );
		PreviewFrame();
	}
}

void PageMagick::videoNextScene()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_NEXT_SCENE, false );
	common->toggleComponents( VIDEO_STOP, true );
	StopPreview();
	if ( !isGuiLocked && keyFrameControllerClient )
	{
		double position = GetCurrentPosition( );
		keyFrameControllerClient->OnControllerNextKey( position );
		previewPosition = GetSelectedFramesForFX().GetIndex( newPosition );
		PreviewFrame();
	}
}

void PageMagick::OnKeyFrameControllerKeyChanged( GtkToggleButton* togglebutton )
{
	if ( !isGuiLocked && keyFrameControllerClient )
	{
		double position = GetCurrentPosition( );
		keyFrameControllerClient->OnControllerKeyChanged( position,
			gtk_toggle_button_get_active( togglebutton ) );
	}
}


/** Factory method for the key frame controller.
*/

KeyFrameController *GetKeyFrameController( KeyFrameControllerClient *client )
{
	common->getPageMagick()->SetKeyFrameControllerClient( client );
	return static_cast< KeyFrameController* >( common->getPageMagick() );
}


/*
* image_transitions.cc -- RGB24 image transitions
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

// Project Includes

#include "image_transitions.h"
#include "page_magick.h"
#include "kino_extra.h"

// C Includes

#include <string.h>
#include <stdio.h>
#include <glade/glade.h>

extern "C"
{
#include "support.h"
	extern GladeXML* magick_glade;
}

class ImageTransitionNone : public GDKImageTransition
{
	char *GetDescription( ) const
	{
		return _( "No Change" );
	}
	
	void GetFrame( uint8_t *pixels, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
	}
};

class ImageTransitionSwitch : public GDKImageTransition
{
private:
	GtkWidget *window;
	double point;

public:
	ImageTransitionSwitch()
	{
		window = glade_xml_get_widget( magick_glade, "image_transition_switch" );
		GtkWidget* widget = lookup_widget( window, "hscale_switch" );
		g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageTransitionSwitch()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Switch" );
	}

	void GetFrame( uint8_t *pixels, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
		GtkRange * range = GTK_RANGE( lookup_widget( window, "hscale_switch" ) );
		GtkAdjustment *adjust = gtk_range_get_adjustment( range );
		point = adjust->value / 100;
		
		if ( position >= point )
			memcpy( pixels, mesh, width * height * 3 );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}

	double SwitchPoint( )
	{
		return point;
	}
};

/** Fade transitions.
*/

class ImageTransitionFade : public GDKImageTransition
{
public:
	char *GetDescription( ) const
	{
		return _( "Dissolve" );
	}

	void GetFrame( uint8_t *io, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
		uint8_t * k = mesh;
		uint8_t *p = io;
		uint8_t r, g, b;
		uint8_t kr, kg, kb;

		if ( reverse )
			position = 1 - position;

		while ( p < ( io + width * height * 3 ) )
		{
			r = *p;
			g = *( p + 1 );
			b = *( p + 2 );
			kr = *k ++;
			kg = *k ++;
			kb = *k ++;
			int vr = ( int ) ( kr * position + r * ( 1 - position ) );
			int vg = ( int ) ( kg * position + g * ( 1 - position ) );
			int vb = ( int ) ( kb * position + b * ( 1 - position ) );
			*p ++ = vr < 0xff ? ( uint8_t ) vr : 0xff;
			*p ++ = vg < 0xff ? ( uint8_t ) vg : 0xff;
			*p ++ = vb < 0xff ? ( uint8_t ) vb : 0xff;
		}
	}
};

/** Bar Wipe transitions
*/

class ImageTransitionBarWipe : public GDKImageTransition
{
private:
	GtkWidget *window;
	int type;

public:
	ImageTransitionBarWipe()
	{
		window = glade_xml_get_widget( magick_glade, "image_transition_bar_wipe" );
		GtkWidget* widget = lookup_widget( window, "optionmenu_wipe" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageTransitionBarWipe()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Push Wipe" );
	}

	void Left( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_width = ( int ) ( width * position );
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *k = mesh + y * width * 3;
			uint8_t *p = io + y * width * 3;
			memmove( p, p + roll_width * 3, ( width * 3 ) - roll_width * 3 );
			p = io + ( ( y + 1 ) * width * 3 ) - roll_width * 3;
			memcpy( p, k, roll_width * 3 );
		}
	}

	void Right( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		uint8_t temp[ 720 * 3 ];
		int roll_width = ( int ) ( width * position );
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *k = mesh + y * width * 3;
			uint8_t *p = io + y * width * 3;
			memcpy( temp, k + ( width - roll_width ) * 3, roll_width * 3 );
			memcpy( temp + roll_width * 3, p, ( width * 3 ) - roll_width * 3 );
			memcpy( p, temp, width * 3 );
		}
	}

	void Down( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_height = ( int ) ( height * position );
		memmove( io + roll_height * width * 3, io, ( height - roll_height ) * width * 3 );
		for ( int y = 0; y < roll_height; y ++ )
		{
			uint8_t *p = io + y * width * 3;
			uint8_t *k = mesh + ( y + height - roll_height ) * width * 3;
			memcpy( p, k, width * 3 );
		}
	}

	void Up( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_height = ( int ) ( height * ( 1 - position ) );
		memmove( io, io + ( height - roll_height ) * width * 3, roll_height * width * 3 );
		for ( int y = roll_height; y < height; y ++ )
		{
			uint8_t *p = io + y * width * 3;
			uint8_t *k = mesh + ( y - roll_height ) * width * 3;
			memcpy( p, k, width * 3 );
		}
	}

	void GetFrame( uint8_t *pixels, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
		GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_wipe" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
		
		if ( type == 0 && !reverse )
			Left( pixels, mesh, width, height, position );
		else if ( type == 0 && reverse )
			Right( pixels, mesh, width, height, position );
		else if ( type == 1 && !reverse )
			Down( pixels, mesh, width, height, position );
		else if ( type == 1 && reverse )
			Up( pixels, mesh, width, height, position );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};

/** 'Differences Only' transition.
*/

#define DIFFERS_BY( w, v, o )	( abs( v - w ) < o )

class ImageTransitionDifferences : public GDKImageTransition
{
private:
	GtkWidget *window;
	int type;
	int sensitivity;
	GdkColor color;

public:
	ImageTransitionDifferences()
	{
		window = glade_xml_get_widget( magick_glade, "image_transition_differences" );
		GtkWidget* widget = lookup_widget( window, "optionmenu_action" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
		widget = lookup_widget( window, "colorpicker" );
		g_signal_connect( G_OBJECT( widget ), "color-set", G_CALLBACK( Repaint ), 0 );
		widget = lookup_widget( window, "hscale_sensitivity" );
		g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageTransitionDifferences()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Differences" );
	}

	void GetFrame( uint8_t *io, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
		GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_action" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );

		GtkColorButton *colorButton = GTK_COLOR_BUTTON( lookup_widget( window, "colorpicker" ) );
		gtk_color_button_get_color( colorButton, &color );

		GtkRange *range = GTK_RANGE( lookup_widget( window, "hscale_sensitivity" ) );
		GtkAdjustment *adjust = gtk_range_get_adjustment( range );
		sensitivity = ( int ) adjust->value;
		
		// Differences only
		uint8_t * k = mesh;
		uint8_t *p = io;
		uint8_t r, g, b;
		uint8_t kr, kg, kb;
		for ( int y = 0; y < height; y ++ )
		{
			for ( int x = 0; x < width; x ++ )
			{
				r = *p;
				g = *( p + 1 );
				b = *( p + 2 );
				kr = *k ++;
				kg = *k ++;
				kb = *k ++;

				bool differs = DIFFERS_BY( kr, r, sensitivity ) && DIFFERS_BY( kg, g, sensitivity ) && DIFFERS_BY( kb, b, sensitivity );

				if ( ( !differs && type == 0 ) || ( differs && type == 1 ) )
				{
					*p ++ = color.red >> 8;
					*p ++ = color.green >> 8;
					*p ++ = color.blue >> 8;
				}
				else
				{
					*p ++ = r;
					*p ++ = g;
					*p ++ = b;
				}
			}
		}
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};

/** Barn Door transition.
*/

class ImageTransitionBarnDoorWipe : public GDKImageTransition
{
private:
	GtkWidget *window;
	int type;

public:
	ImageTransitionBarnDoorWipe()
	{
		window = glade_xml_get_widget( magick_glade, "image_transition_barndoor" );
		GtkWidget* widget = lookup_widget( window, "optionmenu_door" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageTransitionBarnDoorWipe()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Barn Door Wipe" );
	}

	void VerticalOpen( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_width = ( int ) ( width * position ) / 2;
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *k = mesh + y * width * 3 + ( width * 3 / 2 ) - roll_width * 3;
			uint8_t *p = io + y * width * 3 + ( width * 3 / 2 ) - roll_width * 3;
			memcpy( p, k, roll_width * 3 * 2 );
		}
	}

	void VerticalClose( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_width = ( width - ( int ) ( width * position ) ) / 2;
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *k = mesh + y * width * 3 + ( width * 3 / 2 ) - roll_width * 3;
			uint8_t *p = io + y * width * 3 + ( width * 3 / 2 ) - roll_width * 3;
			memcpy( p, k, roll_width * 3 * 2 );
		}
	}

	void HorizontalOpen( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_height = ( int ) ( height * position ) / 2;
		for ( int y = height / 2 - roll_height; y < height / 2 + roll_height; y ++ )
		{
			uint8_t *k = mesh + y * width * 3;
			uint8_t *p = io + y * width * 3;
			memcpy( p, k, width * 3 );
		}
	}

	void HorizontalClose( uint8_t *io, uint8_t *mesh, int width, int height, double position )
	{
		int roll_height = ( int ) ( height * position ) / 2;
		for ( int y = 0; y < roll_height; y ++ )
		{
			uint8_t *k = mesh + y * width * 3;
			uint8_t *p = io + y * width * 3;
			memcpy( p, k, width * 3 );
			k = mesh + ( height - y - 1 ) * width * 3;
			p = io + ( height - y - 1 ) * width * 3;
			memcpy( p, k, width * 3 );
		}
	}

	void GetFrame( uint8_t *pixels, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
		GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_door" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
		
		if ( type == 0 && !reverse )
			VerticalOpen( pixels, mesh, width, height, position );
		else if ( type == 0 && reverse )
			VerticalClose( pixels, mesh, width, height, position );
		else if ( type == 1 && !reverse )
			HorizontalOpen( pixels, mesh, width, height, position );
		else if ( type == 1 && reverse )
			HorizontalClose( pixels, mesh, width, height, position );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};


/** Callback for selection change.
*/

static void
on_optionmenu_selected ( GtkMenuItem *menu_item, gpointer user_data )
{
	( ( GDKImageTransitionRepository * ) user_data ) ->SelectionChange();
}

GDKImageTransitionRepository::GDKImageTransitionRepository()
	: selected_transition( 0 )
{
	// Register an instance of each object (adapting raw transitions to GDK transitions where necessary)
	Register( new ImageTransitionFade() );
	Register( new ImageTransitionBarnDoorWipe() );
	Register( new ImageTransitionDifferences() );
	Register( new ImageTransitionNone() );
	Register( new ImageTransitionBarWipe() );
	Register( new ImageTransitionSwitch() );
}

GDKImageTransitionRepository::~GDKImageTransitionRepository()
{
	// Remove the transitions in the repository
	for ( unsigned int index = 0; index < transitions.size(); index ++ )
		delete transitions[ index ];
}

/** Register an image transition.
*/

void GDKImageTransitionRepository::Register( GDKImageTransition *transition )
{
	std::cerr << ">>> Image Transition: " << transition->GetDescription() << std::endl;
	transitions.push_back( transition );
}

void GDKImageTransitionRepository::Initialise( GtkOptionMenu *menu, GtkBin *container )
{
	// Store these for future reference
	this->menu = menu;
	this->container = container;

	// Add the transitions to the menu
	GtkMenu *menu_new = GTK_MENU( gtk_menu_new( ) );
	for ( unsigned int index = 0; index < transitions.size(); index ++ )
	{
		GtkWidget *item = gtk_menu_item_new_with_label( transitions[ index ] ->GetDescription( ) );
		gtk_widget_show( item );
		gtk_menu_append( menu_new, item );
		g_signal_connect( G_OBJECT( item ), "activate", G_CALLBACK( on_optionmenu_selected ), this );
	}
	gtk_menu_set_active( menu_new, 0 );
	gtk_option_menu_set_menu( menu, GTK_WIDGET( menu_new ) );

	// Register the selected items widgets
	SelectionChange();
}

GDKImageTransition *GDKImageTransitionRepository::Get( ) const
{
	GtkMenu * innerMenu = GTK_MENU( gtk_option_menu_get_menu( menu ) );
	GtkWidget *active_item = gtk_menu_get_active( innerMenu );
	return transitions[ g_list_index( GTK_MENU_SHELL( innerMenu ) ->children, active_item ) ];
}

/** Handle attach/detach widgets on last selected/selected items.
*/

void GDKImageTransitionRepository::SelectionChange( )
{
	bool isPreviewing = false;
	PageMagick* magick = 0;

	if ( common && common->getPageMagick( ) )
		magick = common->getPageMagick( );
	if ( magick && magick->IsPreviewing() )
	{
		isPreviewing = true;
		magick->StopPreview();
	}

	// Detach the selected Transitions widgets
	if ( selected_transition != NULL )
		selected_transition->DetachWidgets( container );

	if ( magick )
	{
		magick->SetKeyFrameControllerClient(0);
		magick->ShowCurrentStatus( magick->GetCurrentPosition(), LOCKED_KEY, false, false );
	}

	// Get the new selection
	selected_transition = Get();

	// Attach the new transition widgets
	if ( selected_transition != NULL )
		selected_transition->AttachWidgets( container );

	if ( magick )
	{
		if ( isPreviewing )
			magick->StartPreview();
		else
			magick->PreviewFrame();
	}
}

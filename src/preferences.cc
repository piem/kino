/*
* Copyright (C) 2001-2009 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "preferences.h"
#include "kino_common.h"
#include "stringutils.h"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <string>
using std::string;
using std::make_pair;
#include <sstream>
#include <iostream>

#define GKEYFILE_GROUP "general"

static string kino_config_file()
{
	static string filename;

	if(!filename.size())
	{
		char *kinoHome=getenv("KINO_HOME");

		if(kinoHome)
			filename=string(kinoHome)+string("/kinorc");
		else
			filename = string( getenv( "HOME" ) ) + string( "/.kinorc" );
	}

	return filename;
}

/** The global prefs object.
    
    It is accessible to other classes via the getInstance method.
*/

Preferences *Preferences::instance = NULL;

/** Singleton getInstance method. Standard pattern followed, non threadsafe
	but not required (at the moment) since it gets called before any threading
	is activated. Keep an eye on it though.
 
	\returns the single instance of Preferences.
*/

Preferences &Preferences::getInstance()
{
	if ( instance == NULL )
	{
		instance = new Preferences;
	}
	return *instance;
}

void Preferences::get( int& var, const char* name, const char* def )
{
	GError *err = NULL;
	var = g_key_file_get_integer( config, GKEYFILE_GROUP, name, &err );
	if ( err != NULL )
	{
		var = atoi( def );
		g_key_file_set_integer( config, GKEYFILE_GROUP, name, var );
		g_error_free (err);
	}
}

void Preferences::get( char *var, int size, const char* name, const char* def )
{
	gchar *temp = g_key_file_get_string( config, GKEYFILE_GROUP, name, NULL );
	if ( temp != NULL )
	{
		strncpy( var, temp, size - 1 );
		g_free( temp );
	}
	else
	{
		strncpy( var, def, size - 1 );
		g_key_file_set_string( config, GKEYFILE_GROUP, name, def );
	}
}

void Preferences::get( bool& var, const char* name, const char* def )
{
	GError *err = NULL;
	var = g_key_file_get_boolean( config, GKEYFILE_GROUP, name, &err );
	if ( err != NULL )
	{
		var = strcmp( def, "false" );
		g_key_file_set_boolean( config, GKEYFILE_GROUP, name, var );
		g_error_free (err);
	}
}


/** The constructor loads the user preferences.
 
    Since this uses gnome-config, preferences are typically
    loaded from ~/.gnome/kino.
*/
Preferences::Preferences() : phyID( -1 )
{
	char* key = new char[4096];
	string filename;
	
	// Give the new config file priority, but failover to the old gnome-config one
	filename = kino_config_file();
	FILE *f = fopen( filename.c_str(), "r" );
	if ( f )
		fclose( f );
	else
		// Try to load old config if nothing yet found
		filename = string( getenv( "HOME" ) ) + string( "/.gnome2/kino" );
	
	// Read the config file
	config = g_key_file_new();
	g_key_file_load_from_file( config, filename.c_str(), G_KEY_FILE_KEEP_COMMENTS, NULL );

	// Put the config entries into member vars with defaults
	get( file, sizeof(file), "file", "capture" );
	get( defaultNormalisation, "defaultNormalisation", "0" );
	get( defaultAudio, "defaultAudio", "2" );
	get( defaultAspect, "defaultAspect", "0" );
	snprintf( key, 4096, "%d", RAW_FORMAT );
	get( fileFormat, "fileFormat", key );
	get( autoSplit, "autoSplit", "true" );
	get( timeStamp, "timeStamp", "false" );
	get( frames, "frames", "0" );
	get( every, "every", "1" );
	get( interface, "interface", "0" );
	get( channel, "channel", "63" );
	get( avcGUID, sizeof(avcGUID), "avcGUID", "" );
	snprintf( key, 4096, "%d", DISPLAY_XV );
	get( displayMode, "displayMode", key );
	get( displayQuality, "displayQuality", "2" );
	get( displayFixed, "displayFixed", "false" );
	get( enableAudio, "enableAudio", "true" );
	get( cip_n, "cip_increment", "0" );
	get( cip_d, "cip_threshold", "0" );
	get( syt_offset, "syt_offset", "0" );
	get( preview_capture, "preview_capture", "true" );
	get( dropFrame, "dropFrame", "true" );
	get( audioDevice, sizeof(audioDevice), "audioDevice2", "/dev/dsp" );
	get( enableV4L, "enableV4L", "false" );
	get( disableKeyRepeat, "disableKeyRepeat", "false" );
	get( audioRendering, "audioRendering", "0" );
	get( maxUndos, "maxUndos", "50" );
	get( dvCaptureBuffers, "dvCaptureBuffers", "50" );
	get( dvExportBuffers, "dvExportBuffers", "10" );
	get( dvDecoderClampLuma, "dvDecoderClampLuma", "false" );
	get( dvDecoderClampChroma, "dvDecoderClampChroma", "false" );
	get( maxFileSize, "maxFileSize", "0" );
	get( audioScrub, "audioScrub", "false" );
	get( v4lVideoDevice, sizeof(v4lVideoDevice), "v4lVideoDevice", "/dev/video0" );
	get( v4lAudioDevice, sizeof(v4lAudioDevice), "v4lAudioDevice", "/dev/dsp" );
	get( v4lInput, sizeof(v4lInput), "v4lInput", "PAL" );
	get( v4lAudio, sizeof(v4lAudio), "v4lAudio", "32000" );
	get( isOpenDML, "isOpenDML", "false" );
	get( defaultDirectory, sizeof(defaultDirectory), "defaultDirectory", "~/" );
	get( displayExtract, "displayExtract", "1" );
	get( relativeSave, "relativeSave", "true" );
	get( dvCaptureDevice, sizeof(dvCaptureDevice), "dvCaptureDevice", "/dev/dv1394/0" );
	get( dv1394Preview, "dv1394Preview", "false" );
	get( dvExportDevice, sizeof(dvExportDevice), "dvExportDevice", "/dev/dv1394/0" );
	get( avcPollIntervalMs, "avcPollIntervalMs", "200" );
	get( dvExportPrerollSec, "dvExportPrerollSec", "4" );
	get( dvTwoPassEncoder, "dvTwoPassEncoder", "true" );
	get( windowWidth, "windowWidth", "-1" );
	get( windowHeight, "windowHeight", "-1" );
	get( storyboardPosition, "storyboardPosition", "-1" );
	get( previewSize, "previewSize", "0" );
	get( timeFormat, "timeFormat", "3" );
	get( trimModeInsert, "trimModeInsert", "false" );
	get( enablePublish, "enablePublish", "true" );
	get( expandStoryboard, "expandStoryboard", "false" );
	get( newProjectURI, sizeof(newProjectURI), "newProjectURI", "" );
	get( newProjectXPath, sizeof(newProjectXPath), "newProjectXPath", "" );
	get( metaNames, sizeof(metaNames), "metaNames", "*title,author,copyright,id,abstract" );
	get( enableAVC, "enableAVC", "true" );

	get( exportMjpegFormat, "exportMjpegFormat", "6" );
	get( exportMjpegAspect, "exportMjpegAspect", "0" );
	get( exportMjpegDeinterlace, "exportMjpegDeinterlace", "0" );
	get( exportMjpegSceneSplit, "exportMjpegSceneSplit", "false" );
	get( exportMjpegCleanup, "exportMjpegCleanup", "true" );
	get( exportMjpegDvdTool, "exportMjpegDvdTool", "1" );
	get( exportMjpegVideoPipe, sizeof(exportMjpegVideoPipe), "exportMjpegVideoPipe", "mpeg2enc -v 0" );
	get( exportMjpegAudioPipe, sizeof(exportMjpegAudioPipe), "exportMjpegAudioPipe", "mp2enc -v 0" );
	get( exportMjpegMultiplex, sizeof(exportMjpegMultiplex), "exportMjpegMultiplex", "mplex -v 0" );

	// build a metaValues key for each metaName
	// metaValues is a range of values to pick from
	// metaValue * = open-ended, values outside range permitted, can be used alongside values
	vector< string > metaNamesList;
	vector< string >::iterator metaNamesIter;
	
	StringUtils::split( metaNames, ",", metaNamesList );
	for ( metaNamesIter = metaNamesList.begin(); metaNamesIter != metaNamesList.end(); ++metaNamesIter )
	
	{
		std::ostringstream str;
		std::string metaname = *metaNamesIter;

		if ( metaname.at( 0 ) == '*' )
			metaname.erase( 0, 1 );

		str << "metaValues_" << metaname;
		if ( metaname == "copyright" )
			get( key, 4096, str.str().c_str(), "No license (All rights reserved)=-1,Creative Commons Attribution=1,Creative Commons Attribution-NoDerivs=2,Creative Commons Attribution-NonCommercial-NoDerivs=3,Creative Commons Attribution-NonCommercial=4,Creative Commons Attribution-NonCommercial-ShareAlike=5,Creative Commons Attribution-ShareAlike=6,Public Domain=7,*" );
		else
			get( key, 4096, str.str().c_str(), "*" );
		{
			vector< string > metaValuesSimpleList;
			vector< string >::iterator iter;
			vector< pair< string, string > > metaValuesList;

			StringUtils::split( key, ",", metaValuesSimpleList );
			for ( iter = metaValuesSimpleList.begin(); iter != metaValuesSimpleList.end(); ++iter )
			{
				vector< string > labelValueVector;
				pair< string, string > labelValuePair;
				
				StringUtils::split( *iter, "=", labelValueVector );
				if ( labelValueVector.size() > 1 )
					labelValuePair = make_pair( labelValueVector[0], labelValueVector[1] );
				else
					labelValuePair = make_pair( *iter, *iter );
				metaValuesList.push_back( labelValuePair );
			}
			metaValues.insert( make_pair( metaname, metaValuesList ) );
		}
	}

	JogShuttleInit();
	RecentFilesInit();
	
	Save();
	delete[] key;
}


Preferences::~Preferences()
{
	g_key_file_free( config );
}

/** Initialise JogShuttle bindings
 
    Turns out to be an evil resource hog at compile time. So it was
    split into several smaller functions to lower the memory footprint.
 */
void Preferences::JogShuttleInit(void)
{
	/// Jogshuttle stuff
	get( enableJogShuttle, "enableJogShuttle", "false" );
	_isGrabbing = false;
	/* Fill array of actions */
	/* Element 0 is empty */
	int count = 0; /** Ease moving code around */
	/* "<none>" should always go first */
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "<none>" ),
	                       "",
	                       "" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++,
	                       _( "Toggle play" ),
	                       _( "Toggle between play and pause" ),
	                       " " ) );
	
	JogShuttleInitNavigation(count);
	JogShuttleInitCut(count);
	JogShuttleInitCopy(count);
	JogShuttleInitPaste(count);
	JogShuttleInitModeSwitching(count);
	JogShuttleInitGeneral(count);
	JogShuttleInitTrim(count);

	// TODO: Add more actions?

	/* Actually get the preferences */
	get( count, "jogShuttleNumMappings", "0" );
	if ( 0 != count )
	{
		for ( int i = 0; i < count ; i++ )
		{
			char key[ 512 ];
			sprintf( key, "jogShuttleMappingFirstButton%i", i );
			int first_code;
			get( first_code, key, "0" );
			sprintf( key, "jogShuttleMappingSecondButton%i", i );
			int second_code;
			get( second_code, key, "0" );
			sprintf( key, "jogShuttleMappingAction%i", i );
			// g_print ( " Key = %s\n", key );
			get( key, sizeof(key), key, "" );

			// TODO: Free
			// g_print( "tmpstring Read action %s\n", tempString );
			string action = string( key );
			// g_print( "Read action %s\n", action.c_str() );
			/* Insert */
			_JogShuttleMappings[ make_pair ( first_code, second_code ) ]
			= JogShuttleMapping( action );
		}
	}
	else
	{
		// Set up defaults manuall, if 0 count
		// These are my own default mappings
		_JogShuttleMappings[ make_pair ( 0x100, 0x0 ) ]
		= JogShuttleMapping( "i" );
		_JogShuttleMappings[ make_pair ( 0x101, 0x0 ) ]
		= JogShuttleMapping( "o" );
		_JogShuttleMappings[ make_pair ( 0x102, 0x0 ) ]
		= JogShuttleMapping( "h" );
		_JogShuttleMappings[ make_pair ( 0x103, 0x0 ) ]
		= JogShuttleMapping( "l" );
		
		_JogShuttleMappings[ make_pair ( 0x113, 0x0 ) ]
		= JogShuttleMapping( "b" );
		_JogShuttleMappings[ make_pair ( 0x111, 0x0 ) ]
		= JogShuttleMapping( "w" );
		_JogShuttleMappings[ make_pair ( 0x110, 0x0 ) ]
		= JogShuttleMapping( " " );
		_JogShuttleMappings[ make_pair ( 0x112, 0x0 ) ]
		= JogShuttleMapping( "k" );
		_JogShuttleMappings[ make_pair ( 0x114, 0x0 ) ]
		= JogShuttleMapping( "j" );
		_JogShuttleMappings[ make_pair ( 0x115, 0x0 ) ]
		= JogShuttleMapping( "gg" );
		_JogShuttleMappings[ make_pair ( 0x116, 0x0 ) ]
		= JogShuttleMapping( "G" );
		
		_JogShuttleMappings[ make_pair ( 0x10, 0x0 ) ]
		= JogShuttleMapping( " " );
	}
}

/// Set up JogShuttle navigation bindings
void Preferences::JogShuttleInitNavigation(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Forward one frame" ),
	                       _( "Move one frame forward" ),
	                       "l" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Back one frame" ),
	                       _( "Move one frame backward" ),
	                       "h" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Forward one second" ),
	                       _( "Move one second forward" ),
	                       "w" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Forward 30 seconds" ),
	                       _( "Move 30 seconds forward" ),
	                       "30w" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Back one second" ),
	                       _( "Move one second backward" ),
	                       "b" ) );


	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Back 30 seconds" ),
	                       _( "Move 30 seconds backward" ),
	                       "30b" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Start of scene" ),
	                       _( "Move to start of current scene" ),
	                       "0" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "End of scene" ),
	                       _( "Move to end of current scene" ),
	                       "$" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Previous scene" ),
	                       _( "Move to start of previous scene" ),
	                       "k" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Next scene" ),
	                       _( "Move to start of next scene" ),
	                       "j" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "First frame" ),
	                       _( "Move to beginning of movie" ),
	                       "gg" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Last frame" ),
	                       _( "Move to end of movie" ),
	                       "G" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Forward 5 scenes" ),
	                       _( "Move forward 5 scenes" ),
	                       "5$" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Back 5 scene" ),
	                       _( "Move backward 5 scenes" ),
	                       "5^" ) );
}

/// Set up JogShuttle cut bindings
void Preferences::JogShuttleInitCut(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut current frame" ),
	                       _( "Cut current frame" ),
	                       "x" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut a second" ),
	                       _( "Cut a second" ),
	                       "dw" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut current scene" ),
	                       _( "Cut current scene" ),
	                       "dd" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut to end of current scene" ),
	                       _( "Cut to end of current scene" ),
	                       "d$" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut to end of movie" ),
	                       _( "Cut to end of movie" ),
	                       "dG" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut from start of current scene" ),
	                       _( "Cut from start of current scene" ),
	                       "d0" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Cut from start of movie" ),
	                       _( "Cut from start of movie" ),
	                       "dgg" ) );
}

/// Set up JogShuttle copy bindings
void Preferences::JogShuttleInitCopy(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Copy current scene" ),
	                       _( "Copy current scene" ),
	                       "yy" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Copy to end of scene" ),
	                       _( "Copy to end of scene" ),
	                       "y$" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Copy from start of scene" ),
	                       _( "Cut from start of scene" ),
	                       "y0" ) );
}

/// Set up JogShuttle paste bindings
void Preferences::JogShuttleInitPaste(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Paste after current frame" ),
	                       _( "Paste after current frame" ),
	                       "p" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Paste before current frame" ),
	                       _( "Paste before current frame" ),
	                       "P" ) );
}

/// Set up JogShuttle mode switching bindings
void Preferences::JogShuttleInitModeSwitching(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Switch to Edit" ),
	                       _( "Switch to the edit interface" ),
	                       "F2" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Switch to Capture" ),
	                       _( "Switch to the capture interface" ),
	                       "A" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Switch to Timeline" ),
	                       _( "Switch to the capture interface" ),
	                       "v" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Switch to Export" ),
	                       _( "Switch to the capture interface" ),
	                       ":W" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Switch to FX" ),
	                       _( "Switch to the capture interface" ),
	                       "C" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Switch to Trim" ),
	                       _( "Switch to trim mode" ),
	                       "t" ) );
}

/// Set up JogShuttle general bindings
void Preferences::JogShuttleInitGeneral(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Repeat last command" ),
	                       _( "Repeat last command" ),
	                       "." ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Split scene" ),
	                       _( "Split scene before current frame" ),
	                       "Ctrl+J" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Join scenes" ),
	                       _( "Join this scene with the following" ),
	                       "J" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Undo" ),
	                       _( "Undo last command" ),
	                       "u" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Redo" ),
	                       _( "Redo last command" ),
	                       "Ctrl+R" ) );
						   
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Capture/Overwrite/Export/Render" ),
	                       _( "Start Capture, Export, or Render or Apply Overwrite" ),
	                       "Enter" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Stop" ),
	                       _( "Stop transport, capture, export, or render" ),
	                       "Esc" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Publish SMIL" ),
	                       _( "Publish SMIL" ),
	                       "Ctrl+W" ) );
}

/// Set up JogShuttle trim bindings
void Preferences::JogShuttleInitTrim(int& count)
{
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Set In" ),
	                       _( "Set the In Point to current position" ),
	                       "i" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Set Out" ),
	                       _( "Set the Out Point to current position" ),
	                       "o" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Insert Before" ),
	                       _( "Insert Before" ),
	                       ":r" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Insert After" ),
	                       _( "Insert After" ),
	                       ":a" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Toggle Insert/Overwrite" ),
	                       _( "Toggle Insert/Overwrite" ),
	                       "Ins" ) );
	                       
	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Toggle Looping" ),
	                       _( "Toggle Looping" ),
	                       "\\" ) );

	_JogShuttleActions.push_back(
	    JogShuttleActions( count++, _( "Toggle Link" ),
	                       _( "Toggle Link" ),
	                       "=" ) );
}
/** Save the user preferences.
*/

void Preferences::set( const char *name, const char* value )
{
	g_key_file_set_string( config, GKEYFILE_GROUP, name, value );
}

void Preferences::set( const char *name, int value )
{
	g_key_file_set_integer( config, GKEYFILE_GROUP, name, value );
}

void Preferences::set( const char *name, bool value )
{
	g_key_file_set_boolean( config, GKEYFILE_GROUP, name, value );
}

/// Save the user preferences.
void
Preferences::Save()
{
	set( "defaultNormalisation", defaultNormalisation );
	set( "defaultAudio", defaultAudio );
	set( "defaultAspect", defaultAspect );
	set( "file", file );
	set( "fileFormat", fileFormat );
	set( "autoSplit", autoSplit );
	set( "timeStamp", timeStamp );
	set( "frames", frames );
	set( "every", every );
	set( "interface", interface );
	set( "channel", channel );
	set( "avcGUID", avcGUID );
	set( "displayMode", displayMode );
	set( "displayQuality", displayQuality );
	set( "displayFixed", displayFixed );
	set( "enableAudio", enableAudio );
	set( "cip_increment", cip_n );
	set( "cip_threshold", cip_d );
	set( "syt_offset", syt_offset );
	set( "preview_capture", preview_capture );
	set( "dropFrame", dropFrame );
	set( "audioDevice2", audioDevice );

	/* JogShuttle Stuff */
	set( "enableJogShuttle", enableJogShuttle );
	set( "jogShuttleNumMappings", static_cast< int >( _JogShuttleMappings.size() ) );
	int count = 0;
	map<pair<unsigned short, unsigned short>, JogShuttleMapping>::iterator i;
	pair<unsigned short, unsigned short> map_key;
	for ( i = _JogShuttleMappings.begin();
	        i != _JogShuttleMappings.end() ; i++, count++ )
	{
		map_key = ( *i ).first;
		char key[ 512 ];
		sprintf( key, "jogShuttleMappingFirstButton%i", count );
		set( key, map_key.first );
		sprintf( key, "jogShuttleMappingSecondButton%i", count );
		set( key, map_key.second );
		sprintf( key, "jogShuttleMappingAction%i", count );
		set( key, ( *i ).second._action.c_str() );
	}

	set( "enableV4L", enableV4L );
	set( "disableKeyRepeat", disableKeyRepeat );
	set( "audioRendering", audioRendering );
	set( "maxUndos", maxUndos );
	set( "dvCaptureBuffers", dvCaptureBuffers );
	set( "dvExportBuffers", dvExportBuffers );
	set( "dvDecoderClampLuma", dvDecoderClampLuma );
	set( "dvDecoderClampChroma", dvDecoderClampChroma );
	set( "maxFileSize", maxFileSize );
	set( "audioScrub", audioScrub );
	set( "v4lVideoDevice", v4lVideoDevice );
	set( "v4lAudioDevice", v4lAudioDevice );
	set( "v4lInput", v4lInput );
	set( "v4lAudio", v4lAudio );
	set( "isOpenDML", isOpenDML );
	set( "defaultDirectory", defaultDirectory );
	set( "displayExtract", displayExtract );
	set( "relativeSave", relativeSave );

	set( "dvCaptureDevice", dvCaptureDevice );
	set( "dv1394Preview", dv1394Preview );
	set( "dvExportDevice", dvExportDevice );
	set( "avcPollIntervalMs", avcPollIntervalMs );
	set( "dvExportPrerollSec", dvExportPrerollSec );
	set( "dvTwoPassEncoder", dvTwoPassEncoder );
	set( "windowWidth", windowWidth );
	set( "windowHeight", windowHeight );
	set( "storyboardPosition", storyboardPosition );
	set( "previewSize", previewSize );
	set( "timeFormat", timeFormat );
	set( "metaNames", metaNames );
	set( "enablePublish", enablePublish );
	set( "expandStoryboard", expandStoryboard );
	set( "newProjectURI", newProjectURI );
	set( "newProjectXPath", newProjectXPath );
	set( "trimModeInsert", trimModeInsert );
	set( "enableAVC", enableAVC );
	
	set( "exportMjpegFormat", exportMjpegFormat );
	set( "exportMjpegDeinterlace", exportMjpegDeinterlace );
	set( "exportMjpegAspect", exportMjpegAspect );
	set( "exportMjpegSceneSplit", exportMjpegSceneSplit );
	set( "exportMjpegCleanup", exportMjpegCleanup );
	set( "exportMjpegDvdTool", exportMjpegDvdTool );
	set( "exportMjpegVideoPipe", exportMjpegVideoPipe );
	set( "exportMjpegAudioPipe", exportMjpegAudioPipe );
	set( "exportMjpegMultiplex", exportMjpegMultiplex );

	RecentFilesSave();

	// Persist
	string filename = kino_config_file();
	FILE *f = fopen( filename.c_str(), "w" );
	if ( f )
	{
		gsize data_size = 0;
		gchar *data = g_key_file_to_data( config, &data_size, NULL );
		fwrite( data, data_size, 1, f );
		fclose( f );
		g_free( data );
	}
	
	// Delete the old file if it exists (ignore error)
	filename = string( getenv( "HOME" ) ) + string( "/.gnome2/kino" );
	unlink( filename.c_str() );
	errno = 0;
}

void Preferences::RecentFilesInit()
{
	char* key = new char[64];
	char* value = new char[ PATH_MAX + NAME_MAX ];

	for ( int i = 0; i < 5 ; i++ )
	{
		sprintf( key, "recentFile%i", i );
		value[0] = 0;
		get( value, PATH_MAX + NAME_MAX, key, "" );
		if ( strcmp( value, "" ) )
			recentFiles.push_back( value );
	}
	
	delete[] key;
	delete[] value;
}

void Preferences::RecentFilesSave()
{
	vector<string>::iterator i;
	int count = 0;

	for ( i = recentFiles.begin(); i != recentFiles.end(); i++, count++ )
	{
		char key[64];
		sprintf( key, "recentFile%i", count );
		set( key, (*i).c_str() );
	}
}

void Preferences::addRecentFile( string filename )
{
	vector<std::string>::iterator i;
	for ( i = recentFiles.begin(); i != recentFiles.end(); i++ )
	{
		if ( (*i) == filename )
		{
			recentFiles.erase( i );
			recentFiles.push_back( filename );
			break;
		}
	}
	if ( i == recentFiles.end() )
	{
		if ( recentFiles.size() > 4 )
			recentFiles.erase( recentFiles.begin() );
		recentFiles.push_back( filename );
	}
}

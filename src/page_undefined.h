/*
* page_undefined.h Notebook Dummy Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_UNDEFINED_H
#define _PAGE_UNDEFINED_H

#include "kino_common.h"
#include "page.h"

/** Defines the action of a dummy page (ie: does nothing).
*/

class PageUndefined : public Page
{
private:
	KinoCommon *common;
public:
	PageUndefined( KinoCommon *common )
	{
		this->common = common;
	}
};

#endif

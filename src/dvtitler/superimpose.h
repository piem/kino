/*
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _SUPERIMPOSE_H
#define _SUPERIMPOSE_H 1


#include <gdk-pixbuf/gdk-pixbuf.h>
#include "../image_filters.h"
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "dvtitler.h"

class Superimpose : public DVTitler
{
public:
	Superimpose();
	virtual ~Superimpose() {};
	char *GetDescription( ) const;
	void AttachWidgets( GtkBin *bin );
	void DetachWidgets( GtkBin *bin );
	void InterpretWidgets( GtkBin *bin );
	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta );

protected:
	static char file[ PATH_MAX + NAME_MAX ];
	GtkWidget *file_entry;
	int count;
	double zoom;
	bool hasFilenameChanged;
	int count_offset;
};

#endif

/*
 * Copyright (C) 2003-2004 Alejandro Aguilar Sierra <asierra@servidor.unam.mx>
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _TEXT_BLOCK_H
#define _TEXT_BLOCK_H

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <pango/pangoft2.h>

const int TEXTBLOCK_ALIGN_LEFT = 0;
const int TEXTBLOCK_ALIGN_CENTER = 1;
const int TEXTBLOCK_ALIGN_RIGHT = 2;

struct DVColor;

class TextBlock
{
public:
	TextBlock();
	~TextBlock();

	GdkPixbuf *getPixbuf( DVColor fg, DVColor bg, int outline, DVColor outlinecolor, bool isMarkup );

	void setSpace( int p )
	{
		space = p;
	}

	void setAlign( int p )
	{
		align = p;
	}

	void setPad( int width, int height )
	{
		padw = width;
		padh = height;
	}

	void setFont( const char *f )
	{
		font = pango_font_description_from_string( f );
	}

	int getAlign()
	{
		return align;
	}

	int getPadWidth()
	{
		return padw;
	}

	int getPadHeight()
	{
		return padh;
	}

	int getSpace()
	{
		return space;
	}

	void setText( char *s )
	{
		text = s;
	}

private:
	void fillRectangle( GdkPixbuf *pixbuf, DVColor bg );
	void drawPixbuf( GdkPixbuf *pixbuf, DVColor fg, DVColor bg, int outline, DVColor outlinecolor );

	int align;
	int padw, padh;
	int space;
	char *text;

	int w, h;
	PangoFT2FontMap *fontmap;
	PangoContext *context;
	PangoFontDescription *font;
	PangoLayout *layout;
};


#endif



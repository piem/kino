/*
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <glib/gi18n.h>
#include <string.h>
#include <stdlib.h>

#include "superimpose.h"
#include "dvtitler.h"
#include "../kino_extra.h"

#define GAP_MAX (9999)

char Superimpose::Superimpose::file[ PATH_MAX + NAME_MAX ] = "";

static void
on_button_file_clicked( GtkButton *button, gpointer user_data );

Superimpose::Superimpose()
	: hasFilenameChanged( false )
	, count_offset( 0 )
{
	GtkWidget* widget = glade_xml_get_widget( glade, "button_superimpose_open" );
	GtkWidget* widget2 = glade_xml_get_widget( glade, "entry_superimpose" );
	g_signal_connect( G_OBJECT( widget ), "clicked", G_CALLBACK( on_button_file_clicked ), widget2 );
	g_signal_connect( G_OBJECT( widget2 ), "activate", G_CALLBACK( Repaint ), 0 );
	g_signal_connect( G_OBJECT( glade_xml_get_widget( glade, "hscale_superimpose_zoom" ) ), "value-changed", G_CALLBACK( Repaint ), 0 );
}

char *Superimpose::GetDescription( ) const
{
	return _("Superimpose");
}

void Superimpose::FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
{
	if ( strcmp( file, "" ) == 0 )
		return;

	GError* gerror = NULL;
	bool isUnscaled = false;
	SelectedFrames& fx = GetSelectedFramesForFX();
	
	// Determine if this is an image sequence
	if ( strchr( file, '%' ) != NULL )
	{
		char full[ PATH_MAX + NAME_MAX ];
		int gap = 0;
	
		if ( fx.IsRepainting() || fx.IsPreviewing() )
		{
			// compute the relative count
			count = fx.GetIndex( position );
			
			// determine offset to first file
			if ( hasFilenameChanged )
			for ( count_offset = 0; count_offset < GAP_MAX; ++count_offset )
			{
				struct stat buf;
				snprintf( full, PATH_MAX + NAME_MAX, file, count + count_offset );
				if ( stat( full, &buf ) == 0 )
					break;
			}
			count += count_offset;
		}
		// Permit gaps in the file number sequence up to 100 difference
		while ( gap < GAP_MAX )
		{
			struct stat buf;
			snprintf( full, PATH_MAX + NAME_MAX, file, count++ );
			if ( stat( full, &buf ) == 0 )
				break;
			else
				gap ++;
		}
		// If successfully obtained a new file
		if ( gap < GAP_MAX )
		{
			pixbuf = gdk_pixbuf_new_from_file( full, &gerror );
			isUnscaled = true;
		}
	}
	// Otherwise, import single image just once
	else if ( hasFilenameChanged || fx.IsRepainting() )
	{
		if ( pixbuf )
			g_object_unref( pixbuf );
		pixbuf = gdk_pixbuf_new_from_file( file, &gerror );
		isUnscaled = true;
	}

	// If gdk-pixbuf loader generated an error
	if ( gerror != NULL )
	{
		if ( hasFilenameChanged )
		{
			if ( fx.IsPreviewing() )
				gdk_threads_enter();
			GtkWidget * dialog = gtk_message_dialog_new(
									GetKinoWidgetWindow(),
									GTK_DIALOG_DESTROY_WITH_PARENT,
									GTK_MESSAGE_ERROR,
									GTK_BUTTONS_CLOSE,
									gerror->message );
			gtk_dialog_run( GTK_DIALOG( dialog ) );
			gtk_widget_destroy( dialog );
			if ( fx.IsPreviewing() )
				gdk_threads_leave();
		}
		g_error_free( gerror );
		if ( pixbuf )
			g_object_unref( pixbuf );
		pixbuf = 0;
	}
	// Otherwise, if there is a pixbuf
	else if ( pixbuf && isUnscaled )
	{
		if ( gdk_pixbuf_get_has_alpha( pixbuf ) == FALSE )
		{
			GdkPixbuf *temp = pixbuf;
			GdkPixbuf *alpha = gdk_pixbuf_add_alpha( pixbuf, FALSE, 0, 0, 0 );
			pixbuf = alpha;
			g_object_unref( temp );
		}

		int w = gdk_pixbuf_get_width ( pixbuf );
		int h = gdk_pixbuf_get_height ( pixbuf );
		GdkInterpType interp = GDK_INTERP_HYPER;

		// scale to preview size
		if ( width < 720 )
		{
			w /= 4;
			h /= 4;
			xoff /= 4;
			yoff /= 4;
			interp = GDK_INTERP_BILINEAR;
		}
		// scale to adjust for NTSC sample aspect ratio
		if ( height < 576 )
		{
			GdkPixbuf * temp = pixbuf;
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( pixbuf,
				gint( double( w ) * 720.0/640.0 * zoom + 0.5 ), gint( h * zoom + 0.5 ), interp );
			pixbuf = scaled;
			g_object_unref( temp );
		}
		// scale to adjust for PAL sample aspect ratio
		else
		{
			GdkPixbuf * temp = pixbuf;
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( pixbuf,
				gint( float( w ) * 720.0/768.0 * zoom + 0.5 ), gint( h * zoom + 0.5 ), interp );
			pixbuf = scaled;
			g_object_unref( temp );
		}
		
		w = gdk_pixbuf_get_width ( pixbuf );
		h = gdk_pixbuf_get_height ( pixbuf );

		// if Left Off -title width, else if Right Off frame width,
		// else (Left = 0, Center = 1, Right = 2) * difference / 2
		// pih = initial horizontal position
		// pfh = final horizontal position
		pih = ( ih == 3 ) ? -w : ( ih > 3 ) ? width : ( float ) ih * ( width - w ) / 2.0;
		pfh = ( fh == 3 ) ? -w : ( fh > 3 ) ? width : ( float ) fh * ( width - w ) / 2.0;

		// if Top Off -title height, else if Bottom Off frame height, else
		// else (Top = 0, Middle = 1, Bottom = 2) * difference / 2
		// piv = initial vertical position
		// pfv = final vertical position
		piv = ( iv == 3 ) ? -h : ( iv > 2 ) ? height : ( float ) iv * ( height - h ) / 2.0;
		pfv = ( fv == 3 ) ? -h : ( fv > 2 ) ? height : ( float ) fv * ( height - h ) / 2.0;

		// store the width and height for future use in other methods
		frame_w = width;
		frame_h = height;
	}

	if ( pixbuf )
	{
		for ( int field = 0; field < ( interlaced ? 2 : 1 ); ++ field )
		{
			// Offset the position based on which field we're looking at ...
			const double field_position = position + ( 1 - field ) * frame_delta * 0.5;
				
			// apply the progress factor, position, to the path as an offset to the
			// initial position plus apply user-supplied offsets
			int x = ( int ) ( field_position * ( pfh - pih ) + pih ) + xoff;
			int y = ( int ) ( field_position * ( pfv - piv ) + piv ) + yoff;

			//std::cerr << "drawing title on field " << field << " at " << x << ", " << y << std::endl;
			drawPixbuf( pixels, // destination buffer
                                    x, y,
                                    width * 3, // destination buffer stride
                                    ( 1 - field ),
                                    position, frame_delta );
		}
	}
	hasFilenameChanged = false;
}

static void
on_button_file_clicked( GtkButton *button, gpointer user_data )
{
	GtkWidget *widget = static_cast< GtkWidget* >( user_data );
	GtkWidget *dialog;
	gchar *filename = NULL;

	dialog = gtk_file_chooser_dialog_new( _("Select an Image"),
				GetKinoWidgetWindow(),
				GTK_FILE_CHOOSER_ACTION_OPEN,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				NULL );
	gtk_dialog_set_alternative_button_order( GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_CANCEL, -1 );
	gtk_window_set_modal( GTK_WINDOW( dialog ), TRUE );
	char* folder = strdup( gtk_entry_get_text( GTK_ENTRY( widget ) ) );
	if ( strrchr( folder, '/' ) )
		*( strrchr( folder, '/' ) + 1 ) = '\0';
	gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( dialog ), folder );
	free( folder );
	if ( gtk_dialog_run( GTK_DIALOG( dialog ) ) == GTK_RESPONSE_ACCEPT )
		filename = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER( dialog ) );
	gtk_widget_destroy( dialog );
	if ( filename && strcmp( filename, "" ) )
		gtk_entry_set_text( GTK_ENTRY( widget ), filename );
	g_free( filename );
}
	
void Superimpose::AttachWidgets( GtkBin *bin )
{
	GtkWidget *vbox_dvtitler = glade_xml_get_widget( glade, "vbox_dvtitler" );
	GtkWidget *vbox_superimpose = glade_xml_get_widget( glade, "vbox_superimpose" );
	GtkWidget *widget = glade_xml_get_widget( glade, "checkbutton_interlaced" );

	// Detach the Position and Animation section from the Titler
	g_object_ref( widget );
	gtk_container_remove( GTK_CONTAINER( vbox_dvtitler ), widget );
	gtk_box_pack_start( GTK_BOX( vbox_superimpose ), widget, FALSE, TRUE, 0 );
	g_object_unref( widget );

	// Attach the Position and Animation section to the Superimpose
	widget = glade_xml_get_widget( glade, "expander_position" );
	g_object_ref( widget );
	gtk_container_remove( GTK_CONTAINER( vbox_dvtitler ), widget );
	gtk_box_pack_start( GTK_BOX( vbox_superimpose ), widget, FALSE, TRUE, 0 );
	g_object_unref( widget );


	window = glade_xml_get_widget( glade, "superimpose" );
	gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
}

void Superimpose::DetachWidgets( GtkBin *bin )
{
	GtkWidget *vbox_dvtitler = glade_xml_get_widget( glade, "vbox_dvtitler" );
	GtkWidget *vbox_superimpose = glade_xml_get_widget( glade, "vbox_superimpose" );
	GtkWidget *widget = glade_xml_get_widget( glade, "checkbutton_interlaced" );

	// Detach the Position and Animation section from the Superimpose
	g_object_ref( widget );
	gtk_container_remove( GTK_CONTAINER( vbox_superimpose ), widget );
	gtk_box_pack_start( GTK_BOX( vbox_dvtitler ), widget, FALSE, TRUE, 0 );
	g_object_unref( widget );

	// Attach the Position and Animation section to the Titler
	widget = glade_xml_get_widget( glade, "expander_position" );
	g_object_ref( widget );
	gtk_container_remove( GTK_CONTAINER( vbox_superimpose ), widget );
	gtk_box_pack_start( GTK_BOX( vbox_dvtitler ), widget, FALSE, TRUE, 0 );
	g_object_unref( widget );

	widget = glade_xml_get_widget( glade, "entry_superimpose" );
	if ( gtk_entry_get_text( GTK_ENTRY( widget ) ) )
		strcpy( file, gtk_entry_get_text( GTK_ENTRY( widget ) ) );

	window = glade_xml_get_widget( glade, "superimpose" );
	gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
}

void Superimpose::InterpretWidgets( GtkBin *bin )
{
	GtkWidget *widget = glade_xml_get_widget( glade, "filechooserbutton_superimpose" );

	widget = glade_xml_get_widget( glade, "entry_superimpose" );
	if ( gtk_entry_get_text( GTK_ENTRY( widget ) ) )
	{
		int n = PATH_MAX + NAME_MAX - 1;
		char newFileName[ n + 1 ];
		newFileName[ n ] = '\0';
		strncpy( newFileName, gtk_entry_get_text( GTK_ENTRY( widget ) ), n );
		SelectedFrames& fx = GetSelectedFramesForFX();
		if ( strcmp( newFileName, file ) || !( fx.IsRepainting() || fx.IsPreviewing() ) )
			hasFilenameChanged = true;
		strcpy( file, newFileName );
	}
	else
	{
		throw _( "No image file name specified - aborting." );
	}
	count = 0;
	widget = glade_xml_get_widget( glade, "hscale_superimpose_zoom" );
	zoom = gtk_range_get_value( GTK_RANGE( widget ) ) / 100.0;
	DVTitler::InterpretWidgets( bin );
}


/*
 * Copyright (C) 2001-2005 Alejandro Aguilar Sierra <asierra@servidor.unam.mx>
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _DVTITLER_H
#define _DVTITLER_H 1


#include <gdk-pixbuf/gdk-pixbuf.h>
#include "../image_filters.h"
#include <gtk/gtk.h>
#include <glade/glade.h>


struct DVColor
{
	guint8 r, g, b, a;
};

class TextBlock;

class DVTitler : public GDKImageFilter
{
public:
	DVTitler();
	virtual ~DVTitler();
	char *GetDescription( ) const;
	void AttachWidgets( GtkBin *bin );
	void DetachWidgets( GtkBin *bin );
	void InterpretWidgets( GtkBin *bin );
	void FilterFrame( uint8_t *io, int width, int height, double position, double frame_delta );

protected:
	void drawPixbuf( uint8_t *io, int x, int y, int row_w, int field, double position, double frame_delta );
	void drawRectangle( uint8_t *io, int x, int y, int w, int h, int row_w );
	char *getFontPath( char *fontname );
	bool isTextDynamic();

private:
	char *text;
	DVColor colorfg, colorbg, coloroutline;
	TextBlock *titler;
	bool isMarkup;

protected:
	GladeXML *glade;
	GtkWidget *window;
	int padw, padh, space, size;
	int fadein, fadeout, outline;
	int xoff, yoff;
	int ih, iv, fh, fv;
	float pih, pfh, piv, pfv;
	int frame_w, frame_h;
	GdkPixbuf *pixbuf;
	bool interlaced;
	double position;
};

#endif


/*
 * Copyright (C) 2001-2004 Alejandro Aguilar Sierra <asierra@servidor.unam.mx>
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "textblock.h"
#include <stdint.h>

struct DVColor
{
	guint8 r, g, b, a;
};

TextBlock::TextBlock()
{
	padw = padh = space = 0;
	align = 0;
	w = h = 0;
	fontmap = (PangoFT2FontMap*) pango_ft2_font_map_new();
	pango_ft2_font_map_set_resolution( fontmap, 72, 72 );
	context = pango_ft2_font_map_create_context( fontmap );
	layout = pango_layout_new( context );
}


TextBlock::~TextBlock()
{
	g_object_unref( layout );
	g_object_unref( context );
	g_object_unref( fontmap );
}

GdkPixbuf *TextBlock::getPixbuf( DVColor fg, DVColor bg, int outline, DVColor outlinecolor, bool isMarkup )
{
	pango_layout_set_width( layout, /*720 * PANGO_SCALE*/ -1 ); // set wrapping constraints
	pango_layout_set_font_description( layout, font );
	pango_layout_set_spacing( layout, space );
	pango_layout_set_alignment( layout, static_cast< PangoAlignment >( align ) );
	if ( isMarkup )
	{
		pango_layout_set_text( layout, "", -1 );
		pango_layout_set_markup( layout, text, (text == NULL ? 0 : strlen( text ) ) );
	}
	else
	{
		pango_layout_set_markup( layout, "", -1 );
		pango_layout_set_text( layout, text, (text == NULL ? 0 : strlen( text ) ) );
	}
	pango_layout_get_pixel_size( layout, &w, &h );

	GdkPixbuf *pixbuf = gdk_pixbuf_new( GDK_COLORSPACE_RGB,
	                                    TRUE, /* has alpha */
	                                    8,
	                                    w + 2 * padw, h + 2 * padh );
	if ( pixbuf )
	{
		fillRectangle( pixbuf, bg );
		drawPixbuf( pixbuf, fg, bg, outline, outlinecolor );
	}
	
	return pixbuf;
}


void TextBlock::fillRectangle( GdkPixbuf *pixbuf, DVColor bg )
{
	int ww = gdk_pixbuf_get_width( pixbuf );
	int hh = gdk_pixbuf_get_height( pixbuf );
	uint8_t *p = gdk_pixbuf_get_pixels( pixbuf );

	for ( int j = 0; j < hh; j++ )
	{
		for ( int i = 0; i < ww; i++ )
		{
			*p++ = bg.r;
			*p++ = bg.g;
			*p++ = bg.b;
			*p++ = bg.a;
		}
	}
}


void TextBlock::drawPixbuf( GdkPixbuf *pixbuf, DVColor fg, DVColor bg, int outline, DVColor outlinecolor )
{
	int x;
	int stride = gdk_pixbuf_get_rowstride( pixbuf );
	FT_Bitmap bitmap;
	
	bitmap.rows      = h;
	bitmap.width     = w;
	bitmap.pitch     = 32 * ( ( w + 31 ) / 31 );
	bitmap.buffer    = static_cast< unsigned char * >( calloc( 1, h * bitmap.pitch ) );
	bitmap.num_grays = 256;
	bitmap.pixel_mode = ft_pixel_mode_grays;

	pango_ft2_render_layout( &bitmap, layout, 0, 0 );
	
	x = ( gdk_pixbuf_get_width( pixbuf ) - w - 2 * padw ) * align / 2 + padw;
	uint8_t *dest = gdk_pixbuf_get_pixels( pixbuf ) + 4 * x + padh * stride;

	for ( int j = 0; j < h; j++ )
	{
		uint8_t *d = dest;
		for ( int i = 0; i < w; i++ )
		{
			float a_outline = 0;
			if ( outline > 0 && fg.a > 0 ) 
			{
#define 		geta(x, y) (float) bitmap.buffer[ (y) * bitmap.pitch + (x) ] / 255.0

				a_outline = geta(i, j);
				// One pixel fake circle
				if ( i > 0 )
					a_outline = MAX( a_outline, geta(i - 1, j) );
				if ( i < w - 1 )
					a_outline = MAX( a_outline, geta(i + 1, j) );
				if ( j > 0 )
					a_outline = MAX( a_outline, geta(i, j - 1) );
				if ( j < h - 1 )
					a_outline = MAX( a_outline, geta(i, j + 1) );
				if ( outline >= 2 ) {
					// Two pixels fake circle
					if ( i > 1 ) {
						a_outline = MAX( a_outline, geta(i - 2, j) );
						if ( j > 0 )
							a_outline = MAX( a_outline, geta(i - 2, j - 1) );
						if ( j < h - 1 )
							a_outline = MAX( a_outline, geta(i - 2, j + 1) );
					}
					if ( i > 0 ) {
						if ( j > 0 )
							a_outline = MAX( a_outline, geta(i - 1, j - 1) );
						if ( j > 1 )
							a_outline = MAX( a_outline, geta(i - 1, j - 2) );
						if ( j < h - 1 )
							a_outline = MAX( a_outline, geta(i - 1, j + 1) );
						if ( j < h - 2 )
							a_outline = MAX( a_outline, geta(i - 1, j + 2) );
					}
					if ( j > 1 )
						a_outline = MAX( a_outline, geta(i, j - 2) );
					if ( j < h - 2 )
						a_outline = MAX( a_outline, geta(i, j + 2) );
					if ( i < w - 1 ) {
						if ( j > 0 )
							a_outline = MAX( a_outline, geta(i + 1, j - 1) );
						if ( j > 1 )
							a_outline = MAX( a_outline, geta(i + 1, j - 2) );
						if ( j < h - 1 )
							a_outline = MAX( a_outline, geta(i + 1, j + 1) );
						if ( j < h - 2 )
							a_outline = MAX( a_outline, geta(i + 1, j + 2) );
					}
					if ( i < w - 2 ) {
						a_outline = MAX( a_outline, geta(i + 2, j) );
						if ( j > 0 )
							a_outline = MAX( a_outline, geta(i + 2, j - 1) );
						if ( j < h - 1 )
							a_outline = MAX( a_outline, geta(i + 2, j + 1) );
					}
				}
				if ( outline >= 3 ) {
					// Three pixels fake circle
					if ( i > 2 ) {
						a_outline = MAX( a_outline, geta(i - 3, j) );
						if ( j > 0 )
							a_outline = MAX( a_outline, geta(i - 3, j - 1) );
						if ( j < h - 1 )
							a_outline = MAX( a_outline, geta(i - 3, j + 1) );
					}
					if ( i > 1 ) {
						if ( j > 1 )
							a_outline = MAX( a_outline, geta(i - 2, j - 2) );
						if ( j < h - 2 )
							a_outline = MAX( a_outline, geta(i - 2, j + 2) );
					}
					if ( i > 0 ) {
						if ( j > 2 )
							a_outline = MAX( a_outline, geta(i - 1, j - 3) );
						if ( j < h - 3 )
							a_outline = MAX( a_outline, geta(i - 1, j + 3) );
					}
					if ( j > 2 )
						a_outline = MAX( a_outline, geta(i, j - 3) );
					if ( j < h - 3 )
						a_outline = MAX( a_outline, geta(i, j + 3) );
					if ( i < w - 1 ) {
						if ( j > 2 )
							a_outline = MAX( a_outline, geta(i + 1, j - 3) );
						if ( j < h - 3 )
							a_outline = MAX( a_outline, geta(i + 1, j + 3) );
					}
					if ( i < w - 2 ) {
						if ( j > 1 )
							a_outline = MAX( a_outline, geta(i + 2, j - 2) );
						if ( j < h - 2 )
							a_outline = MAX( a_outline, geta(i + 2, j + 2) );
					}
					if ( i < w - 3 ) {
						a_outline = MAX( a_outline, geta(i + 3, j) );
						if ( j > 0 )
							a_outline = MAX( a_outline, geta(i + 3, j - 1) );
						if ( j < h - 1 )
							a_outline = MAX( a_outline, geta(i + 3, j + 1) );
					}
				}
			}

			float a_text = ( float ) bitmap.buffer[ j * bitmap.pitch + i ] / 255.0;
			
			*d++ = ( int ) ( a_text * fg.r + ( 1 - a_text ) * ( a_outline * outlinecolor.r + ( 1 - a_outline ) * bg.r ) );
			*d++ = ( int ) ( a_text * fg.g + ( 1 - a_text ) * ( a_outline * outlinecolor.g + ( 1 - a_outline ) * bg.g ) );
			*d++ = ( int ) ( a_text * fg.b + ( 1 - a_text ) * ( a_outline * outlinecolor.b + ( 1 - a_outline ) * bg.b ) );
			*d++ = ( int ) ( a_text * fg.a + ( 1 - a_text ) * ( a_outline * outlinecolor.a + ( 1 - a_outline ) * bg.a ) );
		}
		dest += stride;
	}

	free( bitmap.buffer );
}

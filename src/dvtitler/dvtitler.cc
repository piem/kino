/*
 * Copyright (C) 2001-2005 Alejandro Aguilar Sierra <asierra@servidor.unam.mx>
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <iostream>

#include <glib/gi18n.h>

#include "dvtitler.h"
#include "textblock.h"
#include "superimpose.h"
#include "../kino_extra.h"
#include "../frame.h"
#include "../playlist.h"
#include "../stringutils.h"
#include "../smiltime.h"
#include "../kino_common.h"

DVTitler::DVTitler()
{
	glade = glade_xml_new( DATADIR "/" PACKAGE "/dvtitler.glade", NULL, NULL );

	window = glade_xml_get_widget( glade, "dvtitler" );
	text = 0;
	titler = 0;
	pixbuf = 0;
	pih = pfh = piv = pfv = 0;
	interlaced = true;
	isMarkup = false;
	GtkColorButton *colorFG = GTK_COLOR_BUTTON( glade_xml_get_widget( glade, "colorpickerfg" ) );
	g_signal_connect( G_OBJECT( colorFG ), "color-set", G_CALLBACK( Repaint ), 0 );
	GtkColorButton *colorBG = GTK_COLOR_BUTTON( glade_xml_get_widget( glade, "colorpickerbg" ) );
	g_signal_connect( G_OBJECT( colorBG ), "color-set", G_CALLBACK( Repaint ), 0 );
	GtkColorButton *colorOutline = GTK_COLOR_BUTTON( glade_xml_get_widget( glade, "colorpickeroutline" ) );
	g_signal_connect( G_OBJECT( colorOutline ), "color-set", G_CALLBACK( Repaint ), 0 );

	GdkColor color = {0, 0, 0};
	gtk_color_button_set_color( colorBG, &color );
	gtk_color_button_set_alpha( colorBG, 0 );
	gtk_color_button_set_color( colorOutline, &color );
	gtk_color_button_set_alpha( colorOutline, 0xffff );
	color.red = 0xffff; color.green = 0xffff; color.blue = 0xffff;
	gtk_color_button_set_color( colorFG, &color );
	gtk_color_button_set_alpha( colorFG, 0xffff );

	GtkTextView *titleEntry = GTK_TEXT_VIEW( glade_xml_get_widget( glade, "text" ) );
	GtkTextBuffer* buffer = gtk_text_view_get_buffer(titleEntry);
	g_signal_connect( G_OBJECT( buffer ), "changed", G_CALLBACK( Repaint ), 0 );
	GtkWidget* widget = glade_xml_get_widget( glade, "fontpicker" );
	gtk_font_button_set_font_name( GTK_FONT_BUTTON( widget ), "Sans Bold 32" );
	g_signal_connect( G_OBJECT( widget ), "font-set", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinpadw" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinpadh" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinoutline" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "alignmenu" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinfadein" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinfadeout" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinxoff" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "spinyoff" );
	g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "optionmenuih" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "optionmenuiv" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "optionmenufh" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "optionmenufv" );
	g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	widget = glade_xml_get_widget( glade, "checkbutton_markup" );
	g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), 0 );
}

DVTitler::~DVTitler()
{
	if ( pixbuf != 0 )
		g_object_unref( pixbuf );
	gtk_widget_destroy( window );
	delete titler;
}

char *DVTitler::GetDescription( ) const
{
	return _("Titler");
}

void DVTitler::AttachWidgets( GtkBin *bin )
{
	gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
}

void DVTitler::DetachWidgets( GtkBin *bin )
{
	gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
}

void DVTitler::InterpretWidgets( GtkBin *bin )
{
	bool isPreviewing = GetSelectedFramesForFX().IsPreviewing();
	if ( isPreviewing )
		gdk_threads_enter();
	GtkMenu *ihMenu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( glade_xml_get_widget( glade, "optionmenuih" ) ) ) );
	GtkMenu *ivMenu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( glade_xml_get_widget( glade, "optionmenuiv" ) ) ) );
	GtkMenu *fhMenu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( glade_xml_get_widget( glade, "optionmenufh" ) ) ) );
	GtkMenu *fvMenu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( glade_xml_get_widget( glade, "optionmenufv" ) ) ) );

	GtkTextView *titleEntry = GTK_TEXT_VIEW( glade_xml_get_widget( glade, "text" ) );
	GtkColorButton *colorFG = GTK_COLOR_BUTTON( glade_xml_get_widget( glade, "colorpickerfg" ) );
	GtkColorButton *colorBG = GTK_COLOR_BUTTON( glade_xml_get_widget( glade, "colorpickerbg" ) );
	GtkColorButton *colorOutline = GTK_COLOR_BUTTON( glade_xml_get_widget( glade, "colorpickeroutline" ) );

	outline = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinoutline" ) ) );


	// Initial horizontal position
	GtkWidget *active_item = gtk_menu_get_active( ihMenu );
	ih = g_list_index ( GTK_MENU_SHELL ( ihMenu ) ->children, active_item ) ;

	// Initial vertical position
	active_item = gtk_menu_get_active( ivMenu );
	iv = g_list_index ( GTK_MENU_SHELL ( ivMenu ) ->children, active_item ) ;

	// Final horizontal position
	active_item = gtk_menu_get_active( fhMenu );
	fh = g_list_index ( GTK_MENU_SHELL ( fhMenu ) ->children, active_item ) ;

	// Final vertical position
	active_item = gtk_menu_get_active( fvMenu );
	fv = g_list_index ( GTK_MENU_SHELL ( fvMenu ) ->children, active_item ) ;

	if ( fh > 4 ) // no change
		fh = ih;

	if ( fv > 4 ) // no change
		fv = iv;

	padw = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinpadw" ) ) );
	padh = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinpadh" ) ) );
	int space = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinspace" ) ) );

	fadein  = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinfadein" ) ) );
	fadeout = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinfadeout" ) ) );

	GtkMenu *alignMenu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( glade_xml_get_widget( glade, "alignmenu" ) ) ) );
	active_item = gtk_menu_get_active( alignMenu );
	int align = g_list_index ( GTK_MENU_SHELL ( alignMenu ) ->children, active_item ) ;

	GtkFontButton *fontpicker = GTK_FONT_BUTTON( glade_xml_get_widget( glade, "fontpicker" ) );
	const char *fontname = gtk_font_button_get_font_name( fontpicker );

	xoff = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinxoff" ) ) );
	yoff = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( glade_xml_get_widget( glade, "spinyoff" ) ) );

	// Obtain the text
	GtkTextIter start;
	GtkTextIter end;
	GtkTextBuffer* buffer = gtk_text_view_get_buffer( titleEntry );
	gtk_text_buffer_get_iter_at_offset( buffer, &start, 0 );
	gtk_text_buffer_get_iter_at_offset( buffer, &end, -1 );
	g_free( text );
	text = gtk_text_buffer_get_text( buffer, &start, &end, FALSE );
	if ( isPreviewing )
		gdk_threads_leave();
	if ( isTextDynamic() )
	{
		char* oldText = text;
		std::string newText( text );
		Frame& frame = *( GetFramePool()->GetFrame() );
		int frameNumber = GetSelectedFramesForFX().GetFrameNumber( position );
		GetCurrentPlayList().GetFrame( frameNumber, frame );

		if (  strstr( text, "#dv.datetime#" ) || strstr( text, "#dv.date#" ) || strstr( text, "#dv.time#" ) )
		{
			std::string dateTime = frame.GetRecordingDate();
			newText = StringUtils::replaceAll( newText, "#dv.datetime#", dateTime );
			vector< std::string > dateTimeParts;
			StringUtils::split( dateTime, " ", dateTimeParts );
			vector< std::string >::iterator dateTimeIter = dateTimeParts.begin();
			newText = StringUtils::replaceAll( newText, "#dv.date#", *dateTimeIter++ );
			newText = StringUtils::replaceAll( newText, "#dv.time#", *dateTimeIter );
		}
		if ( strstr( text, "#dv.timecode#" ) )
		{
			TimeCode tc;
			frame.GetTimeCode( tc );
			char tcString[12];
			snprintf( tcString, 12, "%2.2d:%2.2d:%2.2d:%2.2d", tc.hour, tc.min, tc.sec, tc.frame );
			newText = StringUtils::replaceAll( newText, "#dv.timecode#", tcString );
		}
		if ( strstr( text, "#timecode#" ) )
		{
			SMIL::MediaClippingTime time( frame.GetFrameRate() );
			newText = StringUtils::replaceAll( newText, "#timecode#",
				time.parseFramesToString( frameNumber, common->getTimeFormat() ) );
		}
		if ( strstr( text, "#dv.filename#" ) )
		{
			
			char* filename = GetCurrentPlayList().GetFileNameOfFrame( frameNumber );
			char* basename = g_path_get_basename( filename );
			newText = StringUtils::replaceAll( newText, "#dv.filename#", basename );
			free( filename );
		}
		if ( strstr( text, "#filename#" ) )
		{
			newText = StringUtils::replaceAll( newText, "#filename#",
				GetCurrentPlayList().GetDocName() );
		}
		std::string s( "#meta." );
		while ( newText.find( s ) != string::npos &&
		        newText.find( "#", newText.find( s ) + s.length() ) != string::npos )
		{
			string::size_type metaPos = newText.find( s ) + s.length();
			string::size_type namePos = newText.find( "#", metaPos );
			std::string name = newText.substr( metaPos, namePos - metaPos );
			std::string value = GetCurrentPlayList().GetSeqAttribute( frameNumber, name.c_str() );
			newText = StringUtils::replaceAll( newText, s + name + std::string("#"), value );
		}
		GetFramePool()->DoneWithFrame( &frame );
		text = strdup( newText.c_str() );
		g_free( oldText );
	}

	if ( isPreviewing )
		gdk_threads_enter();
	interlaced = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( glade_xml_get_widget( glade, "checkbutton_interlaced" ) ) );
	isMarkup = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( glade_xml_get_widget( glade, "checkbutton_markup" ) ) );

	if ( !titler )
		titler = new TextBlock();
	titler->setFont( fontname );
	titler->setPad( padw, padh );
	titler->setSpace( space );
	titler->setAlign( align );
	titler->setText( text );

	// Obtain the color
	GdkColor color;
	gtk_color_button_get_color( colorFG, &color );
	colorfg.r = color.red >> 8; colorfg.g = color.green >> 8; colorfg.b = color.blue >> 8;
	colorfg.a = gtk_color_button_get_alpha( colorFG ) >> 8;
	gtk_color_button_get_color( colorBG, &color );
	colorbg.r = color.red >> 8; colorbg.g = color.green >> 8; colorbg.b = color.blue >> 8;
	colorbg.a = gtk_color_button_get_alpha( colorBG ) >> 8;
	gtk_color_button_get_color( colorOutline, &color );
	coloroutline.r = color.red >> 8; coloroutline.g = color.green >> 8; coloroutline.b = color.blue >> 8;
	coloroutline.a = gtk_color_button_get_alpha( colorOutline ) >> 8;
	if ( isPreviewing )
		gdk_threads_leave();
}

bool DVTitler::isTextDynamic()
{
	GtkTextIter start;
	GtkTextIter end;
	GtkTextView *titleEntry = GTK_TEXT_VIEW( glade_xml_get_widget( glade, "text" ) );
	bool isPreviewing = GetSelectedFramesForFX().IsPreviewing();
	if ( isPreviewing )
		gdk_threads_enter();
	GtkTextBuffer* buffer = gtk_text_view_get_buffer( titleEntry );
	gtk_text_buffer_get_iter_at_offset( buffer, &start, 0 );
	gtk_text_buffer_get_iter_at_offset( buffer, &end, -1 );
	char* text = gtk_text_buffer_get_text( buffer, &start, &end, FALSE );
	bool result = strstr( text, "#dv.date" )
		|| strstr( text, "#dv.time" )
		|| strstr( text, "#timecode#" )
		|| strstr( text, "filename#" )
		|| strstr( text, "#meta." )
		;
	g_free( text );
	if ( isPreviewing )
		gdk_threads_leave();
	return result;
}

void DVTitler::FilterFrame( uint8_t *io, int width, int height, double position, double frame_delta )
{
	if ( !text || strcmp( text, "") == 0 )
		return;
	this->position = position;
	if ( position == 0 || GetSelectedFramesForFX().IsRepainting() || isTextDynamic() )
	{
		InterpretWidgets( NULL );

		if ( pixbuf != 0 )
			g_object_unref( pixbuf );

		pixbuf = titler->getPixbuf( colorfg, colorbg, outline, coloroutline, isMarkup );
		if ( pixbuf == 0 )
		{
			titler->setText( _("Rendering failed.\nAre you using bad markup?") );
			pixbuf = titler->getPixbuf( colorfg, colorbg, outline, coloroutline, false );
		}

		int w = gdk_pixbuf_get_width ( pixbuf );
		int h = gdk_pixbuf_get_height ( pixbuf );
		GdkInterpType interp = GDK_INTERP_HYPER;

		// scale to preview size
		if ( width < 720 )
		{
			w /= 4;
			h /= 4;
			xoff /= 4;
			yoff /= 4;
			interp = GDK_INTERP_BILINEAR;
		}
		// scale to adjust for NTSC sample aspect ratio
		if ( height < 576 )
		{
			GdkPixbuf * temp = pixbuf;
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( pixbuf,
				gint( float( w ) * 720.0/640.0 ), h, interp );
			pixbuf = scaled;
			g_object_unref( temp );
		}
		// scale to adjust for PAL sample aspect ratio
		else
		{
			GdkPixbuf * temp = pixbuf;
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( pixbuf,
				gint( float( w ) * 720.0/768.0 ), h, interp );
			pixbuf = scaled;
			g_object_unref( temp );
		}
		
		w = gdk_pixbuf_get_width ( pixbuf );
		h = gdk_pixbuf_get_height ( pixbuf );

		// if Left Off -title width, else if Right Off frame width,
		// else (Left = 0, Center = 1, Right = 2) * difference / 2
		// pih = initial horizontal position
		// pfh = final horizontal position
		pih = ( ih == 3 ) ? -w : ( ih > 3 ) ? width : ( float ) ih * ( width - w ) / 2.0;
		pfh = ( fh == 3 ) ? -w : ( fh > 3 ) ? width : ( float ) fh * ( width - w ) / 2.0;

		// if Top Off -title height, else if Bottom Off frame height, else
		// else (Top = 0, Middle = 1, Bottom = 2) * difference / 2
		// piv = initial vertical position
		// pfv = final vertical position
		piv = ( iv == 3 ) ? -h : ( iv > 2 ) ? height : ( float ) iv * ( height - h ) / 2.0;
		pfv = ( fv == 3 ) ? -h : ( fv > 2 ) ? height : ( float ) fv * ( height - h ) / 2.0;

		// store the width and height for future use in other methods
		frame_w = width;
		frame_h = height;
	}

	if ( pixbuf )
	{
		for ( int field = 0; field < ( interlaced ? 2 : 1 ); ++ field )
		{
			// Offset the position based on which field we're looking at ...
			const double field_position = position + ( 1 - field ) * frame_delta * 0.5;
				
			// apply the progress factor, position, to the path as an offset to the
			// initial position plus apply user-supplied offsets
			int x = ( int ) ( field_position * ( pfh - pih ) + pih ) + xoff;
			int y = ( int ) ( field_position * ( pfv - piv ) + piv ) + yoff;

			//std::cerr << "drawing title on field " << field << " at " << x << ", " << y << std::endl;
			drawPixbuf( io, // destination buffer
                                    x, y,
                                    width * 3, // destination buffer stride
                                    ( 1 - field ),
                                    position, frame_delta );
		}
	}
}


/** Draw the pixbuf from the titler into the frame RGB buffer

    \param io Pointer to RGB buffer
    \param x  Horizontal position
    \param y  Vertical position
    \param frame_stride The number of bytes in a single row of the frame RGB buffer
    \param field 0 for the upper field or progressive frame, 1 for the lower field
*/
void DVTitler::drawPixbuf( uint8_t *io, int x, int y, int frame_stride, int field, double position, double frame_delta )
{
	if ( 3 * x > frame_stride )
		return ;

	int xb = 0, yb = 0, title_w, title_h, title_stride;

	title_w = gdk_pixbuf_get_width ( pixbuf );
	title_h = gdk_pixbuf_get_height ( pixbuf );
	title_stride = gdk_pixbuf_get_rowstride( pixbuf );

	// optimization point - no work to do
	if ( ( x < 0 && -x >= title_w ) || ( y < 0 && -y >= title_h ) )
		return;

	// crop overlay off the left edge of frame
	if ( x < 0 )
	{
		xb = -x;
		title_w -= xb;
		x = 0;
	}
	// crop overlay beyond right edge of frame
	if ( x + title_w > frame_w )
		title_w = frame_w - x;

	// crop overlay off the top edge of the frame
	if ( y < 0 )
	{
		yb = -y;
		title_h -= yb;
		y = 0;
	}
	// crop overlay below bottom edge of frame
	if ( y + title_h > frame_h )
		title_h = frame_h - y;

	// initialise pointer into overlay buffer based on cropping
	uint8_t *pixels = gdk_pixbuf_get_pixels( pixbuf ) + xb * 4 + yb * title_stride;

	// initialise pointer into frame buffer
	uint8_t *base = io + ( x < 0 ? 0 : x ) * 3 + y * frame_stride;

	// special care is taken to make sure frame buffer offset is aligned to the correct field.
	// field 0 = lower field and y should be odd (y is 0-based).
	// field 1 = upper field and y should be even.
	if ( interlaced && ( ( field == 0 && y % 2 == 0 ) || ( field == 1 && y % 2 != 0 ) ) )
		base += frame_stride;
	
	double fadein_ratio;
	if (fadein > 0) {
		fadein_ratio = CLAMP(position/frame_delta/fadein, 0, 1);
	} else {
		fadein_ratio = 1;
	}

	double fadeout_ratio;
	if (fadeout > 0) {
		fadeout_ratio = CLAMP((1-position-frame_delta)/frame_delta/fadeout, 0, 1);
	} else {
		fadeout_ratio = 1;
	}

	double fade_ratio = MIN(fadein_ratio, fadeout_ratio);

	// now do the compositing only to cropped extents
	for ( int j = 0; j < title_h; j += ( interlaced ? 2 : 1 ) )
	{
		uint8_t *p = base + ( j * frame_stride );
		uint8_t *q = pixels + ( j * title_stride );
		for ( int i = 0; i < title_w; i++ )
		{
			float a = ( float ) *( q + 3 ) * fade_ratio / 255.0;
			*p = ( int ) ( a * ( *q++ ) + ( 1 - a ) * ( *p ) );
			p++;
			*p = ( int ) ( a * ( *q++ ) + ( 1 - a ) * ( *p ) );
			p++;
			*p = ( int ) ( a * ( *q++ ) + ( 1 - a ) * ( *p ) );
			p++;
			q++;
		}
	}
}

extern "C"
{

	GDKImageFilter *GetImageFilter( int index )
	{

		switch ( index )
		{
		case 0:
			return new Superimpose();
		case 1:
			return new DVTitler();
		}

		return NULL;
	}
}


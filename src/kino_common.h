/*
* kino_common.h KINO GUI Common Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _KINO_COMMON_H
#define _KINO_COMMON_H

#include <vector>

#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <gdk/gdkkeysyms.h>
#include <limits.h>
#include <stdlib.h>
#include <stdint.h>

#include "playlist.h"
#include "frame.h"
#include "preferences.h"
#include "filehandler.h"
#include "smiltime.h"


/** Component enumeration. This defines all the main window widgets that can be
    activated and deactivated by a notebook page.
*/

enum component_enum {
    EDIT_MENU = 2,
    SCENE_LIST = 32,
    VIDEO_START_OF_MOVIE = 64,
    VIDEO_START_OF_SCENE = 128,
    VIDEO_REWIND = 256,
    VIDEO_BACK = 512,
    VIDEO_PLAY = 1024,
    VIDEO_PAUSE = 2048,
    VIDEO_STOP = 4096,
    VIDEO_FORWARD = 8192,
    VIDEO_FAST_FORWARD = 16384,
    VIDEO_NEXT_SCENE = 32768,
    VIDEO_END_OF_MOVIE = 65536,
    INFO_FRAME = 131072,
    VIDEO_SHUTTLE = 262144
};


/** Page enumeration
 
    This assigns each page an identifier that can be used as arguments
    for certain methods.
*/
enum page_enum {
    PAGE_EDITOR,
    PAGE_CAPTURE,
    PAGE_TIMELINE,
    PAGE_TRIM,
    PAGE_MAGICK,
    PAGE_EXPORT,
    PAGE_BTTV
};

// Forward references to page classes.

class Page;
class PageEditor;
class PageCapture;
class PageTimeline;
class PageBttv;
class PageExport;
class PageTrim;
class PageMagick;
class PageUndefined;

/**	This class is the main GUI class. As such it is responsible for providing
	the functionality for all GUI actions relating to widgets which are outside
	of an individual notebook page (ie: the menu, scene list, transport 
	controls, command line, frame info and the notebook itself).
 
	It is also responsible for holding the current play list.
 
	NB: I would like to make this class more dynamic, especially in terms of 
	notebook pages. Ideally, a notebook page would become a plug in, with each
	Page object being registered with this class (and a subsequent notebook
	page being added to the GUI). Glade (and libglade) could be used in such a 
	scenario, but it would necessitate a separate glade project per page (I
	think... haven't really looked at how best to do this yet). The advantage
	would be that not only could third party plug-ins be developed, but 
	irrelevant notebook pages could be discarded (ie: if you have no firewire
	capture card, then that page could be excluded). 
*/

class KinoCommon
{
private:
	//		Preferences config;

	// GTK Widgets used in this class
	GtkWidget *widget;
	GtkWidget *edit_menu;
	GtkWidget *view_menu;
	GtkNotebook *notebook;
	GtkEntry *command;
	GtkButton *video_start_movie_button;
	GtkButton *video_start_scene_button;
	GtkButton *video_rewind_button;
	GtkButton *video_back_button;
	GtkButton *video_play_button;
	GtkButton *video_stop_button;
	GtkButton *video_forward_button;
	GtkButton *video_fast_forward_button;
	GtkButton *video_end_scene_button;
	GtkButton *video_end_movie_button;
	GtkRange *video_shuttle;
	GtkStatusbar *statusbar;

	// Playlist object
	PlayList playlist;

	// Currently selected notebook page
	int currentPage;

	// Page objects
	PageEditor *editor;
	PageCapture *capture;
	PageTimeline *timeline;
	PageBttv *bttv;
	PageExport *exportPage;
	PageTrim *trimPage;
	PageMagick *magickPage;
	PageUndefined *undefined;

	// Directory and file information
	char tempFileName[ PATH_MAX + NAME_MAX ];
	char playlistFileName[ PATH_MAX + NAME_MAX ];
	// char playlistName[ 1024 ];

	// Other
	gulong component_state;
	bool is_component_state_changing;
	string last_directory;
	bool showMoreInfo;
	int currentScene;
	SMIL::MediaClippingTime time;
	std::vector< GtkWidget* > recentMenuItems;

public:

    static GtkWindow * getWidgetWindow( GtkWidget *widget );

	KinoCommon( GtkWidget *widget );
	~KinoCommon();

	GtkWidget *getWidget( )
	{
		return this->widget;
	}

	PlayList *getPlayList()
	{
		return & playlist;
	}

	int getCurrentScene()
	{
		return this->currentScene;
	}

	// Common actions applied to all pages
	bool newFile( bool prompt = true, bool isExiting = false );

	// File Load and Save dialogs
	void bulkLoad( int, char *[] );
	std::string importFile( const char *filename );
	void insertFile( );
	void appendFile( );
	void loadFile( );
	bool savePlayListAs( );
	bool savePlayList( );
	void saveFrame( );
	void publishPlayList( );
	void publishFrame( );
	void loadPlayList( char* );

	// Page related functions
	void changePageRequest( int page );
	void setCurrentPage( int page );
	Page *getCurrentPage();
	Page *getPage( int page );
	void activateWidgets();

	// Accessors for each [predefined] page
	PageEditor *getPageEditor();
	PageCapture *getPageCapture();
	PageTimeline *getPageTimeline();
	PageBttv *getPageBttv();
	PageExport *getPageExport();
	PageTrim *getPageTrim();
	PageMagick *getPageMagick();
	PageUndefined *getPageUndefined();

	// Frame handling (vars [spit, spit] are temporary)
	int moveToFrame( );
	int moveToFrame( int );
	int moveByFrames( int );
	void showFrameInfo( int );
	void showFrameMoreInfo( Frame&, FileHandler* );
	int g_currentFrame;
	gboolean hasListChanged;

	// Keyboard and command handling
	void keyboardFeedback( const char *, const char * );

	// Current page related methods
	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char * );
	void selectScene( int );
	void videoStartOfMovie();
	void videoPreviousScene();
	void videoStartOfScene();
	void videoRewind();
	void videoBack(int step = -1);
	void videoPlay();
	void videoForward(int step = 1);
	void videoFastForward();
	void videoNextScene();
	void videoEndOfScene();
	void videoEndOfMovie();
	void videoPause();
	void videoStop();
	void videoShuttle( int );
	void windowMoved();
	void visibilityChanged( gboolean );

	// transport control state management
	void toggleComponents( component_enum, bool );
	component_enum getComponentState();
	void commitComponentState( component_enum = ( component_enum ) 0 );

	// File Dialogs
	char *getFileToOpen( char *title, bool isDVFile, GtkWidget *widget );
	char *getFileToSaveFormat( char *title, GtkWidget *widget, int& format );
	char *getFileToOpen( char *title, bool isDVFile = true )
	{
		return getFileToOpen( title, isDVFile, getWidget() );
	}
	char *getFileToSave( char *title )
	{
		int format = 0;
		return getFileToSaveFormat( title, getWidget(), format );
	}
	char *getFileToSaveFormat( char *title, int& format )
	{
		return getFileToSaveFormat( title, getWidget(), format );
	}

	// misc
	void setPreviewSize( float factor, bool noWarning = false );
	void loadSplash( GtkDrawingArea * );
	void clearPreview( GtkDrawingArea * );
	void setWindowTitle( );
	void saveFrame( int, char * );
	bool loadMediaObject( char *, int );
	void packIt( const char *, const char * );
	void setStatusBar( const char *, ... );
	void setMoreInfo( bool state )
	{
		showMoreInfo = state;
	}
	SMIL::Time::TimeFormat getTimeFormat() const
	{
		return static_cast< SMIL::Time::TimeFormat >( Preferences::getInstance().timeFormat );
	}
	void setTimeFormat( SMIL::Time::TimeFormat format );
	SMIL::MediaClippingTime& getTime()
	{
		return time;
	}
	void setCurrentScene( int frame );
	string getLastDirectory( )
	{
		if ( last_directory == "" )
			last_directory = playlist.GetProjectDirectory( );
		return ( last_directory + "/" );
	}
	void setLastDirectory( const string& value )
	{
		last_directory = value;
	}
	void updateRecentFiles();

	// Exit the application
	bool exitKino( );
	
	// File type checker
	int checkFile( char * );

protected:
	// Start and clean current page
	void start();
	void clean();

	// Set the current file being editted
	void setFileEditted( char * );
	void setCurrentDirectoryFromFile( char * );

	void saveAVI( char * );
	bool loadPlayList( char *, int );
	bool savePlayList( char * );
	void fetchProjectMetadata( const std::string& projectKey );
};

extern "C" {
	// Yucky global reference
	extern KinoCommon *common;
}

#endif

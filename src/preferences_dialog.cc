/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define MAX_1394_PORTS 8

#include "kino_common.h"
#include "page.h"
#include "preferences.h"
#include "jogshuttle.h"
#include "mediactrl.h"
extern "C"
{

#include "callbacks.h"
#include "support.h"
#include "commands.h"
#include "message.h"
#include <libavc1394/avc1394.h>
#include <libavc1394/rom1394.h>

	/* Declare callbacks for the jogshuttle handling */
	static void
	on_checkbutton_jogshuttle_toggled ( GtkToggleButton * togglebutton,
	                             gpointer user_data );
	static void
	on_optionmenu_jogshuttle_firstsecond_button_clicked( GtkButton * button,
	        gpointer user_data );
	static void
	on_optionmenu_jogshuttle_action_clicked( GtkButton * button,
	        gpointer user_data );
	void jogshuttle_input_callback( void * button,
	                                unsigned short first, unsigned short second );


	extern GtkWidget *main_window;

	gint gPrefsSelectedFileFormat;
	gint gPrefsDisplayMode;

	void
	on_preferences_activate ( GtkMenuItem * menuitem,
	                          gpointer user_data )
	{
		Preferences & prefs = Preferences::getInstance();
		gchar s[ 512 ];
		GtkWidget* glade_menu;
		GtkWidget* glade_menuitem;
		GtkWidget* widget;
		int currentNode, itemCount = 0, currentItem = 0;
		rom1394_directory rom1394_dir;
		glade_menu = gtk_menu_new ();
		static raw1394handle_t handle;

		media_ctrl_key *mkeys;
		
		
		/* initialize temp variables, only use temp if Okay clicked and not cancel */
		gPrefsSelectedFileFormat = prefs.fileFormat;
		gPrefsDisplayMode = prefs.displayMode;

		GtkWidget *dialog = lookup_widget( main_window, "preferences_dialog" );
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(main_window));
		widget = lookup_widget( dialog, "radiobutton_quicktime" );
#ifdef HAVE_LIBQUICKTIME
		gtk_widget_show( widget );
		gtk_widget_set_sensitive( widget, 1 );
#endif

		switch ( prefs.fileFormat )
		{
		case AVI_DV1_FORMAT:
			widget = lookup_widget( dialog, "radiobutton_avi1" );
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), 1 );
			break;

		case AVI_DV2_FORMAT:
			widget = lookup_widget( dialog, "radiobutton_avi2" );
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), 1 );
			break;

		case RAW_FORMAT:
			widget = lookup_widget( dialog, "radiobutton_rawdv" );
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), 1 );
			break;

		case QT_FORMAT:
			widget = lookup_widget( dialog, "radiobutton_quicktime" );
			gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), 1 );
			break;

		}

		widget = lookup_widget( dialog, "optionmenu_default_normalisation" );
		gtk_option_menu_set_history( GTK_OPTION_MENU( widget ), prefs.defaultNormalisation );
		widget = lookup_widget( dialog, "optionmenu_default_audio" );
		gtk_option_menu_set_history( GTK_OPTION_MENU( widget ), prefs.defaultAudio );
		widget = lookup_widget( dialog, "optionmenu_default_ratio" );
		gtk_option_menu_set_history( GTK_OPTION_MENU( widget ), prefs.defaultAspect );

		widget = lookup_widget( dialog, "checkbutton_autosplit" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.autoSplit );
		widget = lookup_widget( dialog, "checkbutton_timestamp" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.timeStamp );

#ifndef HAVE_X11_EXTENSIONS_XVLIB_H
		gtk_widget_set_sensitive( lookup_widget( dialog, "radiobutton_XV" ), FALSE );
		gtk_widget_set_sensitive( lookup_widget( dialog, "radiobutton_XX" ), FALSE );
#endif

		switch ( prefs.displayMode )
		{
		case DISPLAY_XX:
			widget = lookup_widget( dialog, "radiobutton_XX" );
			break;
		case DISPLAY_XV:
			widget = lookup_widget( dialog, "radiobutton_XV" );
			break;
		case DISPLAY_GDKRGB:
			widget = lookup_widget( dialog, "radiobutton_GDK" );
			break;
		default:
			widget = lookup_widget( dialog, "radiobutton_GDK" );
			break;
		}
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), TRUE );

		widget = lookup_widget( dialog, "hscale_display_quality" );
		GtkAdjustment *displayQualityValue = gtk_range_get_adjustment( GTK_RANGE( widget ) );
		displayQualityValue->value = prefs.displayQuality;
		gtk_range_set_adjustment( GTK_RANGE( widget ), displayQualityValue );


		widget = lookup_widget( dialog, "entry_file" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.file );
		widget = lookup_widget( dialog, "entry_frames" );
		sprintf( s, "%d", prefs.frames );
		gtk_entry_set_text( GTK_ENTRY( widget ), s );
		widget = lookup_widget( dialog, "entry_every" );
		sprintf( s, "%d", prefs.every );
		gtk_entry_set_text( GTK_ENTRY( widget ), s );
		widget = lookup_widget( dialog, "entry_size" );
		sprintf( s, "%d", prefs.maxFileSize );
		gtk_entry_set_text( GTK_ENTRY( widget ), s );

#ifdef HAVE_IEC61883
		gtk_widget_hide( lookup_widget( dialog, "label114" ) );
		gtk_widget_hide( lookup_widget( dialog, "entry_dvcapture_device" ) );
#else
		widget = lookup_widget( dialog, "entry_dvcapture_device" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.dvCaptureDevice );
#endif
		widget = lookup_widget( dialog, "checkbutton_enable_audio" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.enableAudio );
		widget = lookup_widget( dialog, "entry_audio_device" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.audioDevice );
		widget = lookup_widget( dialog, "checkbutton_preview_capture" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.preview_capture );
		widget = lookup_widget( dialog, "checkbutton_drop_frame" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.dropFrame );
		widget = lookup_widget( dialog, "checkbutton_enable_jogshuttle" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.enableJogShuttle );

		/* JogShuttle mappings */
		mkeys = JogShuttle::getInstance().getKeyset();
		
		/* JogShuttle toggle signal handler */
		widget = lookup_widget( dialog, "checkbutton_enable_jogshuttle" );
		if ( widget )
		{
			g_signal_connect( G_OBJECT( widget ), "toggled", 
				G_CALLBACK( on_checkbutton_jogshuttle_toggled ), NULL );
		}
		
		/* Set callbacks for the optionmenus */
		widget = lookup_widget( dialog, "optionmenu_jogshuttle_first_button" );
		if ( mkeys != NULL ) {

			GtkMenu *menu_new_k1 = GTK_MENU( gtk_menu_new( ) );
			GtkOptionMenu *omenu = GTK_OPTION_MENU( widget );
			GtkWidget *kitem;
			
			if ( mkeys != NULL ) {
			
				int mk = 0;
				while(mkeys[mk].key != 0) {
					kitem = gtk_menu_item_new_with_label ( mkeys[mk].name );
					gtk_widget_show( kitem );
					gtk_menu_append ( menu_new_k1, kitem );
					mk++;
				}
				gtk_menu_set_active( menu_new_k1, 0 );
				gtk_option_menu_set_menu( omenu, GTK_WIDGET( menu_new_k1 ) );
			}
			
			g_signal_connect ( G_OBJECT ( GTK_OPTION_MENU ( widget ) ->menu ),
								 "deactivate",
								 G_CALLBACK ( on_optionmenu_jogshuttle_firstsecond_button_clicked ),
								 NULL );
		}	
		
		widget = lookup_widget( dialog, "optionmenu_jogshuttle_second_button" );
		if ( mkeys != NULL ) {
	
			GtkMenu *menu_new_k2 = GTK_MENU( gtk_menu_new( ) );
			GtkOptionMenu *omenu = GTK_OPTION_MENU( widget );
			GtkWidget *kitem;
			
			kitem = gtk_menu_item_new_with_label ( _( "<none>" ) );
			gtk_widget_show( kitem );
			gtk_menu_append ( menu_new_k2, kitem );
			
			// mkeys = JogShuttle::getInstance().getKeyset();
			int mk = 0;
			if ( mkeys != NULL ) {
				while(mkeys[mk].key != 0) {
					kitem = gtk_menu_item_new_with_label ( mkeys[mk].name );
					gtk_widget_show( kitem );
					gtk_menu_append ( menu_new_k2, kitem );
					mk++;
				}
			}
			gtk_menu_set_active( menu_new_k2, 0 );
			gtk_option_menu_set_menu( omenu, GTK_WIDGET( menu_new_k2 ) );
	
			g_signal_connect ( G_OBJECT ( GTK_OPTION_MENU ( widget ) ->menu ),
								 "deactivate",
								 G_CALLBACK ( on_optionmenu_jogshuttle_firstsecond_button_clicked ),
								 NULL  );
		}
		
		
		/* Setup the callback from the JogShuttle, used to grab jogshuttle keyboard
		   presses */
		JogShuttle::getInstance().registerCallback( static_cast<void *>( widget ),
		        jogshuttle_input_callback );

		widget = lookup_widget( dialog, "optionmenu_jogshuttle_action" );
		if ( mkeys == NULL ) gtk_widget_set_sensitive( GTK_WIDGET( widget ), false );
			
		
		/* Fill the action menu. Get the content from the preferences */
		Preferences &pref = Preferences::getInstance();
		GtkOptionMenu *menu = GTK_OPTION_MENU( widget );
		GtkMenu *menu_new = GTK_MENU( gtk_menu_new( ) );
		string caption;
		GtkWidget *item;
		for ( unsigned int i = 0; i < pref._JogShuttleActions.size() ; i++ )
		{
			caption = pref._JogShuttleActions[ i ]._short_desc;
			if ( _( "<none>" ) != caption )
			{
				caption +=
				    " (" + pref._JogShuttleActions[ i ]._action + ")";
			}
			item = gtk_menu_item_new_with_label ( caption.c_str() );
			gtk_widget_show( item );
			gtk_menu_append ( menu_new, item );
		}
		gtk_menu_set_active( menu_new, 0 );
		gtk_option_menu_set_menu( menu, GTK_WIDGET( menu_new ) );
		
		g_signal_connect ( G_OBJECT ( GTK_OPTION_MENU ( widget ) ->menu ),
		                     "deactivate",
		                     G_CALLBACK ( on_optionmenu_jogshuttle_action_clicked ),
		                     NULL );

		// Make sure the menus match
		widget = lookup_widget( dialog, "optionmenu_jogshuttle_first_button" );
		on_optionmenu_jogshuttle_firstsecond_button_clicked( GTK_BUTTON ( widget ),
		        widget );


		widget = lookup_widget( dialog, "checkbutton_disable_key_repeat" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.disableKeyRepeat );
		widget = lookup_widget( dialog, "checkbutton_dv_clamp_luma" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.dvDecoderClampLuma );
		widget = lookup_widget( dialog, "checkbutton_dv_clamp_chroma" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.dvDecoderClampChroma );
		widget = lookup_widget( dialog, "checkbutton_audio_scrub" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.audioScrub );
		widget = lookup_widget( dialog, "checkbutton_avi_opendml" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.isOpenDML );
		widget = lookup_widget( dialog, "entry_default_dir" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.defaultDirectory );
		widget = lookup_widget( dialog, "optionmenu_extract" );
		gtk_option_menu_set_history( GTK_OPTION_MENU( widget ), prefs.displayExtract );
		widget = lookup_widget( dialog, "checkbutton_relative" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.relativeSave );

		widget = lookup_widget( dialog, "optionmenu_avc_phyid" );

#ifdef HAVE_DV1394
		gtk_widget_hide_all( lookup_widget( dialog, "hbox_ieee1394_warning" ) );
#endif
		if ( ( handle = raw1394_new_handle() ) )
		{
			raw1394_portinfo ports[MAX_1394_PORTS];
			int totalPorts = raw1394_get_port_info( handle, ports, MAX_1394_PORTS );
			
			gtk_widget_hide_all( lookup_widget( dialog, "hbox_ieee1394_warning" ) );
			raw1394_destroy_handle( handle );
			handle = NULL;

			for (int port = 0; port < totalPorts; port++ )
			{
				if ( ( handle = raw1394_new_handle_on_port( port ) ) )
				{
					for ( currentNode = 0; currentNode < raw1394_get_nodecount( handle ); currentNode++ )
					{
						if ( rom1394_get_directory( handle, currentNode, &rom1394_dir ) >= 0 )
						{
							if ( ( rom1394_get_node_type( &rom1394_dir ) == ROM1394_NODE_TYPE_AVC ) &&
								avc1394_check_subunit_type( handle, currentNode, AVC1394_SUBUNIT_TYPE_VCR ) )
							{
								octlet_t currentGUID = rom1394_get_guid( handle, currentNode );
								char currentGUIDStr[ 65 ];

								snprintf( currentGUIDStr, 64, "%08x%08x", ( quadlet_t ) ( currentGUID >> 32 ),
									( quadlet_t ) ( currentGUID & 0xffffffff ) );
								currentGUIDStr[64] = 0;
	
								if ( rom1394_dir.label == NULL )
									glade_menuitem = gtk_menu_item_new_with_label ( currentGUIDStr );
								else
									glade_menuitem = gtk_menu_item_new_with_label ( rom1394_dir.label );
	
								gtk_widget_show ( glade_menuitem );
								gtk_menu_append ( GTK_MENU ( glade_menu ), glade_menuitem );
								g_signal_connect ( G_OBJECT ( glade_menuitem ), "activate",
												G_CALLBACK ( on_avc_phyid_activate ),
												( gpointer ) strdup(currentGUIDStr) );
								// TODO: free the duplicated string on re-entrance and destruction
	
								// default the phyID to the first AVC node found
								if ( strcmp( prefs.avcGUID, "" ) == 0 )
								{
									prefs.phyID = currentNode;
									strncpy( prefs.avcGUID, currentGUIDStr, 64 );
									currentItem = itemCount;
								}
								else if ( strncmp( currentGUIDStr, prefs.avcGUID, 64 ) == 0 )
								{
									prefs.phyID = currentNode;
									currentItem = itemCount;
								}
								itemCount++;
							}
							rom1394_free_directory( &rom1394_dir );
						}
					}
					raw1394_destroy_handle( handle );
					handle = NULL;
				}
			}
		}

		gtk_option_menu_set_menu ( GTK_OPTION_MENU ( widget ), glade_menu );
		gtk_option_menu_set_history( GTK_OPTION_MENU( widget ), currentItem );
		widget = lookup_widget( dialog, "checkbutton_v4l" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.enableV4L );
		widget = lookup_widget( dialog, "entry_v4l_device" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.v4lVideoDevice );
		widget = lookup_widget( dialog, "combo_entry_tv_input" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.v4lInput );
		widget = lookup_widget( dialog, "entry_v4l_audio_device" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.v4lAudioDevice );
		widget = lookup_widget( dialog, "combo_entry_sample_rate" );
		gtk_entry_set_text( GTK_ENTRY( widget ), prefs.v4lAudio );

		widget = lookup_widget( dialog, "checkbutton_dv1394_preview" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.dv1394Preview );
		widget = lookup_widget( dialog, "checkbutton_two_pass" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.dvTwoPassEncoder );
		widget = lookup_widget( dialog, "checkbutton_expand_storyboard" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.expandStoryboard );
		widget = lookup_widget( dialog, "checkbutton_trim_mode_insert" );
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), prefs.trimModeInsert );

#ifdef HAVE_LIBAVCODEC
		gtk_widget_hide( lookup_widget( dialog, "vbox_display_decoder" ) );
		gtk_widget_hide( lookup_widget( dialog, "checkbutton_two_pass" ) );
#endif

		gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_HELP, GTK_RESPONSE_OK, GTK_RESPONSE_CANCEL, -1);
		gtk_widget_show( dialog );
	}


	void
	on_avc_phyid_activate( GtkMenuItem * menu_item, gpointer user_data )
	{
		strncpy( Preferences::getInstance().avcGUID, (char *) user_data, 64 );
	}

	void
	on_radiobutton_avi1_clicked ( GtkButton * button,
	                              gpointer user_data )
	{
		gPrefsSelectedFileFormat = AVI_DV1_FORMAT;
	}


	void
	on_radiobutton_avi2_clicked ( GtkButton * button,
	                              gpointer user_data )
	{
		gPrefsSelectedFileFormat = AVI_DV2_FORMAT;
	}

	void
	on_radiobutton_rawdv_clicked ( GtkButton * button,
	                               gpointer user_data )
	{
		gPrefsSelectedFileFormat = RAW_FORMAT;
	}

	void
	on_radiobutton_XX_clicked ( GtkButton * button,
	                            gpointer user_data )
	{
		gPrefsDisplayMode = DISPLAY_XX;
	}

	void
	on_radiobutton_XV_clicked ( GtkButton * button,
	                            gpointer user_data )
	{
		gPrefsDisplayMode = DISPLAY_XV;
	}

	void
	on_radiobutton_GDK_clicked ( GtkButton * button,
	                             gpointer user_data )
	{
		gPrefsDisplayMode = DISPLAY_GDKRGB;
	}

	void
	on_checkbutton_v4l_toggled ( GtkToggleButton * togglebutton,
	                             gpointer user_data )
	{
		GtkWidget *widget = lookup_widget( GTK_WIDGET( togglebutton ), "vbox94" );
		static bool warn = false;
		
		gtk_widget_set_sensitive( widget, gtk_toggle_button_get_active( togglebutton ) );
		if ( warn )
			modal_message_with_parent( widget, _("You must restart Kino for V4L option to take effect.") );
		warn = true;
	}

	static void
	on_checkbutton_jogshuttle_toggled ( GtkToggleButton * togglebutton,
	                             gpointer user_data )
	{
		GtkWidget *widget = lookup_widget( GTK_WIDGET( togglebutton ), "vbox93" );
		Preferences& prefs = Preferences::getInstance();
		
		prefs.enableJogShuttle = gtk_toggle_button_get_active( togglebutton );
		gtk_widget_set_sensitive( widget, prefs.enableJogShuttle );
		if ( prefs.enableJogShuttle )
			JogShuttle::getInstance().start();
		else
			JogShuttle::getInstance().stop();
	}

	/* In this method, change the action as reflected in the option menu */
	static void
	on_optionmenu_jogshuttle_firstsecond_button_clicked( GtkButton * button,
	        gpointer user_data )
	{
		media_ctrl_key *mkeys;
		mkeys = JogShuttle::getInstance().getKeyset();
		
		/* Get the indexes for the button menus */
		GtkOptionMenu * menu = GTK_OPTION_MENU ( lookup_widget( common->getWidget(),
		                                   "optionmenu_jogshuttle_first_button" ) );
		
		// TODO: This is hardcoded to add 0x100 to match the
		// Contour JogShuttle.
		gint active_index_first
		= ( mkeys == NULL ) ? 0 : mkeys[gtk_option_menu_get_history ( menu )].code;

		menu = GTK_OPTION_MENU ( lookup_widget( common->getWidget(),
		                                   "optionmenu_jogshuttle_second_button" ) );
		gint active_index_second
		= gtk_option_menu_get_history ( menu );
		if ( active_index_second != 0 )
		{
			active_index_second = ( mkeys == NULL ) ? 0 : mkeys[active_index_second-1].code;; /* -1 == <none> */
		}

		menu = GTK_OPTION_MENU ( lookup_widget( common->getWidget(),
		                                        "optionmenu_jogshuttle_action" ) );

		/* Look up the action */
		Preferences &prefs = Preferences::getInstance();
		string action =
		    prefs._JogShuttleMappings[
		        make_pair( active_index_first,
		                   active_index_second ) ]._action;

		// g_print ( "Action for this combo : %s\n", action.c_str() );
		/* Figure the new index to set */
		gint new_index = 0;
		string desc;
		if ( "" != action )
		{
			for ( unsigned int i = 0;
			        i < prefs._JogShuttleActions.size(); i++ )
			{
				if ( prefs._JogShuttleActions[ i ]._action
				        == action )
				{
					new_index =
					    prefs._JogShuttleActions[ i ]._option_index;
					desc =
					    prefs._JogShuttleActions[ i ]._desc;
					break;
				}

			}
		}

		// g_print ( "Index for this action : %i\n", new_index );
		/* Set the index and description */
		gtk_option_menu_set_history( menu, new_index );
#if 0
		gtk_label_set_text(
		    GTK_LABEL (
		        lookup_widget(
		            GTK_WIDGET ( button ),
		            "label_jogshuttle_action_description"
		        ) ),
		    desc.c_str() );
#endif
	}

	/* This stores the action with the combinations */
	static void
	on_optionmenu_jogshuttle_action_clicked( GtkButton * button,
	        gpointer user_data )
	{
		media_ctrl_key *mkeys;
		mkeys = JogShuttle::getInstance().getKeyset();
		
		//g_print( "In callback for action\n" );
		/* Get the indexes for the button menus */
		GtkOptionMenu * menu
		= GTK_OPTION_MENU ( lookup_widget( common->getWidget(),
		                                   "optionmenu_jogshuttle_first_button" ) );
		// TODO: This is hardcoded to add 0x100 to match the
		// Contour JogShuttle.
		gint active_index_first
		= ( mkeys == NULL ) ? 0 : mkeys[gtk_option_menu_get_history ( menu )].code;
//		gint active_index_first
//		= gtk_option_menu_get_history ( menu )
//		  + 0x100;


		menu = GTK_OPTION_MENU ( lookup_widget( common->getWidget(),
		                                        "optionmenu_jogshuttle_second_button" ) );
		gint active_index_second
		= gtk_option_menu_get_history ( menu );
		if ( active_index_second != 0 )
		{
			active_index_second = ( mkeys == NULL ) ? 0 : mkeys[active_index_second-1].code;; /* -1 == <none> */
		}

/*		gint active_index_second
		= gtk_option_menu_get_history ( menu );
		if ( active_index_second != 0 )
		{
			active_index_second += 0x100 - 1;
		}
*/
		menu = GTK_OPTION_MENU ( lookup_widget( common->getWidget(),
		                                        "optionmenu_jogshuttle_action" ) );
		gint active_index_action
		= gtk_option_menu_get_history ( menu );

		g_print ( "Active indexs: %i & %i, %i\n",
		      active_index_first, active_index_second,
		      active_index_action );

		/* Look up the action from the index */
		Preferences &prefs = Preferences::getInstance();

		/* Figure the new action to set */
		string action
		= prefs._JogShuttleActions[ active_index_action ]._action;
		string desc
		= prefs._JogShuttleActions[ active_index_action ]._desc;
		//g_print ( "Action for this combo : %s\n", action.c_str() );
#if 0
		/* Update label */
		gtk_label_set_text(
		    GTK_LABEL (
		        lookup_widget(
		            GTK_WIDGET ( button ),
		            "label_jogshuttle_action_description"
		        ) ),
		    desc.c_str() );
#endif
		/* Set it */
		prefs._JogShuttleMappings[
		    make_pair( active_index_first,
		               active_index_second ) ] = action;
	}

	/* Set the option menu's, call the button callback, which deals with the
	   action */
	void jogshuttle_input_callback( void * button,
	                                unsigned short first, unsigned short second )
	{
		// g_print( "Setting to %i, %i\n", first, second );
		gtk_option_menu_set_history( GTK_OPTION_MENU (
		                                 lookup_widget(
		                                     GTK_WIDGET ( button ),
		                                     "optionmenu_jogshuttle_first_button" ) ),
		                             first );
		gtk_option_menu_set_history( GTK_OPTION_MENU (
		                                 lookup_widget(
		                                     GTK_WIDGET ( button ),
		                                     "optionmenu_jogshuttle_second_button" ) ),
		                             second );
		// g_print( "Calling firstsecond\n" );
		on_optionmenu_jogshuttle_firstsecond_button_clicked(
		    GTK_BUTTON( ( button ) ),
		    NULL );
		// g_print( "Done calling firstsecond\n" );
	}

	/* Make sure we always deregister */
	gboolean
	on_preferences_dialog_destroy_event ( GtkWidget * widget,
	                                      GdkEvent * event,
	                                      gpointer user_data )
	{
		/* Remove the input callback */
		// g_print( "In on preferences_dialog_destroy_event\n" );
		JogShuttle::getInstance().deregisterCallback();
		gtk_widget_hide_on_delete( widget );
		return TRUE;
	}



	void
	on_preferences_dialog_ok_button_clicked( GtkButton * button, gpointer user_data )
	{
		Preferences & prefs = Preferences::getInstance();
		GtkWidget *dialog = gtk_widget_get_toplevel( GTK_WIDGET( button ) );
		GtkWidget *widget = lookup_widget( dialog, "checkbutton_autosplit" );

		prefs.autoSplit = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );

#ifdef HAVE_LIBQUICKTIME
		widget = lookup_widget( dialog, "radiobutton_quicktime" );
		if ( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) ) )
			gPrefsSelectedFileFormat = QT_FORMAT;
#endif

		widget = gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( dialog, "optionmenu_default_normalisation" ) ) );
		GtkWidget *active_item = gtk_menu_get_active( GTK_MENU( widget ) );
		prefs.defaultNormalisation = g_list_index ( GTK_MENU_SHELL ( widget ) ->children, active_item );

		widget = gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( dialog, "optionmenu_default_audio" ) ) );
		active_item = gtk_menu_get_active( GTK_MENU( widget ) );
		prefs.defaultAudio = g_list_index ( GTK_MENU_SHELL ( widget ) ->children, active_item );

		widget = gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( dialog, "optionmenu_default_ratio" ) ) );
		active_item = gtk_menu_get_active( GTK_MENU( widget ) );
		prefs.defaultAspect = g_list_index ( GTK_MENU_SHELL ( widget ) ->children, active_item );

		widget = lookup_widget( dialog, "checkbutton_timestamp" );
		prefs.timeStamp = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "entry_file" );
		strcpy( prefs.file, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		widget = lookup_widget( dialog, "entry_frames" );
		prefs.frames = atoi( gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		if ( prefs.frames < 0 )
			prefs.frames = 7000;
		widget = lookup_widget( dialog, "entry_every" );
		prefs.every = atoi( gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		if ( prefs.every <= 0 )
			prefs.every = 1;
		widget = lookup_widget( dialog, "entry_size" );
		prefs.maxFileSize = atoi( gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		if ( prefs.maxFileSize < 0 )
			prefs.maxFileSize = 2000;
#ifndef HAVE_LIBAVCODEC
		GtkAdjustment *displayQualityValue;
		widget = lookup_widget( dialog, "hscale_display_quality" );
		displayQualityValue = gtk_range_get_adjustment( GTK_RANGE( widget ) );
		if ( int( gtk_adjustment_get_value( displayQualityValue ) ) != prefs.displayQuality )
		{
			prefs.displayQuality = int( gtk_adjustment_get_value( displayQualityValue ) );
			common->moveToFrame();
		}
#endif
#ifdef HAVE_DV1394
		widget = lookup_widget( dialog, "entry_dvcapture_device" );
		strcpy( prefs.dvCaptureDevice, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		strcpy( prefs.dvExportDevice, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
#endif
		widget = lookup_widget( dialog, "checkbutton_enable_audio" );
		prefs.enableAudio = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "entry_audio_device" );
		strcpy( prefs.audioDevice, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		widget = lookup_widget( dialog, "checkbutton_preview_capture" );
		prefs.preview_capture = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_drop_frame" );
		prefs.dropFrame = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_enable_jogshuttle" );
		prefs.enableJogShuttle = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );

		/* Deregister callback */
		JogShuttle::getInstance().deregisterCallback();

		widget = lookup_widget( dialog, "checkbutton_disable_key_repeat" );
		prefs.disableKeyRepeat = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_dv_clamp_luma" );
		prefs.dvDecoderClampLuma = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_dv_clamp_chroma" );
		prefs.dvDecoderClampChroma = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_audio_scrub" );
		prefs.audioScrub = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_v4l" );
		prefs.enableV4L = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "entry_v4l_device" );
		strcpy( prefs.v4lVideoDevice, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		widget = lookup_widget( dialog, "combo_entry_tv_input" );
		strcpy( prefs.v4lInput, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		widget = lookup_widget( dialog, "entry_v4l_audio_device" );
		strcpy( prefs.v4lAudioDevice, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		widget = lookup_widget( dialog, "combo_entry_sample_rate" );
		strcpy( prefs.v4lAudio, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		widget = lookup_widget( dialog, "checkbutton_avi_opendml" );
		prefs.isOpenDML = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( dialog, "optionmenu_extract" ) ) );
		active_item = gtk_menu_get_active( GTK_MENU( widget ) );
		prefs.displayExtract = g_list_index( GTK_MENU_SHELL( widget )->children, active_item );
		widget = lookup_widget( dialog, "checkbutton_relative" );
		prefs.relativeSave = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );

		widget = lookup_widget( dialog, "entry_default_dir" );
		strcpy( prefs.defaultDirectory, gtk_entry_get_text( GTK_ENTRY( widget ) ) );
		if ( prefs.defaultDirectory[ strlen( prefs.defaultDirectory ) - 1 ] != '/' )
			strcat( prefs.defaultDirectory, "/" );

		widget = lookup_widget( dialog, "checkbutton_dv1394_preview" );
		prefs.dv1394Preview = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
#ifndef HAVE_LIBAVCODEC
		widget = lookup_widget( dialog, "checkbutton_two_pass" );
		prefs.dvTwoPassEncoder = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
#endif
		widget = lookup_widget( dialog, "checkbutton_expand_storyboard" );
		prefs.expandStoryboard = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		widget = lookup_widget( dialog, "checkbutton_trim_mode_insert" );
		prefs.trimModeInsert = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );

		prefs.fileFormat = gPrefsSelectedFileFormat;
		prefs.displayMode = gPrefsDisplayMode;

		prefs.Save();
		gtk_widget_hide( dialog );

		common->getCurrentPage() ->clean();
		common->activateWidgets();
		common->getCurrentPage() ->start();
	}


	void
	on_preferences_dialog_cancel_button_clicked( GtkButton * button, gpointer user_data )
	{
		/* Remove the input callback */
		JogShuttle::getInstance().deregisterCallback();
		GtkWidget *dialog = gtk_widget_get_toplevel( GTK_WIDGET( button ) );
		gtk_widget_hide( dialog );
	}


	void
	on_button_capture_open_clicked( GtkButton       *button,
	                                gpointer         user_data )
	{
		int dummy = 0;
		char *filename = common->getFileToSaveFormat( _("Choose a DV file"), GTK_WIDGET(button), dummy );
		if ( filename && strcmp( filename, "" ) )
		{
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_file" ) ),
				filename );
		}
	}

}

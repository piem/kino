/*
* page_export_stills.cc Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vector>
#include <iostream>
using std::cerr;
using std::endl;
#include <string>

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "kino_av_pipe.h"
#include "page_export_audio.h"
#include "preferences.h"
#include "kino_common.h"
#include "frame.h"
#include "page_editor.h"
#include "message.h"


/** Constructor for page.

\param common	common object to which this page belongs
*/

ExportAudio::ExportAudio( PageExport *_exportPage, KinoCommon *_common ) :
		Export( _exportPage, _common )
{
	cerr << "> Creating ExportAudio Page" << endl;
	fileEntry = GTK_ENTRY( lookup_widget( common->getWidget(), "entry_export_audio_file" ) );

	sampleRate
	= GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
	                                      "optionmenu_export_audio_sampling" ) ) ) );
	audioFormat
	= GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
	                                      "optionmenu_export_audio_format" ) ) ) );
	splitCheck
	= GTK_TOGGLE_BUTTON( lookup_widget( exportPage->getWidget(),
	                                    "checkbutton_export_audio_split" ) );
}

/** Destructor for page.
 */

ExportAudio::~ExportAudio()
{
	cerr << "> Destroying ExportAudio Page" << endl;
}

/** start exporting audio
 */
enum export_result
ExportAudio::doExport( PlayList * playlist, int begin, int end, int every,
                       bool preview )
{
	cerr << ">>> ExportAudio::doExport" << endl;
	Frame& frame = *GetFramePool()->GetFrame();
	gchar *file = NULL;
	enum export_result status = EXPORT_RESULT_SUCCESS;

	/* Check the filename */
	file = g_strdup( gtk_entry_get_text( fileEntry ) );
	if ( !( strcmp( file, "" ) && strpbrk( file, "\'" ) == NULL ) )
	{
		modal_message( _( "You must enter a filename which does not contain quotes." ) );
		g_free( file );
		return EXPORT_RESULT_FAILURE;
	}


	// Get a sample frame to obtain recording info
	playlist->GetFrame( begin, frame );

	AudioInfo info;
	frame.GetAudioInfo( info );
	short channels = info.channels;
	int outputFrequency = info.frequency;
	bool splitScene = gtk_toggle_button_get_active( splitCheck );

	// Determine requested output frequency
	GtkWidget *active_item = gtk_menu_get_active( sampleRate );
	int menuValue = g_list_index ( GTK_MENU_SHELL ( sampleRate ) ->children,
	                               active_item );
	if ( menuValue == 1 )
		outputFrequency = 32000;
	else if ( menuValue == 2 )
		outputFrequency = 44100;
	else if ( menuValue == 3 )
		outputFrequency = 48000;

	// Determine the output format
	active_item = gtk_menu_get_active( audioFormat );
	menuValue = g_list_index ( GTK_MENU_SHELL ( audioFormat ) ->children, active_item );

	gchar *outputtemplate = NULL;
	kino_audio_pipe type = PIPE_AUDIO_WAV;

	if ( menuValue == 0 )
	{
		outputtemplate = g_strdup_printf( "%s%%s.wav", file );
		type = PIPE_AUDIO_WAV;
	}
	else if ( menuValue == 1 )
	{
		outputtemplate = g_strdup_printf( "| sox -t .raw -r %d -c 2 -w -s - \'%s\'%%s.wav",
		         outputFrequency, file );
		type = PIPE_AUDIO_WAV;
	}
	else if ( menuValue == 2 )
	{
		outputtemplate = g_strdup_printf( "| mp2enc -r %d -o \'%s\'%%s.mp2",
		         outputFrequency, file );
		type = PIPE_AUDIO_WAV;
	}
	else if ( menuValue == 3 )
	{
		outputtemplate = g_strdup_printf( "| lame -r -s %d -x - -o \'%s\'%%s.mp3",
		         outputFrequency / 1000, file );
		type = PIPE_AUDIO_PCM;
	}
	else if ( menuValue == 4 )
	{
		outputtemplate = g_strdup_printf( "| oggenc - -o \'%s\'%%s.ogg", file );
		type = PIPE_AUDIO_WAV;
	}

	// Iterate through each scene (opening and closing audio pipes as requested)
	KinoAudioPipe *outputPipe = KinoAudioFactory::CreateAudioPipe( type );
	int scene = 0;
	for ( int sceneBegin = begin; sceneBegin <= end && exportPage->isExporting;
	        scene ++ )
	{
		gchar *full;
		char sceneString[ 5 ];
		int sceneEnd = end;

		// Determine if we need to split by scene or not
		if ( splitScene )
		{
			sprintf( sceneString, "%03d", scene );
			sceneEnd = playlist->FindEndOfScene( sceneBegin );
			if ( sceneEnd > end )
				sceneEnd = end;
		}
		else
		{
			strcpy( sceneString, "" );
		}

		// expand the template to accomodate the scene splits
		full = g_strdup_printf( outputtemplate, sceneString );

		// output this scene
		bool active = outputPipe->OpenAudio( full, channels, outputFrequency, 2 );
		g_free( full );
		
		if ( active )
		{
			// Call this before hand to initialise the counters
			// calculateAdjustedRate will accumulate time as paused time to prevent
			// distorting the estimate.
			innerLoopUpdate( sceneBegin, begin, end, every );

			// Determine correct amount of audio for duration
			double adjustedRate = calculateAdjustedRate( playlist, outputFrequency, sceneBegin, sceneEnd, every );
			if ( ! adjustedRate )
				status = EXPORT_RESULT_ABORT;

			// Setup a resampler
			AsyncAudioResample<int16_ne_t,int16_le_t>* resampler = new AsyncAudioResample<int16_ne_t,int16_le_t>(
				AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, playlist, outputFrequency, sceneBegin, sceneEnd, every );
			if ( resampler->IsError() )
			{
				std::cerr << ">>> Resampler error: " << resampler->GetError() << std::endl;
				exportPage->isExporting = false;
				status = EXPORT_RESULT_FAILURE;
				break;
			}

			// Now run through the frames in this scene
			for ( int i = sceneBegin, j = 0; i <= sceneEnd && exportPage->isExporting;
			        i += every, j++ )
			{
				innerLoopUpdate( i, begin, end, every );

				int requestedSamples = frame.CalculateNumberSamples( outputFrequency, j );
				int nsamples = resampler->Process( adjustedRate, requestedSamples );
				if ( nsamples > 0 && !outputPipe->OutputAudioFrame( (int16_t *)resampler->GetOutput(), nsamples * channels * sizeof(int16_t) ) )
				{
					modal_message( _( "Error during audio export - aborting." ) );
					status = EXPORT_RESULT_FAILURE;
					exportPage->isExporting = false;
				}
			}
			if ( status == EXPORT_RESULT_SUCCESS && !exportPage->isExporting )
				status = EXPORT_RESULT_ABORT;
			
			// Cleanup
			outputPipe->CloseAudio();
			delete resampler;
			resampler = 0;
		}
		else
		{
			modal_message( _( "Error starting output" ) );
			status = EXPORT_RESULT_FAILURE;
			break;
		}

		// Move to start of next scene
		sceneBegin = sceneEnd + 1;
	}

	g_free( file );
	g_free( outputtemplate );
	delete outputPipe;

	GetFramePool()->DoneWithFrame( &frame );
	
	return status;
}

void ExportAudio::start()
{
	GtkWidget* widget = lookup_widget( common->getWidget(), "menuitem_export_audio_sox" );
	system( "which sox > /dev/null 2> /dev/null" ) ? gtk_widget_hide( widget ) : gtk_widget_show( widget );
	widget = lookup_widget( common->getWidget(), "menuitem_export_audio_mp2enc" );
	system( "which mp2enc > /dev/null 2> /dev/null" ) ? gtk_widget_hide( widget ) : gtk_widget_show( widget );
	widget = lookup_widget( common->getWidget(), "menuitem_export_audio_lame" );
	system( "which lame > /dev/null 2> /dev/null" ) ? gtk_widget_hide( widget ) : gtk_widget_show( widget );
	widget = lookup_widget( common->getWidget(), "menuitem_export_audio_oggenc" );
	system( "which oggenc > /dev/null 2> /dev/null" ) ? gtk_widget_hide( widget ) : gtk_widget_show( widget );
}

extern "C"
{
	void
	on_button_export_audio_file_clicked    (GtkButton       *button,
                                            gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to Save As") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_export_audio_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_export_audio_file" ) ), filename );
	}
}

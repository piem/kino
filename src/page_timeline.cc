/*
* page_timeline.cc Notebook Timeline Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <pthread.h>

#include "page_timeline.h"
#include "page_editor.h"
#include "commands.h"

enum
{
    COLUMN_ICON = 0,
    COLUMN_TIME,
	COLUMN_FRAME,
    N_COLUMNS
};

/*
 * Callbacks
 */

extern "C"
{

	extern navigate_control g_nav_ctl;

	int getOneSecond( void )
	{
		Frame & frame = *( GetFramePool() ->GetFrame( ) );
		common->getPlayList() ->GetFrame( common->g_currentFrame, frame );
		int value = ( frame.IsPAL() ? 25 : 30 );
		GetFramePool( ) ->DoneWithFrame( &frame );
		return value;
	}

	static unsigned char	pixels[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];

	void
	on_iconview_timeline_selection_changed( GtkIconView  *iconview,
	                                        gpointer user_data )
	{
		PageTimeline* page = static_cast< PageTimeline* >( user_data );
		GtkTreeIter iter;
		GValue val = {0, };
		
		GList* items = gtk_icon_view_get_selected_items( page->getView() );
		if ( items )
		{
			gtk_tree_model_get_iter( GTK_TREE_MODEL( page->getModel() ), &iter,
				static_cast< GtkTreePath* >( items->data ) );
			gtk_tree_model_get_value( GTK_TREE_MODEL( page->getModel() ), &iter, COLUMN_FRAME, &val );
			common->moveToFrame( g_value_get_int( &val ) );
			g_value_unset( &val );
			g_list_foreach( items, reinterpret_cast< GFunc >( gtk_tree_path_free ), NULL );
			g_list_free( items );
	
			common->changePageRequest( PAGE_EDITOR );
		}
	}

	void
	on_iconview_timeline_item_activated( GtkIconView     *iconview,
	                                     GtkTreePath     *path,
	                                     gpointer         user_data)
	{
		PageTimeline *page = static_cast< PageTimeline* >( user_data );
		GtkTreeIter iter;
		GValue val = {0, };

		gtk_tree_model_get_iter( GTK_TREE_MODEL( page->getModel() ), &iter, path );
		gtk_tree_model_get_value( GTK_TREE_MODEL( page->getModel() ), &iter, COLUMN_FRAME, &val );
		common->moveToFrame( g_value_get_int( &val ) );
// 	 	std::cerr << "on_iconview_timeline_item_activated " << g_value_get_int( &val ) << std::endl;
		g_value_unset( &val );
 		common->changePageRequest( PAGE_EDITOR );
	}
	
	
	void
	on_timeline_ok_button_pressed ( GtkButton * button,
	                                gpointer user_data )
	{
		common->getPageTimeline( ) ->showIcons( );
	}

	static gboolean on_iconlist_refresh_required( GtkWidget * some_widget, void * some_event, gpointer user_data )
	{
		( ( PageTimeline * ) user_data ) ->refresh( );
		return false;
	}
	
	void
	on_start_spin_value_changed            (GtkSpinButton   *spinbutton,
											gpointer         user_data)
	{
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_timeline_start" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
	}

	void
	on_entry_timeline_start_activate       (GtkEntry        *entry,
											gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "start_spin" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_start_spin_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_timeline_start_focus_out_event
											(GtkWidget       *widget,
											GdkEventFocus   *event,
											gpointer         user_data)
	{
		on_entry_timeline_start_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}

	void
	on_end_spin_value_changed              (GtkSpinButton   *spinbutton,
											gpointer         user_data)
	{
		gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( spinbutton ), "entry_timeline_end" ) ),
			common->getTime().parseFramesToString( ( int )gtk_spin_button_get_value( spinbutton ),
			common->getTimeFormat() ).c_str() );
	}

	void
	on_entry_timeline_end_activate         (GtkEntry        *entry,
											gpointer         user_data)
	{
		common->getTime().parseValueToString( gtk_entry_get_text( entry ), common->getTimeFormat() );
		GtkSpinButton *spinbutton = GTK_SPIN_BUTTON( lookup_widget( GTK_WIDGET( entry ), "end_spin" ) );
		gtk_spin_button_set_value( spinbutton, common->getTime().getFrames() );
		on_end_spin_value_changed( spinbutton, NULL );
	}
	
	gboolean
	on_entry_timeline_end_focus_out_event  (GtkWidget       *widget,
											GdkEventFocus   *event,
											gpointer         user_data)
	{
		on_entry_timeline_end_activate( GTK_ENTRY( widget ), NULL );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}
}

PageTimeline::PageTimeline( KinoCommon *common ) :
		refresh_required( false ), action( 0 ),
		last_begin( 0 ), last_end( 0 ), scene( -1 ),
		showIconsRunning( TIMELINE_INIT )
{
	this->common = common;

	view = GTK_ICON_VIEW( lookup_widget( common->getWidget(), "iconview_timeline" ) );
	g_signal_connect( G_OBJECT( view ), "expose_event", G_CALLBACK( on_iconlist_refresh_required ), this );
 	g_signal_connect( G_OBJECT( view ), "item-activated", G_CALLBACK( on_iconview_timeline_item_activated ), this );
	g_signal_connect( G_OBJECT( view ), "selection-changed", G_CALLBACK( on_iconview_timeline_selection_changed ), this );
	model = gtk_list_store_new( N_COLUMNS, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_INT );
	gtk_icon_view_set_model( view, GTK_TREE_MODEL( model ) );
	gtk_icon_view_set_pixbuf_column( view, COLUMN_ICON );
	gtk_icon_view_set_text_column( view, COLUMN_TIME );
	pthread_mutex_init( &key, NULL );
	pthread_mutex_init( &mutex, NULL );
}

void PageTimeline::start()
{
	std::cerr << ">> Starting timeline" << std::endl;

	if ( common->getPlayList() ->GetNumFrames() > 0 )
	{
		int begin = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
		int end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );

		if ( last_begin != begin || last_end != end )
		{
			GtkEntry *startSpin = GTK_ENTRY( lookup_widget( common->getWidget(), "start_spin" ) );
			GtkEntry *endSpin = GTK_ENTRY( lookup_widget( common->getWidget(), "end_spin" ) );
			GtkAdjustment *adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( startSpin ) );

			adjust->lower = 0;
			adjust->upper = common->getPlayList() ->GetNumFrames();
			gtk_spin_button_set_value( GTK_SPIN_BUTTON( startSpin ), begin );
			g_signal_emit_by_name( adjust, "changed" );

			adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( endSpin ) );
			adjust->lower = 0;
			adjust->upper = common->getPlayList() ->GetNumFrames();
			gtk_spin_button_set_value( GTK_SPIN_BUTTON( endSpin ), end );
			g_signal_emit_by_name( adjust, "changed" );

			last_begin = begin;
			last_end = end;

			scene = 0;
			vector <int> scenes = common->getPageEditor() ->GetScene();
			while ( begin > scenes[ scene++ ] )
				;
			scene--;

			refresh_required = true;
		}
	}
	gtk_notebook_set_page( GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_keyhelp" ) ), 3 );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_timeline" ) ), TRUE );
}

void PageTimeline::selectScene( int i )
{
	std::cerr << "Time line scene " << i << std::endl;
	GtkWidget *start_spin = lookup_widget( common->getWidget(), "start_spin" );
	GtkWidget *end_spin = lookup_widget( common->getWidget(), "end_spin" );
	GtkAdjustment *adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( start_spin ) );
	vector <int> scenes = common->getPageEditor() ->GetScene();

	if ( i < 0 )
		i = 0;
	if ( i >= ( int ) scenes.size() )
		i = scenes.size() - 1;
	int value = i == 0 ? 0 : scenes[ i - 1 ];
	adjust->lower = 0;
	adjust->upper = scenes[ scenes.size() - 1 ];
	gtk_spin_button_set_value( GTK_SPIN_BUTTON( start_spin ), value );

	adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( end_spin ) );

	adjust->lower = 0;
	adjust->upper = scenes[ scenes.size() - 1 ];
	gtk_spin_button_set_value( GTK_SPIN_BUTTON( end_spin ), scenes[ i ] - 1 );

	last_begin = value;
	last_end = scenes[ i ] - 1;
	scene = i;

	common->g_currentFrame = value;

	Frame &frame = *( GetFramePool( ) ->GetFrame( ) );
	FileHandler *media;
	common->getPlayList() ->GetMediaObject( value, &media );
	common->getPlayList() ->GetFrame( value, frame );
	common->showFrameMoreInfo( frame, media );
	GetFramePool( ) ->DoneWithFrame( &frame );
	common->setCurrentScene( value );

	showIcons( );
}

gulong PageTimeline::activate()
{
	return SCENE_LIST;
}

void PageTimeline::showIcons( )
{
	pthread_mutex_lock( &mutex );

	// If running then, set as RESTART otherwise start the thread,
	// otherwise we're already waiting on a restart so do nothing
	if ( showIconsRunning == TIMELINE_RUNNING )
	{
		showIconsRunning = TIMELINE_RESTART;
	}
	else if ( showIconsRunning == TIMELINE_INIT )
	{
		showIconsRunning = TIMELINE_STARTING;
		pthread_create( &thread, NULL, ThreadProxy, this );
	}
	else if ( showIconsRunning == TIMELINE_IDLE )
	{
		pthread_join( thread, NULL );
		showIconsRunning = TIMELINE_STARTING;
		pthread_create( &thread, NULL, ThreadProxy, this );
	}

	pthread_mutex_unlock( &mutex );
}

void* PageTimeline::ThreadProxy( void* arg )
{
	PageTimeline* self = static_cast< PageTimeline* >( arg );
	return self->Thread();
}

void* PageTimeline::Thread()
{
	Frame &frame = *GetFramePool( ) ->GetFrame( );

	// Set the lowest coloured quality for decode
	frame.decoder->quality = DV_QUALITY_DC;
	if ( Preferences::getInstance().displayQuality < 4 )
		frame.decoder->quality |= DV_QUALITY_COLOR;

	while ( showIconsRunning != TIMELINE_IDLE )
	{
		GtkTreeIter iter;

		showIconsRunning = TIMELINE_RUNNING;
		refresh_required = false;
		gdk_threads_enter();
	
		// Increment and store the number of times this is called
		int my_action = ++ action;
	
		// Lock the mutex to ensure only one scene is rendered
		pthread_mutex_lock( &key );
	
		// Obtain the start and end widgets
		GtkWidget *start_spin = lookup_widget( common->getWidget(), "start_spin" );
		GtkWidget *end_spin = lookup_widget( common->getWidget(), "end_spin" );
	
		// Get the start and end frames from the widgets
		int start = atoi( gtk_entry_get_text( GTK_ENTRY( start_spin ) ) );
		int end = atoi( gtk_entry_get_text( GTK_ENTRY( end_spin ) ) );
	
		// Calculate the number of thumbs per line
		int items_per_row = ( GTK_WIDGET( view )->allocation.width - 12 ) / 96;
		int number_of_rows = GTK_WIDGET( view )->allocation.height / 98;
		int total_possible = items_per_row * number_of_rows;
		int total_required = end - start + 1;
	
		// Calculate the increment
		double increment = 1;
		if ( total_required == 1 )
			increment = 2;
		else if ( total_possible < total_required )
			increment = 1 / ( ( double ) total_possible - 1 );
		else
			increment = 1 / ( ( double ) total_required );
	
		// Wipe the previous contents
		gtk_list_store_clear( model );
	
		// Hmm - avoid rounding errors?
		int count = 0;
	
		// Loop for each thumb nail
		for ( double position = 0;
				showIconsRunning == TIMELINE_RUNNING &&
					action == my_action && count ++ < total_possible;
				position += increment )
		{
			// Calculate the index
			int index = start + ( int ) ( position * total_required );
	
			if ( position >= 1 )
				index = start + total_required - 1;
	
			// Define the label text variable
			string text = "";
	
			// Get the frame at the index
			common->getPlayList() ->GetFrame( ( int ) index, frame );
	
			// Extract the RGB image
			frame.ExtractRGB( pixels );
	
			// Create gdk pixbuf image to allow rescaling
			GdkPixbuf *image = gdk_pixbuf_new_from_data( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
							frame.GetWidth(), frame.GetHeight(), frame.GetWidth() * 3, NULL, NULL );
	
			// Rescale
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( image, 90, frame.IsWide() ? 51 : 68, GDK_INTERP_NEAREST );
	
			// Set the label
			char label[ 256 ] = "";
			sprintf( label, "%d", ( int ) index + 1 );
			common->getTime().setFramerate( frame.GetFrameRate() );
			text = common->getTime().parseFramesToString( index, common->getTimeFormat() );
	
			// Place image in icon list
			gtk_list_store_append( model, &iter );
			gtk_list_store_set( model, &iter, COLUMN_ICON, scaled, 
				COLUMN_TIME, text.c_str(), COLUMN_FRAME, index, -1 );
	
			// Delete the original, unscaled pixbuf
			g_object_unref( image );
			g_object_unref( scaled );
	
			// Process any pending events
			while ( action == my_action && gtk_events_pending() )
			{
				pthread_mutex_unlock( &key );
				gtk_main_iteration();
				pthread_mutex_lock( &key );
			}
	
			if ( position >= 1 )
				break;
		}
	
		// Unlock the mutex
 		pthread_mutex_unlock( &key );
		gdk_threads_leave();
	
		// If we're still running now, it's OK to idle again.
		if ( showIconsRunning == TIMELINE_RUNNING )
			showIconsRunning = TIMELINE_IDLE;
	}
	GetFramePool( ) ->DoneWithFrame( &frame );

	return NULL;
}

void PageTimeline::refresh( )
{
	if ( refresh_required )
	{
		refresh_required = false;
		showIcons( );
	}
}

gboolean PageTimeline::processKeyboard( GdkEventKey *event )
{
	gboolean ret = FALSE;

	// Translate special keys to equivalent command
	switch ( event->keyval )
	{
	case GDK_k:
	case GDK_Up:
		selectScene( scene - 1 );
		break;
	case GDK_j:
	case GDK_Down:
		selectScene( scene + 1 );
		break;
	case GDK_Return:
		selectScene( scene );
		break;
	case GDK_Escape:
		common->changePageRequest( PAGE_EDITOR );
		break;
	default:
		break;
	}

	return ret;
}

gboolean PageTimeline::processCommand( const char *cmd )
{
	if ( strcmp( cmd, "F2" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Edit" ) );
		common->changePageRequest( PAGE_TIMELINE );
	}

	else if ( strcmp( cmd, "A" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, append to movie" ) );
		common->moveToFrame( common->getPlayList() ->GetNumFrames() );
		FileTracker::GetInstance().SetMode( CAPTURE_MOVIE_APPEND );
		common->changePageRequest( PAGE_CAPTURE );
	}

	else if ( strcmp( cmd, "a" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, insert after frame" ) );
		common->moveToFrame( common->getPlayList() ->FindEndOfScene( common->g_currentFrame ) );
		FileTracker::GetInstance().SetMode( CAPTURE_FRAME_APPEND );
		common->changePageRequest( PAGE_CAPTURE );
	}

	else if ( strcmp( cmd, "t" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Trim" ) );
		common->changePageRequest( PAGE_TRIM );
	}

	else if ( strcmp( cmd, "v" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Timeline" ) );
		common->changePageRequest( PAGE_TIMELINE );
	}

	else if ( strcmp( cmd, "C" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "FX" ) );
		common->changePageRequest( PAGE_MAGICK );
	}

	else if ( strcmp( cmd, ":W" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Export" ) );
		common->changePageRequest( PAGE_TIMELINE );
	}

	/* write PlayList */

	else if ( strcmp( cmd, ":w" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Write playlist" ) );
		common->savePlayList( );
	}

	/* quit */

	else if ( strcmp( cmd, ":q" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "quit" ) );
		kinoDeactivate();
	}

	return FALSE;
}

void PageTimeline::timeFormatChanged()
{
	on_start_spin_value_changed( GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "start_spin" ) ), NULL );
	on_end_spin_value_changed( GTK_SPIN_BUTTON( lookup_widget( common->getWidget(), "end_spin" ) ), NULL );
	showIcons();
}

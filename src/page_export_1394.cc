/*
* page_export_1394.cc Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "page_export_1394.h"
#include "preferences.h"
#include "ieee1394io.h"
#include "message.h"
#include "frame.h"

/** Constructor for page.

\param exportPage the export page object to bind to
\param common	common object to which this page belongs
*/

Export1394::Export1394( PageExport *_exportPage, KinoCommon *_common ) :
		Export( _exportPage, _common ), avc( NULL ), exportWriter( NULL ),
		system_loaded( false )
{
	cerr << "> Creating Export1394 Page" << endl;
	entry_preroll = GTK_ENTRY( lookup_widget( common->getWidget(), "spinbutton_export_1394_preroll" ) );
}

/** Destructor for page.
 */

Export1394::~Export1394()
{
	cerr << "> Destroying Export1394 Page" << endl;
}

/** Start of page.
 */

void Export1394::start()
{
	static raw1394handle_t handle;
	gchar s[ 512 ];

	cerr << ">> Starting Export1394" << endl;

	sprintf( s, "%d", Preferences::getInstance().dvExportPrerollSec );
	gtk_entry_set_text( entry_preroll, s );

	if ( ( handle = raw1394_new_handle() ) )
	{
		if ( raw1394_set_port( handle, 0 ) >= 0 )
		{
			raw1394_destroy_handle( handle );
			handle = NULL;
			avc = new AVC();
			Preferences::getInstance( ).phyID = avc->getNodeId( Preferences::getInstance( ).avcGUID );
			Preferences::getInstance( ).phyID = avc->isPhyIDValid( Preferences::getInstance( ).phyID );
			system_loaded = true;
			if ( avc->getPort() < 0 )
			{
				delete avc;
				avc = 0;
			}
		}
		else
		{
			system_loaded = false;
			common->setStatusBar( _( "Error setting the IEEE 1394 port (host adapater)." ) );
		}
	}
#ifdef HAVE_IEC61883
	else
	{
		system_loaded = false;
		common->setStatusBar( _( "WARNING: raw1394 kernel module not loaded or failure to read/write /dev/raw1394!" ) );
	}
#else
	else
	{
		common->setStatusBar( _( "Failed to open raw1394; you must manually control recording on the device." ) );
	}
#endif

#ifdef HAVE_IEC61883
	exportWriter = new iec61883Writer( ( avc && avc->getPort() > -1 ) ? avc->getPort() : 0,
					Preferences::getInstance().channel,
					Preferences::getInstance().dvExportBuffers );
#else
	exportWriter = new dv1394Writer(
					   Preferences::getInstance().dvExportDevice,
					   Preferences::getInstance().channel,
					   Preferences::getInstance().dvExportBuffers,
					   Preferences::getInstance().cip_n,
					   Preferences::getInstance().cip_d,
					   Preferences::getInstance().syt_offset
				   );
	system_loaded = exportWriter->isValid();
	if ( !system_loaded )
	{
		common->setStatusBar( _( "WARNING: dv1394 kernel module not loaded or failure to read/write %s." ),
			 Preferences::getInstance().dvExportDevice );
	}
#endif

	/* Change the sensitivity of the hosting widget according to
	   wheter the system loaded or not */
	GtkVBox * tmp
	= GTK_VBOX( lookup_widget ( exportPage->getWidget(),
	                            "vbox_export_1394" ) );
	if ( system_loaded )
	{
		gtk_widget_set_sensitive ( GTK_WIDGET ( tmp ), TRUE );
	}
	else
	{
		gtk_widget_set_sensitive ( GTK_WIDGET ( tmp ), FALSE );
	}
}

/** Define active widgets.
    This page does not support pausing, and if the system is
    not loaded, it does not support anything at all.
 */

gulong Export1394::onActivate()
{
	if ( system_loaded )
	{
		if ( avc )
			return EXPORT_PREVIEW | EXPORT_EXPORT | EXPORT_SCENE_LIST;
		else
			return EXPORT_PREVIEW | EXPORT_SCENE_LIST;
	}
	return 0;
}

/** Leaving the page
 */

void Export1394::clean()
{
	cerr << ">> Leaving Export1394" << endl;
	delete exportWriter;
	exportWriter = NULL;
	delete avc;
	avc = NULL;
}

/** The actual export function */

enum export_result
Export1394::doExport( PlayList * playlist, int begin, int end, int every,
                      bool preview )
{
	int i = -1;
	Preferences::getInstance().dvExportPrerollSec = atoi( gtk_entry_get_text( entry_preroll ) );
	enum export_result result = EXPORT_RESULT_ABORT;
	string filename = "";

	assert( exportWriter );
	if ( exportWriter->isValid() )
	{
		Frame& frame = *GetFramePool()->GetFrame();

		bool resample = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(
		                    lookup_widget( common->getWidget(), "checkbutton_export_1394_resample" ) ) );

		int16_le_t *audio_buffers[ 4 ];
		AudioInfo info;
		AsyncAudioResample<int16_ne_t,int16_le_t>* resampler = 0;
		double adjustedRate = 0;
		int frameNum = 0;

		for ( i = 0; i < 4; i++ )
			audio_buffers[ i ] = ( int16_le_t * ) calloc( 2 * DV_AUDIO_MAX_SAMPLES, sizeof( int16_t ) );

		innerLoopUpdate( begin, begin, end, every );

		// Calculate resample rate for locked audio
		playlist->GetFrame( begin, frame );
		if ( resample )
		{
			// first call to innerLoopUpdate initializes progress tracker, which
			// we want to do because calculateAdjustedRate generates paused time.
			innerLoopUpdate( begin, begin, end, every );
			frame.GetAudioInfo( info );
			adjustedRate = calculateAdjustedRate( playlist, info.frequency, i, end, every );
		}

		// Only for export, NOT for preview.
		if ( !preview && avc )
		{
			avc->Record( Preferences::getInstance().phyID );
		}
	
		/* pre-roll */
		for ( i = begin; i <= end && i <= begin +
		        Preferences::getInstance().dvExportPrerollSec * ( frame.IsPAL() ? 25 : 30 )
		        && exportPage->isExporting; i++ )
		{

			/* re-encode the audio to ensure consistent 16bit bitstream */
			frame.GetAudioInfo( info );
			info.channels = 2;
			frame.EncodeAudio( info, audio_buffers );
			if ( !exportWriter->SendFrame( frame ) )
			{
				exportPage->isExporting = false;
				result = EXPORT_RESULT_FAILURE;
			}
		}

		/* Iterate over all frames in selection */
		for ( i = begin; i <= end && exportPage->isExporting; i += every, frameNum++ )
		{
			innerLoopUpdate( i, begin, end, every );
			playlist->GetFrame( i, frame );

			/* resample audio */
			if ( resample )
			{
				if ( ! resampler && adjustedRate )
				{
					frame.GetAudioInfo( info );

					// Determine correct amount of audio for duration
					resampler = new AsyncAudioResample<int16_ne_t,int16_le_t>(
						AUDIO_RESAMPLE_SRC_SINC_MEDIUM_QUALITY, playlist, info.frequency, i, end, every );
					if ( ! resampler )
					{
						result = EXPORT_RESULT_FAILURE;
						break;
					}
					if ( resampler->IsError() )
					{
						result = EXPORT_RESULT_FAILURE;
						std::cerr << ">>> Resampler error: " << resampler->GetError() << std::endl;
						break;
					}
				}
				int requestedSamples = frame.CalculateNumberSamples( info.frequency, frameNum );
				info.samples = resampler->Process( adjustedRate, requestedSamples );
				int16_le_t *p = resampler->GetOutput();
				for ( int s = 0; s < info.samples; s++ )
					for ( int c = 0; c < info.channels; c++ )
						audio_buffers[ c ][ s ] = *p++;
				if ( info.samples )
					frame.EncodeAudio( info, audio_buffers );
			}

			if ( !exportWriter->SendFrame( frame ) )
			{
				result = EXPORT_RESULT_FAILURE;
				break;
			}
		}

		if ( result != EXPORT_RESULT_FAILURE && i > end )
			result = EXPORT_RESULT_SUCCESS;
			
		for ( i = 0; i < 4; i++ )
			free( audio_buffers[ i ] );
		delete resampler;
			
		GetFramePool()->DoneWithFrame( &frame );
	}

	common->setStatusBar( "" );
	if ( !preview && avc )
	{
		avc->Stop( Preferences::getInstance().phyID );
	}

	return result;
}

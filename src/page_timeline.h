/*
* page_timeline.h Notebook Timeline Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_TIMELINE_H
#define _PAGE_TIMELINE_H

#include "kino_common.h"
#include "page.h"

#include <pthread.h>

enum TimelineStatus {
    TIMELINE_INIT,
    TIMELINE_IDLE,
    TIMELINE_STARTING,
    TIMELINE_RUNNING,
    TIMELINE_RESTART
};

/** This class defines the Page behaviour of the Timeline notebook page.
*/

class PageTimeline : public Page
{
private:
	KinoCommon *common;
	bool refresh_required;
	int action;
	int last_begin;
	int last_end;
	int scene;
	GtkIconView *view;
	GtkListStore *model;

	pthread_t thread;
	pthread_mutex_t key;
	pthread_mutex_t mutex;
	enum TimelineStatus showIconsRunning;
	static void* ThreadProxy( void *arg );

public:
	PageTimeline( KinoCommon *common );
	virtual ~PageTimeline()
	{ }
	gulong activate();
	void start( );
	void selectScene( int );
	void showIcons( );
	void refresh( );
	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char *cmd );
	void timeFormatChanged();
	GtkListStore *getModel() const
	{
		return model;
	}
	GtkIconView *getView() const
	{
		return view;
	}
	void* Thread();
};


#endif

/*
* page_capture.h Notebook Firewire Capture Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_CAPTURE_H
#define _PAGE_CAPTURE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "kino_common.h"
#include "page.h"
#include <libavc1394/avc1394.h>

/** This class controls the firewire capture notebook page.
*/

class PageCapture : public Page
{
private:
	KinoCommon *common;

	/* state info */
	quadlet_t avcState;
	bool	isCapturing;
	bool	captureMutex;
	gulong validComponents;
	int gui_state_was;

	/* widgets */
	GtkDrawingArea *frameArea;
	GtkToggleButton	*avcButton;
	GtkToggleButton	*recordButton;
	GtkToggleButton	*stopButton;
	GtkToggleButton	*playrecordButton;
	GtkToggleButton	*muteButton;
	GtkButton *snapshotButton;
	GtkEntry	*fileEntry;
	GtkLabel	*timecodeLabel;

public:
	bool	driver_available;
	bool avc_enabled;
	bool	audio_enabled;
	bool	check_phyid;
	bool	driver_locked;

	PageCapture( KinoCommon *common );
	virtual ~PageCapture();
	gulong activate();
	void newFile();
	void start();
	void clean();

	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char *cmd );

	void videoStartOfMovie();
	void videoPreviousScene();
	void videoStartOfScene();
	void videoRewind();
	void videoBack( int step = 1 );
	void videoPlay();
	void videoForward( int step = 1 );
	void videoFastForward();
	void videoNextScene();
	void videoEndOfScene();
	void videoEndOfMovie();
	void videoPause();
	void videoStop();
	void videoShuttle( int );
	void applyAVCState( quadlet_t );
	quadlet_t getAVCState()
	{
		return this->avcState;
	}
	void startCapture();
	void stopCapture();
	void saveFrame();
	void windowMoved();
	void showFrameInfo( int );
	Frame* getFrame();

	void CheckDevices( );
	void collectFiles( );
	std::string getHelpPage()
	{
		return "capture";
	}

};

#endif


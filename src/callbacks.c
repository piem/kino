/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "callbacks.h"
#include "support.h"
#include "commands.h"
#include "message.h"

#include <stdio.h>

extern GtkWidget *main_window;

struct navigate_control g_nav_ctl;
static gboolean doShuttle = FALSE;

gboolean
on_main_window_delete_event ( GtkWidget *widget,
                              GdkEvent *event,
                              gpointer user_data )
{
	return kinoDeactivate();
}

gboolean
on_main_window_key_press_event ( GtkWidget *widget,
                                 GdkEventKey *event,
                                 gpointer user_data )
{
	// Process the keypress event with regard to the current
	// notebook selection
	return processKeyboard( event );
}

gboolean
on_main_window_key_release_event ( GtkWidget *widget,
                                   GdkEventKey *event,
                                   gpointer user_data )
{
	/* DRD> I had to comment out the following to prevent keybaord input
	   from interrupting AVC control. I changed page_editor.cc and page_capture.cc
	   to explicitly issue videoStop() with Esc. Seems to work fine.
	 */ 
	//    if ( event->keyval != 0x00000020) /* spacebar = play/pause */
	//        videoStop();
	return FALSE;
}

void
on_new_activate ( GtkWidget *menuitem,
                  gpointer user_data )
{
	newFile( );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_open_activate ( GtkWidget *menuitem,
                   gpointer user_data )
{
	openFile( );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_save_activate ( GtkWidget *menuitem,
                   gpointer user_data )
{
	savePlayList( );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_save_as_activate ( GtkMenuItem *menuitem,
                      gpointer user_data )
{
	savePlayListAs( );
}


void
on_exit_activate ( GtkWidget *menuitem,
                   gpointer user_data )
{
	kinoDeactivate();
}


void
on_cut_current_scene_activate ( GtkWidget *menuitem,
                                gpointer user_data )
{
	processCommand( "dd" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_copy_current_scene_activate ( GtkWidget *menuitem,
                                 gpointer user_data )
{
	processCommand( "yy" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_paste_before_current_frame_activate ( GtkWidget *menuitem,
        gpointer user_data )
{
	processCommand( "P" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_insert_movie_activate ( GtkWidget *menuitem,
                           gpointer user_data )
{
	insertFile();
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}

gboolean
on_expose_event ( GtkWidget *widget,
                                     GdkEventExpose *event,
                                     gpointer user_data )
{
#if 0
	if ( gdk_events_pending() )
	{
		GdkEvent * e;
		while ( ( e = gdk_event_get() ) != NULL )
		{
			if ( e->type != GDK_EXPOSE )
			{
				gdk_event_put( ( GdkEvent * ) e );
				break;
			}
		}
	}
#endif
	windowMoved();
	return FALSE;
}

void
on_about_activate ( GtkWidget *menuitem,
                    gpointer user_data )
{
	GtkWidget * win = lookup_widget( menuitem, "about_window" );
	if ( win )
	{
		gtk_window_set_transient_for(GTK_WINDOW(win), GTK_WINDOW(main_window));
		gtk_widget_show( win );
	}
}


void
on_main_notebook_switch_page ( GtkNotebook *notebook,
                               GtkNotebookPage *page,
                               gint page_num,
                               gpointer user_data )
{
	pageStart( page_num );
}


void
on_video_start_movie_button_clicked ( GtkButton *button,
                                      gpointer user_data )
{
	videoStart( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_start_scene_button_clicked ( GtkButton *button,
                                      gpointer user_data )
{
	videoPreviousScene( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_rewind_button_clicked ( GtkButton *button,
                                 gpointer user_data )
{
	videoRewind( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_back_button_clicked ( GtkButton *button,
                               gpointer user_data )
{
	videoBack( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_play_button_clicked ( GtkButton *button,
                               gpointer user_data )
{
	videoPlay( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ) , "eventbox_edit_drawingarea" ) );
}


void
on_video_forward_button_clicked ( GtkButton *button,
                                  gpointer user_data )
{
	videoForward( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ) , "eventbox_edit_drawingarea" ) );
}


void
on_video_fast_forward_button_clicked ( GtkButton *button,
                                       gpointer user_data )
{
	videoFastForward( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_end_scene_button_clicked ( GtkButton *button,
                                    gpointer user_data )
{
	videoNextScene( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_end_movie_button_clicked ( GtkButton *button,
                                    gpointer user_data )
{
	videoEndOfMovie( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_pause_button_clicked ( GtkButton *button,
                                gpointer user_data )
{
	videoPause( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_video_stop_button_clicked ( GtkButton *button,
                               gpointer user_data )
{
	videoStop( );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}


void
on_editor_activate ( GtkMenuItem *menuitem,
                     gpointer user_data )
{
	notebookChangePage( 0 );
}


void
on_capture_activate ( GtkMenuItem *menuitem,
                      gpointer user_data )
{
	notebookChangePage( 1 );
}


void
on_timeline_activate ( GtkMenuItem *menuitem,
                       gpointer user_data )
{
	notebookChangePage( 2 );
}


gboolean
on_eventbox_edit_drawingarea_button_press_event ( GtkWidget *widget,
                                  GdkEventButton *event,
                                  gpointer user_data )
{
	g_nav_ctl.escaped = FALSE;
	gtk_widget_grab_focus( widget );
	return FALSE;
}


void
on_main_window_size_allocate ( GtkWidget *widget,
                               GtkAllocation *allocation,
                               gpointer user_data )
{
}

gboolean
on_main_window_configure_event ( GtkWidget *widget,
                                 GdkEventConfigure *event,
                                 gpointer user_data )
{
	windowMoved();
	return FALSE;
}


gboolean
on_main_window_visibility_notify_event ( GtkWidget *widget,
        GdkEvent *event,
        gpointer user_data )
{
	return FALSE;
}


gboolean
on_main_window_event ( GtkWidget *widget,
                       GdkEvent *event,
                       gpointer user_data )
{
	//visibilityChanged();
	return FALSE;
}


gboolean
on_main_window_map_event ( GtkWidget *widget,
                           GdkEvent *event,
                           gpointer user_data )
{
	visibilityChanged( TRUE );
	return FALSE;
}


gboolean
on_main_window_unmap_event ( GtkWidget *widget,
                             GdkEvent *event,
                             gpointer user_data )
{
	visibilityChanged( FALSE );
	return FALSE;
}


void
on_export_activate ( GtkMenuItem *menuitem,
                     gpointer user_data )
{
	notebookChangePage( 5 );

}


void
on_50percent_activate ( GtkMenuItem *menuitem,
                        gpointer user_data )
{
	setPreviewSize( 0.5 );
}


void
on_100percent_activate ( GtkWidget *menuitem,
                         gpointer user_data )
{
	setPreviewSize( 1.0 );
}

void
on_hscale_shuttle_value_changed        (GtkRange        *range,
                                        gpointer         user_data)
{
	if ( doShuttle == TRUE )
		videoShuttle( gtk_range_get_value( range ) );
}

gboolean
on_hscale_shuttle_button_press_event ( GtkWidget *widget,
                                       GdkEventButton *event,
                                       gpointer user_data )
{
	doShuttle = TRUE;
	return FALSE;
}

gboolean
on_hscale_shuttle_button_release_event ( GtkWidget *widget,
        GdkEventButton *event,
        gpointer user_data )
{
	doShuttle = FALSE;
	return FALSE;
}

gboolean
on_eventbox_capture_drawingarea_button_press_event
( GtkWidget *widget,
  GdkEventButton *event,
  gpointer user_data )
{

	g_nav_ctl.escaped = FALSE;
	gtk_entry_set_editable( GTK_ENTRY( lookup_widget( widget, "entry_capture_file" ) ), FALSE );
	gtk_widget_grab_focus( widget );
	return FALSE;
}



void
on_append_movie_activate ( GtkWidget *menuitem,
                           gpointer user_data )
{
	appendFile();
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_split_scene_activate ( GtkWidget *menuitem,
                          gpointer user_data )
{
	processCommand( "Ctrl+J" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_button_viewsize_clicked ( GtkButton *button,
                             gpointer user_data )
{
	setPreviewSize( -1.0 );
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "eventbox_edit_drawingarea" ) );
}

void
on_help_topics_activate ( GtkWidget *menuitem,
                          gpointer user_data )
{
	showHelp( NULL );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_undo_activate ( GtkWidget *menuitem,
                   gpointer user_data )
{
	processCommand( "u" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_redo_activate ( GtkWidget *menuitem,
                   gpointer user_data )
{
	processCommand( "Ctrl+R" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


void
on_join_scenes_activate ( GtkWidget *menuitem,
                          gpointer user_data )
{
	processCommand( "J" );
	gtk_widget_grab_focus( lookup_widget( menuitem, "eventbox_edit_drawingarea" ) );
}


gboolean
on_eventbox_trim_button_press_event ( GtkWidget *widget,
                                      GdkEventButton *event,
                                      gpointer user_data )
{
	g_nav_ctl.escaped = FALSE;
	gtk_widget_grab_focus( widget );
	return FALSE;
}


void
on_menuitem_trim_activate ( GtkMenuItem *menuitem,
                            gpointer user_data )
{
	notebookChangePage( 3 );
}



void
on_fx1_activate ( GtkMenuItem *menuitem,
                  gpointer user_data )
{
	notebookChangePage( 4 );
}


void
on_command_reference_activate ( GtkMenuItem *menuitem,
                                gpointer user_data )
{
	GtkWidget * dialog = lookup_widget( GTK_WIDGET( menuitem ), "keyhelp_window" );
#include "cmd_ref.c"

	if ( dialog )
	{
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(main_window));
		gtk_widget_show( dialog );
	}

}


void
on_time_format_smpte_activate ( GtkMenuItem *menuitem,
                                gpointer user_data )
{
	setTimeFormat( 2 );

}


void
on_time_format_frames_activate ( GtkMenuItem *menuitem,
                                 gpointer user_data )
{
	setTimeFormat( 1 );
}


void
on_time_format_none_activate ( GtkMenuItem *menuitem,
                               gpointer user_data )
{
	GtkLabel * l = GTK_LABEL( lookup_widget( GTK_WIDGET( menuitem ), "position_label_current" ) );
	gtk_label_set_text( l, "" );
	l = GTK_LABEL( lookup_widget( GTK_WIDGET( menuitem ), "position_label_total" ) );
	gtk_label_set_text( l, "" );
	setTimeFormat( 0 );
}


void
on_autosize_activate ( GtkMenuItem *menuitem,
                       gpointer user_data )
{
	setPreviewSize( 0 );
}



void
on_hscale_shuttle_event_after          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
	if ( event->type == GDK_BUTTON_RELEASE )
	{
		gtk_range_set_value( GTK_RANGE( widget ), 0 );
		doShuttle = FALSE;
	}
}


void
on_save_still_frame_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	saveFrame( );
}


void
on_publish_project_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	publishPlayList( );
}


void
on_publish_still_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	publishFrame( );
}


void
on_menuitem_v4l_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	notebookChangePage( 6 );
}


gboolean
on_main_window_scroll_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
	handleMouseScroll( event ); 
	return FALSE;
}


gboolean
on_entry_focus_in_event                (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{

	g_nav_ctl.escaped = TRUE;
	return FALSE;
}


gboolean
on_entry_focus_out_event               (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
	g_nav_ctl.escaped = FALSE;
	return FALSE;
}

void
on_time_format_clock_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	setTimeFormat( 3 );
}


void
on_time_format_ms_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	setTimeFormat( 4 );
}


void
on_time_format_s_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	setTimeFormat( 5 );
}


void
on_time_format_min_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	setTimeFormat( 6 );
}


void
on_time_format_h_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	setTimeFormat( 7 );
}

void
on_preferences_dialog_help_button_clicked
                                        (GtkButton       *button,
                                        gpointer         user_data)
{
	int page = gtk_notebook_get_current_page( GTK_NOTEBOOK( lookup_widget( GTK_WIDGET( button ), "notebook" ) ) );
	char s[ 14 ];
	s[ 13 ] = '\0';
	snprintf( s, 13, "prefs%d", page );
	showHelp( s );
}

void
on_expander_properties_activate        (GtkExpander     *expander,
                                        gpointer         user_data)
{
	setMoreInfo( gtk_expander_get_expanded( expander ) ? 0 : 1 );
	windowMoved();
	gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( expander ), "eventbox_edit_drawingarea" ) );
}

/*
* rwpipe.cc - bidirectional pipe
* Copyright (C) 2003 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "rwpipe.h"

RWPipe::RWPipe( ) : m_pid( -1 ), m_error( NULL )
{}

RWPipe::~RWPipe( )
{
	stop( );
}

bool RWPipe::run( string command )
{
	const char * args[ 4 ];

	args[ 0 ] = "/bin/sh";
	args[ 1 ] = "-c";
	args[ 2 ] = ( char * ) command.c_str( );
	args[ 3 ] = NULL;

	return g_spawn_async_with_pipes( ".", (gchar**) args, NULL, G_SPAWN_LEAVE_DESCRIPTORS_OPEN, NULL, NULL, &m_pid, &m_writer, &m_reader, NULL, &m_error );
}

bool RWPipe::isRunning( )
{
	return m_pid != -1;
}

int RWPipe::readData( void *data, int size )
{
	if ( m_pid != -1 )
	{
		int bytes = 0;
		int len;
		uint8_t *p = ( uint8_t * ) data;

		while ( size > 0 )
		{
			len = read( m_reader, p, size );
			if ( len <= 0 )
				break;
			p += len;
			size -= len;
			bytes += len;
		}

		return bytes;
	}
	else
		return -1;
}

int RWPipe::readLine( char *text, int max )
{
	int len = -1;
	strcpy( text, "" );
	if ( m_pid != -1 )
	{
		while ( len < max - 1 && readData( &text[ ++ len ], 1 ) )
			if ( text[ len ] == '\n' )
				break;
		text[ len ] = '\0';
	}
	return len;
}

int RWPipe::writeData( void *data, int size )
{
	if ( m_pid != -1 )
		return write( m_writer, ( uint8_t * ) data, size );
	else
		return -1;
}

void RWPipe::stop( )
{
	if ( m_pid != -1 )
	{
		close( m_reader );
		close( m_writer );
		waitpid( m_pid, NULL, 0 );
		m_pid = -1;
	}
}

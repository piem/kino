/*
* kino_common.cc KINO GUI Common Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fstream>
#include <iostream>
using std::cerr;
using std::endl;
#include <sstream>
#include <string>
#include <algorithm>

#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <libxml/nanohttp.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <ctype.h>

#include "kino_common.h"
#include "page_editor.h"
#include "page_capture.h"
#include "page_timeline.h"
#include "page_undefined.h"
#include "page_export.h"
#include "page_bttv.h"
#include "page_trim.h"
#include "page_magick.h"
#include "preferences.h"
#include "message.h"
#include "frame.h"
#include "displayer.h"
#include "storyboard.h"
#include "preferences.h"
#include "riff.h"
#include "avi.h"
#include "filehandler.h"
#include "ieee1394io.h"
#include "error.h"
#include "stringutils.h"
#include "rwpipe.h"


extern "C"
{
#include <pthread.h>
}

GtkWindow * KinoCommon::getWidgetWindow(GtkWidget *widget)
{
	if(widget)
		if(GTK_IS_WINDOW(widget))
			return GTK_WINDOW(widget);
		else
			return getWidgetWindow(widget->parent);
	else
		return NULL;
}

/** Constructor for KinoCommon - initialises all GUI widgets.

  	\param widget top level widget for the main window
*/

KinoCommon::KinoCommon( GtkWidget *widget ) : last_directory( "" )
{
	cerr << "> Kino Common being built" << endl;

	// Initialise class variables from GUI components
	this->widget = widget;
	this->edit_menu = lookup_widget( widget, "edit" );
	this->view_menu = lookup_widget( widget, "view1" );
	this->notebook = GTK_NOTEBOOK( lookup_widget( widget, "main_notebook" ) );
	this->video_start_movie_button = GTK_BUTTON( lookup_widget( widget, "video_start_movie_button" ) );
	this->video_start_scene_button = GTK_BUTTON( lookup_widget( widget, "video_start_scene_button" ) );
	this->video_rewind_button = GTK_BUTTON( lookup_widget( widget, "video_rewind_button" ) );
	this->video_back_button = GTK_BUTTON( lookup_widget( widget, "video_back_button" ) );
	this->video_play_button = GTK_BUTTON( lookup_widget( widget, "video_play_button" ) );
	this->video_stop_button = GTK_BUTTON( lookup_widget( widget, "video_stop_button" ) );
	this->video_forward_button = GTK_BUTTON( lookup_widget( widget, "video_forward_button" ) );
	this->video_fast_forward_button = GTK_BUTTON( lookup_widget( widget, "video_fast_forward_button" ) );
	this->video_end_scene_button = GTK_BUTTON( lookup_widget( widget, "video_end_scene_button" ) );
	this->video_end_movie_button = GTK_BUTTON( lookup_widget( widget, "video_end_movie_button" ) );
	this->video_shuttle = GTK_RANGE( lookup_widget( widget, "hscale_shuttle" ) );
	this->statusbar = GTK_STATUSBAR( lookup_widget( widget, "statusbar" ) );

	// Create page objects
	this->editor = new PageEditor( this );
	this->capture = new PageCapture( this );
	this->timeline = new PageTimeline( this );
	this->undefined = new PageUndefined( this );
	this->bttv = new PageBttv( this );
	this->exportPage = new PageExport( this );
	this->trimPage = new PageTrim( this );
	this->magickPage = new PageMagick( this );

	// Initialise other class variables
	strcpy( this->tempFileName, "" );
	this->g_currentFrame = -1;
	this->hasListChanged = TRUE;
	this->currentScene = -1;
	this->currentPage = -1;
	this->showMoreInfo = false;

	this->component_state = VIDEO_STOP;
	this->is_component_state_changing = false;
	
	SMIL::Time::TimeFormat timeFormat = static_cast< SMIL::Time::TimeFormat >( Preferences::getInstance().timeFormat );
	GtkWidget *timemenu = lookup_widget( widget, "optionmenu_time_format" );
	if  ( timeFormat == SMIL::Time::TIME_FORMAT_NONE )
		setTimeFormat( SMIL::Time::TIME_FORMAT_FRAMES );
	gtk_option_menu_set_history( GTK_OPTION_MENU( timemenu ), Preferences::getInstance().timeFormat - 1 );
	
}

/**	Destructor for the kino common object.
 */

KinoCommon::~KinoCommon()
{
	cerr << "> Kino Common being destroyed" << endl;
	clean();
	delete this->editor;
	delete this->capture;
	delete this->timeline;
	delete this->undefined;
	delete this->bttv;
	delete this->exportPage;
	delete this->trimPage;
	delete this->magickPage;
	GetFileMap() ->Clear();
	cerr << "> Kino Common destroyed" << endl;
}

/** Uses libml2 to fetch the metadata from a CMS

    Added for tagesschau.de. It uses config items newProjectURI to make an HTTP
    request and newProjectXPath to extract the project ID, and expands
    metaValues containing xpaths.

    \param projectKey a project name or identifier to put into the newProjectURI.
*/

void KinoCommon::fetchProjectMetadata( const std::string& projectKey )
{
	if ( projectKey == "" )
		return;

	// Fetch the project metadata from CMS
	char uri[1024];
	uri[1023] = '\0';
	snprintf( uri, 1023, Preferences::getInstance().newProjectURI, projectKey.c_str() );
	cerr << "Composed Project URI " << uri << endl;
	xmlNanoHTTPInit();
	void *httpContext = xmlNanoHTTPOpen( uri, NULL );
	int code = 0;
	if ( httpContext && ( code = xmlNanoHTTPReturnCode( httpContext ) ) == 200 )
	{
		char *buffer = (char*) calloc( 1, 1000 );
		int size = 0;
		int nread;
		while ( ( nread = xmlNanoHTTPRead( httpContext, buffer + size, 999 ) ) > 0 )
		{
			size += nread;
			if ( nread == 999 )
			{
				buffer = ( char* )realloc( buffer, size + 999 );
				buffer[ size + 1 ] = '\0';
			}
		}

		// Parse the response
		xmlDocPtr doc = xmlParseMemory( buffer, size );
		xmlXPathInit();
		xmlXPathContextPtr xpathContext = xmlXPathNewContext( doc );
		if ( xpathContext )
		{
			xmlXPathObjectPtr xpathResult;

			// Extract the project ID
			if ( strcmp( Preferences::getInstance().newProjectXPath, "" ) )
			{
				xpathResult = xmlXPathEval( (xmlChar*) Preferences::getInstance().newProjectXPath, xpathContext );
				if ( xpathResult )
				{
					if ( xpathResult->type == XPATH_NODESET && !xmlXPathNodeSetIsEmpty( xpathResult->nodesetval ) )
					{
						getPlayList()->SetDocId( (char*) xmlXPathCastToString( xpathResult ) );
						cerr << "newProject ID = " << xmlXPathCastToString( xpathResult ) << endl;
						if ( strcmp( reinterpret_cast< char* >( xmlXPathCastToString( xpathResult ) ), "" ) == 0 )
							modal_message( _("The server returned an empty response.\n\nPlease choose File/New to query the server again.") );
					}
					else
						modal_message( _("Failed to parse project metadata:\nthe result of newProjectXPath is empty") );
					xmlXPathFreeObject( xpathResult );
				}
				else
					modal_message( _("Failed to parse project metadata:\nbad newProjectXPath expression") );
			}

			// Expand metaValues
			map< string, vector< std::pair< std::string, std::string > > >& metaValuesMap = Preferences::getInstance().metaValues;
			map< string, vector< std::pair< std::string, std::string > > >::iterator metaValuesMapIter;
			for ( metaValuesMapIter = metaValuesMap.begin(); metaValuesMapIter != metaValuesMap.end(); ++metaValuesMapIter )
			{
				if ( metaValuesMapIter->second.size() > 0 )
				{
					const string labelPath = metaValuesMapIter->second[0].first;
					const string valuePath = metaValuesMapIter->second[0].second;

					// Expand the label
					if ( labelPath.length() > 5 && labelPath.substr( 0, 6 ) == "xpath:" )
					{
						// remove the xpath from the values
						metaValuesMapIter->second.erase( metaValuesMapIter->second.begin() );

						xpathResult = xmlXPathEval( (xmlChar*) labelPath.substr( 6 ).c_str(), xpathContext );
						if ( xpathResult )
						{
							if ( xpathResult->type == XPATH_NODESET )
							{
								for ( int i = 0; i < xmlXPathNodeSetGetLength( xpathResult->nodesetval ); i++ )
								{
									string label( (char *) xmlXPathCastNodeToString( xmlXPathNodeSetItem( xpathResult->nodesetval, i ) ) );
									metaValuesMapIter->second.push_back( make_pair( label, label ) );
								}
							}
							xmlXPathFreeObject( xpathResult );
						}
						else
							modal_message( _("Failed to parse project metadata:\nbad metaValues XPath expression") );
					}

					// Expand the value
					if ( valuePath.length() > 5 && valuePath.substr( 0, 6 ) == "xpath:" )
					{
						xpathResult = xmlXPathEval( (xmlChar*) valuePath.substr( 6 ).c_str(), xpathContext );
						if ( xpathResult )
						{
							if ( xpathResult->type == XPATH_NODESET )
							{
								for ( int i = 0; i < xmlXPathNodeSetGetLength( xpathResult->nodesetval ); i++ )
									metaValuesMapIter->second[ i ].second =
										(char *) xmlXPathCastNodeToString( xmlXPathNodeSetItem( xpathResult->nodesetval, i ) );
							}
							xmlXPathFreeObject( xpathResult );
						}
						else
							modal_message( _("Failed to parse project metadata:\nbad metaValues XPath expression") );
					}
				}
			}

			xmlXPathFreeContext( xpathContext );
		}

		free( buffer );
	}
	else if ( httpContext )
	{
		modal_message( "Server responded with error %d", code );
	}
	if ( httpContext )
		xmlNanoHTTPClose( httpContext );

	xmlNanoHTTPCleanup();
}

/** Carries out a New File request in the kino application. All notebook
	pages are informed of the action through their own newFile method.
*/

bool KinoCommon::newFile( bool prompt, bool isExiting )
{
	bool result = true;

	cerr << ">> Kino Common newFile" << endl;

	this->getPageMagick()->Stop();
	this->changePageRequest( PAGE_EDITOR );
	if ( getPlayList() ->IsDirty() )
	{
		switch ( modal_confirm( GTK_STOCK_SAVE, isExiting? GTK_STOCK_QUIT: GTK_STOCK_NEW, _("Save changes to project \"%s\" before closing?"),
			 getPlayList()->GetDocName() == "" ? _("Untitled") : getPlayList()->GetDocName().c_str() ) )
		{
		case GTK_RESPONSE_ACCEPT:
			result = savePlayList( );
			break;
		case GTK_RESPONSE_CLOSE:
			result = true;
			break;
		default:
			// Do nothing - action cancelled
			result = false;
			break;
		}
	}
	if ( result )
	{
		// Specifics for this class
		vector <string> list;
		PlayList clean;

		getPlayList( ) ->GetLastCleanPlayList( clean );
		GetFileMap() ->GetUnusedFxFiles( clean, list );

		if ( list.begin() != list.end() )
		{
			string buffer;

			vector < string >::iterator it;
			for ( it = list.begin(); it != list.end(); it ++ )
				buffer += *it + "\n";

			switch ( modal_confirm_files( GTK_STOCK_DELETE, isExiting? GTK_STOCK_QUIT: GTK_STOCK_NEW, buffer.c_str(), 
				_( "The following FX rendered files are no longer used.\nDo you want me to delete them now?" ) ) )
			{
			case GTK_RESPONSE_ACCEPT:
				for ( it = list.begin(); it != list.end(); it ++ )
					unlink( ( *it ).c_str() );
				result = true;
				break;
			case GTK_RESPONSE_CLOSE:
				// Do nothing - leave files on the system
				result = true;
				break;
			default:
				// Do nothing - action cancelled
				result = false;
				break;
			}
		}

		if ( result )
		{
			getPlayList() ->CleanPlayList( );
			GetFileMap() ->Clear();
			this->g_currentFrame = -1;
			this->currentScene = -1;
			this->setWindowTitle( );

			// Call new File for each page
			Page *page = NULL;
			for ( int index = 0; ( page = getPage( index ) ) != NULL; index ++ )
				page->newFile();

			GetStoryboard() ->reset();
			GetStoryboard() ->redraw();
			this->windowMoved();

			GetEditorBackup() ->Clear();
			getPageEditor() ->snapshot();
		}
	}

	// Prompt for the Project Name/Id if applicable
	// added for tagesschau.de
	if ( prompt && strcmp( Preferences::getInstance().newProjectURI, "" ) )
	{
		if ( strstr( Preferences::getInstance().newProjectURI, "%" ) )
		{
			char *response = modal_prompt( _("Enter a project name") );
			if ( response )
			{
				getPlayList()->SetDocTitle( response );
				fetchProjectMetadata( response );
				free( response );
			}
		}
		else
		{
			fetchProjectMetadata( "_" );
		}
	}
	return result;
}

/**	Sends change page request to the gui.

	\param page page to change to
*/

void KinoCommon::changePageRequest( int page )
{
	gtk_notebook_set_page( notebook, page );
}

/**	Carries out a change page - this should be triggered by changePageRequest
	to ensure that the GUI stays in sync with the common structure.

	\param page page to change to
*/

void KinoCommon::setCurrentPage( int page )
{
	if ( this->currentPage != page )
	{
		if ( this->currentPage != -1 )
			clean();
		this->currentPage = page;
		start();
		moveToFrame();
		gtk_widget_queue_draw( getWidget() );
	}
}

/** Returns the Page object associated to the specified notebook page.

	\param page index of Page object to return
	\return the Page object associated to the given notebook page or NULL if invalid.
*/

Page *KinoCommon::getPage( int page )
{
	Page * ret = NULL;

	switch ( page )
	{
	case PAGE_EDITOR:
		ret = getPageEditor();
		break;
	case PAGE_CAPTURE:
		ret = getPageCapture();
		break;
	case PAGE_TIMELINE:
		ret = getPageTimeline();
		break;
	case PAGE_TRIM:
		ret = getPageTrim();
		break;
	case PAGE_EXPORT:
		ret = getPageExport();
		break;
	case PAGE_MAGICK:
		ret = getPageMagick();
		break;
	case PAGE_BTTV:
		ret = getPageBttv();
		break;
	}

	return ret;
}

/** Returns the current Page object.

	\return the current Page object (will be PageUndefined if current page is invalid).
*/

Page *KinoCommon::getCurrentPage( )
{
	Page * ret = getPage( this->currentPage );
	if ( ret == NULL )
		ret = getPageUndefined();
	return ret;
}

/** Returns the Page associated to the Editor page.

	\return the Page object requested
*/

PageEditor *KinoCommon::getPageEditor( )
{
	return editor;
}

/** Returns the Page associated to the Capture page.

	\return the Page object requested
*/

PageCapture *KinoCommon::getPageCapture( )
{
	return capture;
}

/** Returns the Page associated to the Timeline page.

	\return the Page object requested
*/

PageTimeline *KinoCommon::getPageTimeline( )
{
	return timeline;
}

/** Returns the Page associated to the Undefined page.

	\return the Page object requested
*/

PageUndefined *KinoCommon::getPageUndefined( )
{
	return undefined;
}

/** Returns the 'BTTV' page (test code for extending hardware support).

	\return the Page object requested
*/

PageBttv *KinoCommon::getPageBttv( )
{
	return bttv;
}

/** Returns the Export page.

	\return the Page object requested
*/

PageExport *KinoCommon::getPageExport( )
{
	return exportPage;
}

/** Returns the Trim page.

	\return the Page object requested
*/

PageTrim *KinoCommon::getPageTrim( )
{
	return trimPage;
}

/** Returns the Magick page.

	\return the Page object requested
*/

PageMagick *KinoCommon::getPageMagick( )
{
	return magickPage;
}

/** Give option to import a file by transcoding to DV.
*/

std::string KinoCommon::importFile( const char *filename )
{
	std::string importedFile( filename );

	if ( modal_confirm( GTK_STOCK_OK, NULL,
		_("\"%s\" is not a DV file. Do you want to import it?"), filename )
		== GTK_RESPONSE_ACCEPT )
	{
		const char * args[ 7 ];
		int pid, status = -1;
		GError *gerror = NULL;
		gboolean visible = TRUE;
		GtkWidget *dialog = lookup_widget( widget, "import_window" );
		GtkProgressBar *progressBar = GTK_PROGRESS_BAR( lookup_widget( widget, "progressbar_import" ) );
		char *kinoHome = getenv("KINO_HOME");
		string homeFile;
		int testHomeFile = 0;
		RWPipe pipe;
		std::ostringstream command;
		char duration[ 13 ];

		// Get the duration as a time value string
		command << "\"" << DATADIR << "/kino/scripts/import/duration.sh\" ";
		command << "\"" << filename << "\"" << std::ends;
		duration[0] = 0;
		if ( pipe.run( command.str() ) )
		{
			while ( pipe.readLine( duration, sizeof(duration) ) == 0 );
			pipe.stop();
		}

		if ( kinoHome )
			homeFile = string( kinoHome ) + string( "/import/media.sh" );
		else
			homeFile = string( getenv( "HOME" ) ) + string( "/kino/import/media.sh" );

		gtk_window_set_transient_for( GTK_WINDOW( dialog ), GTK_WINDOW( getWidgetWindow( getWidget() ) ) );
		gtk_progress_bar_set_fraction( progressBar, 0 );
		gtk_progress_bar_set_text( progressBar, "" );
		gtk_widget_show_all( dialog );

		// Check if media is loaded
		if ( g_currentFrame == -1 )
		{
			// Use Preferences/Defaults
			if ( Preferences::getInstance().defaultNormalisation == NORM_UNSPECIFIED )
			{
				switch ( modal_confirm( "PAL", "NTSC", _("Please choose a video standard") ) )
				{
				case GTK_RESPONSE_ACCEPT:
					Preferences::getInstance().defaultNormalisation = NORM_PAL;
					break;
				case GTK_RESPONSE_CLOSE:
					Preferences::getInstance().defaultNormalisation = NORM_NTSC;
					break;
				default:
					// Do nothing - action cancelled
					gtk_widget_hide( dialog );
					return importedFile;
				}
			}
			args[ 3 ] = const_cast<char*>(
				Preferences::getInstance().defaultNormalisation == NORM_PAL ? "pal" : "ntsc" );
			args[ 4 ] = const_cast<char*>(
				Preferences::getInstance().defaultAspect == ASPECT_169 ? "16:9" : "4:3" );
			switch ( Preferences::getInstance().defaultAudio )
			{
				case AUDIO_32KHZ:
					args[ 5 ] = "32000";
					break;
				case AUDIO_44KHZ:
					args[ 5 ] = "44100";
					break;
				case AUDIO_48KHZ:
					args[ 5 ] = "48000";
					break;
			}
		}
		else
		{
			// Use first frame
			Frame& frame = *GetFramePool()->GetFrame();
			AudioInfo ainfo;
			std::ostringstream strstream;

			this->getPlayList() ->GetFrame( 0, frame );
			frame.GetAudioInfo( ainfo );
			strstream << ainfo.frequency;
			args[ 3 ] = const_cast<char*>( frame.IsPAL() ? "pal" : "ntsc" );
			args[ 4 ] = const_cast<char*>( frame.IsWide() ? "16:9" : "4:3" );
			args[ 5 ] = const_cast<char*>( strstream.str().c_str() );
			GetFramePool()->DoneWithFrame( &frame );
		}
		importedFile = StringUtils::replaceAll( importedFile, "\"", "\\\"") + std::string( ".dv" );
		
		args[ 0 ] = DATADIR "/kino/scripts/import/media.sh";
		args[ 1 ] = filename;
		args[ 2 ] = importedFile.c_str();
		args[ 6 ] = NULL;

		// Allow a script in home directory to override
		testHomeFile = open( homeFile.c_str(), O_RDONLY );
		if ( testHomeFile > -1 )
		{
			close( testHomeFile );
			args[ 0 ] = const_cast< char* >( homeFile.c_str() );
		}

		g_spawn_async_with_pipes( ".", (gchar**) args, NULL, G_SPAWN_DO_NOT_REAP_CHILD,
			NULL, NULL, &pid, NULL, NULL, NULL, &gerror );
		
		// Get the total expected frames from the detected duration
		int totalFrames = 0;
		SMIL::MediaClippingTime tempTime( strcmp( args[ 3 ], "pal" ) ? ( 30000.0 / 1001.0 ) : 25.0 );
		if ( strcmp( duration, "" ) )
		{
			tempTime.parseTimeValue( duration );
			totalFrames = tempTime.getFrames() - 1;
		}

		// Check for cancellation or child exit
 		while ( ! WIFEXITED( status ) && visible )
		{
 			timespec ts = { 0, 100000000L };
			while ( gtk_events_pending() )
				gtk_main_iteration();
			nanosleep( &ts, NULL );

			// Determine size of converted file
			struct stat fileStatus;
			if ( stat( importedFile.c_str(), &fileStatus ) == 0 )
			{
				int frames = fileStatus.st_size / ( strcmp( args[ 3 ], "pal" ) ? 120000 : 144000 );
				if ( frames > 0 )
				{
					// If we have a valid duration
					if ( totalFrames > 0 )
					{
						double fraction = frames;
						fraction /= totalFrames;
						gtk_progress_bar_set_fraction( progressBar, fraction > 1.0 ? 1.0 : fraction );
					}
					else
					{
						gtk_progress_bar_pulse( progressBar );
					}
				
					// Put the amount converted as a textual time value
					gtk_progress_bar_set_text( progressBar, tempTime.parseFramesToString( frames, getTimeFormat() ).c_str() );
				}
				else
				{
					gtk_progress_bar_pulse( progressBar );
				}
			}
			else
			{
				gtk_progress_bar_pulse( progressBar );
			}
			g_object_get( G_OBJECT( dialog ), "visible", &visible, static_cast<gchar*>( NULL ) );
			waitpid( pid, &status, WNOHANG );
		}
		if ( ! WIFEXITED( status ) )
		{
			kill( pid, SIGTERM );
			waitpid( pid, &status, WNOHANG );
			if ( ! WIFEXITED( status ) )
			{
 				timespec ts = { 0, 100000000L };
				nanosleep( &ts, NULL );
				kill( pid, SIGKILL );
				waitpid( pid, &status, 0 );
			}
		}
		gtk_widget_hide( dialog );
	}
	return importedFile;
}

/** Inserts a file at the current position.
*/

void KinoCommon::insertFile( )
{
	changePageRequest( PAGE_EDITOR );
	char * filename = this->getFileToOpen( _( "Choose a DV or SMIL file to insert" ) );
	if ( filename && strcmp( filename, "" ) )
	{
		switch ( checkFile( filename ) )
		{
		case AVI:
		case RAW_DV:
		case QT:
			if ( loadMediaObject( filename, g_currentFrame == -1 ? 0 : g_currentFrame ) )
			{
				hasListChanged = TRUE;
				getPlayList() ->SetDirty( true );
				break;
			}
			// Else, ask to import
		case UNKNOWN_FORMAT:
		{
			const std::string& importedFile = importFile( filename );
			if ( loadMediaObject( const_cast<char*>( importedFile.c_str() ), g_currentFrame == -1 ? 0 : g_currentFrame ) )
			{
				hasListChanged = TRUE;
				getPlayList() ->SetDirty( true );
			}
			else
			{
				modal_message( _( "Failed to load media file \"%s\"" ), importedFile.c_str() );
			}
			break;
		}
		case PLAYLIST:
			if ( loadPlayList( filename, g_currentFrame ) )
			{
				hasListChanged = TRUE;
				getPlayList() ->SetDirty( true );
				getPageEditor() ->snapshot();
			}
			break;
		}
	}

	setWindowTitle();
	moveToFrame( );
}

/** Appends a file after the current position.
*/

void KinoCommon::appendFile( )
{
	changePageRequest( PAGE_EDITOR );
	char * filename = this->getFileToOpen( _( "Choose a DV or SMIL file to append" ) );
	if ( filename && strcmp( filename, "" ) )
	{
		switch ( checkFile( filename ) )
		{
		case AVI:
		case RAW_DV:
		case QT:
			if ( loadMediaObject( filename, g_currentFrame + 1 ) )
			{
				g_currentFrame++;
				hasListChanged = TRUE;
				getPlayList() ->SetDirty( true );
				break;
			}
			// Else, ask to import
		case UNKNOWN_FORMAT:
		{
			const std::string& importedFile = importFile( filename );
			if ( loadMediaObject( const_cast<char*>( importedFile.c_str() ), g_currentFrame + 1 ) )
			{
				g_currentFrame++;
				hasListChanged = TRUE;
				getPlayList() ->SetDirty( true );
				break;
			}
			else
			{
				modal_message( _( "Failed to load media file \"%s\"" ), importedFile.c_str() );
			}
			break;
		}
		case PLAYLIST:
			// Insert the new smil
			if ( loadPlayList( filename, g_currentFrame + 1 ) )
			{
				g_currentFrame++;
				hasListChanged = TRUE;
				getPlayList() ->SetDirty( true );
				getPageEditor() ->snapshot();
			}
			break;
		}
	}

	setWindowTitle();
	moveToFrame( );
}

/** Opens a file. An AVI or a PLAYLIST causes a newFile to called prior to the load
	and a PLAYLIST becomes the currently editted file.
*/

void KinoCommon::loadFile( )
{
	changePageRequest( PAGE_EDITOR );
	char * filename = this->getFileToOpen( _( "Choose a DV or SMIL file" ) );
	bool askNewFile = true;

	if ( filename && strcmp( filename, "" ) )
	{
		switch ( checkFile( filename ) )
		{
		case AVI:
		case RAW_DV:
		case QT:
			askNewFile = false;
			if ( newFile( ) )
			{
				GetEditorBackup() ->Clear();
				if ( loadMediaObject( filename, 0 ) )
				{
					g_currentFrame = 0;
					hasListChanged = TRUE;
					break;
				}
				// Else, ask to import
			}
			else
				break;
		case UNKNOWN_FORMAT:
			if ( ( askNewFile && newFile( ) ) || ! askNewFile )
			{
				const std::string& importedFile = importFile( filename );
				GetEditorBackup() ->Clear();
				if ( loadMediaObject( const_cast<char*>( importedFile.c_str() ), 0 ) )
				{
					g_currentFrame = 0;
					hasListChanged = TRUE;
				}
				else
				{
					modal_message( _( "Failed to load media file \"%s\"" ), importedFile.c_str() );
				}
			}
			break;
		case PLAYLIST:
			if ( newFile( false ) )
			{
				GetEditorBackup() ->Clear();
				if ( loadPlayList( filename, 0 ) )
				{
					g_currentFrame = 0;
					hasListChanged = TRUE;
					getPageEditor( ) ->snapshot( );
					getPlayList( ) ->SetDocName( filename );
					getPlayList( ) ->SetDirty( false );
					fetchProjectMetadata( getPlayList()->GetDocId() );
				}
			}
			break;
		}
	}
	setWindowTitle();
	this->currentScene = 0;
	this->getCurrentPage()->clean();
	this->getCurrentPage()->start();
	moveToFrame( );
}

/** Save the current play list with a different name to the original loaded (or
	previously saved as) play list file name.
*/

bool KinoCommon::savePlayListAs( )
{
	bool result = false;
	int format = 1;
	char *filename = this->getFileToSaveFormat( _( "Save the movie" ), format );

	if ( filename && strcmp( filename, "" ) )
	{
		// Append an extension if not supplied
		char* extension = strrchr( filename, '.' );
		switch ( format )
		{
			case 0: // SMIL
			case 1: // Legacy XML
				if ( !extension || ( strcmp( extension, ".kino" ) && 
					strcmp( extension, ".smil" ) && strcmp( extension, ".xml" ) ) )
					strcat( filename, ".kino" );
				break;
			case 2: // ELI
				if ( !extension || strcmp( extension, ".eli" ) )
					strcat( filename, ".eli" );
				break;
			case 3: // SRT - SubRip subtitle
				if ( !extension || strcmp( extension, ".srt" ) )
					strcat( filename, ".srt" );
				break;
		}
		
		// See if it exists already and warn user
		FILE *f = fopen( filename, "r" );
		if ( f )
		{
			fclose( f );
			if ( modal_confirm( GTK_STOCK_SAVE, NULL, _("The file \"%s\" already exists. Do you want to continue?\n"), filename ) != GTK_RESPONSE_ACCEPT )
				return result;
		}
		switch ( format )
		{
			case 0: // SMIL
				playlist.SetDocName( filename );
				result = savePlayList( filename );
				break;
			case 1: // Legacy XML
				if ( !this->getPlayList()->SavePlayList( filename, true ) )
				{
					modal_message( _( "Could not save the XML" ) );
					result = false;
				}
				break;
			case 2: // ELI
				if ( g_currentFrame != -1 )
				{
					Frame& frame = *GetFramePool()->GetFrame();
					this->getPlayList() ->GetFrame( g_currentFrame, frame );
					if ( !this->getPlayList() ->SavePlayListEli( filename, frame.IsPAL() ) )
					{
						modal_message( _( "Could not save the ELI" ) );
						result = false;
					}
					GetFramePool()->DoneWithFrame( &frame );
				}
				else
				{
					modal_message( _( "Could not save the ELI" ) );
					result = false;
				}
				break;
			case 3: // SRT - SubRip subtitle
				if ( !this->getPlayList()->SavePlayListSrt( filename ) )
				{
					modal_message( _( "Could not save the subtitle" ) );
					result = false;
				}
				break;
		}
	}
	return result;
}

/** Save the current play list to the file used to load the playlist originally. If
	no file was originally used then use savePlayListAs to obtain one.
*/

bool KinoCommon::savePlayList( )
{
	bool result = true;
	if ( getPlayList() ->GetDocName() == "" )
		result = savePlayListAs( );
	else
		result = savePlayList( ( char * ) ( getPlayList() ->GetDocName().c_str() ) );
	return result;
}


/**	Save the current frame as a still frame.
*/

void KinoCommon::saveFrame( )
{
	if ( currentPage == PAGE_CAPTURE )
	{
		getPageCapture()->saveFrame();
	}
	else
	{
		char * filename = common->getFileToSave( _( "Save Still Frame" ) );
		if ( filename && strcmp( filename, "" ) )
			common->saveFrame( common->g_currentFrame, filename );
	}
}

/**

Used by update_preview_cb to generate a file preview in the File->Open dialog

*/
gboolean generate_file_preview( GtkWidget *preview, const char *filename )
{
	FileHandler *mediaFile = 0;
	try
	{
		if ( strcmp( filename, "" ) )
		{
			switch ( common->checkFile( const_cast<char*>( filename ) ) )
			{
				case AVI:
					mediaFile = new AVIHandler();
					break;
				case RAW_DV:
					mediaFile = new RawHandler();
					break;
				case QT:
#ifdef HAVE_LIBQUICKTIME
					mediaFile = new QtHandler();
#endif
					break;
			}
		}
	
		if ( mediaFile && mediaFile->Open( filename ) )
		{
			// Force GDK displayer because freeing the XvDisplayer will
			// causing nothing to show
			Frame& frame = *GetFramePool()->GetFrame();
			int frameNum = mediaFile->GetTotalFrames() < 61 ? ( mediaFile->GetTotalFrames() / 2 ) : 60;

			if ( mediaFile->GetFrame( frame, frameNum ) >= 0 )
			{
				GdkDisplayer* display = new GdkDisplayer( preview, frame.GetWidth(), frame.GetHeight(), frame.IsWide() );
				unsigned char *pixels = new unsigned char[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
				frame.ExtractPreviewRGB( pixels );
				gdk_window_clear( preview->window );
				display->put( pixels );
				delete[] pixels;
				delete display;
			}
			GetFramePool()->DoneWithFrame( &frame );
			delete mediaFile;
			mediaFile = 0;

			return true;
		}
	}
	catch ( string s )
	{
		cerr << "generate_file_preview exception: " << s << endl;
	}
	delete mediaFile;

	return false;
}

/**

Callback used by KinoCommon::getFileToOpen to generate file previews

*/

void update_preview_cb (GtkFileChooser *file_chooser, gpointer data)
{
	GtkWidget *preview;
	char *filename;
	gboolean have_preview;

	preview = GTK_WIDGET( data );
	filename = gtk_file_chooser_get_preview_filename( file_chooser );

	if ( filename && !g_file_test( filename, G_FILE_TEST_IS_DIR )) {
		have_preview = generate_file_preview( preview, filename );
		gtk_file_chooser_set_preview_widget_active( file_chooser,
						have_preview );
	}
}

/** Handles the modal dialog for collecting a file to open. The file is not opened,
	but returned by this method.

	\param title title of the window
	\return a string inidicating the selected file (an empty string denotes no selection)
*/

char *KinoCommon::getFileToOpen( char *title, bool isDVFile, GtkWidget *widget )
{
	GtkWidget *dialog;
	GtkDrawingArea *preview;
	GtkFileFilter *filter;

	dialog = gtk_file_chooser_dialog_new( title,
				getWidgetWindow(widget),
				GTK_FILE_CHOOSER_ACTION_OPEN,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				static_cast<gchar*>( NULL ) );
	gtk_dialog_set_alternative_button_order( GTK_DIALOG(dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_CANCEL, -1 );
// 	gtk_file_chooser_set_select_multiple( GTK_FILE_CHOOSER(dialog), TRUE );

	if ( isDVFile )
	{
		preview = ( GtkDrawingArea* )gtk_drawing_area_new();
		gtk_widget_set_size_request( GTK_WIDGET(preview), 160, 120 );
		gtk_file_chooser_set_preview_widget( GTK_FILE_CHOOSER(dialog),
					GTK_WIDGET(preview) );
		g_signal_connect( dialog, "update-preview",
					G_CALLBACK(update_preview_cb), preview );
	
		filter = gtk_file_filter_new();
		gtk_file_filter_set_name( filter, "All Files" );
		gtk_file_filter_add_pattern( filter, "*" );
		gtk_file_chooser_add_filter( GTK_FILE_CHOOSER(dialog), filter );
	
		filter = gtk_file_filter_new();
		gtk_file_filter_set_name( filter, "SMIL Files (*.smil, *.kino)" );
		gtk_file_filter_add_pattern( filter, "*.smil" );
		gtk_file_filter_add_pattern( filter, "*.SMIL" );
		gtk_file_filter_add_pattern( filter, "*.kino" );
		gtk_file_filter_add_pattern( filter, "*.Kino" );
		gtk_file_filter_add_pattern( filter, "*.KINO" );
		gtk_file_chooser_add_filter( GTK_FILE_CHOOSER(dialog), filter );
	
		filter = gtk_file_filter_new();
		gtk_file_filter_set_name( filter, "Raw DV Files (*.dv, *.dif)" );
		gtk_file_filter_add_pattern( filter, "*.dv" );
		gtk_file_filter_add_pattern( filter, "*.dif" );
		gtk_file_filter_add_pattern( filter, "*.DV" );
		gtk_file_filter_add_pattern( filter, "*.Dv" );
		gtk_file_filter_add_pattern( filter, "*.DIF" );
		gtk_file_filter_add_pattern( filter, "*.Dif" );
		gtk_file_chooser_add_filter( GTK_FILE_CHOOSER(dialog), filter );
	
		filter = gtk_file_filter_new();
		gtk_file_filter_set_name( filter, "AVI Files (*.avi)" );
		gtk_file_filter_add_pattern( filter, "*.avi" );
		gtk_file_filter_add_pattern( filter, "*.AVI" );
		gtk_file_filter_add_pattern( filter, "*.Avi" );
		gtk_file_chooser_add_filter( GTK_FILE_CHOOSER(dialog), filter );
	
#ifdef HAVE_LIBQUICKTIME
		filter = gtk_file_filter_new();
		gtk_file_filter_set_name( filter, "Quicktime Files (*.mov)" );
		gtk_file_filter_add_pattern( filter, "*.mov" );
		gtk_file_filter_add_pattern( filter, "*.MOV" );
		gtk_file_filter_add_pattern( filter, "*.Mov" );
		gtk_file_chooser_add_filter( GTK_FILE_CHOOSER(dialog), filter );
#endif
	}
	
	gtk_window_set_modal( GTK_WINDOW( dialog ), TRUE );
	if ( last_directory == "" )
	{
		string directory = getPlayList() ->GetProjectDirectory( );
		if ( directory != "" )
			gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( dialog ), ( char * ) ( directory + "/" ).c_str() );
		last_directory = directory;
	}
	else
	{
		gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( dialog ), ( char * ) ( last_directory + "/" ).c_str() );
	}
	if (gtk_dialog_run( GTK_DIALOG( dialog ) ) == GTK_RESPONSE_ACCEPT) {
		gchar *filename = gtk_file_chooser_get_filename(
						GTK_FILE_CHOOSER( dialog ));
		if ( filename )
		{
			strncpy( tempFileName, filename, sizeof(tempFileName) );
			last_directory = directory_utils::get_directory_from_file( filename );
			g_free( filename );
		}
		else
			strcpy( tempFileName, "" );
	} else
		strcpy( tempFileName, "" );

	gtk_widget_destroy( dialog );
	return tempFileName;
}

char *KinoCommon::getFileToSaveFormat( char *title, GtkWidget *widget, int& format )
{
	GtkWidget *dialog;
	GtkComboBox* formatCombo = NULL;

	dialog = gtk_file_chooser_dialog_new( title,
					getWidgetWindow(widget),
					GTK_FILE_CHOOSER_ACTION_SAVE,
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					static_cast<gchar*>( NULL ) );
	gtk_dialog_set_alternative_button_order (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT, GTK_RESPONSE_CANCEL, -1);
	gtk_window_set_modal( GTK_WINDOW( dialog ), TRUE );
	gtk_dialog_set_default_response( GTK_DIALOG( dialog ), GTK_RESPONSE_ACCEPT );

	if ( format )
	{
		GtkWidget* hbox = gtk_hbox_new( FALSE, 6 );
		GtkWidget* label = gtk_label_new_with_mnemonic( _("F_ormat:") );
		
		formatCombo = GTK_COMBO_BOX( gtk_combo_box_new_text() );
		gtk_widget_show( hbox );
		gtk_widget_show( label );
		gtk_widget_show( GTK_WIDGET( formatCombo ) );
	
		gtk_combo_box_append_text( formatCombo, "SMIL 2.0" );
		gtk_combo_box_append_text( formatCombo, "Kino < 0.9.1 XML" );
		gtk_combo_box_append_text( formatCombo, "MJPEG Tools ELI" );
		gtk_combo_box_append_text( formatCombo, "SRT subtitle" );
		gtk_combo_box_set_active( formatCombo, 0 );
		gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0 );
		gtk_box_pack_start( GTK_BOX(hbox), GTK_WIDGET(formatCombo), FALSE, FALSE, 0 );
		gtk_file_chooser_set_extra_widget( GTK_FILE_CHOOSER( dialog ), hbox );
	}

	if ( last_directory == "" )
	{
		string directory = getPlayList() ->GetProjectDirectory( );
		if ( directory != "" )
			gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( dialog ), ( char * ) ( directory + "/" ).c_str() );
		last_directory = directory;
	}
	else
	{
		gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( dialog ), ( char * ) ( last_directory + "/" ).c_str() );
	}
	if (gtk_dialog_run (GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if ( filename )
		{
			strncpy (tempFileName, filename, sizeof(tempFileName));
			g_free (filename);
		}
		else
			strcpy( tempFileName, "" );
	} else
		strcpy(tempFileName, "");
	if ( format )
		format = gtk_combo_box_get_active( GTK_COMBO_BOX(formatCombo) );

	gtk_widget_destroy (dialog);
	return tempFileName;
}

/**	Determines the format of the file.

	It is required that a file be at least 20 bytes for it to load.

	Note that it is currently required that mediafiles have either
	a .dv or .avi suffix since the PlayList uses the suffixes .avi
	and .dv to determine what FileHandler to create. Smil files
	are allowed to load without suffixes. (Correct?)

	\param filename file to check
	\return AVI, PLAYLIST, RAW_DV or UNKNOWN_FORMAT
*/

int KinoCommon::checkFile( char *FileName )
{
	// Try reading a few bytes from the file ...
	std::ifstream file( FileName );
	std::vector<char> buffer( 22, '\0' );
	file.read( &buffer[ 0 ], buffer.size() );

	// If it didn't work, we're done ...
	if ( file.bad() )
	{
		cerr << "> Error reading file: " << FileName << endl;
		return UNKNOWN_FORMAT;
	}

	// If the file is shorter than our buffer, we're done ...
	if ( file.eof() )
	{
		cerr << "> File size < " << buffer.size() << " bytes: " << FileName << endl;
		return UNKNOWN_FORMAT;
	}

	// Start looking at file suffixes ...
	const std::string filename( FileName );
	std::string suffix( filename.begin() + filename.rfind( "." ), filename.end() );
	std::transform( suffix.begin(), suffix.end(), suffix.begin(), ::tolower );

	if ( suffix == ".avi" )
	{
		return AVI;
	}
	else if ( suffix == ".dv" || suffix == ".dif" )
	{
		// This bit of magic brought to you from the dvgrab AVI iso handler ...
		const unsigned char * const p = reinterpret_cast<unsigned char*>( &buffer[ 0 ] );
		const int section_type = p[ 0 ] >> 5;
		const int dif_sequence = p[ 1 ] >> 4;

		if ( 0 == section_type && 0 == dif_sequence )
			return RAW_DV;

		return UNKNOWN_FORMAT;
	}
	else if ( suffix == ".mov" )
	{
		return QT;
	}

	// Check to see if it's SMIL ...
	const std::string smil_magic( "<?xml version=\"1.0\"?>" );
	if ( std::string( buffer.begin(), buffer.begin() + smil_magic.size() ) == smil_magic )
		return PLAYLIST;

	// No can do!
	return UNKNOWN_FORMAT;
}

/** Sets the document name in the window title.
*/
void KinoCommon::setWindowTitle( )
{
	GtkWidget * mainWindow = lookup_widget( this->getWidget(), "main_window" );
	if ( mainWindow )
	{
		char strbuf[ 1024 ];
		if ( getPlayList() ->GetDocName( ) == "" )
			strcpy( strbuf, _( "Untitled" ) );
		else
			strcpy( strbuf, getPlayList() ->GetDocName( ).c_str() );
		if ( getPlayList( ) ->IsDirty( ) )
			strcat( strbuf, _(" (modified) ") );
		strcat( strbuf, " - Kino" );
		gtk_window_set_title( GTK_WINDOW( mainWindow ), strbuf );
	}
	if ( getPlayList()->GetDocName( ) != "" && !getPlayList()->IsDirty() )
	{
		Preferences::getInstance().addRecentFile( getPlayList()->GetDocName() );
		updateRecentFiles();
	}
	bool b = GetEditorBackup()->CanUndo();
	gtk_widget_set_sensitive( lookup_widget( widget, "undo" ), b );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_undo" ), b );
	b = GetEditorBackup()->CanRedo();
	gtk_widget_set_sensitive( lookup_widget( widget, "redo" ), b );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_redo" ), b );
}

/** Loads the specified media file before the specified frame.

	\param file file to load
	\param before frame to insert at
*/

bool KinoCommon::loadMediaObject( char *file, int before )
{

	PlayList newList;
	bool result = true;

	try
	{
		if ( newList.LoadMediaObject( file ) )
		{
			this->getPlayList() ->InsertPlayList( newList, before );
			getPageEditor() ->snapshot();
		}
		else
		{
			result = false;
		}
	}
	catch ( string s )
	{
		cerr << "Could not load file " << file << ", because an exception has occurred: " << endl;
		cerr << s << endl;
		result = false;
	}

	return result;
}


/** Loads the specified play list before the specified frame.
 
	\param file file to load
	\param before frame to insert at
*/

bool KinoCommon::loadPlayList( char *file, int before )
{

	PlayList newList;
	bool result = newList.LoadPlayList( file );

	if ( result )
	{
		this->getPlayList() ->InsertPlayList( newList, before );
		
		// propogate these new attributes
		if ( newList.GetDocId() != "" )
			this->getPlayList()->SetDocId( newList.GetDocId().c_str() );
		if ( newList.GetDocTitle() != "" )
			this->getPlayList()->SetDocTitle( newList.GetDocTitle().c_str() );
	}
	else
		modal_message( _( "Could not load the SMIL file \"%s\"" ), file );

	return result;
}

/** Saves the playlist.
 
	\param file file to save
*/

bool KinoCommon::savePlayList( char *file )
{
	bool result = false;
	result = getPlayList() ->SavePlayList( file );

	if ( result )
		setWindowTitle( );
	else
		modal_message( _( "Could not save the SMIL" ) );

	return result;
}

static int tohex( char p )
{
	return isdigit( p ) ? p - '0' : tolower( p ) - 'a' + 10;
}

static char *url_decode( char *dest, char *src )
{
	char *p = dest;
	
	while ( *src )
	{
		if ( *src == '%' )
		{
			*p ++ = ( tohex( *( src + 1 ) ) << 4 ) | tohex( *( src + 2 ) );
			src += 3;
		}
		else
		{
			*p ++ = *src ++;
		}
	}

	*p = *src;

	return dest;
}

/** Loads all files specified in the command line style arguments.
 
  	\param argc number of arguments
	\param argv arguments
*/

void KinoCommon::bulkLoad( int argc, char* argv[] )
{
	char temp[ PATH_MAX + NAME_MAX  ];
	char filename[ PATH_MAX + NAME_MAX ];
	
	for ( int i = 1; i < argc; ++i )
	{
		/* Get the real name of the file, to make sure that
		we store absolute paths in smil files */
		if ( NULL != realpath( url_decode( temp, argv[ i ] ), filename ) )
		{

			switch ( checkFile( filename ) )
			{
			case AVI:
			case RAW_DV:
			case QT:
				// a little hook to make the trimmer load the clip for insert edting
				if ( currentPage == PAGE_TRIM )
				{
					gtk_entry_set_text( GTK_ENTRY( lookup_widget( widget, "entry_trim_clip" ) ), filename );
					break;
				}
				else if ( loadMediaObject( filename, this->getPlayList() ->GetNumFrames() ) )
				{
					if ( g_currentFrame == -1 )
						g_currentFrame = 0;
					this->hasListChanged = TRUE;
					break;
				}
				else
				{
					cerr << "KinoCommon::bulkLoad: Failed to load " << filename << endl;
					// Else, ask to import
				}
			case UNKNOWN_FORMAT:
				{
					const std::string& importedFile = importFile( filename );
					if ( currentPage == PAGE_TRIM )
					{
						gtk_entry_set_text( GTK_ENTRY( lookup_widget( widget, "entry_trim_clip" ) ), importedFile.c_str() );
					}
					else if ( loadMediaObject( const_cast<char*>( importedFile.c_str() ), this->getPlayList() ->GetNumFrames() ) )
					{
						if ( g_currentFrame == -1 )
							g_currentFrame = 0;
						hasListChanged = TRUE;
						getPlayList() ->SetDirty( true );
					}
					else
					{
						modal_message( _( "Failed to load media file \"%s\"" ), importedFile.c_str() );
					}
				}
				break;
			case PLAYLIST:
				{
					int last_count = getPlayList( ) ->GetNumFrames( );
					string last_doc_name = getPlayList( ) ->GetDocName( );
					if ( loadPlayList( filename, last_count ) )
					{
						if ( last_count == 0 && last_doc_name == "" )
						{
							getPlayList() ->SetDocName( filename );
							getPlayList() ->SetDirty( false );
						}
						else
						{
							getPlayList() ->SetDirty( true );
						}
						getPageEditor() ->snapshot();
						if ( g_currentFrame == -1 )
							g_currentFrame = 0;
						this->hasListChanged = TRUE;
					}
				}
				break;
			}
		}
		else
		{
			cerr << "KinoCommon::bulkLoad: Unable to resolve " << filename << endl;
		}
	}
	
	setWindowTitle( );
}

/** Save the specified frame to the specified file.
 
  	\param	abs_frame_num the frame to save
	\param	file the file to save it to
*/

void KinoCommon::saveFrame( int abs_frame_num, char *file )
{
	Frame *frame = GetFramePool()->GetFrame();
	if ( frame != NULL )
	{
		unsigned char pixels[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
		GError *gerror = NULL;
		this->getPlayList()->GetFrame( this->g_currentFrame, *frame );

		frame->ExtractPreviewRGB( pixels );
		GdkPixbuf *im = gdk_pixbuf_new_from_data
		                ( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
		                  frame->GetWidth(), frame->GetHeight(),
		                  frame->GetWidth() * 3, NULL, NULL );

		// resample pixel aspect
		{
			int width = frame->GetWidth();
			if ( frame->IsWide() )
				width = frame->IsPAL() ? 1024 : 854;
			else
				width = frame->IsPAL() ? 768 : 640;
			AspectRatioCalculator calc( width, frame->GetHeight(),
		                            frame->GetWidth(), frame->GetHeight(),
		                            frame->IsPAL(), frame->IsWide() );
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( im, calc.width, calc.height, GDK_INTERP_HYPER );
			g_object_unref( im );
			im = scaled;
		}

		const std::string FileName( file );
		const std::string suffix( FileName.begin() + FileName.rfind( "." ), FileName.end() );
		if ( suffix == ".png" )
			gdk_pixbuf_save( im, file, "png", &gerror, static_cast<gchar*>( NULL ) );
		else
			gdk_pixbuf_save( im, file, "jpeg", &gerror, "quality", "80", static_cast<gchar*>( NULL ) );
		if ( gerror != NULL )
		{
			modal_message( gerror->message );
			g_error_free( gerror );
		}
		g_object_unref( im );
		GetFramePool()->DoneWithFrame( frame );
	}
}


/** Relay the current command and any message associated to it.
 
  	\param cmd command
	\param msg message;
*/

void KinoCommon::keyboardFeedback( const char *cmd, const char *msg )
{
	char s[ 256 ];

	strcpy( s, cmd );
	strcat( s, "   " );
	strcat( s, msg );

	setStatusBar( s );
}

/** Move to the current frame. Corrects the frame specified to be within the
	range of the current play list and informs the currently selected page of the
	change.
 
	\param frame requested frame
	\return the frame actually moved to
*/

int KinoCommon::moveToFrame( )
{
	if ( currentPage == PAGE_TRIM )
	{
		getPageTrim() ->movedToFrame( getPageTrim() ->getPosition() );
		return getPageTrim() ->getPosition();
	}
	else if ( currentPage == PAGE_MAGICK )
	{
		return moveToFrame( g_currentFrame - playlist.FindStartOfScene( g_currentFrame ) );
	}
	return moveToFrame( g_currentFrame );
}

/** Move to the frame specified. Corrects the frame specified to be within the
	range of the current play list and informs the currently selected page of the
	change.
 
	\param frame requested frame
	\return the frame actually moved to
*/

int KinoCommon::moveToFrame( int frame )
{
	if ( currentPage == PAGE_TRIM || currentPage == PAGE_MAGICK )
	{
		getCurrentPage()->movedToFrame( frame );
		return frame;
	}

	if ( frame >= 0 && frame < getPlayList() ->GetNumFrames() )
		g_currentFrame = frame;
	else if ( frame >= getPlayList() ->GetNumFrames() )
		g_currentFrame = getPlayList() ->GetNumFrames() - 1;
	else if ( frame < 0 && getPlayList() ->GetNumFrames() > 0 )
		g_currentFrame = 0;
	else
		g_currentFrame = -1;

	getCurrentPage() ->movedToFrame( g_currentFrame );

	return g_currentFrame;
}

/** Move the frame relative to the number specified.
 
	\param frames requested frame offset
	\return the frame actually moved to
*/

int KinoCommon::moveByFrames( int frames )
{
	if ( frames == 0 )
		return g_currentFrame;
		
	int frame = g_currentFrame + frames;
	
	if ( currentPage == PAGE_TRIM )
		frame = getPageTrim() ->getPosition() + frames;

	return moveToFrame( frame );
}

/** Relay the current frame info to the GUI.
 
	\param i current frame
*/

void KinoCommon::showFrameInfo( int i )
{

	getCurrentPage() ->showFrameInfo( i );
}

/** Display detailed information about this frame and its file.
 
	\param i current frame
*/

void KinoCommon::showFrameMoreInfo( Frame &frame, FileHandler *media )
{
	if ( showMoreInfo )
	{
		GtkLabel *label = GTK_LABEL( lookup_widget( widget, "label_properties" ) );
		char* s = new char[ 2048 ];

		if ( media != NULL )
		{
			TimeCode tc;
			AudioInfo ainfo;
			string format;

			frame.GetTimeCode( tc );
			frame.GetAudioInfo( ainfo );

			string ext = media->GetExtension();
			if ( ext == ".dv" || ext == ".dif" )
				format = "Raw DV";
			else if ( ext == ".avi" )
			{
				AVIHandler * avi = dynamic_cast< AVIHandler* >( media );
				format = ( avi->GetFormat() == AVI_DV1_FORMAT ) ? "AVI, Type 1" : "AVI, Type 2";
				if ( avi->GetOpenDML() )
					format += ", OpenDML";
			}
			else if ( ext == ".mov" )
				format = "Quicktime";
			else
				format = "unknown";


			snprintf( s, 2048, _( "%s\n%s\n%2.2d:%2.2d:%2.2d:%2.2d\n%s\n%d bit, %d KHz, %d samples\n%d x %d, %s, %s, %2.2f fps" ),
			          basename( media->GetFilename().c_str() ),
			          frame.GetRecordingDate().c_str(),
			          tc.hour, tc.min, tc.sec, tc.frame,
			          format.c_str(),
			          ainfo.quantization, ainfo.frequency / 1000, ainfo.samples,
			          frame.GetWidth(), frame.GetHeight(),
			          frame.IsPAL() ? "PAL" : "NTSC",
			          frame.IsWide() ? "16:9" : "4:3",
			          frame.GetFrameRate()
			        );
		}
		else
			sprintf( s, "\n\n\n" );

		gtk_label_set_text( label, s );
		delete[] s;
	}
}

/** Trigger the start action of the current page. This method is also responsible
	for determining the state of the main page buttons and widgets.
*/

void KinoCommon::start()
{
	gtk_label_set_text( GTK_LABEL( lookup_widget( getWidget(), "position_label_current" ) ), "" );
	gtk_label_set_text( GTK_LABEL( lookup_widget( getWidget(), "position_label_total" ) ), "" );
	
	getCurrentPage() ->start();
	activateWidgets();

	toggleComponents( getComponentState(), false );
	if ( currentPage != PAGE_MAGICK )
		toggleComponents( VIDEO_STOP, true );
	commitComponentState();
}

/** Trigger the clean action of the current page.
*/

void KinoCommon::clean()
{
	getCurrentPage() ->clean();
}

/** Trigger the keyboard action of the current page.
*/

gboolean KinoCommon::processKeyboard( GdkEventKey *event )
{
	return getCurrentPage() ->processKeyboard( event );
}

/** Trigger a menu command action of the current page. These can be
	either keyboard or command line style strings.
*/

gboolean KinoCommon::processCommand( const char *cmd )
{
	return getCurrentPage() ->processCommand( cmd );
}

/** Trigger the select scene action of the current page.
*/

void KinoCommon::selectScene( int scene )
{
	getCurrentPage() ->selectScene( scene );
}

/** Trigger the start of movie action of the current page.
*/

void KinoCommon::videoStartOfMovie()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoStartOfMovie();

		commitComponentState();
	}
}

/** Trigger the previous scene action of the current page.
*/

void KinoCommon::videoPreviousScene()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoPreviousScene();

		commitComponentState();
	}
}

/** Trigger the start of scene action of the current page.
*/

void KinoCommon::videoStartOfScene()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoStartOfScene();

		commitComponentState();
	}
}

/** Trigger the rewind action of the current page.
*/

void KinoCommon::videoRewind()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoRewind();

		commitComponentState();
	}
}

/** Trigger the back action of the current page.
*/

void KinoCommon::videoBack(int step)
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoBack(step);

		commitComponentState();
	}
}

/** Trigger the play action of the current page.
*/

void KinoCommon::videoPlay()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoPlay();

		commitComponentState();
	}
}

/** Trigger the forward action of the current page.
*/

void KinoCommon::videoForward(int step)
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoForward(step);

		commitComponentState();
	}
}

/** Trigger the fast forward action of the current page.
*/

void KinoCommon::videoFastForward()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoFastForward();

		commitComponentState();
	}
}

/** Trigger the next scene action of the current page.
*/

void KinoCommon::videoNextScene()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoNextScene();

		commitComponentState();
	}
}

/** Trigger the end of scene action of the current page.
*/

void KinoCommon::videoEndOfScene()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoEndOfScene();

		commitComponentState();
	}
}

/** Trigger the end movie action of the current page.
*/

void KinoCommon::videoEndOfMovie()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoEndOfMovie();

		commitComponentState();
	}
}

/** Trigger the pause action of the current page.
*/

void KinoCommon::videoPause()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoPause();

		commitComponentState();
	}
}

/** Trigger the stop action of the current page.
*/

void KinoCommon::videoStop()
{
	if ( ! is_component_state_changing )
	{

		getCurrentPage() ->videoStop();

		commitComponentState();
	}
}

/** Bi-directional variable-speed playback
 
	\param angle A number from -15 (fastest reverse) to 15 (fastest forward)
*/

void KinoCommon::videoShuttle( int angle )
{
	if ( ! is_component_state_changing )
	{
		toggleComponents( getComponentState(), false );

		if ( ! Preferences::getInstance().dropFrame )
			// this has bad thread interaction with dropFrame playback
			gtk_range_set_value( this->video_shuttle, angle );
		getCurrentPage() ->videoShuttle( angle );

		commitComponentState();
	}
}

void KinoCommon::windowMoved()
{
	getCurrentPage() ->windowMoved();
}

void KinoCommon::visibilityChanged( gboolean visible )
{
	getCurrentPage() ->visibilityChanged( visible );
}

/** Activate or deactivate the widgets at the request of the previous page.
    This method is always called immediately after the current pages start method
	and can be called at the discretion of the page if required.
*/

void KinoCommon::activateWidgets()
{
	component_enum pattern = ( component_enum ) ( this->getCurrentPage()->activate() ^ this->getCurrentPage()->deactivate() );
	gtk_widget_set_sensitive( lookup_widget( widget, "undo" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "redo" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "copy_current_scene" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "cut_current_scene" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "paste_before_current_frame" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "split_scene" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "join_scenes" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_undo" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_redo" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_cut" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_copy" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_paste" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_split" ), pattern & EDIT_MENU );
	gtk_widget_set_sensitive( lookup_widget( widget, "button_join" ), pattern & EDIT_MENU );
	GetStoryboard() ->setSensitive( pattern & SCENE_LIST );
	gtk_widget_set_sensitive( GTK_WIDGET( video_start_movie_button ), pattern & VIDEO_START_OF_MOVIE );
	gtk_widget_set_sensitive( GTK_WIDGET( video_start_scene_button ), pattern & VIDEO_START_OF_SCENE );
	gtk_widget_set_sensitive( GTK_WIDGET( video_rewind_button ), pattern & VIDEO_REWIND );
	gtk_widget_set_sensitive( GTK_WIDGET( video_back_button ), pattern & VIDEO_BACK );
	gtk_widget_set_sensitive( GTK_WIDGET( video_play_button ), pattern & VIDEO_PLAY );
	gtk_widget_set_sensitive( GTK_WIDGET( video_stop_button ), pattern & VIDEO_STOP );
	gtk_widget_set_sensitive( GTK_WIDGET( video_forward_button ), pattern & VIDEO_FORWARD );
	gtk_widget_set_sensitive( GTK_WIDGET( video_fast_forward_button ), pattern & VIDEO_FAST_FORWARD );
	gtk_widget_set_sensitive( GTK_WIDGET( video_end_scene_button ), pattern & VIDEO_NEXT_SCENE );
	gtk_widget_set_sensitive( GTK_WIDGET( video_end_movie_button ), pattern & VIDEO_END_OF_MOVIE );
	gtk_widget_set_sensitive( GTK_WIDGET( video_shuttle ), pattern & VIDEO_SHUTTLE );
}


/** Set the state of toggle buttons.
 
	The state of the buttons are retained in memory, and
	this method will set a flag that the state has changed (dirty),
	but has not yet been committed. The button commands will
	not respond while the state is dirty. Use commitComponentState()
	to commit the changes and let the buttons issue their commands once again.
 
    \param pattern A set of component_enums to toggle
    \param state If true then lower the button, else raise the button
*/

void KinoCommon::toggleComponents( component_enum pattern, bool state )
{

	this->is_component_state_changing = true;

	if ( state )
		this->component_state |= pattern;
	else
		this->component_state ^= pattern;

	if ( pattern & VIDEO_START_OF_MOVIE )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_start_movie_button ), state );
	if ( pattern & VIDEO_START_OF_SCENE )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_start_scene_button ), state );
	if ( pattern & VIDEO_REWIND )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_rewind_button ), state );
	if ( pattern & VIDEO_BACK )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_back_button ), state );
	if ( pattern & VIDEO_PLAY )
	{
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_play_button ), state );
		if ( state )
			gtk_image_set_from_file( GTK_IMAGE( lookup_widget( widget, "pixmap_play_pause" ) ), DATADIR "/kino/stock_media-pause-16.png" );
		else
			gtk_image_set_from_file( GTK_IMAGE( lookup_widget( widget, "pixmap_play_pause" ) ), DATADIR "/kino/stock_media-play-16.png" );
	}
	if ( pattern & VIDEO_STOP )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_stop_button ), state );
	if ( pattern & VIDEO_FORWARD )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_forward_button ), state );
	if ( pattern & VIDEO_FAST_FORWARD )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_fast_forward_button ), state );
	if ( pattern & VIDEO_NEXT_SCENE )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_end_scene_button ), state );
	if ( pattern & VIDEO_END_OF_MOVIE )
		gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( video_end_movie_button ), state );

}

/** Get the current component state data
*/
component_enum KinoCommon::getComponentState()
{
	return ( component_enum ) this->component_state;
}

/** Set the component state data and commit it.
 
    You can set component state at the same time too.
    See the toggleComponents() method.
	
    \param pattern A set of component_enums to set true
*/
void KinoCommon::commitComponentState( component_enum pattern )
{
	this->component_state |= pattern;
	this->is_component_state_changing = false;
}


/** Resize the video preview area to a percentage of source image size.
 
    \param factor The scaling factor from 0 to 1. If zero, then auto-size.
                  If negative, then cycle to the next preset factor.
    \param noWarning A boolean to indicate whether to suppress the warning
                     dialog when Kino is unable to determine video size.
*/

void KinoCommon::setPreviewSize( float factor, bool noWarning )
{
	static bool skip = false;
	int width = 720;
	int height = -1;
	bool isWide;
	bool isPAL;

	if ( skip )
	    return;
	
	// handle the cycle case
	if ( factor < 0 )
	{
		factor = float( Preferences::getInstance().previewSize ) / 10.0;
		factor += 0.5;
		if ( factor > 1.0 )
			factor = 0.0;
	}
		
	// save the new size
	Preferences::getInstance().previewSize = int( factor * 10 );

	// handle fixed size factors
	if ( Preferences::getInstance().displayFixed != ( factor > 0 ) )
	{
		Preferences::getInstance().displayFixed = ( factor > 0 );
		packIt( "packer_edit", "packer_edit_outer" );
		packIt( "packer_capture", "packer_capture_outer" );
		packIt( "packer_trim", "packer_trim_outer" );
	}

	skip = true;
	// set the menu radio item
	if ( factor == 0 )
		gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( widget, "menuitem_zoom_fit" ) ), TRUE );
	else if ( factor == 0.5 )
		gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( widget, "menuitem_zoom_50percent" ) ), TRUE );
	else
		gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( widget, "menuitem_zoom_100percent" ) ), TRUE );
	skip = false;

	// this sets a sane factor for zoom to fit
	if ( factor == 0 )
		factor = 0.5;

	if ( currentPage == PAGE_CAPTURE )
	{
		Frame *frame = getPageCapture()->getFrame();
		if ( frame != NULL )
		{
			if ( frame->IsWide() )
				width = frame->IsPAL() ? 1024 : 854;
			AspectRatioCalculator calc( width, frame->GetHeight(),
		                            frame->GetWidth(), frame->GetHeight(),
		                            frame->IsPAL(), frame->IsWide() );

			width = ( int ) ( calc.width * factor );
			height = ( int ) ( calc.height * factor );
		}
	}
	else if ( this->g_currentFrame != -1 )
	{
		Frame& frame = *GetFramePool()->GetFrame();
		this->getPlayList() ->GetFrame( this->g_currentFrame, frame );
		if ( frame.IsWide() )
			width = frame.IsPAL() ? 1024 : 854;
		else
			width = frame.IsPAL() ? 768 : 640;
		AspectRatioCalculator calc( width, frame.GetHeight(),
		                            frame.GetWidth(), frame.GetHeight(),
		                            frame.IsPAL(), frame.IsWide() );
		GetFramePool()->DoneWithFrame( &frame );

		width = ( int ) ( calc.width * factor );
		height = ( int ) ( calc.height * factor );
	}
	else if ( Preferences::getInstance().defaultNormalisation != NORM_UNSPECIFIED )
	{
		height = Preferences::getInstance().defaultNormalisation == NORM_PAL ? 576 : 480;
		isPAL = Preferences::getInstance().defaultNormalisation == NORM_PAL;
		isWide = Preferences::getInstance().defaultAspect == ASPECT_169;
		AspectRatioCalculator calc( isWide ? (isPAL ? 1024 : 854) : (isPAL ? 768 : 640),
		                            height, width, height,
		                            isPAL, isWide );

		width = ( int ) ( calc.width * factor );
		height = ( int ) ( calc.height * factor );
	}

	if ( height == -1 && ! noWarning )
	{
		modal_message( _( "The project is empty and the default preferences for video creation have not been specified." ) );
	}
	else if ( height != -1 )
	{
		cerr << "> setting video preview size to " << width << "x" << height << endl;
		GtkWidget *frameArea = lookup_widget( getWidget(), "packer_edit" );
		gtk_widget_set_size_request( frameArea, width + 4, height + 4 );
		frameArea = lookup_widget( getWidget(), "packer_capture" );
		gtk_widget_set_size_request( frameArea, width + 4, height + 4 );
		frameArea = lookup_widget( getWidget(), "packer_trim" );
		gtk_widget_set_size_request( frameArea, width + 4, height + 4 );
	}
}

/** Load the splash image into the video preview area.
 
    \param widget the GtkDrawingArea into which to render the image.
*/
void KinoCommon::loadSplash( GtkDrawingArea *drawable )
{
	if ( drawable && ( ( GtkWidget* ) drawable )->window )
	{
		GdkGC *gc = gdk_gc_new( ( ( GtkWidget* ) drawable ) ->window );
		if ( gc )
		{
			GdkPixbuf *splash = create_pixbuf( "about.jpeg" );
		
			if ( splash )
			{
				int width = ( ( GtkWidget* ) drawable ) ->allocation.width;
				int height = ( ( GtkWidget* ) drawable ) ->allocation.height;
				GdkPixbuf *image = gdk_pixbuf_scale_simple( splash, width, height, GDK_INTERP_BILINEAR );
				if ( image )
				{
					gdk_draw_pixbuf( ( ( GtkWidget * ) drawable ) ->window, gc, image,
									0, 0, 0, 0, -1, -1, GDK_RGB_DITHER_NORMAL, 0, 0 );
					g_object_unref( image );
				}
				g_object_unref( splash );
			}
			g_object_unref( gc );
		}
	}
}


/** Make the video preview area black.
 
    \param widget the GtkDrawingArea to clear.
*/
void KinoCommon::clearPreview( GtkDrawingArea *drawable )
{
	GtkWidget * widget = GTK_WIDGET( drawable );
	if ( widget->window && GDK_IS_DRAWABLE( widget->window ) )
	{
		gdk_draw_rectangle ( widget->window, widget->style->black_gc, TRUE,
		                     widget->allocation.x, widget->allocation.y,
		                     widget->allocation.width, widget->allocation.height );
	}
}


/** Set the video display to be fixed size or scalable.
 
    \param packerNameInner The name of the GTK AspectFrame widget to adjust.
    \param packerNameOuter The name of the GTK AspectFrame widget to adjust.
*/
void KinoCommon::packIt( const char *packerNameInner, const char* packerNameOuter )
{
	GdkColor color;
	gdk_color_parse ( "black", &color );

	GtkWidget *packer = lookup_widget( getWidget(), packerNameOuter );
	gtk_widget_modify_bg ( packer, GTK_STATE_NORMAL, &color );

	packer = lookup_widget( getWidget(), packerNameInner );
	gtk_widget_modify_bg ( packer, GTK_STATE_NORMAL, &color );

	GtkWidget *parent = gtk_widget_get_parent( packer );
	gtk_box_set_child_packing( GTK_BOX( parent ), packer,
	                           TRUE /* expand */,
	                           Preferences::getInstance().displayFixed ? FALSE : TRUE /* fill */,
	                           0 /* padding */, GTK_PACK_START );
}


/** Helper function for the pages */
void KinoCommon::setStatusBar( const char * msg, ... )
{
	va_list list;
	va_start( list, msg );
	static char prevMsg[ 1024 ] = "";

	if ( strcmp( msg, "" ) == 0 )
		gtk_statusbar_pop( statusbar, 1 );
	else if ( strncmp( prevMsg, msg, 1023 ) != 0 )
	{
		if ( vsnprintf( prevMsg, 1023, msg, list ) != 0 )
		{
			gtk_statusbar_pop( statusbar, 1 );
			gtk_statusbar_push( statusbar, 1, prevMsg );
		}
	}
}


bool KinoCommon::exitKino( )
{
	return newFile( false, true );
}

void KinoCommon::setTimeFormat( SMIL::Time::TimeFormat format )
{
	Preferences::getInstance().timeFormat = static_cast< int >( format );
	if ( g_currentFrame != -1 )
	{
		common->moveToFrame();
		GetStoryboard() ->reset();
		GetStoryboard() ->redraw();
		getCurrentPage() ->timeFormatChanged();
	}
}


void KinoCommon::setCurrentScene( int frame )
{
	if ( frame > -1 )
	{
		int pos = 0;

		vector <int> scenes = getPageEditor() ->GetScene();
		for ( pos = 0; pos < ( int ) scenes.size() && frame >= scenes[ pos ]; pos++ )
			;
		if ( pos != currentScene && pos > -1 )
		{
			currentScene = pos;
			GetStoryboard() ->select( currentScene );
		}
	}
}

void KinoCommon::publishPlayList()
{
	std::ostringstream command;
	GError *gerror = NULL;

	if ( playlist.GetDocName() == "" )
	{
		PlayList* copy = new PlayList( playlist );
		char filename[] = "/tmp/kino.XXXXXX";
		mkstemp( filename );
		copy->SavePlayList( filename );
		delete copy;
		command << "\"" << DATADIR << "/kino/scripts/publish/project.sh\" ";
		command << "\"" << filename << "\" ";
		command << "\"" << playlist.GetDocId() << "\" ";
		command << "\"" << playlist.GetDocTitle() << "\" ";
		command << std::ends;
		const char * args[ 4 ];
		args[ 0 ] = "/bin/sh";
		args[ 1 ] = "-c";
		args[ 2 ] = command.str().c_str();
		args[ 3 ] = NULL;
		g_spawn_sync( ".", (gchar**) args, NULL, (GSpawnFlags)0, NULL, NULL, NULL, NULL, NULL, &gerror );
		unlink( filename );
	}
	else
	{
		savePlayList();
		command << "\"" << DATADIR << "/kino/scripts/publish/project.sh\" ";
		command << "\"" << playlist.GetDocName() << "\" ";
		command << "\"" << playlist.GetDocId() << "\" ";
		command << "\"" << playlist.GetDocTitle() << "\" ";
		command << std::ends;
		const char * args[ 4 ];
		args[ 0 ] = "/bin/sh";
		args[ 1 ] = "-c";
		args[ 2 ] = command.str().c_str();
		args[ 3 ] = NULL;
		g_spawn_async_with_pipes( ".", (gchar**) args, NULL, (GSpawnFlags)0, NULL, NULL, NULL, NULL, NULL, NULL, &gerror );
	}
}

void KinoCommon::publishFrame()
{
	if ( currentPage == PAGE_CAPTURE )
	{
		modal_message( _("Sorry, you can not publish still frames from Capture.") );
	}
	else
	{
		char * filename = common->getFileToSave( _( "Save Still Frame" ) );
		if ( strcmp( filename, "" ) )
		{
			std::ostringstream command;
			GError *gerror = NULL;
			
			common->saveFrame( common->g_currentFrame, filename );
			
			command << "\"" << DATADIR << "/kino/scripts/publish/frame.sh\" ";
			command << "\"" << filename << "\" ";
			command << "\"" << getPlayList()->GetDocId() << "\" ";
			command << "\"" << getPlayList()->GetSeqAttribute( g_currentFrame, "title" ) << "\" ";
			command << "\"" << getPlayList()->GetDocTitle() << "\" ";
			command << std::ends;
			const char * args[ 4 ];
			args[ 0 ] = "/bin/sh";
			args[ 1 ] = "-c";
			args[ 2 ] = command.str().c_str();
			args[ 3 ] = NULL;
			g_spawn_async_with_pipes( ".", (gchar**) args, NULL, (GSpawnFlags)0, NULL, NULL, NULL, NULL, NULL, NULL, &gerror );
		}
	}
	
}

void KinoCommon::loadPlayList( char* filename )
{
	if ( newFile( false ) )
	{
		GetEditorBackup() ->Clear();
		if ( loadPlayList( filename, 0 ) )
		{
			g_currentFrame = 0;
			hasListChanged = TRUE;
			getPageEditor( ) ->snapshot( );
			getPlayList( ) ->SetDocName( filename );
			getPlayList( ) ->SetDirty( false );
			fetchProjectMetadata( getPlayList()->GetDocId() );
		}
	}
	setWindowTitle();
	this->currentScene = 0;
	this->getCurrentPage()->clean();
	this->getCurrentPage()->start();
	moveToFrame( );
	setLastDirectory( directory_utils::get_directory_from_file( filename ) );
}


static void on_open_recent_activate ( GtkWidget *menuitem, gpointer user_data )
{
	common->loadPlayList( static_cast< char* >( user_data ) );
}

void KinoCommon::updateRecentFiles()
{
	GtkWidget* menu = lookup_widget( widget, "file_menu" );
	vector< std::string >& recentFiles = Preferences::getInstance().recentFiles;
	int count = recentFiles.size();
	const int menuPosition = Preferences::getInstance().enablePublish ? 12 : 10;

	if ( recentMenuItems.empty() && !recentFiles.empty() )
	{
		GtkWidget* widget = gtk_separator_menu_item_new();
		gtk_menu_shell_insert( GTK_MENU_SHELL( menu ), widget, menuPosition );
		gtk_widget_show( widget );
	}
	for ( vector< GtkWidget* >::iterator i = recentMenuItems.begin(); i != recentMenuItems.end(); i++ )
	{
		gtk_container_remove( GTK_CONTAINER( menu ), *i );
		gtk_widget_destroy( *i );
	}
	recentMenuItems.clear();
	for ( vector< std::string >::iterator i = recentFiles.begin(); i != recentFiles.end(); i++, count-- )
	{
		std::ostringstream ss;
		ss << "_" << count << ". "
			<< StringUtils::replaceAll( basename( ( *i ).c_str() ), "_", "__" )
			<< std::ends;
		GtkWidget* widget = gtk_menu_item_new_with_mnemonic( ss.str().c_str() );
		g_signal_connect( G_OBJECT( widget ), "activate", G_CALLBACK( on_open_recent_activate ), gpointer( (*i).c_str() ) );
		gtk_menu_shell_insert( GTK_MENU_SHELL( menu ), widget, menuPosition );
		gtk_widget_show( widget );
		recentMenuItems.push_back( widget );
	}
}

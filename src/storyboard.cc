/*
* storyboard.cc Storyboard view object
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <vector>
#include <string>

#include <pthread.h>

#include "storyboard.h"
#include "frame.h"
#include "preferences.h"
#include "error.h"
#include "page_editor.h"
#include "commands.h"
#include "stringutils.h"

extern "C"
{
#include "support.h"
#include "cell-renderers/mg-popup-entry.h"
#include "cell-renderers/mg-cell-renderer-popup.h"
#include "cell-renderers/mg-cell-renderer-list.h"

	extern KinoCommon *common;
	extern struct navigate_control g_nav_ctl;
}

enum SceneStatus {
    SCENE_INIT,
    SCENE_IDLE,
    SCENE_STARTING,
    SCENE_RUNNING,
    SCENE_RESTART
};

enum
{
    COLUMN_THUMBNAIL = 0,
    COLUMN_NAME,
    COLUMN_NAME_MODE,
    COLUMN_VALUE,
    COLUMN_VALUE_MODE,
    N_COLUMNS
};

enum SceneStatus showScenesRunning = SCENE_INIT;

static std::vector< GdkPixbuf * > *backup = NULL;
static pthread_mutex_t scene_mutex = PTHREAD_MUTEX_INITIALIZER;


static int getSceneFromPath( gchar *path )
{
	std::vector< int > scenes = common->getPageEditor() ->GetScene();
	int i = atoi( path );
	if ( i > ( int ) scenes.size() )
		i = scenes.size() - 1;
	return ( i <= 0 ) ? 0 : scenes[ i - 1 ];
}

static void removeImages( std::vector< GdkPixbuf * >** list )
{
	if ( *list != NULL )
	{
		std::vector< GdkPixbuf * >::iterator iter;
		for ( iter = ( *list ) ->begin(); iter != ( *list ) ->end(); iter++ )
		{
			if ( *iter != NULL )
				g_object_unref( *iter );
		}
		( *list ) ->erase( ( *list ) ->begin(), ( *list ) ->end() );
		delete ( *list );
		*list = NULL;
	}
}

static void tree_model_row_inserted_cb( GtkTreeModel* model, GtkTreePath* path, GtkTreeIter* iter, gpointer data )
{
	Storyboard * storyboard = ( Storyboard* ) ( data );
	//std::cerr << "tree_model_row_inserted_cb skip " << storyboard->getSkip() << std::endl;
	if ( ! storyboard->getSkip() )
	{
		gchar * row = gtk_tree_path_to_string( path );
		int row_num = atoi( row );
		//std::cerr << "tree_model_row_inserted_cb row " << row << std::endl;
		if ( strchr( row, ':' ) != NULL )
			row_num++;
		storyboard->setSkip();
		storyboard->moveScene( row_num );
		g_free( row );
	}
}

static gboolean tree_view_row_select( GtkWidget *widget, GdkEventButton *event, gpointer data )
{
	GtkTreeView * treeview = GTK_TREE_VIEW( widget );
	GtkTreeSelection *selection = gtk_tree_view_get_selection( treeview );

	GtkTreeIter iter;
	GtkTreeModel *model;

	if ( gtk_tree_selection_get_selected ( selection, &model, &iter ) )
	{
		gchar * row = gtk_tree_model_get_string_from_iter( model, &iter );
		if ( strchr( row, ':' ) )
		{
			GtkTreeViewColumn* column;
			GtkTreePath *path;
			gtk_tree_view_get_cursor( treeview, &path, &column );
			if ( path && column )
				gtk_tree_view_set_cursor( treeview, path, column, TRUE);
		}
		else
		{
			common->selectScene( atoi( row ) );
		}
		g_free( row );
	}

	return FALSE;
}

// Double click expands/collapses
static void tree_view_row_activated( GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *col, gpointer data )
{
	// Reserve this action for future use.
#if 0
	if ( gtk_tree_view_row_expanded( treeview, path ) )
		gtk_tree_view_collapse_row( treeview, path );
	else
		gtk_tree_view_expand_row( treeview, path, FALSE );
#endif
}


static void
on_name_start_editing( MgCellRendererPopup *cell )
{
	g_nav_ctl.escaped = TRUE;
}

static void
on_name_show_popup( MgCellRendererList *cell,
				  const gchar        *path_string,
				  gint                x1,
				  gint                y1,
				  gint                x2,
				  gint                y2,
				  GtkTreeModel       *model)
{
	GtkTreePath  *path = gtk_tree_path_new_from_string( path_string );
	GtkTreeIter   iter;
	vector< std::string > metaNames;
	vector< std::string >::iterator metaNamesIter;
	GList *list = NULL;
	int currentScene = getSceneFromPath( const_cast< char* >( path_string ) );

	gtk_tree_model_get_iter (model, &iter, path);
	StringUtils::split( Preferences::getInstance().metaNames, ",", metaNames );

	for ( metaNamesIter = metaNames.begin(); metaNamesIter != metaNames.end(); ++metaNamesIter )
	{
		std::string metaname = *metaNamesIter;
		if ( metaname.at( 0 ) != '*' )
		{
			// Only add the name if it does not yet exist
			if ( common->getPlayList()->GetSeqAttribute( currentScene, metaname.c_str() ) == "" )
				list = g_list_append( list, strdup( metaname.c_str() ) );
		}
	}
	cell->list = list;
	cell->selected_index = 0;
	
	gtk_tree_path_free( path );
}

static void
on_name_edited( MgCellRendererList *cell,
	gchar               *path_string,
	gchar               *new_text,
	GtkTreeStore        *model )
{
	GtkTreePath  *path = gtk_tree_path_new_from_string( path_string );
	GtkTreeIter   iter;
	gchar        *parentPathString = path_string;
	GtkTreePath  *parentPath = NULL;
	GtkTreeIter   parent;
	vector< std::string > metaNames;
	vector< std::string >::iterator metaNamesIter;
	int currentScene = getSceneFromPath( const_cast< char* >( path_string ) );
	int i = 0;

	path_string = strrchr( path_string, ':' );
	if ( path_string )
	{
		*path_string = 0;
		parentPath = gtk_tree_path_new_from_string( parentPathString );
		gtk_tree_model_get_iter( GTK_TREE_MODEL( model ), &parent, parentPath );
	}
	gtk_tree_model_get_iter( GTK_TREE_MODEL( model ), &iter, path );
	
	StringUtils::split( Preferences::getInstance().metaNames, ",", metaNames );


	for ( GList *l = cell->list; l; l = l->next )
	{
		if ( cell->selected_index == i++ )
		{
			// Get the existing name
			const char *name = mg_popup_entry_get_text( MG_POPUP_ENTRY( cell->parent.editable ) );
			
			// Update the model
			gtk_tree_store_set( model, &iter, COLUMN_NAME, (gchar*) l->data, COLUMN_VALUE, "-", -1 );

			// Refresh the view
			gtk_tree_model_row_changed( GTK_TREE_MODEL( model ), path, &iter );

			if ( name[0] != '(' )
			{
				// Clear the old xml attribute
				common->getPlayList()->SetSeqAttribute( currentScene, name, "" );
			}
			else
			{
				// Set the new xml attribute name, and value = ""
				common->getPlayList()->SetSeqAttribute( currentScene, (char*) l->data, "-" );

				// See if there is an unset metadata item
				for ( metaNamesIter = metaNames.begin(); metaNamesIter != metaNames.end(); ++metaNamesIter )
				{
					std::string metaname = *metaNamesIter;
					if ( metaname.at( 0 ) == '*' )
						metaname.erase( 0,1 );
						
					// There is
					if ( common->getPlayList()->GetSeqAttribute( currentScene, metaname.c_str() ) == "" )
					{
						GetStoryboard()->setSkip();
						// Add an extra metadata row to let user add attributes
						gtk_tree_store_append( model, &iter, &parent );
						gtk_tree_store_set( model, &iter,
				                    COLUMN_NAME, _("(new)"),
				                    COLUMN_NAME_MODE, GTK_CELL_RENDERER_MODE_EDITABLE,
				                    COLUMN_VALUE, "",
				                    COLUMN_VALUE_MODE, GTK_CELL_RENDERER_MODE_EDITABLE,
				                    -1 );
						GetStoryboard()->clearSkip();
						break;
					}
				}
			}

			break;
		}
	}

	g_nav_ctl.escaped = FALSE;

	if ( path )
		gtk_tree_path_free( path );
	if ( parentPath )
		gtk_tree_path_free( parentPath );
}

static void
on_value_start_editing( MgCellRendererPopup *cell )
{
	g_nav_ctl.escaped = TRUE;
}

static void
on_value_show_popup( MgCellRendererList *cell,
				  const gchar        *path_string,
				  gint                x1,
				  gint                y1,
				  gint                x2,
				  gint                y2,
				  GtkTreeStore       *model)
{
	GtkTreePath  *path = gtk_tree_path_new_from_string( path_string );
	GtkTreeIter   iter;
	GValue        name;
	gchar        *name_string;
	vector< std::pair< std::string, std::string > > metaValues;
	vector< std::pair< std::string, std::string > >::iterator metaValuesIter;
	GList *list = NULL;

    // Get the attribute name
	gtk_tree_model_get_iter( GTK_TREE_MODEL( model ), &iter, path );
	memset( &name, 0, sizeof( name ) );
	gtk_tree_model_get_value( GTK_TREE_MODEL( model ), &iter, COLUMN_NAME, &name );
	name_string = const_cast< char* >( g_value_get_string( &name ) );

	// Put non-* values into the popup list
	metaValues = Preferences::getInstance().metaValues[ name_string ];
	for ( metaValuesIter = metaValues.begin(); metaValuesIter != metaValues.end(); ++metaValuesIter )
	{
		if ( metaValuesIter->first != "*" )
			list = g_list_append( list, strdup( metaValuesIter->first.c_str() ) );
	}
	cell->list = list;
	cell->selected_index = 0;

	if ( path )
		gtk_tree_path_free( path );
	g_value_unset( &name );
}

static void
on_value_edited( MgCellRendererList *cell,
	gchar               *path_string,
	gchar               *new_text,
	GtkTreeStore        *model )
{
	GtkTreePath  *path = gtk_tree_path_new_from_string( path_string );
	GtkTreeIter   iter;
	GValue        name;
	gchar        *name_string;

	// Get the attribute name
	gtk_tree_model_get_iter( GTK_TREE_MODEL( model ), &iter, path );
	memset( &name, 0, sizeof( name ) );
	gtk_tree_model_get_value( GTK_TREE_MODEL( model ), &iter, COLUMN_NAME, &name );
	name_string = const_cast< char* >( g_value_get_string( &name ) );

	if ( name_string && name_string[0] != '(' )
	{
		if ( Preferences::getInstance().metaValues.find( name_string ) != Preferences::getInstance().metaValues.end() )
		{
			vector< std::pair< std::string, std::string > >& metaValues = Preferences::getInstance().metaValues[ name_string ];
			vector< std::pair< std::string, std::string > >::iterator metaValuesIter;
			char *value = NULL;
			char *label = NULL;
		
			// See if values range is open-ended (* in metaValues)
			for ( metaValuesIter = metaValues.begin(); metaValuesIter != metaValues.end() && metaValuesIter->first != "*"; ++metaValuesIter );

			if ( metaValuesIter != metaValues.end() && MG_CELL_RENDERER_POPUP( cell )->shown == FALSE )
			{
				// Get the value from the entry
				label = value = const_cast< char* >( mg_popup_entry_get_text( MG_POPUP_ENTRY( cell->parent.editable ) ) );

				// See if a hidden value exists for the label value
				for ( metaValuesIter = metaValues.begin(); metaValuesIter != metaValues.end(); ++metaValuesIter )
				{
					if ( metaValuesIter->first == label )
					{
						value = const_cast< char* >( metaValuesIter->second.c_str() );
						break;
					}
				}
			}
			else if ( MG_CELL_RENDERER_POPUP( cell )->shown == TRUE )
			{
				int i = 0;
				
				// Get the value from the popup list
				for ( GList *l = cell->list; l; l = l->next )
				{
					if ( cell->selected_index == i++ )
					{
						label = (char*) l->data;
						break;
					}
				}

				// See if a hidden value exists for the label value
				for ( metaValuesIter = metaValues.begin(); metaValuesIter != metaValues.end(); ++metaValuesIter )
				{
					if ( metaValuesIter->first == label )
					{
						value = const_cast< char* >( metaValuesIter->second.c_str() );
						break;
					}
				}
			}

			if ( value )
			{
				// Update the model
				gtk_tree_store_set( model, &iter, COLUMN_VALUE, label, -1 );

				// Refresh the view
				gtk_tree_model_row_changed( GTK_TREE_MODEL( model ), path, &iter );

				// Set the old xml attribute
				common->getPlayList()->SetSeqAttribute( getSceneFromPath( const_cast< char* >( path_string ) ), name_string, value );
			}
		}
	}
    
	g_nav_ctl.escaped = FALSE;

	if ( path )
		gtk_tree_path_free( path );
	g_value_unset( &name );
}

static gboolean scroll_handler( gpointer data )
{
	GtkTreeView* view = GTK_TREE_VIEW( data );
	GtkTreeSelection* selection = gtk_tree_view_get_selection( view );
	GtkTreeIter iter;
	GtkTreeModel* model;
	if ( gtk_tree_selection_get_selected( selection, &model, &iter ) == TRUE )
	{
		GtkTreePath* path = gtk_tree_model_get_path( model, &iter );
		gtk_tree_view_scroll_to_cell( view, path, NULL, TRUE, 0.5, 0.0 );
		gtk_tree_path_free( path );
	}
	return FALSE;
}

static gboolean expansion_handler( gpointer data )
{
	GtkTreeView* view = GTK_TREE_VIEW( data );
	gtk_tree_view_expand_all( view );
	
	GdkWindow* win = gtk_tree_view_get_bin_window( view );
	GdkRectangle rect;
	gtk_tree_view_get_visible_rect( view, &rect );
	gtk_tree_view_tree_to_widget_coords( view, rect.x, rect.y, &rect.x, &rect.y );
	gdk_window_invalidate_rect( win, &rect, TRUE );

	g_idle_add( scroll_handler, data );
	
	return FALSE;
}

Storyboard::Storyboard( KinoCommon *common ) :
		common( common ), skipSelect( false ), skip( false )
{
	GtkCellRenderer* renderer;
	GtkTreeViewColumn* column;
	GtkTreeSelection* select;

	// Create the tree view
	view = GTK_TREE_VIEW( lookup_widget( common->getWidget(), "treeview_storyboard" ) );
	g_signal_connect( G_OBJECT( view ), "button-release-event", G_CALLBACK( tree_view_row_select ), this );
	g_signal_connect( G_OBJECT( view ), "row-activated", G_CALLBACK( tree_view_row_activated ), this );
	gtk_tree_view_set_enable_search( view, FALSE );

	// Store the current scene
	selection = common->getCurrentScene();

	// Create the model and connect it to the view
	model = gtk_tree_store_new( N_COLUMNS, G_TYPE_OBJECT, G_TYPE_STRING,
		G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT );
	gtk_tree_view_set_model( view, GTK_TREE_MODEL( model ) );
	g_signal_connect( G_OBJECT( model ), "row-inserted",
	                  G_CALLBACK( tree_model_row_inserted_cb ), static_cast<gpointer>( this ) );

	// Add the thumbnail column
	renderer = gtk_cell_renderer_pixbuf_new();
	column = gtk_tree_view_column_new_with_attributes(
	             _( "Storyboard" ), renderer, "pixbuf", COLUMN_THUMBNAIL, NULL );
	gtk_tree_view_column_set_fixed_width( column, 100 );
	gtk_tree_view_column_set_min_width( column, 100 );
	gtk_tree_view_column_set_max_width( column, 100 );
	gtk_tree_view_append_column( view, column );
	gtk_tree_view_set_expander_column( view, column );

	// Add the meta name column
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title( column, _("Property") );
	gtk_tree_view_column_set_resizable( column, TRUE );
	gtk_tree_view_append_column( view, column );
	
	renderer = mg_cell_renderer_list_new();
	g_object_set( G_OBJECT( renderer ), "editable", TRUE, NULL );
	gtk_tree_view_column_pack_start( column, renderer, TRUE );
	gtk_tree_view_column_set_attributes( column, renderer,
				"text", COLUMN_NAME,
				"mode", COLUMN_NAME_MODE,
				NULL );

	g_signal_connect( G_OBJECT( renderer ),
				"start-editing",
				G_CALLBACK( on_name_start_editing ),
				NULL );
	g_signal_connect( G_OBJECT( renderer ),
				"show_popup",
				G_CALLBACK( on_name_show_popup ),
				model );
	g_signal_connect( G_OBJECT( renderer ),
				"edited",
				G_CALLBACK( on_name_edited ),
				model );
	
	// Add the meta value column
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title( column, _("Value") );
	gtk_tree_view_column_set_resizable( column, FALSE );
	gtk_tree_view_append_column( view, column );
	
	renderer = mg_cell_renderer_list_new();
	g_object_set( G_OBJECT( renderer ), "editable", TRUE, NULL );

	gtk_tree_view_column_pack_start( column, renderer, TRUE );
	gtk_tree_view_column_set_attributes( column, renderer,
				"text", COLUMN_VALUE,
				"mode", COLUMN_VALUE_MODE,
				NULL );

	g_signal_connect( G_OBJECT( renderer ),
				"start-editing",
				G_CALLBACK( on_value_start_editing ),
				NULL );
	g_signal_connect( G_OBJECT( renderer ),
				"show_popup",
				G_CALLBACK( on_value_show_popup ),
				model );
	g_signal_connect( G_OBJECT( renderer ),
				"edited",
				G_CALLBACK( on_value_edited ),
				model );

	select = gtk_tree_view_get_selection( view );
	gtk_tree_selection_set_mode( select, GTK_SELECTION_SINGLE );
	
	g_object_unref( G_OBJECT( model ) ); // view has the reference
}

Storyboard::~Storyboard()
{}

void *showScenesThread( void *arg )
{
	Frame & frame = *( GetFramePool( ) ->GetFrame( ) );
	GdkPixbuf *image = NULL;
	GdkPixbuf *thumbnail = NULL;
	static std::vector< GdkPixbuf * > *icons = NULL;
	std::vector< int > scene;
	int frameNum = 0;
	Storyboard *storyboard = static_cast<Storyboard *>( arg );
	GtkTreeIter iter, child;
	unsigned char *pixels = new unsigned char[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];

	frame.decoder->quality = DV_QUALITY_DC;
	if ( Preferences::getInstance().displayQuality < 4 )
		frame.decoder->quality |= DV_QUALITY_COLOR;


	while ( showScenesRunning != SCENE_IDLE )
	{
		// Apparently(?) the thumbs generated cannot be removed while they're used in the
		// icon list - this doesn't appear to be the case for the timeline page, but is
		// definitely causing problems here - could be the modified images causing an issue
		// perhaps? Anyway, to avoid a blank list during the execution of this thread, take
		// a backup of the previous list and create a new one (see comments below).

		icons = new std::vector< GdkPixbuf *>;

		// Change from SCENE_STARTING to SCENE_RUNNING
		showScenesRunning = SCENE_RUNNING;

		// Get the scene list (contains the frame number first frame of each scene)
		scene = common->getPageEditor() ->GetScene();

		// Create the icons vector but terminate if the running state changes
		for ( unsigned int i = 0; showScenesRunning == SCENE_RUNNING && i < scene.size(); ++i )
		{

			frameNum = i == 0 ? 0 : scene[ i - 1 ];

			fail_neg( common->getPlayList() ->GetFrame( frameNum, frame ) );

			frame.ExtractHeader();
			frame.ExtractRGB( pixels );

			// Get the image
			image = gdk_pixbuf_new_from_data( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
			                                  frame.GetWidth(), frame.GetHeight(), frame.GetWidth() * 3,
			                                  NULL, NULL );

			// create the thumbnail
			thumbnail = gdk_pixbuf_scale_simple( image, 80, frame.IsWide() ? 45 : 60, GDK_INTERP_NEAREST );
			icons->push_back( thumbnail );
			g_object_unref( image );
		}

		// Render the icons if we're still running, otherwise remove the [partial] list generated
		// NB: The freeze followed by a thread_leave causes the icon list to be blanked on the screen and
		// removing the current list before clearing the iconlist causes possible core dumps if a redraw
		// is required before the new list is in place - hence the backup keeps the last used list until
		// we're sure that we can safely remove them

		if ( showScenesRunning == SCENE_RUNNING )
		{
			pthread_mutex_lock( &scene_mutex );
			storyboard->setSkip();
			gdk_threads_enter();
			gtk_tree_store_clear( storyboard->getModel() );
			for ( unsigned int i = 0; showScenesRunning == SCENE_RUNNING && i < scene.size(); ++i )
			{
				char start[ 1024 ] = "";
				vector< std::string > metaNames;
				vector< std::string >::iterator metaNamesIter;
				StringUtils::split( Preferences::getInstance().metaNames, ",", metaNames );

				// Generate scene info
				frameNum = ( i == 0 ) ? 0 : scene[ i - 1 ];
				fail_neg( common->getPlayList() ->GetFrame( frameNum, frame ) );
				frame.ExtractHeader();
				storyboard->getTime().setFramerate( frame.GetFrameRate() );
				char* fileName = common->getPlayList() ->GetFileNameOfFrame( frameNum );
				snprintf( start, 1024, "%s\n%s\n%s",
					g_path_get_basename( fileName ),
					storyboard->getTime().parseFramesToString( frameNum, common->getTimeFormat() ).c_str(),
					storyboard->getTime().parseFramesToString( scene[ i ] - frameNum, common->getTimeFormat() ).c_str()
				);
				free( fileName );
				
				// Put the thumbnail and scene metadata into the model
				gtk_tree_store_append( storyboard->getModel(), &iter, NULL );
				gtk_tree_store_set( storyboard->getModel(), &iter,
				                    COLUMN_THUMBNAIL, ( *icons ) [ i ],
				                    COLUMN_NAME, _("File:\nBegin:\nDuration:"),
				                    COLUMN_NAME_MODE, GTK_CELL_RENDERER_MODE_ACTIVATABLE,
				                    COLUMN_VALUE, start,
				                    COLUMN_VALUE_MODE, GTK_CELL_RENDERER_MODE_ACTIVATABLE,
				                    -1 );

				for ( metaNamesIter = metaNames.begin(); metaNamesIter != metaNames.end(); ++metaNamesIter )
				{
					std::string metaname = *metaNamesIter;
					if ( metaname.at(0) == '*' )
						metaname.erase(0,1);
						
					// Add standard metadata where exists or required (*name)
					if ( metaNamesIter->at( 0 ) == '*' || common->getPlayList()->GetSeqAttribute( frameNum, metaname.c_str() ) != "" )
					{
						vector< std::pair< std::string, std::string > >& metaValues = Preferences::getInstance().metaValues[ metaname ];
						vector< std::pair< std::string, std::string > >::iterator metaValuesIter;
						std::string labelString = common->getPlayList()->GetSeqAttribute( frameNum, metaname.c_str() );
						char *label = const_cast< char* >( labelString.c_str() );
	
						// See if a label exists for the value
						for ( metaValuesIter = metaValues.begin(); metaValuesIter != metaValues.end(); ++metaValuesIter )
						{
							if ( metaValuesIter->second == label )
							{
								label = const_cast< char* >( metaValuesIter->first.c_str() );
								break;
							}
						}
						
						gtk_tree_store_append( storyboard->getModel(), &child, &iter );
						gtk_tree_store_set( storyboard->getModel(), &child,
				                    COLUMN_NAME, metaname.c_str(),
				                    COLUMN_NAME_MODE, GTK_CELL_RENDERER_MODE_EDITABLE,
				                    COLUMN_VALUE, label,
				                    COLUMN_VALUE_MODE, GTK_CELL_RENDERER_MODE_EDITABLE,
				                    -1 );
					}
				}
				// See if we should add extra row for setting a new metadata item
				for ( metaNamesIter = metaNames.begin(); metaNamesIter != metaNames.end(); ++metaNamesIter )
				{
					std::string metaname = *metaNamesIter;
					if ( metaname.at( 0 ) == '*' )
						metaname.erase( 0, 1 );
						
					if ( common->getPlayList()->GetSeqAttribute( frameNum, metaname.c_str() ) == "" )
					{

						gtk_tree_store_append( storyboard->getModel(), &child, &iter );
						gtk_tree_store_set( storyboard->getModel(), &child,
				                    COLUMN_NAME, _("(new)"),
				                    COLUMN_NAME_MODE, GTK_CELL_RENDERER_MODE_EDITABLE,
				                    COLUMN_VALUE, "",
				                    COLUMN_VALUE_MODE, GTK_CELL_RENDERER_MODE_EDITABLE,
				                    -1 );
						break;
					}
				}	
			}
			if ( scene.size() )
				storyboard->select( common->getCurrentScene() );

			gdk_threads_leave();
			storyboard->clearSkip();
			pthread_mutex_unlock( &scene_mutex );

			if ( scene.size() && Preferences::getInstance().expandStoryboard )
				g_idle_add( expansion_handler, storyboard->getView() );
			
			// Clean up temporary backup now
			removeImages( &backup );
			// Now backup
			backup = icons;
		}
		else
		{
			// Remove any unused thumbs in icons
			removeImages( &icons );
		}
		// If we're still running now, it's OK to idle again.
		if ( showScenesRunning == SCENE_RUNNING )
			showScenesRunning = SCENE_IDLE;

	}
	GetFramePool( ) ->DoneWithFrame( &frame );

	delete[] pixels;
	return NULL;
}

void Storyboard::redraw( )
{
	static pthread_t scenes_thread;
	static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

	pthread_mutex_lock( &mutex );
	selection = -1;

	// If running then, set as RESTART otherwise start the thread,
	// otherwise we're already waiting on a restart so do nothing
	if ( showScenesRunning == SCENE_RUNNING )
	{
		showScenesRunning = SCENE_RESTART;
	}
	else if ( showScenesRunning == SCENE_INIT )
	{
		showScenesRunning = SCENE_STARTING;
		pthread_create( &scenes_thread, NULL, showScenesThread, this );
	}
	else if ( showScenesRunning == SCENE_IDLE )
	{
		pthread_join( scenes_thread, NULL );
		showScenesRunning = SCENE_STARTING;
		pthread_create( &scenes_thread, NULL, showScenesThread, this );
	}

	pthread_mutex_unlock( &mutex );
}

void Storyboard::select( int scene )
{
	if ( scene > -1 && scene < gtk_tree_model_iter_n_children( GTK_TREE_MODEL( model ), NULL ) )
	{
		GtkTreeIter iter;
		GtkTreeSelection* selection = gtk_tree_view_get_selection( view );
		if ( gtk_tree_model_iter_nth_child( GTK_TREE_MODEL( model ), &iter, NULL, scene ) )
		{
			GtkTreePath * path = gtk_tree_model_get_path( GTK_TREE_MODEL( model ), &iter );
			skipSelect = true;
			gtk_tree_selection_select_iter( selection, &iter );
			gtk_tree_view_scroll_to_cell( view, path, NULL, TRUE, 0.5, 0.0 );
			gtk_tree_path_free( path );
			skipSelect = false;
		}
		this->selection = scene;
	}
}

void Storyboard::setSensitive( bool sensitive )
{
	gtk_widget_set_sensitive( GTK_WIDGET( view ), sensitive ? TRUE : FALSE );
}

void Storyboard::moveScene( unsigned int destScene )
{
	std::cerr << "moving scene " << selection << " to scene " << destScene << std::endl;
	std::vector <int> scenes = common->getPageEditor() ->GetScene();
	int start = ( selection == 0 ) ? 0 : scenes[ selection - 1 ];
	int end = common->getPlayList() ->FindEndOfScene( start );
	int destFrame = 0;

	common->getPageEditor() ->CopyFrames( start, end );
	common->getPlayList() ->Delete( start, end );

	if ( destScene >= scenes.size() )
	{
		destFrame = common->getPlayList() ->GetNumFrames();
	}
	else if ( ( int ) destScene > selection )
	{
		common->getPageEditor() ->ResetBar();
		scenes = common->getPageEditor() ->GetScene();
		destFrame = scenes[ destScene - 2 ];
	}
	else if ( destScene > 0 )
	{
		common->getPageEditor() ->ResetBar();
		scenes = common->getPageEditor() ->GetScene();
		destFrame = scenes[ destScene - 1 ];
	}

	common->getPageEditor() ->PasteFrames( destFrame );
	common->moveToFrame( destFrame );

	redraw( );
}

Storyboard *GetStoryboard( )
{
	static Storyboard * singleton = new Storyboard( common );
	return singleton;
}

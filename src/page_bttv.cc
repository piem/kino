/*
* Copyright (C) 2001-2007 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include "kino_common.h"
#include "page.h"

#include "page_bttv.h"
#include "displayer.h"
#include "v4l.h"

extern "C"
{
#include "support.h"

	extern KinoCommon *common;

	void
	on_v4l_capture_page_record_button_clicked
	( GtkButton * button,
	  gpointer user_data )
	{

		common->getPageBttv() ->startCapture();
	}


	void
	on_v4l_capture_page_stop_button_clicked
	( GtkButton * button,
	  gpointer user_data )
	{
		common->getPageBttv() ->stopCapture();
	}

	void
	on_button_v4l_file_clicked( GtkButton       *button,
                                gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to Save As") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_v4l_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_v4l_file" ) ), filename );
	}

}

PageBttv::PageBttv( KinoCommon *common ) : seeking( false ), lastPreferenceFilename( "" ), channel( 0 )
{
	this->common = common;
}

PageBttv::~PageBttv()
{}

void PageBttv::start()
{
	std::cerr << ">>> Starting bttv" << std::endl;
	v4l = new GDKV4L( lookup_widget( common->getWidget(), "drawingarea3" ) );
	GtkEntry *fileEntry = GTK_ENTRY( lookup_widget( common->getWidget(), "entry_v4l_file" ) );

	// Determine the capture directory and file - rules are:
	// 1) if a full path is specified in the preferences, then use that
	// 2) if a non-full path is specified, then pick up the project directory and attach preference

	string capture_file_name = Preferences::getInstance().file;
	if ( capture_file_name[ 0 ] != '/' )
		capture_file_name = common->getPlayList( ) ->GetProjectDirectory( ) + "/" + capture_file_name;

	// If the preferences have changed, then update the filename

	if ( capture_file_name != lastPreferenceFilename )
	{
		lastPreferenceFilename = capture_file_name;
		gtk_entry_set_text( fileEntry, capture_file_name.c_str( ) );
	}

	Preferences &prefs = Preferences::getInstance();
	v4l->setInfo( prefs.v4lVideoDevice, prefs.v4lInput, prefs.v4lAudioDevice, atoi( prefs.v4lAudio ) );

	if ( v4l->openDevice() )
	{
		v4l->report();
		v4l->setChannel( channel );
		v4l->setTuner( 0 );
		v4l->startAudio();
		v4l->startVideo();
	}
	else
	{
		std::cerr << "unable to open video device" << std::endl;
	}
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_v4l" ) ), TRUE );
}

gulong PageBttv::activate()
{
	if ( v4l->deviceAvailable() )
		return VIDEO_START_OF_MOVIE | VIDEO_START_OF_SCENE | VIDEO_REWIND | VIDEO_BACK |
		       VIDEO_PLAY | VIDEO_STOP |
		       VIDEO_FAST_FORWARD | VIDEO_FORWARD | VIDEO_NEXT_SCENE | VIDEO_END_OF_MOVIE;
	else
		return 0;
}

void PageBttv::clean()
{
	std::cerr << ">> Leaving bttv" << std::endl;
	if ( v4l->deviceAvailable() )
	{
		v4l->stopVideo();
		v4l->stopAudio();
	}
	delete v4l;
	v4l = NULL;
	std::cerr << ">> Left bttv" << std::endl;
}

/** Called when a new file operation is selected by the user.
*/

void PageBttv::newFile( )
{
	lastPreferenceFilename = "";
}


void PageBttv::videoStartOfMovie()
{
	std::cerr << "channels = " << v4l->getNumberOfChannels( ) << " channel = " << channel;
	channel = ( channel - 1 ) % v4l->getNumberOfChannels( );
	v4l->setChannel( channel );
	std::cerr << " new channel = " << channel << std::endl;
}

void PageBttv::videoPreviousScene()
{
	v4l->setFrequency( v4l->getFrequency() - 100 );
}

void PageBttv::videoRewind()
{
	if ( seeking == true )
		return ;
	seeking = true;
	do
	{
		if ( ( v4l->getFrequency() - 1 ) > 0 )
			v4l->setFrequency( v4l->getFrequency() - 1 );
		else
			break;
	}
	while ( v4l->getSignal() != 0 );
	do
	{
		if ( ( v4l->getFrequency() - 2 ) > 0 )
			v4l->setFrequency( v4l->getFrequency() - 2 );
		else
			break;
		std::cerr << "frequency = " << v4l->getFrequency() << std::endl;
		while ( gtk_events_pending() )
		{
			gtk_main_iteration();
		}
	}
	while ( v4l->getSignal() == 0 && seeking );
	seeking = false;
}

void PageBttv::videoPlay()
{
	v4l->startVideo();
}

void PageBttv::videoStop()
{
	seeking = false;
	v4l->stopVideo();
}

void PageBttv::videoFastForward()
{
	if ( seeking == true )
		return ;
	seeking = true;
	do
	{
		if ( ( v4l->getFrequency() + 1 ) < 0xffff )
			v4l->setFrequency( v4l->getFrequency() + 1 );
		else
			break;
	}
	while ( v4l->getSignal() != 0 );
	do
	{
		if ( ( v4l->getFrequency() + 2 ) < 0xffff )
			v4l->setFrequency( v4l->getFrequency() + 2 );
		else
			break;
		std::cerr << "frequency = " << v4l->getFrequency() << std::endl;
		while ( gtk_events_pending() )
		{
			gtk_main_iteration();
		}
	}
	while ( v4l->getSignal() == 0 && seeking );
	seeking = false;
}

void PageBttv::videoBack()
{
	v4l->setFrequency( v4l->getFrequency() - 1 );
}

void PageBttv::videoForward()
{
	v4l->setFrequency( v4l->getFrequency() + 1 );
}

void PageBttv::videoNextScene()
{
	v4l->setFrequency( v4l->getFrequency() + 100 );
}

void PageBttv::videoEndOfMovie()
{
	channel = ( channel + 1 ) % v4l->getNumberOfChannels( );
	v4l->setChannel( channel );
}

void PageBttv::startCapture()
{
	v4l->startCapturing();
}

void PageBttv::stopCapture()
{
	v4l->stopCapturing();
}

void PageBttv::saveFrame()
{
	std::cerr << "BTTV Save Frame Requested" << std::endl;
}


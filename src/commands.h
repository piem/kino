/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _COMMANDS_H
#define _COMMANDS_H

#include <gtk/gtk.h>

struct navigate_control
{
	gint step;
	gint rate;
	gint subframe;
	gboolean active;
	gboolean escaped;
	gboolean capture_active;
};

#ifdef __cplusplus
extern "C"
{
#endif

	void kinoInitialise( GtkWidget * );
	void kinoPostInit();
	gboolean kinoDeactivate( void );
	void bulkLoad( int argc, char * argv[] );
	void newFile( void );
	void openFile( void );
	void savePlayListAs( void );
	void savePlayList( void );
	void saveFrame( void );
	void insertFile( void );
	void appendFile( void );
	gboolean processKeyboard( GdkEventKey * event );
	void processCommand( const char * command );
	void pageStart( int );
	int moveToFrame( int );
	int moveByFrames( int );
	void videoStart( void );
	void videoPreviousScene( void );
	void videoStartOfScene( void );
	void videoRewind( void );
	void videoBack(void);
	void videoBackBy( int );
	void videoPlay( void );
	void videoPause( void );
	void videoStop( void );
	void videoForwardBy( int );
	void videoForward(void);
	void videoFastForward( void );
	void videoNextScene( void );
	void videoEndOfScene( void );
	void videoEndOfMovie( void );
	void videoShuttle( int );
	void windowMoved( void );
	void visibilityChanged( gboolean );
	void selectScene( int );
	void showIcons( GtkWidget * );
	void selectIcon( GtkWidget *, int );
	void notebookChangePage( int );
	void stop_navigator( void );
	void startCapture( void );
	void stopCapture( void );
	void setPreviewSize( float factor );
	void previewExport( void );
	void startExport( void );
	void stopExport( void );
	void pauseExport( void );
	void setExportMode( int );
	void RefreshBar( GtkWidget * drawingarea );
	void setMoreInfo( int );
	void setTimeFormat( int );
	void publishPlayList( void );
	void publishFrame( void );
	void startJogShuttle( void );
	void showHelp( const char *page );
	void handleMouseScroll( GdkEvent *event );
	int kino2raw( char* filename, char* option );

#ifdef __cplusplus
}
#endif

#endif

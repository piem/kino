/*
* kino_av_pipe.cc -- AV Export Pipe Implementations and Factories
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "error.h"
#include "kino_av_pipe.h"
#include "endian_types.h"
#include "rwpipe.h"
#include "frame.h"
#include "stringutils.h"

/*
 * C Functions for generating a WAV output. Should be rewritten in C++ 
 * rather than just wrapped...
 */

typedef struct
{
	char		riff[ 4 ];
	int32_le_t	length;
	char		type[ 4 ];
}
RIFFChunkType, *RIFFChunk;

typedef struct
{
	char		format[ 4 ];
	int32_le_t	length;
	int16_le_t	filler;
	int16_le_t	channels;
	int32_le_t	rate;
	int32_le_t	bytespersecond;
	int16_le_t	bytespersample;
	int16_le_t	bitspersample;
}
FORMATChunkType, *FORMATChunk;

typedef struct
{
	char		data[ 4 ];
	int32_le_t	length;
}
DATAChunkType, *DATAChunk;

typedef struct
{
	FILE *file;
	RIFFChunk riff;
	FORMATChunk format;
	DATAChunk data;
	int isapipe;
}
WAVStructType, *WAVStruct;

static RIFFChunk RIFFChunk_Init( )
{
	RIFFChunk chunk = ( RIFFChunk ) malloc( sizeof( RIFFChunkType ) );
	memcpy( chunk->riff, "RIFF", 4 );
	chunk->length = 4 + sizeof( FORMATChunkType ) + sizeof( DATAChunkType );
	memcpy( chunk->type, "WAVE", 4 );
	return chunk;
}

static FORMATChunk FORMATChunk_Init( short channels, int rate, int bytespersample )
{
	FORMATChunk format = ( FORMATChunk ) malloc( sizeof( FORMATChunkType ) );
	memset( format, 0, sizeof( FORMATChunkType ) );
	memcpy( format->format, "fmt ", 4 );
	format->length = 0x10;
	format->filler = 0x01;
	format->channels = channels;
	format->rate = rate;
	format->bytespersecond = rate * channels * bytespersample;
	format->bytespersample = bytespersample * channels;
	format->bitspersample = bytespersample * 8;
	return format;
}

static DATAChunk DATAChunk_Init( )
{
	DATAChunk data = ( DATAChunk ) malloc( sizeof( DATAChunkType ) );
	memset( data, 0, sizeof( DATAChunkType ) );
	memcpy( data->data, "data", 4 );
	return data;
}

static void WAVStruct_WriteHeader( WAVStruct wav )
{
	if ( wav->isapipe )
	{
		wav->riff->length |= 0x7ffff000;
		wav->data->length |= 0x7ffff000;
	}
	fwrite( wav->riff, sizeof( RIFFChunkType ), 1, wav->file );
	fwrite( wav->format, sizeof( FORMATChunkType ), 1, wav->file );
	fwrite( wav->data, sizeof( DATAChunkType ), 1, wav->file );
	fflush( wav->file );
}

static WAVStruct WAVStruct_Init( char *output, short channels, int frequency, short bytespersample )
{
	WAVStruct wav = ( WAVStruct ) malloc( sizeof( WAVStructType ) );
	wav->riff = RIFFChunk_Init();
	wav->format = FORMATChunk_Init( channels, frequency, bytespersample );
	wav->data = DATAChunk_Init();
	wav->isapipe = output[ 0 ] == '|';
	if ( wav->isapipe )
		wav->file = popen( output + 1, "w" );
	else
		wav->file = fopen( output, "w" );
	WAVStruct_WriteHeader( wav );
	return wav;
}

static int WAVStruct_WriteData( WAVStruct wav, void *data, int length )
{
	wav->riff->length += length;
	wav->data->length += length;
	int written = fwrite( data, length, 1, wav->file );
	fflush( wav->file );
	return written == 1;
}

static void WAVStruct_Close( WAVStruct wav )
{
	if ( !wav->isapipe )
	{
		rewind( wav->file );
		WAVStruct_WriteHeader( wav );
		fclose( wav->file );
	}
	else
	{
		pclose( wav->file );
	}
	free( wav->riff );
	free( wav->format );
	free( wav->data );
	free( wav );
}

class WAVImport : public KinoAudioInput
{
private:
	WAVStructType wav;
	long start;
	int channels;
	int frequency;
	int bytespersample;
	long length;
public:
	WAVImport();
	virtual ~WAVImport( );
	bool Open( char *input );
	int GetChannels( )
	{
		return channels;
	}
	int GetFrequency( )
	{
		return frequency;
	}
	int GetBytesPerSample( )
	{
		return bytespersample;
	}
	long GetNumberOfSamples( )
	{
		return length / channels / bytespersample;
	}
	bool Seek( off_t offset );
	bool Get( int16_t *data, int length );
	bool Close( );
};

WAVImport::WAVImport()
{
	wav.file = NULL;
}

WAVImport::~WAVImport()
{
	Close( );
}

bool WAVImport::Open( char *input )
{
	bool iswav = false;

	wav.file = fopen( input, "r" );

	if ( wav.file != NULL )
	{
		wav.riff = ( RIFFChunk ) malloc( sizeof( RIFFChunkType ) );
		wav.format = ( FORMATChunk ) malloc( sizeof( FORMATChunkType ) );
		wav.data = ( DATAChunk ) malloc( sizeof( DATAChunkType ) );

		// Bad... temporary code
		fread( wav.riff, sizeof( RIFFChunkType ), 1, wav.file );
		fread( wav.format, sizeof( FORMATChunkType ), 1, wav.file );
		int remainder = wav.format->length - sizeof( FORMATChunkType );
		if ( remainder > 0 )
			fseek( wav.file, remainder, SEEK_CUR );
		fread( wav.data, sizeof( DATAChunkType ), 1, wav.file );
		if ( strncasecmp( wav.data->data, "JUNK", 4 ) == 0 )
		{
			fseek( wav.file, wav.data->length, SEEK_CUR );
			fread( wav.data, sizeof( DATAChunkType ), 1, wav.file );
		}

		if ( !strncmp( wav.riff->riff, "RIFF", 4 ) && !strncmp( wav.riff->type, "WAVE", 4 ) )
		{
			iswav = true;
			channels = wav.format->channels;
			frequency = wav.format->rate;
			bytespersample = wav.format->bytespersample / channels;
			start = ftell( wav.file );
			length = wav.data->length;
		}
	}

	return iswav;
}

bool WAVImport::Seek( off_t offset )
{
	return fseek( wav.file, offset + start, SEEK_SET ) == 0;
}

bool WAVImport::Get( int16_t *data, int length )
{
	return fread( data, 1, length, wav.file ) == ( unsigned ) length;
}

bool WAVImport::Close( )
{
	if ( wav.file != NULL )
	{
		fclose( wav.file );
		free( wav.riff );
		free( wav.format );
		free( wav.data );
		wav.file = NULL;
	}
	return true;
}


class PipeImport : public KinoAudioInput
{
private:
	RWPipe m_pipe;
	string m_file;
	int16_t m_buffer[ 4 * DV_AUDIO_MAX_SAMPLES ];
	char *m_command;
public:
	PipeImport()
		: m_command( 0 )
	{
	}
	virtual ~PipeImport( )
	{
		m_pipe.stop();
		delete m_command;
	}
	bool Open( char *input )
	{
		bool result = false;
		char *kinoHome = getenv("KINO_HOME");
		string homeFile, script;
		int testHomeFile = 0;

		// Allow a script in home directory to override
		if ( kinoHome )
			homeFile = string( kinoHome ) + string( "/import/audio.sh" );
		else
			homeFile = string( getenv( "HOME" ) ) + string( "/kino/import/audio.sh" );
		testHomeFile = open( homeFile.c_str(), O_RDONLY );
		if ( testHomeFile > -1 )
		{
			close( testHomeFile );
			script = homeFile;
		}
		else
		{
			script = string( DATADIR "/kino/scripts/import/audio.sh" );
		}

		delete m_command;
		string inputFilename = StringUtils::replaceAll( input, "\"", "\\\"" );
		m_command = g_strdup_printf( "%s \"%s\"", script.c_str(), inputFilename.c_str() );
		m_pipe.stop();
		if ( m_pipe.run( m_command ) )
		{
			char temp[ 10 ];
			result = ( m_pipe.readData( temp, 10 ) == 10 );
			m_pipe.stop();
			if ( result )
				return m_pipe.run( m_command );
		}
		return result;
	}
	int GetChannels( )
	{
		return 2;
	}
	int GetFrequency( )
	{
		return 44100;
	}
	int GetBytesPerSample( )
	{
		return 2;
	}
	long GetNumberOfSamples( )
	{
		return -1;
	}
	bool Seek( off_t offset )
	{
		return false;
	}
	bool Get( int16_t *data, int length )
	{
		return ( m_pipe.readData( data, length ) == length );
	}
	bool Close( )
	{
		m_pipe.stop();
		return true;
	}
};


KinoAudioInput *KinoAudioInputFactory::CreateAudioInput( char *input )
{
	const std::string filename( input );
	const std::string suffix( filename.begin() + filename.rfind( "." ), filename.end() );
	KinoAudioInput* filter;

	if ( suffix == ".wav" )
		filter = new WAVImport();
	else
		filter = new PipeImport();
	if ( filter->Open( input ) )
		return filter;
	else
		delete filter;
	return 0;
}

/** WAV Export Pipe.
*/

class WAVExport : public KinoAudioPipe
{
private:
	WAVStruct wav;
public:
	bool OpenAudio( char *output, int channels, int frequency, int bytespersample );
	bool OutputAudioFrame( int16_t *data, int length );
	bool CloseAudio( );
};

/** Open an audio output for WAV.
*/

bool WAVExport::OpenAudio( char *output, int channels, int frequency, int bytespersample )
{
	sigpipe_clear();
	wav = WAVStruct_Init( output, channels, frequency, bytespersample );
	return wav != NULL && sigpipe_get() == 0;
}

/** Output an audio frame of the specified length.
*/

bool WAVExport::OutputAudioFrame( int16_t *data, int length )
{
	sigpipe_clear();
	return WAVStruct_WriteData( wav, data, length ) == 1 && sigpipe_get() == 0;
}

/** Close the output.
*/

bool WAVExport::CloseAudio( )
{
	sigpipe_clear();
	WAVStruct_Close( wav );
	return sigpipe_get() == 0;
}

/** PCM Export Pipe.
*/

class PCMExport : public KinoAudioPipe
{
private:
	FILE *out;
	int isapipe;
public:
	bool OpenAudio( char *output, int channels, int frequency, int bytespersample );
	bool OutputAudioFrame( int16_t *data, int length );
	bool CloseAudio( );
};

/** Open an audio output for PCM.
*/

bool PCMExport::OpenAudio( char *output, int channels, int frequency, int bytespersample )
{
	if ( output[ 0 ] == '|' )
	{
		out = popen( output + 1, "w" );
		isapipe = 1;
	}
	else
	{
		out = fopen( output, "w" );
		isapipe = 0;
	}

	return out != NULL && sigpipe_get() == 0;
}

/** Write the PCM data
*/

bool PCMExport::OutputAudioFrame( int16_t *data, int length )
{
	sigpipe_clear();
	return fwrite( data, length, 1, out ) == 1 && sigpipe_get() == 0;
}

/** Close the output.
*/

bool PCMExport::CloseAudio( )
{
	sigpipe_clear();
	if ( isapipe )
		pclose( out );
	else
		fclose( out );
	return sigpipe_get() == 0;
}

/** Create a wav or pcm audio pipe.
*/

KinoAudioPipe *KinoAudioFactory::CreateAudioPipe( kino_audio_pipe type )
{
	switch ( type )
	{
	case PIPE_AUDIO_WAV:
		return new WAVExport();
	case PIPE_AUDIO_PCM:
		return new PCMExport();
	default:
		return NULL;
	}
}

/**
 * Pilfered directly from MJPEG - converts YUV422 to YUV420P.
 */

static void frame_YUV422_to_YUV420P( uint8_t **output, uint8_t *input, int width, int height )
{
	int i, j, w2;
	uint8_t *y, *cb, *cr;

	w2 = width / 2;
	y = output[ 0 ];
	cb = output[ 1 ];
	cr = output[ 2 ];

	for ( i = 0; i < height; i += 2 )
	{
		/* process two scanlines (one from each field, interleaved) */
		for ( j = 0; j < w2; j++ )
		{
			/* packed YUV 422 is: Y[i] U[i] Y[i+1] V[i] */
			*( y++ ) = *( input++ );
			*( cb++ ) = *( input++ );
			*( y++ ) = *( input++ );
			*( cr++ ) = *( input++ );
		}
		/* process next two scanlines (one from each field, interleaved) */
		for ( j = 0; j < w2; j++ )
		{
			/* skip every second line for U and V */
			*( y++ ) = *( input++ );
			input++;
			*( y++ ) = *( input++ );
			input++;
		}
	}
}
#define SCALEBITS 8
#define ONE_HALF  (1 << (SCALEBITS - 1))
#define FIX(x)		((int) ((x) * (1L<<SCALEBITS) + 0.5))

static void frame_RGB24_to_YUV420P( uint8_t **output, uint8_t *input, int width, int height )
{
	int wrap, wrap3, x, y;
	int r, g, b, r1, g1, b1;
	uint8_t *p;

	uint8_t *lum = output[ 0 ];
	uint8_t *cb = output[ 1 ];
	uint8_t *cr = output[ 2 ];

	wrap = width;
	wrap3 = width * 3;
	//wrap3 = 0;
	p = input;
	for ( y = 0;y < height;y += 2 )
	{
		for ( x = 0;x < width;x += 2 )
		{
			r = p[ 0 ];
			g = p[ 1 ];
			b = p[ 2 ];
			r1 = r;
			g1 = g;
			b1 = b;
			lum[ width ] = lum[ 0 ] = ( FIX( 0.29900 ) * r + FIX( 0.58700 ) * g +
			                            FIX( 0.11400 ) * b + ONE_HALF ) >> SCALEBITS;
			r = p[ 3 ];
			g = p[ 4 ];
			b = p[ 5 ];
			r1 += r;
			g1 += g;
			b1 += b;
			lum[ width + 1 ] = lum[ 1 ] = ( FIX( 0.29900 ) * r + FIX( 0.58700 ) * g +
			                                FIX( 0.11400 ) * b + ONE_HALF ) >> SCALEBITS;
			lum += wrap;

			cb[ 0 ] = ( ( - FIX( 0.16874 ) * r1 - FIX( 0.33126 ) * g1 +
			              FIX( 0.50000 ) * b1 + 4 * ONE_HALF - 1 ) >> ( SCALEBITS + 1 ) ) + 128;
			cr[ 0 ] = ( ( FIX( 0.50000 ) * r1 - FIX( 0.41869 ) * g1 -
			              FIX( 0.08131 ) * b1 + 4 * ONE_HALF - 1 ) >> ( SCALEBITS + 1 ) ) + 128;

			cb++;
			cr++;
			p += 6;
			lum += -wrap + 2;
		}
		p += wrap3;
		lum += wrap;
	}
}


/** Pipe for MJPEG lav tools.
*/

class KinoMJPEGVideoPipe : public KinoVideoPipe
{
protected:
	FILE *output;
	int width;
	int height;
	int pitches[ 3 ];
	uint8_t *frame[ 3 ];

public:
	bool OpenVideoPipe( char *pipe, int width, int height, bool interlaced = true );
	bool OutputVideoFrame( uint8_t *frame, int size );
	bool CloseVideo( );
};

/** Open the video pipe.
*/

bool KinoMJPEGVideoPipe::OpenVideoPipe( char *pipe, int width, int height, bool interlaced )
{
	sigpipe_clear();
	output = popen( pipe, "w" );

	this->width = width;
	this->height = height;

	pitches[ 0 ] = width * 2;
	pitches[ 1 ] = 0;
	pitches[ 2 ] = 0;

	frame[ 0 ] = new uint8_t[ width * height ];
	frame[ 1 ] = new uint8_t[ width * height / 4 ];
	frame[ 2 ] = new uint8_t[ width * height / 4 ];

	int written = 0;
	if ( output != NULL )
		written = fprintf( output, "YUV4MPEG2 W%d H%d F%s %s\n", width, height, 
			height == 576 ? "25:1 A118:81" : "30000:1001 A40:33", interlaced ? "Ib" : "Ip" );

	return written != 0 && sigpipe_get() == 0;
}

/** Output a video frame - the input is assumed to be YUV422 and output is
 	YUV420P.
*/

bool KinoMJPEGVideoPipe::OutputVideoFrame( uint8_t *pixels, int size )
{
	sigpipe_clear();
	fprintf( output, "FRAME\n" );
	frame_YUV422_to_YUV420P( frame, pixels, width, height );
	fwrite( frame[ 0 ], width * height, 1, output );
	fwrite( frame[ 1 ], width * height / 4, 1, output );
	fwrite( frame[ 2 ], width * height / 4, 1, output );
	return sigpipe_get() == 0;
}

/** Close the video pipe.
*/

bool KinoMJPEGVideoPipe::CloseVideo( )
{
	sigpipe_clear();
	pclose( output );
	delete[] frame[ 0 ];
	delete[] frame[ 1 ];
	delete[] frame[ 2 ];
	return sigpipe_get() == 0;
}

class KinoDeinterlacedMJPEGVideoPipe : public KinoMJPEGVideoPipe
{
public:
	bool OutputVideoFrame( uint8_t *frame, int size );
};

/** Output a video frame - the input is assumed to be RGB24 and output is
 	YUV420P.
*/

bool KinoDeinterlacedMJPEGVideoPipe::OutputVideoFrame( uint8_t *pixels, int size )
{
	sigpipe_clear();
	fprintf( output, "FRAME\n" );
	frame_RGB24_to_YUV420P( frame, pixels, width, height );
	fwrite( frame[ 0 ], width * height, 1, output );
	fwrite( frame[ 1 ], width * height / 4, 1, output );
	fwrite( frame[ 2 ], width * height / 4, 1, output );
	return sigpipe_get() == 0;
}


/** YUV Pipe for encodedv. Note that this is not supported by libdv.
*/

class DvEncoderVideoPipeYUV : public KinoVideoPipe
{
private:
	int width;
	int height;
	int m_pid, m_writer;
	GError *m_error;

public:
	bool OpenVideoPipe( char *pipe, int width, int height, bool interlaced = true );
	bool OutputVideoFrame( uint8_t *frame, int size );
	bool CloseVideo( );
};

/** Open the video pipe.
*/

bool DvEncoderVideoPipeYUV::OpenVideoPipe( char *pipe, int width, int height, bool interlaced )
{
	sigpipe_clear();
	const char * args[ 4 ];

	args[ 0 ] = "/bin/sh";
	args[ 1 ] = "-c";
	args[ 2 ] = ( char * )pipe;
	args[ 3 ] = NULL;

	g_spawn_async_with_pipes( ".", (gchar**) args, NULL, G_SPAWN_LEAVE_DESCRIPTORS_OPEN, NULL, NULL, &m_pid, &m_writer, NULL, NULL, &m_error );

	this->width = width;
	this->height = height;
	char output[ 256 ];
	sprintf( output, "YUV4MPEG2 W%d H%d F%s %s XYSCSS=422\n", width, height,
			height == 576 ? "25:1 A118:81" : "30000:1001 A40:33", interlaced ? "Ib" : "Ip" );
	write( m_writer, output, strlen( output ) );
	return m_writer != -1 && sigpipe_get() == 0;
}

/** Output a YUV422 image to the pipe.
*/

bool DvEncoderVideoPipeYUV::OutputVideoFrame( uint8_t *frame, int size )
{
	sigpipe_clear();
	char output[ 256 ];
	sprintf( output, "FRAME\n" );
	write( m_writer, output, strlen( output ) );
	write( m_writer, frame, size );
	return sigpipe_get() == 0;
}

/** Close the video pipe.
*/

bool DvEncoderVideoPipeYUV::CloseVideo( )
{
	sigpipe_clear();
	close( m_writer );
	return sigpipe_get() == 0;
}

/** PGM Pipe for encodedv.
*/

class DvEncoderVideoPipePGM : public KinoVideoPipe
{
private:
	int width;
	int height;
	int m_pid, m_writer;
	GError *m_error;
	
public:
	bool OpenVideoPipe( char *pipe, int width, int height, bool interlaced = true );
	bool OutputVideoFrame( uint8_t *frame, int size );
	bool CloseVideo( );
};

/** Open the video pipe.
*/

bool DvEncoderVideoPipePGM::OpenVideoPipe( char *pipe, int width, int height, bool interlaced )
{
	sigpipe_clear();
	const char * args[ 4 ];

	args[ 0 ] = "/bin/sh";
	args[ 1 ] = "-c";
	args[ 2 ] = ( char * )pipe;
	args[ 3 ] = NULL;

	g_spawn_async_with_pipes( ".", (gchar**) args, NULL, G_SPAWN_LEAVE_DESCRIPTORS_OPEN, NULL, NULL, &m_pid, &m_writer, NULL, NULL, &m_error );
	
	this->width = width;
	this->height = height;
	return m_writer != -1 && sigpipe_get() == 0;
}

/** Output a YUV420P image to the pipe.
*/

bool DvEncoderVideoPipePGM::OutputVideoFrame( uint8_t *frame, int size )
{
	sigpipe_clear();
	char output[ 256 ];
	sprintf( output, "P5\n%d %d\n255\n", width, height * 3 / 2 );
	write( m_writer, output, strlen( output ) );
	write( m_writer, frame, width * height );
	int h = height >> 1;
	int w = width >> 1;
	uint8_t *ptr1 = frame + width * height;
	uint8_t *ptr2 = ptr1 + ( w * h ) ;
	for ( int i = 0; i < h; i++ )
	{
		write( m_writer, ptr1, w );
		write( m_writer, ptr2, w );
		ptr1 += w;
		ptr2 += w;
	}
	return sigpipe_get() == 0;
}

/** Close DV Encode pipe
*/

bool DvEncoderVideoPipePGM::CloseVideo( )
{
	sigpipe_clear();
	close( m_writer );
	return sigpipe_get() == 0;
}

/** Create a video output pipe. Note there is an implicit agreement between the
 	output pipe and the image format:
	- MJPEG expects YUV422 and outputs YUV420P
	- DV_YUV expects YUV422P and outputs YUV422P
	- DV_PGM expects YUV420P and outputs YUVPPM (a la dvencodes expected input)
*/

KinoVideoPipe *KinoVideoFactory::CreateVideoPipe( kino_video_pipe type )
{
	switch ( type )
	{
	case PIPE_VIDEO_MJPEG:
		return new KinoMJPEGVideoPipe();
	case PIPE_VIDEO_DEINTERLACED_MJPEG:
		return new KinoDeinterlacedMJPEGVideoPipe();
	case PIPE_VIDEO_DV_YUV:
		return new DvEncoderVideoPipeYUV();
	case PIPE_VIDEO_DV_PGM:
		return new DvEncoderVideoPipePGM();
	default:
		return NULL;
	}
}



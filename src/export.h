/*
* export.h Export Mode Base Object
* Copyright (C) 2002 Mads Bondo Dydensborg <madsdyd@challenge.dk>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _EXPORT_H
#define _EXPORT_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/time.h>

#include <gtk/gtk.h>
#include "page_export.h"
#include "kino_common.h"

extern "C"
{
#include "support.h"
}

/** Export mode enumeration
 
    This assigns each export mode an identifier that can be used as arguments
    for certain methods.
*/
enum export_mode {
    EXPORT_MODE_1394,
    EXPORT_MODE_AVI,
    EXPORT_MODE_STILLS,
    EXPORT_MODE_AUDIO,
    EXPORT_MODE_MJPEG,
    EXPORT_MODE_PIPE
};

/** Export result enumeration
 
    The export page returns one of these to indicate state upon 
    completion of an export job.
*/
enum export_result {
    EXPORT_RESULT_FAILURE,
    EXPORT_RESULT_SUCCESS,
    EXPORT_RESULT_ABORT
};

/** Export component enumarations. This defines widgets that activated
    and deactivated by the individual export pages. */
enum export_component_enum {
    EXPORT_SCENE_LIST = 2,
    /* I guess the compiler will barf if we choose SCENE_LIST again */
    EXPORT_PREVIEW = 0x01000000,
    EXPORT_EXPORT = 0x02000000,
    EXPORT_PAUSE = 0x04000000
                   /* Note, stop is supported by all - no need to
                      have it in here... */
};

/** An Export reflects the interaction between the PageExport object and an
	individual export notebook page. All subsequent Export objects must extend this 
	class (publicly) and rewrite any or all of the virtual methods provided.
*/

class Export
{
protected:
	/* What we support. This is updated on each activate */
	gulong actions;

	/* Perhaps the following variables should be made static,
	   as they really need not be allocated by each page. 
	   OTOH: KISS */
	/* Pointers to the page and KinoCommon */
	PageExport *exportPage;
	KinoCommon *common;

	/* Pointers to range control elements */
	GtkOptionMenu	*menuRange;
	GtkSpinButton	*startSpin;
	GtkEntry        *startEntry;
	GtkSpinButton	*endSpin;
	GtkEntry        *endEntry;
	GtkSpinButton	*everySpin;

	/* The common buttons - all export pages
	   should support these buttons */
	GtkToggleButton	*previewButton;
	GtkToggleButton	*exportButton;
	GtkToggleButton	*stopButton;
	GtkToggleButton	*pauseButton;

	/** Variables used to measure time during the export */
	double startTime;
	double pauseTime;
	double nextUpdateTime;

	/** Every child should call this method once every loop
	    It will check for pausing, update the statusbar and the progressbar 
	    It is recommended that you call it the beginning of your innermost loop
	    likely before exporting any actual frame data.
	    Parameters marked with * should really be the ones passed to doExport
	    (Perhaps these should really be stored in this object... hmmm 
	    
	    \param progress the number of frames processed thus far
	    \param begin first frame to be exported (*)
	    \param end last frame to be exported (*)
	    \param every step for frames to be exported
	    \param currentFrame the frame to be exported (usually loop parameter)
	           this should be in playList counting - ie, 1 will be added before
	           the user sees it. -1 (default) = use progress value.
	    \param activity a short description of what is happening
	*/
	void innerLoopUpdate( int progress, int begin, int end, int every,
		int currentFrame = -1, const char* activity = _("Exporting") );

	/** Determine a new audio sampling rate that will try to maximize the proper
	    number of audio samples for the duration because DV often contains
	    unlocked audio. Also, edits can accumulate sample count differences.
	
	    \param playlist the current project file
	    \param outputRate the frequency to which all audio is being conformed
	    \param begin first frame to be exported (*)
	    \param end last frame to be exported (*)
	    \param every step for frames to be exported
	*/
	double calculateAdjustedRate( PlayList* playlist, double outputRate, int begin,
		int end, int every );

	/* Override this, if you need behaviour different from sleeping
	   and checking the "isPausing" flag (every 20 millisecond). 
	   Please remember to also maintain the time used in pause.
	*/
	virtual void doPause();

	/** The actual method that does the export.
	    Children overwrites this. 
	   
	    \param playlist to extract frames and other information from
	           This is duplicated when the export starts.
	    This allows for the user to continue editing while export
	    is undergoing. In other words, done expect the "real" playlist
	    to stay the same during export.
	    \param begin first frame to be exported
	    \param end last frame to be exported
	    \param every increment for frames to be exported
	    \param preview - if true, a preview is supposed to 
	           take place, not an actual export.
	    \return a result code. see enum export_result.
	*/
	virtual enum export_result
	doExport( PlayList * playlist, int begin, int end, int every, bool preview ) = 0;

	/* Override these to indicate what to support, what not */
	virtual gulong onActivate();
	virtual gulong onDeactivate();

public:
	/* Format a number of seconds into HH:MM:SS notation
	   \param buf buffer to format into
	   \param size max size of buffer
	   \param seconds passed as double
	   \return pointer to buf 
	*/
	static char * formatSecs( char * buf, int size, double seconds );

public:
	Export( PageExport *_exportPage, KinoCommon *_common );
	virtual ~Export( ) { }

	/* Children should return a bitfield for the actions supported,
	   when activate is called.
	   Note, these methods are not declared virtual, as childrens should
	   override "onActivate" and "onDeactivate" instead.
	   This is because we maintain our on set of commands in here... */
	gulong activate();
	gulong deactivate();
	virtual void start();
	virtual void clean();
	/* Called when the start button is toggled
	   A wrapper for doExport - does some setup, then runs
	   doExport and finally tears down some stuff again 
	   \param preview optional, is passed to doExport
	*/

	void startExport( bool preview = false ); // { }
	/* Called when the stop button is toggled
	   All children should honor the "isExporting" flag, and 
	   stop exporting when this flag is set to false. */
	virtual void stopExport();
	/* Called when the pause button is toggled
	   All children should call "checkPause" in their mainloop. 
	   checkPause will call doPause if needed.
	   default doPause just sleeps - if you need to do something else,
	   then overwrite doPause.
	*/
	virtual void pauseExport();

	virtual void selectScene( int );

	virtual void selectSection( int );

	/** Callback function for the start and end spinners */
	void onRangeChange( GtkSpinButton *widget );
};

#endif

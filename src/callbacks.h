/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _CALLBACKS_H
#define _CALLBACKS_H

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include "preferences_dialog.h"

void
on_new_activate ( GtkWidget *menuitem,
                  gpointer user_data );

void
on_open_activate ( GtkWidget *menuitem,
                   gpointer user_data );

void
on_save_activate ( GtkWidget *menuitem,
                   gpointer user_data );

void
on_save_as_activate ( GtkMenuItem *menuitem,
                      gpointer user_data );

void
on_exit_activate ( GtkWidget *menuitem,
                   gpointer user_data );

void
on_cut_current_scene_activate ( GtkWidget *menuitem,
                                gpointer user_data );

void
on_copy_current_scene_activate ( GtkWidget *menuitem,
                                 gpointer user_data );

void
on_paste_before_current_frame_activate ( GtkWidget *menuitem,
        gpointer user_data );

void
on_insert_movie_activate ( GtkWidget *menuitem,
                           gpointer user_data );

gboolean
on_expose_event ( GtkWidget *widget,
                                     GdkEventExpose *event,
                                     gpointer user_data );


gboolean
on_main_window_key_release_event ( GtkWidget *widget,
                                   GdkEventKey *event,
                                   gpointer user_data );

void
on_about_activate ( GtkWidget *menuitem,
                    gpointer user_data );

gboolean
on_main_window_delete_event ( GtkWidget *widget,
                              GdkEvent *event,
                              gpointer user_data );

gboolean
on_main_window_key_press_event ( GtkWidget *widget,
                                 GdkEventKey *event,
                                 gpointer user_data );


gboolean
on_main_window_button_key_event ( GtkWidget *widget,
                                  GdkEventKey *event,
                                  gpointer user_data );

gboolean
on_main_window_key_release_event ( GtkWidget *widget,
                                   GdkEventKey *event,
                                   gpointer user_data );

void
on_main_notebook_switch_page ( GtkNotebook *notebook,
                               GtkNotebookPage *page,
                               gint page_num,
                               gpointer user_data );

void
on_timeline_ok_button_pressed ( GtkButton *button,
                                gpointer user_data );

void
on_video_start_movie_button_clicked ( GtkButton *button,
                                      gpointer user_data );

void
on_video_start_scene_button_clicked ( GtkButton *button,
                                      gpointer user_data );

void
on_video_rewind_button_clicked ( GtkButton *button,
                                 gpointer user_data );

void
on_video_back_button_clicked ( GtkButton *button,
                               gpointer user_data );

void
on_video_play_button_clicked ( GtkButton *button,
                               gpointer user_data );

void
on_video_forward_button_clicked ( GtkButton *button,
                                  gpointer user_data );

void
on_video_fast_forward_button_clicked ( GtkButton *button,
                                       gpointer user_data );

void
on_video_end_scene_button_clicked ( GtkButton *button,
                                    gpointer user_data );

void
on_video_end_movie_button_clicked ( GtkButton *button,
                                    gpointer user_data );

void
on_video_pause_button_clicked ( GtkButton *button,
                                gpointer user_data );

void
on_video_stop_button_clicked ( GtkButton *button,
                               gpointer user_data );

void
on_video_forward_button_clicked ( GtkButton *button,
                                  gpointer user_data );

void
on_video_fast_forward_button_clicked ( GtkButton *button,
                                       gpointer user_data );

void
on_video_end_scene_button_clicked ( GtkButton *button,
                                    gpointer user_data );

void
on_video_end_movie_button_clicked ( GtkButton *button,
                                    gpointer user_data );

void
on_editor_activate ( GtkMenuItem *menuitem,
                     gpointer user_data );

void
on_capture_activate ( GtkMenuItem *menuitem,
                      gpointer user_data );

void
on_timeline_activate ( GtkMenuItem *menuitem,
                       gpointer user_data );

void
on_v4l_start_capture_button_clicked ( GtkButton *button,
                                      gpointer user_data );

void
on_v4l_stop_capture_button_clicked ( GtkButton *button,
                                     gpointer user_data );

void
on_main_window_size_allocate ( GtkWidget *widget,
                               GtkAllocation *allocation,
                               gpointer user_data );

gboolean
on_main_window_motion_notify_event ( GtkWidget *widget,
                                     GdkEventMotion *event,
                                     gpointer user_data );

gboolean
on_main_window_drag_motion ( GtkWidget *widget,
                             GdkDragContext *drag_context,
                             gint x,
                             gint y,
                             guint time,
                             gpointer user_data );

void
on_main_window_state_changed ( GtkWidget *widget,
                               GtkStateType state,
                               gpointer user_data );

gboolean
on_main_window_event ( GtkWidget *widget,
                       GdkEvent *event,
                       gpointer user_data );

gboolean
on_main_window_configure_event ( GtkWidget *widget,
                                 GdkEventConfigure *event,
                                 gpointer user_data );

gboolean
on_main_window_visibility_notify_event ( GtkWidget *widget,
        GdkEvent *event,
        gpointer user_data );

gboolean
on_main_window_event ( GtkWidget *widget,
                       GdkEvent *event,
                       gpointer user_data );

gboolean
on_main_window_map_event ( GtkWidget *widget,
                           GdkEvent *event,
                           gpointer user_data );

gboolean
on_main_window_unmap_event ( GtkWidget *widget,
                             GdkEvent *event,
                             gpointer user_data );

void
on_export_activate ( GtkMenuItem *menuitem,
                     gpointer user_data );

void
on_50percent_activate ( GtkMenuItem *menuitem,
                        gpointer user_data );

void
on_100percent_activate ( GtkWidget *menuitem,
                         gpointer user_data );

void
on_hscale_shuttle_value_changed        (GtkRange        *range,
                                        gpointer         user_data);

gboolean
on_hscale_shuttle_button_release_event ( GtkWidget *widget,
        GdkEventButton *event,
        gpointer user_data );

void
on_capture_page_record_button_clicked ( GtkButton *button,
                                        gpointer user_data );

void
on_capture_page_stop_button_clicked ( GtkButton *button,
                                      gpointer user_data );

void
on_capture_page_playrecord_button_clicked ( GtkButton *button,
        gpointer user_data );

void
on_capture_page_snapshot_button_clicked ( GtkButton *button,
        gpointer user_data );

gboolean
on_capture_page_file_entry_focus_in_event ( GtkWidget *widget,
        GdkEventFocus *event,
        gpointer user_data );

gboolean
on_capture_page_file_entry_focus_out_event ( GtkWidget *widget,
        GdkEventFocus *event,
        gpointer user_data );

void
on_capture_page_file_entry_grab_focus ( GtkWidget *widget,
                                        gpointer user_data );

gboolean
on_capture_page_file_entry_button_press_event ( GtkWidget *widget,
        GdkEventButton *event,
        gpointer user_data );

void
on_notebook_export_switch_page ( GtkNotebook *notebook,
                                 GtkNotebookPage *page,
                                 gint page_num,
                                 gpointer user_data );

gboolean
on_eventbox_capture_drawingarea_button_press_event
( GtkWidget *widget,
  GdkEventButton *event,
  gpointer user_data );
void
on_togglebutton_export_1394_preview_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_1394_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_1394_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_avi_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_avi_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_radiobutton_export_stills_all_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_radiobutton_export_stills_current_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_radiobutton_export_stills_range_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_stills_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_stills_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_hscale_export_stills_value_changed_event ( GtkWidget *widget,
        gpointer user_data );

void
on_capture_page_button_pressed ( GtkButton *button,
                                 gpointer user_data );

void
on_capture_page_button_pressed ( GtkButton *button,
                                 gpointer user_data );

void
on_capture_page_button_released ( GtkButton *button,
                                  gpointer user_data );

void
on_capture_page_mute_button_toggled ( GtkToggleButton *togglebutton,
                                      gpointer user_data );


void
on_append_movie_activate ( GtkWidget *menuitem,
                           gpointer user_data );

void
on_split_scene_activate ( GtkWidget *menuitem,
                          gpointer user_data );


void
on_button_viewsize_clicked ( GtkButton *button,
                             gpointer user_data );


void
on_togglebutton_export_audio_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_audio_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );


void
on_help_topics_activate ( GtkWidget *menuitem,
                          gpointer user_data );

void
on_undo_activate ( GtkWidget *menuitem,
                   gpointer user_data );

void
on_redo_activate ( GtkWidget *menuitem,
                   gpointer user_data );

void
on_join_scenes_activate ( GtkWidget *menuitem,
                          gpointer user_data );

gboolean
on_eventbox_trim_button_press_event ( GtkWidget *widget,
                                      GdkEventButton *event,
                                      gpointer user_data );

void
on_menuitem_trim_activate ( GtkMenuItem *menuitem,
                            gpointer user_data );

void
on_button_trim_in_reset_clicked ( GtkButton *button,
                                  gpointer user_data );

void
on_button_trim_out_reset_clicked ( GtkButton *button,
                                   gpointer user_data );


void
on_button_trim_in_set_clicked ( GtkButton *button,
                                gpointer user_data );

void
on_button_trim_out_set_clicked ( GtkButton *button,
                                 gpointer user_data );

void
on_togglebutton_trim_link_toggled ( GtkToggleButton *togglebutton,
                                    gpointer user_data );

void
on_save_as_eli1_activate ( GtkMenuItem *menuitem,
                           gpointer user_data );


void
on_checkbutton_v4l_toggled ( GtkToggleButton *togglebutton,
                             gpointer user_data );

void
on_checkbutton_v4l_toggled ( GtkToggleButton *togglebutton,
                             gpointer user_data );

void
on_v4l_capture_page_record_button_clicked
( GtkButton *button,
  gpointer user_data );

void
on_v4l_capture_page_stop_button_clicked
( GtkButton *button,
  gpointer user_data );

void
on_togglebutton_export_mjpeg_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_mjpeg_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_mjpeg_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_mjpeg_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_audio_record_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_togglebutton_export_audio_stop_toggled
( GtkToggleButton *togglebutton,
  gpointer user_data );

void
on_vbox71_unrealize ( GtkWidget *widget,
                      gpointer user_data );

void
on_vbox71_hide ( GtkWidget *widget,
                 gpointer user_data );

gboolean
on_vbox71_no_expose_event ( GtkWidget *widget,
                            GdkEventNoExpose *event,
                            gpointer user_data );

gboolean
on_drawingarea_magick_preview_visibility_notify_event
( GtkWidget *widget,
  GdkEvent *event,
  gpointer user_data );

gboolean
on_eventbox6_expose_event ( GtkWidget *widget,
                            GdkEventExpose *event,
                            gpointer user_data );

gboolean
on_eventbox6_visibility_notify_event ( GtkWidget *widget,
                                       GdkEvent *event,
                                       gpointer user_data );

void
on_entry17_changed ( GtkEditable *editable,
                     gpointer user_data );

void
on_entry_magick_dub_file_changed ( GtkEditable *editable,
                                   gpointer user_data );

void
on_notebook_magick_switch_page ( GtkNotebook *notebook,
                                 GtkNotebookPage *page,
                                 gint page_num,
                                 gpointer user_data );
void
on_togglebutton_export_preview_toggled ( GtkToggleButton *togglebutton,
        gpointer user_data );

void
on_togglebutton_export_record_toggled ( GtkToggleButton *togglebutton,
                                        gpointer user_data );

void
on_togglebutton_export_stop_toggled ( GtkToggleButton *togglebutton,
                                      gpointer user_data );

void
on_togglebutton_export_pause_toggled ( GtkToggleButton *togglebutton,
                                       gpointer user_data );

void
on_spinbutton_export_range_start_end_changed
( GtkEditable *editable,
  gpointer user_data );

void
on_vbox71_unrealize ( GtkWidget *widget,
                      gpointer user_data );

void
on_vbox71_hide ( GtkWidget *widget,
                 gpointer user_data );

gboolean
on_vbox71_no_expose_event ( GtkWidget *widget,
                            GdkEventNoExpose *event,
                            gpointer user_data );

gboolean
on_eventbox6_expose_event ( GtkWidget *widget,
                            GdkEventExpose *event,
                            gpointer user_data );

gboolean
on_eventbox6_visibility_notify_event ( GtkWidget *widget,
                                       GdkEvent *event,
                                       gpointer user_data );

void
on_entry17_changed ( GtkEditable *editable,
                     gpointer user_data );



gboolean
on_spinbutton_trim_in_focus_in_event ( GtkWidget *widget,
                                       GdkEventFocus *event,
                                       gpointer user_data );

gboolean
on_spinbutton_trim_in_focus_out_event ( GtkWidget *widget,
                                        GdkEventFocus *event,
                                        gpointer user_data );

gboolean
on_spinbutton_trim_out_focus_in_event ( GtkWidget *widget,
                                        GdkEventFocus *event,
                                        gpointer user_data );

gboolean
on_spinbutton_trim_out_focus_out_event ( GtkWidget *widget,
        GdkEventFocus *event,
        gpointer user_data );

void
on_fx1_activate ( GtkMenuItem *menuitem,
                  gpointer user_data );

void
on_command_reference_activate ( GtkMenuItem *menuitem,
                                gpointer user_data );

gboolean
on_preferences_dialog_destroy_event ( GtkWidget *widget,
                                      GdkEvent *event,
                                      gpointer user_data );

gboolean
on_preferences_dialog_destroy_event ( GtkWidget *widget,
                                      GdkEvent *event,
                                      gpointer user_data );

gboolean
on_hscale_shuttle_button_press_event ( GtkWidget *widget,
                                       GdkEventButton *event,
                                       gpointer user_data );


void
on_capture_page_button_pressed ( GtkButton *button,
                                 gpointer user_data );

void
on_capture_page_button_released ( GtkButton *button,
                                  gpointer user_data );

void
on_time_format_smpte_activate ( GtkMenuItem *menuitem,
                                gpointer user_data );

void
on_time_format_frames_activate ( GtkMenuItem *menuitem,
                                 gpointer user_data );

void
on_time_format_none_activate ( GtkMenuItem *menuitem,
                               gpointer user_data );

void
on_spinbutton_trim_in_value_changed ( GtkSpinButton *spinbutton,
                                      gpointer user_data );

void
on_spinbutton_trim_out_value_changed ( GtkSpinButton *spinbutton,
                                       gpointer user_data );

void
on_autosize_activate ( GtkMenuItem *menuitem,
                       gpointer user_data );


void
on_hscale_shuttle_event_after          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);


gboolean
on_eventbox_edit_drawingarea_button_press_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_capture_page_avc_button_clicked     (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_combo_trim_clip_entry_focus_in_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_combo_trim_clip_entry_focus_out_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_combo_trim_clip_entry_changed       (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_button_trim_open_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_trim_insert_before_clicked   (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_trim_insert_after_clicked    (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_entry_capture_file_focus_in_event   (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_entry_capture_file_focus_out_event  (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_capture_file_button_clicked         (GtkButton       *button,
                                        gpointer         user_data);


void
on_entry_capture_file_grab_focus       (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_button_export_avi_file_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_export_stills_file_clicked   (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_export_audio_file_clicked    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_export_mjpeg_file_clicked    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_export_pipe_file_clicked     (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_v4l_file_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_save_still_frame_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_publish_project_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_publish_still_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);


void
on_menuitem_v4l_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_menuitem_trim_update_activate       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_menuitem_trim_insert_activate       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_button_trim_apply_clicked           (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_main_window_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_main_window_scroll_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_entry_focus_in_event                (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_entry_focus_out_event               (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_time_format_clock_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_time_format_ms_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_time_format_s_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_time_format_min_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_time_format_h_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_entry_timeline_start_focus_out_event
                                        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_start_spin_value_changed            (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_entry_timeline_start_activate       (GtkEntry        *entry,
                                        gpointer         user_data);

gboolean
on_entry_timeline_end_focus_out_event  (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_timeline_end_activate         (GtkEntry        *entry,
                                        gpointer         user_data);

void
on_end_spin_value_changed              (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

gboolean
on_entry_trim_in_focus_out_event       (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_trim_in_activate              (GtkEntry        *entry,
                                        gpointer         user_data);

gboolean
on_entry_trim_out_focus_out_event      (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_trim_out_activate             (GtkEntry        *entry,
                                        gpointer         user_data);

gboolean
on_entry_export_start_focus_out_event  (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_export_start_activate         (GtkEntry        *entry,
                                        gpointer         user_data);

gboolean
on_entry_export_end_focus_out_event    (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_export_end_activate           (GtkEntry        *entry,
                                        gpointer         user_data);

void
on_spinbutton_export_range_start_value_changed
                                        (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_spinbutton_export_range_end_value_changed
                                        (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_preferences_dialog_help_button_clicked
                                        (GtkButton       *button,
                                        gpointer         user_data);

void
on_iconview_timeline_item_activated    (GtkIconView     *iconview,
                                        GtkTreePath     *path,
                                        gpointer         user_data);

void
on_expander_properties_activate        (GtkExpander     *expander,
                                        gpointer         user_data);

#include <gtk/gtk.h>

void
on_togglebutton_magick_start_toggled ( GtkToggleButton *togglebutton,
                                       gpointer user_data );


gboolean
on_hscale_transition_start_button_press_event
( GtkWidget *widget,
  GdkEventButton *event,
  gpointer user_data );

gboolean
on_hscale_transition_start_button_release_event
( GtkWidget *widget,
  GdkEventButton *event,
  gpointer user_data );

gboolean
on_hscale_transition_end_button_press_event
( GtkWidget *widget,
  GdkEventButton *event,
  gpointer user_data );

gboolean
on_hscale_transition_end_button_release_event
( GtkWidget *widget,
  GdkEventButton *event,
  gpointer user_data );

void
on_button_magick_file_clicked          (GtkButton       *button,
                                        gpointer         user_data);

void
on_spinbutton_magick_start_value_changed
                                        (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_spinbutton_magick_end_value_changed (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

gboolean
on_entry_magick_start_focus_out_event  (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_magick_start_activate         (GtkEntry        *entry,
                                        gpointer         user_data);

gboolean
on_entry_magick_end_focus_out_event    (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_entry_magick_end_activate           (GtkEntry        *entry,
                                        gpointer         user_data);

#ifdef __cplusplus
}
#endif

#endif

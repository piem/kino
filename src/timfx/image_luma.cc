// timfx 
// Copyright 2002, Timothy M. Shead <tshead@k-3d.com>
// Copyright 2006-2007 Dan Dennedy <dan@dennedy.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "image_luma.h"
#include "../kino_plugin_types.h"
#include "../image_transitions.h"
#include "../kino_extra.h"

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib/gi18n.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

extern "C" {
	extern GladeXML *m_glade;
}

namespace
{

class invert_luma
{
public:
	void operator()(kino::basic_luma<double>& RHS)
	{
		RHS.luma = 1.0 - RHS.luma;
	}
};
	
class image_luma :
	public GDKImageTransition
{
public:
	image_luma() :
		m_filepath(std::string( DATADIR "/" PACKAGE "/lumas" )),
		m_softness(0.2),
		m_interlaced(true),
		m_even_field_first(true)
	{
 		m_window = glade_xml_get_widget( m_glade, "image_luma" );
		GtkWidget *widget = glade_xml_get_widget( m_glade, "filechooserbutton_image_luma" );
		gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER( widget ), m_filepath.c_str() );
		gtk_file_chooser_set_filename( GTK_FILE_CHOOSER( widget ), ( m_filepath + "/bar_left.png" ).c_str()  );
		g_signal_connect( G_OBJECT( widget ), "file-activated", G_CALLBACK( Repaint ), 0 );
		widget = glade_xml_get_widget( m_glade, "spinbutton_image_luma_softness" );
		g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
		widget = glade_xml_get_widget( m_glade, "checkbutton_image_luma_interlace" );
		g_signal_connect( G_OBJECT( widget ), "toggled", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~image_luma()
	{
		gtk_widget_destroy( m_window );
	}

	char *GetDescription( ) const
	{
		return _("Luma Wipe");
	}

	void GetFrame( uint8_t *io, uint8_t *mesh, int width, int height, double position, double frame_delta, bool reverse )
	{
		GtkWidget* widget = glade_xml_get_widget( m_glade, "spinbutton_image_luma_softness" );
		m_softness = gtk_spin_button_get_value( GTK_SPIN_BUTTON( widget ) ) / 100;

		widget = glade_xml_get_widget( m_glade, "checkbutton_image_luma_interlace" );
		m_interlaced = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		
		if ( !m_luma.data() )
		{
			GError *gerr = NULL;

			// Load the luma definition from file
			kino::raii<GdkPixbuf> raw_image( gdk_pixbuf_new_from_file( m_filepath.c_str(), &gerr ),
				reinterpret_cast< void (*)(GdkPixbuf*) >( g_object_unref ) );
			if ( !raw_image.get() )
				throw( _("failed to load luma image from file") );

			// Scale the luma definition to the frame
			kino::raii<GdkPixbuf> scaled_image( gdk_pixbuf_scale_simple( raw_image.get(), width, height, GDK_INTERP_HYPER ),
				reinterpret_cast< void (*)(GdkPixbuf*) >( g_object_unref ) );

			// Cache the image luma as doubles, normalized in the range [0, 1]
			m_luma.reset( width, height );
			std::copy( reinterpret_cast< kino::basic_rgb<uint8_t>* >( gdk_pixbuf_get_pixels( scaled_image.get() ) ),
				reinterpret_cast< kino::basic_rgb<uint8_t>* >( gdk_pixbuf_get_pixels( scaled_image.get() ) +
					height * gdk_pixbuf_get_rowstride( scaled_image.get() ) ),
				m_luma.begin() );
			if ( reverse )
				std::for_each( m_luma.begin(), m_luma.end(), invert_luma() );
		}
	
		const kino::basic_rgb<uint8_t>* a = reinterpret_cast<kino::basic_rgb<uint8_t>*>(io);
		const kino::basic_rgb<uint8_t>* b = reinterpret_cast<kino::basic_rgb<uint8_t>*>(mesh);
		kino::basic_rgb<uint8_t>* output = reinterpret_cast<kino::basic_rgb<uint8_t>*>(io);

		for( int field = 0; field < (m_interlaced ? 2 : 1); ++field )
		{
			// Offset the position based on which field we're looking at ...
			const double field_position = position + ((m_even_field_first ? 1 - field : field) * frame_delta * 0.5);

			// Scale the position so that the entire wipe will be complete (including the "soft" edge) by the time we finish ...
			const double adjusted_position = kino::lerp(0.0, 1.0 + m_softness, field_position);

			// For each row ...
			for( int row = field; row < height; row += (m_interlaced ? 2 : 1) )
			{
				const kino::basic_rgb<uint8_t>* pixel_a = &a[row * width];
				const kino::basic_rgb<uint8_t>* pixel_b = &b[row * width];
				const kino::basic_luma<double>* pixel_luma = &m_luma.data()[row * width];
				kino::basic_rgb<uint8_t>* pixel_output = &output[row * width];

				for( int column = 0; column < width; ++column )
				{
					const double mix = kino::smoothstep(pixel_luma->luma, pixel_luma->luma + m_softness, adjusted_position);
					
					pixel_output->red = kino::lerp(pixel_a->red, pixel_b->red, mix);
					pixel_output->green = kino::lerp(pixel_a->green, pixel_b->green, mix);
					pixel_output->blue = kino::lerp(pixel_a->blue, pixel_b->blue, mix);
					
					++pixel_a;
					++pixel_b;
					++pixel_luma;
					++pixel_output;
				}
			}
		}
	}

	void AttachWidgets(GtkBin* bin)
	{
		gtk_widget_reparent( ( GTK_BIN( m_window ) )->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets(GtkBin* bin)
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( m_window ) );
	}

	void InterpretWidgets( GtkBin *bin )
	{
		GtkWidget *widget = glade_xml_get_widget( m_glade, "filechooserbutton_image_luma" );

		m_filepath = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER( widget ) );

		m_luma.clear();
	}

private:
	std::string m_filepath;
	kino::basic_bitmap<kino::basic_luma<double> > m_luma;
	double m_softness;
	bool m_interlaced;
	bool m_even_field_first;

	GtkWidget *m_window;
};

} // namespace

GDKImageTransition* image_luma_factory()
{
	return new image_luma();
}

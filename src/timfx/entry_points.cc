// timfx 
// Copyright 2002, Timothy M. Shead <tshead@k-3d.com>
// Copyright 2006-2007 Dan Dennedy <dan@dennedy.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "blur.h"
#include "color_hold.h"
#include "image_luma.h"
#include "soft_focus.h"
#include "../image_filters.h"
#include "../image_transitions.h"

extern "C"
{

GDKImageFilter* GetImageFilter(int Index)
{
	switch( Index )
		{
			case 0:
				return blur_factory();
			case 1:
				return color_hold_factory();
			case 2:
				return soft_focus_factory();
		}

	return NULL;
}

GDKImageTransition* GetImageTransition(int Index)
{
	switch( Index )
		{
			case 0:
				return image_luma_factory();
		}

	return NULL;
}

} // extern "C"



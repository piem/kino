// timfx 
// Copyright 2002, Timothy M. Shead <tshead@k-3d.com>
// Copyright 2006-2007 Dan Dennedy <dan@dennedy.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "blur.h"
#include "../kino_plugin_types.h"
#include "../image_filters.h"
#include "../kino_extra.h"

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>

#include <deque>
#include <numeric>
#include <vector>

GladeXML *m_glade = glade_xml_new( DATADIR "/" PACKAGE "/timfx.glade", NULL, NULL );

namespace
{

class blur :
	public GDKImageFilter
{
public:
	blur() :
		m_radius(5),
		m_horizontal(true),
		m_vertical(true)
	{
		m_window = glade_xml_get_widget( m_glade, "blur" );
		GtkWidget *widget = glade_xml_get_widget( m_glade, "spinbutton_blur_radius" );
		g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~blur()
	{
		gtk_widget_destroy( m_window );
	}

	char *GetDescription( ) const
	{
		return _("Blur");
	}

	void FilterFrame(uint8_t* pixels, int width, int height, double position, double frame_delta)
	{
		GtkWidget *widget = glade_xml_get_widget( m_glade, "spinbutton_blur_radius" );
		m_radius = gtk_spin_button_get_value_as_int( GTK_SPIN_BUTTON( widget ) );

		widget = glade_xml_get_widget( m_glade, "checkbutton_blur_horizontal" );
		m_horizontal = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );

		widget = glade_xml_get_widget( m_glade, "checkbutton_blur_vertical" );
		m_vertical = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( widget ) );
		
		// Create an N-pixel filter kernel
		kino::convolve_filter<kino::basic_rgb<double> > filter;
		
		unsigned int n = m_radius * 2;
		
/* Gaussian distribution		
		for(unsigned int i = 0; i <= n; ++i)
			filter.push_weight(factorial(n) / (factorial(i) * factorial(n - i)));
*/

/* Box distribution 
		for(unsigned int i = 0; i <= n; ++i)
			filter.push_weight(1);
*/

/* Triangle distribution */
		for(unsigned int i = 0; i <= n; ++i)
			filter.push_weight((n * 0.5) - fabs(i - (n * 0.5)));

		// Filter horizontally ...
		if(m_horizontal)
			{
				for(int row = 0; row < height; ++row)
					{
						kino::basic_rgb<uint8_t>* const a = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels) + (row * width);
						kino::basic_rgb<uint8_t>* const b = a + filter.neighbors();
						kino::basic_rgb<uint8_t>* const c = a + width - filter.neighbors();
						kino::basic_rgb<uint8_t>* const d = a + width;
						
						// Initialize the kernel ...
						for(const kino::basic_rgb<uint8_t>* neighbor = a; neighbor != b; ++neighbor)
							filter.push_value(*neighbor);
						
						// First border zone ...
						unsigned int start = filter.middle();
						for(kino::basic_rgb<uint8_t>* current = a; current != b; ++current, --start)
							{
								filter.push_value(*(current + filter.neighbors()));
								*current = filter.get_value(start, filter.width());
							}

						// Image body ...
						for(kino::basic_rgb<uint8_t>* current = b; current != c; ++current)
							{
								filter.push_value(*(current + filter.neighbors()));
								*current = filter.get_value();
							}

						// Second border zone ...
						unsigned int remaining_width = filter.width()-1;
						for(kino::basic_rgb<uint8_t>* current = c; current != d; ++current, --remaining_width)
							{
								filter.push_value(kino::basic_rgb<uint8_t>());
								*current = filter.get_value(0, remaining_width);
							}
					}
			}

		// Filter vertically ...
		if(m_vertical)
			{
				for(int column = 0; column < width; ++column)
					{
						kino::basic_rgb<uint8_t>* const a = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels) + column;
						kino::basic_rgb<uint8_t>* const b = a + filter.neighbors() * width;
						kino::basic_rgb<uint8_t>* const c = a + (height - filter.neighbors()) * width;
						kino::basic_rgb<uint8_t>* const d = a + height * width;
						
						// Initialize the kernel ...
						for(const kino::basic_rgb<uint8_t>* neighbor = a; neighbor != b; neighbor += width)
							filter.push_value(*neighbor);
						
						// First border zone ...
						unsigned int start = filter.middle();
						for(kino::basic_rgb<uint8_t>* current = a; current != b; current += width, --start)
							{
								filter.push_value(*(current + filter.neighbors() * width));
								*current = filter.get_value(start, filter.width());
							}

						// Image body ...
						for(kino::basic_rgb<uint8_t>* current = b; current != c; current += width)
							{
								filter.push_value(*(current + filter.neighbors() * width));
								*current = filter.get_value();
							}

						// Second border zone ...
						unsigned int remaining_width = filter.width() - 1;
						for(kino::basic_rgb<uint8_t>* current = c; current != d; current += width, --remaining_width)
							{
								filter.push_value(kino::basic_rgb<uint8_t>());
								*current = filter.get_value(0, remaining_width);
							}
					}
			}
	}

	void AttachWidgets(GtkBin* bin)
	{
		gtk_widget_reparent( ( GTK_BIN( m_window ) )->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets(GtkBin* bin)
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( m_window ) );
	}

private:
	unsigned int m_radius;
	bool m_horizontal;
	bool m_vertical;

	GtkWidget *m_window;
};

} // namespace

GDKImageFilter* blur_factory()
{
	return new blur();
}

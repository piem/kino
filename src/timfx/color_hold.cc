// timfx 
// Copyright 2002, Timothy M. Shead <tshead@k-3d.com>
// Copyright 2006-2007 Dan Dennedy <dan@dennedy.org>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "color_hold.h"
#include "../kino_plugin_types.h"
#include "../image_filters.h"
#include "../kino_extra.h"

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <set>
#include <string>
#include <vector>

extern "C" {
	extern GladeXML *m_glade;
}

namespace
{

class color_hold :
	public GDKImageFilter
{
public:
	color_hold() :
		m_color(0, 1, 1),
		m_tolerance(0.1),
		m_softness(0.1)
	{
		m_window = glade_xml_get_widget( m_glade, "color_hold" );

		GtkWidget *widget = glade_xml_get_widget( m_glade, "spinbutton_color_hold_tolerance" );
		g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
		widget = glade_xml_get_widget( m_glade, "spinbutton_color_hold_threshold" );
		g_signal_connect( G_OBJECT( widget ), "value-changed", G_CALLBACK( Repaint ), 0 );
		widget = glade_xml_get_widget( m_glade, "colorselection_color_hold" );
		g_signal_connect( G_OBJECT( widget ), "color-changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~color_hold()
	{
		gtk_widget_destroy( m_window );
	}

	char *GetDescription( ) const
	{
		return _("Colour Hold");
	}

	void FilterFrame(uint8_t* pixels, int width, int height, double position, double frame_delta)
	{
		GdkColor color;
		GtkWidget *widget = glade_xml_get_widget( m_glade, "colorselection_color_hold" );

		gtk_color_selection_get_current_color( GTK_COLOR_SELECTION( widget ), &color );
		m_color = kino::basic_rgb<double>(color.red, color.green, color.blue);
	
		widget = glade_xml_get_widget( m_glade, "spinbutton_color_hold_tolerance" );
		m_tolerance = gtk_spin_button_get_value( GTK_SPIN_BUTTON( widget ) ) / 100;

		widget = glade_xml_get_widget( m_glade, "spinbutton_color_hold_threshold" );
		m_softness = gtk_spin_button_get_value( GTK_SPIN_BUTTON( widget ) ) / 100;
		
		// For each pixel in the source data ...
		const kino::basic_rgb<uint8_t>* const end = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels) + width * height;
		for(kino::basic_rgb<uint8_t>* pixel = reinterpret_cast<kino::basic_rgb<uint8_t>*>(pixels); pixel != end; ++pixel)
			{
				const uint8_t weighted_luma = kino::color_traits<uint8_t>::convert(0.299 * kino::color_traits<double>::convert(pixel->red) + 0.587 * kino::color_traits<double>::convert(pixel->green) + 0.114 * kino::color_traits<double>::convert(pixel->blue));
			
				kino::basic_hsv hsv_pixel(*pixel);

				double d = m_color.hue - hsv_pixel.hue;
				while(d < -180)
					d += 360;
				while(d > 180)
					d -= 360;
							
				d /= 180;
				
				d = fabs(d);
				
				d = kino::linearstep(m_tolerance, m_tolerance + m_softness, d);
				
				pixel->red = kino::lerp(pixel->red, weighted_luma, d);
				pixel->green = kino::lerp(pixel->green, weighted_luma, d);
				pixel->blue = kino::lerp(pixel->blue, weighted_luma, d);
			}
	}

	void AttachWidgets(GtkBin* bin)
	{
		gtk_widget_reparent( ( GTK_BIN( m_window ) )->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets(GtkBin* bin)
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) )->child, GTK_WIDGET( m_window ) );
	}

private:
	kino::basic_hsv m_color;
	double m_tolerance;
	double m_softness;

	GtkWidget *m_window;
};

} // namespace

GDKImageFilter* color_hold_factory()
{
	return new color_hold();
}

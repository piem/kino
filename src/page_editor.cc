/*
* page_edit.cc Notebook Editor Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include "page_editor.h"
#include "frame.h"
#include "commands.h"
#include "sys/time.h"
#include "pthread.h"
#include "message.h"
#include "filehandler.h"
#include "ieee1394io.h"
#include "gtkenhancedscale.h"
#include "callbacks.h"
#include "storyboard.h"

#undef PLAY_WITH_STATS

extern "C"
{
#include "support.h"

	extern KinoCommon *common;
	extern struct navigate_control g_nav_ctl;
	char cmd[ 256 ] = { 0 };
	char lastcmd[ 256 ] = { 0 };
	static int _getOneSecond( void );

	static void resetThreads( );
	static void *readThread( void * info );
	static void *audioThread( void * info );
	static void *videoThread( void * info );
	static pthread_t readthread = 0;
	static pthread_t audiothread = 0;
	static pthread_t videothread = 0;
	static pthread_mutex_t threadlock = PTHREAD_MUTEX_INITIALIZER;
	static pthread_mutex_t readlock = PTHREAD_MUTEX_INITIALIZER;
	static int newFrame = 0;
	static int lastFrame = -1;

#define PLAYBACK_FRAMES 50

	static Frame *frameContent[ PLAYBACK_FRAMES ];
	IEEE1394Writer *writer1394 = NULL;

	static gboolean doScrub = FALSE;
	gboolean
	on_scrub_bar_button_press_event ( GtkWidget * widget,
	                                  GdkEventButton * event,
	                                  gpointer user_data )
	{
		doScrub = TRUE;
		videoPause();
		return FALSE;
	}


	gboolean
	on_scrub_bar_button_release_event ( GtkWidget * widget,
	                                    GdkEventButton * event,
	                                    gpointer user_data )
	{
		doScrub = FALSE;
		return FALSE;
	}

	gboolean
	on_scrub_bar_value_changed_event ( GtkWidget * widget,
	                                   GdkEventButton * event,
	                                   gpointer user_data )
	{
		if ( doScrub == TRUE )
			moveToFrame( ( int ) GTK_ADJUSTMENT( widget ) ->value );
		return FALSE;
	}

}

/** Constructor for the page editor object.
	
  	\param common	KinCommon object to which this page belongs
*/

PageEditor::PageEditor( KinoCommon *common )
{
	cerr << "> Creating page editor" << endl;
	this->common = common;

	this->frameArea = GTK_DRAWING_AREA( lookup_widget( common->getWidget(), "drawingarea1" ) );
	gtk_widget_set_double_buffered( GTK_WIDGET( frameArea ), FALSE );
	this->positionLabelCurrent = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_current" ) );
	this->positionLabelTotal = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_total" ) );
	this->lastFrameShown = -1;
	this->g_copiedPlayList = new PlayList();

	scrubAdjustment = GTK_ADJUSTMENT( gtk_adjustment_new( 0, 0, 0, 1, 10, 0 ) );
	g_signal_connect( G_OBJECT( scrubAdjustment ), "value_changed",
	                  G_CALLBACK( on_scrub_bar_value_changed_event ), NULL );

	scrubBar = gtk_enhanced_scale_new( ( GtkObject** ) & scrubAdjustment, 1 );
	sceneIndex = g_array_new( FALSE, FALSE, sizeof( int ) );
	gtk_widget_set_name( scrubBar, "scrubBar" );
	gtk_widget_ref( scrubBar );
	gtk_object_set_data_full( GTK_OBJECT( common->getWidget() ), "scrubBar", scrubBar,
	                          ( GtkDestroyNotify ) gtk_widget_unref );
	GtkWidget *vbox_edit = lookup_widget( common->getWidget(), "vbox58" );
	gtk_widget_show( scrubBar );
	gtk_box_pack_start( GTK_BOX( vbox_edit ), scrubBar, FALSE, TRUE, 0 );
	g_signal_connect( G_OBJECT( scrubBar ), "button_press_event",
	                  G_CALLBACK( on_scrub_bar_button_press_event ), NULL );
	g_signal_connect( G_OBJECT( scrubBar ), "button_release_event",
	                  G_CALLBACK( on_scrub_bar_button_release_event ), NULL );

}

/** Destructor for the page editor object.
*/

PageEditor::~PageEditor( )
{
	cerr << "> Destroying page editor" << endl;
	delete this->g_copiedPlayList;
	if ( sceneIndex != NULL )
		g_array_free( sceneIndex, TRUE );
}

/** New File action.
*/

void PageEditor::newFile()
{
	this->stopNavigator();
	common->loadSplash( frameArea );
	this->ResetBar( );
}

/** Start action. Called when the page becomes current.
*/

void PageEditor::start()
{
	cerr << ">> Starting Editor" << endl;
	
	for ( int i = 0; i < PLAYBACK_FRAMES; i ++ )
		frameContent[ i ] = GetFramePool( ) ->GetFrame( );

	this->displayer = new FrameDisplayer();
	if ( Preferences::getInstance().dv1394Preview )
	{
#ifdef HAVE_IEC61883
		AVC avc;
		Preferences::getInstance( ).phyID = avc.getNodeId( Preferences::getInstance( ).avcGUID );
		Preferences::getInstance( ).phyID = avc.isPhyIDValid( Preferences::getInstance( ).phyID );
		if ( !writer1394 )
			writer1394 = new iec61883Writer(
			             ( avc.getPort() < 0 ) ? 0 : avc.getPort(),
			             Preferences::getInstance().channel,
			             2 );
#else
		if ( Preferences::getInstance().dvExportDevice > 0 && !writer1394 )
			writer1394 = new dv1394Writer(
			             Preferences::getInstance().dvExportDevice,
			             Preferences::getInstance().channel,
			             2,
			             Preferences::getInstance().cip_n,
			             Preferences::getInstance().cip_d,
			             Preferences::getInstance().syt_offset );
#endif
	}

	this->lastFrameShown = common->g_currentFrame;

	gtk_widget_grab_focus( GTK_WIDGET( frameArea ) );
	common->packIt( "packer_edit", "packer_edit_outer" );

	gtk_notebook_set_page( GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_keyhelp" ) ), 0 );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_editor" ) ), TRUE );
}

/** Activate the returned widgets.
*/

gulong PageEditor::activate()
{
	return EDIT_MENU |
	       SCENE_LIST |
	       VIDEO_START_OF_MOVIE |
	       VIDEO_START_OF_SCENE |
	       VIDEO_REWIND |
	       VIDEO_BACK |
	       VIDEO_PLAY |
	       VIDEO_PAUSE |
	       VIDEO_STOP |
	       VIDEO_FORWARD |
	       VIDEO_FAST_FORWARD |
	       VIDEO_NEXT_SCENE |
	       VIDEO_END_OF_MOVIE |
	       VIDEO_SHUTTLE |
	       INFO_FRAME;
}

/** Clean action. Called when another page becomes current.
*/

void PageEditor::clean()
{
	cerr << ">> Leaving Editor" << endl;
	stopNavigator();
	delete displayer;
	displayer = 0;
	delete writer1394;
	writer1394 = NULL;
	for ( int i = 0; i < PLAYBACK_FRAMES; i ++ )
		GetFramePool( ) ->DoneWithFrame( frameContent[ i ] );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_editor" ) ), FALSE );
	cerr << ">> Left Editor" << endl;
}

/** Starts the navigator thread. See comments on g_nav_ctl.
*/

void PageEditor::startNavigator()
{
	stopNavigator( );
	g_nav_ctl.active = TRUE;
	resetThreads( );
	pthread_create( &readthread, NULL, readThread, &g_nav_ctl );
	pthread_create( &audiothread, NULL, audioThread, &g_nav_ctl );
	pthread_create( &videothread, NULL, videoThread, &g_nav_ctl );
}

/** Stops the navigator thread. See comments on g_nav_ctl.
*/

void PageEditor::stopNavigator()
{
	if ( g_nav_ctl.active )
	{
		g_nav_ctl.active = FALSE;
		gdk_threads_leave();
		pthread_join( readthread, NULL );
		pthread_join( audiothread, NULL );
		pthread_join( videothread, NULL );
		gdk_threads_enter();
		getFrameDisplayer()->CloseSound();
	}
}

/** Called when the current frame has changed through the common moveToFrame
	method and this page is the current page.

	\param	frame	frame moved to
*/

void PageEditor::movedToFrame( int frame )
{
	if ( g_nav_ctl.active == FALSE )
	{
		showFrame( frame, ( frame == lastFrameShown ) || ( Preferences::getInstance().audioScrub == FALSE ) );
	}
	else
	{
		stopNavigator();
		common->g_currentFrame = frame;
		startNavigator();
	}
}

/** Show the frame requested

  	\param i		frame to be shown
	\param no_audio	indicate if audio is required or not
*/

void PageEditor::showFrame( int i, gboolean no_audio )
{
	DrawBar( i );
	if ( common->getPlayList() ->GetNumFrames() == 0 )
	{
		common->loadSplash( frameArea );
		common->showFrameInfo( 0 );
	}
	else
	{
		Frame *frame = GetFramePool( ) ->GetFrame( );
		common->getPlayList() ->GetFrame( i, *frame );
		common->showFrameInfo( i );
		if ( writer1394 != NULL )
			writer1394->SendFrame( *frame, false ) ;
		getFrameDisplayer() ->Put( *frame, GTK_WIDGET( frameArea ), no_audio );
		lastFrameShown = i;
		GetFramePool( ) ->DoneWithFrame( frame );
	}
}

/** Move to the start of the movie.
*/

void PageEditor::videoStartOfMovie()
{
	common->moveToFrame( 0 );
	common->toggleComponents( VIDEO_START_OF_MOVIE, false );
}

/** Move to the start of the previous scene.
*/

void PageEditor::videoPreviousScene()
{
	int frame = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );

	if ( g_nav_ctl.active == FALSE && frame == common->g_currentFrame )
		frame = common->getPlayList() ->FindStartOfScene( frame - 1 );
	else if ( g_nav_ctl.active == TRUE && ( frame == common->g_currentFrame || ( common->g_currentFrame - frame ) <= 15 ) )
		frame = common->getPlayList() ->FindStartOfScene( frame - 1 );

	common->moveToFrame( frame );
	common->toggleComponents( VIDEO_START_OF_SCENE, false );
}

/** Move to the start of the current scene.
*/

void PageEditor::videoStartOfScene()
{
	int frame = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
	common->moveToFrame( frame );
	common->toggleComponents( VIDEO_START_OF_SCENE, false );
}

/** Rewind.
*/

void PageEditor::videoRewind()
{
	common->toggleComponents( common->getComponentState(), false );
	stopNavigator();
	// Toggle Rewind state
	if ( g_nav_ctl.step == -10 )
	{
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		common->toggleComponents( VIDEO_REWIND, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
	else
	{
		common->toggleComponents( VIDEO_REWIND, true );
		g_nav_ctl.step = -10;
		g_nav_ctl.rate = 0;
		startNavigator();
	}
}

/** Move one frame back. If the navigator is active, then this action toggles
	between reverse and stop.
*/

void PageEditor::videoBack(int step)
{
	common->toggleComponents( common->getComponentState(), false );
	if ( g_nav_ctl.active )
	{
		stopNavigator();
		if ( g_nav_ctl.step != -1 )
		{
			g_nav_ctl.step = -1;
			g_nav_ctl.rate = 1;
			common->toggleComponents( VIDEO_BACK, true );
			startNavigator();
		}
		else
		{
			g_nav_ctl.step = 1;
			g_nav_ctl.rate = 0;
			common->toggleComponents( VIDEO_BACK, false );
			common->toggleComponents( VIDEO_STOP, true );
		}
	}
	else
	{
		common->moveByFrames( step );
		common->toggleComponents( VIDEO_BACK, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Play.
*/

void PageEditor::videoPlay()
{
	common->toggleComponents( common->getComponentState(), false );
	if ( g_nav_ctl.active == FALSE || g_nav_ctl.step != 1 )
	{
		common->toggleComponents( VIDEO_PLAY, true );
		stopNavigator();
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		startNavigator();
	}
	else
	{
		stopNavigator();
		common->toggleComponents( VIDEO_PLAY, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Pause.
*/

void PageEditor::videoPause()
{
	videoStop();
}

/** Stop.
*/

void PageEditor::videoStop()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_STOP, true );
	stopNavigator();
	g_nav_ctl.step = 0;
	g_nav_ctl.rate = 0;
}

/** Move one frame forward. If the navigator is active, then this action puts
	the video into normal play.
*/

void PageEditor::videoForward(int step)
{
	
	common->toggleComponents( common->getComponentState(), false );
	if ( g_nav_ctl.active && g_nav_ctl.step != 1 )
	{
		common->toggleComponents( VIDEO_FORWARD, true );
		stopNavigator();
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		startNavigator();
	}
	else
	{
		stopNavigator();
		common->moveByFrames( step );
		common->toggleComponents( VIDEO_FORWARD, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Fast forward.
*/

void PageEditor::videoFastForward()
{
	common->toggleComponents( common->getComponentState(), false );
	stopNavigator();
	if ( g_nav_ctl.step != 10 )
	{
		common->toggleComponents( VIDEO_FAST_FORWARD, true );
		g_nav_ctl.step = 10;
		g_nav_ctl.rate = 0;
		startNavigator();
	}
	else
	{
		g_nav_ctl.step = 1;
		g_nav_ctl.rate = 1;
		common->toggleComponents( VIDEO_FAST_FORWARD, false );
		common->toggleComponents( VIDEO_STOP, true );
	}
}

/** Shuttle

	Bi-directionaly variable-speed playback.

	\param angle A number from -15 (fastest reverse) to 15 (fastest forward), 0=stop
*/

void PageEditor::videoShuttle( int angle )
{
	int frames_sec = _getOneSecond();
	int speedTable[] = {
			0,
			8,  10, 15, 20, 33, 50, 75,
			100,
			200, 300, 400, 500, 800, 1200,
			( frames_sec * 100 ) };
	char s[ 64 ];

	if ( angle < -15 )
		angle = -15;
	if ( angle > 15 )
		angle = 15;

	int speed = speedTable[ ( angle < 0 ) ? -angle : angle ] * ( ( angle < 0 ) ? -1 : 1 );
	if ( speed == 0 || g_nav_ctl.step != speed / 100 || g_nav_ctl.rate != 100 / speed )
	{
		stopNavigator( );
		if ( speed == 0 )
		{
			common->keyboardFeedback( "", "" );
			common->toggleComponents( VIDEO_STOP, true );
		}
		else
		{
			g_nav_ctl.step = speed / 100;
			g_nav_ctl.rate = 100 / speed;
			g_nav_ctl.subframe = 0;
			startNavigator();
			snprintf( s, 63, _( "Shuttle %+.1f fps" ), ( float ) speed / 100.0 * frames_sec );
			common->keyboardFeedback( "", s );
		}
	}
}

/** Move to the start of the next scene.
*/

void PageEditor::videoNextScene()
{
	int frame = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
	common->moveToFrame( frame + 1 );
	common->toggleComponents( VIDEO_NEXT_SCENE, false );
}

/** Move to the end of the current scene.
*/

void PageEditor::videoEndOfScene()
{
	int frame = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
	common->moveToFrame( frame );
	common->toggleComponents( VIDEO_NEXT_SCENE, false );
}

/** Move to the end of the play list.
*/

void PageEditor::videoEndOfMovie()
{
	common->toggleComponents( common->getComponentState(), false );
	common->toggleComponents( VIDEO_END_OF_MOVIE, true );
	stopNavigator();
	common->moveToFrame( common->getPlayList() ->GetNumFrames() - 1 );
	common->toggleComponents( VIDEO_END_OF_MOVIE, false );
	common->toggleComponents( VIDEO_STOP, true );
}

/** Move to start of selected scene.

	\param i	scene to move to
*/

void PageEditor::selectScene( int i )
{
	vector <int> scene = GetScene();
	int value = i == 0 ? 0 : scene[ i - 1 ];
	common->moveToFrame( value );
}

/** Process a keyboard event.

  	\param event	keyboard event
*/

gboolean PageEditor::processKeyboard( GdkEventKey *event )
{
	gboolean ret = FALSE;

	// Only process while not escape mode
	if ( g_nav_ctl.escaped == FALSE )
	{
		if ( strcmp( lastcmd, "alt" ) == 0 )
		{
			strcpy( lastcmd, "" );
			return ret;
		}

		// Translate special keys to equivalent command
		switch ( event->keyval )
		{
		case GDK_Home:
			strcat( cmd, "gg");
			ret = TRUE;
			break;
		case GDK_End:
			strcat( cmd, "G");
			ret = TRUE;
			break;
		case GDK_Page_Up:
			strcat( cmd, "5^" );
			ret = TRUE;
			break;
		case GDK_Page_Down:
			strcat( cmd, "5$" );
			ret = TRUE;
			break;
		case GDK_BackSpace:
		case GDK_Left:
			strcat( cmd, "h" );
			ret = TRUE;
			break;
		case GDK_Up:
			strcat( cmd, "k" );
			ret = TRUE;
			break;
		case GDK_Right:
			strcat( cmd, "l" );
			ret = TRUE;
			break;
		case GDK_Return:
			if ( cmd[ 0 ] != 0 )
			{
				// Last command is now
				strcpy( lastcmd, cmd );

				// end the command entry
				cmd[ 0 ] = 0;
				common->setStatusBar( "" );
			}
			break;
		case GDK_Down:
			strcat( cmd, "j" );
			ret = TRUE;
			break;
		case GDK_Delete:
			strcat( cmd, "x" );
			ret = TRUE;
			break;
		case GDK_Escape:
			common->keyboardFeedback( cmd, _( "Stop" ) );
			common->videoStop( );
			cmd[ 0 ] = 0;
			return ret;
		case GDK_Alt_L:
		case GDK_Alt_R:
			strcpy( lastcmd, "alt" );
			return ret;
		default:
			if ( strcmp( event->string, "." ) )
				strcat( cmd, event->string );
			break;
		}

		if ( !strcmp( event->string, "." ) )
			strcpy( cmd, lastcmd );
		else if ( cmd[ 0 ] == 0x06)   // Ctrl+f
			strcpy( cmd, "5$" );
		else if ( cmd[ 0 ] == 0x02 )   // Ctrl+b
			strcpy( cmd, "5^" );
		else if ( cmd[ 0 ] == 0x12 )
			strcpy( cmd, "Ctrl+R" );

#if 0
		printf( "send_event: %2.2x\n", event->send_event );
		printf( "time  : %8.8x\n", event->time );
		printf( "state : %8.8x\n", event->state );
		printf( "keyval: %8.8x\n", event->keyval );
		printf( "length: %8.8x\n", event->length );
		printf( "string: %s\n", event->string );
		printf( "(hex) : %2.2x\n", event->string[ 0 ] );
		printf( "cmd   : %s\n", cmd );
		printf( "(hex) : %8.8x\n", cmd[ 0 ] );
		fflush( stdout );
#endif

		processCommand( cmd );
	}

	return ret;
}

/** Internal method for handling a complete keyboard scene.

  	\param cmd		command to be processed;
*/

gboolean PageEditor::processCommand( const char *command )
{
	int	start, end;
	int count = 1;
	char real[ 256 ] = "";

	if ( cmd != command )
	{
		strncpy( cmd, command, sizeof(cmd) );
		cmd[ sizeof(cmd) - 1 ] = '\0';
	}

	switch ( sscanf( cmd, "%d%s", &count, real ) )
	{
	case 1:
		// Numeric value only - return immediately if the cmd is not "0"
		if ( strcmp( cmd, "0" ) )
		{
			common->keyboardFeedback( cmd, "" );
			return FALSE;
		}
		break;
	case 0:
		sscanf( cmd, "%s", real );
		count = 1;
		break;
	}

	if ( strcmp( cmd, "." ) )
		strcpy( lastcmd, cmd );

	/* Navigation */

	/* play, pause */

	if ( strcmp( cmd, " " ) == 0 )
	{
		if ( g_nav_ctl.active == FALSE )
		{
			common->keyboardFeedback( cmd, _( "Play" ) );
			common->videoPlay( );
		}
		else
		{
			common->keyboardFeedback( cmd, _( "Pause" ) );
			common->videoPause( );
		}
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "Esc" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Stop" ) );
		common->videoStop( );
		cmd[ 0 ] = 0;
	}

	/* advance one frame */

	else if ( strcmp( real, "l" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward" ) );
		common->moveByFrames( count );
		cmd[ 0 ] = 0;
	}

	/* backspace one frame */

	else if ( strcmp( real, "h" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move backward" ) );
		common->moveByFrames( 0 - count );
		cmd[ 0 ] = 0;
	}

	/* advance one second */

	else if ( strcmp( real, "w" ) == 0 || strcmp( real, "W" ) == 0 ||
	          strcmp( real, "e" ) == 0 || strcmp( real, "E" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward second" ) );
		common->moveByFrames( count * _getOneSecond() );
		cmd[ 0 ] = 0;
	}

	/* backspace one second */

	else if ( ( strcmp( real, "b" ) == 0 ) || ( strcmp( real, "B" ) == 0 ) )
	{
		common->keyboardFeedback( cmd, _( "Move backwards one second" ) );
		common->moveByFrames( 0 - count * _getOneSecond() );
		cmd[ 0 ] = 0;
	}

	/* start of scene */

	else if ( ( strcmp( cmd, "0" ) == 0 ) || ( strcmp( real, "^" ) == 0 ) )
	{
		common->videoStartOfScene( );
		for ( ; count > 1 && common->g_currentFrame > 0; count -- )
		{
			common->g_currentFrame --;
			common->videoStartOfScene( );
		}
		common->keyboardFeedback( cmd, _( "Move to start of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* end of scene */

	else if ( strcmp( real, "$" ) == 0 )
	{
		common->videoEndOfScene( );
		for ( ; count > 1 && common->g_currentFrame < common->getPlayList() ->GetNumFrames() - 1; count -- )
		{
			common->g_currentFrame ++;
			common->videoEndOfScene( );
		}
		common->keyboardFeedback( cmd, _( "Move to end of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* start of next scene */

	else if ( ( strcmp( real, "j" ) == 0 ) || strcmp( real, "+" ) == 0 )
	{
		for ( ; count >= 1 && common->g_currentFrame < common->getPlayList() ->GetNumFrames() - 1; count -- )
			common->videoNextScene( );
		common->keyboardFeedback( cmd, _( "Move to start of next scene" ) );
		cmd[ 0 ] = 0;
	}

	/* start of previous scene */

	else if ( ( strcmp( real, "k" ) == 0 ) || ( strcmp( real, "-" ) == 0 ) )
	{
		for ( ; count >= 1 && common->g_currentFrame > 0; count -- )
			common->videoPreviousScene( );
		common->keyboardFeedback( cmd, _( "Move to start of previous scene" ) );
		cmd[ 0 ] = 0;
	}

	/* first frame */

	else if ( strcmp( cmd, "gg" ) == 0 )
	{
		common->videoStartOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to first frame" ) );
		cmd[ 0 ] = 0;
	}

	/* last frame */

	else if ( strcmp( cmd, "G" ) == 0 )
	{
		common->videoEndOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to last frame" ) );
		cmd[ 0 ] = 0;
	}

	/* delete current frame */

	else if ( ( strcmp( real, "x" ) == 0 ) || ( strcmp( cmd, "d " ) == 0 ) || ( strcmp( real, "dl" ) == 0 ) )
	{
		CopyFrames( common->g_currentFrame, common->g_currentFrame + count - 1 );
		DeleteFrames( common->g_currentFrame, common->g_currentFrame + count - 1 );
		common->moveToFrame( );
		common->keyboardFeedback( cmd, _( "Cut current frame" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete one second */

	else if ( strcmp( cmd, "dw" ) == 0 )
	{
		end = common->g_currentFrame + _getOneSecond() - 1;
		CopyFrames( common->g_currentFrame, end );
		DeleteFrames( common->g_currentFrame, end );
		common->moveToFrame( );
		common->keyboardFeedback( cmd, _( "Cut one second" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete current scene */

	else if ( strcmp( real, "dd" ) == 0 )
	{
		start = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
		end = start;
		for ( ; count >= 1 && end <= common->getPlayList() ->GetNumFrames() - 1; count -- )
		{
			end = common->getPlayList() ->FindEndOfScene( end );
			end ++;
		}
		CopyFrames( start, end - 1 );
		DeleteFrames( start, end - 1 );
		common->moveToFrame( start );
		common->keyboardFeedback( cmd, _( "Cut current scene" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete from current frame up to end of scene */

	else if ( ( strcmp( cmd, "o" ) == 0 ) || strcmp( cmd, "d$" ) == 0 )
	{
		start = common->g_currentFrame;
		if ( strcmp( cmd, "o" ) == 0 )
			++start;
		end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
		CopyFrames( start, end );
		DeleteFrames( start, end );
		common->moveToFrame( );
		common->keyboardFeedback( cmd, _( "Cut to end of scene" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete from current frame up to end of file */

	else if ( strcmp( cmd, "dG" ) == 0 )
	{
		end = common->getPlayList() ->GetNumFrames();
		CopyFrames( common->g_currentFrame, end );
		DeleteFrames( common->g_currentFrame, end );
		common->moveByFrames( -1 );
		common->keyboardFeedback( cmd, _( "Cut to end of movie" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete from start of scene just before current frame */

	else if ( ( strcmp( cmd, "i" ) == 0 ) || ( strcmp( cmd, "d0" ) == 0 ) || strcmp( cmd, "d^" ) == 0 )
	{
		start = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
		if ( start < common->g_currentFrame )
		{
			CopyFrames( start, common->g_currentFrame - 1 );
			DeleteFrames( start, common->g_currentFrame - 1 );
			common->moveToFrame( start );
		}
		common->keyboardFeedback( cmd, _( "Cut from start of scene" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* delete from start of file just before current frame */

	else if ( strcmp( cmd, "dgg" ) == 0 )
	{
		CopyFrames( 0, common->g_currentFrame - 1 );
		DeleteFrames( 0, common->g_currentFrame - 1 );
		common->moveToFrame( 0 );
		common->keyboardFeedback( cmd, _( "Cut from start of movie" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* copy current frame */

	else if ( ( strcmp( cmd, "y " ) == 0 ) || ( strcmp( real, "yl" ) == 0 ) )
	{
		CopyFrames( common->g_currentFrame, common->g_currentFrame + count - 1 );
		common->keyboardFeedback( cmd, _( "Copy current frame" ) );
		cmd[ 0 ] = 0;
	}

	/* copy current scene */

	else if ( ( strcmp( real, "yy" ) == 0 ) || ( strcmp( real, "Y" ) == 0 ) )
	{
		start = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
		end = start;
		for ( ; count >= 1; count -- )
		{
			end = common->getPlayList() ->FindEndOfScene( end );
			end ++;
		}
		CopyFrames( start, end - 1 );
		common->keyboardFeedback( cmd, _( "Copy current scene" ) );
		cmd[ 0 ] = 0;
	}

	/* copy from current frame up to end of scene */

	else if ( strcmp( cmd, "y$" ) == 0 )
	{
		end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
		CopyFrames( common->g_currentFrame, end );
		common->keyboardFeedback( cmd, _( "Copy to end of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* copy from start of scene just before current frame */

	else if ( ( strcmp( cmd, "y0" ) == 0 ) || strcmp( cmd, "y^" ) == 0 )
	{
		start = common->getPlayList() ->FindStartOfScene( common->g_currentFrame );
		if ( start < common->g_currentFrame )
		{
			CopyFrames( start, common->g_currentFrame - 1 );
			common->moveToFrame( start );
		}
		common->keyboardFeedback( cmd, _( "Copy from start of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* paste after current frame */

	else if ( strcmp( real, "p" ) == 0 )
	{
		start = common->g_currentFrame;
		for ( ; count >= 1; count -- )
			PasteFrames( common->g_currentFrame + 1 );
		common->moveToFrame( start + 1 );
		common->keyboardFeedback( cmd, _( "Paste after current frame" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* paste before current frame */

	else if ( strcmp( real, "P" ) == 0 )
	{
		for ( ; count >= 1; count -- )
			PasteFrames( common->g_currentFrame );
		end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame + 1 );
		common->moveToFrame( );
		common->keyboardFeedback( cmd, _( "Paste before current frame" ) );
		DrawBar( common->g_currentFrame );
		cmd[ 0 ] = 0;
	}

	/* Switch to capture mode */

	else if ( strcmp( cmd, "a" ) == 0 )
	{
		common->keyboardFeedback( cmd, "Capture, insert after frame" );
		end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
		common->moveToFrame( end );
		FileTracker::GetInstance().SetMode( CAPTURE_FRAME_APPEND );
		common->changePageRequest( PAGE_CAPTURE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "A" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Capture, append to movie" ) );
		end = common->getPlayList() ->GetNumFrames();
		common->moveToFrame( end );
		FileTracker::GetInstance().SetMode( CAPTURE_MOVIE_APPEND );
		common->changePageRequest( PAGE_CAPTURE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "t" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Trim" ) );
		common->changePageRequest( PAGE_TRIM );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "v" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Timeline" ) );
		common->changePageRequest( PAGE_TIMELINE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "C" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "FX" ) );
		common->changePageRequest( PAGE_MAGICK );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "u" ) == 0 )
	{
		GetEditorBackup() ->Undo( common->getPlayList() );
		common->keyboardFeedback( cmd, _( "Undo" ) );
		common->hasListChanged = TRUE;
		DrawBar( common->g_currentFrame );
		common->moveToFrame( );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "Ctrl+R" ) == 0 )
	{
		GetEditorBackup() ->Redo( common->getPlayList() );
		common->keyboardFeedback( cmd, _( "Redo" ) );
		common->hasListChanged = TRUE;
		DrawBar( common->g_currentFrame );
		common->moveToFrame( );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "J" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Join scenes" ) );
		for ( ; count >= 1 && common->getPlayList() ->JoinScenesAt( common->g_currentFrame ); count -- )
			common->hasListChanged = TRUE;
		if ( common->hasListChanged == TRUE )
		{
			common->moveToFrame();
			DrawBar( common->g_currentFrame );
			GetEditorBackup() ->Store( common->getPlayList() );
			cmd[ 0 ] = 0;
		}
	}

	/* read AVI or PlayList */

	else if ( strcmp( cmd, ":r" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Insert file" ) );
		common->insertFile( );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, ":a" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Append file to scene" ) );
		int end = common->getPlayList() ->FindEndOfScene( common->g_currentFrame );
		common->moveToFrame( end );
		common->appendFile( );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, ":A" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Append file to movie" ) );
		int end = common->getPlayList() ->GetNumFrames();
		common->moveToFrame( end );
		common->appendFile( );
		cmd[ 0 ] = 0;
	}

	/* switch to export mode */

	else if ( strcmp( cmd, ":W" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Export" ) );
		common->changePageRequest( PAGE_EXPORT );
		cmd[ 0 ] = 0;
	}

	/* write PlayList */

	else if ( strcmp( cmd, ":w" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Write playlist" ) );
		common->savePlayList( );
		cmd[ 0 ] = 0;
	}

	/* quit */

	else if ( strcmp( cmd, ":q" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "quit" ) );
		kinoDeactivate();
		cmd[ 0 ] = 0;
	}

	/* split scene */

	else if ( strcmp( cmd, "Ctrl+J" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Split scene before frame" ) );
		if ( common->getPlayList() ->SplitSceneBefore( common->g_currentFrame ) )
		{
			common->hasListChanged = TRUE;
			common->moveToFrame();
			DrawBar( common->g_currentFrame );
			GetEditorBackup() ->Store( common->getPlayList() );
			cmd[ 0 ] = 0;
		}
	}

	/* goto a frame */
	else if ( strncmp( cmd, ":", 1 ) == 0 )
	{
		int val = 0;
		char t[ 132 ] = "";
		if ( sscanf( cmd + 1, "%d", &val ) == 1 )
		{
			common->moveToFrame( val );
			sprintf( t, _( "Move to frame %d" ), val );
			common->keyboardFeedback( cmd, t );
		}
		else
			common->setStatusBar( cmd );
	}

	else
	{
		// Check for invalid commands
		size_t real_len = strlen(real);
		if ( real_len > 5 )
			cmd[ 0 ] = 0;
		else if ( real_len >= 1 && strchr( "dgy ", real[ strlen( real ) - 1 ] ) == NULL )
			cmd[ 0 ] = 0;

		common->keyboardFeedback( cmd, "" );

	}

	common->setWindowTitle( );

	return FALSE;
}

/** Copy the requested frames to the playlist buffer.

  	\param first	first frame
	\param last		last frame
*/

void PageEditor::CopyFrames( int first, int last )
{
	PlayList * playList = new PlayList;

	// Delete the old list
	delete g_copiedPlayList;
	// Create the one requested
	common->getPlayList() ->GetPlayList( first, last, *playList );
	g_copiedPlayList = playList;
	playList->SetDocName( "" );
	playList->SetDirty( true );
}


/** Paste the playlist buffer before the specified frame.

	\param before		frame
*/

void PageEditor::PasteFrames( int before )
{
	PlayList temp( *g_copiedPlayList );

	if ( common->getPlayList() ->InsertPlayList( temp, before ) )
	{
		common->hasListChanged = TRUE;
		GetEditorBackup() ->Store( common->getPlayList() );
	}
	common->setWindowTitle( );
}

/** Delete and copy the requested frames to the playlist buffer.

  	\param first	first frame
	\param last		last frame
*/

void PageEditor::DeleteFrames( int first, int last )
{
	int before, after;

	before = common->getPlayList() ->GetNumFrames();
	if ( common->getPlayList() ->Delete( first, last ) )
	{
		after = common->getPlayList() ->GetNumFrames();
		common->hasListChanged = TRUE;
		GetEditorBackup() ->Store( common->getPlayList() );
	}
	common->setWindowTitle( );
}

void PageEditor::showFrame( int position, Frame& frame )
{
	gdk_threads_enter();
	if ( common->getPlayList() ->GetNumFrames() == 0 )
	{
		common->loadSplash( frameArea );
	}
	else
	{
		DrawBar( position );
		getFrameDisplayer() ->Put( frame, GTK_WIDGET( frameArea ), TRUE );
		showFrameInfo( position, frame );
		common->g_currentFrame = position;
	}
	gdk_flush();
	if ( position >= ( common->getPlayList()->GetNumFrames() - 1 ) ||
		( position <= 0 && ( g_nav_ctl.step < 0 || g_nav_ctl.rate < 0 ) ) )
	{
		common->videoStop();
	}
	gdk_threads_leave();
}


void PageEditor::windowMoved()
{
	if ( g_nav_ctl.active == FALSE )
	{
		if ( common->getPlayList() ->GetNumFrames() )
		{
			showFrame( common->g_currentFrame, TRUE );
		}
		else
		{
			common->loadSplash( frameArea );
		}
	}
}


void PageEditor::showFrameInfo( int i )
{
	if ( common->g_currentFrame == -1 )
	{
		gtk_label_set_text( positionLabelCurrent, "" );
		gtk_label_set_text( positionLabelTotal, "" );
		return ;
	}

	Frame *frame = GetFramePool() ->GetFrame();
	common->getPlayList() ->GetFrame( i, *frame );
	showFrameInfo( i, *frame );
	GetFramePool() ->DoneWithFrame( frame );
}

void PageEditor::showFrameInfo( int i, Frame &frame )
{
	FileHandler * media;
	int total = common->getPlayList() ->GetNumFrames();

	common->getPlayList() ->GetMediaObject( i, &media );
	common->showFrameMoreInfo( frame, media );

	common->getTime().setFramerate( frame.GetFrameRate() );
	string tc = "<span size=\"x-large\">" + common->getTime().parseFramesToString( i, common->getTimeFormat() ) + "</span>";
	gtk_label_set_markup( positionLabelCurrent, tc.c_str() );
	gtk_widget_set_redraw_on_allocate( GTK_WIDGET( positionLabelCurrent ), FALSE );
	tc = _("Duration: ") + common->getTime().parseFramesToString( total, common->getTimeFormat() );
	gtk_label_set_markup( positionLabelTotal, tc.c_str() );
	common->setCurrentScene( i );
}

void PageEditor::DrawBar( int currentFrame )
{
	if ( common->hasListChanged == TRUE )
	{
		ResetBar();
		common->hasListChanged = FALSE;
	}
	if ( currentFrame > -1 )
		gtk_adjustment_set_value( scrubAdjustment, ( gfloat ) currentFrame );
}

void PageEditor::ResetBar()
{
	int frameNum = 0, lastFrame = 0;

	sceneStartList.erase( sceneStartList.begin(), sceneStartList.end() );

	if ( sceneIndex != NULL )
		g_array_free( sceneIndex, TRUE );
	sceneIndex = g_array_new( FALSE, FALSE, sizeof( int ) );
	g_array_set_size( sceneIndex, 0 );

	while ( frameNum < common->getPlayList() ->GetNumFrames() )
	{
		lastFrame = frameNum = common->getPlayList() ->FindEndOfScene( frameNum );
		frameNum++;
		g_array_append_val( sceneIndex, frameNum );
		sceneStartList.insert( sceneStartList.end(), frameNum );
	}
	GetStoryboard() ->reset();
	GetStoryboard() ->redraw();
	frameNum = common->getPlayList() ->GetNumFrames() - 1;
	if ( frameNum < 0 )
		frameNum = 0;
	scrubAdjustment->upper = frameNum;
	g_signal_emit_by_name( scrubAdjustment, "changed" );
	gtk_enhanced_scale_set_breaks( scrubBar, sceneIndex );
}

vector <int> PageEditor::GetScene()
{
	return sceneStartList;
}

void PageEditor::snapshot()
{
	GetEditorBackup() ->Store( common->getPlayList() );
	common->setWindowTitle( );
}

extern "C"
{

	// Share the frames extracted from the audio and video thread - this
	// provides a smoother playback and is less intensive on the CPU. The
	// code assumes the video will never fall more than PLAYBACK_FRAMES/2
	// behind the audio.
	//
	// The read thread is responsible for write access to the queues of
	// frameNumber and frameContent.

	static int frameNumber[ PLAYBACK_FRAMES ];
	static int pending = 0;
	static int head = -1;
	static int tail = -1;
	static bool playing = false;
	static bool showing = false;

	static void resetThreads( )
	{
		playing = false;
		showing = false;
		pending = 0;
		head = 0;
		tail = 0;
	}

	/** This function carries out the read ahead and is responsible for obtaining
		the each frame. audioThread monitors the queue (by checking mainly on pending)
		and plays the audio of each frame. videoThread picks up the last tail and
		displays it.

		Note that only half of the queue is filled here - this provides the video thread
		some time to safely use a frame without worrying about it being overwritten.
	*/

	static void *readThread( void * info )
	{
		struct navigate_control * ctl = ( struct navigate_control * ) info;
		newFrame = 0;
		lastFrame = common->g_currentFrame - 1;
		gint totalFrames = common->getPlayList() ->GetNumFrames();
		gint countFrames = 0;
		static Preferences &prefs = Preferences::getInstance();
		int time_per_frame = 1000000 / _getOneSecond( );

		// cerr << ">>> Starting read thread " << endl;

		// Get start of time
		struct timeval start;
		struct timeval end;
		gettimeofday( &start, NULL );

		// Loop while active
		while ( ctl->active )
		{
			// Calculate time for next frame
			start.tv_usec += time_per_frame;
			if ( start.tv_usec >= 1000000 )
			{
				start.tv_usec -= 1000000;
				start.tv_sec ++;
			}

			pthread_mutex_lock( &readlock );
			// Determine the frame to render
			newFrame = lastFrame + ctl->step;

			// determine new frame based upon jogshuttle rate
			if ( ctl->step == 0 )
			{
				ctl->subframe++;
				if ( ctl->rate < 0 )
				{
					if ( ctl->subframe >= -ctl->rate )
					{
						newFrame --;
						ctl->subframe = 0;
					}
				}
				else
				{
					if ( ctl->subframe >= ctl->rate )
					{
						newFrame ++;
						ctl->subframe = 0;
					}
				}
			}
			pthread_mutex_unlock( &readlock );

			// Check the bounds and adjust as necessary
			if ( newFrame < 0 )
				newFrame = 0;
			else if ( newFrame >= totalFrames )
				newFrame = totalFrames - 1;

			// Determine which locations in frameNumber and frameContent we need to use
			if ( pending >= PLAYBACK_FRAMES - 10 )
			{
				// Queue is full.. do nothing
				playing = true;
				struct timespec t = { 0, 1000000 }; // 1ms
				nanosleep( &t, NULL );
			}
			else
			{
				frameNumber[ head ] = newFrame;
				common->getPlayList() ->GetFrame( frameNumber[ head ], *frameContent[ head ] );
				if ( writer1394 != NULL )
					writer1394->SendFrame( *frameContent[ head ], false );
				
				pthread_mutex_lock( &threadlock );
				++pending;
				head = ( head + 1 ) % PLAYBACK_FRAMES;
				pthread_mutex_unlock( &threadlock );

				// We have a frame, so start playing
				playing = true;

				// Render all frames if requested
				if ( ( ! prefs.dropFrame || ! prefs.enableAudio ) && ctl->active )
				{
					if ( prefs.enableAudio && ( frameNumber[ tail ] != lastFrame || ctl->rate > 1 || ctl->rate < -1 ) )
						common->getPageEditor() ->getFrameDisplayer() ->PutSound( *frameContent[ tail ] );
					common->getPageEditor() ->showFrame( frameNumber[ tail ], *frameContent[ tail ] );
					tail = ( tail + 1 ) % PLAYBACK_FRAMES;
					--pending;

					// Determine how far from the next frame we really are
					gettimeofday( &end, NULL );
					int difference = ( ( start.tv_sec * 1000000 + start.tv_usec ) - ( end.tv_sec * 1000000 + end.tv_usec ) );

					if ( difference > 2000 && difference < time_per_frame )
					{
						// Sleep for half the remaining time when audio is disabled
						if ( ! prefs.enableAudio )
						{
							struct timespec t =
							    {
								    0, difference * 500
							    };
							nanosleep( &t, NULL );
						}
					}
					else if ( difference < 0 || difference >= time_per_frame )
					{
						gettimeofday( &start, NULL );
					}
				}

				// update the lastFrame and lastPos variables
				lastFrame = newFrame;

				// Incrment the frame count
				countFrames ++;
			}
		}
		// cerr << ">>> Ending read thread " << countFrames << endl;

		return NULL;
	}

	static void *audioThread( void * info )
	{

		static Preferences & prefs = Preferences::getInstance();
		struct navigate_control *ctl = ( struct navigate_control * ) info;
		int lastFrame = common->g_currentFrame - 1;
		int time_per_frame = 1000000 / _getOneSecond( );

		// cerr << ">>> Starting audio thread" << endl;

		while ( ctl->active )
		{
			// Only do this when drop frames is active (otherwise the audio thread takes care of it)
			if ( prefs.dropFrame && prefs.enableAudio )
			{
				// As long as its different to the previous one, then display it
				if ( playing && pending > 0 )
				{
					// Play the audio
					if ( prefs.enableAudio && ( frameNumber[ tail ] != lastFrame || ctl->rate > 1 || ctl->rate < -1 ) )
					{
						common->getPageEditor() ->getFrameDisplayer() ->PutSound( *frameContent[ tail ] );
						if ( pending < 10 )
						{
							struct timespec t = { 0, 1000000 }; // 1ms
							nanosleep( &t, NULL );
						}
					}
					else if ( ctl->step == 0 && !prefs.audioScrub )
					{
						struct timespec t =
						    {
							    0, time_per_frame * 1000
						    };
						nanosleep( &t, NULL );
					}

					// Last encountered audio frame
					lastFrame = frameNumber[ tail ];

					// Move the tail
					pthread_mutex_lock( &threadlock );
					--pending;
					tail = ( tail + 1 ) % PLAYBACK_FRAMES;;
					pthread_mutex_unlock( &threadlock );

					// Start showing frames from this point onwards
					showing = true;
				}
				else
				{
					playing = false;
					struct timespec t = { 0, 1000000 }; // 1ms
					nanosleep( &t, NULL );
				}
			}
			else
			{
				// It is safe to cancel the audio thread here since a change to prefs necessitates a restart.
				// cerr << ">>> Audio Thread not needed" << endl;
				return NULL;
			}
		}
		// cerr << ">>> Ending audio thread" << endl;

		return NULL;
	}

	static void *videoThread( void * info )
	{

		static Preferences & prefs = Preferences::getInstance();
		struct navigate_control *ctl = ( struct navigate_control * ) info;
		int lastFrame = common->g_currentFrame - 1;

#ifdef PLAY_WITH_STATS
		// Statistical analysis variables
		int dropped = 0;
		int count = 0;
#endif

		// cerr << ">>> Starting video thread" << endl;
		while ( ctl->active )
		{
			// Only do this when drop frames is active (otherwise the audio thread takes care of it)
			if ( prefs.dropFrame && prefs.enableAudio )
			{
				if ( showing && pending > 0 && ctl->active )
				{
					// Store the current tail
					int my_tail = tail;

					// As long as its different to the previous one, then display it
					if ( frameNumber[ my_tail ] != lastFrame || ctl->rate > 1 )
					{
#ifdef PLAY_WITH_STATS
						// Statistical analysis of frame playback (useful for limited kinds of tests)
						if ( ctl->step == 1 )
						{
							int skipped = frameNumber[ my_tail ] - lastFrame - 1;
							count += skipped + 1;
							if ( skipped != 0 && lastFrame != 0 && count != 0 )
							{
								dropped += skipped;
								cerr << ">>>> Dropped " << dropped << " in " << count << " frames - " << ( double ) ( ( ( double ) dropped / ( double ) count ) * 100.0 ) << "%" << endl;
							}
							lastFrame = frameNumber[ my_tail ];
						}
#endif
						// Show this frame
						common->getPageEditor() ->showFrame( frameNumber[ my_tail ], *frameContent[ my_tail ] );
					}
				}
				struct timespec t = { 0, 1000000 }; // 1ms
				nanosleep( &t, NULL );
			}
			else
			{
				// It is safe to cancel the video thread here since a change to prefs necessitates a restart.
				// cerr << ">>> Video Thread not needed" << endl;
				return NULL;
			}
		}

#ifdef PLAY_WITH_STATS
		cerr << ">>>> Video stopped: Dropped " << dropped << " in " << count << " frames - " << ( double ) ( ( ( double ) dropped / ( double ) count ) * 100.0 ) << "%" << endl;
#endif
		// cerr << ">>> Ending video thread" << endl;

		return NULL;
	}

	static int _getOneSecond( void )
	{
		Frame & frame = *( GetFramePool() ->GetFrame( ) );
		common->getPlayList() ->GetFrame( common->g_currentFrame, frame );
		int value = ( frame.IsPAL() ? 25 : 30 );
		GetFramePool( ) ->DoneWithFrame( &frame );
		return value;
	}

}

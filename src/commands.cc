/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

// GUI Page Handling Code
#include "kino_common.h"
#include "page.h"
#include "page_editor.h"
#include "page_capture.h"
#include "page_timeline.h"
#include "page_export.h"
#include "preferences.h"
#include "avi.h"
#include "filehandler.h"
#include "ieee1394io.h"
#include "message.h"
#include "jogshuttle.h"

extern "C"
{

#include <sys/stat.h>
#include <sys/time.h>
#include <gtk/gtk.h>
#include <math.h>
#include <ctype.h>

#include "callbacks.h"
#include "support.h"
#include "commands.h"
#include "jogshuttle.h"
#include "gtkenhancedscale.h"

extern struct navigate_control g_nav_ctl;

KinoCommon *common = NULL;

static void getDroppedFiles( GtkWidget * w, GdkDragContext * context,
								gint x, gint y, GtkSelectionData * data, guint info,
								guint time, gpointer * extra )
{

	if ( data->data )
	{
		gchar** urls = g_strsplit( ( gchar* ) data->data, "\n", 0 );
		gchar** tmp = urls;
		char *argv[ 2 ];

		/* For each URL we are passed */
		while ( tmp )
		{
			if ( *tmp == 0 || strcmp( *tmp, "" ) == 0 )
				break;
			if ( !strncmp( *tmp, "file:", 5 ) )
			{
				argv[ 1 ] = *tmp + 5;
				if ( isspace( argv[ 1 ][ strlen( argv[ 1 ] ) - 1 ] ) )
					argv[ 1 ][ strlen( argv[ 1 ] ) - 1 ] = '\0';
				common->bulkLoad( 2, argv );
			}
			tmp++;
		};
		g_strfreev( urls );
		windowMoved();
		gtk_drag_finish( context, TRUE, FALSE, time );
	}
	gtk_drag_finish( context, FALSE, FALSE, time );
}

void kinoInitialise( GtkWidget * widget )
{
	common = new KinoCommon( widget );
	common->setWindowTitle( );
	JogShuttle::getInstance();
	if ( ! Preferences::getInstance().enableV4L )
	{
		GtkWidget *widget2 = lookup_widget( widget, "menuitem_v4l" );
		gtk_container_remove( GTK_CONTAINER( widget2->parent ), widget2 );
		gtk_notebook_remove_page( GTK_NOTEBOOK( lookup_widget( widget, "main_notebook" ) ), PAGE_BTTV );
	}

	/* Allow drag from file manager */
	static GtkTargetEntry file_targets[] = {
												{(gchar*) "text/uri-list", 0, 0}
											};

	g_signal_connect( G_OBJECT( widget ), "drag_data_received",
						G_CALLBACK( getDroppedFiles ), NULL );
	gtk_drag_dest_set( GTK_WIDGET( widget ),
						( GtkDestDefaults ) ( GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_HIGHLIGHT |
												GTK_DEST_DEFAULT_DROP ), file_targets, 1,
						( GdkDragAction ) ( GDK_ACTION_COPY | GDK_ACTION_MOVE ) );
	g_signal_connect( G_OBJECT( lookup_widget( widget, "main_notebook" ) ), "drag_data_received",
						G_CALLBACK( getDroppedFiles ), NULL );
	gtk_drag_dest_set( GTK_WIDGET( lookup_widget( widget, "main_notebook" ) ),
						( GtkDestDefaults ) ( GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_HIGHLIGHT |
												GTK_DEST_DEFAULT_DROP ), file_targets, 1,
						( GdkDragAction ) ( GDK_ACTION_COPY | GDK_ACTION_MOVE ) );
	gtk_window_set_default_size( GTK_WINDOW( widget ),
		Preferences::getInstance().windowWidth, Preferences::getInstance().windowHeight );
	gtk_paned_set_position( GTK_PANED( lookup_widget( widget , "hpaned1" ) ),
		Preferences::getInstance().storyboardPosition );

	if ( ! Preferences::getInstance().enablePublish )
	{
		GtkWidget *widget2 = lookup_widget( widget, "button_publish_project" );
		gtk_container_remove( GTK_CONTAINER( widget2->parent ), widget2 );
		widget2 = lookup_widget( widget, "button_publish_still" );
		gtk_container_remove( GTK_CONTAINER( widget2->parent ), widget2 );
		widget2 = lookup_widget( widget, "publish_project" );
		gtk_container_remove( GTK_CONTAINER( widget2->parent ), widget2 );
		widget2 = lookup_widget( widget, "publish_still" );
		gtk_container_remove( GTK_CONTAINER( widget2->parent ), widget2 );
	}
	common->updateRecentFiles();
}

void kinoPostInit()
{
	common->setPreviewSize( float( Preferences::getInstance().previewSize ) / 10.0, true );
	notebookChangePage( PAGE_EDITOR );
	common->setCurrentPage( PAGE_EDITOR );
}

gboolean kinoDeactivate()
{
	if ( common->exitKino( ) )
	{
		cerr << "Exiting Kino\n";
		gtk_window_get_size( GTK_WINDOW( common->getWidget() ), &Preferences::getInstance().windowWidth,
			&Preferences::getInstance().windowHeight );
		Preferences::getInstance().storyboardPosition = gtk_paned_get_position( GTK_PANED(
			lookup_widget( common->getWidget() , "hpaned1" ) ) );
		Preferences::getInstance().Save();
		gtk_main_quit();
	}
	return TRUE;
}

void bulkLoad( int argc, char * argv[] )
{
	if ( GetEditorBackup()->Restore( common->getPlayList() ) )
	{
		common->g_currentFrame = 0;
		common->hasListChanged = TRUE;
		common->setWindowTitle();
		windowMoved();
	}
	else
	{
		common->newFile();
		common->bulkLoad( argc, argv );
	}
}

void newFile( )
{
	common->newFile();
}

void openFile( )
{
	common->loadFile();
}

void savePlayListAs( )
{
	common->savePlayListAs();
}

void savePlayList( )
{
	common->savePlayList();
}

void saveFrame( )
{
	common->saveFrame();
}

void insertFile( )
{
	common->moveToFrame( common->getPlayList() ->FindStartOfScene( common->g_currentFrame ) );
	common->insertFile();
}

void appendFile( )
{
	common->moveToFrame( common->getPlayList() ->FindEndOfScene( common->g_currentFrame ) );
	common->appendFile();
}

void pageStart( int page )
{
	common->setCurrentPage( page );
}

int moveToFrame( int frame )
{
	return common->moveToFrame( frame );
}

int moveByFrames( int offs )
{
	return common->moveByFrames( offs );
}

void videoStart( )
{
	common->videoStartOfMovie();
}

void videoPreviousScene( )
{
	common->videoPreviousScene();
}

void videoStartOfScene( )
{
	common->videoStartOfScene();
}

void videoRewind( )
{
	common->videoRewind();
}

void videoBack()
{
	common->videoBack(-1);
}
void videoBackBy(int step)
{
	common->videoBack(step);
}

void videoPlay( )
{
	common->videoPlay();
}

void videoForward()
{
	common->videoForward(1);
}
void videoForwardBy(int step)
{
	common->videoForward(step);
}

void videoFastForward( )
{
	common->videoFastForward();
}

void videoNextScene( )
{
	common->videoNextScene();
}

void videoEndOfScene( )
{
	common->videoEndOfScene();
}

void videoEndOfMovie( )
{
	common->videoEndOfMovie();
}

void videoStop( )
{
	common->videoStop();
}

void videoPause( )
{
	common->videoPause();
}

void videoShuttle( int angle )
{
	common->videoShuttle( angle );
}

void windowMoved()
{
	if ( common )
		common->windowMoved();
}

void visibilityChanged( gboolean visible )
{
	common->visibilityChanged( visible );
}

void notebookChangePage( int page )
{
	common->changePageRequest( page );
}

gboolean processKeyboard( GdkEventKey * event )
{
	// To avoid problems with repeat keys only process
	// top of the event queue key presses - issue is that
	// we don't know if any pending event is actually a
	// keypress... think all key press should move over to
	// a snoop, but even then.. dunno if that'll help
	if ( Preferences::getInstance().disableKeyRepeat && gdk_events_pending() )
		return TRUE;

	return common->processKeyboard( event );
}

void processCommand( const char * command )
{
	if ( strcmp( command, "Ctrl+W" ) == 0 )
		publishPlayList();
	else
		common->processCommand( command );
}

void selectScene( int i )
{
	common->selectScene( i );
}

void startCapture( void )
{
	common->getPageCapture() ->startCapture();
}

void stopCapture( void )
{
	common->getPageCapture() ->stopCapture();
}

void setPreviewSize( float factor )
{
	common->setPreviewSize( factor );
}

void previewExport( void )
{
	common->getPageExport() ->previewExport();
}

void startExport( void )
{
	common->getPageExport() ->startExport();
}
void stopExport( void )
{
	common->getPageExport() ->stopExport();
}

void pauseExport( void )
{
	common->getPageExport() ->pauseExport();
}

void setExportMode( int mode )
{
	common->getPageExport() ->setCurrentMode( mode );
}

void RefreshBar( GtkWidget * drawingarea )
{
	common->getPageEditor() ->DrawBar( common->g_currentFrame );
}

void setMoreInfo( int state )
{
	common->setMoreInfo( state == 1 );
	Frame &frame = *( GetFramePool( ) ->GetFrame( ) );
	common->showFrameMoreInfo( frame, NULL );
	GetFramePool( ) ->DoneWithFrame( &frame );
	common->moveToFrame();
}

void setTimeFormat( int format )
{
	common->setTimeFormat( static_cast< SMIL::Time::TimeFormat >( format ) );
}

void publishPlayList( void )
{
	common->publishPlayList( );
}

void publishFrame( void )
{
	common->publishFrame( );
}

void startJogShuttle( void )
{
	JogShuttle::getInstance().start();
}

void showHelp( const char *page )
{
	extern char* g_help_language;
	std::string cmd = "\"" DATADIR "/kino/scripts/help.sh\" \"file://" DATADIR "/kino/help/";
	if ( page == NULL || strcmp( page, "" ) == 0 )
		page = common->getCurrentPage()->getHelpPage().c_str();
	cmd += std::string( g_help_language ) + std::string( "\" \"" ) + std::string( page ) + "\" &";
	system( cmd.c_str() );
}

void handleMouseScroll( GdkEvent *event )
{
	if ( Preferences::getInstance().enableJogShuttle )
		return;
	
	if ( event->scroll.state & GDK_SHIFT_MASK )
	{
		if ( event->scroll.direction == GDK_SCROLL_UP )
			videoBack( );
		else if ( event->scroll.direction == GDK_SCROLL_DOWN )
			videoForward( );
	}
	else if ( event->scroll.state & GDK_CONTROL_MASK )
	{
		int speedTable[] = {
			0,
			8,  10, 16, 20, 33, 50, 75,
			100,
			200, 300, 400, 500, 800, 1200,
			3001 };
		int i = 0;
		int speed;
		int direction;

		if ( g_nav_ctl.active == FALSE )
		{
			speed = 8;
			direction = ( event->scroll.direction == GDK_SCROLL_UP ? -1 : 1 );
		}
		else if ( g_nav_ctl.step == 0 )
		{
			speed = 100 / ( g_nav_ctl.rate < 0 ? -g_nav_ctl.rate : g_nav_ctl.rate );
			speed = speed == 100 ? 75 : speed;
			direction = g_nav_ctl.rate < 0 ? -1 : 1;
		}
		else
		{
			speed = ( g_nav_ctl.step < 0 ? -g_nav_ctl.step : g_nav_ctl.step ) * 100;
			direction = g_nav_ctl.step < 0 ? -1 : 1;
		}
		
		while ( i < 16 && speedTable[ i ] < speed )
			++i;
		
		i *= direction;
		//fprintf( stderr, "speed %d direction %d i %d\n", speed, direction, i );
		
		if ( event->scroll.direction == GDK_SCROLL_UP )
			videoShuttle( -- i );
		else if ( event->scroll.direction == GDK_SCROLL_DOWN )
			videoShuttle( ++ i );
	}
	else
	{
		if ( event->scroll.direction == GDK_SCROLL_UP )
			videoBackBy( -10 );
		else if ( event->scroll.direction == GDK_SCROLL_DOWN )
			videoForwardBy( 10 );
	}
}

int kino2raw( char* filename, char* option )
{
	try
	{
		PlayList playlist;
		Frame frame;
		AudioInfo info;

		if ( playlist.LoadPlayList( filename ) )
		{
			playlist.GetFrame( 0, frame );

			if ( option )
			{
				if ( strcmp( option, "aspect" ) == 0 )
				{
					std::cout << ( frame.IsWide() ? "16:9" : "4:3" ) << std::endl;
				}
				else if ( strcmp( option, "normalisation" ) == 0 )
				{
					std::cout << ( frame.IsPAL() ? "pal" : "ntsc" ) << std::endl;
				}
				return 0;
			}

			frame.GetAudioInfo( info );
			const double outputRate = info.frequency;

			// Determine correct amount of audio for duration
			double adjustedRate = 0.0;
			{
				double seconds = double( playlist.GetNumFrames() ) / frame.GetFrameRate();
				int64_t idealSamples = int64_t( seconds * outputRate + 0.5 );
				int64_t actualSamples = 0;
				int n = -1, frameNum = 0, previousProgress = -1;
				AsyncAudioResample<int16_ne_t,int16_le_t> resampler(
					AUDIO_RESAMPLE_SRC_SINC_FASTEST, &playlist, outputRate, 0, playlist.GetNumFrames() - 1, 1 );
					char buf[ 512 ];

				// Determine the actual number of samples
				while ( n != 0 )
				{
					n = resampler.Process( outputRate, frame.CalculateNumberSamples( int( outputRate ), frameNum++ ) );
					actualSamples += n;
					int progress = int( double( frameNum ) / playlist.GetNumFrames() * 100 );
					if ( progress != previousProgress )
					{
						previousProgress = progress;
						snprintf( buf, 512, "\r%s (%02d%%)", _("Locking audio"), progress );
						std::cerr << buf;
					}
				}
				adjustedRate = outputRate + ( idealSamples - actualSamples ) / seconds;
				std::cerr << std::endl;
			}

			// Set up resampling
			int16_le_t *audio_buffers[ 4 ];
			for ( int c = 0; c < 4; c++ )
				audio_buffers[ c ] = new int16_le_t[ 2 * DV_AUDIO_MAX_SAMPLES ];
	
			// Create the resampler
			AsyncAudioResample<int16_ne_t,int16_le_t> resampler(
				AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, &playlist, adjustedRate, 0, playlist.GetNumFrames() - 1, 1 );
			if ( resampler.IsError() )
			{
				std::cerr << "Resampler error: " << resampler.GetError() << std::endl;
				for ( int c = 0; c < 4; c++ )
					delete[] audio_buffers[ c ];
				return 4;
			}
	
			for ( int i = 0; i < playlist.GetNumFrames(); ++i )
			{
				// Resample
				int requestedSamples = frame.CalculateNumberSamples( int( outputRate ), i );

				playlist.GetFrame( i, frame );
				info.samples = resampler.Process( adjustedRate, requestedSamples );
				if ( info.samples )
				{
					// Deinterleave samples
					int16_le_t *p = resampler.GetOutput();
					for ( int s = 0; s < info.samples; s++ )
						for ( int c = 0; c < info.channels; c++ )
							audio_buffers[ c ][ s ] = *p++;

					frame.EncodeAudio( info, audio_buffers );
				}
				if ( !fwrite( frame.data, frame.GetFrameSize(), 1, stdout ) )
				{
					perror( "Failed to write raw DV" );
					for ( int c = 0; c < 4; c++ )
						delete[] audio_buffers[ c ];
					return 2;
				}
			}

			// Cleanup
			for ( int c = 0; c < 4; c++ )
				delete[] audio_buffers[ c ];
		}
	}
	catch (...)
	{
		std::cerr << "General Error" << std::endl;
		return 3;
	}
	return 0;
}

} // extern "C"

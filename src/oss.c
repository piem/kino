/*
*  oss.c
*
*  OSS code Copyright (C) Charles 'Buck' Krasic - January 2001 (from libdv)
*  OSS fragment handling addition by Tomoaki Hayasaka <hayasakas@postman.riken.go.jp>
*  ALSA code Copyright 2005 Troy Rollo
*  Integration code by Dan Dennedy
*
*  This program is free software; you can redistribute it and/or modify it
*  under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2, or (at your
*  option) any later version.
*
*  This program is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with GNU Make; see the file COPYING.  If not, write to
*  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA. 
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libdv/dv_types.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/soundcard.h>


#include "oss.h"

kino_sound_t *
kino_sound_new( void )
{
	kino_sound_t * result;

	result = ( kino_sound_t * ) calloc( 1, sizeof( kino_sound_t ) );
	if ( result != NULL )
	{
		result->fd = -1;
	}

	return result;
}

/* Very simplistic for sound output using the OSS API - returns 0 if unable to open or the
 * sampling rate used if successful. It is assumed that all subsequent play requests will
 * resample top this rate.
 */
int
kino_sound_init( dv_audio_t *audio, kino_sound_t *sound, char const *device )
{
	int rate_request, channels_request, fragment_request;
	int used = audio->frequency;

	channels_request = audio->num_channels;
	rate_request = audio->frequency;
	fragment_request = 0x0004000B;
	sound->buffer = NULL;

	if ( channels_request == 0 || rate_request == 0 )
		return FALSE;

	if ( !device || !*device )
		device = "plughw:0,0";

	/* open audio device
	   Open nonblocking first, to test if available. */
	sound->fd = open( device, O_WRONLY | O_NONBLOCK | O_NDELAY );
	if ( sound->fd != -1 )
	{
		close( sound->fd );
		sound->fd = open( device, O_WRONLY );
	}
	if ( sound->fd != -1 )
	{
		/* OSS */
		int format = AFMT_S16_NE;
		int error = 0;
		if ( ioctl( sound->fd, SNDCTL_DSP_SETFRAGMENT, &fragment_request ) == -1 )
		{
			perror( "SNDCTL_DSP_SETFRAGMENT" );
			error = 1;
		}
		if ( error == 0 && ioctl( sound->fd, SNDCTL_DSP_SETFMT, &format ) == -1 )
		{
			perror( "SNDCTL_DSP_SETFMT" );
			error = 2;
		}
		if ( error == 0 && format != AFMT_S16_NE )
		{
			fprintf( stderr, "soundcard doesn't support format\n" );
			error = 3;
		}
		if ( error == 0 && ioctl( sound->fd, SNDCTL_DSP_CHANNELS, &channels_request ) )
		{
			fprintf( stderr, "soundcard doesn't support %d channels\n", audio->num_channels );
			error = 4;
		}
		if ( error == 0 && channels_request != audio->num_channels )
		{
			fprintf( stderr, "soundcard doesn't support %d channels\n", audio->num_channels );
			error = 5;
		}
		if ( error == 0 && ioctl( sound->fd, SNDCTL_DSP_SPEED, &rate_request ) == -1 )
		{
			perror( "SNDCTL_DSP_SPEED" );
			error = 6;
		}
		if ( error == 0 && (rate_request < 0.9*audio->frequency ||
		                    rate_request > 1.1*audio->frequency))
		{
			perror( "SNDCTL_DSP_SPEED" );
			error = 7;
		}

		if ( error == 6 || error == 7 )
		{
			rate_request = 44100;
			used = rate_request;
			fprintf( stderr, ">>> audio at %d failed - forcing resample at %dhz\n", audio->frequency, rate_request );
			if ( ioctl( sound->fd, SNDCTL_DSP_SPEED, &rate_request ) == -1 )
			{
				perror( "SNDCTL_DSP_SPEED 44.1" );
				used = 0;
				error = 6;
			}
			else
			{
				error = 0;
			}
			if ( error == 0 && rate_request != 44100 )
			{
				perror( "SNDCTL_DSP_SPEED" );
				error = 7;
				used = 0;
			}
		}

		if ( error == 0 )
		{
			sound->buffer = malloc( DV_AUDIO_MAX_SAMPLES * audio->num_channels * sizeof( int16_t ) );
			if ( sound->buffer == NULL )
			{
				fprintf( stderr, "Unable to allocate memory for the buffer\n" );
				error = 8;
			}
		}

		if ( error != 0 )
		{
			close( sound->fd );
			sound->fd = -1;
			used = 0;
		}
	}
#ifdef HAVE_ALSA
	else
	{
		/* ALSA */
		int serr;
		int error;
	
		sound->hwparams = NULL;
		sound->writefail_noted = 0;
	
		if ( channels_request == 0 || rate_request == 0 )
			return FALSE;
	
		sound->channels = audio->num_channels;
	
		if ((serr = snd_pcm_open(&sound->pcmh, device, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK)) < 0)
		{
			fprintf(stderr, "Could not open ALSA device \"%s\": %s\n", device ? device : "(default)", snd_strerror(serr));
			return FALSE;
		}
	
		snd_pcm_nonblock(sound->pcmh, 0);
	
		error = 0;
	
		if ((serr = snd_pcm_hw_params_malloc(&sound->hwparams)) < 0)
		{
			fprintf(stderr, "Could not allocate ALSA hardware parameters: %s\n", snd_strerror(serr));
			error = 8;
		}
	
		if (!error &&
			(serr = snd_pcm_hw_params_any(sound->pcmh, sound->hwparams)) < 0)
		{
			fprintf(stderr, "Could not get ALSA hardware parameters: %s\n", snd_strerror(serr));
			error = 1;
		}
		if (!error &&
			(serr = snd_pcm_hw_params_set_access(sound->pcmh, sound->hwparams, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
		{
			fprintf(stderr, "Could not set ALSA interleaved access: %s\n", snd_strerror(serr));
			error = 2;
		}
		if (!error &&
			(serr = snd_pcm_hw_params_set_format(sound->pcmh, sound->hwparams, SND_PCM_FORMAT_S16)) < 0)
		{
			fprintf(stderr, "Could not set ALSA sample format: %s\n", snd_strerror(serr));
			error = 2;
		}
		if (!error &&
			(serr = snd_pcm_hw_params_set_channels(sound->pcmh, sound->hwparams, channels_request)) < 0)
		{
			fprintf(stderr, "Could not set ALSA channels to %d: %s\n", channels_request, snd_strerror(serr));
			error = 4;
		}
		if (!error &&
			(serr = snd_pcm_hw_params_set_rate_resample(sound->pcmh, sound->hwparams, 1)) < 0)
		{
			fprintf(stderr, "Could not set ALSA resampling: %s\n", snd_strerror(serr));
			error = 7;
		}
	
		if (!error &&
			(serr = snd_pcm_hw_params_set_rate_near(sound->pcmh, sound->hwparams, ( unsigned* )&rate_request, 0)) < 0)
		{
			fprintf(stderr, "Could not set ALSA rate to %d: %s\n", rate_request, snd_strerror(serr));
			error = 6;
		}
	
		if (!error &&
			(serr = snd_pcm_hw_params(sound->pcmh, sound->hwparams)) < 0)
		{
			fprintf(stderr, "Could not set ALSA Hardware parameters %s\n", snd_strerror(serr));
			error = 9;
		}
	
		if (!error)
		{
			sound->buffer = malloc( DV_AUDIO_MAX_SAMPLES * audio->num_channels * sizeof( int16_t ) );
			if ( sound->buffer == NULL )
			{
				fprintf( stderr, "Unable to allocate memory for the buffer\n" );
				error = 8;
			}
		}
	
		if (error)
		{
			fprintf( stderr, "Unable to open audio device %s\n", device );
			snd_pcm_close(sound->pcmh);
			if (sound->hwparams)
				free(sound->hwparams);
			sound->pcmh = 0;
			used = 0;
		}
	}
#endif
	return used;
}

int
kino_sound_player( kino_sound_t *sound, void *data, int size )
{
	if ( sound->fd > -1 )
	{
		/* OSS */
		return write( sound->fd, data, size ) == size;
	}
#ifdef HAVE_ALSA
	else
	{
		/* ALSA */
		int frames = size / sound->channels / sizeof(int16_t);
		int result;
		int16_t *framedata = data;
	
		while (frames > 0)
		{
			result = snd_pcm_writei( sound->pcmh, framedata, frames );
			if ( result == -EAGAIN )
			{
				snd_pcm_wait( sound->pcmh, 1000 );
				result = snd_pcm_writei( sound->pcmh, framedata, frames );
			}
			else if ( result == -EPIPE )
			{
				snd_pcm_prepare( sound->pcmh );
				result = snd_pcm_writei( sound->pcmh, framedata, frames );
			}
			if (result < 0)
			{
				if (!sound->writefail_noted)
				{
					sound->writefail_noted = 1;
					fprintf( stderr, "Could not write to ALSA: %s\n", snd_strerror ( result ) );
				}
				return FALSE;
			}
			framedata += result * sound->channels;
			frames -= result;
		}
		if ( framedata != ( int16_t* )( (char *) data + size ) )
		{
			fprintf(stderr, "Misaligned after write to ALSA: should be at %p, but we are at %p\n",
					(char *) data + size,
					framedata);
		}
		return TRUE;
	}
#endif
	return FALSE;
}

int
kino_sound_play( dv_audio_t *audio, kino_sound_t *sound, int16_t **out )
{
	int ch, i, j = 0, total, written = 0, result;

	/* Interleave the audio into a single buffer */
	for ( i = 0; i < audio->samples_this_frame; i++ )
	{
		for ( ch = 0; ch < audio->num_channels; ch++ )
		{
			sound->buffer[ j++ ] = out[ ch ][ i ];
		}
	}

	if ( sound->fd > -1 )
	{
		/* OSS */
		total = audio->samples_this_frame * audio->num_channels * sizeof( int16_t );
		do
		{
			result = write( sound->fd, sound->buffer, total );
			if ( result != total )
				goto write_error;
			written += result;
		}
		while ( total > written );
		if ( !sound->arg_audio_file )
		{
			if ( ioctl( sound->fd, SNDCTL_DSP_POST, NULL ) == -1 )
				goto post_ioctl;
		} /* if */
	
		return ( TRUE );
	
write_error:
		perror( "write" );
		return ( FALSE );

post_ioctl:
		perror( "SNDCTL_DSP_POST" );
		return ( FALSE );
	}
#ifdef HAVE_ALSA
	else
	{
		/* ALSA */
		total = audio->samples_this_frame;
		do
		{
			result = snd_pcm_writei( sound->pcmh, sound->buffer + written * audio->num_channels, total - written );
			if ( result == -EAGAIN )
			{
				snd_pcm_wait( sound->pcmh, 1000 );
				result = snd_pcm_writei( sound->pcmh, sound->buffer + written * audio->num_channels, total - written );
			}
			else if ( result == -EPIPE )
			{
				snd_pcm_prepare( sound->pcmh );
				result = snd_pcm_writei( sound->pcmh, sound->buffer + written * audio->num_channels, total - written );
			}
			if (result < 0)
			{
				if (!sound->writefail_noted)
				{
					sound->writefail_noted = 1;
					fprintf( stderr, "Could not write to ALSA: %s\n", snd_strerror ( result ) );
				}
				return FALSE;
			}
			written += result;
		}
		while ( total > written );
	
		return ( TRUE );
	}
#endif
}

void
kino_sound_close( kino_sound_t *sound )
{
	if ( sound != NULL )
	{
		if ( sound->fd != -1 )
			close( sound->fd );
		if ( sound->buffer )
			free( sound->buffer );
#ifdef HAVE_ALSA
		if ( sound->pcmh )
			snd_pcm_close( sound->pcmh );
		if ( sound->hwparams )
			free( sound->hwparams );
#endif
		free( sound );
	}
}

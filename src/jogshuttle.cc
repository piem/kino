// $Id: jogshuttle.cc,v 1.22 2007/02/20 06:32:05 ddennedy Exp $
/*
 * Copyright (C) 2001 Tomoaki Hayasaka <hayasakas@postman.riken.go.jp>
 * Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/// Support for special USB Jog/Shuttle input controllers

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <linux/input.h>

#include "jogshuttle.h"
#include "preferences.h"
#include "commands.h"

#include <string>
using std::string;
#include <utility>
using std::pair;
using std::make_pair;

/* **********************************************************************
 
Overview of jogshuttle mapping code.
 
Note, this is mostly about the preferences, but I thought it should go in here
 
* Knowing all the actions
 
When starting, we fill the optionmenus with the choices. These are
also into an array. Array is keyed on _option_index (duplicated),
contains _short_desc, _desc, _action. When we fill the option menu,
the index is maintained/duplicated in the array.
 
* Getting the stored or default actions
 
Then the preference.cc file needs to read a lot of stuff from the
config file or a default mapping. These are ints that are the codes of
the buttons, and the strings that are the actual actions. 
 
These are read into the mappings of buttons, which is a map keyed on a
pair of codes. The codes are (for now) the actual codes as returned
from the events - a future level of abstraction may call for a mapping
from buttons to codes.
 
* Changing the mappings - the user changes the optionmenu selections.
 
- When the user changes one of the two "buttons", we look up the
  action in the JogShuttleMapping map. The action is then located in
  the JogShuttleActions array and the right index is set for the
  Action optionmenu.
 
- Whenever the action optionmenu is changed, we get the action string
  from the list of all actions (keyed on index) and stuff this into
  the mappings. In other words, changing the action optionmenu,
  "saves" this choice.
 
* Using the mappings
 
The jogshuttle.cc code only needs a mapping from code, code to string,
which it gets from the Preference object.
 
* Saving the mappings
 
When saving, the stuff the action mapping into the config file.
 
********************************************************************** */


extern "C"
{
#include "support.h"
	extern struct navigate_control g_nav_ctl;
}

/// JogShuttle

/// A C wrappper for the GDK input handler callback
void JogShuttle_inputCallback( gpointer data, gint source, GdkInputCondition condition )
{
	JogShuttle * js = static_cast<JogShuttle *>( data );
	g_return_if_fail( js != NULL );
	js->inputCallback( source, condition );
}

/** The global JogShuttle object.
	 
    It is accesible to other classes via the getInstance method.
*/

JogShuttle *JogShuttle::_instance = NULL;

/** Singleton getInstance method. Standard pattern. Non-threadsafe, but
	first instance gets called from the commands.cc which should be OK.
*/

JogShuttle &JogShuttle::getInstance()
{
	if ( _instance == NULL )
		_instance = new JogShuttle();
	return *_instance;
}

/** Constructor
*/
JogShuttle::JogShuttle() :
		_callback( NULL ),
		input_( -1 ),
		monitorTag_( -1 ),
		_modifier_code( 0 )
{
	_ctrl.device = NULL;
	start();
}

/** Destructor
 
    Remove the callback function from the GDK input handler.
*/
JogShuttle::~JogShuttle()
{
	if ( input_ >= 0 )
	{
		gdk_input_remove( monitorTag_ );
		media_ctrl_close(&_ctrl);
	}
}

/** Start the GDK input handler if enabled.

    \return a boolean indicating if not started due to not enabled or device
            open failed.
*/
bool JogShuttle::start()
{
	Preferences & prefs = Preferences::getInstance();
	
	if ( prefs.enableJogShuttle )
	{
		stop();
		media_ctrl_open( &_ctrl );
		if ( _ctrl.device ) 
		{
			monitorTag_ = gdk_input_add( _ctrl.fd, GDK_INPUT_READ,
						     JogShuttle_inputCallback,
						     ( gpointer ) this );
			return true;
		}
	}
	return false;
}

/** Stop the GDK input handler.
*/
void JogShuttle::stop()
{
	if ( monitorTag_ != -1 )
	{
		gdk_input_remove( monitorTag_ );
		monitorTag_ = -1;
	}
	if ( _ctrl.device )
		media_ctrl_close( &_ctrl );
}

/** Register callback
 
    Register an interest in getting notification when buttons are pressed
    
    \param callback Function to call
*/
void JogShuttle::registerCallback( void * user, JogShuttleCallback callback )
{
	if ( _callback )
		g_warning( "JogShuttle::registerCallback - already registered\n" );
	_callback = callback;
	_callbackdata = user;
}

/** Deegister callback
 
    Deregister an interest in getting notification when buttons are pressed
    
    \param callback Function to stop calling
*/
void JogShuttle::deregisterCallback()
{
	if ( _callback == NULL )
		g_warning( "JogShuttle::deregisterCallback - not registered\n" );
	_callback = NULL;
}

struct media_ctrl_key *JogShuttle::getKeyset()
{
	if ( _ctrl.device != NULL ) return _ctrl.device->keys;
	else return NULL;
}






/** Handle movement on the jog dial.
 
    \param dir A number from -x to x to specify the offset from the current frame.
               A negative number moves backward. Typically, x is 1 to step frame-by-frame.
*/
void JogShuttle::jog( int offs )
{
	gdk_threads_enter();
	if ( offs < 0 )
		videoBackBy(offs);
	else
		videoForwardBy(offs);
	gdk_threads_leave();
}

/** Handle movement of the shuttle ring.
 
    \param angle A number from -8 to 8 that speccifies a direction and speed.
*/
void JogShuttle::shuttle( int angle )
{
	/*if ( angle > 0 )
		angle--;
	if ( angle < 0 )
		angle++;
	*/
	gdk_threads_enter();
	videoShuttle( angle );
	gdk_threads_leave();
}

/** Handle key press
 
    \param ev The key event
	Modifier keys - we maintain the state of the first key that is
	pressed down. This is a "poor man"s way of doing modifier
	keys. It means that we can press a single key, keep it down, and
	press a number of others. Any key can be used as a modifier key,
	although you would probably want to not assign an action to a
	modifier key when first pressed. There is no way to react to multiple
	modifier keys though.

*/
void JogShuttle::button( struct media_ctrl_event *ev )
{
	/* Figure out what codes to use */
	unsigned short first;
	unsigned short second;

	/* 
		Release may need to clear a modifier key
	*/
	if ( ev->value == KEY_RELEASE && _modifier_code != 0 )
	{
		_modifier_code = 0;
		return;
	}

	/* Do a callback or action based command */
	if ( _callback != NULL )
	{
		/* This is a key press - if there are no modifier, make sure that
		this is saved */
		if ( _modifier_code == 0 )
			_modifier_code = ev->index + 1;

		if ( _modifier_code != ev->index + 1 )
		{
			first = _modifier_code - 1;
			second = ev->index + 1;
		}
		else
		{
			first = ev->index; /* Same as modifier */
			second = 0;
		}
		_callback( _callbackdata, first, second );
	}
	else
	{
		/* This is a key press - if there are no modifier, make sure that
		this is saved */
		if ( _modifier_code == 0 )
			_modifier_code = ev->code;

		if ( _modifier_code != ev->code )
		{
			first = _modifier_code;
			second = ev->code;
		}
		else
		{
			first = ev->code; /* Same as modifier */
			second = 0;
		}
		
		/* Get the action from the prefs */
		string action
		= Preferences::getInstance()._JogShuttleMappings[
			  make_pair( first, second ) ]._action;

		if ( "" != action )
		{
			gdk_threads_enter();
			processCommand( ( char * ) action.c_str() );
			gdk_threads_leave();
		}
	}
	return ;

}




/** The GDK input callback function.
 
    GDK calls this whenever input is received. It is hooked into the system
    during object construction.
*/
void JogShuttle::inputCallback( gint source, GdkInputCondition condition )
{
	g_return_if_fail( this != NULL );
	// g_return_if_fail( input_ >= 0 );
	// g_return_if_fail( input_ == source );
	if ( condition != GDK_INPUT_READ )
		stop();
	g_return_if_fail( condition == GDK_INPUT_READ );

	struct media_ctrl_event ev;
	
	ev.type = MEDIA_CTRL_EVENT_NONE;
	media_ctrl_read_event(&_ctrl, &ev);


	/* We can get four "kinds" of events:

	ev.type == MEDIA_CTRL_EVENT_NONE (0x00)
	An event that actually isnt one... Since we read every event
	from the input subsystem, we sometimes get double events 
	(e.g. the shuttlepro reports its state approx. every second.)
	libmediactrl translates these events to none-events.
	
	
	ev.type == MEDIA_CTRL_EVENT_KEY (0x01)
	This is a button press. The whole event will be handled in the
	button method..

	ev.type == MEDIA_CTRL_EVENT_JOG (0x02)
	This event is issued when ever the dial position changes. 
	ev.value is the offset to the current jog position.


	- ev.code == EDIA_CTRL_EVENT_SHUTTLE (0x02) 
	the outer wheel ev.value indicates the position, with positive 
	values from 0x01 to 0x0f (inclusive) indicating clockwise twist, 
	and negative values from -1 to -15 indicating counterclockwise 
	twist.  A 0 value is reported. 


	*/

	if ( ev.type == MEDIA_CTRL_EVENT_NONE ) return;
#if 0
	printf( "JogShuttle: %02x %02x %02d\n", ev.type, ev.code, ev.value );
#endif


	if ( ev.type == MEDIA_CTRL_EVENT_JOG ) 
	{
		this->jog( CLAMP( ev.value, -1, 1 ) );
	} 
	else if ( ev.type == MEDIA_CTRL_EVENT_SHUTTLE ) 
	{
		this->shuttle( ev.value );
	}
	else if ( ev.type == MEDIA_CTRL_EVENT_KEY ) 
	{
		this->button( &ev );
	}
	else
	{
		return;
	}
}

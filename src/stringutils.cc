/*
* stringutils.cc -- C++ STL string functions
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sstream>
using std::ostringstream;
#include <stdio.h>

#include "stringutils.h"

string StringUtils::replaceAll ( string haystack, string needle, string s )
{
	string::size_type pos = 0;
	while ( ( pos = haystack.find ( needle, pos ) ) != string::npos )
	{
		haystack.erase ( pos, needle.length() );
		haystack.insert ( pos, s );
		pos += s.length();
	}
	return haystack;
}

string StringUtils::stripWhite ( string s )
{
	ostringstream dest;
	char c;
	for ( string::size_type pos = 0; pos < s.size(); ++pos )
	{
		c = s.at( pos );
		if ( c != 0x20 && c != 0x09 && c != 0x0d && c != 0x0a )
			dest << c;
	}
	return dest.str();
}


bool StringUtils::begins( string source, string sub )
{
	return ( source.substr ( 0, sub.length() ) == sub );
}

bool StringUtils::ends( string source, string sub )
{
	if ( sub.length() >= source.length() )
		return false;
	return ( source.substr( source.length() - sub.length() ) == sub );
}

string StringUtils::ltos( long num )
{
	char s[ 81 ];

	sprintf ( s, "%ld", num );
	return string( s );
}

string StringUtils::itos( int num )
{
	char s[ 81 ];

	sprintf ( s, "%d", num );
	return string( s );
}

/** Split the string by delimiter and return each resultant string in items.
 
    Note that the items vector is not cleaned betweeen iterations.
*/

int StringUtils::split( const string &input, const string &delimiter, vector< string > &items, const bool clean )
{
	int delimiter_size = delimiter.size();
	int input_size = input.size();

	// Find the first delimiter
	int position = 0;
	int end_position = input.find( delimiter, 0 );

	// While we have a valid position
	while ( end_position >= position )
	{
		// Obtain the substr and push if valid
		string s = input.substr( position, end_position - position );
		if ( !clean || s.size() > 0 )
			items.push_back( s );

		// Find the next delimiter
		position = end_position + delimiter_size;
		end_position = input.find( delimiter, position );
	}

	// Obtain the substr of what's left and push if valid
	string s = input.substr( position, input_size - position );
	if ( !clean || s.size() > 0 )
		items.push_back( s );

	// Return the number of items found
	return items.size();
}

/** Join the contents of items with the given delimiter.
 
    Note that the no leading or trailing delimters are output.
*/

string StringUtils::join( vector< string >&items, const string &delimiter )
{
	string output( "" );

	// Loop through the items
	for ( vector< string >::iterator item = items.begin( ); item != items.end( ); item ++ )
	{
		if ( item == items.begin() )
			output += *item;
		else
			output += delimiter + *item;
	}

	return string( output );
}

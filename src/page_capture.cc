/*
* page_capture.cc Notebook Firewire Capture Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include <gtk/gtk.h>
#include <math.h>
#include <limits.h>
#include <unistd.h>

#include "page_capture.h"
#include "riff.h"
#include "avi.h"
#include "playlist.h"
#include "filehandler.h"
#include "ieee1394io.h"
#include "framedisplayer.h"
#include "message.h"
#include "error.h"
#include "frame.h"
#include "page_editor.h"

static FrameDisplayer *displayer = NULL;

extern "C"
{
#include "support.h"
	#include "callbacks.h"
	#include "commands.h"

#include <pthread.h>

	extern struct navigate_control g_nav_ctl;

	// reader changed to pointer to delay creation of Frame
	// objects so prefs dv decoder quality is properly read
	IEEE1394Reader *reader = NULL;
	AVC *avc = NULL;
	static FileHandler *writer = NULL;
	extern char cmd[];
	static char lastcmd[ 256 ];
	static gchar lastPreferenceFilename[ 512 ];
	static quadlet_t avcStatus;
	char timecode[ 256 ];

	//
	// Preview rendering implementation
	//

	static void *captureThread( void * p );
	static void *avcThread( void * p );
	static void *videoThread( void * p );
	static pthread_t capturethread;
	static pthread_t avcthread;
	static pthread_t videothread;
	static gboolean audioOn = FALSE;
	static pthread_mutex_t writerlock = PTHREAD_MUTEX_INITIALIZER;

	// Define the size of the buffer
#define BUFFERED_FRAMES		25

	// Buffer and positional information
	static int framePosition = -1;
	static Frame *frameBuffer[ BUFFERED_FRAMES ];
	static int frameCount = 0;
	static void TriggerAction( );
	static int framesCaptured = 0;
	static int prevFramesWritten = 0;
}

/** Constructor for page.
 
  	\param common	common object to which this page belongs
*/

PageCapture::PageCapture( KinoCommon *common ) : isCapturing( false ), captureMutex( false ),
		avc_enabled( false ), driver_locked( false )
{
	std::cerr << "> Creating Capture Page" << std::endl;
	this->common = common;
	this->frameArea = GTK_DRAWING_AREA( lookup_widget( common->getWidget(), "capture_drawingarea" ) );
	gtk_widget_set_double_buffered( GTK_WIDGET( frameArea ), FALSE );
	this->avcButton = GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(), "capture_page_avc_button" ) );
	this->recordButton = GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(), "capture_page_record_button" ) );
	this->stopButton = GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(), "capture_page_stop_button" ) );
	this->muteButton = GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(), "capture_page_mute_button" ) );
	this->snapshotButton = GTK_BUTTON( lookup_widget( common->getWidget(), "capture_page_snapshot_button" ) );
	this->fileEntry = GTK_ENTRY( lookup_widget( common->getWidget(), "entry_capture_file" ) );
	this->timecodeLabel = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_current" ) );
	lastPreferenceFilename[ 0 ] = 0;
	memset( timecode, 0, sizeof( timecode ) );
}

/** Destructor for page.
*/

PageCapture::~PageCapture()
{
	std::cerr << "> Destroying Capture Page" << std::endl;
	delete reader;
	reader = NULL;
}

/** Called when a new file operation is selected by the user.
*/

void PageCapture::newFile( )
{
	strcpy( lastPreferenceFilename, "" );
}

/** Start of page.
*/

void PageCapture::start()
{
	std::cerr << ">> Starting Capture" << std::endl;

	// By default, AVC is always disabled
	driver_available = false;
	check_phyid = true;
	avc_enabled = false;
	validComponents = 0;
	gui_state_was = -1;

	gtk_toggle_button_set_active( avcButton, Preferences::getInstance().enableAVC );
	GtkLabel *label = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_total" ) );
	gtk_label_set_text( label, "" );

	// Create the reader object
#ifdef HAVE_IEC61883
	reader = new iec61883Reader( Preferences::getInstance().channel,
	                             Preferences::getInstance().dvCaptureBuffers );
#else
	reader = new dv1394Reader( Preferences::getInstance().channel,
	                           Preferences::getInstance().dvCaptureBuffers );
#endif

	// Create the AV/C object
	avc = new AVC();

	// Determine the capture directory and file - rules are:
	// 1) if a full path is specified in the preferences, then use that
	// 2) if a non-full path is specified, then pick up the project directory and attach preference

	string capture_file_name = Preferences::getInstance().file;
	if ( capture_file_name[ 0 ] != '/' )
		capture_file_name = common->getPlayList( ) ->GetProjectDirectory( ) + "/" + capture_file_name;

	// If the preferences have changed, then update the filename

	if ( capture_file_name != lastPreferenceFilename )
	{
		strcpy( lastPreferenceFilename, capture_file_name.c_str( ) );
		gtk_entry_set_text( fileEntry, capture_file_name.c_str( ) );
	}

	// Have the GUI reflect the mute status
	gtk_toggle_button_set_active( this->muteButton, !audioOn );

	// Create the FrameDisplayer
	displayer = new FrameDisplayer;

	// Set up the preview rendering
	g_nav_ctl.capture_active = TRUE;
	pthread_create( &capturethread, NULL, captureThread, this );
	pthread_create( &avcthread, NULL, avcThread, this );
	pthread_create( &videothread, NULL, videoThread, lookup_widget( common->getWidget(), "capture_drawingarea" ) );

	common->activateWidgets();
	gtk_label_set_text( timecodeLabel, "" );
	common->packIt( "packer_capture", "packer_capture_outer" );
	common->toggleComponents( VIDEO_STOP, false );
	common->commitComponentState();

	gtk_notebook_set_page( GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_keyhelp" ) ), 1 );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( lookup_widget( common->getWidget(), "menuitem_capture" ) ), TRUE );
}

/** Check devices and status.
*/

void PageCapture::CheckDevices( )
{
	static int avcRetry = 0;

	if ( driver_locked == true )
	{
		std::cerr << "Stopping thread" << std::endl;
		reader->StopThread( );
		driver_locked = false;
		driver_available = false;
		avc_enabled = false;
		check_phyid = true;
		TriggerAction( );
	}
	else if ( driver_available == false )
	{
		// Check if the id of the device is valid
		Preferences::getInstance( ).phyID = avc->getNodeId( Preferences::getInstance( ).avcGUID );
		Preferences::getInstance( ).phyID = avc->isPhyIDValid( Preferences::getInstance( ).phyID );
		check_phyid = Preferences::getInstance( ).phyID < 0;

		// Start the reader thread
		//std::cerr << "Starting thread" << std::endl;
		if ( reader->StartThread( ( avc->getPort() < 0 ) ? 0 : avc->getPort() ) )
		{
			// The drivers are installed
			driver_available = true;
			avc_enabled = false;
		}
		TriggerAction( );
	}
	else if ( check_phyid )
	{
		// Check that we're still valid.
		Preferences::getInstance( ).phyID = avc->isPhyIDValid( Preferences::getInstance( ).phyID );
		check_phyid = Preferences::getInstance( ).phyID < 0;
		avc_enabled = false;
		TriggerAction( );
	}
	else if ( avc_enabled )
	{
		quadlet_t status = avc->TransportStatus( Preferences::getInstance().phyID );

		if ( ( int ) status < 0 )
		{
			cerr << ">>> AVC status error" << std::endl;
			if ( ++avcRetry > 2 )
			{
				avcRetry = 0;
				avc_enabled = false;
				check_phyid = true;
			}
		}
		else
		{
			char t[ 12 ];

			avcStatus = status;
			avcRetry = 0;

			if ( !isCapturing && avc->Timecode( Preferences::getInstance().phyID, t ) )
			{
				strncpy( timecode, t, 12 );
			}
			TriggerAction();
		}
	}
	else
	{
		avc_enabled = Preferences::getInstance().enableAVC;
		gtk_toggle_button_set_active( this->avcButton, avc_enabled );
		avc->Noop();
	}
}

/** Reflect the the AV/C status in the transport buttons.
*/
void PageCapture::applyAVCState( quadlet_t newAVCState )
{
	if ( driver_available == false )
	{
		if ( gui_state_was != 0 )
		{
			gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), false );
			gtk_widget_set_sensitive( GTK_WIDGET( recordButton ), false );
			gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), false );
			gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), false );
			gtk_widget_set_sensitive( GTK_WIDGET( muteButton ), false );
			validComponents = 0;
			common->activateWidgets( );
			common->commitComponentState();
#ifdef HAVE_IEC61883
			common->setStatusBar( _( "WARNING: raw1394 kernel module not loaded or failure to read/write /dev/raw1394!" ) );
#else
			common->setStatusBar( _( "WARNING: dv1394 kernel module not loaded or failure to read/write %s" ),
				Preferences::getInstance().dvCaptureDevice );
#endif
		}
		gui_state_was = 0;
	}
	else if ( check_phyid == true )
	{
		if ( gui_state_was != 1 )
		{
			gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), false );
			gtk_widget_set_sensitive( GTK_WIDGET( recordButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( muteButton ), true );
			validComponents = 0;
			common->activateWidgets( );
			common->commitComponentState();
			common->setStatusBar( _( "No AV/C compliant cam connected or not switched on?" ) );
		}
		gui_state_was = 1;
	}
	else if ( avc_enabled )
	{
		if ( gui_state_was != 2 )
		{
			gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( recordButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( muteButton ), true );

			validComponents = VIDEO_REWIND |
			                  VIDEO_PLAY |
			                  VIDEO_STOP |
			                  VIDEO_FAST_FORWARD |
			                  VIDEO_FORWARD |
			                  VIDEO_BACK |
			                  VIDEO_NEXT_SCENE |
			                  VIDEO_START_OF_SCENE |
			                  VIDEO_START_OF_MOVIE |
			                  VIDEO_SHUTTLE |
			                  VIDEO_END_OF_MOVIE |
			                  INFO_FRAME;

			common->activateWidgets( );

			gui_state_was = 2;

			common->setStatusBar( "AV/C Controls Enabled" );
			cerr << ">>> AVC enabled " << std::endl;

			// Ensure that we reflect the current avc state when changing gui states
			avcState = true;
		}

		if ( avcState != newAVCState )
		{
			quadlet_t resp2 = AVC1394_MASK_RESPONSE_OPERAND( newAVCState, 2 );
			quadlet_t resp3 = AVC1394_MASK_RESPONSE_OPERAND( newAVCState, 3 );
			this->avcState = newAVCState;

			common->toggleComponents( common->getComponentState(), false );
			g_nav_ctl.active = FALSE;

			if ( resp2 == AVC1394_VCR_RESPONSE_TRANSPORT_STATE_PLAY )
			{
				if ( resp3 >= AVC1394_VCR_OPERAND_PLAY_FAST_FORWARD_1
				        && resp3 <= AVC1394_VCR_OPERAND_PLAY_FASTEST_FORWARD )
				{
					common->toggleComponents( VIDEO_FAST_FORWARD, true );
					g_nav_ctl.active = TRUE;
				}
				else if ( resp3 >= AVC1394_VCR_OPERAND_PLAY_FAST_REVERSE_1
				          && resp3 <= AVC1394_VCR_OPERAND_PLAY_FASTEST_REVERSE )
				{
					common->toggleComponents( VIDEO_REWIND, true );
					g_nav_ctl.active = TRUE;
				}
				else if ( resp3 == AVC1394_VCR_OPERAND_PLAY_FORWARD_PAUSE )
				{
					common->toggleComponents( VIDEO_PLAY, false );
				}
				else
				{
					common->toggleComponents( VIDEO_PLAY, true );
					g_nav_ctl.active = TRUE;
				}
			}
			else if ( resp2 == AVC1394_VCR_RESPONSE_TRANSPORT_STATE_WIND )
			{
				if ( resp3 == AVC1394_VCR_OPERAND_WIND_HIGH_SPEED_REWIND )
				{
					common->toggleComponents( VIDEO_START_OF_MOVIE, true );
				}
				else if ( resp3 == AVC1394_VCR_OPERAND_WIND_STOP )
				{
					stopCapture();
					common->toggleComponents( VIDEO_STOP, true );
				}
				else if ( resp3 == AVC1394_VCR_OPERAND_WIND_REWIND )
				{
					common->toggleComponents( VIDEO_START_OF_MOVIE, true );
				}
				else if ( resp3 == AVC1394_VCR_OPERAND_WIND_FAST_FORWARD )
				{
					common->toggleComponents( VIDEO_END_OF_MOVIE, true );
				}
				else
				{
					std::cerr << "AVC Status: Unkown winding" << std::endl;
				}
			}
			else if ( resp2 == AVC1394_VCR_RESPONSE_TRANSPORT_STATE_RECORD )
			{
				if ( resp3 == AVC1394_VCR_OPERAND_RECORD_PAUSE )
				{
					common->toggleComponents( VIDEO_PLAY, false );
				}
				else
				{
					common->toggleComponents( VIDEO_PLAY, true );
					g_nav_ctl.active = TRUE;
				}
			}
			else
			{
				std::cerr << "AVC Status: Unknown state" << std::endl;
			}
			common->commitComponentState();
		}
	}
	else
	{
		if ( gui_state_was != 3 )
		{
			gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( recordButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( muteButton ), true );
			validComponents = 0;
			common->activateWidgets( );
			common->commitComponentState();
			if ( !isCapturing )
				common->setStatusBar( _( "AV/C Controls Not Enabled." ) );
			gui_state_was = 3;
		}
	}
}

/** Define active widgets.
*/

gulong PageCapture::activate()
{
	return this->validComponents | SCENE_LIST;
}

/** Leaving the page
*/

void PageCapture::clean()
{
	std::cerr << ">> Leaving Capture" << std::endl;
	
	if ( g_nav_ctl.capture_active == TRUE )
	{
		// Stop the capture
		stopCapture();

		// Stop the video
		if ( avc_enabled && Preferences::getInstance( ).phyID >= 0 )
			videoStop();

		// Start the deactivation process for the backround processes
		g_nav_ctl.active = FALSE;
		g_nav_ctl.capture_active = FALSE;

		// Join with the capture thread to make sure we're definitely finished
		reader->TriggerAction();
		pthread_join( capturethread, NULL );
		pthread_join( avcthread, NULL );

		// Stop the video thread
		gdk_threads_leave();
		pthread_join( videothread, NULL );
		gdk_threads_enter();

		// Stop the reading thread
		reader->StopThread();
	}

	common->setStatusBar( "" );

	// Clean up objects created in start
	delete displayer;
	displayer = NULL;
	delete reader;
	reader = NULL;
	delete avc;
	avc = NULL;
}


/** Start of movie.
*/

void PageCapture::videoStartOfMovie()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_START_OF_MOVIE, avc_enabled );
		if ( avc->Stop( Preferences::getInstance().phyID ) >= 0 )
			avc->Rewind( Preferences::getInstance().phyID );
	}
}

/** Start of previous scene.
*/

void PageCapture::videoPreviousScene()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_START_OF_SCENE, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->PreviousScene( Preferences::getInstance().phyID );
	}
}

/** Start of current scene.
*/

void PageCapture::videoStartOfScene()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_START_OF_SCENE, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->PreviousScene( Preferences::getInstance().phyID );
	}
}

/** Rewind.
*/

void PageCapture::videoRewind()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_REWIND, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->Rewind( Preferences::getInstance().phyID );
	}
}

/** Frame back.
*/

void PageCapture::videoBack( int step )
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_BACK, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->Back( Preferences::getInstance().phyID );
	}
}

/** Play.
*/

void PageCapture::videoPlay()
{
	if ( avc_enabled )
	{
		if ( g_nav_ctl.active == TRUE )
		{
			audioOn = FALSE;
			gtk_toggle_button_set_active( this->muteButton, TRUE );
			common->toggleComponents( VIDEO_PLAY, false );
			avc->Pause( Preferences::getInstance().phyID );
		}
		else
		{
			audioOn = TRUE;
			gtk_toggle_button_set_active( this->muteButton, FALSE );
			common->toggleComponents( VIDEO_PLAY, true );
			avc->Play( Preferences::getInstance().phyID );
		}
	}
}

/** Pause.
*/

void PageCapture::videoPause()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		avc->Pause( Preferences::getInstance().phyID );
	}
}

/** Stop.
*/

void PageCapture::videoStop()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_STOP, avc_enabled );
		avc->Stop( Preferences::getInstance().phyID );
	}
}

/** Forward.
*/

void PageCapture::videoForward( int step )
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_FORWARD, false );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->Forward( Preferences::getInstance().phyID );
	}
}

/** Fast Forward.
*/

void PageCapture::videoFastForward()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_FAST_FORWARD, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->FastForward( Preferences::getInstance().phyID );
	}
}

/** Start of next scene.
*/

void PageCapture::videoNextScene()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_NEXT_SCENE, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->NextScene( Preferences::getInstance().phyID );
	}
}

/** End of current scene.
*/

void PageCapture::videoEndOfScene()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_NEXT_SCENE, avc_enabled );
		if ( avc->Pause( Preferences::getInstance().phyID ) >= 0 )
			avc->NextScene( Preferences::getInstance().phyID );
	}
}

/** End of movie.
*/

void PageCapture::videoEndOfMovie()
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );
		common->toggleComponents( VIDEO_END_OF_MOVIE, avc_enabled );
		if ( avc->Stop( Preferences::getInstance().phyID ) >= 0 )
			avc->FastForward( Preferences::getInstance().phyID );
	}
}

/** Shuttle.

	Bi-directional variable-speed playback.

	\param angle The relative speed.
*/

void PageCapture::videoShuttle( int angle )
{
	if ( avc_enabled )
	{
		audioOn = FALSE;
		if ( angle != 0 )
			avc->Shuttle( Preferences::getInstance().phyID, angle * 2 );
		else
			avc->Pause( Preferences::getInstance().phyID );
	}
}


/** Process a keyboard event.

  	\param event	keyboard event
*/

gboolean PageCapture::processKeyboard( GdkEventKey *event )
{
	gboolean ret = FALSE;

	// Only process while not escape mode
	if ( g_nav_ctl.escaped == FALSE )
	{
		if ( strcmp( lastcmd, "alt" ) == 0 )
		{
			strcpy( lastcmd, "" );
			return ret;
		}

		// Translate special keys to equivalent command
		switch ( event->keyval )
		{
		case GDK_Home:
			strcat( cmd, "gg");
			ret = TRUE;
			break;
		case GDK_End:
			strcat( cmd, "G");
			ret = TRUE;
			break;
		case GDK_BackSpace:
		case GDK_Left:
			strcat( cmd, "h" );
			ret = TRUE;
			break;
		case GDK_Up:
			strcat( cmd, "k" );
			ret = TRUE;
			break;
		case GDK_Right:
			strcat( cmd, "l" );
			ret = TRUE;
			break;
		case GDK_Return:
			startCapture();
			cmd[ 0 ] = 0;
			break;
		case GDK_Down:
			strcat( cmd, "j" );
			ret = TRUE;
			break;
		case GDK_Escape:
			if ( this->isCapturing )
			{
				common->keyboardFeedback( "Esc", _( "Stop Capture" ) );
				stopCapture();
			}
			else if ( common->getComponentState() & VIDEO_STOP )
			{
				common->changePageRequest( PAGE_EDITOR );
			}
			else if ( avc_enabled && Preferences::getInstance( ).phyID >= 0 )
			{
				common->keyboardFeedback( "Esc", _( "Stop" ) );
				common->videoStop( );
			}
			else
			{
				common->changePageRequest( PAGE_EDITOR );
			}
			cmd[ 0 ] = 0;
			return TRUE;
		case GDK_Alt_L:
		case GDK_Alt_R:
			strcpy( lastcmd, "alt" );
			return ret;
		default:
			strcat( cmd, event->string );
			break;
		}

		if ( !strcmp( cmd, "." ) )
			strcpy( cmd, lastcmd );

		processCommand( cmd );

	}
	else if ( event->keyval == GDK_Return || event->keyval == GDK_Escape )
	{
		g_nav_ctl.escaped = FALSE;
		gtk_entry_set_editable( fileEntry, FALSE );
		gtk_widget_grab_focus( common->getWidget() );
	}
	return ret;
}

/** Internal method for handling a complete keyboard scene.

  	\param cmd		command to be processed;
*/

gboolean PageCapture::processCommand( const char *command )
{
	int count = 1;
	char real[ 256 ] = "";

	if ( cmd != command )
	{
		strncpy( cmd, command, 256 );
		cmd[ 255 ] = '\0';
	}

	switch ( sscanf( cmd, "%d%s", &count, real ) )
	{
	case 1:
		// Numeric value only - return immediately if the cmd is not "0"
		if ( strcmp( cmd, "0" ) )
		{
			common->keyboardFeedback( cmd, "" );
			return FALSE;
		}
		break;
	case 0:
		sscanf( cmd, "%s", real );
		count = 1;
		break;
	}

	strcpy( lastcmd, cmd );

	/* mode switching */
	
	if ( strcmp( cmd, "t" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Trim" ) );
		common->changePageRequest( PAGE_TRIM );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "v" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Timeline" ) );
		common->changePageRequest( PAGE_TIMELINE );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "C" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "FX" ) );
		common->changePageRequest( PAGE_MAGICK );
		cmd[ 0 ] = 0;
	}

	/* switch to export mode */

	else if ( strcmp( cmd, ":W" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Export" ) );
		common->changePageRequest( PAGE_EXPORT );
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( cmd, "F2" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Edit" ) );
		common->changePageRequest( PAGE_EDITOR );
		cmd[ 0 ] = 0;
	}

	/* write PlayList */

	else if ( strcmp( cmd, ":w" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Write playlist" ) );
		common->savePlayList( );
		cmd[ 0 ] = 0;
	}

	/* quit */

	else if ( strcmp( cmd, ":q" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "quit" ) );
		kinoDeactivate();
		cmd[ 0 ] = 0;
	}
	
	else if ( strcmp( real, "Enter" ) == 0 && ! isCapturing )
	{
		common->keyboardFeedback( cmd, _( "Start Capture" ) );
		startCapture();
		cmd[ 0 ] = 0;
	}

	else if ( strcmp( real, "Esc" ) == 0 )
	{
		if ( this->isCapturing )
		{
			common->keyboardFeedback( "Esc", _( "Stop Capture" ) );
			stopCapture();
		}
		else if ( avc_enabled && Preferences::getInstance( ).phyID >= 0 )
		{
			common->keyboardFeedback( "Esc", _( "Stop" ) );
			common->videoStop( );
		}
		cmd[ 0 ] = 0;
	}

	/* Navigation */

	if ( validComponents == 0 || Preferences::getInstance( ).phyID < 0 )
		return FALSE;

	/* play, pause */

	if ( strcmp( cmd, " " ) == 0 )
	{
		if ( g_nav_ctl.active == FALSE )
		{
			common->keyboardFeedback( cmd, _( "Play" ) );
			common->videoPlay( );
		}
		else
		{
			common->keyboardFeedback( cmd, _( "Pause" ) );
			common->videoPause( );
		}
		cmd[ 0 ] = 0;
	}

	/* advance one frame */

	else if ( strcmp( real, "l" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move forward" ) );
		common->videoForward();
		cmd[ 0 ] = 0;
	}

	/* backspace one frame */

	else if ( strcmp( real, "h" ) == 0 )
	{
		common->keyboardFeedback( cmd, _( "Move backward" ) );
		common->videoBack();
		cmd[ 0 ] = 0;
	}

	/* advance one second */

	else if ( ( strcmp( real, "w" ) == 0 || strcmp( real, "W" ) == 0 ||
	            strcmp( real, "e" ) == 0 || strcmp( real, "E" ) == 0 ) )
	{
		common->keyboardFeedback( cmd, _( "Fast Forward" ) );
		common->videoFastForward();
		cmd[ 0 ] = 0;
	}

	/* backspace one second */

	else if ( ( strcmp( real, "b" ) == 0 || strcmp( real, "B" ) == 0 ) )
	{
		common->keyboardFeedback( cmd, _( "Rewind" ) );
		common->videoRewind();
		cmd[ 0 ] = 0;
	}

	/* start of scene */

	else if ( ( strcmp( cmd, "0" ) == 0 || strcmp( real, "^" ) == 0 ) )
	{
		common->videoStartOfScene( );
		for ( ; count > 1; count -- )
		{
			common->g_currentFrame --;
			videoStartOfScene( );
		}
		common->keyboardFeedback( cmd, _( "Move to start of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* end of scene */

	else if ( strcmp( real, "$" ) == 0 )
	{
		common->videoEndOfScene( );
		for ( ; count > 1; count -- )
		{
			common->g_currentFrame ++;
			videoEndOfScene( );
		}
		common->keyboardFeedback( cmd, _( "Move to end of scene" ) );
		cmd[ 0 ] = 0;
	}

	/* start of next scene */

	else if ( ( strcmp( real, "j" ) == 0 || strcmp( real, "+" ) == 0 ) )
	{
		for ( ; count >= 1; count -- )
			common->videoNextScene( );
		common->keyboardFeedback( cmd, _( "Move to start of next scene" ) );
		cmd[ 0 ] = 0;
	}

	/* start of previous scene */

	else if ( ( strcmp( real, "k" ) == 0 || strcmp( real, "-" ) == 0 ) )
	{
		for ( ; count >= 1; count -- )
			common->videoPreviousScene( );
		common->keyboardFeedback( cmd, _( "Move to start of previous scene" ) );
		cmd[ 0 ] = 0;
	}

	/* first frame */

	else if ( strcmp( cmd, "gg" ) == 0 )
	{
		common->videoStartOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to first frame" ) );
		cmd[ 0 ] = 0;
	}

	/* last frame */

	else if ( strcmp( cmd, "G" ) == 0 )
	{
		common->videoEndOfMovie( );
		common->keyboardFeedback( cmd, _( "Move to last frame" ) );
		cmd[ 0 ] = 0;
	}

	else
	{
		// Check for invalid commands
		if ( strlen( real ) > 3 )
			cmd[ 0 ] = 0;
		else if ( strchr( "dgy ", real[ strlen( real ) - 1 ] ) == NULL )
			cmd[ 0 ] = 0;

		common->keyboardFeedback( cmd, "" );

#if 0

		printf( "send_event: %2.2x\n", event->send_event );
		printf( "time  : %8.8x\n", event->time );
		printf( "state : %8.8x\n", event->state );
		printf( "keyval: %8.8x\n", event->keyval );
		printf( "length: %8.8x\n", event->length );
		printf( "string: %s\n", event->string );
		printf( "(hex) : %2.2x\n", event->string[ 0 ] );
		printf( "cmd   : %s\n", cmd );
#endif

	}

	return FALSE;
}

/** Start capturing video to the disk.

    If AV/C is enabled, tell the device to play and temporarily
    disable AV/C and shutdown the AV/C status/timecode monitor.
*/
void PageCapture::startCapture()
{
	if ( !captureMutex && !isCapturing )
	{
		captureMutex = true;
		isCapturing = true;
		gtk_toggle_button_set_active( recordButton, true );
		gtk_toggle_button_set_active( stopButton, false );
		gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), false );
		gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), false );
		captureMutex = false;

		char filename[ 512 ];
		strcpy( filename, gtk_entry_get_text( fileEntry ) );

		if ( !strcmp( filename, "" ) )
		{
			common->setStatusBar( _( "You must enter a filename to capture." ) );
			captureMutex = true;
			gtk_toggle_button_set_active( recordButton, false );
			gtk_toggle_button_set_active( stopButton, true );
			gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), true );
			gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), true );
			captureMutex = false;
			isCapturing = false;

		}
		else
		{
			Frame *frame = NULL;
			int retry;
			const int MAX_RETRIES = 10;
			struct timespec t =
			    {
				    1, 0UL
			    };

			if ( avc_enabled )
			{
				g_nav_ctl.active = FALSE;
				videoPlay();
				g_nav_ctl.active = TRUE;
				gui_state_was = 2;
				validComponents = 0;
				common->activateWidgets( );
				common->commitComponentState();
				gtk_widget_set_sensitive( GTK_WIDGET( recordButton ), true );
				gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), true );
				gtk_widget_set_sensitive( GTK_WIDGET( muteButton ), true );
				gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), false );
				gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), false );
			}

			FileHandler *handler = NULL;

			switch ( Preferences::getInstance().fileFormat )
			{
			case RAW_FORMAT:
				handler = new RawHandler();
				break;
#ifdef HAVE_LIBQUICKTIME

			case QT_FORMAT:
				handler = new QtHandler();
				break;
#endif

			case AVI_DV1_FORMAT:
			case AVI_DV2_FORMAT:
				AVIHandler *aviWriter = new AVIHandler( Preferences::getInstance().fileFormat );
				handler = aviWriter;
				aviWriter->SetOpenDML( Preferences::getInstance().isOpenDML ||
				                       Preferences::getInstance().maxFileSize == 0 ||
				                       Preferences::getInstance().maxFileSize > 1000 );
				break;
			}

			/* set command line parameters to the handler object */

			handler->SetMaxFrameCount( Preferences::getInstance().frames );
			handler->SetAutoSplit( Preferences::getInstance().autoSplit );
			handler->SetTimeStamp( Preferences::getInstance().timeStamp );
			handler->SetEveryNthFrame( Preferences::getInstance().every );
			handler->SetMaxFileSize( ( off_t ) Preferences::getInstance().maxFileSize * ( off_t ) ( 1024 * 1024 ) );
			handler->SetBaseName( filename );

			// without this, if no video has yet played, then frameBuffer is
			// likely to be empty due to latency of avcPlay above
			for ( retry = MAX_RETRIES; retry > 0 && isCapturing; --retry )
			{
				frame = frameBuffer[ framePosition ];

				if ( frame != NULL )
				{
					// frame must be normal speed to get correct audio stream params for type2 AVI
					if ( frame->IsNormalSpeed() )
					{
						reader->ResetDroppedFrames();
						if ( handler->WriteFrame( *frame ) == false )
						{
							delete handler;
							captureMutex = false;
							stopCapture();
							modal_message( _( "Failed to write to file. Is file name OK?" ) );
						}
						else
						{
							pthread_mutex_lock( &writerlock );
							writer = handler;
							pthread_mutex_unlock( &writerlock );
							common->setStatusBar( ( string( _( "Capturing " ) ) + handler->GetFilename() ).c_str() );
						}
						break;
					}
				}
				common->setStatusBar( _( "Waiting for DV %s%d ..." ),
				                      ( avc_enabled ? "" : _( "(press play on camera) " ) ), retry );
				while ( gtk_events_pending() )
					gtk_main_iteration();
				nanosleep( &t, NULL );
			}
			if ( retry == 0 && isCapturing )
			{
				// timed out waiting for iso stream, stop capture
				this->stopCapture();
				common->setStatusBar( _( "Capture failed." ) );
			}
			else if ( !isCapturing )
				common->setStatusBar( _( "Capture stopped." ) );
		}
	}
	else if ( !captureMutex )
	{
		captureMutex = true;
		gtk_toggle_button_set_active( recordButton, true );
		captureMutex = false;
	}
}

/** Stop capturing video to the disk.

    If AV/C was previously enabled, then re-enable it, pause the
    device and restart the AV/C status/timecode monitor.
*/
void PageCapture::stopCapture()
{

	if ( !captureMutex && isCapturing )
	{
		captureMutex = true;

		pthread_mutex_lock( &writerlock );
		if ( writer != NULL )
		{
			writer->Close();
			delete writer;
			writer = NULL;
		}
		pthread_mutex_unlock( &writerlock );

		gtk_toggle_button_set_active( recordButton, false );
		gtk_toggle_button_set_active( stopButton, true );
		gtk_widget_set_sensitive( GTK_WIDGET( snapshotButton ), true );
		gtk_widget_set_sensitive( GTK_WIDGET( avcButton ), true );

		audioOn = FALSE;
		gtk_toggle_button_set_active( this->muteButton, !audioOn );

		if ( avc_enabled )
		{
			gtk_toggle_button_set_active( avcButton, avc_enabled );
			gui_state_was = -1;
			videoPause( );
		}
		common->setStatusBar( "" );
		isCapturing = false;
		captureMutex = false;
	}
	else if ( !captureMutex )
	{
		captureMutex = true;
		gtk_toggle_button_set_active( stopButton, true );
		captureMutex = false;
	}
	
	// Pick up any captured files
	for ( unsigned int index = 0; index < FileTracker::GetInstance().Size(); index ++ )
	{
		char *file = FileTracker::GetInstance().Get( ( int ) index );
		switch ( FileTracker::GetInstance().GetMode() )
		{
		case CAPTURE_IGNORE:
			// Do nothing
			break;
		case CAPTURE_FRAME_INSERT:
			common->loadMediaObject( file, common->g_currentFrame == -1 ? 0 : common->g_currentFrame );
			break;
		case CAPTURE_FRAME_APPEND:
			common->loadMediaObject( file, common->g_currentFrame + 1 );
			break;
		case CAPTURE_MOVIE_APPEND:
			common->loadMediaObject( file, common->getPlayList() ->GetNumFrames() );
			break;
		}
		common->hasListChanged = TRUE;
	}

	// Clear the list to avoid reloading on a subsequent re-entry
	FileTracker::GetInstance().Clear();

	// Just to make sure we refresh what needs to be refreshed
	if ( common->hasListChanged )
	{
		GetEditorBackup()->Store( common->getPlayList() );
		common->setWindowTitle( );
		common->getPageEditor()->DrawBar( common->g_currentFrame );
		if ( common->g_currentFrame == -1 )
			common->g_currentFrame = 0;
	}
	
}


/** Save a single still frame of video to the disk.
*/
void PageCapture::saveFrame()
{
	Frame * frame = frameBuffer[ framePosition ];

	if ( frame != NULL )
	{
		unsigned char pixels[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
		GError *gerror = NULL;

		frame->ExtractPreviewRGB( pixels );
		GdkPixbuf *im = gdk_pixbuf_new_from_data
		                ( pixels, GDK_COLORSPACE_RGB, FALSE, 8,
		                  frame->GetWidth(), frame->GetHeight(),
		                  frame->GetWidth() * 3, NULL, NULL );

		// resample pixel aspect
		{
			int width = frame->GetWidth();
			if ( frame->IsWide() )
				width = frame->IsPAL() ? 1024 : 854;
			AspectRatioCalculator calc( width, frame->GetHeight(),
		                            frame->GetWidth(), frame->GetHeight(),
		                            frame->IsPAL(), frame->IsWide() );
			GdkPixbuf *scaled = gdk_pixbuf_scale_simple( im, calc.width, calc.height, GDK_INTERP_HYPER );
			g_object_unref( im );
			im = scaled;
		}

		char *filename = common->getFileToSave( _( "Save Still Frame" ) );
		if ( strcmp( filename, "" ) )
		{
			const std::string FileName( filename );
			const std::string suffix( FileName.begin() + FileName.rfind( "." ), FileName.end() );
			if ( suffix == ".png" )
				gdk_pixbuf_save( im, filename, "png", &gerror, NULL );
			else
				gdk_pixbuf_save( im, filename, "jpeg", &gerror, "quality", "80", NULL );
			if ( gerror != NULL )
			{
				modal_message( gerror->message );
				g_error_free( gerror );
			}
		}

		g_object_unref( im );
	}
}

void PageCapture::windowMoved()
{
	if ( frameBuffer[ framePosition ] == NULL )
		common->clearPreview( frameArea );
	else
		displayer->Put( *frameBuffer[ framePosition ], GTK_WIDGET( frameArea ), TRUE );
}


/** Show the AV/C timecode.
*/
void PageCapture::showFrameInfo( int i )
{
	GtkLabel *total = GTK_LABEL( lookup_widget( common->getWidget(), "position_label_total" ) );
	string tc;
	char msg[ 128 ] = "";
	Frame *frame = getFrame();
	
	if ( isCapturing && frame )
	{
		common->getTime().setFramerate( frame->GetFrameRate() );
		string tc = "<span size=\"x-large\">" + common->getTime().parseFramesToString( i, common->getTimeFormat() ) + "</span>";
		gtk_label_set_markup( timecodeLabel, tc.c_str() );
		gtk_widget_set_redraw_on_allocate( GTK_WIDGET( timecodeLabel ), FALSE );
		snprintf( msg, 128, "%s %d", _("Dropped:"), reader->GetDroppedFrames() );
		gtk_label_set_markup( total, msg );
	}
	else if ( avc_enabled && common->getTimeFormat() != SMIL::Time::TIME_FORMAT_NONE )
	{
		snprintf( msg, 128, "<span size=\"x-large\">%s</span>", timecode );
		gtk_label_set_markup( timecodeLabel, msg );
		gtk_widget_set_redraw_on_allocate( GTK_WIDGET( timecodeLabel ), FALSE );
		gtk_label_set_markup( total, "" );
	}
}

/** Fetch the currently showing frame.
*/
Frame* PageCapture::getFrame()
{
	return frameBuffer[ framePosition ];
}


void PageCapture::collectFiles( )
{
		string file;
		
		// Pick up any captured files
		for ( unsigned int index = 0; index < FileTracker::GetInstance().Size(); index ++ )
		{
			file = FileTracker::GetInstance().Get( ( int ) index );
			if ( index == FileTracker::GetInstance().Size() - 1 )
				break;
			cerr << ">>> File " << file << " returned from tracker" << endl;
			switch ( FileTracker::GetInstance().GetMode() )
			{
				case CAPTURE_IGNORE:
					// Do nothing
					break;
				case CAPTURE_FRAME_INSERT:
					common->loadMediaObject( const_cast< char* >( file.c_str() ), common->g_currentFrame == -1 ? 0 : common->g_currentFrame );
					break;
				case CAPTURE_FRAME_APPEND:
					common->loadMediaObject( const_cast< char* >( file.c_str() ), common->g_currentFrame + 1 );
					break;
				case CAPTURE_MOVIE_APPEND:
					common->loadMediaObject( const_cast< char* >( file.c_str() ), common->getPlayList() ->GetNumFrames() );
					break;
			}
			common->hasListChanged = TRUE;
		}
		
		// Clear the list to avoid reloading on a subsequent re-entry
		FileTracker::GetInstance().Clear();

		if ( file.length() )
			FileTracker::GetInstance().Add( file.c_str() );

		// Just to make sure we refresh what needs to be refreshed
		if ( common->hasListChanged )
		{
			gdk_threads_enter();
			common->getPageEditor()->DrawBar( common->g_currentFrame );
			gdk_threads_leave();
		}
}

/*
 * Callbacks
 */

extern "C"
{
	//
	// Threaded implementation
	//

	static pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
	static pthread_cond_t condition = PTHREAD_COND_INITIALIZER;

	static int WaitForAction( int lastPosition )
	{
		if ( framePosition == lastPosition )
		{
			pthread_mutex_lock( &condition_mutex );
			pthread_cond_wait( &condition, &condition_mutex );
			pthread_mutex_unlock( &condition_mutex );
		}

		return framePosition;
	}

	static void TriggerAction( )
	{
		pthread_mutex_lock( &condition_mutex );
		pthread_cond_signal( &condition );
		pthread_mutex_unlock( &condition_mutex );
	}

	static void *captureThread( void * p )
	{
		Frame * frame = NULL;

		// Make sure we can tell the difference between a used and unused frame
		for ( int index = 0; index < BUFFERED_FRAMES; index ++ )
			frameBuffer[ index ] = NULL;
		
		framesCaptured = 0;

		// Loop until we're informed otherwise
		while ( g_nav_ctl.capture_active )
		{
			// Wait for the reader to indicate that something has happened
			reader->WaitForAction( );

			// Get the next frame
			frame = reader->GetFrame();

			// Check if the out queue is falling behind
			bool critical_mass = reader->GetOutQueueSize( ) > reader->GetInQueueSize( );

			// Make sure we return the oldest frame first
			if ( frame != NULL && frameCount == BUFFERED_FRAMES )
			{
				int oldest = ( framePosition + 1 ) % BUFFERED_FRAMES;
				frameBuffer[ oldest ] = NULL;
				frameCount --;
			}

			// Put the last collected frame into the buffer so it can be picked up
			// by the video thread.
			if ( frame != NULL && !critical_mass )
			{
				// Bung it in to the buffer
				int newest = ( framePosition + 1 ) % BUFFERED_FRAMES;
				frameBuffer[ newest ] = frame;
				frameCount ++;
				framePosition = newest;
				TriggerAction( );
			}

			// Do with it what needs to be done
			if ( frame != NULL )
			{
				TimeCode tc;
				frame->GetTimeCode( tc );
				if ( frame->IsNormalSpeed() &&
				     ! ( tc.hour == 0 && tc.min == 0 && tc.sec == 0 && tc.frame == 0 ) )
				{
					// Play audio
					if ( audioOn && !critical_mass )
						if ( writer == NULL || ( writer != NULL && Preferences::getInstance().preview_capture ) )
							displayer->PutSound( *frame );
	
					// All access to the writer is protected
					pthread_mutex_lock( &writerlock );
					if ( writer != NULL )
					{
						if ( !writer->WriteFrame( *frame ) )
						{
							pthread_mutex_unlock( &writerlock );
							gdk_threads_enter();
							common->getPageCapture() ->stopCapture();
							common->setStatusBar( _( "WARNING: Capture stopped. Disk full?" ) );
							gdk_threads_leave();
						}
						framesCaptured++;
					}
					pthread_mutex_unlock( &writerlock );
				}
				// Return the frame for reuse
				reader->DoneWithFrame( frame );

			}
		}

		// Set the buffer positional vars to defaults to allow restart
		frameCount = 0;
		framePosition = -1;

		g_nav_ctl.capture_active = FALSE;

		TriggerAction( );

		return NULL;
	}


	static void *videoThread( gpointer p )
	{
		int lastPosition = -1;

		GtkWidget *drawingarea = ( GtkWidget* ) p;
		prevFramesWritten = 0;

		while ( g_nav_ctl.capture_active )
		{
			int position = WaitForAction( lastPosition );

			// Lock the gui
			gdk_threads_enter();

			// If we have a valid position and it's different to the last iteration
			if ( g_nav_ctl.capture_active && position != -1 && position != lastPosition )
			{

				// ... display it - note, the displayer and the frame may be destroyed
				// while waiting for the thread lock so additional checks are needed
				if ( ( displayer != NULL && frameBuffer[ position ] != NULL ) )
				{
					if ( writer == NULL || ( writer != NULL && Preferences::getInstance().preview_capture ) )
					{
						displayer->Put( *frameBuffer[ position ], drawingarea, TRUE );
					}
				}

				// Remember the buffer position of the last frame displayed
				lastPosition = position;
			}

			// Apply the changes to the GUI
			common->getPageCapture() ->applyAVCState( avcStatus );
			common->getPageCapture()->showFrameInfo( framesCaptured );

			gdk_flush();
			gdk_threads_leave();
			
			pthread_mutex_lock( &writerlock );
			if ( writer )
			{
				int framesWritten = writer->GetFramesWritten();
				string filename = writer->GetFilename();
				pthread_mutex_unlock( &writerlock );
				
				if ( framesWritten < prevFramesWritten )
				{
					gdk_threads_enter();
					common->setStatusBar( ( string( _( "Capturing " ) ) + filename ).c_str() );
					gdk_threads_leave();
					common->getPageCapture()->collectFiles();
				}
				prevFramesWritten = framesWritten;
			}
			else
			{
				pthread_mutex_unlock( &writerlock );
			}

			// Release temporarily
			struct timespec t = { 0, 1000000 }; // 1ms
			nanosleep( &t, NULL );
		}

		return NULL;
	}

	static void *avcThread( gpointer p )
	{
		while ( g_nav_ctl.capture_active )
		{
			// Check that device is still valid
			common->getPageCapture() ->CheckDevices( );

			// Release temporarily
			struct timespec t =
			    {
				    0, ( unsigned long ) Preferences::getInstance().avcPollIntervalMs
				    * 1000000UL
			    };
			nanosleep( &t, NULL );
		}

		return NULL;
	}

	void
	on_capture_page_record_button_clicked ( GtkButton * button,
	                                        gpointer user_data )
	{
		startCapture();
	}


	void
	on_capture_page_stop_button_clicked ( GtkButton * button,
	                                      gpointer user_data )
	{
		stopCapture();
	}

	void
	on_capture_page_snapshot_button_clicked ( GtkButton * button,
	        gpointer user_data )
	{
		saveFrame();
	}

	gboolean
	on_entry_capture_file_focus_in_event   (GtkWidget       *widget,
                                            GdkEventFocus   *event,
                                            gpointer         user_data)
	{
		gtk_entry_set_editable( GTK_ENTRY( widget ), TRUE );
		g_nav_ctl.escaped = TRUE;
		return FALSE;
	}


	gboolean
	on_entry_capture_file_focus_out_event  (GtkWidget       *widget,
                                            GdkEventFocus   *event,
                                            gpointer         user_data)
	{
		gtk_entry_set_editable( GTK_ENTRY( widget ), FALSE );
		g_nav_ctl.escaped = FALSE;
		return FALSE;
	}


	void
	on_capture_file_button_clicked         (GtkButton       *button,
                                            gpointer         user_data)
	{
		g_nav_ctl.escaped = TRUE;
		gtk_entry_set_editable( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_capture_file" ) ), TRUE );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_capture_file" ) );
		const char *filename = common->getFileToSave( _("Choose a DV file") );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_capture_file" ) ), filename );
	}

	void
	on_entry_capture_file_grab_focus ( GtkWidget * widget,
	                                   gpointer user_data )
	{
		gtk_entry_set_editable( GTK_ENTRY( widget ), TRUE );
		g_nav_ctl.escaped = TRUE;
	}


	static bool buttonGuard = true;
	void
	on_capture_page_button_pressed ( GtkButton * button,
	                                 gpointer user_data )
	{
		buttonGuard = false;
	}


	void
	on_capture_page_button_released ( GtkButton * button,
	                                  gpointer user_data )
	{
		buttonGuard = true;
	}


	void
	on_capture_page_mute_button_toggled ( GtkToggleButton * togglebutton,
	                                      gpointer user_data )
	{
		if ( ! buttonGuard )
		{
			audioOn = ! audioOn;
			buttonGuard = true;
			gtk_toggle_button_set_active( togglebutton, !audioOn );
		}
	}

	void
	on_capture_page_avc_button_clicked( GtkButton *button,
	                                           gpointer user_data)
	{
		avcStatus = !avcStatus;
		Preferences::getInstance().enableAVC = common->getPageCapture( ) ->avc_enabled = !common->getPageCapture( ) ->avc_enabled;
		std::cerr << ">> AV/C " << ( common->getPageCapture( ) ->avc_enabled ? _( "Enabled" ) : _( "Disabled" ) ) << std::endl;
		TriggerAction( );
	}
}

/*
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef PREFERENCES_H
#define PREFERENCES_H 1

enum { AVI, PLAYLIST, RAW_DV, QT, UNKNOWN_FORMAT };
enum { PAL_FORMAT, NTSC_FORMAT, AVI_DV1_FORMAT, AVI_DV2_FORMAT, QT_FORMAT, RAW_FORMAT, TEST_FORMAT, UNDEFINED };
enum { DISPLAY_XX, DISPLAY_GDKRGB, DISPLAY_GDKRGB32, DISPLAY_XV, DISPLAY_SDL };

enum { NORM_UNSPECIFIED = 0, NORM_PAL = 1, NORM_NTSC = 2 };
enum { AUDIO_32KHZ = 0, AUDIO_44KHZ = 1, AUDIO_48KHZ = 2 };
enum { ASPECT_43 = 0, ASPECT_169 = 1 };

/* For jogShuttle selections, some include an a couple of classes */
#include <string>
#include <map>
#include <utility>
#include <vector>
using std::string;
using std::map;
using std::pair;
using std::make_pair;
using std::vector;

#include <glib.h>

class JogShuttleActions
{
public:
	int _option_index;  /* Index into the optionmenu */
	string _short_desc; /* Used for the option menu */
	string _desc;       /* Potentially used for bubblehelp/something */
	string _action;     /* Actual stuff to send to kino command handler */
	JogShuttleActions( int option_index, string short_desc,
	                   string desc, string action ) :
			_option_index( option_index ), _short_desc( short_desc ),
			_desc( desc ), _action( action )
	{}
}
;

class JogShuttleMapping
{
public:
	string _action; /* We only use a single string for now */
	JogShuttleMapping( string action )
			: _action( action )
	{}
	JogShuttleMapping() : _action( "" )
	{}
}
;

/// Load and save user options

class Preferences
{
private:
	static Preferences *instance;
	GKeyFile *config;

	/* Helper functions to initialise JogShuttle bindings. */
	void JogShuttleInit(void);
	void JogShuttleInitNavigation(int& count);
	void JogShuttleInitCut(int& count);
	void JogShuttleInitCopy(int& count);
	void JogShuttleInitPaste(int& count);
	void JogShuttleInitModeSwitching(int& count);
	void JogShuttleInitGeneral(int& count);
	void JogShuttleInitTrim(int& count);
	void RecentFilesInit();
	void RecentFilesSave();

protected:
	Preferences();
	~Preferences();

public:
	static Preferences &getInstance();

	void get( int& var, const char* name, const char* def );
	void get( char *var, int size, const char* name, const char* def );
	void get( bool& var, const char* name, const char* def );
	void set( const char *name, const char* value );
	void set( const char *name, int value );
	void set( const char *name, bool value );
	void addRecentFile( string filename );

	int defaultNormalisation;
	int defaultAudio;
	int defaultAspect;
	char file[ 512 ];
	int fileFormat;
	bool autoSplit;
	bool timeStamp;
	int frames;
	int every;
	int interface;
	int channel;
	int phyID;
	char avcGUID[ 65 ];
	int displayMode;
	int displayQuality;
	bool displayFixed;
	bool enableAudio;
	int cip_n;
	int cip_d;
	int syt_offset;
	bool preview_capture;
	bool dropFrame;
	char audioDevice[ 512 ];

	/* JogShuttle stuff */
	bool enableJogShuttle;
	bool _isGrabbing;
	/* Mappings for the jogShuttle */
	map<pair<unsigned short, unsigned short>, JogShuttleMapping>
	_JogShuttleMappings;
	vector<JogShuttleActions> _JogShuttleActions;


	bool enableV4L;
	bool disableKeyRepeat;
	int audioRendering;
	int maxUndos;
	int dvCaptureBuffers;
	int dvExportBuffers;
	bool dvDecoderClampLuma;
	bool dvDecoderClampChroma;
	int maxFileSize;
	bool audioScrub;
	char v4lVideoDevice[ 512 ];
	char v4lAudioDevice[ 512 ];
	char v4lInput[ 32 ];
	char v4lAudio[ 32 ];
	bool isOpenDML;
	char defaultDirectory[ 512 ];
	int displayExtract;
	bool relativeSave;

	char dvCaptureDevice[ 512 ];
	bool dv1394Preview;
	char dvExportDevice[ 512 ];
	int avcPollIntervalMs;
	int dvExportPrerollSec;
	bool dvTwoPassEncoder;
	int windowWidth;
	int windowHeight;
	int storyboardPosition;
	int previewSize;
	int timeFormat;
	
	/// metaNames is a comma-delimited list of xml attribute names.
	/// Preface name with a '*' to make it required.
	// added for tagesschau.de
	char metaNames[512];
	
	/// metaValues is mapped to each metaName.
	/// Each metaValue is a comma-delimited list of permissable values.
	/// If '*' is a value, then the user can enter a value not in the list.
	/// The list may contain a label=value pair. The 'label' is displayed to
	/// the user while a 'value' is hidden and used in the XML attribute value.
	/// The '=value' part of a pair is optional.
	/// If the first value pair begins with "xpath:" then the metaValues are expanded
	/// using the XML response from newProjectURI. Remember, a XPath query can return
	/// a collection of nodes. The text/value of these nodes is used to build the
	/// replacement list of label/values.
	// added for tagesschau.de
	map< string, vector< pair< string, string > > > metaValues;

	/// enablePublish shows the Publish SMIL and Still menu items and toolbar icons.
	// added for tagesschaue.de
	bool enablePublish;

	bool expandStoryboard;

	/// If newProjectURI is not empty, then a prompt is displayed on startup when no
	/// files on command line or File/New selected. The user is prompted for a
	/// project name/ID which is pasted in the URI where there exists a "%s"
	/// Then, the URI is requested from a server using HTTP. It expects to receive
	/// a well-formed XML document in the body of the response, which is parsed by
	/// libxml2.
	// added for tagesschau.de
	char newProjectURI[512];

	/// newProjectXPath is the XPath expression to locate the project id in the
	/// XML response from newProjectURI. The value is put into the id attribute
	/// value on the smil element.
	// added for tagesschau.de
	char newProjectXPath[512];

	bool trimModeInsert;

	// "Sticky" Export/MJPEG options
	int exportMjpegAspect;
	int exportMjpegDeinterlace;
	int exportMjpegFormat;
	bool exportMjpegSceneSplit;
	bool exportMjpegCleanup;
	int exportMjpegDvdTool;
	char exportMjpegVideoPipe[512];
	char exportMjpegAudioPipe[512];
	char exportMjpegMultiplex[512];

	// Other "sticky" options
	bool enableAVC;

	vector<string> recentFiles;

	void Save();
};

#endif

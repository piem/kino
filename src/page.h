/*
* page.h Notebook Page Base Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_H
#define _PAGE_H

#include <gdk/gdk.h>
#include <gtk/gtk.h>

extern "C"
{
#include "support.h"
}

/** A Page reflects the interaction between the KinoCommon object and an
	individual notebook page. All subsequent Page objects must extend this 
	class (publicly) and rewrite any or all of the virtual methods provided.
*/

class Page
{
public:
	virtual ~Page()
	{ }
	virtual gulong activate()
	{
		return 0;
	}
	virtual gulong deactivate()
	{
		return 0;
	}
	virtual void newFile()
	{ }
	virtual void start()
	{ }
	virtual void clean()
	{ }
	virtual gboolean processKeyboard( GdkEventKey *event )
	{
		return FALSE;
	}
	virtual gboolean processCommand( const char *command )
	{
		return FALSE;
	}
	virtual void selectScene( int i )
	{ }
	virtual void videoStartOfMovie()
	{ }
	virtual void videoPreviousScene()
	{ }
	virtual void videoStartOfScene()
	{ }
	virtual void videoRewind()
	{ }
	virtual void videoBack(int step = -1)
	{ }
	virtual void videoPlay()
	{ }
	virtual void videoForward(int step = 1)
	{ }
	virtual void videoFastForward()
	{ }
	virtual void videoNextScene()
	{ }
	virtual void videoEndOfScene()
	{ }
	virtual void videoEndOfMovie()
	{ }
	virtual void videoPause()
	{ }
	virtual void videoStop()
	{ }
	virtual void videoShuttle( int )
	{ }
	virtual void movedToFrame( int frame )
	{ }
	virtual void windowMoved()
	{ }
	virtual void visibilityChanged( gboolean visible )
	{ }
	virtual void showFrameInfo( int )
	{}
	virtual void saveFrame()
	{}
	virtual void timeFormatChanged()
	{}
	virtual std::string getHelpPage()
	{
		return "index";
	}
}
;

#endif

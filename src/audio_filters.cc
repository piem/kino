/*
* audio_filters.h -- audio filters
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

// Project Includes
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "audio_filters.h"
#include "page_magick.h"
#include "kino_av_pipe.h"
#include "frame.h"
#include "kino_extra.h"
#include "time_map.h"

// C Includes

#include <string.h>
#include <stdio.h>

#define MAX_VECTOR 5000

extern "C"
{
#include "support.h"
	extern GladeXML* magick_glade;
}

/** Class for simple pass through of audio.
*/

class AudioFilterKeep : public AudioFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "No Change" );
	}

	void GetFrame( int16_t **buffer, int frequency, int channels, int& samples, double position, double frame_delta )
	{}
}
;

/** Silences the audio sample.
*/

class AudioFilterSilence : public AudioFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Silence" );
	}

	void GetFrame( int16_t **buffer, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		for ( int i = 0; i < channels; i ++ )
			for ( int j = 0; j < samples; j ++ )
				buffer[ i ][ j ] = 0;
	}
};

/** Fade out.
*/

class AudioFilterFadeOut : public AudioFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Fade Out" );
	}

	void GetFrame( int16_t **buffer, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		for ( int i = 0; i < channels; i ++ )
			for ( int j = 0; j < samples; j ++ )
				buffer[ i ][ j ] = ( int ) ( buffer[ i ][ j ] * ( 1 - position ) );
	}
};

/** Fade In.
*/

class AudioFilterFadeIn : public AudioFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Fade In" );
	}

	void GetFrame( int16_t **buffer, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		for ( int i = 0; i < channels; i ++ )
			for ( int j = 0; j < samples; j ++ )
				buffer[ i ][ j ] = ( int ) ( buffer[ i ][ j ] * position );
	}
};

/** Gain
*/

static void
gtk_curve_reset_vector( GtkCurve *curve, gfloat y = 1.0 )
{
	if ( curve->ctlpoint )
		g_free ( curve->ctlpoint );

	curve->num_ctlpoints = 2;
	curve->ctlpoint = ( gfloat( * ) [ 2 ] ) g_malloc ( 2 * sizeof ( curve->ctlpoint[ 0 ] ) );
	curve->ctlpoint[ 0 ][ 0 ] = curve->min_x;
	curve->ctlpoint[ 0 ][ 1 ] = y;
	curve->ctlpoint[ 1 ][ 0 ] = curve->max_x;
	curve->ctlpoint[ 1 ][ 1 ] = y;

}

class AudioGain : public GDKAudioFilter
{
private:
	GtkWidget* m_window;
	GtkCurve* m_curve;
	gfloat m_vector[ MAX_VECTOR ];

public:
	AudioGain( )
	{
		m_window = glade_xml_get_widget( magick_glade, "window_gain" );
		m_curve = GTK_CURVE( glade_xml_get_widget( magick_glade, "curve_gain" ) );
	}
	char *GetDescription( ) const
	{
		return _( "Gain" );
	}
	void GetFrame( int16_t **buffer, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		gtk_curve_get_vector( m_curve, MAX_VECTOR, m_vector );
		int pos = int( position * MAX_VECTOR + 0.5 );
		double gain = m_vector[ pos ];
		for ( int s = 0; s < samples; s ++ )
			for ( int c = 0; c < channels; c ++ )
				buffer[ c ][ s ] = int16_t( double( buffer[ c ][ s ] * gain + 0.5 ) );
	}
	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( m_window ) ) ->child, GTK_WIDGET( bin ) );
		while ( gtk_events_pending() )
			gtk_main_iteration();
		gtk_curve_reset_vector( m_curve );
		gtk_curve_set_curve_type( m_curve, GTK_CURVE_TYPE_LINEAR );
	}
	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( m_window ) );
	}
};

/** Helper class for transitions with wav inputs associated to them. This class encapsulates
	the KinoAudioInput and resampling requirements.
*/

static void on_entry_file_changed( GtkEditable *editable, gpointer user_data );

class WavSelect
{
private:
	char wav_file[ PATH_MAX + NAME_MAX ];
	KinoAudioInput *wav;
	GtkLabel *label_info;
	GtkEntry *file_entry;
	AudioResample<int16_le_t,int16_ne_t> *resampler;
	int fps;
	int16_t temp[ 4 * DV_AUDIO_MAX_SAMPLES * 2 ];
	bool wav_selected;

public:
	WavSelect() : wav( NULL ), label_info( NULL ), file_entry( NULL ), resampler(0), wav_selected( false )
	{
		strcpy( wav_file, "" );
	}

	~WavSelect()
	{
		delete wav;
		delete resampler;
	}

	void SetWavInfoLabel( GtkLabel *label )
	{
		label_info = label;
	}

	void SetWavFileEntry( GtkEntry *entry )
	{
		file_entry = entry;
		g_signal_connect( G_OBJECT( entry ), "changed", G_CALLBACK( on_entry_file_changed ), this );
	}

	void WavFileSelected( )
	{
		char text[ 10240 ] = "";

		if ( strcmp( wav_file, gtk_entry_get_text( file_entry ) ) )
		{
			delete wav;

			strcpy( wav_file, gtk_entry_get_text( file_entry ) );
			this->wav = KinoAudioInputFactory::CreateAudioInput( wav_file );

			if ( wav != NULL )
			{
				wav_selected = true;
				if ( wav->GetNumberOfSamples() > 0 )
				{
					sprintf( text, _( "Duration: %.02f seconds" ),
							( double ) wav->GetNumberOfSamples() / ( double ) wav->GetFrequency() );
				}
				else
				{
					sprintf( text, _( "Duration: N/A" ) );
				}
			}
			else
			{
				wav_selected = false;
				strcpy( text, _( "Invalid file selected" ) );
			}
		}
		else if ( !strcmp( wav_file, "" ) )
		{
			wav_selected = false;
			strcpy( text, _( "No file selected" ) );
		}

		if ( strcmp( text, "" ) )
			gtk_label_set_text( label_info, text );
	}

	void WavStart( int frequency, int samples, off_t offset )
	{
		if ( wav_selected )
		{
			if ( wav->GetFrequency() != frequency )
			{
				delete resampler;
				resampler = AudioResampleFactory<int16_le_t,int16_ne_t>::createAudioResample(
					AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY );
				resampler->SetOutputFrequency( frequency );
			}
			else
			{
				delete resampler;
				resampler = 0;
			}
			fps = frequency / samples;
			if ( ! wav->Seek( offset ) )
			{
				int16_t data[2];
				wav->Open( wav_file );
				for ( off_t i = 0; i < offset; i += sizeof(data) )
				{
					if ( ! wav->Get( data, sizeof(data) ) )
						break;
				}
			}
		}
	}

	int WavRead( int16_t **a, int f, int c, int bytes, double gain = 1.0 )
	{
		if ( wav_selected )
		{
			if ( resampler )
			{
				memset( temp, 0, sizeof( temp ) );
				int samples = bytes / c / 2 * wav->GetFrequency() / f;
				bytes = samples * c * 2;
				if ( !wav->Get( temp, bytes ) )
					std::cerr << ">>> Underrun? unable to read " << bytes << " bytes" << std::endl;
	
				resampler->Resample( reinterpret_cast<int16_le_t *>(temp), wav->GetFrequency(), c, samples );
	
				int16_t *p = resampler->output;
				for ( int s = 0; s < resampler->size / c / 2; s ++ )
					for ( int i = 0; i < c; i ++ )
						a[ i ][ s ] = int16_t( double( *p++ ) * gain + 0.5 );
				return (resampler->size / c / 2);
			}
			else
			{
				memset( temp, 0, sizeof( temp ) );
				if ( !wav->Get( temp, bytes ) )
					std::cerr << ">>> Underrun? unable to read " << bytes << " bytes" << std::endl;
				int16_t *p = temp;
				for ( int s = 0; s < bytes / c / 2; s ++ )
					for ( int i = 0; i < c; i ++ )
						a[ i ][ s ] = int16_t( double( *p++ ) * gain + 0.5 );
				return (bytes / c / 2);
			}
		}
		return 0;
	}

	bool IsSelected()
	{
		return wav_selected;
	}

	int GetAdjustedSamples( int frequency, int samples ) const
	{
		return samples * wav->GetFrequency() / frequency;
	}
	
	int WavGetFrequency()
	{
		return wav->GetFrequency();
	}
};

/** GTK callback on file selection change.
*/

static void on_entry_file_changed( GtkEditable *editable, gpointer user_data )
{
	( ( WavSelect * ) user_data ) ->WavFileSelected();
}

/** Dub the video with the contents of a wav.
 
	NB: To avoid crashes during a preview which starts without audio and then has it switched on
	midflow, the initiated flag is used to determine that audio is initialised correctly at the
	start of the effect... This has the unfortunate effect of making audio require a restart in
	the preview before it is actually played correctly...
*/

class AudioDub : public GDKAudioFilter, WavSelect
{
private:
	GtkWidget *window;
	GtkCurve* curve;
	double offsetSecs;
	bool initiated;
	gfloat m_vector[ MAX_VECTOR ];

public:
	AudioDub( ) : initiated( false )
	{
		window = glade_xml_get_widget( magick_glade, "window_dub" );
		curve = GTK_CURVE( glade_xml_get_widget( magick_glade, "curve_dub" ) );
		g_signal_connect( G_OBJECT( lookup_widget( window, "button_dub_file" ) ),
			"clicked", G_CALLBACK( on_button_sub_file_clicked ), this );
		SetWavInfoLabel( GTK_LABEL( lookup_widget( window, "label_dub_info" ) ) );
		SetWavFileEntry( GTK_ENTRY( lookup_widget( window, "entry_dub_file" ) ) );
		WavFileSelected();
	}

	virtual ~AudioDub( )
	{}

	char *GetDescription( ) const
	{
		return _( "Dub" );
	}

	bool IsAFrameConsumer() const
	{
		return false;
	}

	void GetFrame( int16_t **aframe, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		if ( IsSelected() )
		{
			gtk_curve_get_vector( curve, MAX_VECTOR, m_vector );
			if ( !initiated )
			{
				initiated = true;
				// nasty approximation (sigh)
				off_t byte_offset = ( int( offsetSecs * WavGetFrequency() )
					+ ( GetSelectedFramesForFX().GetIndex( position ) * GetAdjustedSamples( frequency, samples ) ) )
					* channels * 2;
				WavStart( frequency, samples, byte_offset );
			}
			int pos = int( position * MAX_VECTOR + 0.5 );
			samples = WavRead( aframe, frequency, channels, samples * channels * 2, m_vector[ pos ] );
		}
	}

	static void
	on_button_sub_file_clicked( GtkButton *button, gpointer user_data)
	{
		char *filename = common->getFileToOpen( _("Choose an audio file"), false );
		if ( filename && strcmp( filename, "" ) )
		{
			AudioDub *me = static_cast< AudioDub* >( user_data );
			gtk_entry_set_text( me->getEntry(), filename );
		}
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_curve_reset_vector( curve );
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
		while ( gtk_events_pending() )
			gtk_main_iteration();
		gtk_curve_set_curve_type( curve, GTK_CURVE_TYPE_LINEAR );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}

	void InterpretWidgets( GtkBin *bin )
	{
		GtkWidget *offsetSpin = lookup_widget( window, "spinbutton_dub_offset" );
		offsetSecs = gtk_spin_button_get_value( GTK_SPIN_BUTTON( offsetSpin ) );
		initiated = false;
	}

	GtkEntry *getEntry() const
	{
		return GTK_ENTRY( lookup_widget( window, "entry_dub_file" ) );
	}
};

/** Mix the audio with a wav file.
 
	NB: To avoid crashes during a preview which starts without audio and then has it switched on
	midflow, the initiated flag is used to determine that audio is initialised correctly at the
	start of the effect... This has the unfortunate effect of making audio require a restart in
	the preview before it is actually played correctly...
*/

class AudioMix : public GDKAudioFilter, WavSelect
{
private:
	GtkWidget *window;
	GtkCurve* curve;
	bool initiated;
	double offsetSecs;
	int16_t** bframe;
	gfloat m_vector[ MAX_VECTOR ];

public:
	AudioMix( ) : initiated( false )
	{
		window = glade_xml_get_widget( magick_glade, "window_mix" );
		curve = GTK_CURVE( glade_xml_get_widget( magick_glade, "curve_mix" ) );
		g_signal_connect( G_OBJECT( lookup_widget( window, "button_mix_file" ) ),
			"clicked", G_CALLBACK( on_button_mix_file_clicked ), this );
		SetWavInfoLabel( GTK_LABEL( lookup_widget( window, "label_mix_info" ) ) );
		SetWavFileEntry( GTK_ENTRY( lookup_widget( window, "entry_mix_file" ) ) );
		WavFileSelected();
		bframe = new int16_t*[4];
		for ( int i = 0; i < 4; i++ )
			bframe[i] = new int16_t[ DV_AUDIO_MAX_SAMPLES * 2 ];
	}

	virtual ~AudioMix( )
	{
		for ( int i = 0; i < 4; i++ )
			delete[] bframe[i];
		delete[] bframe;
	}

	char *GetDescription( ) const
	{
		return _( "Mix" );
	}

	void Mix( int16_t **io, int16_t **with, int channels, int samples, double mix )
	{
		for ( int s = 0; s < samples; s ++ )
			for ( int c = 0; c < channels; c ++ )
				io[ c ][ s ] = ( int16_t ) ( ( double ) io[ c ][ s ] * ( 1.0 - mix ) + ( double ) with[ c ][ s ] * mix );
	}

	void GetFrame( int16_t **aframe, int frequency, int channels, int& samples, double position, double frame_delta )
	{
		if ( IsSelected() )
		{
			gtk_curve_get_vector( curve, MAX_VECTOR, m_vector );
			if ( !initiated )
			{
				initiated = true;
				// nasty approximation (sigh)
				off_t byte_offset = ( int( offsetSecs * WavGetFrequency() )
					+ ( GetSelectedFramesForFX().GetIndex( position ) * GetAdjustedSamples( frequency, samples ) ) )
					* channels * 2;
				WavStart( frequency, samples, byte_offset );
			}
			samples = WavRead( bframe, frequency, channels, samples * channels * 2 );
			int pos = int( position * MAX_VECTOR + 0.5 );
			Mix( aframe, bframe, channels, samples, m_vector[ pos ] );
		}
	}

	static void
	on_button_mix_file_clicked( GtkButton *button, gpointer user_data)
	{
		char *filename = common->getFileToOpen( _("Choose an audio file"), false );
		if ( filename && strcmp( filename, "" ) )
		{
			AudioMix *me = static_cast< AudioMix* >( user_data );
			gtk_entry_set_text( me->getEntry(), filename );
		}
	}
	
	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
		while ( gtk_events_pending() )
			gtk_main_iteration();
		gtk_curve_reset_vector( curve, 0.5 );
		gtk_curve_set_curve_type( curve, GTK_CURVE_TYPE_LINEAR );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}

	void InterpretWidgets( GtkBin *bin )
	{
		GtkWidget *offsetSpin = lookup_widget( window, "spinbutton_mix_offset" );
		offsetSecs = gtk_spin_button_get_value( GTK_SPIN_BUTTON( offsetSpin ) );
		initiated = false;
	}

	GtkEntry *getEntry() const
	{
		return GTK_ENTRY( lookup_widget( window, "entry_mix_file" ) );
	}
};

/** Callback for selection change.
*/

static void
on_optionmenu_selected ( GtkMenuItem *menu_item, gpointer user_data )
{
	( ( GDKAudioFilterRepository * ) user_data ) ->SelectionChange();
}

/** Constructor for the audio filter repository.
 
  	Registers an instance of each known filter for later GUI exposure via the Initialise method.
*/

GDKAudioFilterRepository::GDKAudioFilterRepository() : selected_filter( NULL ), menu( NULL ), container( NULL )
{
	// Register an instance of each object (adapting raw filters to GDK filters where necessary)
	Register( new GDKAudioFilterAdapter( new AudioFilterKeep() ) );
	Register( new AudioDub() );
	Register( new GDKAudioFilterAdapter( new AudioFilterFadeIn() ) );
	Register( new GDKAudioFilterAdapter( new AudioFilterFadeOut() ) );
	Register( new AudioGain() );
	Register( new AudioMix() );
	Register( new GDKAudioFilterAdapter( new AudioFilterSilence() ) );
}

/** Destructor for the image repository - clears all the registered filters
*/

GDKAudioFilterRepository::~GDKAudioFilterRepository()
{
	// Remove the filters in the repository
	for ( unsigned int index = 0; index < filters.size(); index ++ )
		delete filters[ index ];
}

/** Register an audio filter.
*/

void GDKAudioFilterRepository::Register( GDKAudioFilter *filter )
{
	std::cerr <<  ">>> Audio Filter: " << filter->GetDescription() << std::endl;
	filters.push_back( filter );
}

/** Initialise the option menu with the current list of registered filters.
*/

void GDKAudioFilterRepository::Initialise( GtkOptionMenu *menu, GtkBin *container )
{
	// Store these for future reference
	this->menu = menu;
	this->container = container;

	// Add the filters to the menu
	GtkMenu *menu_new = GTK_MENU( gtk_menu_new( ) );
	for ( unsigned int index = 0; index < filters.size(); index ++ )
	{
		GtkWidget *item = gtk_menu_item_new_with_label( filters[ index ] ->GetDescription( ) );
		gtk_widget_show( item );
		gtk_menu_append( menu_new, item );
		g_signal_connect( G_OBJECT( item ), "activate", G_CALLBACK( on_optionmenu_selected ), this );
	}
	gtk_menu_set_active( menu_new, 0 );
	gtk_option_menu_set_menu( menu, GTK_WIDGET( menu_new ) );

	// Register the selected items widgets
	SelectionChange();
}

/** Get the currently selected audio filter.
*/

GDKAudioFilter *GDKAudioFilterRepository::Get( ) const
{
	GtkMenu * filterMenu = GTK_MENU( gtk_option_menu_get_menu( menu ) );
	GtkWidget *active_item = gtk_menu_get_active( filterMenu );
	return filters[ g_list_index( GTK_MENU_SHELL( filterMenu ) ->children, active_item ) ];
}

/** Handle attach/detach widgets on last selected/selected items.
*/

void GDKAudioFilterRepository::SelectionChange( )
{
	bool isPreviewing = false;
	PageMagick* magick = 0;

	if ( common && common->getPageMagick( ) )
		magick = common->getPageMagick( );
	if ( magick && magick->IsPreviewing() )
	{
		isPreviewing = true;
		magick->StopPreview();
	}

	// Detach the selected filters widgets
	if ( selected_filter != NULL )
		selected_filter->DetachWidgets( container );

	// Get the new selection
	selected_filter = Get();

	// Attach the new filters widgets
	if ( selected_filter != NULL )
		selected_filter->AttachWidgets( container );

	if ( magick )
	{
		if ( isPreviewing )
			magick->StartPreview();
		else
			magick->PreviewFrame();
	}
}

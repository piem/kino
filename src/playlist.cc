/*
* playlist.cc -- SMIL implementation
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

// C++ includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <list>
#include <map>
#include <algorithm>

using std::cerr;
using std::endl;
using std::ends;
using std::ofstream;
using std::ostringstream;
using std::list;
using std::map;

// C includes
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <glib/gi18n.h>

// local includes
#include "playlist.h"
#include "error.h"
#include "filehandler.h"
#include "frame.h"
#include "stringutils.h"
#include "message.h"

const xmlChar* SMIL20_NAMESPACE_HREF = reinterpret_cast< const xmlChar* >( "http://www.w3.org/2001/SMIL20/Language" );
const string KINO_AUTOSAVE_DIR = string( getenv("HOME") ) + string( "/.kino-history" );

/** The file map implementation - this class is responsible for maintaining the list of
	open files associated to a project.
*/

class KinoFileMap : public FileMap
{
private:
	map < string, FileHandler* > filemap;

public:
	virtual ~KinoFileMap()
	{}

	map<string, FileHandler*> &GetMap()
	{
		return filemap;
	}

	void Clear( )
	{
		map<string, FileHandler*>::iterator n;
		for ( n = filemap.begin(); n != filemap.end(); ++n )
			delete ( *n ).second;
		filemap.erase( filemap.begin(), filemap.end() );
	}

	void GetUnusedFxFiles( PlayList &list, vector< string > &unused )
	{
		unused.erase( unused.begin(), unused.end() );
		map<string, FileHandler*>::iterator n;
		for ( n = filemap.begin(); n != filemap.end(); ++n )
		{
			if ( n->first.find( ".kinofx.dv" ) != string::npos && !list.IsFileUsed( n->first ) )
			{
				unused.push_back( n->first );
			}
		}
	}
};

/** Singleton file map object.
*/

FileMap *GetFileMap( )
{
	static FileMap * thismap = new KinoFileMap( );
	return thismap;
}

/** Join the file to the directory.
 
    It is assumed that if the input file is not an absolute path (ie: starting with a /)
	then the full path is relative to the current working directory. So 'file' would return
	/'cwd' and 'relative/file' would be /'cwd + relative'. 
 
	Note that .. directory specs in the file input are normalised to the full path minus the
	.. and erroneously formed paths (containing double slashes for example) are also 
	corrected in the output.
*/

string directory_utils::join_file_to_directory( const string directory, const string &file )
{
	vector <string> items;

	// Determine if the file is a full file spec or not
	if ( file[ 0 ] != '/' && directory[ 0 ] != '/' )
	{
		char path[ PATH_MAX ];
		getcwd( path, PATH_MAX );
		StringUtils::split( path, "/", items );
	}

	// Now add the directory if file is not absolute
	if ( file[ 0 ] != '/' )
		StringUtils::split( directory, "/", items );

	// Split the file and append to the directory info
	StringUtils::split( file, "/", items );

	// iterate through the items vector
	for ( vector< string >::iterator item = items.begin( ); item != items.end( ); )
	{
		if ( *item == ".." )
		{
			if ( item == items.begin( ) )
			{
				items.erase( item );
				item = items.begin();
			}
			else
			{
				items.erase( -- item + 1 );
				items.erase( -- item + 1 );
				item ++;
			}
		}
		else
		{
			item ++;
		}
	}

	return "/" + StringUtils::join( items, "/" );
}

/** Obtain the directory from the file.
 
	A value of 'file' that actually corresponds to a directory will result in the parent 
	directory being returned ie: /'parent'/'directory' returns /'parent' which is 
	probably not what you want...
*/

string directory_utils::get_directory_from_file( const string &file )
{
	return join_file_to_directory( "", file + "/.." );
}

/** Get absolute path to file.
 
    If file has an absolute path, then you will get a cleaned up version of that path and file
	returned (ie: /'directory'/'sub'/..///'file' becomes /'directory'/'file').
 
	If file is relative (ie: not starting with a /) and directory is absolute (ie: starting with
	a /) then you will receive the appended output.
 
	If neither is absolute, then cwd is pre-pended to directory and file is appended and a 
	cleaned up file spec is returned.
*/

string directory_utils::get_absolute_path_to_file( const string &directory, const string &file )
{
	return join_file_to_directory( directory, file );
}

/** Get relative path to file.
 
    Given a directory of /same-path/different-path and file of /same-path/blah, the output should be
	../blah.
*/

string directory_utils::get_relative_path_to_file( const string &directory, const string &file )
{
	string output = "";
	string absolute = join_file_to_directory( directory, file );
	vector < string > directory_items;
	vector < string > absolute_items;
	StringUtils::split( absolute, "/", absolute_items );
	StringUtils::split( directory, "/", directory_items );

	vector < string >::iterator directory_item;
	vector < string >::iterator absolute_item;

	// While they're both the same, remove from both
	for ( directory_item = directory_items.begin(), absolute_item = absolute_items.begin();
	        directory_item != directory_items.end() && absolute_item != absolute_items.end() && *directory_item == *absolute_item; )
	{
		directory_items.erase( directory_item );
		absolute_items.erase( absolute_item );
		directory_item = directory_items.begin();
		absolute_item = absolute_items.begin();
	}

	// For each item left in the directory_items, output a ../
	for ( directory_item = directory_items.begin(); directory_item != directory_items.end() ; directory_item ++ )
		output += "../";

	// Now simply join what's left in absolute to output and return
	output += StringUtils::join( absolute_items, "/" );
	return output;
}

/** Expand directories with ~ as the first item.
*/

string directory_utils::expand_directory( const string directory )
{
	string output;
	vector <string> items;
	StringUtils::split( directory, "/", items );
	vector< string >::iterator item = items.begin( );

	if ( item != items.end( ) && *item == "~" )
	{
		output = getenv( "HOME" );
		item ++;
	}

	for ( ; item != items.end( ); item ++ )
		output += "/" + *item;

	return output;
}

typedef bool ( *callback ) ( xmlNodePtr node, void *p, bool *freed );

typedef struct
{
	int	absFrame;
	int	absBegin;
	int	absEnd;
	int	clipFrame;
	int	clipBegin;
	int	clipEnd;
	int clipNumber;
	int clipLength;
	char	fileName[ 1024 ];
	xmlNodePtr	sequence;
	xmlNodePtr	video;
}
MovieInfo;

/** Finds the file corresponding to an absolute frame number.
 
    The absolute frame number is passed in MovieInfo.absFrame.
    If found, the MovieInfo struct is filled:
 
	absFrame:   some absolute frame number
	absBegin:   absolute frame number of movie start
	absEnd:     absolute frame number of movie end
	clipFrame:  corresponding relative frame number
	clipBegin:  corresponding relative frame number
	clipEnd:    corresponding relative frame number
	clipNumber: 
	clipLength: frame count in this clip
	fileName:   its file name
	sequence:   node ptr to its <seq> node
	video:      node ptr to its <video> node
 
    \param node a node
    \param p pointer to MovieInfo struct
    \result true if file has been found and xml tree walk is done */

static bool findFile( xmlNodePtr node, void *p, bool *freed )
{
	MovieInfo * data = ( MovieInfo* ) p;

	if ( xmlStrcmp( node->name, ( const xmlChar* ) "seq" ) == 0 )
	{
		data->sequence = node;
		data->clipNumber++;
	}

	// if this is a <video> node, calculate its absolute begin and end positions

	else if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{

		data->video = node;

		// Check whether the required properties exist

		xmlChar *src = xmlGetProp( node, ( const xmlChar* ) "src" );
		xmlChar *clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		xmlChar *clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

		if ( ( src != NULL ) && ( clipBegin != NULL ) && ( clipEnd != NULL ) )
		{

			data->clipBegin = atoi( ( const char* ) clipBegin );
			data->clipEnd = atoi( ( const char* ) clipEnd );

			data->absBegin += data->clipLength; // add length of previous clip
			data->clipLength = data->clipEnd - data->clipBegin + 1;
			data->absEnd = data->absBegin + data->clipLength - 1;

			// cerr << "Number: " << data->clipNumber << " starts " << data->absBegin << " ends " << data->absEnd << endl;

			// if absFrame is within this scene, we have found the corresponding file.
			// Otherwise, add frame count of this scene to absBegin

			if ( data->absFrame <= data->absEnd )
			{
				strcpy( data->fileName, ( char * ) src );
				data->clipFrame = data->absFrame - data->absBegin + data->clipBegin;

				// Free memory used
				xmlFree( src );
				xmlFree( clipEnd );
				xmlFree( clipBegin );

				// cerr << "Obtaining frame " << data->clipFrame << " from " << data->clipNumber << endl;

				return true; // true means done traversing xml tree
			}
		}

		if ( src )
			xmlFree( src );
		if ( clipEnd )
			xmlFree( clipEnd );
		if ( clipBegin )
			xmlFree( clipBegin );

	}
	return false;
}


/** Searches tree for file names not yet in our filemap
 
    \param node a node
    \param p pointer to a string containing the project directory
    \result true if file has been found and xml tree walk is done */

static bool fillMap( xmlNodePtr node, void *p, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{

		// Check whether the required properties exist

		xmlChar * src = xmlGetProp( node, ( const xmlChar* ) "src" );
		xmlChar *clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		xmlChar *clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

		if ( ( src != NULL ) && ( clipBegin != NULL ) && ( clipEnd != NULL ) )
		{

			// Determine the absolute path to the file indicated by src
			string & directory = *( string * ) p;
			string index = directory_utils::get_absolute_path_to_file( directory, ( char * ) src );

			// Internally, we always use absolute paths so convert now, just to be on the safe side
			xmlSetProp( node, ( const xmlChar* ) "src", ( xmlChar * ) index.c_str() );

			// Check if the file actually exists in the file map
			if ( GetFileMap() ->GetMap().find( index ) == GetFileMap() ->GetMap().end() )
			{

				FileHandler * mediaFile;
				/* determine file type */
				if ( strncasecmp( strrchr( ( char* ) src, '.' ), ".avi", 4 ) == 0 )
					mediaFile = new AVIHandler();
				else if ( strncasecmp( strrchr( ( char* ) src, '.' ), ".dv", 3 ) == 0 ||
				          strncasecmp( strrchr( ( char* ) src, '.' ), ".dif", 4 ) == 0 )
					mediaFile = new RawHandler();
#ifdef HAVE_LIBQUICKTIME

				else if ( strncasecmp( strrchr( ( char* ) src, '.' ), ".mov", 4 ) == 0 )
					mediaFile = new QtHandler();
#endif

				else
				{
					xmlFree( src );
					xmlFree( clipEnd );
					xmlFree( clipBegin );
					return false;
				}
				/* construct appropriate filehandler */
				if ( mediaFile->Open( index.c_str() ) )
				{
					GetFileMap() ->GetMap() [ index ] = mediaFile;
				}
				else
				{
					cerr << "Unable to open " << ( char * ) src
					<< " - removing from list" << endl;
					xmlUnlinkNode( node );
					xmlFreeNode( node );
					*freed = true;
				}
			}
		}

		xmlFree( src );
		xmlFree( clipEnd );
		xmlFree( clipBegin );
	}
	return false;
}


/** Finds the start of a scene.
 
    \param node a node
    \param p pointer to some private data
    \result true if file has been found and xml tree walk is done 
 
    If the scene has been found in the playlist, the file name 
    and first frame number are returned in the private data. */

static bool findSceneStart( xmlNodePtr node, void *p, bool *freed )
{
	int fileCount = 0;
	MovieInfo *data = ( MovieInfo* ) p;
	int begin = data->absBegin;

	// if this is a <seq> node process all of its <video> child nodes

	if ( xmlStrcmp( node->name, ( const xmlChar* ) "seq" ) == 0 )
	{

		data->sequence = node;

		node = node->children;
		while ( node != NULL )
		{
			if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
			{

				data->video = node;

				xmlChar *src = xmlGetProp( node, ( const xmlChar* ) "src" );
				xmlChar *clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
				xmlChar *clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

				if ( ( src != NULL ) && ( clipBegin != NULL ) && ( clipEnd != NULL ) )
				{

					data->clipBegin = atoi( ( const char* ) clipBegin );
					data->clipEnd = atoi( ( const char* ) clipEnd );

					// if this is the first file remember its name and start

					if ( fileCount == 0 )
					{
						data->clipFrame = data->clipBegin;
						strcpy( data->fileName, ( char * ) src );
					}

					// if absFrame is within current scene we are done.
					// fine name and relative frame number have been already found (see above)
					// otherwise update absBegin to hold abs frame num of next file

					if ( data->absFrame <= begin + data->clipEnd - data->clipBegin )
					{
						xmlFree( clipBegin );
						xmlFree( clipEnd );
						xmlFree( src );
						return true;
					}
					else
					{
						begin += ( data->clipEnd - data->clipBegin + 1 );
					}
					fileCount++;
				}
				if ( src )
					xmlFree( src );
				if ( clipEnd )
					xmlFree( clipEnd );
				if ( clipBegin )
					xmlFree( clipBegin );
			}
			node = node->next;
		}
	}
	data->absBegin = begin;
	data->clipFrame = 0;
	strcpy( data->fileName, "" );
	return false;
}


static bool findSceneEnd( xmlNodePtr node, void *p, bool *freed )
{
	bool found = false;
	xmlChar *src = NULL;
	MovieInfo *data = ( MovieInfo* ) p;

	// if this is a <seq> node process all of its <video> child nodes

	if ( xmlStrcmp( node->name, ( const xmlChar* ) "seq" ) == 0 )
	{

		data->sequence = node;

		node = node->children;
		while ( node != NULL )
		{
			if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
			{

				data->video = node;

				if ( src )
					xmlFree( src );

				src = xmlGetProp( node, ( const xmlChar* ) "src" );
				xmlChar *clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
				xmlChar *clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

				if ( ( src != NULL ) && ( clipBegin != NULL ) && ( clipEnd != NULL ) )
				{

					data->clipBegin = atoi( ( const char* ) clipBegin );
					data->clipEnd = atoi( ( const char* ) clipEnd );
					data->clipFrame = data->clipEnd;

					if ( data->absFrame <= data->absBegin + data->clipEnd - data->clipBegin )
						found = true;
					data->absBegin += ( data->clipEnd - data->clipBegin + 1 );
				}

				if ( clipEnd )
					xmlFree( clipEnd );
				if ( clipBegin )
					xmlFree( clipBegin );
			}
			node = node->next;
		}

		if ( found )
		{
			strcpy( data->fileName, ( char * ) src );
			xmlFree( src );
			data->absEnd = data->absBegin - 1;
			return true;
		}

		if ( src )
			xmlFree( src );
	}
	data->clipFrame = 0;
	strcpy( data->fileName, "" );
	return false;
}

/** Count number of frames in one node
 
    \param node a node
    \param p pointer to some private data
 
    If this is a <video> node then calculate the number of frames
    in this clip. p is a ptr to an integer holding the total
    number of frames so far.
 
    The node must have this format:
    <video src="file.avi" clipBegin="200" clipEnd="800" \>
 
    \note This code requires a <video> node format that is not defined in smil. */

static bool countFrames( xmlNodePtr node, void *p, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{

		xmlChar * src = xmlGetProp( node, ( const xmlChar* ) "src" );
		xmlChar *clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		xmlChar *clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

		if ( ( src != NULL ) && ( clipBegin != NULL ) && ( clipEnd != NULL ) )
			* ( ( int* ) p ) += atoi( ( const char* ) clipEnd ) - atoi( ( const char* ) clipBegin ) + 1;

		if ( clipEnd )
			xmlFree( clipEnd );
		if ( clipBegin )
			xmlFree( clipBegin );
		if ( src )
			xmlFree( src );
	}
	return false;
}

/** A type needed for ELI format saving */
typedef struct
{
	string file;   /* Contains filename */
	string clipBegin; /* Contains begin counts */
	string clipEnd;   /* Continas end count */
}
EliInfo;

typedef list<EliInfo> EliInfos;
typedef EliInfos::iterator EliInfosIterator;

/** Convert the xml format to ELI format
    
    \param node a node
    \param p pointer to an EliInfo struct
  
    If this is a <video> node, then add the info to p. p is a ptr to an
    EliInfos struct.
 
    The node must have this format:
    <video src="file.avi" clipBegin="200" clipEnd="800" \>
 
    \note This code requires a <video> node format that is not defined in smil. */
static bool convertEli( xmlNodePtr node, void *p, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{

		xmlChar * src = xmlGetProp( node, ( const xmlChar* ) "src" );
		xmlChar *clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		xmlChar *clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

		if ( ( src != NULL ) && ( clipBegin != NULL ) && ( clipEnd != NULL ) )
		{
			/*
			cout << "convertEli : " << src << ", " << clipBegin << ", " << clipEnd
			<< endl; 
			*/
			EliInfos * Eli = ( EliInfos * ) p;
			EliInfo tmp;
			tmp.file = ( const char* ) src;
			tmp.clipBegin = ( const char* ) clipBegin;
			tmp.clipEnd = ( const char* ) clipEnd;
			Eli->push_back( tmp );
		}

		if ( clipEnd )
			xmlFree( clipEnd );
		if ( clipBegin )
			xmlFree( clipBegin );
		if ( src )
			xmlFree( src );
	}
	return false;
}

class SrtContext
{
public:
	ofstream file;
	unsigned counter;
	xmlChar* title;
	xmlChar* abstract;
	xmlChar* src;
	unsigned begin;
	unsigned duration;

	SrtContext( const char* filename ) :
		file(filename), counter(0), title(NULL), abstract(NULL), src(NULL),
		begin(0), duration(0)
	{
	}

	~SrtContext()
	{
		printEntry();
		file.close();
	}

	void printEntry(void)
	{
		if ( title || abstract )
		{
			Frame *frame = GetFramePool( )->GetFrame( );
			FileHandler *mediaFile = GetFileMap()->GetMap() [ string( ( const char* ) src ) ];
			SMIL::MediaClippingTime time;

			mediaFile->GetFrame( *frame, 0 );
			time.setFramerate( frame->GetFrameRate() );
			GetFramePool( )->DoneWithFrame( frame );
			string beginString = time.parseFramesToString( begin, SMIL::Time::TIME_FORMAT_CLOCK );
			string durationString = time.parseFramesToString( duration - begin, SMIL::Time::TIME_FORMAT_CLOCK );
			begin = 0;

			file << ++counter << endl;
			file << StringUtils::replaceAll( beginString, ".", "," );
			file << " --> " << StringUtils::replaceAll( durationString, ".", "," ) << endl;
			if ( title )
				file << title << endl;
			if ( abstract )
				file << abstract << endl;
			file << endl;
		}
		if ( src )
		{
			xmlFree( src );
			src = NULL;
		}
		if ( title )
		{
			xmlFree( title );
			title = NULL;
		}
		if ( abstract )
		{
			xmlFree( abstract );
			abstract = NULL;
		}
	}
};

static bool convertSrt( xmlNodePtr node, void *data, bool *freed )
{
	SrtContext* srt = static_cast< SrtContext* >( data );
	
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "seq" ) == 0 )
	{
		srt->printEntry();
		srt->title = xmlGetProp( node, ( const xmlChar * ) "title" );
		srt->abstract = xmlGetProp( node, ( const xmlChar * ) "abstract" );
	}
	else if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{
		xmlChar* clipBegin = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		xmlChar* clipEnd = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );

		srt->src = xmlGetProp( node, ( const xmlChar* ) "src" );
		if ( !srt->begin && ( srt->title || srt->abstract ) )
			srt->begin = srt->duration;
		srt->duration += atoi( ( const char* ) clipEnd ) - atoi( ( const char* ) clipBegin ) + 1;

		xmlFree( clipBegin );
		xmlFree( clipEnd );
	}

	return false;
}

static bool convertFramesToSmilTime( xmlNodePtr node, void *data, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{
		Frame *frame = GetFramePool( )->GetFrame( );
		xmlChar *src = xmlGetProp( node, ( const xmlChar* ) "src" );
		string index( ( char* ) src );
		xmlFree( src );
		FileHandler *mediaFile = GetFileMap()->GetMap() [ index ];
		mediaFile->GetFrame( *frame, 0 );
		SMIL::MediaClippingTime time;
		time.setFramerate( frame->GetFrameRate() );
		GetFramePool( )->DoneWithFrame( frame );

		xmlChar *prop = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		if ( prop )
		{
			std::string newValue = time.parseFramesToString( atoi( ( const char* ) prop ), SMIL::Time::TIME_FORMAT_CLOCK );
			xmlFree( prop );
			xmlSetProp( node, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) newValue.c_str() );
		}
		prop = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );
		if ( prop )
		{
			std::string newValue = time.parseFramesToString( atoi( ( const char* ) prop ), SMIL::Time::TIME_FORMAT_CLOCK );
			xmlFree( prop );
			xmlSetProp( node, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) newValue.c_str() );
		}
	}
	return false;
}

static bool convertSmilTimeToFrames( xmlNodePtr node, void *data, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{
		Frame *frame = GetFramePool( )->GetFrame( );
		xmlChar *src = xmlGetProp( node, ( const xmlChar* ) "src" );
		string index( ( char* ) src );
		xmlFree( src );
		FileHandler *mediaFile = GetFileMap()->GetMap() [ index ];
		mediaFile->GetFrame( *frame, 0 );
		SMIL::MediaClippingTime time;
		time.setFramerate( frame->GetFrameRate() );
		GetFramePool( )->DoneWithFrame( frame );

		xmlChar *prop = xmlGetProp( node, ( const xmlChar* ) "clipBegin" );
		if ( prop )
		{
			time.parseValue( ( const char* ) prop );
			xmlFree( prop );
			std::string newValue = time.toString( SMIL::Time::TIME_FORMAT_FRAMES );
			xmlSetProp( node, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) newValue.c_str() );
		}
		prop = xmlGetProp( node, ( const xmlChar* ) "clipEnd" );
		if ( prop )
		{
			time.parseValue( ( const char* ) prop );
			xmlFree( prop );
			std::string newValue = time.toString( SMIL::Time::TIME_FORMAT_FRAMES );
			xmlSetProp( node, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) newValue.c_str() );
		}
	}
	return false;
}

static bool clone( xmlNodePtr node, void *data, bool *freed )
{
	xmlNodePtr* parent = (xmlNodePtr*) data;

	xmlNodePtr child = xmlNewNode( NULL, node->name );
	xmlAddChild( *parent, child );
	for ( xmlAttr* attr = node->properties; attr; attr = attr->next )
		xmlNewProp( child, attr->name, xmlGetProp( attr->parent, attr->name) );
	if ( node->children ) // next iteration in depth-first traversal will be a child
		*parent = child;
	else if ( node == node->parent->last ) // last sibling
		*parent = (*parent)->parent;
	
	return false;
}

/** Walk the xml tree
 
    \param node the root node
    \param func the function to execute on each node
    \param p storage to some private data for func 
 
    This function is usually called with the root node
    of an XML tree. It calls the user supplied callback
    function on each node and then visits recursively
    all child nodes.
 
    If the callback function returns true the xml
    tree walk is aborted. */

static bool parse( xmlNodePtr node, callback func, void *p )
{
	bool done = false;

	while ( node != NULL && done == false )
	{
		bool freed = false;
		xmlNodePtr next = node->next;
		done = ( *func ) ( node, p, &freed );
		if ( !done && !freed && node->children )
			done = parse( node->children, func, p );
		node = next;
	}
	return done;
}


/** Default Constructor */

PlayList::PlayList() : dirty( false ), doc_name( "" ), count( 0 )
{
	xmlNsPtr	ns;
	xmlNodePtr	root;

	// cerr << "*PlayList::PlayList()" << endl;

	doc = xmlNewDoc( ( const xmlChar* ) "1.0" );
	root = xmlNewNode( NULL, ( const xmlChar* ) "smil" );
	ns = xmlNewNs( root, SMIL20_NAMESPACE_HREF, ( const xmlChar* ) NULL );
	xmlDocSetRootElement( doc, root );
	xmlAddChild( root, xmlNewNode( NULL, ( const xmlChar* ) "body" ) );
}


/** Copy Constructor */

PlayList::PlayList( const PlayList& playList )
{
	xmlNsPtr	ns;
	xmlNodePtr	root;

	doc = xmlNewDoc( ( const xmlChar* ) "1.0" );
	root = xmlNewNode( NULL, ( const xmlChar* ) "smil" );
	ns = xmlNewNs( root, ( const xmlChar* ) SMIL20_NAMESPACE_HREF, ( const xmlChar* ) NULL );
	xmlDocSetRootElement( doc, root );
	parse( playList.GetBody(), clone, &root );
	dirty = playList.dirty;
	doc_name = playList.GetDocName( );
	RefreshCount( );
}


/** Assignment Operator */

PlayList& PlayList::operator=( const PlayList& playList )
{
	// cerr << "*PlayList::operator=(const PlayList& playList)" << endl;

	if ( doc != playList.doc )
	{
		xmlNsPtr	ns;
		xmlNodePtr	root;

		xmlFreeDoc( doc );
		doc = xmlNewDoc( ( const xmlChar* ) "1.0" );
		root = xmlNewNode( NULL, ( const xmlChar* ) "smil" );
		ns = xmlNewNs( root, ( const xmlChar* ) SMIL20_NAMESPACE_HREF, ( const xmlChar* ) NULL );
		xmlDocSetRootElement( doc, root );
		parse( playList.GetBody(), clone, &root );
		dirty = playList.dirty;
		doc_name = playList.GetDocName( );
		RefreshCount( );
	}
	return *this;
}


/** The PlayList Destructor
    Deletes the XML document if we have one */

PlayList::~PlayList()
{
	// cerr << "*PlayList::~PlayList()" << endl;
	if ( doc != NULL )
	{
		xmlFreeDoc( doc );
		doc = NULL;
	}
}

/** Get the SMIL body element

    \return a node pointer to the body element */

xmlNodePtr PlayList::GetBody( ) const
{
	return xmlDocGetRootElement( doc )->children;
}


void PlayList::RefreshCount( )
{
	count = 0;
	if ( doc != NULL )
		parse( GetBody(), countFrames, &count );
}

/** Counts the number of frames in the playlist
 
    \return the number of frames in the playlist */

int PlayList::GetNumFrames() const
{
	return count;
}


char* PlayList::GetFileNameOfFrame( int frameNum ) const
{
	// cerr << "char* PlayList::GetFileNameOfFrame(int frameNum)" << endl;
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	parse( GetBody(), findFile, &data );
	return strdup( data.fileName );
}


/** Get one frame
 
    gets one frame of the playlist.
 
    \param absFrame the frame number to get
    \param frame the frame
    \return true if a frame could be copied, false otherwise
*/

bool PlayList::GetFrame( int frameNum, Frame &frame )
{
	MovieInfo data;

	// cerr << "bool PlayList::GetFrame(" << frameNum << ", Frame &frame)" << endl;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	parse( GetBody(), findFile, &data );

	if ( strcmp( data.fileName, "" ) )
	{
		// NB: In our playlist, we enforce an internal absolute path - there is no need
		// to convert the file name here
		string index( ( char * ) data.fileName );
		FileHandler *mediaFile = GetFileMap() ->GetMap() [ index ];
		if ( data.clipFrame >= mediaFile->GetTotalFrames() )
			data.clipFrame = mediaFile->GetTotalFrames() - 1;
		return ( mediaFile->GetFrame( frame, data.clipFrame ) >= 0 );
	}

	return false;
}


/** Get the FileHandler to which the current frame belongs
 
    \param absFrame the frame number to get
    \param media a pointer to a FileHandler to be set
    \return true if an FileHandler could be copied, false otherwise
*/
bool PlayList::GetMediaObject( int frameNum, FileHandler **media )
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	parse( GetBody(), findFile, &data );

	if ( strcmp( data.fileName, "" ) )
	{
		// Again, the absolute path is ensure internally - no need to convert
		string index( ( char * ) data.fileName );
		*media = GetFileMap() ->GetMap() [ index ];
		return true;
	}

	return false;
}


int PlayList::GetClipBegin( int frameNum ) const
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	if ( parse( GetBody(), findSceneStart, &data ) )
		return data.clipBegin;
	else
		return 0;
}


int PlayList::GetClipEnd( int frameNum ) const
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	if ( parse( GetBody(), findSceneEnd, &data ) )
		return data.clipEnd;
	else
		return 0;
}


bool PlayList::SetClipBegin( int frameNum, const char* value )
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	if ( parse( GetBody(), findSceneStart, &data ) )
	{
		xmlSetProp( data.video, ( const xmlChar * ) "clipBegin", ( const xmlChar * ) value );
		RefreshCount( );
		return true;
	}
	else
		return false;
}


bool PlayList::SetClipEnd( int frameNum, const char* value )
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	if ( parse( GetBody(), findSceneEnd, &data ) )
	{
		xmlSetProp( data.video, ( const xmlChar * ) "clipEnd", ( const xmlChar * ) value );
		RefreshCount( );
		return true;
	}
	else
		return false;
}


int PlayList::FindStartOfScene( int frameNum ) const
{
	MovieInfo data;

	// cerr << "int PlayList::FindStartOfScene(int frameNum)" << endl;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	parse( GetBody(), findSceneStart, &data );

	if ( strcmp( data.fileName, "" ) )
		return data.absBegin;
	else
		return 0;
}


int PlayList::FindEndOfScene( int frameNum ) const
{
	MovieInfo data;

	// cerr << "int PlayList::FindEndOfScene(int frameNum)" << endl;

	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	parse( GetBody(), findSceneEnd, &data );

	if ( strcmp( data.fileName, "" ) )
		return data.absEnd;
	else
		return 999999;
}


void PlayList::AutoSplit( int start, int end )
{
	MovieInfo firstFile;
	MovieInfo lastFile;
	Frame *frame = GetFramePool( ) ->GetFrame( );
	struct tm	recDate;
	time_t	startTime;
	time_t	endTime;

	// cerr << "PlayList::AutoSplit(int start=" << start << ", int end=" << end << ")" ;
	// cerr << endl;

	memset( &firstFile, 0, sizeof( MovieInfo ) );
	firstFile.absBegin = 0;
	firstFile.absEnd = 0;
	firstFile.absFrame = start;

	parse( GetBody(), findFile, &firstFile );
	string index1( ( char* ) firstFile.fileName );
	FileHandler *mediaFile1 = GetFileMap() ->GetMap() [ index1 ];
	mediaFile1->GetFrame( *frame, firstFile.clipFrame );
	frame->GetRecordingDate( recDate );
	startTime = mktime( &recDate );

	memset( &lastFile, 0, sizeof( MovieInfo ) );
	lastFile.absBegin = 0;
	lastFile.absEnd = 0;
	lastFile.absFrame = end;

	parse( GetBody(), findFile, &lastFile );

	string index2( ( char* ) lastFile.fileName );
	FileHandler *mediaFile2 = GetFileMap() ->GetMap() [ index2 ];
	mediaFile2->GetFrame( *frame, lastFile.clipFrame );
	frame->GetRecordingDate( recDate );
	endTime = mktime( &recDate );

	int fps = frame->IsPAL() ? 25 : 30;

	GetFramePool( ) ->DoneWithFrame( frame );

	// bail out on invalid recording date/time
	if ( startTime < 0 || endTime < 0 )
		return ;

	AutoSplit ( start, startTime, end, endTime, fps );
}


void PlayList::AutoSplit( int start, time_t startTime, int end, time_t endTime, int fps )
{
	time_t diffTime = static_cast<time_t>( difftime( endTime, startTime ) );
	if ( ( ( diffTime * fps ) - ( end - start ) ) > fps || diffTime < 0.0 )
	{
		if ( ( end - start ) > 1 )
		{
			time_t mid = start + ( end - start ) / 2;
			time_t midTime;
			// reduce the scope of temporary varables here to reduce memory usage when recurring:
			{
				MovieInfo midFile;
				struct tm recDate;
				Frame *frame = GetFramePool( ) ->GetFrame( );

				memset( &midFile, 0, sizeof( MovieInfo ) );
				midFile.absFrame = mid;

				parse( GetBody(), findFile, &midFile );

				string index( ( char* ) midFile.fileName );
				FileHandler *mediaFile = GetFileMap() ->GetMap() [ index ];
				mediaFile->GetFrame( *frame, midFile.clipFrame );
				frame->GetRecordingDate( recDate );
				midTime = mktime( &recDate );

				GetFramePool( ) ->DoneWithFrame( frame );
			}

			// bail out on invalid recording date/time
			if ( midTime < 0 )
				return ;

			AutoSplit ( start, startTime, mid, midTime, fps );
			AutoSplit ( mid, midTime, end, endTime, fps );
		}
		else
		{
			SplitSceneBefore( end );
		}
	}
}


bool PlayList::SplitSceneBefore( int frameNum )
{
	MovieInfo data;

	// cerr << "PlayList::SplitSceneBefore(int frameNum=" << frameNum << ")" << endl;

	if ( GetNumFrames() == 0 )
		return false;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;
	parse( GetBody(), findSceneStart, &data );
	int begin = data.absBegin;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;
	parse( GetBody(), findSceneEnd, &data );
	int end = data.absEnd;

	if ( strcmp( data.fileName, "" ) && begin != frameNum )
	{

		dirty = true;

		// Copy the right hand side of the current scene
 		xmlNode *firstSequence = data.sequence;
		PlayList playlist;
		GetPlayList( frameNum, end, playlist );

		// Paste it after the current scene
		xmlAddNextSibling( firstSequence, playlist.GetBody()->children );

		// in the first sequence, delete from frameNum to end of scene
		Delete( frameNum, end );

		return true;
	}
	else
	{
		return false;
	}
}

bool PlayList::JoinScenesAt( int frameNum )
{
	MovieInfo scene1;
	MovieInfo scene2;
	MovieInfo scene2end;

	if ( GetNumFrames() == 0 )
		return false;

	// cerr << "PlayList::JoinScenesAt(int frameNum=" << frameNum << ")" << endl;

	memset( &scene1, 0, sizeof( MovieInfo ) );
	scene1.absBegin = 0;
	scene1.absEnd = 0;
	scene1.absFrame = frameNum;
	parse( GetBody(), findSceneStart, &scene1 );

	memset( &scene2, 0, sizeof( MovieInfo ) );
	scene2.absBegin = 0;
	scene2.absEnd = 0;
	scene2.absFrame = frameNum;
	parse( GetBody(), findSceneEnd, &scene2 );
	int end = scene2.absEnd + 1;

	memset( &scene2end, 0, sizeof( MovieInfo ) );
	scene2end.absBegin = 0;
	scene2end.absEnd = 0;
	scene2end.absFrame = end;
	parse( GetBody(), findSceneEnd, &scene2end );

	if ( scene1.sequence != scene2end.sequence )
	{

		dirty = true;

		// cerr << ">>>> Joining scene at " << scene1.absBegin << " with scene at "
		// << scene2.absBegin << " which ends at " << scene2end.absEnd << endl;

		// concatenate the contents of scene2 into scene1
		xmlNode *lastchild = xmlGetLastChild( scene1.sequence );
		xmlNodePtr next = NULL;
		for ( xmlNodePtr ptr = scene2end.sequence->children; ptr != NULL; ptr = next )
		{
			next = ptr->next;
			lastchild = xmlAddNextSibling( lastchild, ptr );
			ptr = next;
		}
		// Merge the metadata properties
		for ( xmlAttr* attr = scene2end.sequence->properties; attr; attr = attr->next )
		{
			const char *valueB = ( const char* ) xmlGetProp( attr->parent, ( const xmlChar * ) attr->name );
			// Only if the value is meaningful
			if ( valueB && strcmp( valueB, "" ) )
			{
				// See if the property exists on clip A
				const char *valueA = ( const char* ) xmlGetProp( scene1.sequence, ( const xmlChar * ) attr->name );
				if ( valueA && strcmp( valueA, "" ) )
				{
					xmlChar* value = new xmlChar[ strlen(valueA) + strlen(valueB) + 1 ];
					strcpy( (char*) value, (const char*) valueA );
					strcat( (char*) value, " " );
					strcat( (char*) value, (const char*) valueB );
					xmlSetProp( scene1.sequence, attr->name, ( const xmlChar * ) value );
					delete[] value;
				}
				else
				{
					xmlSetProp( scene1.sequence, attr->name, ( const xmlChar * ) valueB );
				}
			}
		}
		xmlUnlinkNode( scene2end.sequence );
		xmlFreeNode( scene2end.sequence );
		RefreshCount( );

		return true;
	}
	else
	{
		return false;
	}
}


/** Get a playlist
 
    Returns a subset of the frames as a playlist. The parameters first and last
    must be within the available frames in the playlist.
 
    \param first number of first frame
    \param last number of last frame
    \param playlist the playlist object to be filled
    \return true if the frames could be copied, false otherwise
*/

bool PlayList::GetPlayList( int first, int last, PlayList &playlist ) const
{
	MovieInfo firstFile, lastFile;
	bool copyFlag = false;

	if ( GetNumFrames() == 0 )
		return false;

	playlist.dirty = false;

	// cerr << " bool PlayList::Copy(int first, int last, PlayList &playlist) " << endl;

	memset( &firstFile, 0, sizeof( MovieInfo ) );
	firstFile.absBegin = 0;
	firstFile.absEnd = 0;
	firstFile.absFrame = first;

	parse( GetBody(), findFile, &firstFile );

	memset( &lastFile, 0, sizeof( MovieInfo ) );
	lastFile.absBegin = 0;
	lastFile.absEnd = 0;
	lastFile.absFrame = last;

	parse( GetBody(), findFile, &lastFile );

	if ( strcmp( firstFile.fileName, "" ) && strcmp( lastFile.fileName, "" ) )
	{

		xmlNodePtr srcNode = GetBody();
		xmlNodePtr dstNode = playlist.GetBody();
		xmlNodePtr nextSeq = NULL;

		for ( xmlNodePtr srcSeq = srcNode->children; srcSeq != NULL; srcSeq = nextSeq )
		{
			nextSeq = srcSeq->next;
			if ( xmlStrcmp( srcSeq->name, ( const xmlChar* ) "seq" ) == 0 )
			{
				xmlNodePtr seq = xmlNewNode( NULL, ( const xmlChar* ) "seq" );
				xmlAddChild( dstNode, seq );
				xmlNodePtr nextVideo = NULL;

				for ( xmlNodePtr srcVideo = srcSeq->children; srcVideo != NULL; srcVideo = nextVideo )
				{
					nextVideo = srcVideo->next;
					if ( xmlStrcmp( srcVideo->name, ( const xmlChar* ) "video" ) == 0 )
					{

						// case 1: selection contains more than one file. This one is neither the first nor the last.

						if ( copyFlag && srcVideo != firstFile.video && srcVideo != lastFile.video )
						{
							xmlNodePtr video = xmlNewNode( NULL, ( const xmlChar* ) "video" );
							xmlAddChild( seq, video );
							for ( xmlAttr* attr = srcVideo->properties; attr; attr = attr->next )
								xmlNewProp( video, attr->name, xmlGetProp( attr->parent, attr->name) );
						}

						// case 2: selection contains more than one file and this is the first file

						else if ( srcVideo == firstFile.video && srcVideo != lastFile.video )
						{

							ostringstream sb1, sb2;

							xmlNodePtr video = xmlNewNode( NULL, ( const xmlChar* ) "video" );
							xmlNewProp( video, ( const xmlChar* ) "src", ( const xmlChar* ) firstFile.fileName );
							sb1 << firstFile.clipFrame << ends;
							xmlNewProp( video, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) sb1.str().c_str() );
							sb2 << firstFile.clipEnd << ends;
							xmlNewProp( video, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) sb2.str().c_str() );
							xmlAddChild( seq, video );
							copyFlag = true;
						}

						// case 3: selection contains more than one file and this is the last file

						else if ( srcVideo != firstFile.video && srcVideo == lastFile.video )
						{

							ostringstream sb1, sb2;

							xmlNodePtr video = xmlNewNode( NULL, ( const xmlChar* ) "video" );
							xmlNewProp( video, ( const xmlChar* ) "src", ( const xmlChar* ) lastFile.fileName );
							sb1 << lastFile.clipBegin << ends;
							xmlNewProp( video, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) sb1.str().c_str() );
							sb2 << lastFile.clipFrame << ends;
							xmlNewProp( video, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) sb2.str().c_str() );
							xmlAddChild( seq, video );
							copyFlag = false;
						}

						// case 4: selection contains exactly one file

						else if ( srcVideo == firstFile.video && srcVideo == lastFile.video )
						{

							ostringstream sb1, sb2;

							xmlNodePtr video = xmlNewNode( NULL, ( const xmlChar* ) "video" );
							xmlNewProp( video, ( const xmlChar* ) "src", ( const xmlChar* ) firstFile.fileName );
							sb1 << firstFile.clipFrame << ends;
							xmlNewProp( video, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) sb1.str().c_str() );
							sb2 << lastFile.clipFrame << ends;
							xmlNewProp( video, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) sb2.str().c_str() );
							xmlAddChild( seq, video );
						}
					}
				}

				// if this sequence does not have any video clips, remove it

				if ( seq->children == NULL )
				{
					xmlUnlinkNode( seq );
					xmlFreeNode( seq );
				}
				else
				// copy the seq attributes
				{
					for ( xmlAttr* attr = srcSeq->properties; attr; attr = attr->next )
						xmlNewProp( seq, attr->name, xmlGetProp( attr->parent, attr->name) );
				}
			}
		}
		// PASS PATH
		string path = directory_utils::get_directory_from_file( GetDocName() );
		parse( playlist.GetBody(), fillMap, &path );
	}
	playlist.RefreshCount( );
	return true;
}


/** Insert a playlist
 
    Inserts all frames contained in the parameter playlist.
    To insert the frames at the start of the playlist, use
    a before value of 0. To append it at the end of the playlist.
    pass the first unused (= number of frames contained) index.
 
    \param playlist The playlist to insert
    \param before insert playlist before this frame
*/

bool PlayList::InsertPlayList( PlayList &playlist, int before )
{
	// cerr << "bool PlayList::Paste(PlayList &playlist, int before(=" << before << "))" << endl;

	if ( playlist.GetNumFrames() == 0 )
		return false;

	// PASS PATH
	string path = directory_utils::get_directory_from_file( GetDocName() );
	parse( playlist.GetBody(), fillMap, &path );

	MovieInfo file;

	memset( &file, 0, sizeof( MovieInfo ) );
	file.absBegin = 0;
	file.absEnd = 0;
	file.absFrame = before;
	file.sequence = NULL;
	file.video = NULL;

	// Fill the map with any new files now, before we change the doc
	parse( GetBody(), findFile, &file );

	xmlNodePtr node = playlist.GetBody();
	bool first = true;
	xmlNodePtr next = NULL;
	xmlNodePtr sequence = file.sequence;

	if ( GetNumFrames() > 0 )
	{
		dirty = true;
	}
	else
	{
		dirty = playlist.dirty;

		if ( doc_name == "" )
			doc_name = playlist.GetDocName( );

	}

	for ( xmlNodePtr ptr = node->children; ptr != NULL; ptr = next )
	{

		//cerr << endl << "Sibling" << endl;
		//xmlElemDump(stderr, NULL, ptr);
		//cerr << endl;

		// Get the next sibling before adding
		next = ptr->next;

		// If first and at start of scene insert, otherwise append
		// cerr << "Scene i'm pasting into starts at " << file.absBegin << " [" << file.absEnd << "]" << endl;

		if ( first && sequence == NULL )
		{
			// This strange approach avoids using xmlCopyNode, which adds extra namespace declarations
			xmlNodePtr tmp = xmlNewNode( NULL, ( const xmlChar* ) "seq" );
			xmlAddChild( GetBody(), tmp );
			sequence = xmlAddNextSibling( tmp, ptr );
			xmlUnlinkNode( tmp );
			xmlFreeNode( tmp );
		}
		else if ( first && before == file.absBegin && before != ( file.absEnd + 1 ) )
		{
			// cerr << "Inserting before " << before << endl;
			sequence = xmlAddPrevSibling( sequence, ptr );
		}
		else if ( first && before != ( file.absEnd + 1 ) )
		{
			// cerr << "Splitting scene that start at " << file.absBegin << " and ends at " << file.absEnd << " at " << before << endl;
			// cerr << endl << "Before Split" << endl;
			// xmlElemDump(stderr, NULL, sequence);
			// cerr << endl;

			// Split the current scene
			SplitSceneBefore( before );

			// Find our new position
			memset( &file, 0, sizeof( MovieInfo ) );
			file.absBegin = 0;
			file.absFrame = before;
			file.sequence = NULL;
			file.video = NULL;

			parse( GetBody(), findFile, &file );

			// cerr << endl << "After Split" << endl;
			// xmlElemDump(stderr, NULL, sequence);
			// cerr << endl;

			// Add before the scene returned
			sequence = xmlAddPrevSibling( file.sequence, ptr );
		}
		else
		{
			// cerr << "Inserting after " << before << endl;
			sequence = xmlAddNextSibling( sequence, ptr );
		}

		// We're definitely no longer first
		first = false;
	}

	RefreshCount( );
	return true;
}


bool PlayList::Delete( int first, int last )
{
	int absClipBegin;
	int clipBegin;
	int clipEnd;
	static int firstCall = 0;

	// cerr << "bool PlayList::Delete(int first=" << first << ", int last=" << last << ")" << endl;

	// SplitSceneBefore calls Delete, avoid recursion

	if ( GetNumFrames() == 0 )
		return false;

	if ( firstCall == 0 )
	{
		firstCall = 1;
		SplitSceneBefore( first );
		firstCall = 0;
	}

	xmlNodePtr srcNode = GetBody();
	absClipBegin = 0;
	xmlNodePtr nextSequence = NULL;
	for ( xmlNodePtr srcSeq = srcNode->children; srcSeq != NULL; srcSeq = nextSequence )
	{

		dirty = true;

		// In case we need to delete this node, get the next pointer before starting
		nextSequence = srcSeq->next;

		if ( xmlStrcmp( srcSeq->name, ( const xmlChar* ) "seq" ) == 0 )
		{
			xmlNodePtr nextVideo = NULL;

			for ( xmlNodePtr srcVideo = srcSeq->children; srcVideo != NULL; srcVideo = nextVideo )
			{

				// In case we have to delete this node
				nextVideo = srcVideo->next;

				if ( xmlStrcmp( srcVideo->name, ( const xmlChar* ) "video" ) == 0 )
				{

					ostringstream sb1, sb2;
					xmlChar *s;

					sb1 << ( s = xmlGetProp( srcVideo, ( const xmlChar* ) "clipBegin" ) ) << ends;
					clipBegin = atoi( sb1.str().c_str() );
					if ( s )
						xmlFree( s );
					s = xmlGetProp( srcVideo, ( const xmlChar* ) "clipEnd" );
					clipEnd = atoi( ( char * ) s );
					sb2 << ( s = xmlGetProp( srcVideo, ( const xmlChar* ) "clipEnd" ) ) << ends;
					clipEnd = atoi( sb2.str().c_str() );
					if ( s )
						xmlFree( s );

					// case 1: selection covers this file completely. Remove this file from playlist.

					if ( first <= absClipBegin && last >= absClipBegin + clipEnd - clipBegin )
					{
						xmlUnlinkNode( srcVideo );
						xmlFreeNode( srcVideo );
						// cerr << "case 1 " << endl;
					}

					// case 2: selection starts before or at start of file and ends somewhere in the file.
					// New start of file is now end of selection + 1

					else if ( first <= absClipBegin && last >= absClipBegin && last <= absClipBegin + clipEnd - clipBegin )
					{

						ostringstream sb;

						sb << last - absClipBegin + clipBegin + 1 << ends;
						xmlSetProp( srcVideo, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) sb.str().c_str() );
						// cerr << "case 2 " << endl;
					}

					// case 3: selection starts somewhere in the file and ends at or after the file
					// New end of file is now start of selection - 1

					else if ( first > absClipBegin && first <= absClipBegin + clipEnd - clipBegin && last >= absClipBegin + clipEnd - clipBegin )
					{

						ostringstream sb;

						sb << first - absClipBegin + clipBegin - 1 << ends;
						xmlSetProp( srcVideo, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) sb.str().c_str() );
						// cerr << "case 3 " << endl;
					}

					// case 4: selection starts somewhere in the file and ends in the file.
					// We must split this node such that end of first file is start of selection - 1
					// and start of second file is end of selection + 1

					else if ( first > absClipBegin && last < absClipBegin + clipEnd - clipBegin )
					{

						ostringstream sb1, sb2;
						xmlChar	*s;

						xmlNodePtr video = xmlNewNode( NULL, ( const xmlChar* ) "video" );
						xmlNewProp( video, ( const xmlChar* ) "src", ( s = xmlGetProp( srcVideo, ( const xmlChar* ) "src" ) ) );
						if ( s )
							xmlFree( s );
						sb1 << last - absClipBegin + clipBegin + 1 << ends;
						xmlNewProp( video, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) sb1.str().c_str() );
						xmlNewProp( video, ( const xmlChar* ) "clipEnd", ( s = xmlGetProp( srcVideo, ( const xmlChar* ) "clipEnd" ) ) );
						if ( s )
							xmlFree( s );
						xmlAddNextSibling( srcVideo, video );
						sb2 << first - absClipBegin + clipBegin - 1 << ends;
						xmlSetProp( srcVideo, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) sb2.str().c_str() );
						// cerr << "case 4 " << endl;
					}

					absClipBegin += clipEnd - clipBegin + 1;
				}
			}

			// if the node is now empty, delete it (can delete - see nextSequence above)

			if ( srcSeq->children == NULL )
			{
				xmlUnlinkNode( srcSeq );
				xmlFreeNode( srcSeq );
			}
		}
	}

	RefreshCount( );

	return true;
}


bool PlayList::LoadMediaObject( char *filename )
{
	// cerr << "bool PlayList::LoadAVI(" << filename << ")" << endl;

	xmlNodePtr	seq;
	xmlNodePtr	node;
	ostringstream	sb;
	FileHandler	*mediaFile = NULL;
	int	existingFrames;
	int	framesInFile;

	dirty = true;

	// This object should be located in a directory relative to cwd or absolute
	string index = directory_utils::get_absolute_path_to_file( "", ( char * ) filename );
	if ( GetFileMap() ->GetMap().find( index ) == GetFileMap() ->GetMap().end( ) )
	{
		if ( strrchr( filename, '.' ) )
		{
			if ( strncasecmp( strrchr( filename, '.' ), ".avi", 4 ) == 0 )
				mediaFile = new AVIHandler();
			else if ( strncasecmp( strrchr( filename, '.' ), ".dv", 3 ) == 0 ||
					strncasecmp( strrchr( filename, '.' ), ".dif", 4 ) == 0 )
				mediaFile = new RawHandler();
#ifdef HAVE_LIBQUICKTIME
			else if ( strncasecmp( strrchr( filename, '.' ), ".mov", 4 ) == 0 )
				mediaFile = new QtHandler();
#endif
		}

		if ( mediaFile == NULL )
			return false;
		if ( mediaFile->Open( filename ) == false )
			return false;
		GetFileMap() ->GetMap() [ index ] = mediaFile;
	}
	else
	{
		mediaFile = GetFileMap() ->GetMap() [ index ];
	}

	framesInFile = mediaFile->GetTotalFrames();
	existingFrames = GetNumFrames();

	seq = xmlNewNode( NULL, ( const xmlChar* ) "seq" );
	xmlAddChild( GetBody(), seq );
	node = xmlNewNode( NULL, ( const xmlChar* ) "video" );
	xmlNewProp( node, ( const xmlChar* ) "src", ( const xmlChar* ) index.c_str() );
	xmlNewProp( node, ( const xmlChar* ) "clipBegin", ( const xmlChar* ) "0" );
	sb << framesInFile - 1 << ends;
	xmlNewProp( node, ( const xmlChar* ) "clipEnd", ( const xmlChar* ) sb.str().c_str() );
	xmlAddChild( seq, node );

	if ( framesInFile > 0 )
	{
		RefreshCount( );
		AutoSplit( existingFrames, existingFrames + framesInFile - 1 );
	}
	return true;
}


bool PlayList::LoadPlayList( char *filename )
{
	// cerr << "bool PlayList::LoadPlayList(" << filename << ")" << endl;

	dirty = false;

	xmlNsPtr ns;
	xmlNodePtr node;

	xmlFreeDoc( doc );
	doc = xmlParseFile( filename );
	if ( !doc )
	{
		cerr << "file does not exist or failed to parse XML" << endl;
		return false;
	}

	node = xmlDocGetRootElement( doc );
	if ( node == NULL )
	{
		cerr << "empty document" << endl;
		xmlFreeDoc( doc );
		doc = NULL;
		return false;
	}
	ns = xmlSearchNsByHref( doc, node, SMIL20_NAMESPACE_HREF );
	if ( ns == NULL )
	{
		cerr << "document of the wrong type, Namespace not found" << endl;
		xmlFreeDoc( doc );
		doc = NULL;
		return false;
	}
	if ( xmlStrcmp( node->name, ( const xmlChar * ) "smil" ) )
	{
		cerr << "document of the wrong type, root node != smil" << endl;
		xmlFreeDoc( doc );
		doc = NULL;
		return false;
	}
	CleanPlayList( node );

	// PASS PATH
	string path = directory_utils::get_directory_from_file( filename );
	parse( GetBody(), fillMap, &path );
	dirty = false;

	// Legacy documents have smil2 namespace prefix declaration
	if ( xmlSearchNs( doc, node, ( const xmlChar * ) "smil2" ) == NULL )
	{
		// New age compliant documents use SMIL time values, but we continue
		// to use frame numbers internally.
		parse( node, convertSmilTimeToFrames, NULL );
	}
	else
	{
		// We need to insert a body element into the legacy document
		xmlNodePtr root = xmlDocGetRootElement( doc );
		xmlNodePtr seq = root->children;
		if ( !seq )
			return false;
		if ( xmlStrcmp( seq->name, ( const xmlChar* ) "seq" ) == 0 )
		{
			xmlNodePtr body = xmlNewNode( NULL, ( const xmlChar * ) "body" );
			while ( seq )
			{
				xmlNodePtr next = seq->next;
				xmlUnlinkNode( seq );
				xmlAddChild( body, seq );
				seq = next;
			}
			xmlAddChild( root, body );
		}
		dirty = true;
	}

	RefreshCount( );

	return true;
}

static bool relativeMap( xmlNodePtr node, void *p, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{
		// Check whether the required properties exist
		xmlChar * src = xmlGetProp( node, ( const xmlChar* ) "src" );

		if ( src != NULL )
		{
			// Determine the absolute path to the file indicated by src
			string & directory = *( string * ) p;
			string index = directory_utils::get_relative_path_to_file( directory, ( char * ) src );

			// Save to relative file now
			xmlSetProp( node, ( const xmlChar* ) "src", ( xmlChar * ) index.c_str() );
		}

		xmlFree( src );
	}
	return false;
}


bool PlayList::SavePlayList( char *filename, bool isLegacyFormat )
{
	// Try to save file
	bool ret = false;
	// Copy the xml doc
	xmlDocPtr copy_doc = xmlNewDoc( ( const xmlChar* ) "1.0" );
	xmlNodePtr root = xmlNewNode( NULL, ( const xmlChar* ) "smil" );
	xmlNewNs( root, ( const xmlChar* ) SMIL20_NAMESPACE_HREF, ( const xmlChar* ) NULL );
	xmlDocSetRootElement( copy_doc, root );

	if ( isLegacyFormat )
	{
		// Clone without body
		parse( this->GetBody()->children, clone, &root );	

		// Add legacy namespace declaration
		xmlNewNs( xmlDocGetRootElement( copy_doc ), SMIL20_NAMESPACE_HREF, ( const xmlChar* ) "smil2" );
	}
	else
	{
		// Clone including body
		parse( this->GetBody(), clone, &root );

		// Convert frame numbers used internally to SMIL time values.
		parse( copy_doc->children, convertFramesToSmilTime, NULL );
	}

	if ( Preferences::getInstance().relativeSave )
	{
		// Obtain path relative to filenames directory
		string path = directory_utils::get_directory_from_file( filename );
		// Convert the copy to relative
		parse( copy_doc->children, relativeMap, &path );
		// Save the copy
		ret = xmlSaveFormatFile( filename, copy_doc, 1 ) != -1;
	}
	else
	{
		// Can save directly
		ret = xmlSaveFormatFile( filename, copy_doc, 1 ) != -1;
	}
	// Delete the copy
	xmlFreeDoc( copy_doc );

	// If saved...
	if ( !isLegacyFormat && ret )
	{
		// ... and doc name is unspecified, we have now and we're not dirty any more
		// though all undos are dirty
		if ( doc_name == "" )
		{
			doc_name = string( filename );
			dirty = false;
			GetEditorBackup( ) ->SetAllDirty( );
		}
		// ... and doc name is the same as file name then we're not dirty any more any more
		// though all undos are dirty
		else if ( !strcmp( filename, doc_name.c_str() ) )
		{
			dirty = false;
			GetEditorBackup( ) ->SetAllDirty( );
		}
		// ... otherwise we're still dirty - undos stay the same
	}

	return ret;
}

/** Save a playlist en mjpegtools ELI format.
 
    Returns a subset of the frames as a playlist. The parameters first and last
    must be within the available frames in the playlist.
 
    \param first number of first frame
    \param last number of last frame
    \return true if the frames could be copied, false otherwise
*/

bool PlayList::SavePlayListEli( char * filename, bool isPAL )
{
	EliInfos eli;
	if ( doc != NULL )
	{
		parse( GetBody(), convertEli, &eli );
	}

	/* Open a file */
	ofstream eli_file( filename );
	if ( !eli_file )
	{
		return false;
	}

	eli_file << "LAV Edit List" << endl;
	eli_file << ( isPAL ? "PAL" : "NTSC" ) << endl;

	/* The number of clips */
	eli_file << eli.size() << endl;

	/* Now, all the clips, without numbers  */
	EliInfosIterator End = eli.end();
	EliInfosIterator i;
	for ( i = eli.begin(); i != End; i++ )
	{
		eli_file << ( *i ).file << endl;
	}

	/* Now, number \s begin \s end\n */
	int count = 0;
	for ( i = eli.begin(); i != End; i++ )
	{
		eli_file << count
		<< " " << ( *i ).clipBegin
		<< " " << ( *i ).clipEnd << endl;
		++count;
	}

	/* Check the final status */
	if ( eli_file.bad() )
	{
		return false;
	}

	/* We are done */
	eli_file.close();

	return true;
}


bool PlayList::SavePlayListSrt( const char* filename )
{
	SrtContext srt( filename );

	/* Open a file */
	if ( !srt.file )
	{
		return false;
	}
	if ( doc )
	{
		parse( GetBody(), convertSrt, &srt );
	}
	
	return !srt.file.bad();
}


/** Recursively deletes unnecessary items from our XML tree.
 
    We need only <smil>, <seq> and <video> nodes, delete
    everything else, in particular the text nodes too.
 
    \param node start deleting here
*/


void PlayList::CleanPlayList( xmlNodePtr node )
{
	while ( node != NULL )
	{

		xmlNodePtr nodeToDelete = NULL;

		CleanPlayList( node->children );
		if ( xmlStrcmp( node->name, ( const xmlChar* ) "smil" ) == 0 )
		{
			//do nothing
		}
		else if ( xmlStrcmp( node->name, ( const xmlChar* ) "body" ) == 0 )
		{
			//do nothing
		}
		else if ( xmlStrcmp( node->name, ( const xmlChar* ) "seq" ) == 0 )
		{
			if ( node->children == NULL )
			{
				nodeToDelete = node;
			}
		}
		else if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
		{
			// do nothing
		}
		else
			nodeToDelete = node;
		node = node->next;

		if ( nodeToDelete != NULL )
		{
			xmlUnlinkNode( nodeToDelete );
			xmlFreeNode( nodeToDelete );
		}
	}
	RefreshCount( );
}

/** Clean the playlist properly - this allows a new doc to be loaded.
*/

void PlayList::CleanPlayList( )
{
	if ( GetNumFrames() > 0 )
		Delete( 0, GetNumFrames() );
	dirty = false;
	doc_name = "";
	RefreshCount( );
}

/** Checks if the file exists in the current playlist.
 
    \param node a node
    \param p pointer to a string
    \result true if file has been found and xml tree walk is done 
*/

static bool checkIfFileUsed( xmlNodePtr node, void *p, bool *freed )
{
	if ( xmlStrcmp( node->name, ( const xmlChar* ) "video" ) == 0 )
	{
		// Check whether the required properties exist
		xmlChar * src = xmlGetProp( node, ( const xmlChar* ) "src" );
		string index( ( char * ) src );
		xmlFree( src );
		return *( string * ) p == index;
	}
	return false;
}

/** Checks if the filename exists in the current playlist.
 
	\param filename file name to search for
	\result true if file exists
*/

bool PlayList::IsFileUsed( string filename ) const
{
	return parse( GetBody(), checkIfFileUsed, &filename );
}

/** Returns the current setting of the dirty flag
 
	\result true if the playlist is dirty
*/

bool PlayList::IsDirty( ) const
{
	return dirty;
}

/** Sets the dirty flag on the playlist.
 
    \param value value of dirty flag
*/

void PlayList::SetDirty( bool value )
{
	dirty = value;
}

/** Returns the doc name associated to the playlist.
 
    NB: This should reflect the first .smil file received via the Load or Save 
	methods above.
 
	\result the file name of the playlist
*/

string PlayList::GetDocName( ) const
{
	return doc_name;
}

/** Protected set doc name method.
 
    The playlist class uses the first file name of load or save, so operations like
	save as shouldn't trigger this method.
 
	\param m_doc_name doc name to use
*/

void PlayList::SetDocName( string m_doc_name )
{
	doc_name = m_doc_name;
}

void PlayList::GetLastCleanPlayList( PlayList &playlist )
{
	if ( playlist.GetNumFrames() > 0 )
		playlist.Delete( 0, playlist.GetNumFrames() );

	// If we have a file, load it, otherwise we have valid contents already...
	if ( doc_name != "" )
		playlist.LoadPlayList( ( char * ) doc_name.c_str() );
}

/** Obtain the current project directory.
 
    \result the directory to place files containing this playlist
*/

string PlayList::GetProjectDirectory( )
{
	string output = "";

	if ( GetDocName() != "" )
		output = directory_utils::get_directory_from_file( GetDocName( ) );

	if ( output == "" && strcmp( Preferences::getInstance().defaultDirectory, "" ) )
		output = directory_utils::expand_directory( Preferences::getInstance().defaultDirectory );

	if ( output == "" )
		output = directory_utils::join_file_to_directory( "", "" );

	return output;
}

string PlayList::GetSeqAttribute( int frameNum, const char* name ) const
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	if ( parse( GetBody(), findSceneStart, &data ) && data.sequence )
	{
		const char *value = ( const char* ) xmlGetProp( data.sequence, ( const xmlChar * ) name );
		if ( value )
			return value;
	}
	return "";
}


bool PlayList::SetSeqAttribute( int frameNum, const char* name, const char* value )
{
	MovieInfo data;

	memset( &data, 0, sizeof( MovieInfo ) );
	data.absBegin = 0;
	data.absEnd = 0;
	data.absFrame = frameNum;

	if ( parse( GetBody(), findSceneStart, &data ) && data.sequence )
	{
		xmlSetProp( data.sequence, ( const xmlChar * ) name, ( const xmlChar * ) value );
		dirty = true;
		return true;
	}
	return false;
}


bool PlayList::SetDocId( const char* value )
{
	if ( GetBody() )
	{
		xmlSetProp( GetBody(), ( const xmlChar * ) "id", ( const xmlChar * ) value );
		dirty = true;
		return true;
	}
	return false;
}


string PlayList::GetDocId( ) const
{
	if ( GetBody() )
	{
		const char *value = ( const char* ) xmlGetProp( GetBody(), ( const xmlChar * ) "id" );
		if ( value )
			return value;
	}
	return "";
}


bool PlayList::SetDocTitle( const char* value )
{
	if ( GetBody() )
	{
		xmlSetProp( GetBody(), ( const xmlChar * ) "title", ( const xmlChar * ) value );
		dirty = true;
		return true;
	}
	return false;
}


string PlayList::GetDocTitle( ) const
{
	if ( GetBody() )
	{
		const char *value = ( const char* ) xmlGetProp( GetBody(), ( const xmlChar * ) "title" );
		if ( value )
			return value;
	}
	return "";
}

string PlayList::GetBaseFileOfFrame( int frameNum ) const
{
	char *fname = GetFileNameOfFrame( frameNum );
	char  bfname[strlen( fname ) + 10];
	char *ptr = strrchr( fname, '.' );
	if ( ptr ) *ptr = 0;
	sprintf( bfname, "%s_%06d", fname, frameNum );
	free( fname );
	return bfname;
}


/** Editor backup - holds all the previous playlists (up to a the maxUndos preference
 	value).
*/

EditorBackup::EditorBackup() : position( -1 )
{
	cerr << ">> Creating undo/redo buffer" << endl;
	maxUndos = Preferences::getInstance().maxUndos;
}

/** Destructor for the Editor Backup object.
*/

EditorBackup::~EditorBackup()
{
	cerr << ">> Destroying undo/redo buffer" << endl;

	while ( backups.size() )
	{
		delete backups[ backups.size() - 1 ];
		backups.pop_back();
	}
}

void EditorBackup::Store( PlayList *playlist, bool isPersisted )
{
	cerr << ">>> Received playlist to store at position " << position + 1 << endl;

	// Three conditions to check:
	//
	// 1. The undo position is 1 less than the current size of the vector and less than the max size
	// in which case we dump the new playlist at the top of the vector and increment the position
	//

	if ( ( position + 1 ) == ( int ) backups.size() && ( position < maxUndos || maxUndos == 0 ) )
	{
		cerr << ">>>> Adding to end" << endl;
		position ++;
		PlayList *temp = new PlayList;
		playlist->GetPlayList( 0, playlist->GetNumFrames() - 1, *temp );
		temp->SetDirty( playlist->IsDirty( ) );
		backups.push_back( temp );
	}

	//
	// 2. The undo position is not at the end, in which case we need to remove everything from position
	// to the end before pushing
	//

	else if ( ( position + 1 ) < ( int ) backups.size() )
	{
		cerr << ">>>> Cleaning from " << position + 1 << " to " << backups.size() << endl;
		position ++;
		while ( position < ( int ) backups.size() )
		{
			delete backups[ backups.size() - 1 ];
			backups.pop_back();
		}
		PlayList *temp = new PlayList;
		playlist->GetPlayList( 0, playlist->GetNumFrames() - 1, *temp );
		temp->SetDirty( playlist->IsDirty( ) );
		backups.push_back( temp );
	}

	//
	// 3. We're at the top of the stack so we need to remove position 0 and push to the end
	//

	else if ( position == maxUndos )
	{
		cerr << ">>>> Removing the earliest playlist to make room" << endl;
		delete backups[ 0 ];
		backups.erase( backups.begin() );
		PlayList *temp = new PlayList;
		playlist->GetPlayList( 0, playlist->GetNumFrames() - 1, *temp );
		temp->SetDirty( playlist->IsDirty( ) );
		backups.push_back( temp );
	}

	//
	// Just in case we missed something...
	//

	else
	{
		cerr << ">>>> Unknown condition - position = " << position << " size = " << backups.size() << endl;
	}

	// Update persistent storage
	if ( isPersisted )
	{
		// First, delete contents of private directory in $HOME
		string directory = KINO_AUTOSAVE_DIR;
		mkdir( directory.c_str(), 0700 );
		DIR* dir = opendir( directory.c_str( ) );
		struct dirent* entry;
		if ( dir )
		{
			while ( ( entry = readdir( dir ) ) != NULL )
			{
				if ( entry->d_name[0] != '.' )
				{
					ostringstream sb;
					sb << directory << "/" << entry->d_name << ends;
					unlink( sb.str().c_str() );
				}
			}
			closedir( dir );
	
			// Second, write each dirty backup
			// absolute filenames is faster and more appropriate here
			bool isRelative = Preferences::getInstance().relativeSave;
			Preferences::getInstance().relativeSave = false;
			for ( size_t i = 0; i < backups.size(); ++i )
			{
				if ( backups[i]->IsDirty() )
				{
					ostringstream sb;
					sb << directory << "/" << i << ".xml" << ends;
					// save in legacy format for speed
					backups[i]->SavePlayList( const_cast<char*>( sb.str().c_str() ), true );
				}
			}
			Preferences::getInstance().relativeSave = isRelative;
		}
	}
}

void EditorBackup::Undo( PlayList *playlist )
{
	cerr << ">>> Received request to undo from position " << position - 1 << endl;
	if ( CanUndo() )
	{
		position --;
		playlist->Delete( 0, playlist->GetNumFrames() - 1 );
		PlayList temp( *( backups[ position ] ) );
		playlist->InsertPlayList( temp, 0 );
		playlist->SetDirty( temp.IsDirty( ) );
	}
	else
	{
		cerr << ">>>> Unable to satisfy request." << endl;
	}
}

void EditorBackup::Redo( PlayList *playlist )
{
	cerr << ">>> Received request to recover from position " << position + 1 << endl;
	if ( CanRedo() )
	{
		position ++;
		playlist->Delete( 0, playlist->GetNumFrames() - 1 );
		PlayList temp( *( backups[ position ] ) );
		playlist->InsertPlayList( temp, 0 );
		playlist->SetDirty( temp.IsDirty( ) );
	}
	else
	{
		cerr << ">>>> Unable to satisfy request." << endl;
	}
}

/** Set all the stored backups as dirty other than the current one.
 
  	When a file is saved, the previous copy (which may still be in the editor backup), 
	will be dirty (relative to the current contents of the file) - this method ensures
	that only the most recent item will be registered as clean.
*/

void EditorBackup::SetAllDirty( )
{
	vector< PlayList *>::iterator n;
	for ( n = backups.begin(); n != backups.end(); ++n )
		( **n ).SetDirty( true );
	if ( position >= 0 )
		backups[ position ] ->SetDirty( false );
}

/** Clear the contents of the editor backup.
*/

void EditorBackup::Clear( )
{
	while ( backups.size() )
	{
		delete backups[ backups.size() - 1 ];
		backups.pop_back();
	}
	position = -1;
}

/** Singleton method to obtain the editor backup.
*/

EditorBackup *GetEditorBackup( )
{
	static EditorBackup * backup = new EditorBackup( );
	return backup;
}

bool EditorBackup::Restore( PlayList * playlist )
{
	string directory = KINO_AUTOSAVE_DIR;
	DIR* dir = opendir( directory.c_str( ) );
	struct dirent* entry;
	bool result = false;

	if ( dir )
	{
		std::vector<int> names;
		while ( ( entry = readdir( dir ) ) != NULL )
		{
			if ( entry->d_name[0] != '.' )
				names.push_back( atoi( entry->d_name ) );
		}
		closedir( dir );

		if ( !names.empty() &&
			( modal_confirm( GTK_STOCK_OK, NULL, _( "Kino has detected a crash. Do you want to recover?" ) )
				== GTK_RESPONSE_ACCEPT ) )
		{
			std::sort( names.begin(), names.end() );
			Clear();
			PlayList pl;
			for ( size_t i = 0; i < names.size(); ++i )
			{
				ostringstream sb;
				sb << directory << "/" << names[i] << ".xml" << ends;
				if ( pl.LoadPlayList( const_cast<char*>( sb.str().c_str() ) ) )
					Store( &pl, false );
			}
			++position;
			Undo( playlist );
			result = true;
		}
	}
	return result;
}

bool EditorBackup::CanUndo( ) const
{
	return ( position > 0 );
}

bool EditorBackup::CanRedo( ) const
{
	return ( ( position + 1 ) < ( int ) backups.size() );
}


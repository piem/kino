/*
* export.cc Export Mode Base Object
* Copyright (C) 2002 Mads Bondo Dydensborg <madsdyd@challenge.dk>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <vector>
using std::cerr;
using std::endl;

#include <math.h>
#include <unistd.h>

#include "export.h"
#include "page_editor.h"

static void on_tool_change( GtkOptionMenu *optionmenu, gpointer user_data )
{
	if ( gtk_option_menu_get_history( optionmenu ) == 2 )
		( ( Export * ) user_data ) ->selectScene( common->getCurrentScene( ) );
	else
		( ( Export * ) user_data ) ->selectSection( gtk_option_menu_get_history( optionmenu ) );
}

/** ********************************************************************** **/
/** Export constructor
    Really only setups some variables */
/** ********************************************************************** **/
Export::Export( PageExport *_exportPage, KinoCommon *_common ) :
		actions( EXPORT_SCENE_LIST ), exportPage( _exportPage ), common( _common )
{
	menuRange = GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(), "optionmenu_export_range" ) );

	startSpin = GTK_SPIN_BUTTON( lookup_widget( exportPage->getWidget(), "spinbutton_export_range_start" ) );
	startEntry = GTK_ENTRY( lookup_widget( exportPage->getWidget(), "entry_export_start" ) );
	endSpin = GTK_SPIN_BUTTON( lookup_widget( exportPage->getWidget(), "spinbutton_export_range_end" ) );
	endEntry = GTK_ENTRY( lookup_widget( exportPage->getWidget(), "entry_export_end" ) );
	everySpin = GTK_SPIN_BUTTON( lookup_widget( exportPage->getWidget(), "spinbutton_export_range_every" ) );

	gtk_option_menu_set_history( menuRange, 0 );
	selectSection( 0 );
	g_signal_connect( G_OBJECT( menuRange ), "changed", G_CALLBACK( on_tool_change ), this );


	/* Get pointers to the common control buttons (bottom of page) */
	previewButton
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "togglebutton_export_preview" ) );
	exportButton
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "togglebutton_export_export" ) );
	stopButton
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "togglebutton_export_stop" ) );
	pauseButton
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "togglebutton_export_pause" ) );

}

/** ********************************************************************** **/
/** This methods wrap the actual calls to doExport.
    It makes sure that only the stop and pause buttons are available.
    It then calls doExport
    Finally it setups the buttons again
    It also intiliases the time stuff, and displays a few status messages.
*/
/** ********************************************************************** **/
void Export::startExport( bool preview )
{
	bool supports_preview = actions & EXPORT_PREVIEW;
	bool supports_export = actions & EXPORT_EXPORT;
	bool supports_pause = actions & EXPORT_PAUSE;

	/* Make sure the user can only click relevant buttons
	 exportPage->exportMutex is a protection against the buttons going
	 into a wild events generating loop. */
	exportPage->exportMutex = true;
	/* When exporting, export is pressed and can not be pressed
	   pause and stop can be pressed, preview can not */
	gtk_toggle_button_set_active( previewButton, FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET( previewButton ), FALSE );

	gtk_toggle_button_set_active( exportButton, TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET( exportButton ), FALSE );

	gtk_toggle_button_set_active( stopButton, FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), TRUE );

	gtk_toggle_button_set_active( pauseButton, FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET( pauseButton ), supports_pause );
	exportPage->exportMutex = false;

	/* Make the tabbed notebook inaccesible during export */
	GtkNotebook * tmp =
	    GTK_NOTEBOOK( lookup_widget( common->getWidget(), "notebook_export" ) );
	if ( !tmp )
	{
		cerr << "Export::startExport. Unable to access the notebook_export widget!"
		<< endl;
	}
	else
	{
		gtk_widget_set_sensitive ( GTK_WIDGET( tmp ), FALSE );
	}

	GtkWidget * tmpframe
	= GTK_WIDGET ( lookup_widget ( common->getWidget(), "vbox_export_range" ) );
	if ( !tmpframe )
	{
		cerr << "Export::startExport. Unable to acces the frame_export_range widget!"
		<< endl;
	}
	else
	{
		gtk_widget_set_sensitive ( GTK_WIDGET( tmpframe ), FALSE );
	}


	/* Allow gtk to handle pending events - I am not sure this is strictly
	   needed, but there seems to be some lag from pressing buttons until
	   their state is updated.... this might help. */
	while ( gtk_events_pending() )
	{
		gtk_main_iteration();
	}

	/* Get a temporary playlist */
	PlayList playlist( *( common->getPlayList() ) );

	/* Set begin, end and every based on the range selections */
	int begin = 0;
	int end = 0;
	int every = 1;

	switch ( gtk_option_menu_get_history( menuRange ) )
	{
	case 0:
		begin = 0;
		end = common->getPlayList() ->GetNumFrames() - 1;
		break;
	case 1:
		begin = end = common->g_currentFrame;
		break;
	case 2:
		begin = gtk_spin_button_get_value_as_int( startSpin );
		end = gtk_spin_button_get_value_as_int( endSpin );
		break;
	}
	every = gtk_spin_button_get_value_as_int( everySpin );

	/* Reset the progess stuff */
	exportPage->resetProgress();

	/* Reset the time measuring stuff */
	startTime = 0.0;
	pauseTime = 0.0;

	/* Reset the statusmessage */
	common->setStatusBar( _( "Starting export" ) );

	/* Do the actual work */
	enum export_result status = doExport( &playlist, begin, end, every, preview );

	struct timeval tv;
	if ( 0 != gettimeofday( &tv, NULL ) )
	{
		cerr << ">>> Export::startExport - error calling gettimeofday?" << endl;
	}


	double now = tv.tv_sec + tv.tv_usec / 1000000.0;
	char buf[ 17 ];
	string message;
	//snprintf(buf, 512, "Export finished/failed/stopped - time: %s",
	//   formatSecs(buf1, 16, now - startTime));
	switch ( status )
	{
	case EXPORT_RESULT_SUCCESS:
		message = _( "Export finished - time: " );
		break;
	case EXPORT_RESULT_FAILURE:
		message = _( "Export failed - time: " );
		break;
	case EXPORT_RESULT_ABORT:
		message = _( "Export stopped - time: " );
		break;
	}
	message += formatSecs( buf, 16, now - startTime );
	common->setStatusBar( message.c_str() );


	/* Enable the common controls and buttons again */
	if ( tmp )
	{
		gtk_widget_set_sensitive ( GTK_WIDGET( tmp ), TRUE );
	}
	if ( tmpframe )
	{
		gtk_widget_set_sensitive ( GTK_WIDGET( tmpframe ), TRUE );
	}

	/* Set the progess to 100% - even in case of errors - the process have
	   stopped.... */
	exportPage->updateProgress( ( gfloat ) 1.0 );

	/* Set the buttons to allow a new export session.
	   export is available
	   stop is pressed, but not available
	   pause is not available
	*/
	exportPage->exportMutex = true;

	gtk_toggle_button_set_active( previewButton, FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET( previewButton ), supports_preview );

	gtk_toggle_button_set_active( exportButton, FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET( exportButton ), supports_export );

	gtk_toggle_button_set_active( stopButton, TRUE );
	gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), FALSE );

	gtk_toggle_button_set_active( pauseButton, FALSE );
	gtk_widget_set_sensitive( GTK_WIDGET( pauseButton ), FALSE );
	exportPage->exportMutex = false;

	/* We are no longer exporting at this point */
	exportPage->isExporting = false;

	while ( gtk_events_pending() )
	{
		gtk_main_iteration();
	}

}


/** ********************************************************************** **/
/** onActivate and onDeactivate */

/** By default we support the stop, and pause buttons, as
    well as EXPORT_SCENE_LIST
    stop is implicit (not defined in fact)
    You should override this method, if you do not support pause or export,
    do support preview or do not support the EXPORT_SCENE_LIST. */
gulong Export::onActivate()
{
	return EXPORT_SCENE_LIST | EXPORT_EXPORT | EXPORT_PAUSE;
}

/* I am not sure I have seen any pages do anything in deactivate */
gulong Export::onDeactivate()
{
	return 0;
}

/** ********************************************************************** **/
/** Wrappers for activate, deactivate, exports own little isolated
    state stuff
    This method toggles buttons and other components to fit with what is
    supported by the current export page.
    Also, it adjust the spin range on the from-to range export.
*/
gulong Export::activate()
{
	actions = onActivate();

	selectSection( gtk_option_menu_get_history( menuRange ) );
	/* Adjust on from-to range
	   start: */
	GtkAdjustment *adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( startSpin ) );
	adjust->lower = 0;
	adjust->upper = common->getPlayList() ->GetNumFrames() - 1;
	g_signal_emit_by_name( adjust, "changed" );
	if ( adjust->value > adjust->upper )
	{
		gtk_adjustment_set_value ( adjust, adjust->upper );
	}
	/* end: */
	adjust
	= gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( endSpin ) );
	adjust->lower = 0;
	adjust->upper = common->getPlayList() ->GetNumFrames() - 1;
	g_signal_emit_by_name( adjust, "changed" );
	if ( adjust->value > adjust->upper )
	{
		gtk_adjustment_set_value ( adjust, adjust->upper );
	}

	/* Enable/disable common buttons */
	bool supports_preview = actions & EXPORT_PREVIEW;
	bool supports_export = actions & EXPORT_EXPORT;
	// bool supports_pause   = tmp & EXPORT_PAUSE;
	/* Adjust to reasonable standard */
	exportPage->exportMutex = true;
	if ( exportPage->isExporting )
	{
		/* Not good, we should locked on a page when exporting */
		cerr << ">>> Export::activate() internal error: "
		<< "called with exportPage->isExporting!" << endl;
		/* Hmm. We should not change _anything_ when exporting. Really... */
		/* Only stop and pause should be available - if pausing, pause should
		   be pressed
		   TODO: we do not really know when we are previewing...
		*/
		/*
		  This code have been left here, until it is certain that the current
		  solution where the user can not change to another export page during
		  export, is acceptable to all parties.
		gtk_toggle_button_set_active( previewButton, FALSE );
		gtk_widget_set_sensitive( GTK_WIDGET(previewButton), supports_preview );

		gtk_toggle_button_set_active( exportButton, TRUE );
		gtk_widget_set_sensitive( GTK_WIDGET(exportButton), FALSE );

		gtk_toggle_button_set_active( stopButton, FALSE );
		gtk_widget_set_sensitive( GTK_WIDGET(stopButton), TRUE );

		gtk_toggle_button_set_active( pauseButton, exportPage->isPausing );
		gtk_widget_set_sensitive( GTK_WIDGET(pauseButton), supports_pause );
		*/
	}
	else
	{
		/* At most preview and export available */
		gtk_toggle_button_set_active( previewButton, FALSE );
		gtk_widget_set_sensitive( GTK_WIDGET( previewButton ), supports_preview );

		gtk_toggle_button_set_active( exportButton, FALSE );
		gtk_widget_set_sensitive( GTK_WIDGET( exportButton ), supports_export );

		gtk_toggle_button_set_active( stopButton, TRUE );
		gtk_widget_set_sensitive( GTK_WIDGET( stopButton ), FALSE );

		gtk_toggle_button_set_active( pauseButton, FALSE );
		gtk_widget_set_sensitive( GTK_WIDGET( pauseButton ), FALSE );
	}
	exportPage->exportMutex = false;


	/* TODO: Activate and deactive buttons as needed */
	cerr << ">> Export::activate()" << endl;
	if ( EXPORT_SCENE_LIST & actions )
	{
		// cerr << "Export::activate() - SCENE_LIST" << endl;
		return SCENE_LIST;
	}
	else
	{
		// cerr << "Export::activate() - NO SCENE_LIST" << endl;
		return 0;
	}
}
gulong Export::deactivate()
{
	cerr << "Export::deactivate()" << endl;
	if ( EXPORT_SCENE_LIST & onDeactivate() )
	{
		// cerr << "Export::deactivate() - SCENE_LIST" << endl;
		return SCENE_LIST;
	}
	else
	{
		// cerr << "Export::deactivate() - NO SCENE_LIST" << endl;
		return 0;
	}
}

/** ********************************************************************** **/
/** Empty methods - here for tracking during development */
void Export::start()
{
	cerr << ">>> Export::start()" << endl;
}
void Export::clean()
{
	cerr << ">>> Export::clean()" << endl;
}

/** ********************************************************************** **/
/** Put the scene begin and end frame numbers into spinners

    \param i the numerical index position of the scene in the playlist

    When the user selects a scene from the scenelist (on pages that supports
    this), the from-to controls are updated to reflect the scenes start and end
    points. This is handled here
*/
void Export::selectScene( int i )
{
	cerr << ">>> Export::selectScene() " << i << endl;
	/* Sanity check */
	if ( !( EXPORT_SCENE_LIST & actions ) )
	{
		cerr << ">>> Export::selectScene: internal consistency error. "
		<< "selectScene, even though page does not support scene list " << actions << endl;
		return ;
	}
	if ( exportPage->isExporting )
		return;
	
	/* Set the start selection spinner */
	GtkAdjustment *adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( startSpin ) );

	vector <int> scene = common->getPageEditor() ->GetScene();

	if ( scene.size( ) != 0 )
	{
		int begin = 0;
		int end = 0;

		begin = i == 0 ? 0 : scene[ i - 1 ];
		adjust->lower = 0;
		adjust->upper = scene[ scene.size() - 1 ] - 1;
		gtk_spin_button_set_value( GTK_SPIN_BUTTON( startSpin ), begin );
		g_signal_emit_by_name( adjust, "changed" );

		/* Set the end selection spinner */
		adjust = gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( endSpin ) );
		adjust->lower = 0;
		adjust->upper = scene[ scene.size() - 1 ] - 1;
		end = scene[ i ] - 1;
		gtk_spin_button_set_value( GTK_SPIN_BUTTON( endSpin ), end );
		g_signal_emit_by_name( adjust, "changed" );
		Frame &frame = *( GetFramePool( ) ->GetFrame( ) );
		FileHandler *media;
		common->getPlayList() ->GetMediaObject( begin, &media );
		common->getPlayList() ->GetFrame( begin, frame );
		GetFramePool( ) ->DoneWithFrame( &frame );
		common->moveToFrame( begin );
		common->showFrameMoreInfo( frame, media );
		common->setCurrentScene( begin );
	}

	/* Set the range button active */
	if ( gtk_option_menu_get_history( menuRange ) != 2 )
		gtk_option_menu_set_history( menuRange, 2 );
	else
		selectSection( 2 );
}

void Export::selectSection( int i )
{
	GtkWidget * label = lookup_widget( exportPage->getWidget(), "label177" );
	switch ( i )
	{
	case 2:
		gtk_widget_show( GTK_WIDGET( startSpin ) );
		gtk_widget_show( GTK_WIDGET( startEntry ) );
		gtk_widget_show( GTK_WIDGET( endSpin ) );
		gtk_widget_show( GTK_WIDGET( endEntry ) );
		gtk_widget_show( GTK_WIDGET( label ) );
		common->getPageExport()->timeFormatChanged();
		break;

	default:
		gtk_widget_hide( GTK_WIDGET( startSpin ) );
		gtk_widget_hide( GTK_WIDGET( startEntry ) );
		gtk_widget_hide( GTK_WIDGET( endSpin ) );
		gtk_widget_hide( GTK_WIDGET( endEntry ) );
		gtk_widget_hide( GTK_WIDGET( label ) );
		break;
	}
}

/** ********************************************************************** **/
/** Fallbacks for children that does not overwrite. */
void Export::stopExport()
{
	cerr << ">>> Export::stopExport()" << endl;
}
void Export::pauseExport()
{
	cerr << ">>> Export::pauseExport()" << endl;
}

/**  When the user changes the start or end of the from-to export, set active
     the from-to radiobutton.
     Also, make sure that start is less than end. This is only used to
     provide a sane GUI

     \param widget the widget that generated the event (I hope...)
*/
void Export::onRangeChange( GtkSpinButton *widget )
{
	/* Make sure that start <= end */
	GtkAdjustment * startAdjust
	= gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( startSpin ) );
	GtkAdjustment *endAdjust
	= gtk_spin_button_get_adjustment( GTK_SPIN_BUTTON( endSpin ) );

	if ( widget == startSpin )
	{
		if ( endAdjust->value < startAdjust->value )
		{
			gtk_adjustment_set_value ( endAdjust, startAdjust->value );
		}
	}
	else
	{
		if ( widget == endSpin )
		{
			if ( startAdjust->value > endAdjust->value )
			{
				gtk_adjustment_set_value ( startAdjust, endAdjust->value );
			}
		}
		else
		{
			cerr << ">>> Export::onRangeChange: internal error: widget passed is unknown"
			<< endl;
		}
	}
}


/** ********************************************************************** **/
/** Do the actual pause. This one is meant to be overwritten if needed,
    say if you need to perform some actions before entering a pause */
void Export::doPause()
{
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec = 1000 * 1000 * 20; /* 20 ms + sched overhead */
	while ( exportPage->isPausing )
	{
		// cerr << ">>> Export::doPause()" << endl;
		while ( gtk_events_pending() )
		{
			gtk_main_iteration();
		}
		/* Lets try not to hog the CPU */
		nanosleep( &ts, NULL );
	}
}

/** Format seconds */
char * Export::formatSecs( char * buf, int size, double seconds )
{
	int _seconds = ( int ) rint( seconds );
	int secs = _seconds % 60;
	int mins = ( _seconds / 60 ) % 60;
	int hours = ( _seconds / 3600 );
	if ( hours > 0 )
	{
		snprintf( buf, size, "%i:%02i:%02i", hours, mins, secs );
	}
	else
	{
		if ( mins > 0 )
		{
			snprintf( buf, size, "%i:%02i", mins, secs );
		}
		else
		{
			snprintf( buf, size, "%i", secs );
		}
	}
	return buf;
}

/** innerLoopUpdate - see header file. */
void Export::innerLoopUpdate( int progress, int begin, int end, int every,
		int currentFrame, const char* activity )
{
	char buf[ 512 ];
	struct timeval tv;
	if ( 0 != gettimeofday( &tv, NULL ) )
	{
		cerr << ">>> Export::innerLoopUpdate: Error calling gettimeofday?" << endl;
	}
	double now = tv.tv_sec + tv.tv_usec / 1000000.0;
	gfloat com_ratio = ( gfloat ) ( progress - begin ) / ( gfloat ) ( end + 1 - begin ) ;
	if ( currentFrame == -1 )
		currentFrame = progress;
	/* Figure out how much time we have spend */
	if ( 0 == startTime )
	{
		/* First time */
		startTime = now;
		nextUpdateTime = now - 1;
		snprintf( buf, 512, _( "%s frame %i." ), activity, currentFrame );
	}
	else
	{
		/* Not first time
		   We do not care to use difftime on this... */
		double time_so_far = now - startTime - pauseTime;
		double total_est = time_so_far / com_ratio;
		/* Write the time values into buffers */
		char buf1[ 16 ];
		char buf2[ 16 ];
		char buf3[ 16 ];
		snprintf( buf, 512, _( "%s frame %i. Time  used: %s,  estimated: %s,  left: %s" ),
		          activity,
		          currentFrame + 1,
		          formatSecs( buf1, 16, time_so_far ),
		          formatSecs( buf2, 16, total_est ),
		          formatSecs( buf3, 16, total_est - time_so_far ) );
	}

	/* Update status message
	   TODO: Do filesystem checks first....
	*/
	if ( now > nextUpdateTime )
	{
		common->setStatusBar( buf );
		nextUpdateTime = now + 0.25;
	}

	/* Update progressbar - assuming currentFrame have yet to be exported
	   Updating the progressbar will handle any pending events. */
	exportPage->updateProgress( com_ratio );

	/* Check pause - make sure we ignore time paused in export. */
	if ( EXPORT_PAUSE & actions && exportPage->isPausing )
	{
		doPause();
		if ( 0 != gettimeofday( &tv, NULL ) )
		{
			cerr << ">>> Export::innerLoopUpdate: Error calling gettimeofday?" << endl;
		}
		double foo = tv.tv_sec + tv.tv_usec / 1000000.0;
		pauseTime += ( foo - now );
	}
}

double Export::calculateAdjustedRate( PlayList* playlist, double outputRate, int begin, int end, int every )
{
	Frame& frame = *GetFramePool()->GetFrame();
	playlist->GetFrame( begin, frame );
	double seconds = double( end - begin + 1 ) / frame.GetFrameRate();
	int64_t idealSamples = int64_t( seconds * outputRate + 0.5 );
	int64_t actualSamples = 0;
	double adjustedRate = 0.0;
	struct timeval tv;
	if ( 0 != gettimeofday( &tv, NULL ) )
	{
		cerr << ">>> Export::calculateAdjustedRate: Error calling gettimeofday?" << endl;
	}
	double now = tv.tv_sec + tv.tv_usec / 1000000.0;
	double nextUpdateTime = now - 1.0;
	char buf[ 512 ];
	int n = -1, frameNum = 0;
	AsyncAudioResample<int16_ne_t,int16_le_t>* resampler = new AsyncAudioResample<int16_ne_t,int16_le_t>(
		AUDIO_RESAMPLE_SRC_SINC_FASTEST, playlist, outputRate, begin, end, every );

	// Determine the actual number of samples
	while ( exportPage->isExporting && n != 0 )
	{
		n = resampler->Process( outputRate, frame.CalculateNumberSamples( int(outputRate), frameNum++ ) );
		actualSamples += n;

		if ( 0 == gettimeofday( &tv, NULL ) )
		{
			now = tv.tv_sec + tv.tv_usec / 1000000.0;
			if ( now > nextUpdateTime )
			{
				snprintf( buf, 512, "%s (%02d%%)", _("Locking audio"),
					int( 100.0 * frameNum / (end - begin + 1) ) );
				common->setStatusBar( buf );
				nextUpdateTime = now + 1.0;
			}
		}
		exportPage->updateProgress( -1 );

		/* Check pause - make sure we ignore time paused in export. */
		if ( EXPORT_PAUSE & actions && exportPage->isPausing )
		{
			doPause();
		}
	}
	GetFramePool()->DoneWithFrame( &frame );

	if ( exportPage->isExporting )
	{
		// If there are less actual samples than ideal then increase the
		// rate to make resampler increase the number of samples.
// 		cerr << "calcAdjustedRate secs " << seconds << " ideal " << idealSamples << " actual " << actualSamples;
		adjustedRate = outputRate + ( idealSamples - actualSamples ) / seconds;
	}

	if ( 0 != gettimeofday( &tv, NULL ) )
		cerr << ">>> Export::innerLoopUpdate: Error calling gettimeofday?" << endl;
	double foo = tv.tv_sec + tv.tv_usec / 1000000.0;
	pauseTime += ( foo - now );

	cerr << ">>> output rate is " << outputRate << ", adjusted rate is " << adjustedRate << endl;

	return adjustedRate;
}

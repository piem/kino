/*
* framedisplayer.cc -- sends frame objects to Displayer and audio device
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include <pthread.h>

#include "framedisplayer.h"
#include "preferences.h"
#include "frame.h"

FrameDisplayer::FrameDisplayer() : 
	displayer( NULL ), 
	resampler( NULL ), 
	audio_device( NULL ),
	audio_device_avail( FALSE ),
	prevIsWide( false )
{
	for ( gint i = 0; i < 4; i++ )
		audio_buffers[ i ] = ( gint16 * ) malloc( 2 * DV_AUDIO_MAX_SAMPLES * sizeof( gint16 ) );
}


FrameDisplayer::~FrameDisplayer()
{
	delete displayer;
	for ( gint i = 0; i < 4; i++ )
		free( audio_buffers[ i ] );
	CloseSound();
}


void FrameDisplayer::CloseSound()
{
	if ( audio_device_avail )
	{
		kino_sound_close( audio_device );
		audio_device_avail = FALSE;
		audio_device = NULL;
		delete resampler;
		resampler = NULL;
	}
}


void FrameDisplayer::PutSound( Frame &frame )
{
	static Preferences & prefs = Preferences::getInstance();
	if ( prefs.enableAudio )
	{
		if ( audio_device == NULL )
			audio_device = kino_sound_new();
		if ( audio_device != NULL )
		{
			if ( !audio_device_avail && 
					( audio_sampling_rate = kino_sound_init( frame.decoder->audio, audio_device, prefs.audioDevice ) ) != 0 )
			{
				audio_device_avail = TRUE;
			}
			if ( audio_device_avail )
			{
				if ( resampler == NULL )
					resampler = AudioResampleFactory<int16_ne_t,int16_ne_t>::createAudioResample(
									AUDIO_RESAMPLE_SRC_SINC_FASTEST, audio_sampling_rate );
				resampler->Resample( frame );
				if ( resampler->size )
					kino_sound_player( audio_device, resampler->output, resampler->size );
			}
		}
	}
}

void FrameDisplayer::Put( Frame &frame, GtkWidget *drawingarea, gboolean no_audio )
{
	if ( frame.GetWidth() > 0 && frame.GetHeight() > 0 )
	{
		if ( displayer == NULL )
			displayer = FindDisplayer::getDisplayer( drawingarea, frame );
		if ( displayer != NULL )
		{
			DisplayerInput input = DISPLAY_NONE;
			switch ( displayer->format() )
			{
			case DISPLAY_YUV:
				frame.ExtractPreviewYUV( pixels );
				input = DISPLAY_YUV;
				break;
			case DISPLAY_RGB:
			default:
				frame.ExtractPreviewRGB( pixels );
				input = DISPLAY_RGB;
				break;
			}
			if ( !no_audio )
				PutSound( frame );
			if ( prevIsWide != frame.IsWide() )
			{
				prevIsWide = frame.IsWide();
				// The numbers here do not affect the actual display size, only its aspect
				gtk_widget_set_size_request( drawingarea, 320, frame.IsWide() ? 180 : 240 );
			}
			displayer->put( input, pixels, frame.GetWidth(), frame.GetHeight() );
		}
	}
}

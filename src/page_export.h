/*
* page_export.h Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EXPORT_H
#define _PAGE_EXPORT_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "kino_common.h"
#include "page.h"
#include "export.h"

// Forward references to page classes.
class Export;
class Export1394;
class ExportAVI;
class ExportStills;
class ExportAudio;
class ExportMJPEG;
class ExportPipe;

/** This class controls the Export notebook page.
*/

class PageExport : public Page
{
private:
	KinoCommon	*common;
	GtkNotebook	*notebook;
	GtkProgressBar	*progressBar;
	int	currentMode;
	Export1394	*export1394;
	ExportAVI	*exportAVI;
	ExportStills	*exportStills;
	ExportAudio	*exportAudio;
	ExportMJPEG	*exportMJPEG;
	ExportPipe	*exportPipe;

public:
	/* isExporting is true, when an export OR an preview is
	   underway */
	bool	isExporting;
	/* True when pausing */
	bool isPausing;
	/* True when changes to the export buttons are beeing
	   carried out  */
	bool	exportMutex;


	PageExport( KinoCommon *common );
	virtual ~PageExport();
	gulong activate();
	void start();
	void clean();
	GtkWidget *getWidget();

	// Mode related functions
	void changeModeRequest( int mode );
	void setCurrentMode( int mode );
	Export *getCurrentPage();
	Export *getPage( int mode );

	// commands from the page interface
	gboolean processKeyboard( GdkEventKey *event );
	gboolean processCommand( const char *cmd );
	void selectScene( int );

	// commands defined by this interface
	void startExport();
	void previewExport();
	void stopExport();
	void pauseExport();
	void updateProgress( gfloat val );
	void resetProgress();
	void timeFormatChanged();
	std::string getHelpPage()
	{
		return "export";
	}

};

#endif

## Process this file with automake to produce Makefile.in

SUBDIRS = cell-renderers timfx dvtitler kinoplus

MAINTAINERCLEANFILES = Makefile.in
EXTRA_DIST = kino.1 cmd_ref.c kino2raw.1 ffmpeg-kino.1

bin_PROGRAMS = kino
man_MANS = kino.1 kino2raw.1 $(FFMPEG_MAN)

pkginclude_HEADERS = image_create.h image_filters.h image_transitions.h \
                     audio_filters.h audio_transitions.h playlist.h frame.h \
                     kino_extra.h endian_types.h smiltime.h \
                     kino_plugin_types.h kino_plugin_utility.h time_map.h

kino_SOURCES = \
	main.c \
	support.c support.h \
	callbacks.c callbacks.h \
	message.cc message.h \
	preferences.cc preferences.h \
	preferences_dialog.cc preferences_dialog.h \
	playlist.cc playlist.h \
	filehandler.cc filehandler.h \
	riff.cc riff.h avi.cc avi.h frame.cc frame.h error.cc error.h endian_types.h \
	ieee1394io.cc ieee1394io.h \
	framedisplayer.cc framedisplayer.h \
	oss.c oss.h \
	jogshuttle.cc jogshuttle.h \
	mediactrl.c mediactrl.h\
	kino_common.cc kino_common.h \
	page.h \
	page_editor.cc page_editor.h \
	page_capture.cc page_capture.h \
	page_timeline.cc page_timeline.h \
	page_export.cc page_export.h \
	page_bttv.cc page_bttv.h \
	export.h export.cc \
	page_export_1394.cc page_export_1394.h \
	page_export_avi.cc page_export_avi.h \
	page_export_stills.cc page_export_stills.h \
	page_export_audio.cc page_export_audio.h \
	page_export_mjpeg.cc page_export_mjpeg.h \
	page_export_pipe.cc page_export_pipe.h \
	page_trim.cc page_trim.h \
	page_magick.cc page_magick.h \
	page_undefined.h \
	commands.cc commands.h \
	page_bttv.h \
	v4l.cc v4l.h \
	displayer.cc displayer.h \
	gtkenhancedscale.c gtkenhancedscale.h \
	kino_av_pipe.cc kino_av_pipe.h \
	image_create.cc image_create.h \
	image_filters.cc image_filters.h  \
	image_transitions.cc image_transitions.h  \
	audio_filters.cc audio_filters.h \
	audio_transitions.cc audio_transitions.h \
	storyboard.cc storyboard.h \
	dv1394.h \
	stringutils.cc stringutils.h \
	smiltime.cc smiltime.h \
	rwpipe.cc rwpipe.h \
	kino_extra.h

EXTRA_kino_SOURCES = cmd_ref.c

kino_LDADD = \
	$(LIBDV_LIBS) \
	$(LIBRAW1394_LIBS) \
	$(LIBAVC1394_LIBS) \
	$(LIBIEC61883_LIBS) \
	$(LIBQUICKTIME_LIBS) \
	$(ALSA_LIBS) \
	$(LIBXML2_LIBS) \
	$(GTK2_LIBS) \
	$(AVCODEC_LIBS) \
	$(SWSCALE_LIBS) \
	$(SRC_LIBS) \
	cell-renderers/libcellrenderers.a

kino_LDFLAGS = -export-dynamic

INCLUDES = -Wall \
	$(LIBDV_CFLAGS) \
	$(LIBRAW1394_CFLAGS) \
	$(LIBAVC1394_CFLAGS) \
	$(LIBIEC61883_CFLAGS) \
	$(LIBQUICKTIME_CFLAGS) \
	$(ALSA_CFLAGS) \
	$(LIBXML2_CFLAGS) \
	$(GTK2_CFLAGS) \
	$(AVCODEC_CFLAGS) \
	$(SWSCALE_CFLAGS) \
	$(SRC_CFLAGS) \
	-D_FILE_OFFSET_BITS=64 \
	-DKINO_PLUGINDIR=\""$(pkglibdir)-gtk2"\" \
	-DDATADIR=\""$(datadir)"\" \
	-D_REENTRANT \
	-D__STDC_CONSTANT_MACROS

# noinst_PROGRAMS = riffdump
# riffdump_SOURCES = error.cc error.h riffdump.cc \
# 	avi.h riff.h avi.cc riff.cc \
# 	frame.h frame.cc \
# 	filehandler.cc filehandler.h \
# 	playlist.cc playlist.h \
# 	smiltime.cc smiltime.h \
# 	stringutils.cc stringutils.h \
# 	preferences.cc preferences.h
# riffdump_LDADD = $(LIBDV_LIBS) $(GTK2_LIBS) $(AVCODEC_LIBS) $(SRC_LIBS)

# a C++ formatter

ASTYLEFLAGS = -c -t -b -P

pretty:
	astyle $(ASTYLEFLAGS) $(kino_SOURCES)

install-exec-hook:
	ln -sf kino $(DESTDIR)$(bindir)/kino2raw

uninstall-hook:
	rm $(DESTDIR)$(bindir)/kino2raw

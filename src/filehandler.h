/*
* filehandler.h
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _FILEHANDLER_H
#define _FILEHANDLER_H

// enum { PAL_FORMAT, NTSC_FORMAT, AVI_DV1_FORMAT, AVI_DV2_FORMAT, QT_FORMAT, RAW_FORMAT, TEST_FORMAT, UNDEFINED };

#include <vector>
using std::vector;

#include <string>
using std::string;

#include "preferences.h"
#include "frame.h"
#include "riff.h"
#include "avi.h"
#include <sys/types.h>
#include <stdint.h>

enum FileCaptureMode {
    CAPTURE_IGNORE,
    CAPTURE_FRAME_APPEND,
    CAPTURE_FRAME_INSERT,
    CAPTURE_MOVIE_APPEND
};

class FileTracker
{
protected:
	FileTracker();
	~FileTracker();
public:
	static FileTracker &GetInstance( );
	void SetMode( FileCaptureMode );
	FileCaptureMode GetMode( );
	unsigned int Size();
	char *Get( int );
	void Add( const char * );
	void Clear( );
private:
	static FileTracker *instance;
	vector <char *> list;
	FileCaptureMode mode;
};

class FileHandler
{
public:

	FileHandler();
	virtual ~FileHandler();

	virtual bool GetAutoSplit() const;
	virtual bool GetTimeStamp() const;
	virtual string GetBaseName() const;
	virtual string GetExtension() const;
	virtual int GetMaxFrameCount() const;
	virtual off_t GetMaxFileSize() const;
	virtual off_t GetFileSize() = 0;
	virtual int GetTotalFrames() = 0;
	virtual string GetFilename() const;

	virtual void SetAutoSplit( bool );
	virtual void SetTimeStamp( bool );
	virtual void SetBaseName( const string& base );
	virtual void SetMaxFrameCount( int );
	virtual void SetEveryNthFrame( int );
	virtual void SetMaxFileSize( off_t );
	virtual void SetSampleFrame( const Frame& sample );

	virtual bool WriteFrame( const Frame& frame );
	virtual bool FileIsOpen() = 0;
	virtual bool Create( const string& filename ) = 0;
	virtual int Write( const Frame& frame ) = 0;
	virtual int Close() = 0;
	virtual bool Done( void );

	virtual bool Open( const char *s ) = 0;
	virtual int GetFrame( Frame &frame, int frameNum ) = 0;
	int GetFramesWritten() const
	{
		return framesWritten;
	}

protected:
	bool done;
	bool autoSplit;
	bool timeStamp;
	int maxFrameCount;
	int framesWritten;
	int everyNthFrame;
	int framesToSkip;
	off_t maxFileSize;
	string base;
	string extension;
	string filename;
};


class RawHandler: public FileHandler
{
public:
	int fd;

	RawHandler();
	~RawHandler();

	bool FileIsOpen();
	bool Create( const string& filename );
	int Write( const Frame& frame );
	int Close();
	off_t GetFileSize();
	int GetTotalFrames();
	bool Open( const char *s );
	int GetFrame( Frame &frame, int frameNum );
private:
	int numBlocks;
};


class AVIHandler: public FileHandler
{
public:
	AVIHandler( int format = AVI_DV1_FORMAT );
	~AVIHandler();

	void SetSampleFrame( const Frame& sample );
	bool FileIsOpen();
	bool Create( const string& filename );
	int Write( const Frame& frame );
	int Close();
	off_t GetFileSize();
	int GetTotalFrames();
	bool Open( const char *s );
	int GetFrame( Frame &frame, int frameNum );
	int16_t* SeekAudioFrame( Frame& frame, int frameNum, int& availableSize );
	bool GetOpenDML() const;
	void SetOpenDML( bool );
	int GetFormat() const
	{
		return aviFormat;
	}

protected:
	AVIFile *avi;
	int aviFormat;
	AudioInfo audioInfo;
	VideoInfo videoInfo;
	bool isOpenDML;
	DVINFO dvinfo;
	FOURCC	fccHandler;
	int channels;
	bool isFullyInitialized;
	int16_t *audioBuffer;
	int16_t *audioChannels[ 4 ];
	bool isInterleave1to1;
	off_t audioOffset;
	int audioBufferSize;
};


#ifdef HAVE_LIBQUICKTIME
#include <quicktime.h>

class QtHandler: public FileHandler
{
public:
	QtHandler();
	~QtHandler();

	bool FileIsOpen();
	bool Create( const string& filename );
	int Write( const Frame& frame );
	int Close();
	off_t GetFileSize();
	int GetTotalFrames();
	bool Open( const char *s );
	int GetFrame( Frame &frame, int frameNum );
	void AllocateAudioBuffers();

private:
	quicktime_t *fd;
	long samplingRate;
	int samplesPerBuffer;
	int channels;
	bool isFullyInitialized;
	unsigned int audioBufferSize;
	int16_t *audioBuffer;
	short int** audioChannelBuffer;

	void Init();
	inline void DeinterlaceStereo16( void* pInput, int iBytes, void* pLOutput, void* pROutput );

};
#endif

#endif

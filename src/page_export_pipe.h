/*
* page_export_pipe.h - DV Pipe exports
* Copyright (C) 2003 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2003-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EXPORT_PIPE_H
#define _PAGE_EXPORT_PIPE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vector>
#include <string>

#include <gtk/gtk.h>

#include "export.h"
#include "page_export.h"
#include "kino_common.h"
#include "playlist.h"

class DVPipeTool;

/** This class represents the DV Pipe notebook page.
*/

class ExportPipe : public Export
{
private:
	vector < DVPipeTool * > m_tools;
	char playlistTempName[ 20 ];

public:
	ExportPipe( PageExport*, KinoCommon* );
	virtual ~ExportPipe( );
	void start( );
	void clean( );
	gulong onActivate( );
	void loadTools( string directory );
	void activateTool( int tool );
	void selectTool( int tool );

private:
	enum export_result doExport( PlayList * playlist, int begin, int end, int every, bool preview );
};

#endif

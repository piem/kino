/*
* page_export_mjpeg.cc -- Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
using std::cerr;
using std::endl;

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>

#include "page_export_mjpeg.h"
#include "kino_av_pipe.h"
#include "preferences.h"
#include "kino_common.h"
#include "frame.h"
#include "page_editor.h"
#include "message.h"
#include "rwpipe.h"

static void
on_optionmenu_export_mjpeg_format_changed( GtkOptionMenu *widget, gpointer user_data )
{
	int i = gtk_option_menu_get_history( widget );
	GtkWidget* vbox = lookup_widget( GTK_WIDGET( widget ), "vbox_export_mjpeg_dvd_options" );
	if ( i == 6 )
		gtk_widget_show( vbox );
	else
		gtk_widget_hide( vbox );
}

class DVDTool : protected RWPipe
{
public:
	string m_command;
	string m_description;
	bool m_active;

	DVDTool( string command ) :
			m_command( command ),
			m_description( "" ),
			m_active( false )
	{
		load( command );
	}

	void load( string command )
	{
		if ( run( command ) )
		{
			char temp[ 10240 ];
			while ( readLine( temp, 10240 ) > 0 )
			{
				char * ptr = strchr( temp, ':' );
				if ( ptr != NULL )
				{
					*ptr = '\0';
					ptr += 2;

					if ( !strcmp( temp, "Title" ) )
						m_description = ptr;
					else if ( !strcmp( temp, "Status" ) )
						m_active = !strcmp( ptr, "Active" );
					else
						fprintf( stderr, "Unrecognised %s: %s\n", temp, ptr );
				}
			}
			stop( );
		}
	}

	bool execute( const string& dvdauthorFile, const string& outputDirectory )
	{
		gchar *command;
		bool result;

		// Construct the command
		command = g_strdup_printf( "\"%s\" \"%s\" \"%s\"", m_command.c_str( ),
		         dvdauthorFile.c_str( ), outputDirectory.c_str() );

		result = system( command );
		g_free( command );
		return result;
	}

	void close( )
	{
		stop( );
	}

	static bool compare( const DVDTool* x, const DVDTool* y )
	{
		return ( x->m_description < y->m_description );
	}
};


/** Constructor for page.

\param _exportPage page to which this page belongs
\param _common KinoCommon object
*/

ExportMJPEG::ExportMJPEG( PageExport *_exportPage, KinoCommon *_common ) :
		Export( _exportPage, _common ),
		prefs( Preferences::getInstance() ),
		hasYuvDeinterlace( false ),
		isAvailable( true )
{
	cerr << "> Creating ExportMJPEG Page" << endl;

	// Load the extensible dvdauthor tools
	loadTools( DATADIR "/kino/scripts/dvdauthor" );
	string alternate_dir = string( getenv( "HOME" ) ) + string( "/kino/dvdauthor" );
	loadTools( alternate_dir );

	char *kinoHome=getenv("KINO_HOME");

	if(kinoHome)
	{
		string alternate_dir = string( kinoHome ) + string( "/dvdauthor" );
		loadTools( alternate_dir );
	}

	activateTool( prefs.exportMjpegDvdTool );

	/* Get pointers to own controls */
	fileEntry
	= GTK_ENTRY( lookup_widget( common->getWidget(), "entry_export_mjpeg_file" ) );

	mjpegFormat
	= GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
	                                      "optionmenu_export_mjpeg_format" ) ) ) );
	deinterlaceMenu
	= GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( common->getWidget(),
	                                      "optionmenu_export_mjpeg_deinterlace" ) ) ) );
	mjpegVideo
	= GTK_ENTRY( lookup_widget( exportPage->getWidget(),
	                            "entry_mjpeg_video" ) );
	mjpegAudio
	= GTK_ENTRY( lookup_widget( exportPage->getWidget(),
	                            "entry_mjpeg_audio" ) );
	mjpegMultiplexer
	= GTK_ENTRY( lookup_widget( exportPage->getWidget(),
	                            "entry_mjpeg_multiplexer" ) );
	splitCheck
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
	                                    "checkbutton_export_mjpeg_split" ) );
	cleanupCheck
	= GTK_TOGGLE_BUTTON( lookup_widget( common->getWidget(),
					"checkbutton_export_mjpeg_cleanup" ) );
	mjpegAspect
	= GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
	                                      "optionmenu_export_aspect_ratio" ) ) ) );

	cerr << "> Initializing MJPEG Export Page settings from Preferences" << endl;
	gtk_option_menu_set_history( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
								"optionmenu_export_mjpeg_format" ) ), prefs.exportMjpegFormat );
	GtkWidget* vbox = lookup_widget( GTK_WIDGET( exportPage->getWidget() ), "vbox_export_mjpeg_dvd_options" );
	if ( prefs.exportMjpegFormat == 6 )
		gtk_widget_show( vbox );
	else
		gtk_widget_hide( vbox );
	gtk_option_menu_set_history( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
								"optionmenu_export_mjpeg_deinterlace" ) ), prefs.exportMjpegDeinterlace );
	gtk_option_menu_set_history( GTK_OPTION_MENU( lookup_widget( exportPage->getWidget(),
								"optionmenu_export_aspect_ratio" ) ) , prefs.exportMjpegAspect );
	gtk_toggle_button_set_active( splitCheck, prefs.exportMjpegSceneSplit );
	gtk_toggle_button_set_active( cleanupCheck, prefs.exportMjpegCleanup );
	gtk_entry_set_text( mjpegVideo, prefs.exportMjpegVideoPipe );
	gtk_entry_set_text( mjpegAudio, prefs.exportMjpegAudioPipe );
	gtk_entry_set_text( mjpegMultiplexer, prefs.exportMjpegMultiplex );

	if ( system( "yuvdeinterlace -h 2> /dev/null" ) == 0 )
	{
		hasYuvDeinterlace = true;
		GtkWidget *widget = gtk_menu_item_new_with_label( _("YUV Deinterlace") );
		gtk_widget_show( widget );
		gtk_menu_append( deinterlaceMenu, widget );
		widget = gtk_menu_item_new_with_label( _("YUV Deinterlace Film-like") );
		gtk_widget_show( widget );
		gtk_menu_append( deinterlaceMenu, widget );
	}
	else if ( system( "yuvdenoise -h 2> /dev/null" ) == 0 )
	{
		GtkWidget *widget = gtk_menu_item_new_with_label( _("YUV Denoise (Fast)") );
		gtk_widget_show( widget );
		gtk_menu_append( deinterlaceMenu, widget );
		widget = gtk_menu_item_new_with_label( _("YUV Denoise (Slow)") );
		gtk_widget_show( widget );
		gtk_menu_append( deinterlaceMenu, widget );
	}
	gtk_option_menu_set_menu( GTK_OPTION_MENU( lookup_widget( common->getWidget(),
		"optionmenu_export_mjpeg_deinterlace" ) ), GTK_WIDGET( deinterlaceMenu ) );
	g_signal_connect ( G_OBJECT( lookup_widget( common->getWidget(),
		"optionmenu_export_mjpeg_format" ) ), "changed",
		G_CALLBACK ( on_optionmenu_export_mjpeg_format_changed ), this );
}

/** Destructor for page.
 */

ExportMJPEG::~ExportMJPEG()
{
	cerr << "> Destroying ExportMJPEG Page" << endl;
}

void ExportMJPEG::start()
{
	// Show DVD options if applicable
	GtkWidget* vbox = lookup_widget(  common->getWidget(), "vbox_export_mjpeg_dvd_options" );
	if ( prefs.exportMjpegFormat == 6 )
		gtk_widget_show( vbox );
	else
		gtk_widget_hide( vbox );
	
	// Disable UI if mjpegtools not installed.
	vbox = lookup_widget(  common->getWidget(), "vbox_export_mjpeg" );
	const char* errorMessage = _( "Failed to execute %s. MPEG export requires a complete installation of mjpegtools." );
	if ( system( "which mpeg2enc > /dev/null 2> /dev/null" ) )
	{
		gtk_widget_set_sensitive( vbox, FALSE );
		common->setStatusBar( errorMessage, "mpeg2enc" );
		isAvailable = false;
		return;
	}
	if ( system( "which mp2enc > /dev/null 2> /dev/null" ) )
	{
		gtk_widget_set_sensitive( vbox, FALSE );
		common->setStatusBar( errorMessage, "mp2enc" );
		isAvailable = false;
		return;
	}
	if ( system( "which mplex > /dev/null 2> /dev/null" ) )
	{
		gtk_widget_set_sensitive( vbox, FALSE );
		common->setStatusBar( errorMessage, "mplex" );
		isAvailable = false;
		return;
	}
	
	// Otherwise, enable UI
	isAvailable = true;
	gtk_widget_set_sensitive( vbox, TRUE );
}

/** Define active widgets.
    If mjpegtools are not installed, do not support any common functions.
 */

gulong ExportMJPEG::onActivate()
{
	if ( isAvailable )
		return EXPORT_EXPORT | EXPORT_PAUSE | EXPORT_SCENE_LIST;
	return 0;
}

/** start exporting MJPEG
 */
enum export_result
ExportMJPEG::doExport( PlayList * playlist, int begin, int end, int every,
                       bool preview )
{
	cerr << ">>> ExportMJPEG::startExport" << endl;

	// Set defaults on own controls
	prefs.exportMjpegFormat = g_list_index ( GTK_MENU_SHELL ( mjpegFormat ) ->children, gtk_menu_get_active( mjpegFormat ) );
	prefs.exportMjpegDeinterlace = g_list_index ( GTK_MENU_SHELL ( deinterlaceMenu ) ->children, gtk_menu_get_active( deinterlaceMenu ) );
	prefs.exportMjpegAspect = g_list_index ( GTK_MENU_SHELL ( mjpegAspect ) ->children, gtk_menu_get_active( mjpegAspect ) );
	prefs.exportMjpegSceneSplit = gtk_toggle_button_get_active( splitCheck );
	prefs.exportMjpegCleanup = gtk_toggle_button_get_active( cleanupCheck );
	GtkWidget* widget = lookup_widget( common->getWidget(), "optionmenu_dvd_tools" );
	widget = gtk_option_menu_get_menu( GTK_OPTION_MENU( widget ) );
	prefs.exportMjpegDvdTool = g_list_index ( GTK_MENU_SHELL( widget )->children, gtk_menu_get_active( GTK_MENU( widget ) ) );

	// TODO: Fix this buffer stuff.
	gchar *filename;
	enum export_result status = EXPORT_RESULT_SUCCESS;

	filename = g_strdup( gtk_entry_get_text( fileEntry ) );

	if ( !( strcmp( filename, "" ) && strpbrk( filename, "\'" ) == NULL ) )
	{
		modal_message( _( "You must enter a filename which doesn't contain quotes." ) );
		g_free( filename );
		return EXPORT_RESULT_FAILURE;
	}
	unsigned char *pixels = new unsigned char[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];
	Frame* frame = GetFramePool()->GetFrame();

	// Get a sample frame to obtain recording info
	// TODO: Sample frames are problematic...
	playlist->GetFrame( begin, *frame );
	frame->decoder->quality = DV_QUALITY_BEST;

	// Get all video and audio info required
	int width = frame->GetWidth();
	int height = frame->GetHeight();
	bool isPAL = frame->IsPAL();
	bool isWide = frame->IsWide();
	AudioInfo info;
	frame->GetAudioInfo( info );
	short channels = info.channels;
	int frequency = info.frequency;

	// vars for collecting audio and video pipe commands
	bool split = false;
	bool author = false;
	bool Usercleanup = true;
	kino_video_pipe type = PIPE_VIDEO_MJPEG;
	gchar *audiopipe = NULL;
	gchar *videopipe = NULL;
	gchar *multiplex = NULL;
	gchar *cleanup = NULL;

	// list to collect the names of the generated mpeg2 files
	// this collection only proceeds if we are authoring an
	// xml file for use with dvdauthor
	std::vector<std::string> nameList;

	// MPEG and DIVX translations to commands
	// What started off as a simple addition is getting pretty damn messy :-/ - ho hum
	// Essentially the important things to remember are that only the 5 variables above
	// are populated in the following kludge... (this should move into seprate methods)
	const char *videopipeInput = "";
	int videoformat = 0;
	int deinterlaceRequest = 0;
	const char *audioencode = "";
	const char *multiplexer = "";
	int aspectRatio = 0;
	bool isY4Minterlaced = true;

	videopipeInput = gtk_entry_get_text( mjpegVideo );
	strncpy( prefs.exportMjpegVideoPipe, videopipeInput, sizeof(prefs.exportMjpegVideoPipe) );
	GtkWidget *active_item = gtk_menu_get_active( mjpegFormat );
	videoformat = g_list_index ( GTK_MENU_SHELL ( mjpegFormat ) ->children, active_item );
	audioencode = gtk_entry_get_text( mjpegAudio );
	strncpy( prefs.exportMjpegAudioPipe, audioencode, sizeof(prefs.exportMjpegAudioPipe) );
	multiplexer = gtk_entry_get_text( mjpegMultiplexer );
	strncpy( prefs.exportMjpegMultiplex, multiplexer, sizeof(prefs.exportMjpegMultiplex) );
	active_item = gtk_menu_get_active( deinterlaceMenu );
	deinterlaceRequest = g_list_index ( GTK_MENU_SHELL ( deinterlaceMenu ) ->children, active_item );
	split = gtk_toggle_button_get_active( splitCheck );
	Usercleanup = gtk_toggle_button_get_active( cleanupCheck );
	/* only necessary to check this option if we are creating DVD video */
	if( videoformat == 6 )
	{
		// Tool 0 is always "None"
		author = prefs.exportMjpegDvdTool > 0;
	}
	active_item = gtk_menu_get_active( mjpegAspect );
	aspectRatio = g_list_index ( GTK_MENU_SHELL ( mjpegAspect ) ->children, active_item );
	
	/* force deinterlace on MPEG-1 formats if not specified by user */
	if ( videoformat < 3 && deinterlaceRequest == 0 )
		deinterlaceRequest = 3;

	char deinterlace[ 30 ] = "";
	char interlace[5] = "-I 0";
	char audioOptions[ 20 ] = "";

	switch ( deinterlaceRequest )
	{
	case 0: // None
		strcpy( interlace, "-I 1" );
		break;
	case 1: // Internal
		type = PIPE_VIDEO_DEINTERLACED_MJPEG;
		isY4Minterlaced = false;
		break;
	case 2: // Already deinterlaced
		isY4Minterlaced = false;
		break;
	case 3:
		strcpy( deinterlace, hasYuvDeinterlace ? "yuvdeinterlace |" : "yuvdenoise -F -f |" );
		break;
	case 4:
		strcpy( deinterlace, hasYuvDeinterlace ? "yuvdeinterlace -f |" : "yuvdenoise -F |" );
		break;
	}

	if ( !strstr( videopipeInput, "yuvscaler" ) )
	{
		char scale[ 20 ] = "";
		char bitrate[ 20 ] = "";

		switch ( videoformat )
		{
		case 1:
			strcpy( scale, "VCD" );
			frequency = 44100;
			if ( strstr( audioencode, "ffmpeg" ) )
				strcpy( audioOptions, "-ar 44100" );
			else if ( strstr( audioencode, "mp2enc" ) )
				strcpy( audioOptions, "-r 44100" );
			break;
		case 2:
			strcpy( scale, "VCD" );
			strcpy( bitrate, "-b 1152" );
			frequency = 44100;
			if ( strstr( audioencode, "ffmpeg" ) )
				strcpy( audioOptions, "-ar 44100" );
			else if ( strstr( audioencode, "mp2enc" ) )
				strcpy( audioOptions, "-r 44100" );
			break;
		case 3:
			strcpy( bitrate, "-b 4000" );
			break;
		case 4:
			strcpy( scale, "SVCD" );
			frequency = 44100;
			if ( strstr( audioencode, "ffmpeg" ) )
				strcpy( audioOptions, "-ar 44100" );
			else if ( strstr( audioencode, "mp2enc" ) )
				strcpy( audioOptions, "-r 44100" );
			break;
		case 5:
			strcpy( scale, "SVCD" );
			strcpy( bitrate, "-b 2500" );
			frequency = 44100;
			if ( strstr( audioencode, "ffmpeg" ) )
				strcpy( audioOptions, "-ar 44100" );
			else if ( strstr( audioencode, "mp2enc" ) )
				strcpy( audioOptions, "-r 44100" );
			break;
		case 6:
			videoformat = 8;
			strcpy( scale, "" );
			if ( strstr( audioencode, "ffmpeg" ) )
			{
				frequency = 48000;
				if ( !strstr( audioencode, " -ab " ) )
					sprintf( audioOptions, "%s -ab 192k", audioOptions );
			}
			else if ( strstr( audioencode, "mp2enc" ) )
			{
				if ( !strstr( audioencode, " -r " ) )
					sprintf( audioOptions, "%s -r 48000", audioOptions );
				frequency = 48000;
				if ( !strstr( audioencode, " -b " ) )
					sprintf( audioOptions, "%s -b 224", audioOptions );
			}
			break;
		}

		// If the bitrate is specified then remove the defaults above
		if ( strstr( videopipeInput, " -b " ) || strstr( videopipeInput, "--video-bitrate" ) )
			strcpy( bitrate, "" );

		// If the interlace is specified then remove the defaults above
		if ( strstr( videopipeInput, " -I " ) || strstr( videopipeInput, "--interlace-mode" ) )
			strcpy( interlace, "" );

		// Yuck (!) - builds the video pipe depending on data collected above...
		if ( strcmp( scale, "" ) )
			videopipe = g_strdup_printf(
			         "%s yuvscaler -v 0 -O %s -n %c | %s -f %d %s -n %c -a %d %s -o \'%s\'.mpv",
			         deinterlace,
			         scale, isPAL ? 'p' : 'n', videopipeInput, videoformat, interlace,
			         isPAL ? 'p' : 'n', aspectRatio ? aspectRatio : (isWide ? 3 : 2), bitrate, filename );
		else
			videopipe = g_strdup_printf( "%s %s -f %d %s -n %c -a %d %s -o \'%s\'.mpv",
			         deinterlace, videopipeInput, videoformat, interlace,
			         isPAL ? 'p' : 'n', aspectRatio ? aspectRatio : (isWide ? 3 : 2), bitrate, filename );
	}
	else
	{
		videopipe = g_strdup_printf( "%s %s -f %d %s -n %c -o \'%s\'.mpv\n",
		         deinterlace, videopipeInput, videoformat, interlace,
		         isPAL ? 'p' : 'n', filename );
	}
	if ( strstr( audioencode, "ffmpeg" ) )
	{
		audiopipe = g_strdup_printf( "|%s -f wav -i pipe: %s -y \'%s\'.%s", audioencode, audioOptions,
			filename, videoformat == 8 ? "ac3" : "mp2" );
		if ( strcmp( multiplexer, "" ) )
		{
			multiplex = g_strdup_printf( "%s -f %d -o \'%s\'%%s.mpeg \'%s\'.mpv \'%s\'.%s",
					multiplexer, videoformat, filename, filename, filename,
					videoformat == 8 ? "ac3" : "mp2" );
			cleanup = g_strdup_printf( "rm -f \'%s\'.mpv \'%s\'.%s \'%s\'.wav",
					filename, filename, videoformat == 8 ? "ac3" : "mp2", filename );
		}
	}
	else if ( strstr( audioencode, "mp2enc" ) )
	{
		audiopipe = g_strdup_printf( "|%s %s -o \'%s\'.mp2", audioencode, audioOptions, filename );
		if ( strcmp( multiplexer, "" ) )
		{
			multiplex = g_strdup_printf( "%s -f %d -o \'%s\'%%s.mpeg \'%s\'.mpv \'%s\'.mp2",
					multiplexer, videoformat, filename, filename, filename );
			cleanup = g_strdup_printf( "rm -f \'%s\'.mpv \'%s\'.mp2 \'%s\'.wav",
					filename, filename, filename );
		}
	}

	KinoAudioPipe *wav = KinoAudioFactory::CreateAudioPipe( PIPE_AUDIO_WAV );
	KinoVideoPipe *video = KinoVideoFactory::CreateVideoPipe( type );
	KinoAVPipe *mpeg = new KinoAVPipe( wav, video );

	// Iterate through each scene (opening and closing audio pipes as requested)
	int scene = 0;

	for ( int sceneBegin = begin; sceneBegin <= end && exportPage->isExporting;
	        scene ++ )
	{
		gchar *full;
		char sceneString[ 20 ];
		int sceneEnd = end;
		bool userMplexSplit = ( multiplex != NULL && ( strstr( multiplex, "-S" ) || 
			strstr( multiplex, "--max-segment-size" ) ) );

		// Determine if we need to split by scene or not
		// when splitting: Output is generated as fileNNN_MMM.mpeg where NNN is the
		// kino scene number and MMM is the mplex split
		// when not splitting: Output is generated file as fileMMM.mpeg
		sceneString[0] = '\0';
		if ( split )
		{
			sprintf( sceneString, "%03d", scene );
			if ( userMplexSplit )
				strcat( sceneString, "_%03d" );
			sceneEnd = playlist->FindEndOfScene( sceneBegin );
			if ( sceneEnd > end )
				sceneEnd = end;
		}
		else if ( userMplexSplit )
		{
			strcpy( sceneString, "%03d" );
		}

		if ( author )
		{
		  	char tmpFilename[512];

		   	if ( split && userMplexSplit ) {
             	snprintf( tmpFilename, sizeof( tmpFilename ), "%s%03d_001.mpeg", filename, scene );
      		} else if ( split ) {
		  		snprintf( tmpFilename, sizeof( tmpFilename ), "%s%03d.mpeg", filename, scene );
      		} else if ( userMplexSplit ) {
		  		snprintf( tmpFilename, sizeof( tmpFilename ), "%s001.mpeg", filename );
      		} else {
		  		snprintf( tmpFilename, sizeof( tmpFilename ), "%s.mpeg", filename );
			}

			nameList.push_back( std::string( tmpFilename ) );
		}

		cerr << ">>> Generated video pipe '" << videopipe << "'" << endl;
		if ( audiopipe )
		{
			cerr << ">>> Generated audio pipe '" << audiopipe << "'" << endl;
			mpeg->OpenAudio( audiopipe, channels, frequency, 2 );
		}
		mpeg->OpenVideoPipe( videopipe, width, height, isY4Minterlaced );

		int imagesize = width * height * 3;

		// first call to innerLoopUpdate initializes progress tracker, which
		// we want to do because calculateAdjustedRate generates paused time.
		innerLoopUpdate( sceneBegin, begin, end, every );

		// Determine correct amount of audio for duration
		double adjustedRate = 0;
		AsyncAudioResample<int16_ne_t,int16_le_t>* resampler = 0;
		if ( audiopipe )
		{
			adjustedRate = calculateAdjustedRate( playlist, frequency, sceneBegin, sceneEnd, every );
			if ( !adjustedRate )
				status = EXPORT_RESULT_ABORT;

			// Setup a resampler
			resampler = new AsyncAudioResample<int16_ne_t,int16_le_t>(
				AUDIO_RESAMPLE_SRC_SINC_BEST_QUALITY, playlist, frequency, sceneBegin, sceneEnd, every );
			if ( resampler->IsError() )
			{
				std::cerr << ">>> Resampler error: " << resampler->GetError() << std::endl;
				exportPage->isExporting = false;
				status = EXPORT_RESULT_FAILURE;
				break;
			}
		}

		/* Iterate over frames in scene */
		for ( int i = sceneBegin, j = 0; i <= sceneEnd && exportPage->isExporting;
			i += every, j++ )
		{
			innerLoopUpdate( i, begin, end, every );
			playlist->GetFrame( i, *frame );

			if ( audiopipe && resampler )
			{
				int requestedSamples = frame->CalculateNumberSamples( frequency, j );
				int nsamples = resampler->Process( adjustedRate, requestedSamples );
				if ( nsamples > 0 && !mpeg->OutputAudioFrame( reinterpret_cast<int16_t *>(resampler->GetOutput()), 
					nsamples * channels * sizeof(int16_t) ) )
				{
					modal_message( _( "Error writing to KINO/MJPEG audio filter - aborting." ) );
					status = EXPORT_RESULT_FAILURE;
					break;
				}
			}

			if ( exportPage->isExporting )
			{
				if ( type == PIPE_VIDEO_MJPEG )
					frame->ExtractYUV( pixels );
				else
					frame->ExtractRGB( pixels );
				if ( !mpeg->OutputVideoFrame( pixels, imagesize ) )
				{
					modal_message( _( "Error writing to KINO/MJPEG video filter - aborting." ) );
					status = EXPORT_RESULT_FAILURE;
					break;
				}
			}
		}

		if ( status == EXPORT_RESULT_SUCCESS && !exportPage->isExporting )
			status = EXPORT_RESULT_ABORT;

		if ( !split )
			exportPage->isExporting = false;

		// Try to finish the stream to create a valid mpeg if possible
		if ( audiopipe )
			mpeg->CloseAudio();
		mpeg->CloseVideo();
		delete resampler;
		resampler = 0;

		if ( status == EXPORT_RESULT_SUCCESS && multiplex != NULL && strcmp( multiplex, "" ) )
		{
			// expand the template to accomodate the scene splits
			full = g_strdup_printf( multiplex, sceneString );
			cerr << ">>> Executing '" << full << "'" << endl;
			if ( 0 != mpeg->ExecuteCommand( full ) )
			{
				// If mplex error, use user setting
				//do_cleanup = Usercleanup;
			}
			g_free( full );
		}

		if ( Usercleanup && cleanup != NULL && strcmp( cleanup, "" ) )
			mpeg->ExecuteCommand( cleanup );
		
		// Move to start of next scene
		sceneBegin = sceneEnd + 1;
	}

	if ( status == EXPORT_RESULT_SUCCESS && author )
	{
     	createAuthorXml( filename, nameList, split, isPAL );

		// Run the selected tool
		// -1 to account for first "None" item
		DVDTool *tool = m_dvdTools[ prefs.exportMjpegDvdTool - 1 ];
		if ( tool )
		{
			string dvdauthorFile( string( filename ) + "-dvdauthor.xml" );
			if ( tool->execute( dvdauthorFile, string( filename ) ) )
				modal_message( _("The dvdauthor script failed, but the MPEG export succeeded.") );
			tool->close();
		}
	}

	g_free( filename );
	g_free( audiopipe );
	g_free( videopipe );
	g_free( multiplex );
	g_free( cleanup );
	delete mpeg;
	delete video;
	delete wav;
	delete[] pixels;

	GetFramePool()->DoneWithFrame( frame );
	
	return status;
}


/* added 2004-01-08
 * Greg Hookey <g@deadly.ca>
 */
void
ExportMJPEG::createAuthorXml( const char *filename,
							  std::vector<std::string>& nameList,
							  bool split, bool isPal )
{
	if( filename == NULL )
	{
	  std::cerr << ">>> ExportMJPEG::createAuthorXml (file name null)" << std::endl;
	  return;
	}

	char filenameBuffer[512];

	snprintf( filenameBuffer, sizeof( filenameBuffer ), "%s-dvdauthor.xml", filename );

	std::cerr << ">>> ExportMJPEG::createAuthorXml (filename: " << filenameBuffer << ")" << std::endl;

	std::stringstream xml;

	xml << "<?xml version=\"1.0\"?>" << std::endl;
	xml << "<dvdauthor>" << std::endl;

	xml << "\t<vmgm>" << std::endl;
	xml << "\t\t<menus>" << std::endl;
	xml << "\t\t\t<video />" << std::endl;
	xml << "\t\t\t<audio />" << std::endl;
	xml << "\t\t\t<subpicture lang=\"en\" />" << std::endl;
	xml << "\t\t</menus>" << std::endl;
	xml << "\t</vmgm>" << std::endl;

	xml << "\t<titleset>" << std::endl;
	xml << "\t\t<titles>" << std::endl;
	xml << "\t\t\t<pgc pause=\"0\">" << std::endl;

	std::vector<std::string>::iterator iter = nameList.begin();

	if( split == true )
	{
		/* output the list list of vobs */
		while( iter != nameList.end() )
		{
			xml << "\t\t\t\t<vob file=\"" << *iter << "\" chapters=\"\" pause=\"0\" />" << std::endl;
			iter++;
		}
	}
	else
	{
		/* retrieve the vector of scenes, assuming that the vector
		 * doesn't have the 0 entry.
		 */
		std::vector<int> sceneStarts = this->common->getPageEditor()->GetScene();
		std::vector<int>::iterator sceneIter = sceneStarts.begin();
		std::stringstream chapterList;

		chapterList << "0";

		for( sceneIter = sceneStarts.begin(); sceneIter != sceneStarts.end() - 1; ++sceneIter )
			chapterList << "," << this->common->getTime().parseFramesToString( *sceneIter, SMIL::Time::TIME_FORMAT_CLOCK );

		/* output the single vob, checking for chapter marks */
		xml << "\t\t\t\t<vob file=\"" << *iter << "\" chapters=\"" <<
		chapterList.str() << "\" pause=\"0\" />" << std::endl;
	}

	xml << "\t\t\t</pgc>" << std::endl;
	xml << "\t\t</titles>" << std::endl;
	xml << "\t</titleset>" <<std::endl;

	xml << "</dvdauthor>" << std::endl;

	std::cerr << xml.str() << std::endl;

	std::ofstream out( filenameBuffer );

	if( out.is_open() == true )
	{
		out << xml.str();
		out.flush();
	}

	out.close(); 
}

void ExportMJPEG::loadTools( string directory )
{
	char * filename;
	struct dirent *entry;

	DIR *dir = opendir( directory.c_str( ) );

	if ( dir )
	{
		while ( ( entry = readdir( dir ) ) != NULL )
		{
			filename = g_strdup_printf( "%s/%s", directory.c_str( ), entry->d_name );
			if ( entry->d_name[ 0 ] != '.' )
			{
				fprintf( stderr, "%s\n", filename );
				DVDTool *tool = new DVDTool( filename );
				if ( tool->m_active )
					m_dvdTools.push_back( tool );
				else
					delete tool;
			}
			g_free( filename );
		}
		closedir( dir );
	}
	std::sort( m_dvdTools.begin(), m_dvdTools.end(), DVDTool::compare );
}

void ExportMJPEG::activateTool( int index )
{
	// Get the main widget
	GtkWidget * window = common->getWidget();

	// Start with the option menu
	GtkOptionMenu *option_menu = GTK_OPTION_MENU( lookup_widget( window, "optionmenu_dvd_tools" ) );
	GtkMenu *menu = GTK_MENU( gtk_option_menu_get_menu( option_menu ) );

	// Create a menu if we don't have one
	if ( menu )
	{
		menu = GTK_MENU( gtk_menu_new( ) );
		// Create a "None" menu item
		GtkWidget *item = gtk_menu_item_new_with_label( _("None") );
		gtk_widget_show( item );
		gtk_menu_append( menu, item );
		// Creat the other menu items
		for ( unsigned int i = 0; i < m_dvdTools.size( ); i ++ )
		{
			GtkWidget *item = gtk_menu_item_new_with_label( m_dvdTools[ i ] ->m_description.c_str( ) );
			gtk_widget_show( item );
			gtk_menu_append( menu, item );
		}

		gtk_option_menu_set_menu( option_menu, GTK_WIDGET( menu ) );
	}

	// Set the active item
	gtk_option_menu_set_history( option_menu, index );
}


extern "C"
{
	void
	on_button_export_mjpeg_file_clicked    (GtkButton       *button,
                                            gpointer         user_data)
	{
		const char *filename = common->getFileToSave( _("Enter a File Name to Save As") );
		gtk_widget_grab_focus( lookup_widget( GTK_WIDGET( button ), "entry_export_mjpeg_file" ) );
		if ( strcmp( filename, "" ) )
			gtk_entry_set_text( GTK_ENTRY( lookup_widget( GTK_WIDGET( button ), "entry_export_mjpeg_file" ) ), filename );
	}
}

/*
* framedisplayer.h -- sends frame objects to Displayer and audio device
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _FRAME_DISPLAYER_H
#define _FRAME_DISPLAYER_H

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#define DV_FOURCC_YV12  0x32315659	/* 4:2:0 Planar mode: Y + V + U  (3 planes) */
#define DV_FOURCC_YUY2  0x32595559	/* 4:2:2 Packed mode: Y0+U0+Y1+V0 (1 plane) */

#include "displayer.h"
#include "oss.h"

class Frame;

class FrameDisplayer
{
public:
	Displayer *displayer;
	AudioResample<int16_ne_t,int16_ne_t> *resampler;

	unsigned char pixels[ FRAME_MAX_WIDTH * FRAME_MAX_HEIGHT * 4 ];

	/* audio support */
	kino_sound_t *audio_device;
	gint16 *audio_buffers[ 4 ];
	gboolean audio_device_avail;
	gint audio_sampling_rate;

	FrameDisplayer();
	~FrameDisplayer();
	void CloseSound();
	void Put( Frame &frame, GtkWidget *drawingarea, gboolean no_audio );
	void PutSound( Frame &frame );
	void PutGDKRGB32( Frame &frame, GtkWidget *drawingarea );
private:
	bool prevIsWide;
};

#endif

/*
* page_export_avi.h Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EXPORT_AVI_H
#define _PAGE_EXPORT_AVI_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "export.h"
#include "page_export.h"
#include "kino_common.h"
#include "filehandler.h"

/** This class represents the Export/AVI notebook page.
 */

class ExportAVI : public Export
{
private:
	FileHandler *writer;

	/* Pointers to the controls on this page */
	GtkEntry *fileEntry;
	GtkToggleButton	*formatDV1Toggle;
	GtkToggleButton	*formatDV2Toggle;
	GtkToggleButton	*formatRawDVToggle;
	GtkToggleButton	*formatQtToggle;
	GtkToggleButton *autoSplitToggle;
	GtkToggleButton *timeStampToggle;
	GtkToggleButton *metadataToggle;
	GtkEntry *framesEntry;
	GtkEntry *sizeEntry;
	GtkToggleButton *OpenDMLToggle;

	enum export_result
	doExport( PlayList * playlist, int begin, int end, int every,
	          bool preview );

public:
	ExportAVI( PageExport*, KinoCommon* );
	virtual ~ExportAVI();

	void start();
	void clean();

};

#endif


/*
* page_export_mjpeg.h Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EXPORT_MJPEG_H
#define _PAGE_EXPORT_MJPEG_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vector>

#include "export.h"
#include "page_export.h"
#include "kino_common.h"


class DVDTool;

/** This class represents the Export/Still Frames notebook page.
*/

class ExportMJPEG : public Export
{
private:
	Preferences& prefs;
	GtkEntry *fileEntry;

	// MJPEG MPEG Inputs
	GtkMenu	*mjpegFormat;
	GtkEntry	*mjpegVideo;
	GtkEntry	*mjpegAudio;
	GtkEntry	*mjpegMultiplexer;
	GtkMenu	*deinterlaceMenu;
	GtkToggleButton	*splitCheck;
	GtkToggleButton	*cleanupCheck;
	GtkMenu *mjpegAspect;
	GtkEntry *mjpegXMLScript;
	bool hasYuvDeinterlace;
	bool isAvailable;
	vector < DVDTool * > m_dvdTools;

	void start();
	gulong onActivate();
	enum export_result
	doExport( PlayList * playlist, int begin, int end, int every,
	          bool preview );

	void createAuthorXml( const char *filename,
						  std::vector<std::string>& nameList,
						  bool split, bool isPal );

public:
	ExportMJPEG( PageExport*, KinoCommon* );
	virtual ~ExportMJPEG();

	void loadTools( string directory );
	void activateTool( int tool );
	void selectTool( int tool );
};
#endif

/*
* displayer.h Displayer Objects
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _KINO_DISPLAYER_H
#define _KINO_DISPLAYER_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
using std::cerr;
using std::endl;

#include <string.h>
#include <math.h>

#include <X11/Xlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>
#include <X11/extensions/Xvlib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <time.h>
#include <sys/time.h>

#include <gtk/gtk.h>
#include <gdk/gdkprivate.h>

#include "preferences.h"
#include "frame.h"

class AspectRatioCalculator
{
public:
	int x;
	int y;
	int width;
	int height;

	AspectRatioCalculator( int widgetWidth, int widgetHeight,
	                       int imageWidth, int imageHeight,
	                       int maxImageWidth, int maxImageHeight );

	AspectRatioCalculator( int widgetWidth, int widgetHeight,
	                       int imageWidth, int imageHeight, bool isPAL, bool isWidescreen );
};

/** Supported Displayer formats
*/

typedef enum {
    DISPLAY_NONE,
    DISPLAY_YUV,
    DISPLAY_RGB,
    DISPLAY_BGR,
    DISPLAY_BGR0,
    DISPLAY_RGB16
}
DisplayerInput;

#define MAX_WIDTH		720
#define MAX_HEIGHT		576

/**	A Displayer is a class which is responsible for rendering an image in some
	way (ie: Xvideo, GDK, X etc).
 
	All Displayer classes must extend the Displayer class and minimally rewrite:
	
	+ usable() - to indicate if the object can be used, 
	+ format() - to indicate what type of input the put method expects
	+ put( void * ) - deal with an image of the expected type and size
 
	By default, all images will be delivered to the put method in a resolution
	of IMG_WIDTH * IMG_HEIGHT. If another size is required, then the rewrite
	the methods:
 
	+ preferredWidth 
	+ preferredHeight
 
	If the widget being written to doesn't need a fixed size, then rewrite
	the two other put methods as required.
*/

class Displayer
{
protected:
	int img_width;
	int img_height;
	virtual void put( void * ) = 0;

public:
	virtual ~Displayer()
	{ }
	virtual bool usable();
	virtual DisplayerInput format();
	virtual int preferredWidth();
	virtual int preferredHeight();
	virtual void put( void *image, int width, int height );
	virtual void put( DisplayerInput inFormat, void *image,
	                  int width, int height );

private:
	unsigned char pixels[ MAX_WIDTH * MAX_HEIGHT * 4 ];
	void reformat( DisplayerInput inFormat, int outFormat, void *image,
	               int width, int height );
	void Reformat424( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight );
	void Reformat222( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight );
	void Reformat323( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight );
	void Reformat323R( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight );
	void Reformat324( unsigned char *image, int inWidth, int inHeight, int outWidth, int outHeight );
};

class XDisplayer : public Displayer
{
private:
	bool canuse;
	int depth;
	GtkWidget *drawingarea;
	Display *display;
	Window window;
	GC gc;
	XImage *xImage;
	XGCValues values;
	XShmSegmentInfo shmInfo;
public:
	XDisplayer( GtkWidget *drawingarea );
	~XDisplayer();
	bool usable();
	DisplayerInput format();
protected:
	void put( void *data );
};

class XvDisplayer : public Displayer
{
private:
	bool gotPort;
	int grabbedPort;
	GtkWidget *drawingarea;
	Display *display;
	Window window;
	GC gc;
	XGCValues values;
	XvImage *xvImage;
	unsigned int port;
	XShmSegmentInfo shmInfo;
	char pix[ MAX_WIDTH * MAX_HEIGHT * 4 ];
	bool isPAL;
	bool isWidescreen;

public:
	XvDisplayer( GtkWidget *drawingarea, int width, int height, bool isPAL, bool isWidescreen );
	~XvDisplayer();
	bool usable();
	DisplayerInput format();

protected:
	void put( void *image );
};

class GdkDisplayer: public Displayer
{
private:
	GtkWidget *drawingarea;
	bool m_is_wide;

public:
	GdkDisplayer( GtkWidget *drawingarea, int width, int height, bool is_wide );
	~GdkDisplayer();
	bool usable();
	DisplayerInput format();
	void put( void *data );
};

class FindDisplayer
{
public:
	static Displayer *getDisplayer( GtkWidget *drawingarea, Frame &frame );
	static Displayer *getDisplayer( GtkWidget *drawingarea, int width, int height );
};

#endif

/*
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
enum
{
   CMD_COLUMN,
   DESC_COLUMN,
   N_COLUMNS
};

GtkWidget* tree = lookup_widget( dialog, "list_keyhelp_edit" );
GList *columns = gtk_tree_view_get_columns( GTK_TREE_VIEW( tree ) );
gint count = g_list_length( columns );
g_list_free( columns );

if ( count == 0 )
{
	GtkListStore *store;
	GtkTreeIter   iter;
	GtkCellRenderer* renderer;
	GtkTreeViewColumn *column;
	
	store = gtk_list_store_new( N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING );
 gtk_list_store_append( store, &iter );   gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Playback"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("<space>"),
	DESC_COLUMN, _("Toggle between play and pause"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Esc"),
	DESC_COLUMN, _("Stop"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Navigation"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] l, <right-arrow>"),
	DESC_COLUMN, _("Move one frame forward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] h, <left-arrow>, Ctrl+H"),
	DESC_COLUMN, _("Move one frame backward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] w, W, e, E"),
	DESC_COLUMN, _("Move one second forward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] b, B"),
	DESC_COLUMN, _("Move one second backward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] 0, ^"),
	DESC_COLUMN, _("Move to start of scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] $"),
	DESC_COLUMN, _("Move to end of scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] j, +, <down-arrow>"),
	DESC_COLUMN, _("Move to start of next scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] k, -, <up-arrow>"),
	DESC_COLUMN, _("Move to start of previous scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("gg, Home"),
	DESC_COLUMN, _("Move to first frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("G, End"),
	DESC_COLUMN, _("Move to last frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+F, Page Down"),
	DESC_COLUMN, _("Move forward 5 scenes"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+B, Page Up"),
	DESC_COLUMN, _("Move backward 5 scenes"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Cut"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] x, Delete, dl"),
	DESC_COLUMN, _("Cut current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("dw"),
	DESC_COLUMN, _("Cut a second"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] dd, Ctrl+X"),
	DESC_COLUMN, _("Cut current scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("o, d$"),
	DESC_COLUMN, _("Cut to end of current scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("dG"),
	DESC_COLUMN, _("Cut to end of movie"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("i, d0, d^"),
	DESC_COLUMN, _("Cut from start of scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("dgg"),
	DESC_COLUMN, _("Cut from start of movie"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Copy"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] y<space>, yl"),
	DESC_COLUMN, _("Copy current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] yy, Y, Ctrl+C"),
	DESC_COLUMN, _("Copy current scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("y$"),
	DESC_COLUMN, _("Copy to end of scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("y^, y0"),
	DESC_COLUMN, _("Copy from start of scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Paste"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] p"),
	DESC_COLUMN, _("Paste after current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] P, Ctrl+V"),
	DESC_COLUMN, _("Paste before current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Mode Switching"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("a, A, F3"),
	DESC_COLUMN, _("Switch to Capture"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("v, F4"),
	DESC_COLUMN, _("Switch to Timeline"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("t, F5"),
	DESC_COLUMN, _("Switch to Trim"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("C, F6"),
	DESC_COLUMN, _("Switch to FX"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":W, F7"),
	DESC_COLUMN, _("Switch to Export"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("General"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("."),
	DESC_COLUMN, _("Repeat last command"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+J"),
	DESC_COLUMN, _("Split scene before current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] J"),
	DESC_COLUMN, _("Join this scene with the following scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("u, Ctrl+Z"),
	DESC_COLUMN, _("Undo"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+R, Shift+Ctrl+Z"),
	DESC_COLUMN, _("Redo"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+O"),
	DESC_COLUMN, _("File/Open"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+P"),
	DESC_COLUMN, _("Preferences"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":r"),
	DESC_COLUMN, _("Insert file (DV AVI or SMIL) before current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":a"),
	DESC_COLUMN, _("Append file (DV AVI or SMIL) to scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":A"),
	DESC_COLUMN, _("Append file (DV AVI or SMIL) to movie"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":w, Ctrl+S"),
	DESC_COLUMN, _("Save the movie as SMIL"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":q, Ctrl+Q"),
	DESC_COLUMN, _("Quit"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":<numeric>"),
	DESC_COLUMN, _("Move to specified frame"), -1 );

	gtk_tree_view_set_model( GTK_TREE_VIEW( tree ), GTK_TREE_MODEL( store ) );
	g_object_unref( G_OBJECT( store ) );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Command"), renderer, "text", CMD_COLUMN, NULL );
	gtk_tree_view_column_set_resizable( column, TRUE );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Description"), renderer, "text", DESC_COLUMN, NULL );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );

	store = gtk_list_store_new( N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Playback"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("<space>"),
	DESC_COLUMN, _("Toggle between play and pause"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Esc"),
	DESC_COLUMN, _("Stop"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Navigation"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("l, <right-arrow>"),
	DESC_COLUMN, _("Move one frame forward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("h, <left-arrow>, Ctrl+H"),
	DESC_COLUMN, _("Move one frame backward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("w, W, e, E"),
	DESC_COLUMN, _("Forward Scan"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("b, B"),
	DESC_COLUMN, _("Reverse Scan"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("0, ^"),
	DESC_COLUMN, _("Previous Index"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("$"),
	DESC_COLUMN, _("Next Index"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("j, +, <down-arrow>"),
	DESC_COLUMN, _("Next Index"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("k, -, <up-arrow>"),
	DESC_COLUMN, _("Previous Index"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("gg, Home"),
	DESC_COLUMN, _("Rewind"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("G, End"),
	DESC_COLUMN, _("Fast Forward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Mode Switching"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Esc, F2"),
	DESC_COLUMN, _("Return to Edit"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("v, F4"),
	DESC_COLUMN, _("Switch to Timeline"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("t, F5"),
	DESC_COLUMN, _("Switch to Trim"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("C, F6"),
	DESC_COLUMN, _("Switch to FX"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":W, F7"),
	DESC_COLUMN, _("Switch to Export"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("General"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Enter"),
	DESC_COLUMN, _("Start Capture"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("."),
	DESC_COLUMN, _("Repeat last command"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+O"),
	DESC_COLUMN, _("File/Open"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+P"),
	DESC_COLUMN, _("Preferences"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":w, Ctrl+S"),
	DESC_COLUMN, _("Save the movie as SMIL"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":q, Ctrl+Q"),
	DESC_COLUMN, _("Quit"), -1 );

	tree = lookup_widget( dialog, "list_keyhelp_capture" );
	gtk_tree_view_set_model( GTK_TREE_VIEW( tree ), GTK_TREE_MODEL( store ) );
	g_object_unref( G_OBJECT( store ) );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Command"), renderer, "text", CMD_COLUMN, NULL );
	gtk_tree_view_column_set_resizable( column, TRUE );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Description"), renderer, "text", DESC_COLUMN, NULL );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );

	store = gtk_list_store_new( N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Playback"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("<space>"),
	DESC_COLUMN, _("Toggle between play and pause"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Esc"),
	DESC_COLUMN, _("Stop"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("\\"),
	DESC_COLUMN, _("Toggle Looping"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Navigation"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] l, <right-arrow>"),
	DESC_COLUMN, _("Move one frame forward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] h, <left-arrow>, Ctrl+H"),
	DESC_COLUMN, _("Move one frame backward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] w, W, e, E"),
	DESC_COLUMN, _("Move one second forward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] b, B"),
	DESC_COLUMN, _("Move one second backward"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("0, ^"),
	DESC_COLUMN, _("Move to in point"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("$"),
	DESC_COLUMN, _("Move to out point"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("j, +, <down-arrow>"),
	DESC_COLUMN, _("Move to start of next scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("k, -, <up-arrow>"),
	DESC_COLUMN, _("Move to start of previous scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("gg, Home"),
	DESC_COLUMN, _("Move to first frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("G, End"),
	DESC_COLUMN, _("Move to last frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Trimming"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("o, d$"),
	DESC_COLUMN, _("Set out point"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("i, d0, d^"),
	DESC_COLUMN, _("Set in point"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("="),
	DESC_COLUMN, _("Toggle Link"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Insert, Ins"),
	DESC_COLUMN, _("Toggle Insert/Overwrite"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Enter"),
	DESC_COLUMN, _("Overwrite/Load Current Scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":r"),
	DESC_COLUMN, _("Insert Before"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":a"),
	DESC_COLUMN, _("Insert After"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Cut"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] dd, Ctrl+X"),
	DESC_COLUMN, _("Cut current scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Copy"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] yy, Y, Ctrl+C"),
	DESC_COLUMN, _("Copy current scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Paste"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] p"),
	DESC_COLUMN, _("Paste after current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] P, Ctrl+V"),
	DESC_COLUMN, _("Paste before current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Mode Switching"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Esc, F2"),
	DESC_COLUMN, _("Return to Edit"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("a, A, F3"),
	DESC_COLUMN, _("Switch to Capture"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("v, F4"),
	DESC_COLUMN, _("Switch to Timeline"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("C, F6"),
	DESC_COLUMN, _("Switch to FX"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":W, F7"),
	DESC_COLUMN, _("Switch to Export"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("General"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("."),
	DESC_COLUMN, _("Repeat last command"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+J"),
	DESC_COLUMN, _("Split scene before current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("[n] J"),
	DESC_COLUMN, _("Join this scene with the following scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("u, Ctrl+Z"),
	DESC_COLUMN, _("Undo edits"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Shift+U"),
	DESC_COLUMN, _("Reset in and out points"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+R, Shift+Ctrl+Z"),
	DESC_COLUMN, _("Redo"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+O"),
	DESC_COLUMN, _("File/Open"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+P"),
	DESC_COLUMN, _("Preferences"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":r"),
	DESC_COLUMN, _("Insert file (DV AVI or SMIL) before current frame"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":a"),
	DESC_COLUMN, _("Append file (DV AVI or SMIL) to scene"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":A"),
	DESC_COLUMN, _("Append file (DV AVI or SMIL) to movie"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":w, Ctrl+S"),
	DESC_COLUMN, _("Save the movie as SMIL"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":q, Ctrl+Q"),
	DESC_COLUMN, _("Quit"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":<numeric>"),
	DESC_COLUMN, _("Move to specified frame"), -1 );

	tree = lookup_widget( dialog, "list_keyhelp_trim" );
	gtk_tree_view_set_model( GTK_TREE_VIEW( tree ), GTK_TREE_MODEL( store ) );
	g_object_unref( G_OBJECT( store ) );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Command"), renderer, "text", CMD_COLUMN, NULL );
	gtk_tree_view_column_set_resizable( column, TRUE );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Description"), renderer, "text", DESC_COLUMN, NULL );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );
	
	store = gtk_list_store_new( N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Mode Switching"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Esc, F2"),
	DESC_COLUMN, _("Return to Edit"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("a, A, F3"),
	DESC_COLUMN, _("Switch to Capture"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("v, F4"),
	DESC_COLUMN, _("Switch to Timeline"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("t, F5"),
	DESC_COLUMN, _("Switch to Trim"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("C, F6"),
	DESC_COLUMN, _("Switch to FX"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":W, F7"),
	DESC_COLUMN, _("Switch to Export"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("General"),
	DESC_COLUMN, "-------------------------------------", -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+O"),
	DESC_COLUMN, _("File/Open"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _("Ctrl+P"),
	DESC_COLUMN, _("Preferences"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":w, Ctrl+S"),
	DESC_COLUMN, _("Save the movie as SMIL"), -1 );
 gtk_list_store_append( store, &iter );  gtk_list_store_set( store, &iter,
	CMD_COLUMN, _(":q, Ctrl+Q"),
	DESC_COLUMN, _("Quit"), -1 );
	
	tree = lookup_widget( dialog, "list_keyhelp_other" );
	gtk_tree_view_set_model( GTK_TREE_VIEW( tree ), GTK_TREE_MODEL( store ) );
	g_object_unref( G_OBJECT( store ) );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Command"), renderer, "text", CMD_COLUMN, NULL );
	gtk_tree_view_column_set_resizable( column, TRUE );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes( _("Description"), renderer, "text", DESC_COLUMN, NULL );
	gtk_tree_view_append_column( GTK_TREE_VIEW( tree ), column );
	
}

/*
* page_bttv.h Notebook v4l Capture Page Object
* Copyright (C) 2001 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_BTTV_H
#define _PAGE_BTTV_H

#include <string>
using std::string;

#include "kino_common.h"
#include "page.h"
#include "displayer.h"
#include "v4l.h"

/** Video4Linux Capture page. Totally disconnected from the rest of the app.
 
	Curious if I can generate a Frame object to allow playback using 
	FrameDisplayer but will require virtual methods on the base class... maybe 
	withdrawn if no one is interested (or proves too difficult).
 
	Silly behaviour - uses Rewind and Fast Forward as channel hopper, start/end
   	of scene for large frequency jumps (+/- 100) and back/forward as fine 
	tuning. Produces a report on the capability of your video capture card on 
	stdout and then promptly ignores everything that it's found out about it ...
   	yippee.
 
	Only tested on hauppage BT878 - no idea about other cards, though will be 
	able to test on an iomega buz at some point.
*/

class PageBttv : public Page
{
private:
	// Imported
	KinoCommon *common;
	bool seeking;
	string lastPreferenceFilename;
	int channel;

	// Video4Linux object
	GDKV4L *v4l;

public:
	PageBttv( KinoCommon *common );
	virtual ~PageBttv();
	void start();
	void newFile( );
	gulong activate();
	void clean();
	void videoStartOfMovie();
	void videoPreviousScene();
	void videoRewind();
	void videoPlay();
	void videoStop();
	void videoFastForward();
	void videoBack();
	void videoForward();
	void videoNextScene();
	void videoEndOfMovie();
	void startCapture();
	void stopCapture();
	void saveFrame();
};

#endif

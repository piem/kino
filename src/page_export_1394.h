/*
* page_export_1394.h Notebook Firewire/AVI/Still Frame Export Page Object
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PAGE_EXPORT_1394_H
#define _PAGE_EXPORT_1394_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>

#include "export.h"
#include "page_export.h"
#include "ieee1394io.h"


/** This class represents the Export/IEEE 1394 notebook page.
*/

class Export1394 : public Export
{
private:

	GtkEntry *entry_preroll;
	AVC *avc;
	IEEE1394Writer *exportWriter;

	gulong onActivate();
	/* Used to figure out what buttons are supported when pages are changed
	   If true, the 1394 system is loaded and OK*/
	bool system_loaded;
public:
	Export1394( PageExport*, KinoCommon* );
	virtual ~Export1394();

	void start();
	void clean();

private:
	enum export_result
	doExport( PlayList * playlist, int begin, int end, int every,
	          bool preview );
};

#endif


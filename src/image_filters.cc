/*
* image_filters.cc -- RGB24 image filters
* Copyright (C) 2002 Charles Yates <charles.yates@pandora.be>
* Copyright (C) 2002-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

// Project Includes

#include "image_filters.h"
#include "error.h"
#include "page_magick.h"
#include "kino_extra.h"

// C Includes

#include <string.h>
#include <stdio.h>
#include <glade/glade.h>

extern "C"
{
#include "support.h"
	extern GladeXML* magick_glade;
}

/** Class for simple pass through of images.
*/

class ImageFilterKeep : public GDKImageFilter, public NullImageFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "No Change" );
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{}
}
;

/** Converts images to black and white.
*/

class ImageFilterBlackWhite : public ImageFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Black & White" );
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		while ( p < ( pixels + width * height * 3 ) )
		{
			r = *( p );
			g = *( p + 1 );
			b = *( p + 2 );
			r = ( uint8_t ) ( 0.299 * r + 0.587 * g + 0.114 * b );
			*p ++ = r;
			*p ++ = r;
			*p ++ = r;
		}
	}
};

/** Fades in from black
*/

class ImageFilterFadeIn : public ImageFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Fade In" );
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		for ( uint8_t *p = pixels; p < ( pixels + width * height * 3 ); ++p )
			*p = uint8_t( *p * position + 0.5 );
	}
};

/** Fades out to black
*/

class ImageFilterFadeOut : public ImageFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Fade Out" );
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		double r = ( 1.0 - position );
		for ( uint8_t *p = pixels; p < ( pixels + width * height * 3 ); ++p )
			*p = uint8_t( *p * r + 0.5 );
	}
};

/** Converts images to sepia.
*/

class ImageFilterSepia : public ImageFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Sepia" );
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		while ( p < ( pixels + width * height * 3 ) )
		{
			r = *( p );
			g = *( p + 1 );
			b = *( p + 2 );
			r = ( uint8_t ) ( 0.299 * r + 0.587 * g + 0.114 * b );
			*p ++ = r < 225 ? ( int ) ( r + 30 ) : 0xff;
			*p ++ = r;
			*p ++ = r > 30 ? ( int ) ( r - 30 ) : 0;
		}
	}
};

/** Mirror the image in various ways.
*/

class ImageFilterMirror : public GDKImageFilter
{
private:
	GtkWidget *window;
	int type;

public:
	ImageFilterMirror()
	{
		window = glade_xml_get_widget( magick_glade, "image_filter_mirror" );
		GtkWidget* widget = glade_xml_get_widget( magick_glade, "optionmenu_mirror" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageFilterMirror()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Mirror" );
	}

	void LeftRight( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t * p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *i = p + y * width * 3;
			uint8_t *o = p + ( y + 1 ) * width * 3 - 1;
			for ( ; o > i; o -= 3 )
			{
				*( o - 2 ) = *i ++;
				*( o - 1 ) = *i ++;
				*o = *i ++;
			}
		}
	}

	void RightLeft( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t * p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *o = p + y * width * 3;
			uint8_t *i = p + ( y + 1 ) * width * 3 - 1;
			for ( ; i > o; i -= 3 )
			{
				*o ++ = *( i - 2 );
				*o ++ = *( i - 1 );
				*o ++ = *i;
			}
		}
	}

	void TopBottom( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t * p = pixels;
		for ( int y = 0; y < height / 2; y ++ )
		{
			uint8_t *i = p + y * width * 3;
			uint8_t *o = p + ( height - y ) * width * 3;
			memcpy( o, i, width * 3 );
		}
	}

	void BottomTop( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t * p = pixels;
		for ( int y = 0; y < height / 2; y ++ )
		{
			uint8_t *i = p + ( height - y ) * width * 3;
			uint8_t *o = p + y * width * 3;
			memcpy( o, i, width * 3 );
		}
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_mirror" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
		
		if ( type == 0 )
			LeftRight( pixels, width, height, position );
		else if ( type == 1 )
			RightLeft( pixels, width, height, position );
		else if ( type == 2 )
			TopBottom( pixels, width, height, position );
		else if ( type == 3 )
			BottomTop( pixels, width, height, position );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};

/** Kaleidoscope the image.
*/

class ImageFilterKaleidoscope: public GDKImageFilter
{
private:
	GtkWidget *window;
	int type;

public:
	ImageFilterKaleidoscope()
	{
		window = glade_xml_get_widget( magick_glade, "image_filter_kaleidoscope" );
		GtkWidget* widget = glade_xml_get_widget( magick_glade, "optionmenu_kaleidoscope" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageFilterKaleidoscope()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Kaleidoscope" );
	}

	void TopLeft( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		for ( int y = 0; y <= height / 2; y ++ )
		{
			uint8_t *i = p + y * width * 3;
			uint8_t *o = p + ( y + 1 ) * width * 3 - 1;
			uint8_t *o1 = p + ( height - y ) * width * 3;
			uint8_t *o2 = p + ( height - y + 1 ) * width * 3 - 1;
			while ( o > i )
			{
				r = *i ++;
				g = *i ++;
				b = *i ++;
				*o -- = b;
				*o -- = g;
				*o -- = r;
				*o1 ++ = r;
				*o1 ++ = g;
				*o1 ++ = b;
				*o2 -- = b;
				*o2 -- = g;
				*o2 -- = r;
			}
		}
	}

	void TopRight( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		for ( int y = 0; y <= height / 2; y ++ )
		{
			uint8_t *i = p + ( y + 1 ) * width * 3 - 1;
			uint8_t *o = p + y * width * 3;
			uint8_t *o1 = p + ( height - y ) * width * 3;
			uint8_t *o2 = p + ( height - y + 1 ) * width * 3 - 1;
			while ( i > o )
			{
				b = *i --;
				g = *i --;
				r = *i --;
				*o ++ = r;
				*o ++ = g;
				*o ++ = b;
				*o1 ++ = r;
				*o1 ++ = g;
				*o1 ++ = b;
				*o2 -- = b;
				*o2 -- = g;
				*o2 -- = r;
			}
		}
	}

	void BottomLeft( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		for ( int y = 0; y <= height / 2; y ++ )
		{
			uint8_t *o1 = p + y * width * 3;
			uint8_t *o2 = p + ( y + 1 ) * width * 3 - 1;
			uint8_t *i = p + ( height - y ) * width * 3;
			uint8_t *o = p + ( height - y + 1 ) * width * 3 - 1;
			while ( o > i )
			{
				r = *i ++;
				g = *i ++;
				b = *i ++;
				*o -- = b;
				*o -- = g;
				*o -- = r;
				*o1 ++ = r;
				*o1 ++ = g;
				*o1 ++ = b;
				*o2 -- = b;
				*o2 -- = g;
				*o2 -- = r;
			}
		}
	}

	void BottomRight( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		for ( int y = 0; y <= height / 2; y ++ )
		{
			uint8_t *o2 = p + ( y + 1 ) * width * 3 - 1;
			uint8_t *o1 = p + y * width * 3;
			uint8_t *o = p + ( height - y ) * width * 3;
			uint8_t *i = p + ( height - y + 1 ) * width * 3 - 1;
			while ( i > o )
			{
				b = *i --;
				g = *i --;
				r = *i --;
				*o ++ = r;
				*o ++ = g;
				*o ++ = b;
				*o1 ++ = r;
				*o1 ++ = g;
				*o1 ++ = b;
				*o2 -- = b;
				*o2 -- = g;
				*o2 -- = r;
			}
		}
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_kaleidoscope" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
		
		if ( type == 0 )
			TopLeft( pixels, width, height, position );
		else if ( type == 1 )
			TopRight( pixels, width, height, position );
		else if ( type == 2 )
			BottomLeft( pixels, width, height, position );
		else if ( type == 3 )
			BottomRight( pixels, width, height, position );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};

/** Reverse video filter.
*/

class ImageFilterReverseVideo : public ImageFilter
{
public:
	char *GetDescription( ) const
	{
		return _( "Reverse Video" );
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		while ( p < ( pixels + width * height * 3 ) )
		{
			r = *( p );
			g = *( p + 1 );
			b = *( p + 2 );
			*p ++ = 0xff - r;
			*p ++ = 0xff - g;
			*p ++ = 0xff - b;
		}
	}
};

/** Swap the image left to right.
*/

class ImageFilterSwap : public GDKImageFilter
{
private:
	GtkWidget *window;
	int type;

public:
	ImageFilterSwap()
	{
		window = glade_xml_get_widget( magick_glade, "image_filter_swap" );
		GtkWidget* widget = glade_xml_get_widget( magick_glade, "optionmenu_swap" );
		g_signal_connect( G_OBJECT( widget ), "changed", G_CALLBACK( Repaint ), 0 );
	}

	virtual ~ImageFilterSwap()
	{
		gtk_widget_destroy( window );
	}

	char *GetDescription( ) const
	{
		return _( "Flip" );
	}

	void LeftRight( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t r, g, b;
		uint8_t *p = pixels;
		for ( int y = 0; y < height; y ++ )
		{
			uint8_t *i = p + y * width * 3;
			uint8_t *o = p + ( y + 1 ) * width * 3 - 1;
			for ( int x = 0; x < width / 2; x ++ )
			{
				r = *i;
				g = *( i + 1 );
				b = *( i + 2 );
				*i ++ = *( o - 2 );
				*i ++ = *( o - 1 );
				*i ++ = *o;
				*o -- = b;
				*o -- = g;
				*o -- = r;
			}
		}
	}

	void TopBottom( uint8_t *pixels, int width, int height, double position )
	{
		uint8_t * p = pixels;
		uint8_t keyFrame[ 720 * 3 ];
		for ( int y = 0; y < height / 2; y ++ )
		{
			uint8_t *i = p + y * width * 3;
			uint8_t *o = p + ( height - y - 1 ) * width * 3;
			memcpy( keyFrame, i, width * 3 );
			memcpy( i, o, width * 3 );
			memcpy( o, keyFrame, width * 3 );
		}
	}

	void FilterFrame( uint8_t *pixels, int width, int height, double position, double frame_delta )
	{
		GtkMenu * menu = GTK_MENU( gtk_option_menu_get_menu( GTK_OPTION_MENU( lookup_widget( window, "optionmenu_swap" ) ) ) );
		GtkWidget *active_item = gtk_menu_get_active( menu );
		type = g_list_index ( GTK_MENU_SHELL ( menu ) ->children, active_item );
		
		if ( type == 0 )
			LeftRight( pixels, width, height, position );
		else if ( type == 1 )
			TopBottom( pixels, width, height, position );
	}

	void AttachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( window ) ) ->child, GTK_WIDGET( bin ) );
	}

	void DetachWidgets( GtkBin *bin )
	{
		gtk_widget_reparent( ( GTK_BIN( bin ) ) ->child, GTK_WIDGET( window ) );
	}
};

/** Callback for selection change.
*/

static void
on_optionmenu_selected ( GtkMenuItem *menu_item, gpointer user_data )
{
	( ( GDKImageFilterRepository * ) user_data ) ->SelectionChange();
}

/** Constructor for the image filter repository.
 
  	Registers an instance of each known filter for later GUI exposure via the Initialise method.
*/

GDKImageFilterRepository::GDKImageFilterRepository()
	: selected_filter( 0 )
{
	// Register an instance of each object (adapting raw filters to GDK filters where necessary)
	Register( new ImageFilterKeep() );
	Register( new GDKImageFilterAdapter( new ImageFilterBlackWhite() ) );
	Register( new ImageFilterKaleidoscope() );
	Register( new GDKImageFilterAdapter( new ImageFilterFadeIn() ) );
	Register( new GDKImageFilterAdapter( new ImageFilterFadeOut() ) );
	Register( new ImageFilterSwap() );
	Register( new ImageFilterMirror() );
	Register( new GDKImageFilterAdapter( new ImageFilterReverseVideo() ) );
	Register( new GDKImageFilterAdapter( new ImageFilterSepia() ) );
}

/** Destructor for the image repository - clears all the registered filters
*/

GDKImageFilterRepository::~GDKImageFilterRepository()
{
	// Remove the filters in the repository
	for ( unsigned int index = 0; index < filters.size(); index ++ )
		delete filters[ index ];
}

/** Register an image filter.
*/

void GDKImageFilterRepository::Register( GDKImageFilter *filter )
{
	std::cerr << ">>> Image Filter: " << filter->GetDescription( ) << std::endl;
	filters.push_back( filter );
}

/** Initialise the option menu with the current list of registered filters.
*/

void GDKImageFilterRepository::Initialise( GtkOptionMenu *menu, GtkBin *container )
{
	// Store these for future reference
	this->menu = menu;
	this->container = container;

	// Add the filters to the menu
	GtkMenu *menu_new = GTK_MENU( gtk_menu_new( ) );
	for ( unsigned int index = 0; index < filters.size(); index ++ )
	{
		GtkWidget *item = gtk_menu_item_new_with_label( filters[ index ] ->GetDescription( ) );
		gtk_widget_show( item );
		gtk_menu_append( menu_new, item );
		g_signal_connect( G_OBJECT( item ), "activate", G_CALLBACK( on_optionmenu_selected ), this );
	}
	gtk_menu_set_active( menu_new, 0 );
	gtk_option_menu_set_menu( menu, GTK_WIDGET( menu_new ) );

	// Register the selected items widgets
	SelectionChange();
}

/** Get the currently selected image filter.
*/

GDKImageFilter *GDKImageFilterRepository::Get( ) const
{
	GtkMenu * filterMenu = GTK_MENU( gtk_option_menu_get_menu( menu ) );
	GtkWidget *active_item = gtk_menu_get_active( filterMenu );
	return filters[ g_list_index( GTK_MENU_SHELL( filterMenu ) ->children, active_item ) ];
}

/** Handle attach/detach widgets on last selected/selected items.
*/

void GDKImageFilterRepository::SelectionChange( )
{
	bool isPreviewing = false;
	PageMagick* magick = 0;

	if ( common && common->getPageMagick( ) )
		magick = common->getPageMagick( );
	if ( magick && magick->IsPreviewing() )
	{
		isPreviewing = true;
		magick->StopPreview();
	}

	// Detach the selected filters widgets
	if ( selected_filter != NULL )
		selected_filter->DetachWidgets( container );

	if ( magick )
	{
		magick->SetKeyFrameControllerClient(0);
		magick->ShowCurrentStatus( magick->GetCurrentPosition(), LOCKED_KEY, false, false );
	}

	// Get the new selection
	selected_filter = Get();

	// Attach the new filters widgets
	if ( selected_filter != NULL )
		selected_filter->AttachWidgets( container );

	if ( magick )
	{
		if ( isPreviewing )
			magick->StartPreview();
		else
			magick->PreviewFrame();
	}
}

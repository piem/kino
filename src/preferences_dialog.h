/*
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _PREFERENCES_DIALOG_H
#define _PREFERENCES_DIALOG_H 1

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C"
{
#endif

	void
	on_preferences_activate ( GtkMenuItem * menuitem,
	                          gpointer user_data );

	void
	on_avc_phyid_activate ( GtkMenuItem * menuitem,
	                        gpointer user_data );

	void
	on_radiobutton_avi1_clicked ( GtkButton * button,
	                              gpointer user_data );

	void
	on_radiobutton_avi2_clicked ( GtkButton * button,
	                              gpointer user_data );

	void
	on_radiobutton_rawdv_clicked ( GtkButton * button,
	                               gpointer user_data );

	void
	on_preferences_dialog_ok_button_clicked
	( GtkButton * button,
	  gpointer user_data );

	void
	on_preferences_dialog_cancel_button_clicked
	( GtkButton * button,
	  gpointer user_data );

	void
	on_radiobutton_GDK_clicked ( GtkButton * button,
	                             gpointer user_data );

	void
	on_radiobutton_XX_clicked ( GtkButton * button,
	                            gpointer user_data );

	void
	on_radiobutton_XV_clicked ( GtkButton * button,
	                            gpointer user_data );

	void
	on_button_capture_open_clicked( GtkButton       *button,
	                                gpointer         user_data );

	/* JogShuttle stuff */
	gboolean
	on_frame_jogshuttle_grab_frame_enter_notify_event
	( GtkWidget * widget,
	  GdkEventCrossing * event,
	  gpointer user_data );

	gboolean
	on_frame_jogshuttle_grab_frame_leave_notify_event
	( GtkWidget * widget,
	  GdkEventCrossing * event,
	  gpointer user_data );


#ifdef __cplusplus
}
#endif

#endif


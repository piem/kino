/*
* message.h -- dialogs and prompts
* Copyright (C) 2000 Arne Schirmacher <arne@schirmacher.de>
* Copyright (C) 2001-2007 Dan Dennedy <dan@dennedy.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _MESSAGE_H
#define _MESSAGE_H

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C"
{
#endif

void modal_message ( const char * message, ... );
char *modal_prompt( const char * message, ... );
int modal_confirm( const char * affirm_label, const char *close_label, const char *message, ... );
int modal_confirm_files( const char * affirm_label, const char *close_label, const char* files, const char *message, ... );
	
void modal_message_with_parent ( GtkWidget *parent, const char * message, ... );
char *modal_prompt_with_parent( GtkWidget *parent, const char * message, ... );
int modal_confirm_with_parent( const char * affirm_label, const char *close_label, GtkWidget *parent, const char *message, ... );
int modal_confirm_files_with_parent( const char * affirm_label, const char *close_label, GtkWidget *parent, const char* files, const char *message, ... );

#ifdef __cplusplus
}
#endif

#endif

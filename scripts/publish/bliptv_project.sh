#!/bin/sh
#
# A Kino publishing script to upload to http://blip.tv as Ogg Theora
# Copyright (C) 2007 Dan Dennedy <dan@dennedy.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

POST_URL='http://blip.tv/'
EDIT_URL='http://blip.tv/file/post/'

getXmlAttributeValue()
{
	attribute="$1"
	filename="$2"
	echo "$@" | awk "
		/${attribute}=\"/ {
			split(\$0, x, \"\\\"\");
			z = 0;
			for (y in x) {
				if (x[y] ~ /${attribute}=/) {
					z = y + 1;
					print x[z];
					exit
				}
			}
		}
	" "$filename"
}

getBlipTvId()
{
	echo "$@" | awk '
		BEGIN { FS = ">" }
		/item_id/ { print int($2) }
	'
}

project_file="$1"
browser="$2"

path=$(dirname "$project_file")
base=$(basename "$project_file" '.kino')
base=$(basename "$base" '.smil')
base=$(basename "$base" '.xml')
ogg_file="${path}/${base}.ogg"

echo Login to blip.tv
read -p "  Username: " username
stty -echo
read -p "  Password: " password
echo
stty echo
echo
read -p "Do you want higher quality (slower and bigger to convert & upload)? [y/N] " quality

title=$(getXmlAttributeValue 'title' "$project_file")
[ -z "$title" ] && title="Untitled"
author=$(getXmlAttributeValue 'author' "$project_file")
[ -z "$author" ] && author="Anonymous"
abstract=$(getXmlAttributeValue 'abstract' "$project_file")
copyright=$(getXmlAttributeValue 'copyright' "$project_file")
copyright=$(echo $copyright | awk '{print int($0)}')
[ -z "$copyright" ] || [ "$copyright" = "0" ] && copyright="-1"

echo
echo Transcoding "$project_file"
echo to "$ogg_file"
aspect=$(kino2raw "$project_file" aspect)
normalisation=$(kino2raw "$project_file" normalisation)
if [ "$aspect" = "16:9" ]; then
	if [ "$normalisation" = "pal" ]; then
		full_res_x="1024"
		full_res_y="576"
		med_res_x="512"
		med_res_y="288"
	else
		full_res_x="856"
		full_res_y="480"
		med_res_x="424"
		med_res_y="240"
	fi
else
	if [ "$normalisation" = "pal" ]; then
		full_res_x="768"
		full_res_y="576"
		med_res_x="384"
		med_res_y="288"
	else
		full_res_x="640"
		full_res_y="480"
		med_res_x="320"
		med_res_y="240"
	fi
fi

# Try ffmpeg2theora first
which ffmpeg2theora 2> /dev/null > /dev/null
if [ $? -eq 0 ]; then
	if [ -z "$quality" ] || [ "$quality" = "n" ] || [ "$quality" = "N" ] || [ "$quality" = "no" ] || [ "$quality" = "No" ] || [ "$quality" = "NO" ]; then
		kino2raw "$project_file" |
		ffmpeg2theora -f dv --optimize -x $med_res_x -y $med_res_y \
			-K 200 -v 5 -a 3 -H 32000 -o "$ogg_file" --artist "$author" --title "$title" --location "$abstract" -
	else
		kino2raw "$project_file" |
		ffmpeg2theora -f dv --optimize -x $full_res_x -y $full_res_y \
			-K 200 -v 7 -a 5 -H 44100 -o "$ogg_file" --artist "$author" --title "$title" --location "$abstract" -
	fi
else
	# Try gstreamer next
	. "$(dirname $0)/../exports/gstreamer_utils.sh"

	# Run the transcode
	if [ -z "$quality" ] || [ "$quality" = "n" ] || [ "$quality" = "N" ] || [ "$quality" = "no" ] || [ "$quality" = "No" ] || [ "$quality" = "NO" ]; then
		# compute other framerates
		halffps=$( [ "$normalisation" = "pal" ] && echo "25/2" || echo "30000/2002" )
	
		kino2raw "$project_file" |
		$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
			video/x-dv,systemstream=\(boolean\)false ! $dvdec ! videorate ! \
			video/x-raw-yuv,framerate=$halffps ! ffmpegcolorspace ! videocrop left=8 right=8 ! \
			videoscale method=0 ! video/x-raw-yuv,width=$med_res_x,height=$med_res_y,pixel-aspect-ratio=\(fraction\)1/1 ! \
			theoraenc bitrate=500 keyframe-freq=200 ! oggmux name=mux ! filesink location="$ogg_file" demux. ! \
			audio/x-raw-int ! queue ! audioresample ! audio/x-raw-int,rate=32000 ! audioconvert ! vorbisenc max-bitrate=64000 ! mux.
	else
		kino2raw "$project_file" |
		$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
			video/x-dv,systemstream=\(boolean\)false ! $dvdec ! \
			ffmpegcolorspace ! videocrop left=8 right=8 ! $deinterlace \
			videoscale method=1 ! video/x-raw-yuv,width=$full_res_x,height=$full_res_y,pixel-aspect-ratio=\(fraction\)1/1 ! \
			theoraenc quality=44 ! oggmux name=mux ! filesink location="$ogg_file" demux. ! \
			audio/x-raw-int ! queue ! audioconvert ! vorbisenc quality=0.3 ! mux.
	fi
fi

echo
echo Uploading to blip.tv
curl_file="file=@${ogg_file};type=application/ogg"
result=$(curl -F "$curl_file" \
	-F "skin=api" -F "section=file" -F "cmd=post" -F "post=1" \
	-F "userlogin=$username" \
	-F "password=$password" \
	-F "title=$title" \
	-F "description=$abstract" \
	-F "license=$copyright" \
	"$POST_URL")
echo
post_id=$(getBlipTvId "$result")
if [ -n "$post_id" ]; then
	echo 'Close the window when you are finished editing your post.'
	echo
	$browser "${EDIT_URL}${post_id}/"
else
	echo $result
	echo 'Close the window when you are finished reading this error.'
fi
read

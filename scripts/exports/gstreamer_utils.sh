#!/bin/sh
# A Kino export script helper for gstreamer - determine availability and executables

# find gst-launch and gst-inspect
isgstlaunch=$( which gst-launch > /dev/null 2>&1 ; echo $? )
isgstlaunch10=$( which gst-launch-0.10 > /dev/null 2>&1 ; echo $? )
isgstinspect=$( which gst-inspect > /dev/null 2>&1 ; echo $? )
isgstinspect10=$( which gst-inspect-0.10 > /dev/null 2>&1 ; echo $? )
gstlaunch=$( ( [ $isgstlaunch -eq 0 ] && echo gst-launch ) || ( [ $isgstlaunch10 -eq 0 ] && echo gst-launch-0.10 ) )
gstinspect=$( ( [ $isgstinspect -eq 0 ] && echo gst-inspect ) || ( [ $isgstinspect10 -eq 0 ] && echo gst-inspect-0.10 ) )

# Usable?
if [ "$gstinspect" != "" ] && [ "$gstlaunch" != "" ]; then
	# we'll use gst-inspect to further validate
	isdvdemux=$( $gstinspect dvdemux > /dev/null ; echo $? ) # good
	isdvdec=$( $gstinspect dvdec > /dev/null ; echo $? ) # good
	isffmpegcolorspace=$( $gstinspect ffmpegcolorspace > /dev/null ; echo $? ) # base
	isoggmux=$( $gstinspect oggmux > /dev/null ; echo $? ) # base
	istheoraenc=$( $gstinspect theoraenc > /dev/null ; echo $? ) # base
	isvorbisenc=$( $gstinspect vorbisenc > /dev/null ; echo $? ) # base
	gststatus=$( ( \
		[ $isdvdemux -eq 0 ] && [ $isdvdec -eq 0 ] && [ $isffmpegcolorspace -eq 0 ] &&
		[ $isoggmux -eq 0 ] && [ $istheoraenc -eq 0 ] && [ $isvorbisenc -eq 0 ] ) &&
		echo Active || echo Inactive )
else
	# do not require gst-inspect, rather hope the user has the base and good plugin sets if Active
	gststatus=$( [ "$gstlaunch" != "" ] && echo Active || echo Inactive )
fi

# locate a deinterlacer, if any, through gst-inspect, if it exists
if [ "$gstinspect" != "" ]; then
	isdeinterlace=$( $gstinspect deinterlace > /dev/null ; echo $? )
	isffdeinterlace=$( $gstinspect ffdeinterlace > /dev/null ; echo $? )
	deinterlace=$( ( [ $isffdeinterlace -eq 0 ] && echo "ffdeinterlace !" ) || ( [ $isdeinterlace -eq 0 ] && echo "deinterlace !" ) )

	# prefer the ffmpeg dv decoder (better but in the ffmpeg set), but failover to libdv decoder (good set)
	isffdecdvvideo=$( $gstinspect ffdec_dvvideo > /dev/null ; echo $? )
	dvdec=$( [ $isffdecdvvideo -eq 0 ] && echo "ffdec_dvvideo" || echo "dvdec" )
fi

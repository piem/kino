#!/bin/sh
# A Kino export script that outputs to Ogg Dirac using ffmpeg2dirac

usage()
{
	# Title
	echo "Title: Ogg Dirac (ffmpeg2dirac)"

	# Usable?
	which ffmpeg2dirac > /dev/null 2>&1
	[ $? -eq 0 ] && echo Status: Active || echo Status: Inactive

	# Type
	echo Flags: single-pass file-producer
	
	# Profiles
	echo "Profile: Best Quality (native size)"
	echo "Profile: High Quality (full size)"
	echo "Profile: Medium Quality (medium size)"
	echo "Profile: Broadband Quality (medium size, 564 Kbps)"
	echo "Profile: Low Quality (small size, 128Kbps)"
}

execute()
{
	# Arguments
	normalisation="$1"
	length="$2"
	profile="$3"
	file="$4"
	project="$5"
	aspect="$7"

	# generate filename if missing
	[ "x$file" = "x" ] && file="kino_export_"`date +%Y-%m-%d_%H.%M.%S`

	# Create metadata options
	title=`awk '/title="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /title=/) z = y + 1; print x[z]; exit}' "$project"`
	artist=`awk '/author="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /author=/) z = y + 1; print x[z]; exit}' "$project"`
	location=`awk '/abstract="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /abstract=/) z = y + 1; print x[z]; exit}' "$project"`
	copyright=`awk '/copyright="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /copyright=/) z = y + 1; print x[z]; exit}' "$project"`

	# some versions seem to not overwrite existing files correctly
	rm "$file".ogv

	if [ "$aspect" = "16:9" ]; then
		if [ "$normalisation" = "pal" ]; then
			full_res_x="1024"
			full_res_y="576"
			med_res_x="512"
			med_res_y="288"
			low_res_x="256"
			low_res_y="144"
		else
			full_res_x="856"
			full_res_y="480"
			med_res_x="424"
			med_res_y="240"
			low_res_x="216"
			low_res_y="120"
		fi
	else
		if [ "$normalisation" = "pal" ]; then
			full_res_x="768"
			full_res_y="576"
			med_res_x="384"
			med_res_y="288"
			low_res_x="192"
			low_res_y="144"
		else
			full_res_x="640"
			full_res_y="480"
			med_res_x="320"
			med_res_y="240"
			low_res_x="160"
			low_res_y="120"
		fi
	fi

	# Run the command
	case "$profile" in 
		"0" ) 	ffmpeg2dirac -f dv -v 10 -S 2 -a 10 \
				--title "$title" --artist "$artist" --location "$location" --copyright "$copyright" \
				-o "$file".ogv - ;;
		"1" ) 	ffmpeg2dirac -f dv -x $full_res_x -y $full_res_y -v 7 -a 3 \
				--title "$title" --artist "$artist" --location "$location" --copyright "$copyright" \
				-o "$file".ogv - ;;
		"2" ) 	ffmpeg2dirac -f dv -x $med_res_x -y $med_res_y -v 7 -a 3 -H 44100 \
				--title "$title" --artist "$artist" --location "$location" --copyright "$copyright" \
				-o "$file".ogv - ;;
		"3" ) 	ffmpeg2dirac -f dv -x $med_res_x -y $med_res_y -v 5 -V 500 -S 1 -K 200 -a 3 -A 64 -H 32000 \
				--title "$title" --artist "$artist" --location "$location" --copyright "$copyright" \
				-o "$file".ogv - ;;
		"4" ) 	ffmpeg2dirac -f dv -x $low_res_x -y $low_res_y -v 1 -V 84 -S 2 -K 200 -a 0 -A 44 -H 22050 \
				--title "$title" --artist "$artist" --location "$location" --copyright "$copyright" \
				-o "$file".ogv - ;;
	esac
}

[ "$1" = "--usage" ] || [ -z "$1" ] && usage "$@" || execute "$@"

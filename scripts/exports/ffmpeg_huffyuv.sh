#!/bin/sh
# A Kino export script that outputs to MPEG-4 AVI with ffmpeg

usage()
{
	# Title
	echo "Title: HuffYUV AVI (FFMPEG)"

	# Usable?
	which ffmpeg > /dev/null
	[ $? -eq 0 ] && echo Status: Active || echo Status: Inactive

	# Type
	echo Flags: single-pass file-producer

	# Profiles
	echo "Profile: Native size"
	echo "Profile: Full size"
	echo "Profile: Medium size"
	echo "Profile: Small size"
}

execute()
{
	# Arguments
	normalisation="$1"
	length="$2"
	profile="$3"
	file="$4"
	aspect="$7"

	. "`dirname $0`/ffmpeg_utils.sh"
	ffmpeg_generate_hq

	# generate filename if missing
	[ "x$file" = "x" ] && file="kino_export_"`date +%Y-%m-%d_%H.%M.%S`
	
	# Run the command
	case "$profile" in 
		"0" ) 	ffmpeg -threads $threads -f dv -i pipe: $interlace -s $normalisation \
			-aspect $aspect -vcodec huffyuv -pix_fmt yuv422p \
			-acodec pcm_s16le -y "$file".avi ;;
		"1" ) 	ffmpeg -threads $threads -f dv -i pipe: $interlace -s $full_res \
			-aspect $aspect -vcodec huffyuv -pix_fmt yuv422p \
			-acodec pcm_s16le -y "$file".avi ;;
		"2" ) 	ffmpeg -threads $threads -f dv -i pipe: $progressive -s $med_res \
			-aspect $aspect -vcodec huffyuv -pix_fmt yuv422p \
			-acodec pcm_s16le -y "$file".avi ;;
		"3" ) 	ffmpeg -threads $threads -f dv -i pipe: $progressive -s $low_res \
			-aspect $aspect -vcodec huffyuv -pix_fmt yuv422p \
			-acodec pcm_s16le -y "$file".avi ;;
	esac
}

[ "$1" = "--usage" ] || [ -z "$1" ] && usage "$@" || execute "$@"

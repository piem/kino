#!/bin/sh
# A Kino export script that outputs to Ogg Theora using gstreamer

. "`dirname $0`/gstreamer_utils.sh"

usage()
{
	# Title
	echo "Title: Ogg Theora (gstreamer)"

	echo Status: $gststatus

	# Type
	echo Flags: single-pass file-producer
	
	# Profiles
	echo "Profile: Best Quality (native size)"
	echo "Profile: High Quality (full size)"
	echo "Profile: Medium Quality (medium size)"
	echo "Profile: Broadband Quality (medium size, 564 Kbps)"
	echo "Profile: Low Quality (small size, 128Kbps)"
}

execute()
{
	# Arguments
	normalisation="$1"
	length="$2"
	profile="$3"
	file="$4"
	project="$5"
	aspect="$7"

	# generate filename if missing
	[ "x$file" = "x" ] && file="kino_export_"`date +%Y-%m-%d_%H.%M.%S`

	# Create metadata options
	title=`awk '/title="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /title=/) z = y + 1; print x[z]; exit}' "$project"`
	artist=`awk '/author="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /author=/) z = y + 1; print x[z]; exit}' "$project"`
	location=`awk '/abstract="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /abstract=/) z = y + 1; print x[z]; exit}' "$project"`
	copyright=`awk '/copyright="/ {split($0, x, "\""); z = 0; for (y in x) if (x[y] ~ /copyright=/) z = y + 1; print x[z]; exit}' "$project"`

	# some versions seem to not overwrite existing files correctly
	rm "$file".ogg > /dev/null 2>&1

	# compute resolutions with square pixels
	if [ "$aspect" = "16:9" ]; then
		if [ "$normalisation" = "pal" ]; then
			full_res_x="1024"
			full_res_y="576"
			med_res_x="512"
			med_res_y="288"
			low_res_x="256"
			low_res_y="144"
		else
			full_res_x="856"
			full_res_y="480"
			med_res_x="424"
			med_res_y="240"
			low_res_x="216"
			low_res_y="120"
		fi
	else
		if [ "$normalisation" = "pal" ]; then
			full_res_x="768"
			full_res_y="576"
			med_res_x="384"
			med_res_y="288"
			low_res_x="192"
			low_res_y="144"
		else
			full_res_x="640"
			full_res_y="480"
			med_res_x="320"
			med_res_y="240"
			low_res_x="160"
			low_res_y="120"
		fi
	fi

	# compute other framerates
	halffps=$( [ "$normalisation" = "pal" ] && echo "25/2" || echo "30000/2002" )

	# Run the command
	case "$profile" in 
		"0" ) 	$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
				video/x-dv,systemstream=\(boolean\)false ! $dvdec ! \
				ffmpegcolorspace ! $deinterlace \
				theoraenc quality=63 ! oggmux name=mux ! filesink location="$file".ogv demux. ! \
				audio/x-raw-int ! queue ! audioconvert ! vorbisenc quality=1.0 ! mux. ;;

		"1" ) 	$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
				video/x-dv,systemstream=\(boolean\)false ! $dvdec ! \
				ffmpegcolorspace ! videocrop left=8 right=8 ! $deinterlace \
				videoscale method=1 ! video/x-raw-yuv,width=$full_res_x,height=$full_res_y,pixel-aspect-ratio=\(fraction\)1/1 ! \
				theoraenc quality=44 ! oggmux name=mux ! filesink location="$file".ogv demux. ! \
				audio/x-raw-int ! queue ! audioconvert ! vorbisenc quality=0.3 ! mux. ;;

		"2" ) 	$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
				video/x-dv,systemstream=\(boolean\)false ! $dvdec ! videorate ! \
				video/x-raw-yuv ! ffmpegcolorspace ! videocrop left=8 right=8 ! \
				videoscale method=0 ! video/x-raw-yuv,width=$med_res_x,height=$med_res_y,pixel-aspect-ratio=\(fraction\)1/1 ! \
				theoraenc quality=44 ! oggmux name=mux ! filesink location="$file".ogv demux. ! \
				audio/x-raw-int ! queue ! audioconvert ! vorbisenc quality=0.3 ! mux. ;;

		"3" ) 	$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
				video/x-dv,systemstream=\(boolean\)false ! $dvdec ! videorate ! \
				video/x-raw-yuv,framerate=$halffps ! ffmpegcolorspace ! videocrop left=8 right=8 ! \
				videoscale method=0 ! video/x-raw-yuv,width=$med_res_x,height=$med_res_y,pixel-aspect-ratio=\(fraction\)1/1 ! \
				theoraenc bitrate=500 keyframe-freq=200 ! oggmux name=mux ! filesink location="$file".ogv demux. ! \
				audio/x-raw-int ! queue ! audioresample ! audio/x-raw-int,rate=32000 ! audioconvert ! vorbisenc max-bitrate=64000 ! mux. ;;

		"4" ) 	$gstlaunch filesrc use-mmap=false location=/dev/stdin ! dvdemux name=demux ! queue ! \
				video/x-dv,systemstream=\(boolean\)false ! $dvdec ! videorate ! \
				video/x-raw-yuv,framerate=$halffps ! ffmpegcolorspace ! videocrop left=8 right=8 ! \
				videoscale method=0 ! video/x-raw-yuv,width=$low_res_x,height=$low_res_y,pixel-aspect-ratio=\(fraction\)1/1 ! \
				theoraenc bitrate=84 keyframe-freq=150 ! oggmux name=mux ! filesink location="$file".ogv demux. ! \
				audio/x-raw-int ! queue ! audioresample ! audio/x-raw-int,rate=22050 ! audioconvert ! vorbisenc max-bitrate=44000 ! mux. ;;
	esac
}

[ "$1" = "--usage" ] || [ -z "$1" ] && usage "$@" || execute "$@"

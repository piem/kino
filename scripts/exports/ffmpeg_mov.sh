#!/bin/sh

usage()
{
	# Title
	echo "Title: Quicktime DV (FFMPEG)"

	# Usable?
	which ffmpeg > /dev/null 2>&1
	[ $? -eq 0 ] && echo Status: Active || echo Status: Inactive

	# Type
	echo Flags: single-pass file-producer
}

execute()
{
	# Arguments
	normalisation="$1"
	length="$2"
	profile="$3"
	file="$4"

	# generate filename if missing
	[ "x$file" = "x" ] && file="kino_export_"`date +%Y-%m-%d_%H.%M.%S`

	# Run the command
	ffmpeg -f dv -i - -vcodec copy -acodec pcm_s16le -y "$file".mov
}

[ "$1" = "--usage" ] || [ -z "$1" ] && usage "$@" || execute "$@"

#!/bin/sh
# A Kino script that invokes Brasero on a generated dvdauthor xml file.

usage()
{
	# Title
	echo "Title: Burn with Brasero"

	# Usable?
	which dvdauthor > /dev/null && which brasero > /dev/null
	[ $? -eq 0 ] && echo Status: Active || echo Status: Inactive
}

execute()
{
	xml="$1"
	output="$2"

	dvdauthor -o "$output" -x "$xml" && \
	brasero -d "$output"/* &
}

[ "$1" = "--usage" ] || [ -z "$1" ] && usage "$@" || execute "$@"

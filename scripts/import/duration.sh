#!/bin/sh
# A Kino script that attempts to get the duration of the video

# Use local ffmpeg if regular ffmpeg no available
which ffmpeg > /dev/null
[ $? -eq 0 ] && ffmpeg="ffmpeg" || ffmpeg="ffmpeg-kino"

IN="$1"
echo $($ffmpeg -i "$IN" 2>&1 | awk '/Duration:/ {print $2}' | cut -d, -f1)00
